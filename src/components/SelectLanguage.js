import React from 'react';
import { getLocalStorage } from 'components/Helper';

class SelectLanguage extends React.Component {
  state =
    {
      value: 'en',
      loginData: {}
    };

  async  componentWillMount() {
    let loginData = getLocalStorage('loginDetail');
    await this.setState({ loginData })

  }

  handleChange = (event) => {
    this.setState({ value: event.target.value });
    this.props.setCurrentLanguage(event.target.value);
  }
  render() {
    const { currentLanguage } = this.props;
    let currentRole = this.state.loginData.role;
    return (
      <div>
        {
          currentRole != "Admin" &&
          <span className="lang__button"
            style={{
              position: "absolute",
              left: "1050px"
            }}
          >
            <select value={currentLanguage} onChange={this.handleChange}>
              <option value="en">EN</option>
              <option value="zh">ZH</option>
            </select>
          </span>
        }
      </div>



    );
  }
}

export default SelectLanguage