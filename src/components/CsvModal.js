import React from 'react';
import PropTypes from 'prop-types';
import {Button} from 'reactstrap';
import _ from 'lodash';
import FileDownload from 'js-file-download';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../localization/data';

let strings = new LocalizedStrings(
  data
);


export const CsvModalHeader = (props) => {
  strings.setLanguage(props.currentLanguage);
  return(
    <div className="headertext">
      {(props.activeCsvModal === 'upload') ?
        <div>
        <h2 className="heading-model-new">{strings.btnGrpText.uploadCSV}</h2>
        {props.viewToBeUpdated === "outgoingOrders" ?<h3 className="text1">{strings.modalText.uploadText}</h3>: <h3 className="text1">{strings.modalText.uploadCSVShipment}</h3> }
        </div>
        :
        <div>
        <h2 className="heading-model-new">{strings.btnGrpText.downloadCSV}</h2>
        <h3 className="text1">{strings.modalText.downloadText}</h3>
        </div>
      }
      <Button className="modal-btn" onClick={() => props.handleCsvModal(false, props.activeCsvModal)}>X</Button>
    </div>
  );
}

/** LinkModal
 *
 * @description This class having a modal that will display a list of incoming orders and having a filter on that list
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class CsvModal extends React.Component {
  constructor(props) {
    super(props);
    this.onFileSelected = this.onFileSelected.bind(this);
    this.onFilesError = this.onFilesError.bind(this);
    this.openFileExplorer = this.openFileExplorer.bind(this);
    this.downloadFile = this.downloadFile.bind(this);
    this.getFileName = this.getFileName.bind(this);

    this.state = { 
      selectedFile: null,
      loaded: 0,
      strip1: '',
      strip2: '',
    }
  }

  async componentWillMount() {
    if (this.props.owner === 'Order_Catalogue') {
      await this.setState({
        strip1: 'order_company_catalogue.xlxs',
        strip2: 'order_product_catalogue.xlxs',
      });
    } else if (this.props.owner === 'Shipment_Catalogue') {
      await this.setState({
        strip1: 'shipment_company_catalogue.xlxs',
        strip2: 'shipment_product_catalogue.xlxs',
      });
    } else {
      await this.setState({strip1: this.props.owner+'.'+'csv'});
    }
  }

  componentWillUnmount() {
    this.props.handleCsvModal(false, this.props.activeCsvModal);
  }

  async onFileSelected(event) {
    await this.setState({selectedFile: event.target.files[0]});

    // uploading files when file is selected (not undefined)
    if (this.state.selectedFile !== undefined) {
      await this.props.uploadFile(this.state.selectedFile, this.props.owner);
      this.props.updateView(this.props.viewToBeUpdated);
    }
  }

  onFilesError(error, file) {
    console.log('error code ' + error.code + ': ' + error.message);
  }

  openFileExplorer() {
    this.refs.fileUploader.click();
  }

  getFileName(type, key) {
    let fileName = 'default.csv';
    if (this.props.owner === 'Order_Catalogue') {
      if (key === 'FIRST') return fileName = 'order_company_catalogue.xlsx';

      return fileName = 'order_product_catalogue.xlsx';
    } else if (this.props.owner === 'Shipment_Catalogue') {
      if (key === 'FIRST') return fileName = 'shipment_company_catalogue.xlsx';

      return fileName = 'shipment_product_catalogue.xlsx';
    } else {
      return fileName = this.props.owner+'.'+type;
    }
    return fileName;
  }

  downloadFile(type, key, shipProd, shipCompany, orderCompany, orderProd) {
    console.log("Function to download file is called");
    console.log(this.props.owner);
    let fileName = this.getFileName(type, key);
    let data = '';
    if (this.props.owner === 'Shipment') {

      // Creating Shipment Template
      if (this.props.role === 'Birla Cellulose') {
      data = 'invoice_number*,invoice_date*,buyer_name*,product*,product_description,product_quantity*,product_unit*,product_lot_number,seller_unit,buyer_unit,transport_mode,shipment_description,buyer_order_number,received_invoice_number*,supplier_name*,consumed_product*,consumed_quantity,consumed_unit\n';
       data += 'Sample IN-123, 20-02-2019 ,Sample Buyer,Sample Product, Sample Product description, 100 , pieces,Sample lot,Sample seller unit,Sample buyer unit,Sample transport Mode, Sample shipment description, Sample PO ,'
     } else if(this.props.role === 'Pulp'){
      data = 'invoice_number*,invoice_date*,buyer_name*,product*,product_description,product_quantity*,product_unit*,product_lot_number,seller_unit,buyer_unit,transport_mode,linked_forest*,shipment_description,buyer_order_number\n';
      data += 'Sample IN-123, 20-02-2019 ,Sample Buyer,Sample Product, Sample Product description, 100 , pieces,Sample lot,Sample seller unit,Sample buyer unit,Sample transport Mode,newforest, Sample shipment description, Sample PO ,'
     }
     
     else {
       data = 'invoice_number*,invoice_date*,buyer_name*,product*,product_description,product_quantity*,product_unit*,product_lot_number,blend_percentage,seller_unit,buyer_unit,transport_mode,shipment_description,buyer_order_number,received_invoice_number*,supplier_name*,consumed_product*,consumed_quantity,consumed_unit\n';
       data += 'Sample IN-123, 20-02-2019 ,Sample Buyer,Sample Product, Sample Product description, 100 , pieces,Sample lot, , Sample seller unit,Sample buyer unit, Sample transport Mode, Sample shipment description, Sample PO ,'
     }
    } else if (this.props.owner === 'Order') {
      // Creating Order Template based on role
      if (this.props.role === 'Garment Manufacturer') {
        data = 'purchase_order*,order_date*,order_expected_date,supplier_company_name*,order_description,product*,product_description,quantity*,unit*,mpg*,glm*,blend_percentage,link_purchase_order\n';
        data += 'Sample PO-100, 20-02-2019 , 22-02-2019 , Sample Supplier ,Sample description ,Sample Product, Sample Product description, 100 , pieces, , , , ,'
      } else if (this.props.role === 'Spinner') {
        data = 'purchase_order*,order_date*,order_expected_date,supplier_company_name*,order_description,product*,product_description,quantity*,unit*,link_purchase_order\n';
        data += 'Sample PO-100, 20-02-2019 , 22-02-2019 , Sample Supplier ,Sample description ,Sample Product, Sample Product description, 100 , pieces, ,'
      }else {
        data = 'purchase_order*,order_date*,order_expected_date,supplier_company_name*,order_description,product*,product_description,quantity*,unit*,blend_percentage,link_purchase_order\n';
        data += 'Sample PO-100, 20-02-2019 , 22-02-2019 , Sample Supplier ,Sample description ,Sample Product, Sample Product description,100 ,pieces, , ,'
      }
    } else if(this.props.owner === 'Order_Catalogue') {
      if (key === 'FIRST') {
        // data = 'company_name\n';
        // if (!_.isEmpty(this.props.compniesList)) {
        //   this.props.compniesList.map((item) => {
        //     data += `${item.company_name}`
        //     data += '\n';
        //   });
        // }
        orderCompany();
      } else {
        // data = 'product_name\n';
        // if (!_.isEmpty(this.props.productsList)) {
        //   this.props.productsList.map((item) => {
        //     data += `${item.product_name}`
        //     data += '\n';
        //   });
        // }
        orderProd();
      }
    } 
    else if(this.props.owner === 'Shipment_Catalogue') {
      if (key === 'FIRST') {
        // data = 'company_name\n';
        // if (!_.isEmpty(this.props.compniesList)) {
        //   this.props.compniesList.map((item) => {
        //     data += `${item.company_name}`
        //     data += '\n';
        //   });
        // }
        shipCompany();
      } else {
        // data = 'product_name\n';
        // if (!_.isEmpty(this.props.productsList)) {
        //   this.props.productsList.map((item) => {
        //     data += `${item.product_name}`
        //     data += '\n';
        //   });
        // }
          shipProd();
      }
    }
     else if (this.props.owner === 'Stocks') {
      if (this.props.role === 'Spinner') {
      data = 'lot_number,product_name*,product_uom*,product_qty*,supplier_company_name*\n';
    }else if (this.props.role === 'Birla Cellulose') {
      data = 'lot_number,product_name,product_uom,product_qty,supplier_company_name*\n';
    }else if (this.props.role === 'Pulp') {
      data = 'lot_number,product_name*,product_uom*,product_qty*,product_source\n';
    }
    else {

       data ='lot_number,product_name*,product_uom*,product_qty*,blend_percentage,supplier_company_name*\n';       
    }
    }else if (this.props.owner === 'ProcessLoss') {
      data = 'seller_company_name,product_name,product_qty,product_uom,consume_product_name,consume_product_Qty,consume_product_uom,process_loss\n';
      if (!_.isEmpty(this.props.data)) {
        this.props.data.map((item) => {
          data += `${item.seller_company_name},"`
          data += `${item.product_name}",`
          data += `${item.product_qty},`
          data += `${item.product_uom},"`
          data += `${item.consume_product_name}",`
          data += `${item.consume_product_Qty},`
          data += `${item.consume_product_uom},`
          data += `${item.process_loss},`
          data += '\n';
        });
      }
    }

    // Download the file with prepared data
    if(this.props.owner !== 'Shipment_Catalogue' && this.props.owner !== 'Order_Catalogue'){
      FileDownload(data, fileName);
    }
    
  }

  render() {
    const { handleSubmit, onChange, owner,currentLanguage, downloadShipmentProduct, downloadShipmentCompany, downloadOrderProduct, downloadOrderCompany } = this.props;
    strings.setLanguage(currentLanguage);
    const {chooseFile, uploaded, download} = strings.csvModal;

    return(
      <div className="model_popup">
        <div>
          <CsvModalHeader
            handleCsvModal={this.props.handleCsvModal}
            getText={this.props.getText}
            activeCsvModal={this.props.activeCsvModal}
            currentLanguage = {this.props.currentLanguage}
            viewToBeUpdated = {this.props.viewToBeUpdated}
          />
        </div>
        <div>
          {(this.props.activeCsvModal === 'upload') ?
            <div>
              <Button className="choose-file" onClick={this.openFileExplorer} >{chooseFile}  +</Button>
              {(this.state.selectedFile !== null && (this.props.isUploaded)) ?
                <div>
                  <div className="clearfix"></div>
                  <div className="file-name">
                    <p className="file-name1">{this.state.selectedFile.name}</p>
                    <span className="cancel-btn"> X </span>
                    <span className="cancel-file">{uploaded}</span>
                  </div>
                </div>
                :
                <div>
                  <div className="clearfix"></div>
                  <div className={(!_.isEmpty(this.props.msg)) ? "file-name" : ""}>
                    <p className="file-name1">{this.props.msg}</p>
                  </div>
                </div>
              }
            </div>
            :
            <div>
              <div className="mt-35">
                <div className="file-name">
                  <p className="file-name1">{this.state.strip1}</p>
                  <span className="cancel-file" onClick={() => this.downloadFile('csv', 'FIRST', downloadShipmentProduct, downloadShipmentCompany, downloadOrderCompany, downloadOrderProduct)}>{download}</span>
                </div>
                {(owner === 'Order_Catalogue' || owner === 'Shipment_Catalogue') &&
                  <div className="file-name">
                    <p className="file-name1">{this.state.strip2}</p>
                    <span className="cancel-file" onClick={() => this.downloadFile('csv', 'SECOND', downloadShipmentProduct, downloadShipmentCompany, downloadOrderCompany, downloadOrderProduct)}>{download}</span>
                  </div>
                }
              </div>
            </div>
          }
          <input
            type="file"
            name='upfile'
            ref={"fileUploader"}
            onChange={this.onFileSelected}
            style={{display: "none"}}
          />
        </div>
      </div>
    );
  }
}

export default CsvModal;