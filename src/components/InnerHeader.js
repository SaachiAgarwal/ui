import React from 'react';
import PropTypes from 'prop-types';
import { Link, browserHistory } from 'react-router';
import { Button } from 'reactstrap';

import './InnerHeader.scss';

const badgeIconStyles = {
  top: -5,
  width: 20,
  height: 20,
  fontSize: 10,
  backgroundColor: '#cf152d'
};

const InnerHeader = (props) => {
  const {
    to = '/',
  } = props;
  const secondryTheme = true;
  return (
    <div className='col-sm-12 col-xl-12 inner-pg-header inner-header-fixed'>
      <div className='row'>
        <a className='col-sm-3 col-3 text-center pl-0 pr-4' onClick={() => props.goTo()}>
        <svg fill="black" height="36" viewBox="0 0 24 24" width="36" xmlns="http://www.w3.org/2000/svg">
            <path d="M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z"/>
            <path d="M0-.5h24v24H0z" fill="none"/>
        </svg>
        </a>
      </div>
    </div>
  );
};

export default InnerHeader;
