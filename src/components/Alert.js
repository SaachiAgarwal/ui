import React from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Button, Col } from "reactstrap";

import {Images} from 'config/Config';

class Alert extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const actions = [
      <div className="text-center row">
        <Col>
          <Button
            onClick={() => this.props.handleAlertBox()}
            className="alert-btn btn col-3"
          >
            OK
          </Button>
        </Col>
      </div>
    ];

    return (
      <div>
        <MuiThemeProvider>
          <Dialog
            actions={actions}
            modal={true}
            open={this.props.showAlert}
            onRequestClose={this.props.handleAlertBox}
            className='text-center alert-box'
          >
            <p className='pt-2 mb-0'>{this.props.message}</p>
            <img src={(this.props.status === false) ? Images.errorIcon : Images.successIcon} alt="" className="menu-icon"/>
          </Dialog>
        </MuiThemeProvider>
      </div>
    );
  }
}

Alert.propTypes = {
};

export default Alert;
