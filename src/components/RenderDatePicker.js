import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import { FormGroup, Input, Label } from 'reactstrap';
// import DatePicker from 'react-datepicker';
import moment from 'moment';
// //import 'react-datepicker/dist/react-datepicker.css';
import DatePicker from 'react-bootstrap-date-picker'
import '../styles/form.scss';

const RenderDatePicker = ({ input, label, defaultValue, meta: { touched, error } }) => {
  return (
    <div className={`form-group`}>
      <div>
        <DatePicker {...input}
          dateFormat="DD/MM/YYYY"
          />
  
        <div className="help-block" />
        {touched && (error && <span className="error-danger">
          <i className="fa fa-exclamation-circle">{error}</i></span>)}
      </div>
    </div>
  );
}

RenderDatePicker.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
}

export default RenderDatePicker;