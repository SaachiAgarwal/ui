import React from 'react';
import PropTypes from 'prop-types';

import {Images} from 'config/Config';
import {getLocalStorage} from 'components/Helper';

export const Tab = (props) => {
  let tabArr = [];
  if (props.pages < props.displayRange) {
    for (var i = 0; i < props.pages; i++) {
      tabArr.push(<li><div>{i+1}</div></li>)
    }
  } else {
    for (var i = 0; i < props.pages; i++) {
      if (i < props.displayRange) {
        tabArr.push(<li><div>{i+1}</div></li>)
      } else if (i === props.displayRange) {
        tabArr.push(<li><div>{'...'}</div></li>)
      } else if (i === props.displayRange +1) {
        tabArr.push(<li><div>{props.pages}</div></li>)
      }
    }
  }
  return (
    <div>
      <li><div><img src={props.prev} alt="" className="img-responsive"/></div></li>
      {tabArr}
      <li><div><img src={props.next} alt="" className="img-responsive"/></div></li>
    </div>
  );
};

class Pagination extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPage: 1,
    }
  }

  render() {
    const {next, prev, totalRecordCount, pages, displayRange} = this.props;
    return (
      <div className="pagination-bg">
        <div className="col col-md-8 col-lg-8">
          <ul className="pagination">
            <Tab
              prev={prev}
              next={next}
              pages={pages}
              totalRecordCount={totalRecordCount}
              displayRange={displayRange}
            />
          </ul>
        </div>
        <div className="col col-md-3 col-lg-3 col-md-offset-1 go-to-page"> 
          <label>Go to page</label>
          <input type="text" className="page-number"/>  
          <div className="go">Go <img src={Images.paginationNext} className="img-responsive" alt=""/></div>
        </div>
      </div>
    );
  }
}

export default Pagination;