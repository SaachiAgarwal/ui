/** renderOnly
 * description This component manages how labels and password will come on form controls
 *
 */
import React, { Component, PropTypes } from 'react';
import '../styles/form.scss';
import FontAwesome from 'react-fontawesome';

const renderOnly = ({ input, label, type, meta: { touched, error, invalid } }) => (
  <div className={`form-group ${touched && invalid ? 'has-error' : ''}`}>
    <label  className="control-label">{label}</label>
    <div>
      <input {...input} 
        className="form-control" 
        type={type}
        autoComplete="off"
        autoCorrect="off"
        spellCheck="off"
        readOnly
        style={{"backgroundColor": "#ffff"}}
      />

      <div className="help-block">
        {touched && (error && <span className="error-danger">
        <i className= "fa fa-exclamation-circle">{error}</i></span>) }
      </div>
    </div>
  </div>
)

export default renderOnly;