const normalizePhone = (value) => {
  if (!value) {
    return value
  }

  const onlyNums = value.replace(/[^\d]/g, '')
  return parseFloat(onlyNums);
}

export default normalizePhone;