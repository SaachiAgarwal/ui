import React from 'react';
import _ from 'lodash';
import { monthArray } from 'config/Config';
import moment from 'moment';
/*
*    Global method (saveLocalStorage, getLocalStorage & removeLocalStorage, getAllArchive). use these *    helper method to store, get and remove, getAllArchive the value from localstorage.
*/

/** func saveLocalStorage
 *
 *  @description  This method will save the values in machine storage 
 */
export const saveLocalStorage = (key, values) => {
  var data = [];
  data.push(values);
  localStorage.setItem(key, JSON.stringify(data));
};

/** func getLocalStorage
 *
 *  @description  This method will get the values from machine`s storage
 *
 *  @param  (key: string)
 *
 *  @return  Array
 *    Return value based on kay if found otherwise return empty array
 */
export const getLocalStorage = (key) => {
  var data = JSON.parse(localStorage.getItem(key));
  if (typeof data !== 'undefined' && data !== null && typeof data[0] !== 'undefined') {
    return data[0];
  }
  return [];
};

/** func removeLocalStorage
 *
 *  @description  Delete data from local storage
 *    1. if key is string, remove that key only from localStorage
 *    2. if key is array, remove all array value from localStorage
 *
 *  @param  (key: string)
 */
export const removeLocalStorage = (keys) => {
  if (typeof keys === 'string') {
    localStorage.removeItem(keys);
  } else if (Array.isArray(keys) && keys.length > 0) {
    keys.map(key => {
      localStorage.removeItem(key);
    });
  }
};

/** func getAllArchive
 *
 *  @description  This method will get all the stored values in machine`s storage
 *
 *  @return  Array
 *    contains all the archive stored in storage
 */
export const getAllArchive = () => {
  var archive = [],
    keys = Object.keys(localStorage),
    i = keys.length;

  while (i--) {
    archive.push(
      {
        [keys[i]]: localStorage.getItem(keys[i])
      }
    );
  }
  return archive;
};

/** func saveSessionStorage
 *
 *  @description  This method will save the values in machine`s session storage 
 */
export const saveSessionStorage = (key, values) => {
  var data = [];
  data.push(values);
  sessionStorage.setItem(key, JSON.stringify(data));
};

/** func getSessionStorage
 *
 *  @description  This method will get the values from machine`s session storage
 *
 *  @param  (key: string)
 *
 *  @return  Array
 *    Return value based on kay if found otherwise return empty array
 */
export const getSessionStorage = (key) => {
  var data = JSON.parse(sessionStorage.getItem(key));
  if (typeof data !== 'undefined' && data !== null && typeof data[0] !== 'undefined') {
    return data[0];
  }
  return [];
};

/** func getToken
 *
 * @description This method will return save auth(jwt) token
 *
 * @return String
 *   Return token if found otherwise empty string
 */
export const getToken = () => {
  if (!_.isEmpty(getLocalStorage('loginDetail')))
    return getLocalStorage('loginDetail').token;

  return '';
};

/** func sleep
 *
 * @description use this function if u want to wait for some pprocess
 *
 * @return Promise
 *   Will return a promise after specified time
 */
export const sleep = (time) => {
  return new Promise((resolve) => setTimeout(resolve, time));
};

/** func getDate
 *
 * @description This function will return the date based on given key.
 *
 * @return String
 *   Will return a date in string
 */
export const getDate = (date, key) => {
  let dateArray = date._d.toString().split(' ');
  let month = '';
  monthArray.map((item) => {
    if (item.key === dateArray[1]) {
      month = item.value;
    }
  });
  return dateArray[3] + '/' + month + '/' + dateArray[2];
};

/** func getCurrentDateWithFirstDay
 *
 * @description This function will return current month and year but day will be first day of the month.
 *
 * @return String
 *   Will return a date in string
 */
export const getCurrentDateWithFirstDay = () => {
  // var d = new Date(),
  //   month = '' + (d.getMonth() - 1),
  //   day = '' + 1,
  //   year = d.getFullYear();

  // if (month.length < 2)
  //   month = '0' + month;
  // if (day.length < 2)
  //   day = '0' + day;

  // return [year, month, day].join('-');


  var k = getDate(moment(new Date(new Date().getFullYear(), new Date().getMonth() - 2, 1)))
  console.log(k)
  var array = k.split("/")
  // console.log(array)
  // console.log(array[1])
  var d = new Date();
  let month = ''
  let year = ''
  let day = ''
  year = array[0]
  month = array[1]
  day = array[2]
  // month = d.getMonth()-3
  // console.log(month)
  // year = d.getFullYear();
  // day = 1
  // console.log(year)
  // console.log(day)
  return [year, month, day].join('-');
}

/** func getDate
 *
 * @description This function will return the current date.
 *
 * @return String
 *   Will return a date in string
 */
export const getCurrentDate = () => {
  var d = new Date(),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
};

/** func getSupplierCompanyKeyId
 *
 * @description This function will return either company_key_id or company_name based on isEdit.
 *
 * @Param  (object, object, bool)
 *
 * @return String
 */
export const getSupplierCompanyKeyId = (list, orders, isEdit) => {
  let data = '';
  if (!isEdit) {
    list.map((item) => {
      if (item.company_name === orders.company_name) {
        data = item.company_key_id
      }
    });
  } else {
    list.map((item) => {
      if (item.company_key_id === orders.supplier_company_key_id) {
        data = item.company_name
      }
    });
  }
  return data;
}

/** func filterByDate
 *
 * @description This function will filter the date to format 'YYYY/MM/DD'
 *
 * @Param Object 
 *
 * @return Object
 */
export const filterByDate = (response, type = '') => {
  response.data.map((item, index) => {
    if (type === '') {
      response.data[index].order_date = item.order_date.split('T')[0];
    } else if (type === 'shipment') {
      response.data[index].invoice_date = item.invoice_date.split('T')[0];
    }
  });
  return response;
}

/** func dateByExpression
 *
 * @description This function will return the date in format user provide.
 *
 * @Param String, Object 
 *
 * @return Object
 */
export const dateByExpression = (sFormat, date) => {
  if (!(date instanceof Date)) date = new Date();
  var nDay = date.getDay(),
    nDate = date.getDate(),
    nMonth = date.getMonth(),
    nYear = date.getFullYear(),
    nHour = date.getHours(),
    aDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    aMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    aDayCount = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334],
    isLeapYear = () => {
      if ((nYear & 3) !== 0) return false;
      return nYear % 100 !== 0 || nYear % 400 === 0;
    },
    getThursday = () => {
      var target = new Date(date);
      target.setDate(nDate - ((nDay + 6) % 7) + 3);
      return target;
    },
    zeroPad = (nNum, nPad) => {
      return ('' + (Math.pow(10, nPad) + nNum)).slice(1);
    };
  return sFormat.replace(/%[a-z]/gi, function (sMatch) {
    return {
      '%a': aDays[nDay].slice(0, 3),
      '%A': aDays[nDay],
      '%b': aMonths[nMonth],
      '%B': aMonths[nMonth],
      '%c': date.toUTCString(),
      '%C': Math.floor(nYear / 100),
      '%d': zeroPad(nDate, 2),
      '%e': nDate,
      '%F': date.toISOString().slice(0, 10),
      '%G': getThursday().getFullYear(),
      '%g': (',' + getThursday().getFullYear()).slice(2),
      '%H': zeroPad(nHour, 2),
      '%I': zeroPad((nHour + 11) % 12 + 1, 2),
      '%j': zeroPad(aDayCount[nMonth] + nDate + ((nMonth > 1 && isLeapYear()) ? 1 : 0), 3),
      '%k': '' + nHour,
      '%l': (nHour + 11) % 12 + 1,
      '%m': zeroPad(nMonth + 1, 2),
      '%M': zeroPad(date.getMinutes(), 2),
      '%p': (nHour < 12) ? 'AM' : 'PM',
      '%P': (nHour < 12) ? 'AM' : 'PM',
      '%s': Math.round(date.getTime() / 1000),
      '%S': zeroPad(date.getSeconds(), 2),
      '%u': nDay || 7,
      '%V': (function () {
        var target = getThursday(),
          n1stThu = target.valueOf();
        target.setMonth(0, 1);
        var nJan1 = target.getDay();
        if (nJan1 !== 4) target.setMonth(0, 1 + ((4 - nJan1) + 7) % 7);
        return zeroPad(1 + Math.ceil((n1stThu - target) / 604800000), 2);
      })(),
      '%w': '' + nDay,
      '%x': date.toLocaleDateString(),
      '%X': date.toLocaleTimeString(),
      '%y': ('' + nYear).slice(2),
      '%Y': nYear,
      '%z': date.toTimeString().replace(/.+GMT([+-]\d+).+/, '$1'),
      '%Z': date.toTimeString().replace(/.+\((.+?)\)$/, '$1')
    }[sMatch] || sMatch;
  });
}

/** func getCategory
 *
 * @description This function will return the category.
 *
 * @Param String, Object 
 *
 * @return String
 */
export const getCategory = (id, list) => {
  let category = '';
  list.map((item) => {
    if (item.product_category_id === id) category = item.product_category_name;
  });

  return category;
}

/** func getType
 *
 * @description This function will return the type.
 *
 * @Param String, Object 
 *
 * @return String
 */
export const getType = (id, list) => {
  let type = '';
  list.map((item) => {
    if (item.product_type_id === id) type = item.product_type_name;
  });

  return type;
}

/** func getProductId
 *
 * @description This function will return the product id.
 *
 * @Param Object, String 
 *
 * @return String
 */
export const getProductId = (list, values) => {
  let data = '';
  list.map((item) => {
    if (item.product_name === values) data = item.product_id;
  });

  return data;
}

/** func getCompanyTypeId
 *
 * @description This function will return the company type id.
 *
 * @Param Object, String 
 *
 * @return String
 */
export const getCompanyTypeId = (list, value, isReview = false) => {
  let data = '';
  if (isReview) {
    list.map((item) => {
      if (item.company_type_id === value) data = item.company_type_name;
    });
  } else {
    list.map((item) => {
      if (item.company_type_name === value) data = item.company_type_id;
    });
  }

  return data;
}

/** func getBuyerCompanyKeyId
 *
 * @description This function will return the buyer id.
 *
 * @Param Object, String, boolean 
 *
 * @return String
 */
export const getBuyerCompanyKeyId = (list, value, isEdit) => {
  let data = '';
  if (isEdit === false) {
    list.map((item) => {
      if (item.company_name === value) data = item.company_key_id;
    });
  } else if (isEdit === null) {
    list.map((item) => {
      if (item.company_key_id === value) data = item.company_key_id;
    });
  } else {
    list.map((item) => {
      if (item.company_key_id === value) {
        data = item.company_name
      }
    });
  }
  return data;
}

/** func getUnit
 *
 * @description This function will return the unit.
 *
 * @Param Object, String, String 
 *
 * @return String
 */
export const getUnit = (list, id, type) => {
  let unit = '';
  if (!_.isEmpty(list))
    list.map((item) => {
      if (item.company_bu_id === id) unit = item.company_bu_name;
    });

  return unit;
}

// export const filterDataByCompanyType = (blockchainData, company_type_name) => {
//   let data = [];
//   blockchainData.map((chainItem, index) => {
//     if (chainItem.seller_company_type_name === company_type_name) {
//       data.push(chainItem);
//     }
//   });

//   return data;
// }

export const filterDataByCompanyType = (blockchainData, seller_company_name) => {
  let data = [];
  blockchainData.map((chainItem, index) => {
    if (chainItem.seller_company_name === seller_company_name) {
      data.push(chainItem);
    }
  });

  return data;
}
export const filterIpDataByCompanyType = (blockchainData, company_type_name, company_name) => {
  let data = [];
  blockchainData.map((chainItem, index) => {
    if (chainItem.seller_company_name === company_name) {
      data.push(chainItem);
    }
  });

  return data;
}
export const filterDataByCompanyTypePdf = (blockchainData, company_type_name) => {
  let data = [];
  blockchainData.map((chainItem, index) => {
    if (chainItem.seller_company_type_name === company_type_name) {
      data.push(chainItem);
    }
  });

  return data;
}

// export const OrderAuditFilterDataByCompanyType = (blockchainData, company_type_name) => {
//   let data = [];
//   blockchainData.map((chainItem, index) => {
//     if (chainItem.order_company_type_name === company_type_name) {
//       data.push(chainItem);
//     }
//   });

//   return data;
// }

export const isBlockchainCompleted = (blockchainData) => {
  var checkForNode = 1
  let data = blockchainData;
  let nextNode = { seller_company_type_name: "Brand" };
  // let length = blockchainData.length
  let length = 0
  blockchainData.map((item, index) => {
    if((!_.isEmpty(item))){
      length = length + 1
    }
  })
  // console.log(length);
  blockchainData.map((item, index) => {
    if (_.isEmpty(item)) data.splice(index, 1);
    console.log(item.seller_company_type_name);
    //if (item.seller_company_type_name === 'Garment Manufacturer' || item.seller_company_type_name === 'Trader') {
      

      //Added checkForNode to remove multiple nextNode push
      if (item.seller_company_type_name === 'Garment Manufacturer' && checkForNode ) {
      console.log("here");
      data.push(nextNode);
      checkForNode = 0
    }

    else if (item.seller_company_type_name === 'Integrated Player' && index === length - 1 && item.buyer_company_type_name === "Brand" && checkForNode) {
      data.push(nextNode);
      checkForNode = 0
    }
  });

  return data;
}

/*export const getBuyerSenderDetail = (blockchainData, sender='', complBlchn=[]) => {
  let data = {
    buyer: '',
    sender: '',
    index: 1
  }

  if (!_.isEmpty(sender)) {
    blockchainData.map((item, index) => {
      if (item.seller_company_type_name === sender) {
        data.sender = item.seller_company_type_name;
        data.buyer = blockchainData[index+1].seller_company_type_name;
        data.index = complBlchn.indexOf(sender)+1;
      }
    });
  } else {
    data.sender = (!_.isEmpty(blockchainData[0])) ? blockchainData[0].seller_company_type_name : '';
    data.buyer = (!_.isEmpty(blockchainData[1])) ? blockchainData[1].seller_company_type_name : '';
  }

  return data;
}*/

export const getChecked = (blockchainData, company_type_name) => {
  let status = false;
  blockchainData.map((chainItem, index) => {
    if (chainItem.seller_company_type_name === company_type_name) {
      status = true;
    }
  });
  return status;
}

export const getText = (type) => {
  if (type === 'upload') {
    return (
      <div>
        <h2 className="heading-model-new">Upload CSV</h2>
        <h3 className="text1">Upload .csv file to upload order</h3>
      </div>
    )
  }
  else if (type === 'uploadCert') {
    return (
      <div>
        <h2 className="heading-model-new">Upload QA Document</h2>
        <h3 className="text1">Upload file to upload QA Document</h3>
      </div>
    )
  }
  else {
    return (
      <div>
        <h2 className="heading-model-new">Download CSV Template</h2>
        <h3 className="text1">Download .csv template file.</h3>
      </div>
    )
  }
}

export const roundOffLoss = (loss) => {
  if (loss !== undefined && !_.isEmpty(loss)) {
    loss.map((item) => {
      item.process_loss = Math.floor(item.process_loss * 100) / 100;
    });
  }
  return loss;
}

export const getPages = (records, pageSize = 10) => {
  //debugger
  let pages = 1;
  if (records < pageSize) {
    pages = 1;
  } else if (records > pageSize) {
    let div = records / pageSize;
    let rem = records % pageSize;
    if (rem > 0) {
      pages = parseInt(div) + 1;
    } else {
      pages = parseInt(div);
    }

  }

  return pages;
}

export const addFullName = (list) => {
  let data = [];
  list.map((item) => {
    item.full_name = (item.first_name + ' ' + item.last_name);
    data.push(item);
  });

  return data;
}

export const getCompanyId = (list, value, isEdit) => {
  let data = '';
  if (!_.isEmpty(list)) {
    if (!isEdit) {
      list.map((item) => {
        if (item.company_name === value) data = item.company_key_id
      });
    } else {
      list.map((item) => {
        if (item.company_key_id === value) data = item.company_name
      });
    }
  }

  return data;
}

export const getUsreInfo = () => {
  let lStorage = getLocalStorage('loginDetail');
  let userDetail = lStorage.userInfo;
  userDetail.full_name = `${userDetail.first_name} ${userDetail.last_name}`;
  if (userDetail.phone_number === undefined) userDetail.phone_number = '0000000000';

  return userDetail;
}

/** func getProductTypeId
 *
 * @description This function will return the type.
 *
 * @Param String, Object 
 *
 * @return String
 */
export const getProductTypeId = (value, list, isEdit = false) => {
  let type = '';
  if (list !== undefined) {
    if (isEdit) {
      list.map((item) => {
        if (item.product_type_id === value) type = item.product_type_name;
      });
    } else {
      list.map((item) => {
        if (item.product_type_name === value) type = item.product_type_id;
      });
    }
  }

  return type;
}

export const getUOM = (list, value, isEdit = false) => {
  let type = '';
  if (list !== undefined) {
    if (isEdit) {
      list.map((item) => {
        if (item.uom_id === value) type = item.uom;
      });
    } else {
      list.map((item) => {
        if (item.uom === value) type = item.uom_id;
      });
    }
  }

  return type;
}

export const getBusinessCompanyId = (list, value, isEdit) => {
  let data = '';
  if (!_.isEmpty(list)) {
    if (isEdit) {
      list.map((item) => {
        if (item.company_id === value) data = item.company_name
      });
    } else {
      list.map((item) => {
        if (item.company_name === value) data = item.company_id
      });
    }
  }

  return data;
}

export const getEntityId = (entities, name) => {
  let id = '';
  if (!_.isEmpty(entities)) {
    entities.map((entity) => {
      if (entity.company_name === name) id = entity.company_key_id;
    });
  }
  return id;
}

export const getForestId = (pulpDrop, company) => {
  let forest_key_id;
  pulpDrop.map((val, index) => {
    if(val.company_name === company){
      forest_key_id = val.company_key_id
      
    }
  })
  return forest_key_id;
}

export const getCompanyIdForPulp = (data) => {
  var companyKeyId = ''
  data.map((val, index) => {
    if(val.seller_company_type_name === "Forest"){
      companyKeyId = val.buyer_company_key_id
    }
  })

  return companyKeyId
}