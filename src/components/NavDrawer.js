import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';

import { Images, Url } from 'config/Config';
import { dateByExpression, getItem, getLocalStorage, removeLocalStorage } from 'components/Helper';

import 'css/style.scss';
//localization 
import LocalizedStrings from 'react-localization';
import data from '../localization/data';

let strings = new LocalizedStrings(
  data
);

/** NavDrawer
 *
 * @description This class is responsible to display a fully functional left navigation drawer.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class NavDrawer extends React.Component {
  constructor(props) {
    super(props);
    this.handleNavItem = this.handleNavItem.bind(this);
    this.handleInnerNavItem = this.handleInnerNavItem.bind(this);

    this.state = {
      date: ''
    }
  }

  async componentWillMount() {
    let lStorage = getLocalStorage('loginDetail');
    this.setState({
      role: lStorage.role
    });
    let date = dateByExpression('%A, %b %e, %Y %l:%M%P');
    await this.setState({ date: date });
  }

  // static defaultProps = {
  //   NavdrawerOptions: [
  //     {
  //       key: 'orders',
  //       value: 'Orders',
  //       class: 'order-menu-icon',
  //       inner: [
  //         {
  //           key: 'inbox',
  //           value: 'Inbox',
  //           class: 'box-menu-icon',
  //           role: ["Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
  //         },
  //         {
  //           key: 'outbox',
  //           value: 'Outbox',
  //           class: 'box-menu-icon',
  //           role: ["Brand", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
  //         },
  //       ],
  //       role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
  //     },
  //     {
  //       key: 'shipments',
  //       value: 'Shipments',
  //       class: 'shipments-menu-icon',
  //       inner: [
  //         {
  //           key: 'inbox',
  //           value: 'Inbox',
  //           class: 'box-menu-icon',
  //           role: ["Brand", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
  //         },
  //         {
  //           key: 'outbox',
  //           value: 'Outbox',
  //           class: 'box-menu-icon',
  //           role: ["Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
  //         },
  //       ],
  //       role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
  //     },
  //     {
  //       key: 'audit',
  //       value: 'Audit Trail',
  //       class: 'audit-trail-menu-icon',
  //       role: ["Brand", "Birla Cellulose"]
  //     },
  //     {
  //       key: 'stock',
  //       value: 'Stock',
  //       class: 'stock-menu-icon',
  //       role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
  //     },
  //     {
  //       key: 'settings',
  //       value: 'Settings',
  //       class: 'settings-menu-icon',
  //       role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
  //     },
  //     {
  //       key: 'company',
  //       value: 'Company',
  //       class: 'company-menu-icon',
  //       role: ["Admin"]
  //     },
  //     {
  //       key: 'business',
  //       value: 'Business Unit',
  //       class: 'business-menu-icon',
  //       role: ["Admin"]
  //     },
  //     {
  //       key: 'user',
  //       value: 'User',
  //       class: 'user-menu-icon',
  //       role: ["Admin"]
  //     },
  //     {
  //       key: 'product',
  //       value: 'Product',
  //       class: 'product-menu-icon',
  //       role: ["Admin"]
  //     },
  //     {
  //       key: 'processloss',
  //       value: 'Process Loss',
  //       class: 'process-menu-icon',
  //       inner: [
  //         {
  //           key: 'actual',
  //           value: 'Actual',
  //           class: 'actual-menu-icon',
  //           role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player", "Admin"]
  //         },
  //         {
  //           key: 'defined',
  //           value: 'Defined',
  //           class: 'define-menu-icon',
  //           role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player", "Admin"]
  //         },
  //       ],
  //       role: ["Admin"]
  //     },
  //     {
  //       key: 'configuration',
  //       value: 'Configure',
  //       class: 'define-menu-icon',
  //       role: ["Brand"]
  //     },
  //     {
  //       key: 'qa',
  //       value: 'Quality Documents',
  //       class: 'define-menu-icon',
  //       role: ["Birla Cellulose"]
  //     },
  //     {
  //       key: 'logout',
  //       value: 'Logout',
  //       class: 'logout-menu-icon',
  //       role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player", "Admin"]
  //     },
  //     {
  //       key: 'scan',
  //       value: 'Scan QR Code',
  //       class: '',
  //       role: ["Admin"]
  //     },
  //     {
  //       key: 'reports',
  //       value: 'Reports',
  //       class: 'define-menu-icon',
  //       inner: [
  //         {
  //           key: 'orders',
  //           value: 'Orders',
  //           class: 'actual-menu-icon',
  //           role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player", "Admin"]
  //         },
  //         {
  //           key: 'shipments',
  //           value: 'Shipments',
  //           class: 'define-menu-icon',
  //           role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player", "Admin"]
  //         },
  //       ],
  //       role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player", "Admin"]
  //     },
  //   ]
  // }

  async handleNavItem(index, value) {
    //setting global state
    await this.props.setGlobalState({ navIndex: index });
    await this.props.setGlobalState({ navClass: value });

    console.log('========',index,value)

    //resetting reports values.
    this.props.setInnerNav(false);
    this.props.setParam('');
    this.props.setSearchString('');
    this.props.setReportFetching(false);

    // Routing rules
    if (index === 0) {
      browserHistory.push(Url.ORDER_PAGE);
      if (this.props.navInnerIndex === 0) {
        this.props.handleInboxOutboxOrders('orders', 'inbox');
      } else if (this.props.navInnerIndex === 1) {
        this.props.handleInboxOutboxOrders('orders', 'outbox');
      }
    } else if (index === 1) {
      browserHistory.push(Url.SHIPMENT_PAGE);
      if (this.props.navInnerIndex === 0) {
        this.props.handleInboxOutboxOrders('shipments', 'inbox');
      } else if (this.props.navInnerIndex === 1) {
        this.props.handleInboxOutboxOrders('shipments', 'outbox');
      }
    } else if (index === 2) {
      await this.props.setGlobalState({ navInnerIndex: 0 });
      browserHistory.push(Url.AUDIT_PAGE);
    } else if (index === 3) {
      await this.props.setGlobalState({ navInnerIndex: 0 });
      browserHistory.push(Url.STOCK_PAGE);
    }else if (index === 4) {
      await this.props.setGlobalState({ navInnerIndex: 0 });
      browserHistory.push(Url.EXPORT_REBATE);
    } else if (index === 5) {
      await this.props.setGlobalState({ navInnerIndex: 0 });
      browserHistory.push(Url.SETTINGS_PAGE);
    } else if (index === 6) {
      await this.props.setGlobalState({ navInnerIndex: 0 });
      browserHistory.push(Url.COMPANY_PAGE);
    } else if (index === 7) {
      this.props.setGlobalState({ navInnerIndex: 0 });
      browserHistory.push(Url.BUSINESS_PAGE);
    } else if (index === 8) {
      this.props.setGlobalState({ navInnerIndex: 0 });
      browserHistory.push(Url.USER_PAGE);
    } else if (index === 9) {
      this.props.setGlobalState({ navInnerIndex: 0 });
      browserHistory.push(Url.PRODUCTS_PAGE);
    } else if (index === 10) {
      browserHistory.push(Url.PROCESS_LOSS_PAGE);
      if (this.props.navInnerIndex === 0) {
        this.props.handleInboxOutboxOrders('processloss', 'actual');
      } else if (this.props.navInnerIndex === 1) {
        this.props.handleInboxOutboxOrders('processloss', 'defined');
      }
    } else if (index === 11) {
      this.props.setGlobalState({ navInnerIndex: 0 });
      browserHistory.push(Url.CONFIGURATION_PAGE);

    } else if (index === 12) {
      this.props.setGlobalState({ navInnerIndex: 0 });
      browserHistory.push(Url.QA_PAGE);
    }
    else if (index === 13) {
      browserHistory.push(Url.REPORTS_PAGE);
      if (this.props.navInnerIndex === 0) {
        await this.props.setGlobalState({ navInnerIndex: 0 });
        this.props.handleInboxOutboxOrders('reports', 'orders');
      } else if (this.props.navInnerIndex === 1) {
        this.props.handleInboxOutboxOrders('reports', 'shipments');
      }
    }
    else if (index === 14) {
      browserHistory.push(Url.ADMIN_REPORTS);
      
    }
    else if(index === 15){
      browserHistory.push(Url.SCANNER_PAGE);
    }
    else if (index === 16) {
      this.props.setGlobalState({ navInnerIndex: 0 });
      removeLocalStorage('loginDetail');
      browserHistory.push(Url.HOME_PAGE);
    }

  }

  async handleInnerNavItem(innerIndex, navClass, index) {
    //console.log("****inner nav item********");
    //console.log(innerIndex, navClass, index);
    if (this.props.navInnerIndex !== innerIndex && this.props.navClass === navClass) {
      await this.props.setGlobalState({ navInnerIndex: innerIndex });
    } else if (this.props.navInnerIndex !== innerIndex && this.props.navClass !== navClass) {
      await this.props.setGlobalState({
        navInnerIndex: innerIndex,
        navClass: navClass
      });
    }

    // Routing Rules
    if (index === 0 && innerIndex === 0) {
      this.props.handleInboxOutboxOrders('orders', 'inbox');
    } else if (index === 0 && innerIndex === 1) {
      this.props.handleInboxOutboxOrders('orders', 'outbox');
    } else if (index === 1 && innerIndex === 0) {
      this.props.handleInboxOutboxOrders(strings.NavdrawerOptions[1].value, strings.NavdrawerOptions[1].inner[0].value);
    } else if (index === 1 && innerIndex === 1) {
      this.props.handleInboxOutboxOrders(strings.NavdrawerOptions[1].value, 'outbox');
    } else if (index === 9 && innerIndex === 0) {
      this.props.handleInboxOutboxOrders('processloss', 'actual');
    } else if (index === 9 && innerIndex === 1) {
      this.props.handleInboxOutboxOrders('processloss', 'defined');
    }
    else if (index === 12 && innerIndex === 0) {

      this.props.handleInboxOutboxOrders('reports', 'orders');
    }
    else if (index === 12 && innerIndex === 1) {
      this.props.handleInboxOutboxOrders('reports', 'shipment');
    }
    else if (index === 12 && innerIndex === 2) {
      this.props.handleInboxOutboxOrders('reports', 'stock');
    }


  }

  render() {
    strings.setLanguage(this.props.currentLanguage);
    return (
      <div className="col-md-2 col-lg-2 sidebar">
        <div className="menu-header"> <img src={Images.menuIcon} alt="" className="menu-icon" />
          {/*<div className="notification"><span></span></div>*/}
          <div className="float-right">
            <img src={Images.grasimIcon} />
            <img src={Images.birlaIcon} />
          </div>
        </div>
        <ul className="nav nav-sidebar">
          {console.log('options',strings.NavdrawerOptions)}
          {strings.NavdrawerOptions.map((item, index) => {
            return (
              <div key={index}>
                {(item.role.indexOf(this.props.role) !== -1) &&
                  <li onClick={() => this.handleNavItem(index, item.value)}>
                    {/* {console.log('checking nav items',index, item.value)} */}
                    <div className={(this.props.navIndex === index) ? "current count-box" : ""}>
                      <span className={item.class}></span>{item.value}
                    </div>
                    {item.value === this.props.navClass && item.inner !== undefined && item.inner.map((innerItem, innerIndex) => {
                      return (
                        <section key={innerIndex}>
                          {/* {
                            console.log(item.value, this.props.navClass)
                          }
                          {
                            //console.log(this.props.navInnerIndex, innerIndex)
                          }
                          {
                            console.log(this.props.role)
                          } */}

                          {/* {
                            console.log(innerItem.role.indexOf(this.props.role) !== -1)
                          } */}
                          {(innerItem.role.indexOf(this.props.role) !== -1) &&
                            <ul onClick={() => this.handleInnerNavItem(innerIndex, item.value, index)}>
                              <li><div className={(this.props.navInnerIndex === innerIndex && this.props.navClass === item.value) ? "active" : ""}>
                                {/* {
                                  console.log(this.props.navInnerIndex === innerIndex && this.props.navClass === item.value)
                                } */}
                                {/* 
                                {
                                  //console.log(innerIndex, `innerIndex`)
                                }
                                {
                                  console.log(this.props.navInnerIndex === innerIndex, this.props.navClass === item.value)
                                } */}
                                <span className={innerItem.class}></span>{innerItem.value}
                                {(innerItem.key === 'inbox') && <span className="count-box">{this.props.unreadItems}</span>}
                              </div></li>
                            </ul>
                          }
                        </section>
                      );
                    })}
                  </li>
                }
              </div>
            );
          })}
        </ul>
        <hr />
        <p className='navdrawerfooter'>{this.state.date}</p>
      </div>
    );
  }
}

NavDrawer.propTypes = {
  handleInboxOutboxOrders: PropTypes.func,
  NavdrawerOptions: PropTypes.array.isRequired,
}

export default NavDrawer;