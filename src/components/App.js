import React from 'react';
import { browserHistory, Router } from 'react-router';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.css';
import {getLocalStorage} from 'components/Helper';
import {Url} from 'config/Config';
import Alert from 'components/Alert';

let IDLE_TIME_OUT = 900; // Time is in seconds
let REMAINING_TIME = IDLE_TIME_OUT;
let ELAPSED_TIME = 0;
let IS_TIMER_START = false;

const ALERT_STATUS = false;
let MESSAGE = '';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.tId = null;
    this._onMouseMove = this._onMouseMove.bind(this);
    this._handleRouter = this._handleRouter.bind(this);
    this._handleKeyPress = this._handleKeyPress.bind(this);
  }

  static propTypes = {
    store: PropTypes.object.isRequired,
    routes: PropTypes.object.isRequired,
  }

  shouldComponentUpdate () {
    return false
  }

  _handleKeyPress() {
    // On key press just do the same happens in _onMouseMove
    this._onMouseMove();
  }

  async _onMouseMove() {
    // First check whether ELAPSED_TIME is greater than the IDLE_TIMEOUT
    // If yes then logout and stop timer
    const elTime = this.isElapsedTimeGreater();
    if (elTime) {
      browserHistory.push(Url.LOGIN_PAGE); // logout user
      this._stop(); // Stop the timer
      MESSAGE = `you were idle from last ${Math.floor(ELAPSED_TIME/60)} minutes`;
      alert(MESSAGE);
      REMAINING_TIME = IDLE_TIME_OUT; // Reset the REMAINING_TIME to initial state.
      ELAPSED_TIME = 0; // Reset ELAPSED_TIME to its initial state
      IS_TIMER_START = false; // Reset timer status
    } else {
      REMAINING_TIME = IDLE_TIME_OUT; // Reset the REMAINING_TIME to initial state.
      ELAPSED_TIME = 0; // Reset ELAPSED_TIME to its initial state
    }
  }

  isElapsedTimeGreater() {
    if (ELAPSED_TIME > IDLE_TIME_OUT) return true;

    return false;
  }

  _stop() {
    return new Promise((resolve) => {
      // Clear timeInterval
      clearInterval(this.tId);
      // Reset the IS_TIMER_START state
      IS_TIMER_START = false;
      setTimeout(() => {
        resolve(true); // Resolve after 1 sec.
      }, 1000);
    });
  }

  async _handleRouter() {
    if ((window.location.pathname !== Url.HOME_PAGE && window.location.pathname !== Url.LOGIN_PAGE) && !IS_TIMER_START) {
      this.tId = setInterval(() => {
        ELAPSED_TIME = ELAPSED_TIME + 1; //  ELAPSED_TIME will increase every seconds.
        REMAINING_TIME = REMAINING_TIME - 1; // REMAINING_TIME will decrease every seconds.
      }, 1000);// this will start a watch timer
      
      // Set IS_TIMER_START to true
      IS_TIMER_START = true;
    } else if (window.location.pathname === Url.HOME_PAGE || window.location.pathname === Url.LOGIN_PAGE) {
      // It simply means that user is logout
      await this._stop();
    }
  }

  render () {
    return (
      <Provider store={this.props.store}>
        <div style={{ height: '100%' }} onMouseMove={this._onMouseMove} onKeyPress={this._handleKeyPress} ref="eventContainer">
          <Router onUpdate={this._handleRouter} history={browserHistory} children={this.props.routes} />
        </div>
      </Provider>
    )
  }
}

export default App;
