const options = {
  sizePerPage: 10,
  pageSize: 10,
  modalPageSize: 5,
  pageStartIndex: 1,
  currentPage: 1,
  paginationSize: 3,
  prePage: "Prev",
  nextPage: "Next",
  firstPage: 'First',
  lastPage: 'Last',
  clearSearch: true,
  hideSizePerPage: true,
  onRowClick: this.editFormatter
}

export default options;
