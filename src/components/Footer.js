import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';

import Logo from './Logo';
import { Images } from 'config/Config';
import 'css/style.scss';

//localization 
import LocalizedStrings from 'react-localization';
import  data  from 'localization/data';

let strings = new LocalizedStrings(
  data
);



export const GlobalFooter = (props) => {
  strings.setLanguage(props.currentLanguage);
  return (
    <div>
      <div className="col-md-7">
        <p>{strings.copyright}</p>
      </div>
      <div className="col-md-5">
        <img src={Images.grasimLIcon} />
        <img src={Images.birlaLIcon} />
        <img src={Images.livaLIcon} />
      </div>
    </div>
  );
}

export const AuditFooter = (props) => {
  strings.setLanguage(props.currentLanguage);
  return (
    <Row>
      <Col>
      </Col>
      <Col>
      <p>{strings.copyright}</p>
      </Col>
      <Col>
        <p>{props.index}</p>
      </Col>
    </Row>
  );
}

export const PrintFooter = (props) => {
  return (
    <div id="printFooter" style={{ position: "fixed", bottom: "0",marginTop: "30px",paddingTop: "30px", height: "50px" }}>
     
    </div>
  )
}

const Footer = (props) => {
  return (
    <Row>
      <Col>
        {(props.isPrint) ?
          < PrintFooter/>
          : <footer className="footer">
            {(props.isPdfDownload) ?
              <AuditFooter index={props.index} {...props}/>
              : < GlobalFooter isPdfDownload={props.isPdfDownload} {...props}/>
            }
          </footer>}
      </Col>
    </Row>
  );
};

export default Footer;