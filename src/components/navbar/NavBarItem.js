import React from 'react';
import NavBar from './NavBar';
import NavBarLink from './NavBarLink';
import Data from './Data';

export default class NavBarItem extends React.Component {
  generateLink () {
    return <NavBarLink key={this.props.url.toString()} url={this.props.url} text={this.props.text} />;
  }

  generateContent () {
    var content = [this.generateLink()];
    if(this.props.submenu){
      content.push(this.generateSubmenu());
    }
    return content;
  }
  render() {
    var content = this.generateContent();
      return(
        <li key={this.props.id}>
          {content}
        </li>
    );
  }
}
