import React from 'react';
import { browserHistory } from 'react-router';

export default class NavBarLink extends React.Component {
 
 constructor(props){
 	super(props);
 };

 render() {
    return (
      <a onClick={() => browserHistory.push(this.props.url)}>{this.props.text}</a>
    );
  }
}
