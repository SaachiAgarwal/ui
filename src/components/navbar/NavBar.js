import React from 'react';
import NavBarItem from './NavBarItem';
import {id} from'./Data';


export default class NavBar extends React.Component {
  constructor(props) {
    super(props);
  }

  generateItem (item) {
    return <NavBarItem key={item.id} text={item.text} url={item.url} />
  }

  render () {
    var items = this.props.items.map(this.generateItem);
    return (
      <ul key={this.props.items.toString()} className="menu">
       {items}
      </ul>
  );
}
}
