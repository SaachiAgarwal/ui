import React from 'react';

class CheckboxGroup extends React.Component {

  checkboxGroup() {
    let { label, required, options, input, meta, disabled } = this.props;
    if (options !== undefined) {
      return options.map((option, index) => {
        return (
          <div className="checkbox" key={index}>
            <label>
              <input type="checkbox"
                // name={`${input.name}[${index}]`}
                readOnly
                disabled={disabled}
                value={option.company_role_id}
                checked={input.value.indexOf(option.company_role_id) !== -1}
                onChange={(event) => {
                  const newValue = [...input.value];
                  if (event.target.checked) {
                    newValue.push(option.company_role_id);
                  } else {
                    newValue.splice(newValue.indexOf(option.company_role_id), 1);
                  }
                  return input.onChange(newValue);
                }}
              />
              {option.company_rolename}
            </label>
          </div>)
      });
    }
  }
  render() {
    return (
      <div>
        {this.checkboxGroup()}
      </div>
    )
  }
}

export default CheckboxGroup;