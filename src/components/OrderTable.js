import React from 'react';
import PropTypes from 'prop-types';

class OrderTable extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div>
        <div className="allordersplacedby">All orders placed by a brand to a garment manufacturer are in pieces(unit).</div>
        <div className="name">name</div>
			 	<div className="mensknittedtshir">Men's Knitted T-Shirt</div>
			 	<div className="womenu2019ssweater">Women’s Sweater</div>
			 	<div className="menu2019sregularjeans">Men’s Regular Jeans</div>
			 	<div className="supplier">Supplier</div>
			 	<div className="botiq">Botiq</div>
			 	<div className="supplierx">Supplier X</div>
			 	<div className="cutter">Cutter</div>
			 	<div className="quantitycopy2">Quantity</div>
			 	<div className="a20000">20,000</div>
			 	<div className="a12000">12,000</div>
			 	<div className="a50000">50,000</div>
			 	<div className="purchaseorder">Purchase Order</div>
			 	<div className="fm001">FM-001</div>
			 	<div className="fm002">FM-002</div>
			 	<div className="fm003">FM-003</div>
			 	<div className="date">Date</div>
			 	<div className="a03102018">03/10/2018</div>
			 	<div className="a02102018">02/10/2018</div>
			 	<div className="a01102018">01/10/2018</div>
			 	<div className="action">Action</div>
			 	<div className="edit">edit</div>
			 	<div className="delete">delete</div>
			</div>
		);
	}
}

OrderTable.propTypes = {

}

export default OrderTable;