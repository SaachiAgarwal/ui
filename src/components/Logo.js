import React from 'react';
import PropTypes from 'prop-types';

import {Images} from 'config/Config';
import 'css/style.scss';

const Logo = (props) => {
  return (
    <div className="row">
      <div className="col-md-4 col-md-offset-4 logo-image">
    	  <img src={Images.birlaIcon}/>
        <img src={Images.grasimIcon}/>
        <img src={Images.livaIcon}/>
      </div>
    </div>
  );
};

export default Logo;