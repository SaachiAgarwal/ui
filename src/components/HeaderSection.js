import React from 'react';
import PropTypes from 'prop-types';

import { Images, HeaderArray } from 'config/Config';
import { getLocalStorage } from 'components/Helper';

//components
//lang selection component
import SelectLanguage from './SelectLanguage';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../localization/data';

let strings = new LocalizedStrings(
  data
);

class HeaderSection extends React.Component {
  constructor(props) {
    super(props);
    this.checkSearchVisibility = this.checkSearchVisibility.bind(this);

    this.state = {
      data: {},
      haveSearch: false,
    }
  }

  componentWillMount() {
    let loginData = getLocalStorage('loginDetail');

    let data = {
      fullname: loginData.userInfo.first_name + ' ' + loginData.userInfo.last_name,
      //role: loginData.userInfo.company_type_name,
      loginrole: loginData.role,
      companyname: loginData.userInfo.company_name
    }
    this.setState({ data: data });
  }

  shouldComponentUpdate() {
    return true;
  }

  checkSearchVisibility() {
    let searchArr = ['inbox', 'outbox', 'inshipments', 'outshipments', 'stock', 'user', 'company', 'product', 'business'];
    let status = false;
    if (searchArr.indexOf(this.props.active) !== -1) {
      status = true;
    }
    return status;

  }
  render() {
    const { currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);
    return (
      <div>
       {
         !this.props.isPdfDownload &&
          <SelectLanguage
          setCurrentLanguage={this.props.setCurrentLanguage}
          currentLanguage={this.props.currentLanguage}
        />
        
        }

        {(this.props.isPdfDownload) ?
          <div>
            <section className="header">
              <div className="search-wrap">
                <div className="col-md-3 col-lg-3">
                  {strings.HeaderArray.map((item, index) => {
                    return (
                      <h1 key={index}>{item.key === this.props.active ? item.name : ''}</h1>
                    );
                  })}
                </div>
                <div className={(!this.checkSearchVisibility()) ? "col-md-6 col-lg-6 search-with-out" : "col-md-6 col-lg-6 search"}>
                  {(this.checkSearchVisibility()) &&
                    <div className="row">
                      <input
                        type="text"
                        className="input-search"
                        placeholder="Search"
                        value={this.props.searchText}
                        onChange={(e) => this.props.handleSearch(e.target.value)} />
                    </div>
                  }
                </div>
                <div className="float-right" >
                  <img src={Images.grasimIcon} style={{ position: 'absolute', top: '29px', right: '157px' }} />
                  <img src={Images.birlaIcon} style={{ position: 'absolute', top: '32px', right: '124px' }} />
                </div>
                <div className="col-md-12 col-lg-12">
                  {strings.HeaderArray.map((item, index) => {
                    return (
                      <p key={index}>{item.key === this.props.active ? (!_.isEmpty(this.props.orderNumber)) ? item.value + ' ' + this.props.orderNumber : item.value : ''}</p>
                    );
                  })}
                </div>
              </div>
            </section>
          </div>
          :
          <div>
            <section className="header">
              <div className="search-wrap">
                <div className="col-md-5 col-lg-5">
                  {strings.HeaderArray.map((item, index) => {
                    return (
                      <h1 key={index}>{item.key === this.props.active ? item.name : ''}</h1>
                    );
                  })}
                </div>
                {(getLocalStorage('loginDetail').role === "Admin") ? <div className={(!this.checkSearchVisibility()) ? "col-md-6 col-lg-6 search-with-out" : "col-md-6 col-lg-6 search"}>
                  {(this.checkSearchVisibility()) &&
                    <div className="row">
                      <input
                        type="text"
                        className="input-search"
                        placeholder={strings.OrderFilterText.Search}
                        value={this.props.searchText}
                        onChange={(e) => this.props.handleSearch(e.target.value)} />
                    </div>
                  }
                </div> : false}
                <div className="col-md-4 col-lg-4 search-with-out"></div>
                <div className="col-md-2 col-lg-2 col-md-offset-1 col-lg-offset-1 user">

                  <div className="user-wrap">
                    <img src={Images.userIcon} alt="" className="user-img img-responsive" />
                  </div>
                  <div className="user-detail">
                    <p className="name">{this.state.data.fullname}</p>
                    <comment><p className="deg">{this.state.data.role}</p></comment>
                    <p className="deg">{this.state.data.companyname}</p>
                  </div>
                </div>
              </div>
              <div className="col-md-12 col-lg-12">
                {strings.HeaderArray.map((item, index) => {
                  return (
                    <div>

                      <p style={{fontSize: '14px'}} key={index}>{item.key === this.props.active ? (!_.isEmpty(this.props.orderNumber)) ? item.value + ' ' + this.props.orderNumber : item.value : ''}</p>
                    </div>
                  );
                })}
              </div>
            </section>
          </div>}
      </div>
    );
  }
}

export default HeaderSection;