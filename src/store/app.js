import axios from 'axios';
import Config from 'config/Config';

import { getLocalStorage, getToken, getCurrentDate, getCurrentDateWithFirstDay } from 'components/Helper';

export const ALERT_DIALOG = 'ALERT_DIALOG';
export const LOADING_IMAGE = 'LOADING_IMAGE';
export const SETTING_GLOBAL_STATE = 'SETTING_GLOBAL_STATE';
export const SET_NAV_INNER_INDEX = 'SET_NAV_INNER_INDEX';
export const SET_NAV_INDEX = 'SET_NAV_INDEX';
export const SET_NAV_CLASS = 'SET_NAV_CLASS';
export const FETCHING = 'FETCHING';
export const ERROR = 'ERROR';
export const FETCHING_UOM_SUCCESS = 'FETCHING_UOM_SUCCESS';
export const UPLOAD_FILE_SUCCESS = 'UPLOAD_FILE_SUCCESS';
export const FETCHIING_UNREAD_ITEMS_SUCCESS = 'FETCHIING_UNREAD_ITEMS_SUCCESS';
export const SET_CURRENT_LANGUAGE = 'SET_CURRENT_LANGUAGE';
export const QA_DATA = 'QA_DATA';
export const RESET_QA_DATA_DETAIL = 'RESET_QA_DATA_DETAIL';
export const SET_FROM_DATE = "SET_FROM_DATE";
export const SET_TO_DATE = "SET_TO_DATE";
export const SET_REPORT_FETCHING = "SET_REPORT_FETCHING";
export const SET_SEARCH_STRING = "SET_SEARCH_STRING";
export const SET_PARAM = "SET_PARAM"
export const SET_INNER_NAV = "SET_INNER_NAV"
// ------------------------------------
// Actions
// ------------------------------------

export const setInnerNav = (boolean) => {
  return{
    type : SET_INNER_NAV,
    nav : boolean
  }
}
export const setParam = (string) => {
  return {
    type: SET_PARAM,
    param: string
  };
}
export const setSearchString = (string) => {
  return {
    type: SET_SEARCH_STRING,
    searchString: string
  };
}
export const setReportFetching = (bool) => {
  return {
    type: SET_REPORT_FETCHING,
    fetchingFromReport: bool
  };
}

export const setFromDate = (date) => {
  return {
    type: SET_FROM_DATE,
    fromDate: date
  };
}
export const setToDate = (date) => {
  return {
    type: SET_TO_DATE,
    toDate: date
  };
}

export const setCurrentLanguage = (currentLanguage) => {
  return {
    type: SET_CURRENT_LANGUAGE,
    currentLanguage: currentLanguage
  };
}

export function alertPopUp(payload) {
  return {
    type: ALERT_DIALOG,
    alertDialog: payload
  };
}

export function showLoadingImage(status) {
  return {
    type: LOADING_IMAGE,
    loader: status
  }
}

export function settingGlobalState(status) {
  return {
    type: SETTING_GLOBAL_STATE,
    innerIndex: status
  }
}

export function setNavInnerIndex(value) {
  return {
    type: SET_NAV_INNER_INDEX,
    navInnerIndex: value
  }
}

export function setNavIndex(value) {
  return {
    type: SET_NAV_INDEX,
    navIndex: value
  }
}

export function setNavClass(value) {
  return {
    type: SET_NAV_CLASS,
    navClass: value
  }
}

export function fetching(status) {
  return {
    type: FETCHING,
    fetchingGlobal: status
  }
}

export function errorFetching(status) {
  return {
    type: ERROR,
    errorGlobal: status
  }
}

export function fetchingUOMSuccess(payload) {
  return {
    type: FETCHING_UOM_SUCCESS,
    uom: payload
  }
}

export function uploadFileSuccess(status, payload) {
  return {
    type: UPLOAD_FILE_SUCCESS,
    isUploaded: status,
    msg: payload
  }
}

export function fetchingUnreadItemsSuccess(payload) {
  return {
    type: FETCHIING_UNREAD_ITEMS_SUCCESS,
    unreadItems: payload,
  }
}
export function qaDataDetail(payload) {
  return {
    type: QA_DATA,
    qaData: payload
  }
}
export function resetqaDataDetail(payload) {
  return {
    type: RESET_QA_DATA_DETAIL,
    qaData: payload
  }
}
// ------------------------------------
// Actions creators
// ------------------------------------
export const alertDialog = (payload) => {
  return (dispatch) => {
    dispatch(alertPopUp(payload));
  };
};

export const loadingImage = (status) => {
  return (dispatch) => {
    dispatch(showLoadingImage(status));
  }
}

export const setGlobalState = (value) => {
  return (dispatch) => {
    return new Promise((resolve, reject) => {
      Object.keys(value).map((item) => {
        if (item === 'navInnerIndex') {
          dispatch(setNavInnerIndex(value[item]));
        } else if (item === 'navClass') {
          dispatch(setNavClass(value[item]));
        } else if (item === 'navIndex') {
          dispatch(setNavIndex(value[item]));
        } else if (item === 'isUploaded') {
          dispatch(uploadFileSuccess(false, ''));
        }
      });
      resolve(true);
    });
  }
}

export const fetchUOM = (type = '') => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'masters/get_uom';
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: Config.url + endPoint,
        params: { type: type },
        headers: { 'token': getToken() }
      }).then(response => {
        if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(fetchingUOMSuccess(response.data));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        reject(true);
      });
    });
  }
}

export const uploadFile = (file, owner) => {
  return (dispatch) => {
    dispatch(fetching(true));
    const data = new FormData();
    data.append('upfile', file);
    data.append('type', owner);
    // console.log(data)
    let endPoint = 'csv/upload';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: {
          'token': getToken(),
          'Content-Type': 'multipart/form-data'
        }
      }).then(response => {
        console.log('response ', response);
        if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(uploadFileSuccess(true, response.data.data));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(uploadFileSuccess(false, response.data.data));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
      })
    });
  }
}

export const uploadCertFile = (file, owner, shipment_id, appendFile) => {
  return (dispatch) => {
    dispatch(fetching(true));
    // console.log(file);
    // console.log(owner);
    // console.log(shipment_id);
    // console.log(appendFile);
    const data = new FormData();
    data.append('upfile', file);
    data.append('type', owner);
    data.append('shipmentid', shipment_id);
    data.append('document_filename', appendFile)
    // console.log(data)
    //debugger
    let endPoint = 'shipment/upload';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: {
          'token': getToken(),
          'Content-Type': 'multipart/form-data'
        }
      }).then(response => {
        console.log('response ', response);
        if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(uploadFileSuccess(true, response.data.data));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(uploadFileSuccess(false, response.data.data));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
      })
    });
  }
}

export const viewQaDetails = (shipment_id) => {
  return (dispatch) => {
    dispatch(fetching(true));
    // console.log(shipment_id);
    let endPoint = 'shipment/get_documents';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint, 
        data: {
          shipment_id: shipment_id
        },
        headers: { 'token': getToken() }
      }).then(response => {
        // console.log(response);
        dispatch(fetching(false));
        if (response.data.error === 1) {
          dispatch(fetching(false));
          //dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(qaDataDetail(response.data));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        console.log(error)
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

// Getting/Setting unread items in order/shipment
export const handleUnreadItems = (type = 'orders', id) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = '';
    let data = {};
    let method = 'get';
    if (type === 'orders') {
      if (!id) {
        endPoint = 'masters/get_neworders_count';
      } else {
        endPoint = 'masters/update_order_read';
        data.order_id = id;
        method = 'post';
      }
    } else {
      if (!id) {
        endPoint = 'masters/get_newshipment_count';
      } else {
        endPoint = 'masters/update_shipment_read';
        data.shipment_id = id;
        method = 'post';
      }
    }

    return new Promise((resolve, reject) => {
      axios({
        method: method,
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(fetchingUnreadItemsSuccess(response.data.count));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        reject(true);
      });
    });
  }
}

export const actions = {
  alertDialog,
  loadingImage,
  setGlobalState,
  handleUnreadItems
};

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_INNER_NAV]: (state, action) => {
    return {
      ...state,
      nav: action.nav
    };
  },
  [SET_PARAM]: (state, action) => {
    return {
      ...state,
      param: action.param
    };
  },
  [SET_SEARCH_STRING]: (state, action) => {
    return {
      ...state,
      searchString: action.searchString
    };
  },
  [SET_REPORT_FETCHING]: (state, action) => {
    return {
      ...state,
      fetchingFromReport: action.fetchingFromReport
    };
  },
  [SET_FROM_DATE]: (state, action) => {
    return {
      ...state,
      fromDate: action.fromDate
    };
  },
  [SET_TO_DATE]: (state, action) => {
    return {
      ...state,
      toDate: action.toDate
    };
  },
  [SET_CURRENT_LANGUAGE]: (state, action) => {
    return {
      ...state,
      currentLanguage: action.currentLanguage
    };
  },
  [ALERT_DIALOG]: (state, action) => {
    return {
      ...state,
      alertDialog: action.alertDialog
    };
  },
  [LOADING_IMAGE]: (state, action) => {
    return {
      ...state,
      loader: action.loader
    };
  },
  [SETTING_GLOBAL_STATE]: (state, action) => {
    return {
      ...state,
      innerIndex: action.innerIndex
    };
  },
  [SET_NAV_INNER_INDEX]: (state, action) => {
    return {
      ...state,
      navInnerIndex: action.navInnerIndex
    };
  },
  [SET_NAV_INDEX]: (state, action) => {
    return {
      ...state,
      navIndex: action.navIndex
    };
  },
  [SET_NAV_CLASS]: (state, action) => {
    return {
      ...state,
      navClass: action.navClass
    };
  },
  [FETCHING]: (state, action) => {
    return {
      ...state,
      fetchingGlobal: action.fetchingGlobal
    };
  },
  [ERROR]: (state, action) => {
    return {
      ...state,
      errorGlobal: action.errorGlobal
    };
  },
  [FETCHING_UOM_SUCCESS]: (state, action) => {
    return {
      ...state,
      uom: action.uom,
    }
  },
  [UPLOAD_FILE_SUCCESS]: (state, action) => {
    return {
      ...state,
      isUploaded: action.isUploaded,
      msg: action.msg,
    }
  },
  [FETCHIING_UNREAD_ITEMS_SUCCESS]: (state, action) => {
    return {
      ...state,
      unreadItems: action.unreadItems,
    }
  },
  [QA_DATA]: (state, action) => {
    return {
      ...state,
      qaData: action.qaData
    }
  },
  [RESET_QA_DATA_DETAIL]: (state, action) => {
    return {
      ...state,
      qaData: action.qaData
    }
  }
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  alertDialog: {
    status: false,
    title: 'Alert',
    text: '',
  },
  loader: false,
  navIndex: 0,
  navInnerIndex: 0,
  navClass: 'Orders',
  fetchingGlobal: false,
  errorGlobal: false,
  uom: [],
  isUploaded: true,
  msg: '',
  unreadItems: 0,
  currentLanguage: 'en',
  qaData: '',
  fromDate: getCurrentDateWithFirstDay(),
  toDate: getCurrentDate(),
  fetchingFromReport : false,
  searchString : '',
  param : '',
  nav : false
};

export default function appReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
