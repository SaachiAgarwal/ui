        const zh = {
    transportMode: [
        {
            key: 'railway',
            value: 'Rail Transport'
        },
        {
            key: 'road',
            value: 'Road Transport'
        },
        {
            key: 'air',
            value: 'Air Transport'
        },
        {
            key: 'water',
            value: 'Ship Transport'
        },
    ],
    stock : {
        stockSummary : '库存总结',
        productSummary : '产品明智库存摘要',
        supplierSummary : '供应商明智库存摘要',
        stockTabular : '表格'
    },
    linkAutoOrderModal: {
        accept: "数量",
        decline: '拒绝',
        autoLinkDescription: '一旦您接受将发货。'
    },
    vendor : {
        vendor : "供应商",
        orderNumber : '订单号',
        vendorDetail : '供应商详细信息',
        externalVendor : '添加工作',
        productQuantity : '产品数量',
        createProduct : '创建产品'
    },
    modalText : {
        uploadText : '上传.csv文件以上传顺序。',
        downloadText : '下载.csv模板文件。',
        uploadFile : '上传文件以上传质量检查文档。',
        downloadFile  : '下载.csv模板文件。',
        uploadCSVShipment : '上传.csv文件以上传货件。',
        uploadQa : '上传质量检查文档',
        chooseFile: '选择文件+'    
    
    },
    qa: {
        documentName: '文件名',
        isApproved: '被批准',
        remarks: '备注',
        qaUploadDoc: '质量检查上传的文档',
        viewDoc : '查看文件'

    },
    orderStatus: {
        totalOrders: '订单总数',
        newOrders: '新订单',
        fulfilledOrders: '履行订单',
        cancelledOrders: '取消的订单',
        inProgress: '进行中的订单',
        orderStatus: '订单状态',
        receivingStatus: '收货订单状态',
        dueOrders: '到期订单',
        topVendors: '顶级厂商',
        orderTurnAround: '订单明智转身',
        vendorTurnAround: '平均供应商周转率'
    },
    shipmentStatus : {
        shipmentStatus : '出货状态',
        totalShipment : '总出货量',
        shippedShipment : '已发货',
        recievedShipment : '收货',
        openShipment : '公开出货',
        cancelledShipment : '取消出货',
        deliveryStatus : '收货状态',
        topBuyers : '顶级买家',   
        mostShipped : '运送最多的物品',
        download: '下载',
        downloadQR: '下载QR'           
    },
    NavdrawerOptions: [
        {
            key: 'orders',
            value: '订单',
            class: 'order-menu-icon',
            inner: [
                {
                    key: 'inbox',
                    value: '收件箱',
                    class: 'box-menu-icon',
                    role: ["Pulp","Forest","Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
                },
                {
                    key: 'outbox',
                    value: '发件箱',
                    class: 'box-menu-icon',
                    role: ["Forest","Brand", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
                },
            ],
            role: ["Pulp","Forest","Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
        },
        {
            key: 'shipments',
            value: '装运',
            class: 'shipments-menu-icon',
            inner: [
                {
                    key: 'inbox',
                    value: '收件箱',
                    class: 'box-menu-icon',
                    role: ["Brand", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
                },
                {
                    key: 'outbox',
                    value: '发件箱',
                    class: 'box-menu-icon',
                    role: ["Pulp","Forest","Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
                },
            ],
            role: ["Pulp","Forest","Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
        },
        {
            key: 'audit',
            value: '审计跟踪',
            class: 'audit-trail-menu-icon',
            // role: ["Brand", "Birla Cellulose"],
            role: ["Brand"]
        },
        {
            key: 'stock',
            value: '库存',
            class: 'stock-menu-icon',
            role: ["Pulp","Forest","Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
        },
        {
            key:'exportRebate',
            value:'出口退税',
            class:'stock-menu-icon',
            role:['Account']
        },
        {
            key: 'settings',
            value: '设置',
            class: 'settings-menu-icon',
            role: ["Pulp","Forest","Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
        },
        {
            key: 'company',
            value: '公司',
            class: 'company-menu-icon',
            role: ["Admin"]
        },
        {
            key: 'business',
            value: '业务部门',
            class: 'business-menu-icon',
            role: ["Admin"]
        },
        {
            key: 'user',
            value: '用户',
            class: 'user-menu-icon',
            role: ["Admin"]
        },
        {
            key: 'product',
            value: '用户',
            class: 'product-menu-icon',
            role: ["Admin"]
        },
        {
            key: 'processloss',
            value: '生产损耗',
            class: 'process-menu-icon',
            inner: [
                {
                    key: 'actual',
                    value: '实际',
                    class: 'actual-menu-icon',
                    role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player", "Admin"]
                },
                {
                    key: 'defined',
                    value: '定义',
                    class: 'define-menu-icon',
                    role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player", "Admin"]
                },
            ],
            role: ["Admin"]
        },
        {
            key: 'configuration',
            value: '组态',
            class: 'define-menu-icon',
            role: ["Brand"]
        },
        {
            key: 'qa',
            value: '质量文件',
            class: 'define-menu-icon',
            role: ["Birla Cellulose"]
        },
        {
            key: 'reports',
            value: '报告',
            class: 'define-menu-icon',
            inner: [
                {
                    key: 'orders',
                    value: '命令',
                    class: 'actual-menu-icon',
                    role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
                }
                ,
                {
                    key: 'shipments',
                    value: '出货量',
                    class: 'define-menu-icon',
                    role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
                },
                {
                    key: 'stock_report',
                    value: '股票',
                    class: 'define-menu-icon',
                    role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
                  },
    
            ],
            role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
        },
     
        {
            key: 'adminReports',
            value: 'Admin Reports',
            class: 'define-menu-icon',
            role: ["Admin"]
        }
        ,
        {
            key: 'scan',
            value: '扫二维码',
            class: '',
            role: ["Admin"]
        },
        {
            key: 'logout',
            value: '注销',
            class: 'logout-menu-icon',
            role: ["Pulp","Forest","Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player", "Admin"]
        },

    ],
    alert: {
        noOrder: 'No Orders Found.',
        unlinkLinkedOrder: 'First unlink the linked order',
        continousRoles: 'Please select continuous roles',
        selectRoles: 'Select atleast 2 roles',
        companyCreated: 'Company created Successfully'
    },
    selectAll : '全选',
    save: '救',
    prev: '上一页',
    next: '下一个',
    change: '更改',
    submit: '提交',
    copyright: '版权所有©2018 Grasim Industries Limited。版权所有。',
    configure: '配置',
    orderItem: '最高订购商品',
    addLot: '添加批次',
    approve : '批准',
    reject : '拒绝',
    autoGenerate: '您要自动生成订单号吗？',
    reviewdefineloss: {
        designing: '设计',
        processing: '工艺',
        knitting: '针织',
        spinning: '纺纱'
    },
    addBusiness: {
        businessDetail: '业务信息',
        businessUnit: '业务部门',
        bUnitName: '业务部门名称',
        company: '公司',
        locationsDetail: '详细地址',
        country: '国家',
        state: '省',
        city: '市',
        zip: '邮编',
        address: '地址',
        companyName: '公司名称',
        companyDetails: '公司详细信息',
        parent: '亲'
    },
    captalise : {
        back : '背部',
        edit :  '编辑',
        delete : "删除",
        place : "地点",
        ship : "船",
        accept:'接受',
        decline:'下降',
    },
    csvModal: {
        chooseFile: '选择文件',
        uploaded: '上传',
        download: '下载',
        print: '打印',
        close: '关'
    },
    scan: {
        scan: '扫描',
        back: '背部',
        error500: '500内部服务器错误',
        downloadPdf: '下载pdf',
        previewPdf: '预览Pdf',
    },
    linkModal: {
        selectStock: '选择股票批次',
        selectStockDescription: '选择您要为此货件消耗的一个或多个库存批次',
        lotEdit: '批量编辑',
        availableQuantity: '可用数量',
        lotConsume: '食用',
        quantity: '数量',
        update: '更新'
    },
    permissionModal: {
        terms: '条款和条件',
        termsdescription: '装运商品已经过审核.',
        permission: '允许',
        placeShipment: '开始发货'
    },
    receiveModal: {
        accept: '收到货物',
        acceptStatus: '收到全部或部分货物'
    },
    declineModal: {
        declineShipment: '拒绝出货',
        cancelledShipment: '取消出货',
        declineReason: '请提供拒绝收货原因.',
        cancelReasonShipment: '请提供取消此货件的原因',
        declineOrder: '拒绝顺序',
        declineReasonOrder: '请提供拒绝此订单的原因。',
        declineReasonShipment: '请提供拒绝发货的原因。',
        cancellationReason: '取消原因',
        status: '状态',
        rejected: '拒绝',
        date: '日期',
        reason: '原因',
        cancelled: '取消',
        rejectionReason: '拒绝原因',
        declineDocument:'拒絕文件',
        declineDocumentReason:'請提供拒絕此文件的原因。'
    },
    exportModal: {
        exportRebaitClaim:'出口退稅索償',
        billOfLoading:'提貨單',
        uploadDocument:'上傳文件',
        billOfLoadingNo:'裝貨單號',
        invoiceDetail:'發票明細',
        invoiceNumber:'發票號碼',
        status:'狀態',
        submit:'提交',
        new : '新'
    },
    setting: {
        settings: '设置',
        role: '角色',
        contactDetail: '联系方式',
        add: '加',
        create: '创造',
        companyType: '公司类型',
        selectCompany: '选择公司',
        firstName: '名字',
        lastName: '姓',
        phone: '电话',
        email: '电子邮件',
        notify: '通知用户电子邮件',
        password: '密码',
        userDetails: '密码',
        changePassword: '变更密码',
        personalDetail: '个人信息',
        fullName: '全名',
        organization: '组织',
        organizationType: '组织类型',
        phoneNumber: '电话号码',
        emailId: '电子邮箱',
        oldPassword: '旧的密码',
        newPassword: '新的密码',
        confirmPassword: '确认密码',
        linkParent: '链接父组织',
        locationDetail: '位置详情'
    },
    createShipment: {
        addProduct: '添加产品',
        shipmentDetail: '装运详情',
        invoiceNumber: '发票号',
        invoiceDate: '发票日期',
        sellerUnit: '卖方单位',
        buyerUnit: '买方单位',
        transportMode: "运输方式",
        shipmentDescription: '装运描述',
        orderDetails: '订单详细信息'
    },
    shipmentTableText: {
        invoiceNumber: '发票号',
        buyer: '采购商',
        shipmentDate: '发货日期',
        receivedDate: '收到的日期',
        linkedPO: '联系的P.O.#',
        shipmentStatus: '装运状态',
        supplier: '供应商',
        action: '动作',
        seller: '卖方',
        lotDetail: '批次详情',
        mot: 'MoT',
        details: '详情',
        orderCompany: '订购公司',
        supplierCompany: '供应商公司',
        orderNumber: '订单号',
        linkedOrderNumber: '链接订单号',
        invoiceDate: '发票日期',
        sellerSource: '卖方/来源'
    },
    addProduct: {
        productDetail: '产品详情 ',
        geoDetail: '地理位置详细信息',
        productName: '产品名称',
        productType: '产品类别',
        uom: 'UOM',
        product: '商品',
        blend: '混纺 %',
        quantity: '数量',
        unit: '单位',
        productDescription: '产品描述',
        grams: '克/线性仪表',
        meters: '米/服装',
        add: '加',
        sNo: '序列号',
        finishedGoods: "成品批号",
        lot: '批号',
        type: '类型',
        productSource: '产品来源',
        lotNo: '批号',
        category: '类别',
        consumedQuantityText: '单击单元格以添加给定订单的实际消耗量。',
        selectedLots: '精选股票',
        entity: '实体',
        materialProduced: '材料制作',
        quantity: '数量',
        materialConsumed: '消耗的材料',
        loss: '失利'
    },
    createOrder: {
        orderDetail: '订单详情',
        purchaseOrder: '采购订单',
        supplier: '供应商',
        linkBuyer: '链接采购商订单',
        orderDate: '订单日期',
        orderDue: '订单到期日',
        description: '描述',
        productDetail: '商品详情',
        next: '下一个',
        create: '创造',
        supplierType: '供应商类型'
    },
    btnGrpText: {
        shipmentCatalogue: '装运目录',
        downloadShipments: '下载装运信息',
        uploadCSV: '上传CSV文件',
        downloadCSV: '下载CSV表格',
        orderCatalogue: '订单目录',
        downloadOrders: '下载订单',
        disclaimer: '*免责声明：上传的产品仅限Birla纤维素纤维产品',
        disclaimerProcess: '免责声明：过程损失是根据情况计算得出的。'
    },
    incomingFormatterText: {
        view: '查看',
        cancel: '取消',
        reason: '原因',
        receive: '收到',
        decline: '拒绝'
    },
    outgoingFormatterText: {
        view: '查看',
        edit: '编辑',
        delete: '删除',
        link: '链接',
        place: '下单',
        cancel: '取消',
        reason: '原因',
        ship: '船',
        unlink: '取消链接',
        editQuantity: '编辑数量',
        upload: '上传质量检查',
        unlink: '取消连结',
        export:'出口'
    },
    OrderFilterText: {
        Search: '搜索',
        From: '开始日期',
        To: '结束日期',
        select: '选择',
        company: '公司'
    },
    OrdersTableText: {
        orderId: 'Order Id',
        purchaseOrder: '采购订单',
        buyer: '采购商',
        orderDate: '订单日期',
        orderDescription: '订单描述',
        orderStatus: '订单状态',
        Supplier: '供应商',
        Action: '动作',
        Buyer: '采购商',
        orderChecklist: '订单清单',
        linkedOrder: '链接顺序'
    },
    HeaderArray:
        [
            {
                key: 'inbox',
                name: '订单',
                value: '收到订单列表.'
            },
            {
                key: 'outbox',
                name: '订单',
                value: '所下订单列表.'
            },
            {
                key: 'createOrder',
                name: '创建订单',
                value: '添加订单和产品详细信息以创建新订单。'
            },
            {
                key: 'inboxReviewOrder',
                name: '查看订单',
                value: '查看订单.'
            },
            {
                key: 'outboxReviewOrder',
                name: '查看订单',
                value: '在放置之前仔细检查订单。'
            },
            {
                key: 'placedReviewOrder',
                name: '查看订单',
                value: '查看订单.'
            },
            {
                key: 'addProduct',
                name: '添加产品',
                value: '添加产品详情。'
            },
            {
                key: 'inshipments',
                name: '装运',
                value: '收到的装运列表.'
            },
            {
                key: 'outshipments',
                name: '装运',
                value: '您已下达的货件清单。'
            },
            {
                key: 'createShipment',
                name: '创建货件',
                value: '添加货件和产品详细信息以创建新货件。'
            },
            {
                key: 'inboxReviewShipment',
                name: '装运查看',
                value: '装运查看.'
            },
            {
                key: 'outboxReviewShipment',
                name: '装运查看',
                value: '在放置之前请仔细检查货件。'
            },
            {
                key: 'placedReviewShipment',
                name: '装运查看',
                value: '装运查看.'
            },
            {
                key: 'audit',
                name: '审计跟踪',
                value: '跟踪装运信息.'
            },
            {
                key: 'auditTracker',
                name: '审计跟踪',
                value: '采购订单跟踪#'
            },
            {
                key: 'orderAudit',
                name: '审计跟踪',
                value: '采购订单审核跟踪（装运尚未开始）#'
            },
            {
                key: 'stock',
                name: '库存',
                value: '收到的所有货物列表.'
            },
            {
                key: 'scan',
                name: 'QR Scan',
                value: '扫二维码.'
            },
            {
                key: 'actual',
                name: '实际生产损耗',
                value: '装运货物实际生产损耗.'
            },
            {
                key: 'defined',
                name: '定义流程损失',
                value: '定义流程损失。'
            },
            {
                key: 'review',
                name: '查看生产损耗',
                value: '仔细审查流程损失。'
            },
            {
                key: 'settings',
                name: '设置',
                value: '变更密码.'
            },
            {
                key: 'user',
                name: '用户',
                value: '管理用户设置。'
            },
            {
                key: 'adduser',
                name: '添加用户',
                value: '添加用户详细信息.'
            },
            {
                key: 'reviewUser',
                name: '审核用户',
                value: '仔细查看用户详细信息'
            },
            {
                key: 'company',
                name: '公司',
                value: '管理公司设置。'
            },
            {
                key: 'addCompany',
                name: '添加公司账号',
                value: '添加公司详细信息.'
            },
            {
                key: 'reviewCompany',
                name: '评论公司',
                value: '仔细审查公司细节。'
            },
            {
                key: 'product',
                name: '产品',
                value: '管理产品设置.'
            },
            {
                key: 'addProducts',
                name: '添加产品',
                value: '添加产品详情。'
            },
            {
                key: 'reviewProduct',
                name: '查看产品',
                value: '仔细查看产品详情。'
            },
            {
                key: 'business',
                name: '业务部门',
                value: '管理业务单位设置。'
            },
            {
                key: 'addBusiness',
                name: '添加业务部门',
                value: '添加业务部门详细信息.'
            },
            {
                key: 'reviewbusiness',
                name: '审核业务部门',
                value: '仔细审查业务部门的详细信'
            },
            {
                key: 'configuration',
                name: '组态',
                value: '指定公共审核页面的配置'
            },
            {
                key: 'qa',
                name: '质量保证',
                value: '接受或拒绝质量保证文件'
            },
            {
                key: 'orders',
                name: '订购报告',
                value: '您已收到的所有订单的概述'
            },
            {
                key: 'shipments',
                name: '出货报告',
                value: '到目前为止已运送的所有货物的概述'
            },
            {
                key: 'externalJob',
                name: '外部工作',
                value: '添加外部供应商详细信息'
            },
            {
                key: 'stock_report',
                name: '存货报告',
                value: '您的库存概述'
            },
            {
                key: 'adminreports',
                name: '管理員報告',
                value: '下載管理報告'
            },
            {
                key:'exportrebate',
                name:'出口退稅單據',
                value:'接受或拒絕出口退稅單據'
            },
            {
                key:'reviewclaim',
                name:'審核索賠',
                value:''
            }

        ]
}

export default zh;