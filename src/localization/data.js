import en from './en'
import zh from './zh'

const data = {
  en: en,
  zh: zh
}
  // {
  //   transportMode: [
  //     {
  //       key: 'railway',
  //       value: 'Rail Transport X'
  //     },
  //     {
  //       key: 'road',
  //       value: 'Road Transport X'
  //     },
  //     {
  //       key: 'air',
  //       value: 'Air Transport X'
  //     },
  //     {
  //       key: 'water',
  //       value: 'Ship Transport X'
  //     },
  //   ],
  //   linkAutoOrderModal : {
  //     accept : "数量",
  //     decline : '拒绝',
  //     autoLinkDescription : 'Once you accept the shipment will be placed. X'
  //   },
  //   linkModal : {
  //     selectStock : ' Select Stock Lots X',
  //     selectStockDescription : 'Select one or more stock lots that you would like to consume for this shipment X',
  //     lotEdit : 'Lot Edit X',
  //     availableQuantity : 'Available Quantity X',
  //     quantity : '数量',
  //     update : 'Update X'
  //   },
  //   permissionModal : {
  //     terms : 'Terms & Conditions X',
  //     termsdescription : '装运商品已经过审核.',
  //     permission :  'permission X',
  //     placeShipment : '开始发货'
  //   },
  //   receiveModal: {
  //     accept: '收到货物',
  //     acceptStatus: '收到全部或部分货物'
  //   },
  //   declineModal: {
  //     declineShipment: '拒绝收货',
  //     declineReason: '请提供拒绝收货原因.',
  //     cancellationReason: '取消原因',
  //     status: 'Status X',
  //     rejected: 'Rejected X',
  //     date: 'Date X',
  //     reason: '原因'
  //   },
  //   setting: {
  //     settings: '设置',
  //     changePassword: '变更密码',
  //     personalDetail: '个人信息',
  //     fullName: '全名',
  //     organization: '组织',
  //     organizationType: '组织类型',
  //     phoneNumber: '电话号码',
  //     emailId: '电子邮箱',
  //     oldPassword: '旧的密码',
  //     newPassword: '新的密码',
  //     confirmPassword: '确认密码'
  //   },

  //   createShipment: {
  //     shipmentDetail: 'Shipment Details X',
  //     invoiceNumber: '发票号',
  //     invoiceDate: '发票日期',
  //     sellerUnit: '卖方单位',
  //     buyerUnit: '买方单位',
  //     transportMode: "运输方式",
  //     shipmentDescription: '装运描述'
  //   },
  //   shipmentTableText: {
  //     invoiceNumber: '发票号',
  //     buyer: '采购商',
  //     shipmentDate: 'Shipment Date X',
  //     receivedDate: 'Received Date X',
  //     linkedPO: 'Linked P.O.# X',
  //     shipmentStatus: '装运状态',
  //     supplier: '供应商',
  //     action: '动作',
  //     seller: '卖方'
  //   },
  //   addProduct: {
  //     productDetail: '产品详情 ',
  //     product: '商品',
  //     blend: '混纺',
  //     quantity: '数量',
  //     unit: '单位',
  //     productDescription: '产品描述',
  //     grams: 'Grams/Linear Meter * X',
  //     meters: 'Meters/Garments * X',
  //     add: 'Add X',
  //     sNo: '序列号',
  //     finishedGoods: "成品批号",
  //     lot: '批号',
  //     type: '类型',
  //     productSource: '产品来源',
  //     lotNo: '批号',
  //     category: 'Category X'

  //   },
  //   createOrder: {
  //     orderDetail: '订单详情',
  //     purchaseOrder: '采购订单',
  //     supplier: '供应商',
  //     linkBuyer: '链接采购商订单',
  //     orderDate: '订单日期',
  //     orderDue: '订单到期日',
  //     description: '描述',
  //     productDetail: '商品详情',
  //     next: 'Next',
  //     create: 'Create'
  //   },

  //   btnGrpText: {
  //     downloadShipments: '下载装运信息',
  //     uploadCSV: '上传CSV文件',
  //     downloadCSV: '下载CSV表格',
  //     orderCatalogue: '订单目录',
  //     downloadOrders: '下载订单',
  //     disclaimer: '*Disclaimer: Products uploaded are Birla Cellulose fibre based products only XX'
  //   },
  //   incomingFormatterText: {
  //     view: '查看',
  //     cancel: '取消',
  //     reason: '原因',
  //     receive: '收到',
  //     decline: '拒绝'
  //   },
  //   outgoingFormatterText: {
  //     view: '查看',
  //     edit: '编辑',
  //     delete: '删除',
  //     link: '链接',
  //     place: '下单',
  //     cancel: '取消',
  //     reason: '原因',
  //     ship: '船'
  //   },
  //   OrderFilterText: {
  //     Search: '搜索',
  //     From: '开始日期',
  //     To: '结束日期'
  //   },
  //   OrdersTableText: {
  //     orderId: 'Order IdXX',
  //     purchaseOrder: '采购订单',
  //     buyer: '采购商',
  //     orderDate: '订单日期',
  //     orderDescription: '订单描述',
  //     orderStatus: '订单状态',
  //     Supplier: '供应商',
  //     Action: '动作'
  //   },

  //   HeaderArray:
  //     [
  //       {
  //         key: 'inbox',
  //         name: '订单',
  //         value: '收到订单列表.'
  //       },
  //       {
  //         key: 'outbox',
  //         name: '订单',
  //         value: '所下订单列表.'
  //       },
  //       {
  //         key: 'createOrder',
  //         name: 'Create Order',
  //         value: 'Add order and products details to create a new order. XX'
  //       },
  //       {
  //         key: 'inboxReviewOrder',
  //         name: '查看订单',
  //         value: '查看订单.'
  //       },
  //       {
  //         key: 'outboxReviewOrder',
  //         name: '查看订单',
  //         value: 'Review the order carefully before placing it XX.'
  //       },
  //       {
  //         key: 'placedReviewOrder',
  //         name: '查看订单',
  //         value: '查看订单.'
  //       },
  //       {
  //         key: 'addProduct',
  //         name: 'Add Product',
  //         value: 'Add product details XX.'
  //       },
  //       {
  //         key: 'inshipments',
  //         name: 'Shipments',
  //         value: '收到的装运列表.'
  //       },
  //       {
  //         key: 'outshipments',
  //         name: 'Shipments',
  //         value: 'List of shipments you have placed XX.'
  //       },
  //       {
  //         key: 'createShipment',
  //         name: 'Create Shipment',
  //         value: 'Add shipment and products details to create a new shipment XX.'
  //       },
  //       {
  //         key: 'inboxReviewShipment',
  //         name: 'Review Shipment',
  //         value: 'Review the shipment XX.'
  //       },
  //       {
  //         key: 'outboxReviewShipment',
  //         name: 'Review Shipment',
  //         value: 'Review the shipment carefully before placing it XX .'
  //       },
  //       {
  //         key: 'placedReviewShipment',
  //         name: 'Review Shipment',
  //         value: 'Review the shipment XX.'
  //       },
  //       {
  //         key: 'audit',
  //         name: 'Audit Trail',
  //         value: 'Track shipment details XX.'
  //       },
  //       {
  //         key: 'auditTracker',
  //         name: 'Audit Trail',
  //         value: 'Audit trail for purchase order# XX'
  //       },
  //       {
  //         key: 'stock',
  //         name: 'Stock',
  //         value: '收到的装运列表.'
  //       },
  //       {
  //         key: 'scan',
  //         name: 'QR Scan',
  //         value: 'Scan your QR code. XX'
  //       },
  //       {
  //         key: 'actual',
  //         name: 'Actual Process Loss',
  //         value: 'Actual process loss of shipped products. XX'
  //       },
  //       {
  //         key: 'defined',
  //         name: 'Define Process Loss',
  //         value: 'Define process loss XX.'
  //       },
  //       {
  //         key: 'review',
  //         name: 'Review Process Loss',
  //         value: 'Review carefully process loss XX.'
  //       },
  //       {
  //         key: 'settings',
  //         name: 'Settings',
  //         value: 'Change Password XX.'
  //       },
  //       {
  //         key: 'user',
  //         name: 'Users',
  //         value: 'Manage user settings XX.'
  //       },
  //       {
  //         key: 'adduser',
  //         name: 'Add User',
  //         value: 'Add user details XX.'
  //       },
  //       {
  //         key: 'reviewUser',
  //         name: 'Review User',
  //         value: 'Review carefully user details XX.'
  //       },
  //       {
  //         key: 'company',
  //         name: 'Company',
  //         value: 'Manage company settings XX.'
  //       },
  //       {
  //         key: 'addCompany',
  //         name: 'Add Company',
  //         value: 'Add company details XX. '
  //       },
  //       {
  //         key: 'reviewCompany',
  //         name: 'Review Company',
  //         value: 'Review carefully company details XX .'
  //       },
  //       {
  //         key: 'product',
  //         name: 'Product',
  //         value: 'Manage product settings XX.'
  //       },
  //       {
  //         key: 'addProducts',
  //         name: 'Add Product',
  //         value: 'Add product details XX.'
  //       },
  //       {
  //         key: 'reviewProduct',
  //         name: 'Review Product',
  //         value: 'Review carefully product details XX.'
  //       },
  //       {
  //         key: 'business',
  //         name: 'Business Unit',
  //         value: 'Manage business unit settings XX.'
  //       },
  //       {
  //         key: 'addBusiness',
  //         name: 'Add Business Unit',
  //         value: 'Add business unit details XX.'
  //       },
  //       {
  //         key: 'reviewbusiness',
  //         name: 'Review Business Unit',
  //         value: 'Review carefully business unit details XX.'
  //       },
  //     ]


  // }

export default  data ;