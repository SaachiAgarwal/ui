const en = {
    transportMode: [
        {
            key: 'railway',
            value: 'Rail Transport'
        },
        {
            key: 'road',
            value: 'Road Transport'
        },
        {
            key: 'air',
            value: 'Air Transport'
        },
        {
            key: 'water',
            value: 'Ship Transport'
        },
    ],
    stock : {
        stockSummary : 'Stock Summary',
        productSummary : 'Product Wise Stock Summary',
        supplierSummary : 'Supplier Wise Stock Summary',
        stockTabular : 'Tabular'
    },
    vendor : {
        vendor : "Vendor",
        orderNumber : 'Order Number',
        vendorDetail : 'Vendor Details',
        externalVendor : 'Add Job Work',
        productQuantity : 'Product Quantity',
        createProduct : 'Create Product'
    },
    linkAutoOrderModal: {
        accept: "Accept",
        decline: 'Decline',
        autoLinkDescription: 'Once you accept the shipment will be placed.'
    },
    modalText : {
        uploadText : 'Upload .csv file to upload order.',
        downloadText : 'Download .csv template file.',
        uploadFile : 'Upload file to upload QA Document.',
        downloadFile  : 'Download .csv template file.',
        uploadCSVShipment : 'Upload .csv file to upload shipment.',
        uploadQa : 'Upload QA Document',
        chooseFile: 'Choose File+'
    },
    qa : {
        documentName : 'Document name',
        isApproved  : 'Is Approved',
        remarks : 'Remarks',
        qaUploadDoc : 'QA Uploaded Documentation',
        viewDoc : 'View Documentation'
    },
    NavdrawerOptions: [
        {
            key: 'orders',
            value: 'Orders',
            class: 'order-menu-icon',
            inner: [
                {
                    key: 'inbox',
                    value: 'Inbox',
                    class: 'box-menu-icon',
                    role: ["Pulp","Forest","Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
                },
                {
                    key: 'outbox',
                    value: 'Outbox',
                    class: 'box-menu-icon',
                    role: ["Forest","Birla Cellulose","Brand", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
                },
            ],
            role: ["Pulp","Forest","Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
        },
        {
            key: 'shipments',
            value: 'Shipments',
            class: 'shipments-menu-icon',
            inner: [
                {
                    key: 'inbox',
                    value: 'Inbox',
                    class: 'box-menu-icon',
                    role: ["Forest","Birla Cellulose","Brand", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
                },
                {
                    key: 'outbox',
                    value: 'Outbox',
                    class: 'box-menu-icon',
                    role: ["Pulp","Forest","Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
                },
            ],
            role: ["Pulp","Forest","Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
        },
        {
            key: 'audit',
            value: 'Audit Trail',
            class: 'audit-trail-menu-icon',
            // role: ["Brand", "Birla Cellulose"],
            role: ["Brand"]
        },
        {
            key: 'stock',
            value: 'Stock',
            class: 'stock-menu-icon',
            role: ["Pulp","Forest","Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
        },
        {
            key:'exportRebate',
            value:'Export Rebate',
            class:'stock-menu-icon',
            role:['Account']
        },
        {
            key: 'settings',
            value: 'Settings',
            class: 'settings-menu-icon',
            role: ["Pulp","Forest","Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player","Account"]
        },
        {
            key: 'company',
            value: 'Company',
            class: 'company-menu-icon',
            role: ["Admin"]
        },
        {
            key: 'business',
            value: 'Business Unit',
            class: 'business-menu-icon',
            role: ["Admin"]
        },
        {
            key: 'user',
            value: 'User',
            class: 'user-menu-icon',
            role: ["Admin"]
        },
        {
            key: 'product',
            value: 'Product',
            class: 'product-menu-icon',
            role: ["Admin"]
        },
        {
            key: 'processloss',
            value: 'Process Loss',
            class: 'process-menu-icon',
            inner: [
                {
                    key: 'actual',
                    value: 'Actual',
                    class: 'actual-menu-icon',
                    role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player", "Admin"]
                },
                {
                    key: 'defined',
                    value: 'Defined',
                    class: 'define-menu-icon',
                    role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player", "Admin"]
                },
            ],
            role: ["Admin"]
        },
        {
            key: 'configuration',
            value: 'Configure',
            class: 'define-menu-icon',
            role: ["Brand"]
        },
        {
            key: 'qa',
            value: 'Quality Documents',
            class: 'define-menu-icon',
            role: ["Birla Cellulose"]
        },
        {
            key: 'reports',
            value: 'Reports',
            class: 'define-menu-icon',
            inner: [
              {
                key: 'orders',
                value: 'Orders',
                class: 'actual-menu-icon',
                role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
              }
              ,
              {
                key: 'shipments',
                value: 'Shipments',
                class: 'define-menu-icon',
                role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
              },
              {
                key: 'stock_report',
                value: 'Stock',
                class: 'define-menu-icon',
                role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
              },
            ],
            role: ["Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player"]
          }, 
      
        {
            key: 'adminReports',
            value: 'Admin Reports',
            class: 'define-menu-icon',
            role: ["Admin"]
        }
        ,
        {
            key: 'scan',
            value: 'Scan QR Code',
            class: '',
            role: ["Admin"]
        },
        {
            key: 'logout',
            value: 'Logout',
            class: 'logout-menu-icon',
            role: ["Pulp","Forest","Brand", "Birla Cellulose", "Spinner", "Garment Manufacturer", "Finish Fabric Manufacturer", "Grey Fabric Manufacturer", "Trader", "Buying House", "Integrated Player", "Admin","Account"]
        }
    ],
    alert: {
        noOrder: 'No Orders Found.',
        unlinkLinkedOrder: 'First unlink the linked order',
        continousRoles: 'Please select continuous roles',
        selectRoles: 'Select atleast 2 roles',
        companyCreated: 'Company created Successfully'
    },
    orderStatus : {
        totalOrders : 'Total Orders',
        newOrders : 'New Orders',
        fulfilledOrders : 'Fulfilled Orders',
        cancelledOrders : 'Cancelled Orders',
        inProgress : 'In Progress Orders',
        orderStatus : 'Outgoing Order Status',
        receivingStatus : 'Incoming Order Status',
        dueOrders : 'Due Orders',
        topVendors : 'Top Vendors',
        orderTurnAround: 'Order Wise Turn Around',
        vendorTurnAround: 'Average Vendor Turn Around'
    },
    shipmentStatus : {
        shipmentStatus : 'Outgoing Shipment Status',
        totalShipment : 'Total Shipment',
        shippedShipment : 'Shippped Shipment',
        recievedShipment : 'Recieved Shipment',
        openShipment : 'Open Shipment',
        cancelledShipment : 'Cancelled Shipment',
        deliveryStatus : 'Incoming Shipment Status',
        topBuyers : 'Top Buyers',   
        mostShipped : 'Most Shipped Item',
        download: 'Download',
        downloadQR: 'Download QR'           
    },
    approve : 'Approve',
    reject : 'Reject',
    save: 'Save',
    prev: 'Prev',
    next: 'Next',
    change: 'Change',
    submit: 'submit',
    configure: 'Configure',
    addLot: 'Add Lot',
    orderItem : 'Most Order Item',
    autoGenerate: 'Do you want to auto generate an order number?',
    copyright: 'Copyright © 2018 Grasim Industries Limited. All rights reserved.',
    selectAll : 'Select All',
    reviewdefineloss: {
        designing: 'Designing',
        processing: 'Processing',
        knitting: 'Knitting',
        spinning: 'Spinning'
    },
    addBusiness: {
        businessDetail: 'Business Details',
        businessUnit: 'Business Unit',
        bUnitName: 'Business Unit Name',
        company: 'Company',
        locationsDetail: 'Locations Details',
        country: 'Country',
        state: 'State',
        city: 'City',
        zip: 'zip',
        address: 'Address',
        companyName: 'Company Name',
        companyDetails: 'Company Details',
        parent: 'Parent'
    },
    csvModal: {
        chooseFile: 'Choose File',
        uploaded: 'Uploaded',
        download: 'Download',
        print: 'Print',
        close: 'Close'
    },
    captalise : {
        back : 'Back',
        edit :  'Edit',
        delete : "Delete",
        place : "Place",
        ship : "Ship",
        accept:'Accept',
        decline:'Decline',
    },
    scan: {
        scan: 'Scan',
        back: 'Back',
        error500: '500 Internal Server Error',
        downloadPdf: 'Download pdf',
        previewPdf: 'Preview Pdf'
    },
    linkModal: {
        selectStock: ' Select Stock Lots',
        selectStockDescription: 'Select one or more stock lots that you would like to consume for this shipment',
        lotEdit: 'Lot Edit',
        availableQuantity: 'Available Quantity',
        lotConsume: 'To be Consumed',
        quantity: 'Quantity',
        update: 'Update'
    },
    permissionModal: {
        terms: 'Terms & Conditions ',
        termsdescription: 'The product being shipped are VSF approved.',
        permission: 'permission',
        placeShipment: 'Place Shipment'
    },
    receiveModal: {
        accept: 'Accept Shipment',
        acceptStatus: 'Accept complete or partial shipment'
    },
    declineModal: {
        declineShipment: 'Decline Shipment',
        cancelledShipment: 'Cancel Shipment',
        declineOrder: 'Decline Order',
        declineReasonOrder: 'Please provide reason for declining this order.',
        declineReasonShipment: 'Please provide reason for declining this shipment.',
        cancelReasonShipment: 'Please provide reason for cancelling this shipment.',
        cancellationReason: 'Cancellation Reason',
        status: 'Status',
        rejected: 'Rejected',
        date: 'Date',
        reason: 'Reason',
        cancelled: 'Cancelled',
        rejectionReason: 'Rejection Reason',
        declineDocument:'Decline Document',
        declineDocumentReason:'Please provide reason for declining this document.'
    },
    exportModal: {
        exportRebaitClaim:'Export Rebate Claim',
        billOfLoading:'Bill Of Loading',
        uploadDocument:'Upload Document',
        billOfLoadingNo:'Bill Of Loading No.',
        invoiceDetail:'Invoice Detail',
        invoiceNumber:'Invoice Number',
        status:'Status',
        submit:'Submit',
        new : 'New'
        // uploadDocument:'Upload Document'
    },
    setting: {
        settings: 'Settings',
        role: 'Role',
        contactDetail: 'Contact Details',
        add: 'Add',
        create: 'Create',
        companyType: 'Company Type',
        selectCompany: 'Select Company',
        firstName: 'First Name',
        lastName: 'Last Name',
        phone: 'Phone',
        email: 'Email',
        notify: 'Notify User Email',
        password: 'Password',
        userDetails: 'User Details',
        changePassword: 'Change Password',
        personalDetail: 'Personal Details',
        fullName: 'Full Name',
        organization: 'Organization',
        organizationType: 'Organization Type',
        phoneNumber: 'Phone Number',
        emailId: 'Email ID',
        oldPassword: 'Old Password',
        newPassword: 'New Password',
        confirmPassword: 'Confirm Password',
        linkParent: 'Link Parent Organization',
        locationDetail: 'Location Details'
    },
    createShipment: {
        addProduct: 'Add Product',
        shipmentDetail: 'Shipment Details',
        invoiceNumber: 'Invoice Number',
        invoiceDate: 'Invoice Date',
        sellerUnit: 'Seller Unit',
        buyerUnit: 'Buyer Unit',
        transportMode: "Transport Mode",
        shipmentDescription: 'Shipment Description',
        orderDetails: 'Order Details'
    },
    shipmentTableText: {
        invoiceNumber: 'Invoice Number',
        buyer: 'Buyer',
        shipmentDate: 'Shipment Date',
        receivedDate: 'Received Date',
        linkedPO: 'Linked P.O.#',
        shipmentStatus: 'Shipment Status',
        supplier: 'Supplier',
        action: 'Action',
        seller: 'Seller',
        lotDetail: 'Lot Details',
        mot: 'MoT',
        details: 'Details',
        orderCompany: 'Order Company',
        supplierCompany: 'Supplier Company',
        orderNumber: 'Order Number',
        linkedOrderNumber: 'Linked Order Number',
        invoiceDate: 'Invoice Date',
        sellerSource: 'Seller/Source'
    },
    addProduct: {
        productDetail: 'Product Details',
        geoDetail: 'Geolocation Details',
        productName: 'Product Name',
        productType: 'Product Type',
        uom: 'UOM',
        product: 'Product',
        blend: 'Blend % of Birla Cellulose',
        quantity: 'Quantity',
        unit: 'Unit',
        productDescription: 'Product Description',
        grams: 'Grams/Linear Meter',
        meters: 'Meters/Garments',
        add: 'Add',
        sNo: 'Serial No.',
        finishedGoods: "Finished goods lot number",
        lot: 'Lot',
        type: 'Type',
        productSource: 'Product Source',
        lotNo: 'Lot No.',
        category: 'Category',
        consumedQuantityText: 'Click cell to add actual quantity consumed for the given order.',
        selectedLots: 'Selected Stock Lots',
        entity: 'Entity',
        materialProduced: 'Material Produced',
        quantity: 'Quantity',
        materialConsumed: 'Material Consumed',
        loss: 'Loss'
    },
    createOrder: {
        orderDetail: 'Order Details',
        purchaseOrder: 'Purchase Order',
        supplier: 'Supplier',
        linkBuyer: 'Link Buyer Purchase Order',
        orderDate: 'Order Date',
        orderDue: 'Order Due Date',
        description: 'Description',
        productDetail: 'Product Details',
        next: 'Next',
        create: 'Create',
        supplierType: 'Supplier Type'
    },
    btnGrpText: {
        shipmentCatalogue: 'Shipment Catalogue',
        downloadShipments: 'Download Shipments',
        uploadCSV: 'Upload CSV',
        downloadCSV: 'Download CSV Template',
        orderCatalogue: 'Order Catalogue',
        downloadOrders: 'Download Orders',
        disclaimer: '*Disclaimer: Products uploaded are Birla Cellulose fibre based products only',
        disclaimerProcess: 'Disclaimer: The process loss has been calculated based on sitra noms.',
        dloadCSV: 'Download CSV'
    },
    incomingFormatterText: {
        view: 'view',
        cancel: 'cancel',
        reason: 'reason',
        receive: 'receive',
        decline: 'decline'
    },
    outgoingFormatterText: {
        view: 'view',
        edit: 'edit',
        delete: 'delete',
        link: 'link',
        place: 'place',
        cancel: 'cancel',
        reason: 'reason',
        ship: 'ship',
        editQuantity: 'edit quantity',
        upload: 'Upload QA',
        unlink: 'unlink',
        export:'Export'
    },
    OrderFilterText: {
        Search: 'Search',
        From: 'From Date',
        To: 'To date',
        select: 'Select',
        company: 'Company'
    },
    OrdersTableText: {
        orderId: 'Order Id',
        purchaseOrder: 'Purchase Order',
        buyer: 'Buyer',
        orderDate: 'Order Date',
        orderDescription: 'Order Description',
        orderStatus: 'Outgoing Order',
        Supplier: 'Supplier',
        Action: 'Action',
        Buyer: 'Buyer',
        orderChecklist: 'Order Checklist',
        linkedOrder: 'Linked Order'
    },
    HeaderArray:
        [
            {
                key: 'inbox',
                name: 'Orders',
                value: 'List of orders you have received.'
            },
            {
                key: 'outbox',
                name: 'Orders',
                value: 'List of orders you have placed.'
            },
            {
                key: 'createOrder',
                name: 'Create Order',
                value: 'Add order and products details to create a new order.'
            },
            {
                key: 'inboxReviewOrder',
                name: 'Review Order',
                value: 'Review the order.'
            },
            {
                key: 'outboxReviewOrder',
                name: 'Review Order',
                value: 'Review the order carefully before placing it.'
            },
            {
                key: 'placedReviewOrder',
                name: 'Review Order',
                value: 'Review the order.'
            },
            {
                key: 'addProduct',
                name: 'Add Product',
                value: 'Add product details.'
            },
            {
                key: 'inshipments',
                name: 'Shipments',
                value: 'List of shipments you have received.'
            },
            {
                key: 'outshipments',
                name: 'Shipments',
                value: 'List of shipments you have placed.'
            },
            {
                key: 'createShipment',
                name: 'Create Shipment',
                value: 'Add shipment and products details to create a new shipment.'
            },
            {
                key: 'inboxReviewShipment',
                name: 'Review Shipment',
                value: 'Review the shipment.'
            },
            {
                key: 'outboxReviewShipment',
                name: 'Review Shipment',
                value: 'Review the shipment carefully before placing it.'
            },
            {
                key: 'placedReviewShipment',
                name: 'Review Shipment',
                value: 'Review the shipment.'
            },
            {
                key: 'audit',
                name: 'Audit Trail',
                value: 'Track shipment details.'
            },
            {
                key: 'auditTracker',
                name: 'Audit Trail',
                value: 'Audit trail for purchase order#'
            },
            {
                key: 'orderAudit',
                name: 'Audit Trail',
                value: 'Purchase Order Audit Trail (Shipment Not Linked)#'
            },
            {
                key: 'stock',
                name: 'Stock',
                value: 'List of all the products you have received.'
            },
            {
                key: 'scan',
                name: 'QR Scan',
                value: 'Scan your QR code.'
            },
            {
                key: 'actual',
                name: 'Actual Process Loss',
                value: 'Actual process loss of shipped products.'
            },
            {
                key: 'defined',
                name: 'Define Process Loss',
                value: 'Define process loss.'
            },
            {
                key: 'review',
                name: 'Review Process Loss',
                value: 'Review carefully process loss.'
            },
            {
                key: 'settings',
                name: 'Settings',
                value: 'Change Password.'
            },
            {
                key: 'user',
                name: 'Users',
                value: 'Manage user settings.'
            },
            {
                key: 'adduser',
                name: 'Add User',
                value: 'Add user details.'
            },
            {
                key: 'reviewUser',
                name: 'Review User',
                value: 'Review carefully user details.'
            },
            {
                key: 'company',
                name: 'Company',
                value: 'Manage company settings.'
            },
            {
                key: 'addCompany',
                name: 'Add Company',
                value: 'Add company details.'
            },
            {
                key: 'reviewCompany',
                name: 'Review Company',
                value: 'Review carefully company details.'
            },
            {
                key: 'product',
                name: 'Product',
                value: 'Manage product settings.'
            },
            {
                key: 'addProducts',
                name: 'Add Product',
                value: 'Add product details.'
            },
            {
                key: 'reviewProduct',
                name: 'Review Product',
                value: 'Review carefully product details.'
            },
            {
                key: 'business',
                name: 'Business Unit',
                value: 'Manage business unit settings.'
            },
            {
                key: 'addBusiness',
                name: 'Add Business Unit',
                value: 'Add business unit details.'
            },
            {
                key: 'reviewbusiness',
                name: 'Review Business Unit',
                value: 'Review carefully business unit details.'
            },
            {
                key: 'configuration',
                name: 'Configuration',
                value: 'Specify configuration for Public Audit Page'
            },
            {
                key: 'qa',
                name: 'Quality Assurance',
                value: 'Accept or reject quality assurance document'
            },
            {
                key: 'orders',
                name: 'Order Report',
                value: 'Overview of all the orders you have received'
            },
            {
                key: 'shipments',
                name: 'Shipment Report',
                value: 'Overview of all the shipments you have shipped so far'
            },
            {
                key: 'externalJob',
                name: 'External Job',
                value: 'Add external vendor details'
            },
            {
                key: 'stock_report',
                name: 'Stock Report',
                value: 'Overview of your inventory'
            },
            {
                key: 'adminreports',
                name: 'Admin Reports',
                value: 'Download Admin Reports'
            },
            {
                key:'exportrebate',
                name:'Export rebate documents',
                value:'Accept or reject export rebate documents'
            },
            {
                key:'reviewclaim',
                name:'Review Claim',
                value:''
            }
        ]
}

export default en;