import brandOrdersRectangle from '../../public/brand-orders-rectangle.png';
import brandOrdersShape from '../../public/brand-orders-shape.png';
import menuIcon from '../images/menu-icon.png';
import userIcon from '../images/user.png';
import fileUploadIcon from '../images/file-upload-button.png';
import errorIcon from '../images/errorIcon.png';
import successIcon from '../images/successIcon.png';
import prev from '../images/pagination-prev.png';
import next from '../images/pagination-next.png';
import birlaIcon from '../images/birla.png';
import birlaLIcon from '../images/BIcon.png';
import grasimIcon from '../images/grasim.png';
import grasimLIcon from '../images/GIcon.png';
import livaIcon from '../images/liva.png';
import livaLIcon from '../images/LIcon.png';
import infoIcon from '../images/info.png';
import pencilImage from '../images/pencil.png';
import certificateImage from '../images/certi.png';
import arrowGif from '../images/arrow.gif'
import arrowGif1 from '../images/arrow1.gif'
import auditCertImage from '../images/certi1.png'
import footerImage from '../images/footerImage.png'
import headerImage from '../images/header.jpg'
import certHeader from '../images/certificate_header.jpg'
import bcAudit from '../images/bc_audit.jpg'
import greenLight from '../images/green.png'
import redLight from '../images/red.png'
import yellowLight from '../images/yellow.png'
import orangeLight from '../images/orange.png'
import informationImage from '../images/information.png'
import assetIdImage from '../images/qr.png'
import gtimage from '../images/blockchain.jpg';
import gtimage1 from '../images/gtimage1.png'
import livaLogin from '../images/liva_login-compressed.jpg'
import valChain from '../images/valchain.jpg'
import listImage from '../images/list.png'
import gridImage from '../images/grid.png'
import csvImage from '../images/csv_export_2.png'
import excelImage from '../images/excel.png'
import pdfIcon from '../images/pdfIcon.png'

export const APP = {
  maintenance: false
}

export const Config = {
  'url': `${API.url}`,
  'scanurl': `${API.scanurl}`,
  'Accept': 'application/json',
  'contentType': 'application/json',
  paginationOption: {
    page: 1,
    sizePerPage: 2,
    pageStartIndex: 1,
    paginationSize: 2,
    prePage: 'Prev',
    nextPage: 'Next',
    firstPage: 'First',
    lastPage: 'Last',
    paginationPosition: 'bottom',
    hideSizePerPage: true,
    alwaysShowAllBtns: false
  }
};

export const Url = {
  HOME_PAGE: '/',
  LOGIN_PAGE: '/login',
  BRAND_PAGE: '/brand',
  ORDER_PAGE: '/orders',
  SHIPMENT_PAGE: '/shipments',
  STOCK_PAGE: '/stock',
  AUDIT_PAGE: '/audit',
  PROCESS_LOSS_PAGE: '/loss',
  SCANNER_PAGE: '/scan',
  SETTINGS_PAGE: '/settings',
  USER_PAGE: '/user',
  COMPANY_PAGE: '/company',
  PRODUCTS_PAGE: '/products',
  BUSINESS_PAGE: '/business',
  ERRORS_PAGE: '/error',
  MAINTENANCE_PAGE: '/maintenance',
  CONFIGURATION_PAGE: '/configuration',
  QA_PAGE: '/qa',
  REPORTS_PAGE: '/reports',
  ADMIN_REPORTS: '/adminreports',
  EXPORT_REBATE: '/exportRebate'
};

export const NavdrawerOptions = [
  {
    key: 'shipments',
    value: 'Shipments',
    class: 'createassetcopy10'
  },
  {
    key: 'outbox',
    value: 'Outbox',
    class: 'createassetcopy12'
  },
  {
    key: 'orders',
    value: 'Orders',
    class: 'viewassecopy2'
  },
  {
    key: 'audit',
    value: 'Audit',
    class: 'moveassetcopy2'
  },
  {
    key: 'logout',
    value: 'Logout',
    class: 'moveassetcopy3'
  }
];

export const Images = {
  baseImage: brandOrdersRectangle,
  plusIcon: brandOrdersShape,
  userIcon: userIcon,
  menuIcon: menuIcon,
  fileUploadIcon: fileUploadIcon,
  successIcon: successIcon,
  errorIcon: errorIcon,
  paginationNext: next,
  paginationPrev: prev,
  birlaIcon: birlaIcon,
  birlaLIcon: birlaLIcon,
  grasimIcon: grasimIcon,
  grasimLIcon: grasimLIcon,
  livaIcon: livaIcon,
  livaLIcon: livaLIcon,
  infoIcon: infoIcon,
  pencilImage: pencilImage,
  certificateImage: certificateImage,
  arrowGif: arrowGif,
  arrowGif1: arrowGif1,
  auditCertImage: auditCertImage,
  footerImage: footerImage,
  headerImage: headerImage,
  certHeader: certHeader,
  bcAudit: bcAudit,
  greenLight: greenLight,
  redLight: redLight,
  yellowLight: yellowLight,
  orangeLight: orangeLight,
  informationImage: informationImage,
  assetIdImage: assetIdImage,
  gtimage: gtimage,
  gtimage1: gtimage1,
  livaLogin: livaLogin,
  valChain: valChain,
  listImage: listImage,
  gridImage: gridImage,
  csvImage: csvImage,
  excelImage: excelImage,
  pdfIcon:pdfIcon
}

export const monthArray = [
  {
    key: 'Jan',
    value: '01'
  },
  {
    key: 'Feb',
    value: '02'
  },
  {
    key: 'Mar',
    value: '03'
  }, {
    key: 'Apr',
    value: '04'
  }, {
    key: 'May',
    value: '05'
  }, {
    key: 'Jun',
    value: '06'
  }, {
    key: 'Jul',
    value: '07'
  }, {
    key: 'Aug',
    value: '08'
  }, {
    key: 'Sep',
    value: '09'
  }, {
    key: 'Oct',
    value: '10'
  }, {
    key: 'Nov',
    value: '11'
  }, {
    key: 'Dec',
    value: '12'
  }
];

export const HeaderArray = [
  {
    key: 'inbox',
    name: 'Orders',
    value: 'List of orders you have received.'
  },
  {
    key: 'outbox',
    name: 'Orders',
    value: 'List of orders you have placed.'
  },
  {
    key: 'createOrder',
    name: 'Create Order',
    value: 'Add order and products details to create a new order.'
  },
  {
    key: 'inboxReviewOrder',
    name: 'Review Order',
    value: 'Review the order.'
  },
  {
    key: 'outboxReviewOrder',
    name: 'Review Order',
    value: 'Review the order carefully before placing it.'
  },
  {
    key: 'placedReviewOrder',
    name: 'Review Order',
    value: 'Review the order.'
  },
  {
    key: 'addProduct',
    name: 'Add Product',
    value: 'Add product details.'
  },
  {
    key: 'inshipments',
    name: 'Shipments',
    value: 'List of shipments you have received.'
  },
  {
    key: 'outshipments',
    name: 'Shipments',
    value: 'List of shipments you have placed.'
  },
  {
    key: 'createShipment',
    name: 'Create Shipment',
    value: 'Add shipment and products details to create a new shipment.'
  },
  {
    key: 'inboxReviewShipment',
    name: 'Review Shipment',
    value: 'Review the shipment.'
  },
  {
    key: 'outboxReviewShipment',
    name: 'Review Shipment',
    value: 'Review the shipment carefully before placing it.'
  },
  {
    key: 'placedReviewShipment',
    name: 'Review Shipment',
    value: 'Review the shipment.'
  },
  {
    key: 'audit',
    name: 'Audit Trail',
    value: 'Track shipment details.'
  },
  {
    key: 'auditTracker',
    name: 'Audit Trail',
    value: 'Audit trail for purchase order#'
  },
  {
    key: 'stock',
    name: 'Stock',
    value: 'List of all the products you have received.'
  },
  {
    key: 'scan',
    name: 'QR Scan',
    value: 'Scan your QR code.'
  },
  {
    key: 'actual',
    name: 'Actual Process Loss',
    value: 'Actual process loss of shipped products.'
  },
  {
    key: 'defined',
    name: 'Define Process Loss',
    value: 'Define process loss.'
  },
  {
    key: 'review',
    name: 'Review Process Loss',
    value: 'Review carefully process loss.'
  },
  {
    key: 'settings',
    name: 'Settings',
    value: 'Change Password.'
  },
  {
    key: 'user',
    name: 'Users',
    value: 'Manage user settings.'
  },
  {
    key: 'adduser',
    name: 'Add User',
    value: 'Add user details.'
  },
  {
    key: 'reviewUser',
    name: 'Review User',
    value: 'Review carefully user details.'
  },
  {
    key: 'company',
    name: 'Company',
    value: 'Manage company settings.'
  },
  {
    key: 'addCompany',
    name: 'Add Company',
    value: 'Add company details.'
  },
  {
    key: 'reviewCompany',
    name: 'Review Company',
    value: 'Review carefully company details.'
  },
  {
    key: 'product',
    name: 'Product',
    value: 'Manage product settings.'
  },
  {
    key: 'addProducts',
    name: 'Add Product',
    value: 'Add product details.'
  },
  {
    key: 'reviewProduct',
    name: 'Review Product',
    value: 'Review carefully product details.'
  },
  {
    key: 'business',
    name: 'Business Unit',
    value: 'Manage business unit settings.'
  },
  {
    key: 'addBusiness',
    name: 'Add Business Unit',
    value: 'Add business unit details.'
  },
  {
    key: 'reviewbusiness',
    name: 'Review Business Unit',
    value: 'Review carefully business unit details.'
  },
  {
    key: 'configuration',
    name: 'Configuration',
    value: 'Specify configuration for Public Audit Page'
  },
  {
    key: 'qa',
    name: 'Quality Assurance',
    value: 'Accept or reject quality assurance document'
  },
  {
    key: 'adminreports',
    name: 'Admin Reports',
    value: 'Download Admin Reports'
  },
  {
    key:'exportrebate',
    name:'Export Rebate',
    value:'Accept or reject export rebate documents'
  }
];

export default Config;
