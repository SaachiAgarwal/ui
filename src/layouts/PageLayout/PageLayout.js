import React from 'react';
import { IndexLink, Link } from 'react-router';
import PropTypes from 'prop-types';
import {Row} from 'reactstrap';

import '../../components/navbar/Navstyle.scss';
import './PageLayout.scss';
import 'css/style.scss';

export const PageLayout = ({ children }) => (
  <div className='col-12 main-container'>
  <Row>
    <div className='page-layout__viewport'>
      {children}
    </div>
    </Row>
  </div>
);

PageLayout.propTypes = {
  children: PropTypes.node,
};

export default PageLayout;
