/** SettingsContainer
 *
 * @description This is container that manages the states for all settings related stuff.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */

import { connect } from 'react-redux';

import Configuration from '../components/Configuration';
import { updateConfiguration, resetAlertBox, setConfigDetail, getOutgoingOrdersSummary, getIncomingOrdersSummary, getTopOrderSupplier, getTopOrderProducts,getOutgoingShipmentSummary,getIncomingShipmentSummary,getTopShipmentBuyer,getTopShipmentProducts,getStockSummary,getStockProductwise,getStockMonthwise,getStockSupplierwise, getAllOrderTurnAround, getTopVendorOrderTurnaround, fetchingReport } from '../modules/configuration';
import { setInnerNav,setGlobalState, setCurrentLanguage, setFromDate, setToDate, setReportFetching, setSearchString, setParam } from '../../../store/app';

const mapStateToProps = (state) => {
  return ({
    navIndex: state.app.navIndex,
    navInnerIndex: state.app.navInnerIndex,
    navClass: state.app.navClass,
    fetching: state.Configuration.fetching,
    error: state.Configuration.error,
    showAlert: state.Configuration.showAlert,
    alertMessage: state.Configuration.alertMessage,
    status: state.Configuration.status,
    configData: state.Configuration.configData,
    currentLanguage: state.app.currentLanguage,
    outGoingOrderSummary: state.Configuration.outGoingOrderSummary,
    mostItem: state.Configuration.mostItem,
    topVendor: state.Configuration.topVendor,
    fetchingFromReport: state.app.fetchingFromReport,
    incoming: state.Configuration.incoming,
    searchString: state.app.searchString,
    currentLanguage : state.app.currentLanguage,
    summary : state.Configuration.summary,
    monthWise : state.Configuration.monthWise,
    productWise : state.Configuration.productWise,
    supplierWise : state.Configuration.supplierWise,
    orderTurnAround: state.Configuration.orderTurnAround,
    vendorTurnAround: state.Configuration.vendorTurnAround,
    fetching: state.Configuration.fetching
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    setGlobalState: (value) => dispatch(setGlobalState(value)),
    resetAlertBox: (showAlert, message) => dispatch(resetAlertBox(showAlert, message)),
    updateConfiguration: (value) => dispatch(updateConfiguration(value)),
    setConfigDetail: () => dispatch(setConfigDetail()),
    setCurrentLanguage: (lang) => dispatch(setCurrentLanguage(lang)),
    getOutgoingOrdersSummary: (curr, from) => dispatch(getOutgoingOrdersSummary(curr, from)),
    getIncomingOrdersSummary: (curr, from) => dispatch(getIncomingOrdersSummary(curr, from)),
    getTopOrderSupplier: (curr, from) => dispatch(getTopOrderSupplier(curr, from)),
    getTopOrderProducts: (curr, from) => dispatch(getTopOrderProducts(curr, from)),
    setFromDate: (date) => dispatch(setFromDate(date)),
    setToDate: (date) => dispatch(setToDate(date)),
    setReportFetching: (bool) => dispatch(setReportFetching(bool)),
    setSearchString: (string) => dispatch(setSearchString(string)),
    setParam : (string) => dispatch(setParam(string)),
    setCurrentLanguage : (lang) => dispatch(setCurrentLanguage(lang)),
    setInnerNav : (nav) => dispatch(setInnerNav(nav)),
    getOutgoingShipmentSummary : (c, f) => dispatch(getOutgoingShipmentSummary(c, f)),
    getIncomingShipmentSummary : (c, f) => dispatch(getIncomingShipmentSummary(c, f)),
    getTopShipmentBuyer : (c, g) => dispatch(getTopShipmentBuyer(c,g)),
    getTopShipmentProducts : (c, g) => dispatch(getTopShipmentProducts(c, g)),
    getStockSummary : (c, f) => dispatch(getStockSummary(c, f)),
    getStockProductwise : (c, f) => dispatch(getStockProductwise(c, f)),
    getStockMonthwise : (c, g) => dispatch(getStockMonthwise(c,g)),
    getStockSupplierwise : (c, g) => dispatch(getStockSupplierwise(c, g)),
    getAllOrderTurnAround: (c, g) => dispatch(getAllOrderTurnAround(c, g)),
    getTopVendorOrderTurnaround: (c, g) => dispatch(getTopVendorOrderTurnaround(c, g)),
    fetchingReport: (val) => dispatch(fetchingReport(val))
  });
};

export default connect(mapStateToProps, mapDispatchToProps)(Configuration);
