import React from 'react';
import { Row, Col } from 'reactstrap';
import { browserHistory } from 'react-router';
//localization 
import LocalizedStrings from 'react-localization';
import data from 'localization/data';
let strings = new LocalizedStrings(
  data
);


class OutGoingOrdersSummary extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getOutgoingShipmentSummary();
  }

  getTotalOrders = (param) => {
    browserHistory.push('/shipments');
    const { setReportFetching, setParam, setSearchString, setInnerNav } = this.props;
    setReportFetching(true);
    setParam(param)
    setSearchString('');
    setInnerNav(true);

  }

  render() {
    const { total_shipment, shipment_shipped, shipment_received, shipment_open, shipment_cancelled } = this.props.outGoingOrderSummary[0] || {};
    strings.setLanguage(this.props.currentLanguage);
    const {shipmentStatus, totalShipment, shippedShipment,recievedShipment, openShipment,cancelledShipment} = strings.shipmentStatus;

    return (
      <div className='col-12' style={{
        backgroundColor: "white",
        marginTop: "20px"

      }} >
        <Row>
          <Col className="col-12 ">
            <h2>{shipmentStatus}</h2>
          </Col>
        </Row>
        <div></div>

        <Row style={{ paddingTop: '2%' }}>

          <Col className="col" style={{
            borderRightStyle: "solid",
            color: "#00000021",
            borderWidth: '2px',
            cursor: "pointer"
          }}
            onClick={() => {
              this.getTotalOrders('')
            }}>
            <div className="d-flex justify-content-center">
              <p style={{ color: 'rgba(0,0,0,.7)' }}>{totalShipment}</p>
            </div>
            <div className="d-flex justify-content-center">
              <label style={{
                fontSize: "xx-large",
                color: "blue",
                cursor: "pointer"
              }} >{total_shipment}</label>
            </div>
          </Col>


          <Col className="col" style={{
            borderRightStyle: "solid",
            color: "#00000021",
            borderWidth: '2px',
          }}
            onClick={() => {
              this.getTotalOrders('Shipped')
            }}
          >
            <div className="d-flex justify-content-center">
              <p style={{ color: 'rgba(0,0,0,.7)' }}>{shippedShipment}</p>
            </div>
            <div className="d-flex justify-content-center">
              <label style={{
                fontSize: "xx-large",
                color: "blue",
                cursor: "pointer",
              }}>{shipment_shipped}</label>
            </div>
          </Col>


          <Col className="col" style={{
            borderRightStyle: "solid",
            color: "#00000021",
            borderWidth: '2px',

          }}
            onClick={() => {
              this.getTotalOrders('Received')
            }}
          >
            <div className="d-flex justify-content-center">
              <p style={{ color: 'rgba(0,0,0,.7)' }}>{recievedShipment}</p>
            </div>
            <div className="d-flex justify-content-center">
              <label style={{
                fontSize: "xx-large",
                color: "blue",
                cursor: "pointer"
              }}>{shipment_received}</label>
            </div>
          </Col>


          <Col className="col "
            onClick={() => {
              this.getTotalOrders('Open')
            }}
            style={{
              borderRightStyle: "solid",
              color: "#00000021",
              borderWidth: '2px',
            }}
          >
            <div className="d-flex justify-content-center">
              <p style={{ color: 'rgba(0,0,0,.7)' }}>{openShipment}</p>
            </div>
            <div className="d-flex justify-content-center">
              <label style={{
                fontSize: "xx-large",
                color: "red",
                cursor: "pointer"
              }}>{shipment_open}</label>
            </div>
          </Col>


          <Col className="col "
            onClick={() => {
              this.getTotalOrders('Rejected')
            }}
          >
            <div className="d-flex justify-content-center">
              <p style={{ color: 'rgba(0,0,0,.7)' }}>{cancelledShipment}</p>
            </div>
            <div className="d-flex justify-content-center">
              <label style={{
                fontSize: "xx-large",
                color: "red",
                cursor: "pointer"
              }}>{shipment_cancelled}</label>
            </div>
          </Col>


        </Row>
        <Row>
          <Col className="col-12 " style={{
            visibility: "hidden"
          }}>
            <p>Order status</p>
          </Col>
        </Row>

      </div>)
  }
}

export default OutGoingOrdersSummary;