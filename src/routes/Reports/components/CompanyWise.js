import React, { Component } from "react";
import Chart from "react-apexcharts";

class CompanyWise extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getStockSupplierwise();
  }


  render() {
    console.log(this.props.supplierWise)
    const company = this.props.supplierWise || [];
    const categories = [];
    var series = [];
    var test = [];
    var categoryName = [];
    var cat = [];
    var i =0;
    var j = 0;
    var k = 0;
    console.log(company);
    if (company.length > 0) {
      debugger
      this.props.supplierWise.forEach((element, index) => {

          if(element.product_qty !== 0){

          

          
          let data = new Array();
        
          test[i++] = element.product_qty
          categoryName[j++] = element.product_name
          cat[k++] = "Product Name: " + element.product_name + ',' + ' ' +'Company name: ' +  element.supplier_company_name
          // if (categories.indexOf(element.supplier_company_name) == -1) {
          //   categories.push(element.supplier_company_name)
          // }
          categories.push(element.supplier_company_name)
          
          data.push(element.product_qty);
          // series.push({
          //   name: element.product_name,
          //   data: data
          // })
          series= [
            {
              name: "Product Qty",
              data: test
              
            }
            
          ]
          }
       
      })
      
    }
    console.log(test)
    console.log(series)
    console.log(cat)
    const options = {
      chart: {
        id: "basic-bar"
      },
      xaxis: {
        categories: cat,
        labels:{
          show: false,
          rotate: -90,
        }
      },
      plotOptions: {
        bar: {
            distributed: true,
            columnWidth: '70%'
        },
      },
    }
    const tooltip ={
      custom: function({series, seriesIndex, dataPointIndex, w}) {
        return '<div class="arrow_box">' +
          '<span>' + 'hello' + '</span>' +
          '</div>'
      }
    }
    console.log(company);
    console.log(series);
    return (
      <div className="app">
        <div className="row">
          <div className="mixed-chart">
            <Chart
              options={options}
              series={series}
              type="bar"
              width="1000"
              height="300"
            />
          </div>
        </div>
      </div>
    );
  }
}

export default CompanyWise;