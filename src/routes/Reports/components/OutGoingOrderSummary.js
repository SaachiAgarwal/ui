import React from 'react';
import { Row, Col } from 'reactstrap';
import { browserHistory } from 'react-router';
//localization 
import LocalizedStrings from 'react-localization';
import data from 'localization/data';
let strings = new LocalizedStrings(
  data
);


class OutGoingOrdersSummary extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getOutgoingOrdersSummary();
  }

  getTotalOrders = (param) => {
    browserHistory.push('/orders');
    const { setReportFetching, setParam } = this.props;
    setReportFetching(true);
    setParam(param)
    this.props.setSearchString('');
    this.props.setInnerNav(true);
  }

  render() {
    const { total_orders, new_orders, fulfilled_orders, cancelled_orders, inprogress_orders } = this.props.outGoingOrderSummary[0] || {};
    strings.setLanguage(this.props.currentLanguage);
    const { orderStatus, totalOrders, newOrders, fulfilledOrders, cancelledOrders, inProgress } = strings.orderStatus;
    return (
      <div className='col-12' style={{
        backgroundColor: "white",
        marginTop: "20px"

      }} >
        <Row>
          <Col className="col-12 ">
            <h2>{orderStatus}</h2>
          </Col>
        </Row>
        <div></div>

        <Row style={{ paddingTop: '2%' }}>

          <Col className="col" style={{
            borderRightStyle: "solid",
            color: "#00000021",
            borderWidth: '2px',
            cursor: "pointer"
          }}
            onClick={() => {
              this.getTotalOrders('')
            }}>
            <div className="d-flex justify-content-center">
              <p style={{ color: 'rgba(0,0,0,.7)' }}>{totalOrders}</p>
            </div>
            <div className="d-flex justify-content-center">
              <label style={{
                fontSize: "xx-large",
                color: "blue",
                cursor: "pointer"
              }} >{total_orders}</label>
            </div>
          </Col>


          <Col className="col" style={{
            borderRightStyle: "solid",
            color: "#00000021",
            borderWidth: '2px',
          }}
            onClick={() => {
              this.getTotalOrders('Not Linked')
            }}
          >
            <div className="d-flex justify-content-center">
              <p style={{ color: 'rgba(0,0,0,.7)' }}>{newOrders}</p>
            </div>
            <div className="d-flex justify-content-center">
              <label style={{
                fontSize: "xx-large",
                color: "blue",
                cursor: "pointer",
              }}>{new_orders}</label>
            </div>
          </Col>


          <Col className="col" style={{
            borderRightStyle: "solid",
            color: "#00000021",
            borderWidth: '2px',

          }}
            onClick={() => {
              this.getTotalOrders('Completed')
            }}
          >
            <div className="d-flex justify-content-center">
              <p style={{ color: 'rgba(0,0,0,.7)' }}>{fulfilledOrders}</p>
            </div>
            <div className="d-flex justify-content-center">
              <label style={{
                fontSize: "xx-large",
                color: "blue",
                cursor: "pointer"
              }}>{fulfilled_orders}</label>
            </div>
          </Col>


          <Col className="col "
            onClick={() => {
              this.getTotalOrders('Cancelled')
            }}
            style={{
              borderRightStyle: "solid",
              color: "#00000021",
              borderWidth: '2px',
            }}
          >
            <div className="d-flex justify-content-center">
              <p style={{ color: 'rgba(0,0,0,.7)' }}>{cancelledOrders}</p>
            </div>
            <div className="d-flex justify-content-center">
              <label style={{
                fontSize: "xx-large",
                color: "red",
                cursor: "pointer"
              }}>{cancelled_orders}</label>
            </div>
          </Col>


          <Col className="col "
            onClick={() => {
              this.getTotalOrders('In Progress')
            }}
          >
            <div className="d-flex justify-content-center">
              <p style={{ color: 'rgba(0,0,0,.7)' }}>{inProgress}</p>
            </div>
            <div className="d-flex justify-content-center">
              <label style={{
                fontSize: "xx-large",
                color: "red",
                cursor: "pointer"
              }}>{inprogress_orders}</label>
            </div>
          </Col>


        </Row>
        <Row>
          <Col className="col-12 " style={{
            visibility: "hidden"
          }}>
            <p>Order status</p>
          </Col>
        </Row>

      </div>)
  }
}

export default OutGoingOrdersSummary;