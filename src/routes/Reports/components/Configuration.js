import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import _ from 'lodash';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
import Loader from 'components/Loader';
import Alert from "components/Alert";
import HeaderSection from 'components/HeaderSection';
import NavDrawer from 'components/NavDrawer';
import { getLocalStorage } from 'components/Helper';

import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import OutGoingOrderSummary from './OutGoingOrderSummary';
import MostItem from './MostItem';
import TopVendor from './TopVendor';
import FilterDate from './FilterDate';
import Status from './Status';
import Footer from 'components/Footer';
import 'css/style.scss';
import Orders from './Orders'
import Shipments from './Shipments'
import Stock from './Stock';
import ReactLoading from 'react-loading';


/** Settings
 *
 * @description This class is a based class for settings stuff
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.handleAlertBox = this.handleAlertBox.bind(this);
    this.handleInboxOutboxOrders = this.handleInboxOutboxOrders.bind(this);
    this.changePassword = this.changePassword.bind(this);

    this.state = {
      active: 'orders',
      role: '',
      isSuperUser: '',
      isActiveOrder: true,
      isActiveShipment: false,
      isActiveStock: false,
      navInnerIndex: 0

    }
  }

  async componentWillMount() {
    let loginDetail = getLocalStorage('loginDetail');
    await this.setState({
      role: loginDetail.role,
      isSuperUser: loginDetail.userInfo.is_super_user
    });
    this.props.setGlobalState({
      navIndex: 13,
      navInnerIndex: 0,
    });



    // this.props.getStockSummary();
    // this.props.getStockProductwise();
    // this.props.getStockMonthwise();  
    // this.props.getStockSupplierwise();

  }

  async handleAlertBox() {
    this.props.resetAlertBox(false, "");
  }

  handleInboxOutboxOrders(classType, innerType) {
    console.log(classType, innerType);
    if (classType === 'orders' && innerType === 'inbox') {
    } else if (classType === 'orders' && innerType === 'outbox') {
    } else if (classType === 'shipments' && innerType === 'inbox') {
    } else if (classType === 'Audit Trail') {
    } else if (classType === 'logout') {
      removeLocalStorage('loginDetail');
      browserHistory.push(Url.HOME_PAGE);
    }
    else if (classType === 'reports' && innerType === 'orders') {
      this.props.fetchingReport(true)
      setTimeout(
        function () {
          console.log(`order case`);
          this.setState({
            isActiveShipment: false,
            isActiveOrder: true,
            isActiveStock: false,
            navInnerIndex: 0,

          })
          this.props.fetchingReport(false)
        }
          .bind(this),
        700
      );


    }
    else if (classType === 'reports' && innerType === 'shipments') {
      this.props.fetchingReport(true)
      setTimeout(
        function () {
          console.log(`shipment case`);
          this.setState({
            isActiveOrder: false,
            isActiveShipment: true,
            isActiveStock: false,
            navInnerIndex: 1,
          })
          this.props.fetchingReport(false)
        }
          .bind(this),
        700
      );


    }
    else if (classType === 'reports' && innerType === 'stock') {
      this.props.fetchingReport(true)
      setTimeout(
        function () {
          console.log(`stock case`);
          this.setState({
            isActiveOrder: false,
            isActiveShipment: false,
            isActiveStock: true,
            navInnerIndex: 2,
          })
          this.props.fetchingReport(false)
        }
          .bind(this),
        700
      );


    }
  }

  changePassword(values) {
    let lStorage = getLocalStorage('loginDetail');
    values.user_id = lStorage.userInfo.user_id;
    this.props.updatePassword(values);
  }

  render() {
    return (
      <div>
        {/* <Loader loading={this.props.fetching} /> */}
        {this.props.fetching &&
          <ReactLoading type="bubbles" style={{
            fill: '#4e5be1e0',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundSize: '200px',
            position: 'fixed',
            zIndex: 10000,
            width: '6%',
            height: '72%',
            top: '50%',
            left: '50%'
          }} />
        }
        <Alert
          showAlert={(typeof this.props.showAlert !== "undefined" ? this.props.showAlert : false)}
          message={(typeof this.props.alertMessage !== "undefined" ? this.props.alertMessage : "")}
          handleAlertBox={this.handleAlertBox}
          status={this.props.status}
        />
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-2 col-lg-2 sidebar">
              <NavDrawer
                role={this.state.role}
                isSuperUser={this.state.isSuperUser}
                setGlobalState={this.props.setGlobalState}
                navIndex={this.props.navIndex}
                navInnerIndex={this.props.navInnerIndex}
                navClass={this.props.navClass}
                handleInboxOutboxOrders={this.handleInboxOutboxOrders}
                currentLanguage={this.props.currentLanguage}
                setInnerNav={this.props.setInnerNav}
                setParam={this.props.setParam}
                setSearchString={this.props.setSearchString}
                setReportFetching={this.props.setReportFetching}
              />
            </div>
            {
              this.state.isActiveOrder &&
              <Orders
                active={this.state.active}
                currentLanguage={this.props.currentLanguage}
                setCurrentLanguage={this.props.setCurrentLanguage}
                getOutgoingOrdersSummary={this.props.getOutgoingOrdersSummary}
                getIncomingOrdersSummary={this.props.getIncomingOrdersSummary}
                getTopOrderSupplier={this.props.getTopOrderSupplier}
                getTopOrderProducts={this.props.getTopOrderProducts}
                getOutgoingOrdersSummary={this.props.getOutgoingOrdersSummary}
                outGoingOrderSummary={this.props.outGoingOrderSummary}
                getTopOrderProducts={this.props.getTopOrderProducts}
                mostItem={this.props.mostItem}
                getTopOrderSupplier={this.props.getTopOrderSupplier}
                topVendor={this.props.topVendor}
                setFromDate={this.props.setFromDate}
                setToDate={this.props.setToDate}
                incoming={this.props.incoming}
                getIncomingOrdersSummary={this.props.getIncomingOrdersSummary}
                setReportFetching={this.props.setReportFetching}
                setSearchString={this.props.setSearchString}
                setParam={this.props.setParam}
                currentLanguage={this.props.currentLanguage}
                setGlobalState={this.props.setGlobalState}
                setInnerNav={this.props.setInnerNav}
                getAllOrderTurnAround={this.props.getAllOrderTurnAround}
                orderTurnAround={this.props.orderTurnAround}
                getTopVendorOrderTurnaround={this.props.getTopVendorOrderTurnaround}
                vendorTurnAround={this.props.vendorTurnAround}
              />
            }

            {
              this.state.isActiveShipment &&
              <Shipments
                active={this.state.active}
                currentLanguage={this.props.currentLanguage}
                setCurrentLanguage={this.props.setCurrentLanguage}
                getOutgoingOrdersSummary={this.props.getOutgoingOrdersSummary}
                getIncomingOrdersSummary={this.props.getIncomingOrdersSummary}
                getTopOrderSupplier={this.props.getTopOrderSupplier}
                getTopOrderProducts={this.props.getTopOrderProducts}
                getOutgoingOrdersSummary={this.props.getOutgoingOrdersSummary}
                outGoingOrderSummary={this.props.outGoingOrderSummary}
                getTopOrderProducts={this.props.getTopOrderProducts}
                mostItem={this.props.mostItem}
                getTopOrderSupplier={this.props.getTopOrderSupplier}
                topVendor={this.props.topVendor}
                fetchingFromReport={this.props.fetchingFromReport}
                setGlobalState={this.props.setGlobalState}
                getOutgoingShipmentSummary={this.props.getOutgoingShipmentSummary}
                incoming={this.props.incoming}
                getIncomingShipmentSummary={this.props.getIncomingShipmentSummary}
                getTopShipmentBuyer={this.props.getTopShipmentBuyer}
                setFromDate={this.props.setFromDate}
                setToDate={this.props.setToDate}
                getTopShipmentProducts={this.props.getTopShipmentProducts}
                setReportFetching={this.props.setReportFetching}
                setSearchString={this.props.setSearchString}
                setParam={this.props.setParam}
                setInnerNav={this.props.setInnerNav}
              />
            }

            {
              this.state.isActiveStock &&
              <Stock
                active={this.state.active}
                currentLanguage={this.props.currentLanguage}
                setCurrentLanguage={this.props.setCurrentLanguage}
                getOutgoingOrdersSummary={this.props.getOutgoingOrdersSummary}
                getIncomingOrdersSummary={this.props.getIncomingOrdersSummary}
                getTopOrderSupplier={this.props.getTopOrderSupplier}
                getTopOrderProducts={this.props.getTopOrderProducts}
                getOutgoingOrdersSummary={this.props.getOutgoingOrdersSummary}
                outGoingOrderSummary={this.props.outGoingOrderSummary}
                getTopOrderProducts={this.props.getTopOrderProducts}
                mostItem={this.props.mostItem}
                getTopOrderSupplier={this.props.getTopOrderSupplier}
                topVendor={this.props.topVendor}
                fetchingFromReport={this.props.fetchingFromReport}
                setGlobalState={this.props.setGlobalState}
                getOutgoingShipmentSummary={this.props.getOutgoingShipmentSummary}
                incoming={this.props.incoming}
                getIncomingShipmentSummary={this.props.getIncomingShipmentSummary}
                getTopShipmentBuyer={this.props.getTopShipmentBuyer}
                setFromDate={this.props.setFromDate}
                setToDate={this.props.setToDate}
                getTopShipmentProducts={this.props.getTopShipmentProducts}
                setReportFetching={this.props.setReportFetching}
                setSearchString={this.props.setSearchString}
                setParam={this.props.setParam}
                setInnerNav={this.props.setInnerNav}
                getStockSummary={this.props.getStockSummary}
                getStockProductwise={this.props.getStockProductwise}
                getStockMonthwise={this.props.getStockMonthwise}
                getStockSupplierwise={this.props.getStockSupplierwise}
                summary={this.props.summary}
                monthWise={this.props.monthWise}
                productWise={this.props.productWise}
                supplierWise={this.props.supplierWise}
                getAllOrderTurnAround={this.props.getAllOrderTurnAround}
                getTopVendorOrderTurnaround={this.props.getTopVendorOrderTurnaround}
              />
            }

          </div>
        </div>
      </div >
    );
  }
}

Settings.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default Settings;