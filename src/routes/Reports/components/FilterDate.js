import React from 'react';
import RenderDatePicker from '../../ProcessLoss/components/RenderDatePicker';
import { Row, Col } from 'reactstrap';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
//localization 
import LocalizedStrings from 'react-localization';
import data from 'localization/data';
let strings = new LocalizedStrings(
    data
);
import { getCurrentDate, getCurrentDateWithFirstDay } from '../../../components/Helper'
import moment from 'moment';
import { getDate } from 'components/Helper';

class FilterDate extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            fromDate: '',
            toDate: ''
        }
        this.toDateHandler = this.toDateHandler.bind(this);
        this.fromDateHandler = this.fromDateHandler.bind(this);
    }

    async toDateHandler(date) {
        let toDate = Object.values(date).reduce((total, val, index) => (index <= 9) ? total + val : total);

        if (typeof toDate == "function") {
            await this.setState({
                toDate: ''
            });
            this.props.setToDate('');
        }
        else {

            await this.setState({
                toDate: toDate
            });
            this.props.setToDate(toDate);
        }



        if (this.state.fromDate && this.state.toDate && typeof toDate != "function") {
            await this.props.getOutgoingOrdersSummary(this.state.toDate, this.state.fromDate);
            await this.props.getIncomingOrdersSummary(this.state.toDate, this.state.fromDate);
            await this.props.getTopOrderSupplier(this.state.toDate, this.state.fromDate);
            await this.props.getTopOrderProducts(this.state.toDate, this.state.fromDate);
            await this.props.getAllOrderTurnAround(this.state.toDate, this.state.fromDate)
        }
    }

    async fromDateHandler(date) {
        let fromDate = Object.values(date).reduce((total, val, index) => (index <= 9) ? total + val : total);
        if (typeof toDate == "function") {
            await this.setState({
                fromDate: ''
            });
            this.props.setFromDate('');
        }
        else {

            await this.setState({
                fromDate: fromDate
            });
            this.props.setFromDate(fromDate);
        }
        if (this.state.fromDate && this.state.toDate && typeof fromDate != "function") {
            await this.props.getOutgoingOrdersSummary(this.state.toDate, this.state.fromDate);
            await this.props.getIncomingOrdersSummary(this.state.toDate, this.state.fromDate);
            await this.props.getTopOrderSupplier(this.state.toDate, this.state.fromDate);
            await this.props.getTopOrderProducts(this.state.toDate, this.state.fromDate);
            await this.props.getAllOrderTurnAround(this.state.toDate, this.state.fromDate)
        }
    }

    render() {
        strings.setLanguage(this.props.currentLanguage);
        const { From, To } = strings.OrderFilterText;
        return (
            <div className='col-8' >
                <Row>
                    <Col className="col">
                        <label>{From}</label>
                        <Field
                            name='from_date'
                            type='date'
                            defaultValue={getDate(moment(new Date(new Date().getFullYear(), new Date().getMonth()-2, 1)), 'fromDate')}
                            component={RenderDatePicker}
                            label='From Date'
                            onChange={(date) => this.fromDateHandler(date)}
                        />
                    </Col>
                    <div style={{ width: '2%' }}></div>
                    <Col className="col">
                        <label>{To}</label>
                        <Field
                            name='to_date'
                            type='date'
                            component={RenderDatePicker}
                            label='To date'
                            onChange={(date) => this.toDateHandler(date)}
                            defaultValue={getDate(moment(new Date()), 'toDate')}

                        />
                    </Col>
                </Row>
            </div>
        )
    }
}


export default reduxForm({
    form: 'orderReports',
})(FilterDate)