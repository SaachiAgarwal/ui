import React, { Component } from "react";
import Chart from "react-apexcharts";
import LocalizedStrings from 'react-localization';
import data from 'localization/data';
let strings = new LocalizedStrings(
  data
);

class VendorTurnAround extends Component {
  
  constructor(props) {
    super(props);
  }

  componentDidMount() {  
    this.props.getTopVendorOrderTurnaround();
  }


  render() {
    strings.setLanguage(this.props.currentLanguage);
    console.log(this.props.vendorTurnAround)
    var company = this.props.vendorTurnAround
    const categories = [];
    var series = [];
    var test = [];
    var categoryName = [];
    var cat = [];
    var i =0;
    var j = 0;
    var k = 0;
    console.log(company);
    if (company.length > 0) {
      //debugger
      this.props.vendorTurnAround.forEach((element, index) => {

          
          let data = new Array();
        
          test[index] = element.avg_trunaround_days
          categories.push(element.supplier_company_name)
          
          data.push(element.avg_trunaround_days);
          series= [
            {
              name: "TurnAroundTime",
              data: test
             
            }         
          ] 
      })
      
    }
    console.log(test)
    console.log(series)
    console.log(cat)
    const options = {
      chart: {
        id: "basic-bar"
      },
      xaxis: {
        categories: categories
       
      },
      plotOptions: {
        bar: {
            distributed: true
        }
      }
    }
    const tooltip ={
      custom: function({series, seriesIndex, dataPointIndex, w}) {
        return '<div class="arrow_box">' +
          '<span>' + 'hello' + '</span>' +
          '</div>'
      }
    }
    console.log(company);
    console.log(series);
    return (
      <div className="app">
        <div className="row">
          <div className="mixed-chart" style={{paddingLeft: '3%'}}>
          <h2>{strings.orderStatus.vendorTurnAround}</h2>
            <Chart
              options={options}
              tooltip = {tooltip}
              series={series}
              type="bar"
              width="500"
              height="150"
            />
          </div>
        </div>
      </div>
    );
  }
}

export default VendorTurnAround;