import React from 'react';
import { Row, Col } from 'reactstrap';
import { browserHistory } from 'react-router';
//localization 
import LocalizedStrings from 'react-localization';
import data from 'localization/data';
// apex chartjs
import Chart from "react-apexcharts";

let strings = new LocalizedStrings(
    data
);


class StockMonthwise extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getOutgoingShipmentSummary();
        this.props.getStockSummary();
        this.props.getStockMonthwise();
    }

    getTotalOrders = (param) => {
        browserHistory.push('/shipments');
        const { setReportFetching, setParam, setSearchString, setInnerNav } = this.props;
        setReportFetching(true);
        setParam(param)
        setSearchString('');
        setInnerNav(true);

    }

    render() {
        const { total_shipment, shipment_shipped, shipment_received, shipment_open, shipment_cancelled } = this.props.outGoingOrderSummary[0] || {};
        strings.setLanguage(this.props.currentLanguage);
        const { shipmentStatus, totalShipment, shippedShipment, recievedShipment, openShipment, cancelledShipment } = strings.shipmentStatus;
        //stocks 
        const summary = this.props.summary[0] || [];
        const monthwise = this.props.monthWise || [];
        console.log(summary, monthwise);
        const { product_qty, product_uom } = summary;
        const label = [];
        const data = [];
        if (monthwise.length > 0) {
            monthwise.forEach(element => {
                label.push(element.stock_month);
                data.push(element.product_qty)



            });
        }

        const options = {
            chart: {  
              id: "basic-bar"
            },
            xaxis: {
              categories: label
            }
          } 

          return (
            <div className='col-12' style={{
                backgroundColor: "white",
                marginTop: "20px"

            }} >
                <Row>
                    <Col className="col-12 ">
                        <h2>{strings.stock.stockSummary}</h2>
                    </Col>
                </Row>
                {/* <div></div> */}

                <Row style={{ paddingTop: '2%' }}>

                    <Col className="col-5" style={{
                        borderRightStyle: "solid",
                        color: "#00000021",
                        borderWidth: '2px',
                        cursor: "pointer"
                    }}
                    >
                        <div className="d-flex justify-content-center">
                            <p style={{ color: 'rgba(0,0,0,.7)' }}>Total Raw Stocks</p>
                        </div>
                        <div className="d-flex justify-content-center">
                            <label style={{
                                fontSize: "xx-large",
                                color: "blue",
                                cursor: "pointer"
                            }} >{`${product_qty} ${product_uom}`}</label>
                        </div>
                    </Col>


                    <Col className="col-7" style={{
                        borderRightStyle: "solid",
                        color: "#00000021",
                        borderWidth: '2px',
                    }}
                    >
                        <div className="d-flex justify-content-center">
                            <Chart
                                options={options}
                                series={[
                                    {
                                        name : 'stocks',
                                        data : data
                                    }

                                ]}
                                type="bar"
                                width="600"
                                height = "200"
                            />
                        </div>
                    </Col>

                </Row>
                <Row>
                    <Col className="col-12 " style={{
                        visibility: "hidden"
                    }}>
                        <p>Order status</p>
                    </Col>
                </Row>

            </div>)
    }
}

export default StockMonthwise;