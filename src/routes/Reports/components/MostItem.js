import React from 'react';
import { Row, Col } from 'reactstrap';
//localization 
import LocalizedStrings from 'react-localization';
import data from 'localization/data';
let strings = new LocalizedStrings(
    data
);
class MostItem extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getTopOrderProducts();
  }

  render() {

    const items = this.props.mostItem || [];
    strings.setLanguage(this.props.currentLanguage);

    return (

      <Row>
        <Col className="col">
          <h2>{strings.orderItem}</h2>
        </Col>
        <div className="col-12" style={{ paddingLeft: '5%', paddingTop: '2%' }}>
          {
            items.map((item, index) => {
              return (
                <Row key={index}>
                  <Col className="col-6 ">
                    <p style={{color: 'rgba(0,0,0,.7)'}} data-toggle="tooltip" title={item.product_name}>{`${item.product_name.substring(0, 30)}...`}</p>
                  </Col>
                  <Col className="col-6 ">
                    <p style={{color: 'rgba(0,0,0,.7)'}}>{item.product_qty} {item.product_uom}</p>
                  </Col>
                </Row>
              )
            })
          }
        </div>
      </Row>

    )
  }

}

export default MostItem;