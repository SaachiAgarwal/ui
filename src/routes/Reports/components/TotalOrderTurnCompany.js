import React from 'react';
// import Chart from "react-apexcharts";
import PieChart from 'react-minimal-pie-chart';


class StockChart extends React.Component {
    constructor(props) {
        super(props);
    }


    componentDidMount() {
      this.props.getAllOrderTurnAround();
    }


    render() {
        const orderTurnAround = this.props.orderTurnAround || [];
        console.log(orderTurnAround)
        const pieData = [];
        const label = []
        const data = [];
        if (orderTurnAround.length > 0) {
          orderTurnAround.forEach(element => {

                // pieData.push(
                //     element.product_qty
                // )
                // label.push(
                //     element.product_name
                // )

                data.push({
                    title: element.supplier_company_name,
                    value: element.trunaround_days,
                    color: '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
                })

            });
        }
        // console.log(data);

        // const options = {
        //     chart: {
        //         id: "pie"
        //     },
        //     dataLabels: {
        //         show: false,

        //         offset: 0,
        //         minAngleToShowLabel: 0
        //     },
        //     // labels: label
        // }

        return (
            <div className="d-flex justify-content-center"
                style={{
                    top: '10px'
                }}
            >
                <PieChart
                    data={data}
                    radius={45}
                />
            </div>
        )
    }
}

export default StockChart