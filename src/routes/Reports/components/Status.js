import React from 'react';
import { Row, Col } from 'reactstrap';
import { browserHistory } from 'react-router'

import { Pie } from 'react-chartjs-2';
//localization 
import LocalizedStrings from 'react-localization';
import data from 'localization/data';
let strings = new LocalizedStrings(
    data
);

class Status extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getIncomingOrdersSummary();
    }

    getTotalOrders = (param) => {
        browserHistory.push('/orders');
        const { setReportFetching, fetchingFromReport } = this.props;
        setReportFetching(true);
        this.props.setParam(param);
        this.props.setSearchString('');
    }


    render() {
        const incoming = this.props.incoming[0] || [];;
        strings.setLanguage(this.props.currentLanguage);
        const { receivingStatus, totalOrders, newOrders, fulfilledOrders, inProgress, dueOrders, cancelledOrders } = strings.orderStatus;
        const IncomingPie = {
            labels: [
                totalOrders,
                newOrders,
                fulfilledOrders,
                inProgress,
                dueOrders,
                cancelledOrders
            ],
            datasets: [{
                data: [incoming.total_orders, incoming.new_orders, incoming.fulfilled_orders, incoming.inprogress_orders, incoming.due_orders, incoming.cancelled_orders],
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#a83299',
                    '#98a832',
                    '#a85f32'
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#a83299',
                    '#98a832',
                    '#a85f32'
                ]
            }]
        };

        return (
            <Row style={{ width: '104%', marginBottom: '-2%' }}>
                <Col className="col-12" style={{
                    backgroundColor: 'white'
                }} >
                    <h2>{receivingStatus}</h2>
                    <Pie
                        data={IncomingPie}
                        onElementsClick={
                            elems => {
                                switch (elems[0]._model.label) {
                                    case totalOrders:
                                        this.getTotalOrders('');
                                        break;
                                    case newOrders:
                                        this.getTotalOrders('Not Linked')
                                        break;
                                    case fulfilledOrders:
                                        this.getTotalOrders('Completed')
                                        break;
                                    case inProgress:
                                        this.getTotalOrders('In Progress')
                                        break;
                                    case dueOrders:
                                        this.getTotalOrders('Due Orders')
                                        break;
                                    case cancelledOrders:
                                        this.getTotalOrders('Cancelled')
                                        break;
                                    default:
                                        
                                }

                            }
                        }
                    />
                </Col>
            </Row>
        )
    }
}


export default Status;