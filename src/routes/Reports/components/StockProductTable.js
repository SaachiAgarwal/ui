import React from 'react';
import { Row, Col } from 'reactstrap';
import { browserHistory } from 'react-router';
//localization 
import LocalizedStrings from 'react-localization';
import data from 'localization/data';
let strings = new LocalizedStrings(
    data
);

class StockProductTable extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getStockProductwise();
    }

    getSupplier = (supplier) => {
        this.props.setSearchString(supplier);
        browserHistory.push('/shipments');
        const {setReportFetching} = this.props;
        setReportFetching(true);
        this.props.setParam('');
        this.props.setInnerNav(true);
    
    }

    render() {
        strings.setLanguage(this.props.currentLanguage);
        const {productSummary, supplierSummary, stockTabular} =  strings.stock;
        const productWise = this.props.productWise || [];
        return (
            <Col className="col-5 overflow-auto" style={{
                backgroundColor : "white",
                height : "400px",
                overflowY: "scroll",
                marginTop : "20px",
                marginRight: '-2%'
            }}>
                <h2>{stockTabular}</h2>
                <div className="col-12 " style={{paddingTop: '2%'}} >
                    {
                        productWise.map((item, index) => {
                            return (
                                <Row key= {index} style={{paddingBottom: '2%'}}>
                                    <Col className="col">
                                        <p style={{color: '#0000ffcf', cursor : "pointer"}}
                                        >{item.product_name}</p>
                                    </Col>
                                    <Col className="col">
                                        <p style={{color: 'rgba(0,0,0,.7)'}}>{item.product_qty  } {item.product_uom}</p>
                                    </Col>
                                </Row>
                            )
                        })
                    }
                </div>
            </Col>
        )
    }
}

export default StockProductTable;