import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import _ from 'lodash';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
import Loader from 'components/Loader';
import Alert from "components/Alert";
import HeaderSection from 'components/HeaderSection';
import NavDrawer from 'components/NavDrawer';
import { getLocalStorage } from 'components/Helper';

import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import OutGoingShipmentSummary from './OutGoingShipmentSummary';
import MostShipItem from './MostShipItem';
import TopBuyer from './TopBuyer';
import FilterDate from './FilterDate';
import ShipmentStatus from './ShipmentStatus';
import Footer from 'components/Footer';
import 'css/style.scss';




/** Settings
 *
 * @description This class is a based class for settings stuff
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class Orders extends React.Component {
  constructor(props) {
    super(props);
    this.handleAlertBox = this.handleAlertBox.bind(this);
    // this.handleInboxOutboxOrders = this.handleInboxOutboxOrders.bind(this);
    this.changePassword = this.changePassword.bind(this);

    this.state = {
      active: 'shipments',
      role: '',
      isSuperUser: '',
      isActiveOrder: false
    }
  }

  async componentWillMount() {
    let loginDetail = getLocalStorage('loginDetail');
    await this.setState({
      role: loginDetail.role,
      isSuperUser: loginDetail.userInfo.is_super_user
    });
    // this.props.setGlobalState({
    //   navIndex: 12,
    //   navInnerIndex: 1,
    //   navClass: 'Settings',
    // });
    // await   this.props.getOutgoingOrdersSummary();
    // this.props.getIncomingOrdersSummary();
    // this.props.getTopOrderSupplier();
    // this.props.getTopOrderProducts();
  }

  async handleAlertBox() {
    this.props.resetAlertBox(false, "");
  }

  // handleInboxOutboxOrders(classType, innerType) {
  //   if (classType === 'orders' && innerType === 'inbox') {
  //   } else if (classType === 'orders' && innerType === 'outbox') {
  //   } else if (classType === 'shipments' && innerType === 'inbox') {
  //   } else if (classType === 'Audit Trail') {
  //   } else if (classType === 'logout') {
  //     removeLocalStorage('loginDetail');
  //     browserHistory.push(Url.HOME_PAGE);
  //   }
  //   else if (classType === 'reports' && innerType === 'shipments') {
  //     this.setState({
  //       navIndex: 12,
  //       navInnerIndex: 1,
  //       navClass: 'Settings',
  //       isActiveOrder: false,
  //     })
  //   }
  // }

  changePassword(values) {
    let lStorage = getLocalStorage('loginDetail');
    values.user_id = lStorage.userInfo.user_id;
    this.props.updatePassword(values);
  }

  render() {
    console.log(`Order is mounted`);

    return (

      <div className="col-md-10 col-lg-10 col-lg-offset-2 col-md-offset-2 main" style={{ overflowX: 'hidden' }}>
        <HeaderSection
          active={this.state.active}
          currentLanguage={this.props.currentLanguage}
          setCurrentLanguage={this.props.setCurrentLanguage}
        />
        <FilterDate
          getOutgoingOrdersSummary={this.props.getOutgoingShipmentSummary}
          getIncomingOrdersSummary={this.props.getIncomingShipmentSummary}
          getTopOrderSupplier={this.props.getTopShipmentBuyer}
          getTopOrderProducts={this.props.getTopShipmentBuyer}
          currentLanguage={this.props.currentLanguage}
          setFromDate = {this.props.setFromDate}
          setToDate = {this.props.setToDate}
        />

        <OutGoingShipmentSummary
          setCurrentLanguage={this.props.setCurrentLanguage}
          getOutgoingShipmentSummary={this.props.getOutgoingShipmentSummary}
          outGoingOrderSummary={this.props.outGoingOrderSummary}
          setReportFetching = {this.props.setReportFetching}
          setSearchString = {this.props.setSearchString}
          setParam ={this.props.setParam}
          setInnerNav = {this.props.setInnerNav}
          currentLanguage={this.props.currentLanguage}
        />
        <div className='col-12'>
          <Row style={{ marginTop: '-1%', width: '104%' }}>
            <Col className="col-6" style={{
              marginTop: "20px"
            }} >
              <ShipmentStatus
                getIncomingShipmentSummary={this.props.getIncomingShipmentSummary}
                incoming={this.props.incoming}
                setReportFetching = {this.props.setReportFetching}
                setSearchString = {this.props.setSearchString}
                setParam ={this.props.setParam}
                setInnerNav = {this.props.setInnerNav} 
                setCurrentLanguage={this.props.setCurrentLanguage} 
                currentLanguage={this.props.currentLanguage}
              />
              <Row style={{ width: '105%' }}>
                <div className='col-12 overflow-auto' style={{
                  height: "230px",
                  overflowY: "scroll",
                  backgroundColor: 'white',
                  marginTop: '20px'
                }} >
                  <MostShipItem
                    getTopShipmentProducts={this.props.getTopShipmentProducts}
                    mostItem={this.props.mostItem}
                    currentLanguage={this.props.currentLanguage}
                    />
                </div>
              </Row>
            </Col>
            <TopBuyer
              getTopShipmentBuyer={this.props.getTopShipmentBuyer}
              topVendor={this.props.topVendor}
              setReportFetching = {this.props.setReportFetching}
              setSearchString = {this.props.setSearchString}
              setParam ={this.props.setParam}
              setInnerNav = {this.props.setInnerNav}
              currentLanguage={this.props.currentLanguage}
              />
          </Row>
        </div>
      </div>

    );
  }
}

Orders.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default Orders;