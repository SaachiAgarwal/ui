import React from 'react';
import { Row, Col } from 'reactstrap';
import { browserHistory } from 'react-router';
//localization 
import LocalizedStrings from 'react-localization';
import data from 'localization/data';
let strings = new LocalizedStrings(
    data
);

class TopVendor extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getTopShipmentBuyer();
    }

    getSupplier = (supplier) => {
        this.props.setSearchString(supplier);
        browserHistory.push('/shipments');
        const {setReportFetching} = this.props;
        setReportFetching(true);
        this.props.setParam('');
        this.props.setInnerNav(true);
    
    }

    render() {
        strings.setLanguage(this.props.currentLanguage);
        const topVendor = this.props.topVendor || [];
        return (
            <Col className="col-6 overflow-auto" style={{
                backgroundColor : "white",
                height : "700px",
                overflowY: "scroll",
                marginTop : "20px",
                marginRight: '-2%'
            }}>
                <h2>{strings.shipmentStatus.topBuyers}</h2>
                <div className="col-12 " style={{paddingTop: '2%'}} >
                    {
                        topVendor.map((item, index) => {
                            return (
                                <Row key= {index} style={{paddingBottom: '2%'}}>
                                    <Col className="col">
                                        <p style={{color: '#0000ffcf', cursor : "pointer"}}
                                        onClick = {() =>{
                                            this.getSupplier(item.buyer_company_name)
                                        } 
                                    }
                                        >{item.buyer_company_name}</p>
                                    </Col>
                                    <Col className="col">
                                        <p style={{color: 'rgba(0,0,0,.7)'}}>{item.product_qty  } {item.product_uom}</p>
                                    </Col>
                                </Row>
                            )
                        })
                    }
                </div>
            </Col>
        )
    }
}

export default TopVendor;