import React from 'react';
// import Chart from "react-apexcharts";
import PieChart from 'react-minimal-pie-chart';


class StockChart extends React.Component {
    constructor(props) {
        super(props);
    }


    componentDidMount() {
        this.props.getStockProductwise();
    }


    render() {
        const productwise = this.props.productWise || [];
        console.log(productwise);
        const pieData = [];
        const label = []
        const data = [];
        if (productwise.length > 0) {
            productwise.forEach(element => {

                // pieData.push(
                //     element.product_qty
                // )
                // label.push(
                //     element.product_name
                // )

                data.push({
                    title: element.product_name,
                    value: element.product_qty,
                    color: '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
                })

            });
        }
        // console.log(data);

        // const options = {
        //     chart: {
        //         id: "pie"
        //     },
        //     dataLabels: {
        //         show: false,

        //         offset: 0,
        //         minAngleToShowLabel: 0
        //     },
        //     // labels: label
        // }

        return (
            <div className="d-flex justify-content-center"
                style={{
                    top: '10px'
                }}
            >
                <PieChart
                    data={data}
                    radius={45}
                />
            </div>
        )
    }
}

export default StockChart