import React from 'react';
import { Row, Col } from 'reactstrap';
import { browserHistory } from 'react-router';

import { Pie } from 'react-chartjs-2';

//localization 
import LocalizedStrings from 'react-localization';
import data from 'localization/data';
let strings = new LocalizedStrings(
    data
);

class Status extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getIncomingShipmentSummary();
    }

    getTotalOrders = (param) => {
        browserHistory.push('/shipments');
        const { setReportFetching, fetchingFromReport } = this.props;
        setReportFetching(true);
        this.props.setParam(param);
        this.props.setSearchString('');
    }


    render() {
        const { total_shipment, shipment_shipped, shipment_received, shipment_open, shipment_cancelled } = this.props.incoming[0] || [];;
        strings.setLanguage(this.props.currentLanguage);
        const {deliveryStatus, totalShipment, shippedShipment,recievedShipment, openShipment,cancelledShipment} = strings.shipmentStatus;
        const IncomingPie = {
            labels: [
                totalShipment,
                shippedShipment,
                recievedShipment,
                openShipment,
                cancelledShipment,
            ],
            datasets: [{
                data: [total_shipment, shipment_shipped, shipment_received, shipment_open, shipment_cancelled],
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#a83299',
                    '#98a832',
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#a83299',
                    '#98a832',
                ]
            }]
        };
        return (
            <Row style={{ width: '104%', marginBottom: '-2%' }}>
                <Col className="col-12" style={{
                    backgroundColor: 'white'
                }} >
                    <h2>{deliveryStatus}</h2>
                    <Pie 
                    data={IncomingPie}
                    onElementsClick={
                        elems => {
                            switch (elems[0]._model.label) {
                                case totalShipment:
                                    this.getTotalOrders('');
                                    break;
                                case shippedShipment:
                                    this.getTotalOrders('Shipped')
                                    break;
                                case recievedShipment:
                                    this.getTotalOrders('Received')
                                    break;
                                case openShipment:
                                    this.getTotalOrders('Open')
                                    break;
                                case cancelledShipment:
                                    this.getTotalOrders('Rejected')
                                    break;
                                default:
                                    
                            }

                        }
                    }
                    />
                </Col>
            </Row>
        )
    }
}


export default Status;