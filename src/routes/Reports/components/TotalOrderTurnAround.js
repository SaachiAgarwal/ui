import React, { Component } from "react";
import Chart from "react-apexcharts";

class TotalOrderTurnAround extends Component {
  
  constructor(props) {
    super(props);
  }

  componentDidMount() {  
    this.props.getAllOrderTurnAround();
    this.props.getTopVendorOrderTurnaround();
  }


  render() {
    console.log(this.props.orderTurnAround)
    const company = this.props.orderTurnAround || [];
    const categories = [];
    var series = [];
    var test = [];
    var categoryName = [];
    var cat = [];
    var i =0;
    var j = 0;
    var k = 0;
    console.log(company);
    if (company.length > 0) {
      //debugger
      this.props.orderTurnAround.forEach((element, index) => {

          if(element.trunaround_days !== 0){

          

          
          let data = new Array();
        
          test[i++] = element.trunaround_days
          categoryName[j++] = element.order_number
          cat[k++] = "Order Number: " + element.order_number + ',' + ' ' +'Company name: ' +  element.supplier_company_name
          // if (categories.indexOf(element.supplier_company_name) == -1) {
          //   categories.push(element.supplier_company_name)
          // }
          categories.push(element.supplier_company_name)
          
          data.push(element.trunaround_days);
          // series.push({
          //   name: element.order_number,
          //   data: data
          // })
          series= [
            {
              name: "TurnAroundTime",
              data: test
              
            }
            
          ]
          }
       
      })
      
    }
    console.log(test)
    console.log(series)
    console.log(cat)
    const options = {
      chart: {
        id: "basic-bar"
      },
      xaxis: {
        categories: cat,
        labels:{
          show: false,
          rotate: -90,
        }
      },
      plotOptions: {
        bar: {
            distributed: true,
            columnWidth: '70%'
        },
      },
    }
    const tooltip ={
      custom: function({series, seriesIndex, dataPointIndex, w}) {
        return '<div class="arrow_box">' +
          '<span>' + 'hello' + '</span>' +
          '</div>'
      }
    }
    console.log(company);
    console.log(series);
    return (
      <div className="app">
        <div className="row">
          <div className="mixed-chart">
            <Chart
              options={options}
              tooltip = {tooltip}
              series={series}
              type="bar"
              width="1000"
              height="300"
            />
          </div>
        </div>
      </div>
    );
  }
}

export default TotalOrderTurnAround;