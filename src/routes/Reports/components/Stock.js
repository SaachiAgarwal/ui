import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import _ from 'lodash';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
import Loader from 'components/Loader';
import Alert from "components/Alert";
import HeaderSection from 'components/HeaderSection';
import NavDrawer from 'components/NavDrawer';
import { getLocalStorage } from 'components/Helper';

import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import StockMonthwise from './StockMonthwise';
import MostShipItem from './MostShipItem';
import StockProductTable from './StockProductTable';
import FilterDate from './FilterDate';
import ShipmentStatus from './ShipmentStatus';
import Footer from 'components/Footer';
import 'css/style.scss';
import StockChart from './StockChart';
import CompanyWise from './CompanyWise';
// import StockChart from './StockChart';

//localization 
import LocalizedStrings from 'react-localization';
import data from 'localization/data';
let strings = new LocalizedStrings(
  data
);




/** Settings
 *
 * @description This class is a based class for settings stuff
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class Orders extends React.Component {
  constructor(props) {
    super(props);
    this.handleAlertBox = this.handleAlertBox.bind(this);
    this.handleInboxOutboxOrders = this.handleInboxOutboxOrders.bind(this);
    this.changePassword = this.changePassword.bind(this);

    this.state = {
      active: 'stock_report',
      role: '',
      isSuperUser: '',
      isActiveOrder: false
    }
  }

  async componentWillMount() {

    let loginDetail = getLocalStorage('loginDetail');
    await this.setState({
      role: loginDetail.role,
      isSuperUser: loginDetail.userInfo.is_super_user
    });
  }

  async handleAlertBox() {
    this.props.resetAlertBox(false, "");
  }

  handleInboxOutboxOrders(classType, innerType) {
    if (classType === 'orders' && innerType === 'inbox') {
    } else if (classType === 'orders' && innerType === 'outbox') {
    } else if (classType === 'shipments' && innerType === 'inbox') {
    } else if (classType === 'Audit Trail') {
    } else if (classType === 'logout') {
      removeLocalStorage('loginDetail');
      browserHistory.push(Url.HOME_PAGE);
    }
    else if (classType === 'reports' && innerType === 'stock') {
      this.setState({
        navIndex: 12,
        navInnerIndex: 2,
        navClass: 'Settings',
        isActiveStock: true,
      })
    }
  }

  changePassword(values) {
    let lStorage = getLocalStorage('loginDetail');
    values.user_id = lStorage.userInfo.user_id;
    this.props.updatePassword(values);
  }

  render() {
    console.log(this.props.supplierWise);
    strings.setLanguage(this.props.currentLanguage);
    const {productSummary, supplierSummary} =  strings.stock;
    return (

      <div className="col-md-10 col-lg-10 col-lg-offset-2 col-md-offset-2 main" style={{ overflowX: 'hidden' }}>
        <HeaderSection
          active={this.state.active}
          currentLanguage={this.props.currentLanguage}
          setCurrentLanguage={this.props.setCurrentLanguage}
        />
        <FilterDate
          getOutgoingOrdersSummary={this.props.getOutgoingShipmentSummary}
          getIncomingOrdersSummary={this.props.getIncomingShipmentSummary}
          getTopOrderSupplier={this.props.getTopShipmentBuyer}
          getTopOrderProducts={this.props.getTopShipmentBuyer}
          currentLanguage={this.props.currentLanguage}
          setFromDate={this.props.setFromDate}
          setToDate={this.props.setToDate}
        />
        <StockMonthwise
          setCurrentLanguage={this.props.setCurrentLanguage}
          getOutgoingShipmentSummary={this.props.getOutgoingShipmentSummary}
          outGoingOrderSummary={this.props.outGoingOrderSummary}
          setReportFetching={this.props.setReportFetching}
          setSearchString={this.props.setSearchString}
          setParam={this.props.setParam}
          setInnerNav={this.props.setInnerNav}
          currentLanguage={this.props.currentLanguage}
          getStockMonthwise={this.props.getStockMonthwise}
          getStockSummary={this.props.getStockSummary}
          summary={this.props.summary}
          monthWise={this.props.monthWise}
        />
        <div className='col-12' style={{ marginTop: '-1%', backgroundColor: 'white', }}>
          <Row>
            <Col className="col-12 ">
              <h2>{productSummary}</h2>
            </Col>
          </Row>
          <Row >

            <Col className="col-7" style={{
              marginTop: "20px",
              backgroundColor: 'white',
              height: "400px",
            }} >

              <StockChart
                productWise={this.props.productWise}
                getStockProductwise={this.props.getStockProductwise}
              />

            </Col>
            <StockProductTable
              getTopShipmentBuyer={this.props.getTopShipmentBuyer}
              topVendor={this.props.topVendor}
              setReportFetching={this.props.setReportFetching}
              setSearchString={this.props.setSearchString}
              setParam={this.props.setParam}
              setInnerNav={this.props.setInnerNav}
              currentLanguage={this.props.currentLanguage}
              productWise={this.props.productWise}
              getStockProductwise={this.props.getStockProductwise}
              currentLanguage={this.props.currentLanguage}
              setCurrentLanguage={this.props.setCurrentLanguage}

            />
          </Row>
        </div>

        <div className="col-12" style={
          {
            backgroundColor: 'white',
            marginTop: '20px'
          }
        }  >
          <Row>
            <Col className="col-12 ">
              <h2>{supplierSummary}</h2>
            </Col>
          </Row>
          <CompanyWise
            supplierWise={this.props.supplierWise}
            getStockSupplierwise={this.props.getStockSupplierwise}
            getAllOrderTurnAround={this.props.getAllOrderTurnAround}
            getTopVendorOrderTurnaround={this.props.getTopVendorOrderTurnaround}
          />
        </div>
      </div>

    );
  }
}

Orders.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default Orders;