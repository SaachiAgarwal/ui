import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import _ from 'lodash';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
import Loader from 'components/Loader';
import Alert from "components/Alert";
import HeaderSection from 'components/HeaderSection';
import NavDrawer from 'components/NavDrawer';
import { getLocalStorage } from 'components/Helper';
import { getCurrentDateWithFirstDay, getCurrentDate } from '../../../components/Helper';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import OutGoingOrderSummary from './OutGoingOrderSummary';
import MostItem from './MostItem';
import TopVendor from './TopVendor';
import FilterDate from './FilterDate';
import Status from './Status';
import Footer from 'components/Footer';
import 'css/style.scss';
import TotalOrderTurnAround from './TotalOrderTurnAround'
import TotalOrderTurnCompany from './TotalOrderTurnCompany'
import VendorTurnAround from './VendorTurnAround'
import data from 'localization/data';
import LocalizedStrings from 'react-localization';
let strings = new LocalizedStrings(
  data
);

/** Settings
 *
 * @description This class is a based class for settings stuff
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class Orders extends React.Component {
  constructor(props) {
    super(props);
    this.handleAlertBox = this.handleAlertBox.bind(this);
    // this.handleInboxOutboxOrders = this.handleInboxOutboxOrders.bind(this);
    // this.changePassword = this.changePassword.bind(this);

    this.state = {
      active: 'orders',
      role: '',
      isSuperUser: '',
      isActiveOrder: false
    }
  }

  componentDidMount() {
    let fromDate = getCurrentDateWithFirstDay();
    let toDate = getCurrentDate();
    this.props.setFromDate(fromDate);
    this.props.setToDate(toDate);
    // this.props.setGlobalState({
    //   navInnerIndex: 0,
    // });
    this.props.setGlobalState({
      navInnerIndex: 0,
    });

  }

  async componentWillMount() {

    let loginDetail = getLocalStorage('loginDetail');
    await this.setState({
      role: loginDetail.role,
      isSuperUser: loginDetail.userInfo.is_super_user
    });
    await this.props.setGlobalState({
      navInnerIndex: 0,
    });
    // await   this.props.getOutgoingOrdersSummary();
    // this.props.getIncomingOrdersSummary();
    // this.props.getTopOrderSupplier();
    // this.props.getTopOrderProducts();
  }

  async handleAlertBox() {
    this.props.resetAlertBox(false, "");
  }

  // handleInboxOutboxOrders(classType, innerType) {
  //   if (classType === 'orders' && innerType === 'inbox') {
  //   } else if (classType === 'orders' && innerType === 'outbox') {
  //   } else if (classType === 'shipments' && innerType === 'inbox') {
  //   } else if (classType === 'Audit Trail') {
  //   } else if (classType === 'logout') {
  //     removeLocalStorage('loginDetail');
  //     browserHistory.push(Url.HOME_PAGE);
  //   }
  //   else if (classType === 'reports' && innerType === 'orders') {
  //     this.setState({
  //       isActiveOrder: true,
  //       navInnerIndex: 0,
  //       navIndex: 12,
  //       navClass: 'Settings', 
  //     })
  //   }
  // }

  // changePassword(values) {
  //   let lStorage = getLocalStorage('loginDetail');
  //   values.user_id = lStorage.userInfo.user_id;
  //   this.props.updatePassword(values);
  // }

  render() {
    strings.setLanguage(this.props.currentLanguage);
    console.log(`Order is mounted`);
    return (

      <div className="col-md-10 col-lg-10 col-lg-offset-2 col-md-offset-2 main" style={{ overflowX: 'hidden' }}>
        <HeaderSection
          active={this.state.active}
          currentLanguage={this.props.currentLanguage}
          setCurrentLanguage={this.props.setCurrentLanguage}
        />
        <FilterDate
          getOutgoingOrdersSummary={this.props.getOutgoingOrdersSummary}
          getIncomingOrdersSummary={this.props.getIncomingOrdersSummary}
          getTopOrderSupplier={this.props.getTopOrderSupplier}
          getTopOrderProducts={this.props.getTopOrderProducts}
          setFromDate={this.props.setFromDate}
          setToDate={this.props.setToDate}
          currentLanguage={this.props.currentLanguage}
          getAllOrderTurnAround={this.props.getAllOrderTurnAround}
        />
        <OutGoingOrderSummary
          getOutgoingOrdersSummary={this.props.getOutgoingOrdersSummary}
          outGoingOrderSummary={this.props.outGoingOrderSummary}
          setParam={this.props.setParam}
          setReportFetching={this.props.setReportFetching}
          setSearchString={this.props.setSearchString}
          currentLanguage={this.props.currentLanguage}
          setInnerNav={this.props.setInnerNav}
        />
        <div className='col-12'>
          <Row style={{ marginTop: '-1%', width: '104%' }}>
            <Col className="col-6" style={{
              marginTop: "20px"
            }} >
              <Status
                getIncomingOrdersSummary={this.props.getIncomingOrdersSummary}
                incoming={this.props.incoming}
                setParam={this.props.setParam}
                setReportFetching={this.props.setReportFetching}
                setSearchString={this.props.setSearchString}
                currentLanguage={this.props.currentLanguage}
              />
              <Row style={{ width: '105%' }}>
                <div className='col-12 overflow-auto' style={{
                  height: "230px",
                  overflowY: "scroll",
                  backgroundColor: 'white',
                  marginTop: '20px'
                }} >
                  <MostItem
                    getTopOrderProducts={this.props.getTopOrderProducts}
                    mostItem={this.props.mostItem}
                    currentLanguage={this.props.currentLanguage}
                  />
                </div>
              </Row>
            </Col>
            <Col className="col-6 overflow-auto" style={{

            }}>
              <TopVendor
                getTopOrderSupplier={this.props.getTopOrderSupplier}
                topVendor={this.props.topVendor}
                setSearchString={this.props.setSearchString}
                setReportFetching={this.props.setReportFetching}
                setParam={this.props.setParam}
                currentLanguage={this.props.currentLanguage}
                setInnerNav={this.props.setInnerNav}
              />
              <Row style={{ width: '105%', paddingLeft: '3%' }}>
                <div className='col-12 overflow-auto' style={{
                  height: "230px",
                  backgroundColor: 'white',
                  marginTop: '20px'

                }} >
                  <VendorTurnAround
                    currentLanguage={this.props.currentLanguage}
                    setCurrentLanguage={this.props.setCurrentLanguage}
                    getAllOrderTurnAround={this.props.getAllOrderTurnAround}
                    orderTurnAround={this.props.orderTurnAround}
                    getTopVendorOrderTurnaround={this.props.getTopVendorOrderTurnaround}
                    vendorTurnAround={this.props.vendorTurnAround}
                  />
                </div>
              </Row>

            </Col>
          </Row>
          <div className="col-12" style={
            {
              backgroundColor: 'white',
              marginTop: '20px'
            }
          }  >
            <Row>
              <Col className="col-12 ">
                <h2>{strings.orderStatus.orderTurnAround}</h2>
              </Col>
            </Row>

            <TotalOrderTurnAround
              getAllOrderTurnAround={this.props.getAllOrderTurnAround}
              orderTurnAround={this.props.orderTurnAround}
              getTopVendorOrderTurnaround={this.props.getTopVendorOrderTurnaround}
            />
            {/* 
            <TotalOrderTurnCompany
              getAllOrderTurnAround={this.props.getAllOrderTurnAround}
              orderTurnAround={this.props.orderTurnAround}
            /> */}



          </div>
        </div>
      </div>

    );
  }
}

Orders.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default Orders;