import axios from 'axios';
import { browserHistory } from 'react-router';
import { Config, Url } from 'config/Config';
import { getToken, getCurrentDateWithFirstDay, getCurrentDate } from 'components/Helper';
// ------------------------------------
// Constant
// ------------------------------------

export const SET_ALERT_MESSAGE = 'SET_ALERT_MESSAGE';
export const FETCHING = 'FETCHING';
export const FETCHING_ERROR = 'FETCHING_ERROR';
export const CONFIG_DATA = 'CONFIG_DATA';
//SHIPMENTS
export const SET_OUTGOING_ORDERS_SUMMARY = "SET_OUTGOING_ORDERS_SUMMARY";
export const SET_INCOMING_ORDERS_SUMMARY = "SET_INCOMING_ORDERS_SUMMARY";
export const SET_TOP_ORDERS_SUPPLIER = "SET_TOP_ORDERS_SUPPLIER";
export const SET_TOP_ORDERS_PRODUCTS = "SET_TOP_ORDERS_PRODUCTS";
//STOCKS
export const SET_STOCK_SUMMARY = 'SET_STOCK_SUMMARY';
export const SET_STOCK_MONTHWISE = 'SET_STOCK_MONTHWISE';
export const SET_STOCK_PRODUCTWISE = 'SET_STOCK_PRODUCTWISE'
export const SET_STOCK_SUPPLIERWISE = 'SET_STOCK_SUPPLIERWISE'
export const ORDER_TURNAROUND = 'ORDER_TURNAROUND'
export const VENDOR_TURNAROUND = 'VENDOR_TURNAROUND'
export const FETCHING_REPORT = 'FETCHING_REPORT'
// ------------------------------------
// Actions
// ------------------------------------
export function fetchingReport(payload){
  return{
    type: FETCHING_REPORT,
    fetching: payload
  }
}
export function setStockSummary(summary) {
  return {
    type: SET_STOCK_SUMMARY,
    summary: summary,
  };
}export function setStockMonthwise(monthWise) {
  return {
    type: SET_STOCK_MONTHWISE,
    monthWise: monthWise,
  };
}export function setStockProductwise(productWise) {
  return {
    type: SET_STOCK_PRODUCTWISE,
    productWise: productWise,
  };
}export function setStockSupplierwise(supplierWise) {
  return {
    type: SET_STOCK_SUPPLIERWISE,
    supplierWise: supplierWise,
  };
}


export function setIncomingOrdersSummary(incoming) {
  return {
    type: SET_INCOMING_ORDERS_SUMMARY,
    incoming: incoming
  }

}

export function setOutgoingOrdersSummary(summary) {
  return {
    type: SET_OUTGOING_ORDERS_SUMMARY,
    outGoingOrderSummary: summary
  }
}

export function setMostItem(item) {
  return {
    type: SET_TOP_ORDERS_PRODUCTS,
    mostItem: item
  }
}

export function setTopVendor(item) {
  return {
    type: SET_TOP_ORDERS_SUPPLIER,
    topVendor: item
  }
}

export function setAlertMeassage(status, message, orderStatus = false) {
  return {
    type: SET_ALERT_MESSAGE,
    showAlert: status,
    alertMessage: message,
    status: orderStatus
  };
}

export function errorFetching(status) {
  return {
    type: FETCHING_ERROR,
    error: status
  }
}
export function setConfigData(payload) {
  return {
    type: CONFIG_DATA,
    configData: payload
  }
}
export function setOrderTurnAround(payload){
  return{
    type: ORDER_TURNAROUND,
    orderTurnAround: payload
  }
}
export function setVendorTurnAround(payload){
  return{
    type: VENDOR_TURNAROUND,
    vendorTurnAround: payload
  }
}
// ------------------------------------
// Action creators
// ------------------------------------
export const resetAlertBox = (showAlert, message) => {
  return (dispatch) => {
    dispatch(setAlertMeassage(showAlert, message));
  }
}

export const updateConfiguration = (value) => {
  return (dispatch) => {
    dispatch(fetchingReport(true));
    let endPoint = 'shipment/update_audit_display';
    axios({
      method: 'post',
      url: Config.url + endPoint,
      data: value,
      headers: { 'token': getToken() }
    }).then(response => {
      if (response.data.error === 1) {
        dispatch(fetchingReport(false));
        dispatch(setAlertMeassage(true, response.data.data, false));
      } else if (response.data.error === 5) {
        dispatch(fetchingReport(false));
        dispatch(setAlertMeassage(true, response.data.data, false));
        browserHistory.push(Url.LOGIN_PAGE);
      } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
        dispatch(fetchingReport(false));
        dispatch(setAlertMeassage(true, response.data.data, true));
        browserHistory.push(Url.CONFIGURATION_PAGE);
      } else if (response.data.error === 1) {
        dispatch(setAlertMeassage(true, response.data.data, false));
      }
    }).catch(error => {
      dispatch(fetchingReport(false));
      dispatch(errorFetching(true));
      // dispatch(setAlertMeassage(true, 'Password can not be updated.', false));
      browserHistory.push(Url.ERRORS_PAGE);
    });
  }
}

export const setConfigDetail = () => {
  return (dispatch) => {
    dispatch(fetchingReport(true));
    let endPoint = 'shipment/get_audit_display';
    let method = 'get';
    return new Promise((resolve, reject) => {
      axios({
        method: method,
        url: Config.url + endPoint,
        headers: { 'token': getToken() }
      }).then(response => {
        // console.log("Configure details")
        // console.log('response =========', response);
        if (response.data.error === 1) {
          dispatch(fetchingReport(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetchingReport(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetchingReport(false));
          dispatch(setConfigData(response.data));
        }
        resolve(true);

      }).catch(error => {
        dispatch(fetchingReport(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });

    });
  }
}

export const getOutgoingOrdersSummary = (toDate = '', fromDate = '') => (dispatch, getState) => {
  // console.log(getState());
  dispatch(fetchingReport(true));
  let fDate = '';
  let tDate = '';
  if (toDate && fromDate) {
    fDate = fromDate
    tDate = toDate
    // console.log(`filter data is provided`);

  } else {

    // console.log(`no filter date is provided`);
    tDate = getCurrentDate();
    fDate = getCurrentDateWithFirstDay();
  }
  // console.log(getCurrentDate(), getCurrentDateWithFirstDay());
  axios({
    method: 'get',
    url: `${Config.url}reports/outgoing_orders_summary?from_date=${fDate}&to_date=${tDate}`,
    headers: { 'token': getToken() }
  }).then(response => {
    // console.log("getOutgoingOrdersSummary")
    // console.log('response =========', response);
    const error = response.data.error;
    const data = response.data.data;
    if (error === 1) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));

    } else if (error === 5) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));
      browserHistory.push(Url.LOGIN_PAGE);

    } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
      dispatch(fetchingReport(false));
      dispatch(setConfigData(response.data));
      dispatch(setOutgoingOrdersSummary(data))

    }
    // resolve(true);

  }).catch(error => {
    dispatch(fetchingReport(false));
    dispatch(errorFetching(true));
    browserHistory.push(Url.ERRORS_PAGE);
    reject(true);
  });

}

export const getIncomingOrdersSummary = (toDate = '', fromDate = '') => (dispatch) => {
  dispatch(fetchingReport(true));
  let fDate = '';
  let tDate = '';
  if (toDate && fromDate) {
    fDate = fromDate
    tDate = toDate
    // console.log(`filter data is provided`);

  } else {

    // console.log(`no filter date is provided`);
    tDate = getCurrentDate();
    fDate = getCurrentDateWithFirstDay();
  }
  // console.log(tDate, fDate);
  axios({
    method: 'get',
    url: `${Config.url}reports/incoming_orders_summary?from_date=${fDate}&to_date=${tDate}`,
    headers: { 'token': getToken() }
  }).then(response => {
    // console.log("getIncomingOrdersSummary")
    // console.log('response =========', response);
    const error = response.data.error;
    const data = response.data.data;
    // console.log(error, data);



    if (response.data.error === 1) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));

    } else if (response.data.error === 5) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));
      browserHistory.push(Url.LOGIN_PAGE);

    } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
      dispatch(fetchingReport(false));
      dispatch(setConfigData(response.data));
      dispatch(setIncomingOrdersSummary(data));

    }
    // resolve(true);

  }).catch(error => {
    dispatch(fetchingReport(false));
    dispatch(errorFetching(true));
    browserHistory.push(Url.ERRORS_PAGE);
    reject(true);
  });

}


export const getTopOrderSupplier = (toDate = '', fromDate = '') => (dispatch) => {
  dispatch(fetchingReport(true));
  let fDate = '';
  let tDate = '';
  if (toDate && fromDate) {
    fDate = fromDate
    tDate = toDate
    // console.log(`filter data is provided`);

  } else {

    // console.log(`no filter date is provided`);
    tDate = getCurrentDate();
    fDate = getCurrentDateWithFirstDay();
  }

  axios({
    method: 'get',
    url: `${Config.url}reports/order_top_supplier?from_date=${fDate}&to_date=${tDate}`,
    headers: { 'token': getToken() }
  }).then(response => {
    // console.log("getTopOrderSupplier")
    // console.log('response =========', response);
    const error = response.data.error;
    const data = response.data.data;
    // console.log(error, data);

    if (response.data.error === 1) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));
    } else if (response.data.error === 5) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));
      browserHistory.push(Url.LOGIN_PAGE);
    } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
      dispatch(fetchingReport(false));
      dispatch(setConfigData(response.data));
      dispatch(setTopVendor(data));
    }


  }).catch(error => {
    dispatch(fetchingReport(false));
    dispatch(errorFetching(true));
    browserHistory.push(Url.ERRORS_PAGE);

  });

}

export const getTopOrderProducts = (toDate = '', fromDate = '') => (dispatch) => {
  dispatch(fetchingReport(true));
  let fDate = '';
  let tDate = '';
  if (toDate && fromDate) {
    fDate = fromDate
    tDate = toDate
    // console.log(`filter data is provided`);

  } else {

    // console.log(`no filter date is provided`);
    tDate = getCurrentDate();
    fDate = getCurrentDateWithFirstDay();
  }

  axios({
    method: 'get',
    url: `${Config.url}reports/order_top_products?from_date=${fDate}&to_date=${tDate}`,
    headers: { 'token': getToken() }
  }).then(response => {
    // console.log("getTopOrderSupplier")
    // console.log('response =========', response);
    const error = response.data.error;
    const data = response.data.data;
    // console.log(error, data);

    if (response.data.error === 1) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));
      // console.log('case A')
    } else if (response.data.error === 5) {
      dispatch(fetchingReport(aflse));
      dispatch(setAlertMeassage(true, response.data.data, false));
      browserHistory.push(Url.LOGIN_PAGE);
      // console.log('case B')
    } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
      dispatch(fetchingReport(false));
      dispatch(setConfigData(response.data));
      // console.log('case C')
      dispatch(setMostItem(data))
    }
    // resolve(true);

  }).catch(error => {
    // console.log("catch is running")
    dispatch(fetchingReport(false));
    dispatch(errorFetching(true));
    browserHistory.push(Url.ERRORS_PAGE);
    reject(true);
  });

}

export const getOutgoingShipmentSummary = (toDate = '', fromDate = '') => (dispatch, getState) => {
  dispatch(setOutgoingOrdersSummary([]))
  // console.log(getState());
  dispatch(fetchingReport(true));
  let fDate = '';
  let tDate = '';
  if (toDate && fromDate) {
    fDate = fromDate
    tDate = toDate
    // console.log(`filter data is provided`);

  } else {

    // console.log(`no filter date is provided`);
    tDate = getCurrentDate();
    fDate = getCurrentDateWithFirstDay();
  }
  // console.log(getCurrentDate(), getCurrentDateWithFirstDay());
  axios({
    method: 'get',
    url: `${Config.url}reports/outgoing_shipment_summary?from_date=${fDate}&to_date=${tDate}`,
    headers: { 'token': getToken() }
  }).then(response => {
    // console.log("getOutgoingOrdersSummary")
    // console.log('response =========', response);
    const error = response.data.error;
    const data = response.data.data;
    if (error === 1) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));

    } else if (error === 5) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));
      browserHistory.push(Url.LOGIN_PAGE);

    } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
      dispatch(fetchingReport(false));
      dispatch(setConfigData(response.data));
      dispatch(setOutgoingOrdersSummary(data))

    }
    // resolve(true);

  }).catch(error => {
    dispatch(fetchingReport(false));
    dispatch(errorFetching(true));
    browserHistory.push(Url.ERRORS_PAGE);
    reject(true);
  });

}

export const getIncomingShipmentSummary = (toDate = '', fromDate = '') => (dispatch) => {
  dispatch(fetchingReport(true));
  let fDate = '';
  let tDate = '';
  if (toDate && fromDate) {
    fDate = fromDate
    tDate = toDate
    // console.log(`filter data is provided`);

  } else {

    // console.log(`no filter date is provided`);
    tDate = getCurrentDate();
    fDate = getCurrentDateWithFirstDay();
  }
  // console.log(tDate, fDate);
  axios({
    method: 'get',
    url: `${Config.url}reports/incoming_shipment_summary?from_date=${fDate}&to_date=${tDate}`,
    headers: { 'token': getToken() }
  }).then(response => {
    // console.log("getIncomingOrdersSummary")
    // console.log('response =========', response);
    const error = response.data.error;
    const data = response.data.data;
    // console.log(error, data);



    if (response.data.error === 1) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));

    } else if (response.data.error === 5) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));
      browserHistory.push(Url.LOGIN_PAGE);

    } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
      dispatch(fetchingReport(false));
      dispatch(setConfigData(response.data));
      dispatch(setIncomingOrdersSummary(data));

    }
    // resolve(true);

  }).catch(error => {
    dispatch(fetchingReport(false));
    dispatch(errorFetching(true));
    browserHistory.push(Url.ERRORS_PAGE);
    reject(true);
  });

}

export const getTopShipmentBuyer = (toDate = '', fromDate = '') => (dispatch) => {
  dispatch(fetchingReport(true));
  let fDate = '';
  let tDate = '';
  if (toDate && fromDate) {
    fDate = fromDate
    tDate = toDate
    // console.log(`filter data is provided`);

  } else {

    // console.log(`no filter date is provided`);
    tDate = getCurrentDate();
    fDate = getCurrentDateWithFirstDay();
  }

  axios({
    method: 'get',
    url: `${Config.url}reports/shipment_top_buyer?from_date=${fDate}&to_date=${tDate}`,
    headers: { 'token': getToken() }
  }).then(response => {
    // console.log("getTopOrderSupplier")
    // console.log('response =========', response);
    const error = response.data.error;
    const data = response.data.data;
    // console.log(error, data);

    if (response.data.error === 1) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));
    } else if (response.data.error === 5) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));
      browserHistory.push(Url.LOGIN_PAGE);
    } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
      dispatch(fetchingReport(false));
      dispatch(setConfigData(response.data));
      dispatch(setTopVendor(data));
    }


  }).catch(error => {
    dispatch(fetchingReport(false));
    dispatch(errorFetching(true));
    browserHistory.push(Url.ERRORS_PAGE);

  });

}

export const getTopShipmentProducts = (toDate = '', fromDate = '') => (dispatch) => {
  dispatch(fetchingReport(true));
  let fDate = '';
  let tDate = '';
  if (toDate && fromDate) {
    fDate = fromDate
    tDate = toDate
    // console.log(`filter data is provided`);

  } else {

    // console.log(`no filter date is provided`);
    tDate = getCurrentDate();
    fDate = getCurrentDateWithFirstDay();
  }

  axios({
    method: 'get',
    url: `${Config.url}reports/shipment_top_products?from_date=${fDate}&to_date=${tDate}`,
    headers: { 'token': getToken() }
  }).then(response => {
    // console.log("getTopOrderSupplier")
    // console.log('response =========', response);
    const error = response.data.error;
    const data = response.data.data;
    // console.log(error, data);

    if (response.data.error === 1) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));
      // console.log('case A')
    } else if (response.data.error === 5) {
      dispatch(fetchingReport(aflse));
      dispatch(setAlertMeassage(true, response.data.data, false));
      browserHistory.push(Url.LOGIN_PAGE);
      // console.log('case B')
    } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
      dispatch(fetchingReport(false));
      dispatch(setConfigData(response.data));
      // console.log('case C')
      dispatch(setMostItem(data))
    }
    // resolve(true);

  }).catch(error => {
    // console.log("catch is running")
    dispatch(fetchingReport(false));
    dispatch(errorFetching(true));
    browserHistory.push(Url.ERRORS_PAGE);
    // reject(true);
  });

}

export const getStockSummary = (toDate = '', fromDate = '') => (dispatch) => {
  dispatch(fetchingReport(true));
  let fDate = '';
  let tDate = '';
  if (toDate && fromDate) {
    fDate = fromDate
    tDate = toDate
    // console.log(`filter data is provided`);

  } else {

    // console.log(`no filter date is provided`);
    tDate = getCurrentDate();
    fDate = getCurrentDateWithFirstDay();
  }
  // console.log(tDate, fDate);
  axios({
    method: 'get',
    url: `${Config.url}reports/stock_summary?from_date=${fDate}&to_date=${tDate}`,
    headers: { 'token': getToken() }
  }).then(response => {
    // console.log("getIncomingOrdersSummary")
    console.log('response =========', response);
    const error = response.data.error;
    const data = response.data.data;
    // console.log(error, data);



    if (response.data.error === 1) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));

    } else if (response.data.error === 5) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));
      browserHistory.push(Url.LOGIN_PAGE);

    } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
      dispatch(fetchingReport(false));
      dispatch(setConfigData(response.data));
      dispatch(setStockSummary(data));

    }
    // resolve(true);

  }).catch(error => {
    dispatch(fetchingReport(false));
    dispatch(errorFetching(true));
    browserHistory.push(Url.ERRORS_PAGE);
    reject(true);
  });

}

export const getStockMonthwise = (toDate = '', fromDate = '') => (dispatch) => {
  dispatch(fetchingReport(true));
  let fDate = '';
  let tDate = '';
  if (toDate && fromDate) {
    fDate = fromDate
    tDate = toDate
    // console.log(`filter data is provided`);

  } else {

    // console.log(`no filter date is provided`);
    tDate = getCurrentDate();
    fDate = getCurrentDateWithFirstDay();
  }
  // console.log(tDate, fDate);
  axios({
    method: 'get',
    url: `${Config.url}reports/stock_monthwise?from_date=${fDate}&to_date=${tDate}`,
    headers: { 'token': getToken() }
  }).then(response => {
    // console.log("getIncomingOrdersSummary")
    console.log('response =========', response);
    const error = response.data.error;
    const data = response.data.data;
    // console.log(error, data);



    if (response.data.error === 1) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));

    } else if (response.data.error === 5) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));
      browserHistory.push(Url.LOGIN_PAGE);

    } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
      dispatch(fetchingReport(false));
      dispatch(setConfigData(response.data));
      dispatch(setStockMonthwise(data));

    }
    // resolve(true);

  }).catch(error => {
    dispatch(fetchingReport(false));
    dispatch(errorFetching(true));
    browserHistory.push(Url.ERRORS_PAGE);
    reject(true);
  });

}


export const getStockProductwise = (toDate = '', fromDate = '') => (dispatch) => {
  dispatch(fetchingReport(true));
  let fDate = '';
  let tDate = '';
  if (toDate && fromDate) {
    fDate = fromDate
    tDate = toDate
    // console.log(`filter data is provided`);

  } else {

    // console.log(`no filter date is provided`);
    tDate = getCurrentDate();
    fDate = getCurrentDateWithFirstDay();
  }
  // console.log(tDate, fDate);
  axios({
    method: 'get',
    url: `${Config.url}reports/stock_productwise?from_date=${fDate}&to_date=${tDate}`,
    headers: { 'token': getToken() }
  }).then(response => {
    // console.log("getIncomingOrdersSummary")
    console.log('response =========', response);
    const error = response.data.error;
    const data = response.data.data;
    // console.log(error, data);



    if (response.data.error === 1) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));

    } else if (response.data.error === 5) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));
      browserHistory.push(Url.LOGIN_PAGE);

    } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
      dispatch(fetchingReport(false));
      dispatch(setConfigData(response.data));
      dispatch(setStockProductwise(data));

    }
    // resolve(true);

  }).catch(error => {
    dispatch(fetchingReport(false));
    dispatch(errorFetching(true));
    browserHistory.push(Url.ERRORS_PAGE);
    reject(true);
  });

}


export const getStockSupplierwise = (toDate = '', fromDate = '') => (dispatch) => {
  dispatch(fetchingReport(true));
  let fDate = '';
  let tDate = '';
  if (toDate && fromDate) {
    fDate = fromDate
    tDate = toDate
    // console.log(`filter data is provided`);

  } else {

    // console.log(`no filter date is provided`);
    tDate = getCurrentDate();
    fDate = getCurrentDateWithFirstDay();
  }
  // console.log(tDate, fDate);
  axios({
    method: 'get',
    url: `${Config.url}reports/stock_supplierwise?from_date=${fDate}&to_date=${tDate}`,
    headers: { 'token': getToken() }
  }).then(response => {
    // console.log("getIncomingOrdersSummary")
    console.log('response =========', response);
    const error = response.data.error;
    const data = response.data.data;
    // console.log(error, data);



    if (response.data.error === 1) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));

    } else if (response.data.error === 5) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));
      browserHistory.push(Url.LOGIN_PAGE);

    } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
      dispatch(fetchingReport(false));
      dispatch(setConfigData(response.data));
      dispatch(setStockSupplierwise(data));

    }
    // resolve(true);

  }).catch(error => {
    dispatch(fetchingReport(false));
    dispatch(errorFetching(true));
    browserHistory.push(Url.ERRORS_PAGE);
    reject(true);
  });

}


export const getAllOrderTurnAround = (toDate = '', fromDate = '') => (dispatch) => {
  dispatch(fetchingReport(true));
  let fDate = '';
  let tDate = '';
  if (toDate && fromDate) {
    fDate = fromDate
    tDate = toDate
    // console.log(`filter data is provided`);

  } else {
    
    // console.log(`no filter date is provided`);
    tDate = getCurrentDate();
    fDate = getCurrentDateWithFirstDay();
    console.log(fDate);
  }
   console.log(tDate, fDate);
  axios({
    method: 'get',
    url: `${Config.url}reports/get_all_order_turnaround?from_date=${fDate}&to_date=${tDate}`,
    headers: { 'token': getToken() }
  }).then(response => {
    // console.log("getIncomingOrdersSummary")
    console.log('response =========', response);
    const error = response.data.error;
    const data = response.data.data;
    // console.log(error, data);



    if (response.data.error === 1) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));

    } else if (response.data.error === 5) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));
      browserHistory.push(Url.LOGIN_PAGE);

    } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
      dispatch(fetchingReport(false));
      dispatch(setOrderTurnAround(data));

    }
    //resolve(true);

  }).catch(error => {
    dispatch(fetchingReport(false));
    dispatch(errorFetching(true));
    browserHistory.push(Url.ERRORS_PAGE);
    reject(true);
  });

}

export const getTopVendorOrderTurnaround = (toDate = '', fromDate = '') => (dispatch) => {
  dispatch(fetchingReport(true));
  let fDate = '';
  let tDate = '';
  if (toDate && fromDate) {
    fDate = fromDate
    tDate = toDate
    // console.log(`filter data is provided`);

  } else {

    // console.log(`no filter date is provided`);
    tDate = getCurrentDate();
    fDate = getCurrentDateWithFirstDay();
  }
  // console.log(tDate, fDate);
  axios({
    method: 'get',
    url: `${Config.url}reports/get_top_vendor_order_turnaround/?from_date=${fDate}&to_date=${tDate}`,
    headers: { 'token': getToken() }
  }).then(response => {
    // console.log("getIncomingOrdersSummary")
    console.log('response =========', response);
    const error = response.data.error;
    const data = response.data.data;
    // // console.log(error, data);



    if (response.data.error === 1) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));

    } else if (response.data.error === 5) {
      dispatch(fetchingReport(false));
      dispatch(setAlertMeassage(true, response.data.data, false));
      browserHistory.push(Url.LOGIN_PAGE);

    } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
      dispatch(fetchingReport(false));
      dispatch(setConfigData(response.data));
      dispatch(setVendorTurnAround(data));

    }
    // resolve(true);

  }).catch(error => {
    dispatch(fetchingReport(false));
    dispatch(errorFetching(true));
    browserHistory.push(Url.ERRORS_PAGE);
    reject(true);
  });

}



export const action = {
  updateConfiguration,
  setConfigDetail,
  getOutgoingOrdersSummary,
  // getIncomingOrdersSummary,
  // getTopOrderSupplier,
  // getTopOrderProducts
};
// ------------------------------------
// Action handlers
// ------------------------------------


const ACTION_HANDLERS = {
  [SET_STOCK_SUMMARY]: (state, action) => {
    return {
      ...state,
      summary: action.summary
    }
  },
  [SET_STOCK_MONTHWISE]: (state, action) => {
    return {
      ...state,
      monthWise: action.monthWise
    }
  },
  [SET_STOCK_PRODUCTWISE]: (state, action) => {
    return {
      ...state,
      productWise: action.productWise
    }
  },
  [SET_STOCK_SUPPLIERWISE]: (state, action) => {
    return {
      ...state,
      supplierWise: action.supplierWise
    }
  },
  [SET_ALERT_MESSAGE]: (state, action) => {
    return {
      ...state,
      showAlert: action.showAlert,
      alertMessage: action.alertMessage,
      status: action.status,
    }
  },
  [FETCHING]: (state, action) => {
    return {
      ...state,
      fetchingReport: action.fetchingReport
    }
  },
  [SET_TOP_ORDERS_SUPPLIER]: (state, action) => {
    return {
      ...state,
      topVendor: action.topVendor
    }
  },
  [SET_OUTGOING_ORDERS_SUMMARY]: (state, action) => {
    return {
      ...state,
      outGoingOrderSummary: action.outGoingOrderSummary
    }
  },
  [SET_TOP_ORDERS_PRODUCTS]: (state, action) => {
    return {
      ...state,
      mostItem: action.mostItem
    }
  },
  [FETCHING_ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
  [CONFIG_DATA]: (state, action) => {
    return {
      ...state,
      configData: action.configData
    }
  },
  [SET_INCOMING_ORDERS_SUMMARY]: (state, action) => {
    return {
      ...state,
      incoming: action.incoming
    }
  },
  [ORDER_TURNAROUND]: (state, action) => {
    return{
      ...state,
      orderTurnAround: action.orderTurnAround
    }
  },
  [VENDOR_TURNAROUND]: (state, action) => {
    return{
      ...state,
      vendorTurnAround: action.vendorTurnAround
    }
  },
  [FETCHING_REPORT]: (state, action) => {
    return{
      ...state,
      fetching: action.fetching
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  showAlert: false,
  alertMessage: '',
  status: false,
  error: false,
  configData: '',
  outGoingOrderSummary: [],
  mostItem: [],
  topVendor: [],
  incoming: [],
  // STOCKS
  summary : [],
  monthWise : [],
  productWise : [],
  supplierWise :[],
  orderTurnAround: [],
  vendorTurnAround: [],
  fetching: false
};

export default function configurationReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
