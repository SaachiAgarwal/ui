import { injectReducer } from '../../store/reducers';
import {checkPageRestriction} from  '../index';

export default (store) => ({
  path: 'reports',
  onEnter: (nextState, replace) => {
    checkPageRestriction(nextState, replace, () => {})
  },
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const Configuration = require('./containers/ConfigurationContainer').default;
      const reducer = require('./modules/configuration').default;
      injectReducer(store, { key: 'Configuration', reducer});
      cb(null, Configuration);
  }, 'Configuration');
  },
});
