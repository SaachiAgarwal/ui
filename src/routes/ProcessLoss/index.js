import { injectReducer } from '../../store/reducers';
import {checkPageRestriction} from '../index';

export default (store) => ({
  path: 'loss',
  onEnter: (nextState, replace) => {
    checkPageRestriction(nextState, replace, () => {})
  },
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const Process = require('./containers/ProcessLossContainer').default;
      const reducer = require('./modules/process').default;
      injectReducer(store, { key: 'Process', reducer });
      cb(null, Process);
  }, 'Process');
  },
});
