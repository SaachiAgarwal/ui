/** process reducer
 *
 * @description This is a reducer responsible for all the process functionality
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
import axios from 'axios';
import { browserHistory } from 'react-router';

import {Config, Url} from 'config/Config';
import {getToken, roundOffLoss, getPages} from 'components/Helper';
// ------------------------------------
// Constant
// ------------------------------------
export const SET_ALERT_MESSAGE = 'SET_ALERT_MESSAGE';
export const FETCHING = 'FETCHING';
export const ERROR = 'ERROR';
export const RECEIVED_ACTUAL_PROCESS_LOSS = 'RECEIVED_ACTUAL_PROCESS_LOSS';
export const SET_DEFINE_PROCESS_LOSS_STATE = 'SET_DEFINE_PROCESS_LOSS_STATE';
export const GET_DEFINE_PROCESS_LOSS = 'GET_DEFINE_PROCESS_LOSS';
export const SET_REVIEW_PROCESS_LOSS = 'SET_REVIEW_PROCESS_LOSS';
export const SET_EDIT_STATE = 'SET_EDIT_STATE';
export const SET_TOTAL_RECORD_COUNT = 'SET_TOTAL_RECORD_COUNT';
export const SET_TOTAL_PAGES = 'SET_TOTAL_PAGES';
export const SET_DOWNLOAD_STATE = 'SET_DOWNLOAD_STATE';
// ------------------------------------
// Actions
// ------------------------------------
export function setAlertMeassage(status, message, orderStatus=false) {
  return {
    type: SET_ALERT_MESSAGE,
    showAlert: status,
    alertMessage: message,
    status: orderStatus
  };
}

export function fetching(status) {
  return {
    type: FETCHING,
    fetching: status,
  };
}

export function errorFetching(status) {
  return {
    type: ERROR,
    error: status,
  };
}

export function receivedActualProcessLoss(payload) {
  return {
    type: RECEIVED_ACTUAL_PROCESS_LOSS,
    actualProcessLoss: payload,
  };
}

export function setDefineProcessLossState(status) {
  return {
    type: SET_DEFINE_PROCESS_LOSS_STATE,
    isDefineProcessLoss: status,
  };
}

export function getDefineProcessLoss(payload) {
  return {
    type: GET_DEFINE_PROCESS_LOSS,
    dpLossDetail: payload,
  };
}

export function setReviewProcessLoss(payload) {
  return {
    type: SET_REVIEW_PROCESS_LOSS,
    isReviewProcessLoss: payload,
  };
}

export function setIsEditState(status) {
  return {
    type: SET_EDIT_STATE,
    isEdit: status,
  };
}

export function setTotalRecordCount(payload) {
  return {
    type: SET_TOTAL_RECORD_COUNT,
    totalRecordCount: payload,
  }
}

export function setTotalPages(payload) {
  return {
    type: SET_TOTAL_PAGES,
    pages: payload,
  }
}

export function setIsDownloadState(status) {
  return {
    type: SET_DOWNLOAD_STATE,
    isDownload: status,
  }
}

// ------------------------------------
// Action creators
// ------------------------------------
export const resetAlertBox = (showAlert, message, status) => {
  return (dispatch) => {
    dispatch(setAlertMeassage(showAlert, message, status));
  }
}

export const setProps = (status, key) => {
  return (dispatch) => {
    switch(key) {
      case 'defineProcessState':
        dispatch(setDefineProcessLossState(status));
        break;
      case 'defineProcess':
        dispatch(getDefineProcessLoss(status));
        break;
      case 'isReviewProcessLoss':
        dispatch(setReviewProcessLoss(status));
        break;
      case 'isEdit':
        dispatch(setIsEditState(status));
        break;
      case 'isDownload':
        dispatch(setIsDownloadState(status));
        break;
    }
  }
}

export const fetchActualProcessLoss = (searchText, frmDt='', toDt='', pageSize=10, currentPage=1) => {
  return (dispatch) => {
    if (!searchText) dispatch(fetching(true));
    let data = {
      search_text: searchText,
      from_date: frmDt,
      to_date: toDt,
      page_size: pageSize,
      page_number: currentPage,
    }
    
    let endPoint = 'masters/actual_process_loss';
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: Config.url + endPoint,
        params: data,
        headers: {'token': getToken()}
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error === 0) {
          dispatch(fetching(false));
          let actualProcessLoss = roundOffLoss(response.data.data);
          let totalPage = getPages(response.data.total_recordcount);
          dispatch(setTotalRecordCount(response.data.total_recordcount));
          dispatch(setTotalPages(totalPage));
          dispatch(receivedActualProcessLoss(actualProcessLoss))
          resolve(true);
        } else if (response.data.error === 1) {
          dispatch(setAlertMeassage(true, response.data.data, true));
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(false));
        // dispatch(setAlertMeassage(true, 'Some Error Occured.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const manageDefineProcessLoss = (values=[]) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let requestMethod = (!_.isEmpty(values) && values !== undefined) ? 'put' : 'get';
    let data = (!_.isEmpty(values) && values !== undefined) ? values : [];
    let endPoint = 'admin/defined_process_loss';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: {'token': getToken()}
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error === 0) {
          dispatch(fetching(false));
          if (requestMethod === 'get') {
            dispatch(getDefineProcessLoss(response.data.data));
          } else {
            dispatch(setAlertMeassage(true, response.data.data, true));
          }
        } else if (response.data.error === 1) {
          dispatch(setAlertMeassage(true, response.data.data, true));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(false));
        // dispatch(setAlertMeassage(true, 'Some Error Occured.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const actions = {
  fetchActualProcessLoss,
  resetAlertBox,
  setProps
};

// ------------------------------------
// Action handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_ALERT_MESSAGE]: (state, action) => {
    return {
      ...state,
      showAlert: action.showAlert,
      alertMessage: action.alertMessage,
      status: action.status,
    }
  },
  [FETCHING]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching,
    }
  },
  [ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error,
    }
  },
  [RECEIVED_ACTUAL_PROCESS_LOSS]: (state, action) => {
    return {
      ...state,
      actualProcessLoss: action.actualProcessLoss,
    }
  },
  [SET_DEFINE_PROCESS_LOSS_STATE]: (state, action) => {
    return {
      ...state,
      isDefineProcessLoss: action.isDefineProcessLoss,
    }
  },
  [GET_DEFINE_PROCESS_LOSS]: (state, action) => {
    return {
      ...state,
      dpLossDetail: action.dpLossDetail,
    }
  },
  [SET_REVIEW_PROCESS_LOSS]: (state, action) => {
    return {
      ...state,
      isReviewProcessLoss: action.isReviewProcessLoss,
    }
  },
  [SET_EDIT_STATE]: (state, action) => {
    return {
      ...state,
      isEdit: action.isEdit,
    }
  },
  [SET_TOTAL_RECORD_COUNT]: (state, action) => {
    return {
      ...state,
      totalRecordCount: action.totalRecordCount,
    }
  },
  [SET_TOTAL_PAGES]: (state, action) => {
    return {
      ...state,
      pages: action.pages,
    }
  },
  [SET_DOWNLOAD_STATE]: (state, action) => {
    return {
      ...state,
      isDownload: action.isDownload,
    }
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  showAlert: false,
  alertMessage: '',
  status: false,
  fetching: false,
  error: false,
  actualProcessLoss: [],
  isDefineProcessLoss: false,
  dpLossDetail: [],
  isReviewProcessLoss: false,
  isEdit: false,
  totalRecordCount: 0,
  pages: 1,
  isDownload: false,
};

export default function processReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
