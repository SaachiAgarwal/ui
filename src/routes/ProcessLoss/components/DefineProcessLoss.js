import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Button } from 'reactstrap';
import _ from 'lodash';

import renderField from './renderField';
import SubmitButtons from 'components/SubmitButtons';

/** func Validate
 * description validate method will validate all the control fields of form based on provided criteria
 *
 * return Array of Errors
 */
const validate = values => {
  const errors = {}
  if (!values.spinning) {
    errors.spinning = 'Required'
  }
  if (!values.knitting) {
    errors.knitting = 'Required'
  }
  if (!values.processing) {
    errors.processing = 'Required'
  }
  /*if (!values.designing) {
    errors.designing = 'Required'
  }*/
  return errors
}

/** DefineProcessLoss
 *
 * @description This class is a based class for define process loss activity
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class DefineProcessLoss extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.handleInitialize();
  }

  async componentWillUnmount() {
    await this.props.setProps(false, 'isEdit');
  }

  handleInitialize() {
    const lossValue = ["Spinning", "Yarning", "Processing", "Designing"];
    let data = [];
    if (!_.isEmpty(this.props.dpLossDetail)) {
      this.props.dpLossDetail.map((item) => {
        if (lossValue.indexOf(item.process_name) !== -1) {// it means key exists
          data[item.process_name] = item.process_loss;
        }
      })
    }
      
    const initData = {
      Spinning: data.Spinning,
      Yarning: data.Yarning,
      Processing: data.Processing,
      Designing: data.Designing,
    }
    this.props.initialize(initData);
  }

  render() {
    const {handleSubmit} = this.props;
    return (
      <div className='col-md-12 col-lg-12 create-order'>
        <div>
          <form onSubmit={handleSubmit}>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='Spinning'
                  type="number"
                  component={renderField}
                  label="Spinning %"
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='Yarning'
                  type="number"
                  component={renderField}
                  label="Knitting/Weaving %"
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='Processing'
                  type="number"
                  component={renderField}
                  label="Processing %"
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='Designing'
                  type="number"
                  component={renderField}
                  label="Designing %"
                />
              </div>
            </div>
            <div>
              <SubmitButtons
                submitLabel={'save'}
                className='btn btn-primary create'
                submitting={this.props.submitting}
              />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

DefineProcessLoss.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default reduxForm({
  form: 'DefineProcessLoss',
  validate,
})(DefineProcessLoss)