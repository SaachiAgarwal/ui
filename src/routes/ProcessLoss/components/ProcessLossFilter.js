import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Row, Col} from 'reactstrap';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import DropdownList from 'react-widgets/lib/DropdownList';
import 'react-widgets/dist/css/react-widgets.css';
import moment from 'moment';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import renderField from './renderField';
import searchField from './searchField';
import {getDate} from 'components/Helper';
import RenderDatePicker from './RenderDatePicker';

const renderDropdownList = ({ input, data, valueField, textField, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div>
      <DropdownList {...input}
        data={data}
        onChange={input.onChange}
        onSelect={customProps.fetch(input.value)}
      />
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  ); 
};

class ProcessLossFilter extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div>
        <form onSubmit={handleSubmit}>
          <div className='col-12 col-sm-12 col-xl-12 mb-2'>
            <Row>
              <Col className="text-center col-4 col-sm-4 col-xl-6 pr-1">
                <Field
                  name='entity'
                  type="text"
                  component={searchField}
                  label="Search"
                  customProps={{
                    filter: this.props.searchFilter,
                    text: this.props.searchText
                  }}
                />
              </Col>
              <Col className="text-center col-4 col-sm-4 col-xl-3 pr-1">
                <label>From Date</label>
                <Field
                  name='from_date'
                  type='date'
                  defaultValue={getDate(moment(new Date(new Date().getFullYear(), new Date().getMonth(), 1)), 'fromDate')}
                  style={{display: "none"}}
                  component={RenderDatePicker}
                  label='From Date'
                  onChange={(date) => this.props.handleDateChange(date, 'fromDate')}
                />
              </Col>
              <Col className="text-center col-4 col-sm-4 col-xl-3 pr-1">
                <label>To Date</label>
                <Field
                  name='to_date'
                  type='date'
                  defaultValue={getDate(moment(new Date()), 'toDate')}
                  style={{display: "none"}}
                  component={RenderDatePicker}
                  label='To Date'
                  onChange={(date) => this.props.handleDateChange(date, 'toDate')}
                />
              </Col>
            </Row>
          </div>
        </form>
      </div>
    );
  }
}

ProcessLossFilter.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
};

export default reduxForm({
  form: 'ProcessLossFilter',
})(ProcessLossFilter)