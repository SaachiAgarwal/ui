 import React, {
Component } from
'react';

//import DatePicker from 'react-datepicker';

import moment
from 'moment';

//import 'react-datepicker/dist/react-datepicker.css';

import DatePicker
from 'react-bootstrap-date-picker'



import { 
getDate } from 
'components/Helper';



const RenderDatePicker = ({
input, label,
value, defaultValue,
meta: { touched,
error } }) => {

if (label ===
'From Date' && 
input.value !== 
'' && moment.isMoment(input.value))
defaultValue = 
getDate(input.value,
'fromDate');

if (label ===
'To Date' && input.value !==
'' && moment.isMoment(input.value))
defaultValue = 
getDate(input.value,
'toDate');

if(defaultValue !=undefined){

const spdate = (defaultValue.split("T"))[0].split("/");

defaultValue = 
spdate[2] + 
"/" + spdate[1] +
"/" + spdate[0];

}

return (

<div>

<DatePicker
{...input}

placeholder={defaultValue}

dateFormat="DD/MM/YYYY"

/>



{touched &&
error && <span
className="error-danger">

<i
className="fa fa-exclamation-circle">{error}</i></span>}

</div>

);

}



RenderDatePicker.propTypes = {

// value: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),

}



export default
RenderDatePicker;
