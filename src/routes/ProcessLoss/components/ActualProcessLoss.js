import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Pagination from 'react-paginate';

import {getDate} from 'components/Helper';

/** ActualProcessLoss
 *
 * @description This class is responsible to display all processloss information
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class ActualProcessLoss extends Component {
  constructor(props) {
    super(props);
    this.handlePageClick = this.handlePageClick.bind(this);
  }

  handlePageClick(value) {
    this.props.fetchActualProcessLoss(this.props.searchText, this.props.fromDt, this.props.toDt, this.props.options.pageSize, value.selected+1);
  }

  render() {
    return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options}>
          <TableHeaderColumn dataField='order_id' hidden={true} isKey={true}>Order Id</TableHeaderColumn>
          <TableHeaderColumn dataField='seller_company_name'>Entity</TableHeaderColumn>
          <TableHeaderColumn dataField='product_name' columnTitle={true}>Material Produced</TableHeaderColumn>
          <TableHeaderColumn dataField='product_qty'>Quantity</TableHeaderColumn>
          <TableHeaderColumn dataField='product_uom'>Unit</TableHeaderColumn>
          <TableHeaderColumn dataField='consume_product_name' columnTitle={true}>Material Consumed</TableHeaderColumn>
          <TableHeaderColumn dataField='consume_product_Qty'>Quantity</TableHeaderColumn>
          <TableHeaderColumn dataField='consume_product_uom'>Unit</TableHeaderColumn>
          <TableHeaderColumn dataField='process_loss'>Loss %</TableHeaderColumn>  
        </BootstrapTable>
        <div className="pagination-box">
          <Pagination
            previousLabel={"Prev"}
            nextLabel={"Next"}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={this.props.pages}
            pageRangeDisplayed={5}
            onPageChange={this.handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        </div>
      </div>        
    );
  }
}

ActualProcessLoss.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default ActualProcessLoss;