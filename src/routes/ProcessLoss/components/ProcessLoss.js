import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Button, Row } from 'reactstrap';
import moment from 'moment';
import Modal from 'react-modal';

import { Url } from 'config/Config';
import { removeLocalStorage, getLocalStorage, getDate, getText } from 'components/Helper';
import NavBar from 'components/navbar/NavBar';
import Loader from 'components/Loader';
import Alert from "components/Alert";
import HeaderSection from 'components/HeaderSection';
import NavDrawer from 'components/NavDrawer';
import options from 'components/options';
import ActualProcessLoss from './ActualProcessLoss';
import ProcessLossFilter from './ProcessLossFilter';
import DefineProcessLoss from './DefineProcessLoss';
import ReviewDefineProcessLoss from './ReviewDefineProcessLoss';
import CsvModal from 'components/CsvModal';
import Footer from 'components/Footer';
import 'css/style.scss';
import ReactLoading from 'react-loading';

//  Custom styles for link modal
const customStyles = {
  content: {
    top: '0',
    left: '0',
    right: '0',
    bottom: '0',
    marginRight: '0',
    transform: 'translate(0, 0)'
  }
};

/** ProcessLoss
 *
 * @description This class is a base class for Process loss functionality
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class ProcessLoss extends React.Component {
  constructor(props) {
    super(props);
    this.setActivePage = this.setActivePage.bind(this);
    this.handleAlertBox = this.handleAlertBox.bind(this);
    this.handleInboxOutboxOrders = this.handleInboxOutboxOrders.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.defineProcessLoss = this.defineProcessLoss.bind(this);
    this.edit = this.edit.bind(this);
    this.searchFilter = this.searchFilter.bind(this);
    this.handleCsvModal = this.handleCsvModal.bind(this);

    this.state = {
      role: '',
      isSuperUser: '',
      active: 'actual',
      fromDt: getDate(moment(new Date(new Date().getFullYear(), new Date().getMonth(), 1)), 'fromDate'),
      toDt: getDate(moment(new Date()), 'fromDate'),
      searchText: '',
      activeCsvModal: 'download'
    }
  }

  async componentWillMount() {
    this.props.setGlobalState({
      navIndex: 10,
      navInnerIndex: 0,
      navClass: 'Process Loss'
    });

    let loginDetail = getLocalStorage('loginDetail');
    this.setState({
      role: (loginDetail.userInfo.is_super_user) ? 'Admin' : loginDetail.role,
      isSuperUser: loginDetail.userInfo.is_super_user,
      active: 'actual'
    });

    await this.props.fetchActualProcessLoss(this.state.searchText, this.state.fromDt, this.state.toDt, options.pageSize, options.currentPage);
  }

  componentWillUnmount() {
    this.props.setProps(false, 'isEdit');
    this.props.setProps(false, 'defineProcessState');
    this.props.setProps([], 'defineProcess');
  }

  setActivePage(status) {
    this.setState({ active: status });
  }

  async handleAlertBox() {
    this.props.resetAlertBox(false, "", false);
  }

  async handleInboxOutboxOrders(classType, innerType) {
    if (classType === 'orders' && innerType === 'inbox') {
    } else if (classType === 'orders' && innerType === 'outbox') {
    } else if (classType === 'shipments' && innerType === 'inbox') {
    } else if (classType === 'shipments' && innerType === 'outbox') {
    } else if (classType === 'processloss' && innerType === 'actual') {
      this.setState({ active: 'actual' });
      this.props.setProps(false, 'defineProcessState');
    } else if (classType === 'processloss' && innerType === 'defined') {
      this.setState({ active: 'review' });
      this.props.setProps(true, 'isReviewProcessLoss');
      this.props.setProps(true, 'defineProcessState');
    } else if (classType === 'logout') {
      this.setState({
        active: 'logout'
      });
      removeLocalStorage('loginDetail');
      browserHistory.push(Url.HOME_PAGE);
    }
  }

  async handleDateChange(date, key) {
    let fromDt = '';
    let toDt = '';
    if (key === 'fromDate' && key !== 'toDate') {
      fromDt = Object.values(date).reduce((total, val, index) => (index <= 9) ? total + val : total);
      await this.setState({ fromDt: fromDt });
    } else if (key === 'toDate' && key !== 'fromDate') {
      toDt = Object.values(date).reduce((total, val, index) => (index <= 9) ? total + val : total);
      await this.setState({ toDt: toDt });
    }

    if (this.state.fromDt !== '' && this.state.toDt !== '' && this.state.fromDt >= this.state.toDt) {
      alert('From-Date must be less then To-Date');
    } else if (this.state.fromDt !== '' && this.state.toDt !== '') {
      this.props.fetchActualProcessLoss(this.state.searchText, this.state.fromDt, this.state.toDt, options.pageSize, options.currentPage);
    }
  }

  async defineProcessLoss(values) {
    await this.props.manageDefineProcessLoss([]);
    if (!_.isEmpty(this.props.dpLossDetail)) {
      this.props.dpLossDetail.map((item) => {
        let key = item.process_name;
        if (values[item.process_name]) {
          item.process_loss = values[item.process_name];
        }
      });
    }

    await this.props.manageDefineProcessLoss(this.props.dpLossDetail);

    // after update go back to review page false isEdit to show edit button and open review page
    this.setState({ active: 'review' });
    await this.props.setProps(false, 'isEdit');
    await this.props.setProps(true, 'isReviewProcessLoss');
  }

  async edit() {
    await this.props.setProps(true, 'isEdit');
    await this.props.manageDefineProcessLoss([]);
    this.setState({ active: 'defined' });
    await this.props.setProps(false, 'isReviewProcessLoss');
  }

  async searchFilter(query) {
    await this.setState({ searchText: query });

    if (query.length >= 2 || query.length === 0) this.props.fetchActualProcessLoss(this.state.searchText, this.state.fromDt, this.state.toDt, options.pageSize, options.currentPage);
  }

  async handleCsvModal(status, key) {
    await this.setState({ activeCsvModal: 'download' });
    this.props.setProps(status, 'isDownload');
    this.props.setGlobalState({ isUploaded: false });

    if (status) {
      await this.props.fetchActualProcessLoss(this.state.searchText, this.state.fromDt, this.state.toDt, this.props.totalRecordCount, options.currentPage);
    } else {
      await this.props.fetchActualProcessLoss(this.state.searchText, this.state.fromDt, this.state.toDt, options.pageSize, options.currentPage);
    }
  }

  render() {
    return (
      <div>
        {/* <Loader loading={this.props.fetching} /> */}
        {this.props.fetching &&
          <ReactLoading type="bubbles" style={{
            fill: '#4e5be1e0',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundSize: '200px',
            position: 'fixed',
            zIndex: 10000,
            width: '6%',
            height: '72%',
            top: '50%',
            left: '50%'
          }} />
        }
        <Alert
          showAlert={(typeof this.props.showAlert !== "undefined" ? this.props.showAlert : false)}
          message={(typeof this.props.alertMessage !== "undefined" ? this.props.alertMessage : "")}
          handleAlertBox={this.handleAlertBox}
          status={this.props.status}
        />
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-2 col-lg-2 sidebar">
              <NavDrawer
                role={this.state.role}
                isSuperUser={this.state.isSuperUser}
                setGlobalState={this.props.setGlobalState}
                navIndex={this.props.navIndex}
                navInnerIndex={this.props.navInnerIndex}
                navClass={this.props.navClass}
                handleInboxOutboxOrders={this.handleInboxOutboxOrders}
                currentLanguage={this.props.currentLanguage}
                setInnerNav={this.props.setInnerNav}
                setParam={this.props.setParam}
                setSearchString={this.props.setSearchString}
                setReportFetching={this.props.setReportFetching}
              />
            </div>
            <div className="col-md-10 col-lg-10 col-lg-offset-2 col-md-offset-2 main">
              <HeaderSection
                active={this.state.active}
                currentLanguage={this.props.currentLanguage}
                setCurrentLanguage={this.props.setCurrentLanguage}
              />
              <Modal
                isOpen={this.props.isUpload || this.props.isDownload}
                style={customStyles}
                ariaHideApp={false}
              >
                <CsvModal
                  owner={'ProcessLoss'}
                  role={this.state.role}
                  handleCsvModal={this.handleCsvModal}
                  getText={getText}
                  activeCsvModal={this.state.activeCsvModal}
                  data={this.props.actualProcessLoss}
                  isUploaded={this.props.isUploaded}
                  msg={this.props.msg}
                  currentLanguage={this.props.currentLanguage}
                />
              </Modal>
              {(!this.props.isDefineProcessLoss) ?
                <div>
                  <ProcessLossFilter
                    fetchProduct={this.fetchProduct}
                    fetchEntity={this.fetchEntity}
                    handleDateChange={this.handleDateChange}
                    fromDt={this.state.fromDt}
                    toDt={this.state.toDt}
                    searchFilter={this.searchFilter}
                  />
                  <ActualProcessLoss
                    fetchActualProcessLoss={this.props.fetchActualProcessLoss}
                    data={this.props.actualProcessLoss}
                    pagination={false}
                    search={false}
                    exportCSV={false}
                    options={options}
                    searchText={this.state.searchText}
                    fromDt={this.state.fromDt}
                    toDt={this.state.toDt}
                    pages={this.props.pages}
                  />
                  <div>
                    <Row>
                      <Button className='download-button process-loss' onClick={() => this.handleCsvModal(true, 'download')}>Download CSV</Button>
                    </Row>
                    <Row>
                      <p className="col-md-6 disclaimer">*Disclaimer: The process loss has been calculated based on sitra noms.</p>
                    </Row>
                  </div>
                </div>
                :
                <div>
                  {(!this.props.isReviewProcessLoss) ?
                    <DefineProcessLoss
                      onSubmit={this.defineProcessLoss}
                      dpLossDetail={this.props.dpLossDetail}
                      setProps={this.props.setProps}
                    />
                    :
                    <ReviewDefineProcessLoss
                      manageDefineProcessLoss={this.props.manageDefineProcessLoss}
                      dpLossDetail={this.props.dpLossDetail}
                      setProps={this.props.setProps}
                    />
                  }
                  <div className='btn-right'>
                    {(!this.props.isEdit) && <Button className="btn btn-primary create" onClick={this.edit}>Edit</Button>}
                  </div>
                </div>
              }
              <div className="clearfix"></div>
              <Footer
                currentLanguage={this.props.currentLanguage}

              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProcessLoss;