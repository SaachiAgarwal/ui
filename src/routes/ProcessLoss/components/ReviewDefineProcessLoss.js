import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Button } from 'reactstrap';
import _ from 'lodash';

import renderField from './renderField';
import SubmitButtons from 'components/SubmitButtons';

/** func readOnly
 *
 * @description This method returns a field object having a span to achieve onClick functionality. Simply we can not use
 *   onClick on field wrapper.
 *
 * return A component
 */
const readOnly = ({ input, label, type, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className="btn-bs-file">
      <span className='form-control'>
        <p>{customProps.value}</p>
      </span>
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

/** ReviewDefineProcessLoss
 *
 * @description This class is a based class to review process loss activity
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class ReviewDefineProcessLoss extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      initData: {},
    }
  }

  async componentWillMount() {
    await this.props.manageDefineProcessLoss([]);
    this.handleInitialize();
  }

  handleInitialize() {
    const lossValue = ["Spinning", "Yarning", "Processing", "Designing"];
    let data = [];
    if (!_.isEmpty(this.props.dpLossDetail)) {
      this.props.dpLossDetail.map((item) => {
        if (lossValue.indexOf(item.process_name) !== -1) {// it means key exists
          data[item.process_name] = item.process_loss;
        }
      })
    }
      
    const initData = {
      spinning: data.Spinning,
      knitting: data.Yarning,
      processing: data.Processing,
      designing: data.Designing,
    }
    this.setState({initData: initData});
    this.props.initialize(initData);
  }

  componentWillUnmount() {
    this.props.setProps([], 'defineProcess');
  }

  render() {
    const {handleSubmit} = this.props;
    return (
      <div className='col-md-12 col-lg-12 create-order'>
        <div>
          <form onSubmit={handleSubmit}>
            <div className="form-row">
              <div className="form-group col-md-4">
                <label>Spinning %</label>
                <Field
                  name='spinning'
                  type="number"
                  component={readOnly}
                  label="Spinning %"
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.spinning : ''
                  }}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <label>Knitting %</label>
                <Field
                  name='knitting'
                  type="number"
                  component={readOnly}
                  label="Knitting/Weaving %"
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.knitting : ''
                  }}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <label>Processing %</label>
                <Field
                  name='processing'
                  type="number"
                  component={readOnly}
                  label="Processing %"
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.processing : ''
                  }}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <label>Designing %</label>
                <Field
                  name='designing'
                  type="number"
                  component={readOnly}
                  label="Designing %"
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.designing : ''
                  }}
                />
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

ReviewDefineProcessLoss.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default reduxForm({
  form: 'ReviewDefineProcessLoss',
})(ReviewDefineProcessLoss)