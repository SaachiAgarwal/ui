/** ProcessLossContainer
 *
 * @description This is container that manages the states for all process loss related functionality.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
import { connect } from 'react-redux';

import ProcessLoss from '../components/ProcessLoss';
import {fetchActualProcessLoss, resetAlertBox, setProps, manageDefineProcessLoss} from '../modules/process.js';
import {setInnerNav,setParam,setSearchString,setReportFetching,setGlobalState, setCurrentLanguage} from '../../../store/app';

const mapStateToProps = (state) => {
  return({
    navIndex: state.app.navIndex,
    navInnerIndex: state.app.navInnerIndex,
    navClass: state.app.navClass,
    isUploaded: state.app.isUploaded,
    msg: state.app.msg,
    showAlert: state.Process.showAlert,
    alertMessage: state.Process.alertMessage,
    status: state.Process.status,
    fetching: state.Process.fetching,
    error: state.Process.error,
    actualProcessLoss: state.Process.actualProcessLoss,
    isDefineProcessLoss: state.Process.isDefineProcessLoss,
    isReviewProcessLoss: state.Process.isReviewProcessLoss,
    isEdit: state.Process.isEdit,
    dpLossDetail: state.Process.dpLossDetail,
    totalRecordCount: state.Process.totalRecordCount,
    pages: state.Process.pages,
    isDownload: state.Process.isDownload,
    currentLanguage: state.app.currentLanguage
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    setGlobalState: (value) => dispatch(setGlobalState(value)),
    resetAlertBox: (showAlert, message, status) => dispatch(resetAlertBox(showAlert, message, status)),
    fetchActualProcessLoss: (searchText, frmDt, toDt, pgSize, pgNo) => dispatch(fetchActualProcessLoss(searchText, frmDt, toDt, pgSize, pgNo)),
    setProps: (status, key) => dispatch(setProps(status, key)),
    manageDefineProcessLoss: (values) => dispatch(manageDefineProcessLoss(values)),
    setCurrentLanguage: (lang) => dispatch(setCurrentLanguage(lang)),
    setInnerNav: (lang) => dispatch(setInnerNav(lang)),
    setParam: (lang) => dispatch(setParam(lang)),
    setSearchString: (lang) => dispatch(setSearchString(lang)),
    setReportFetching: (lang) => dispatch(setReportFetching(lang)),
  });
};

export default connect(mapStateToProps, mapDispatchToProps)(ProcessLoss);
