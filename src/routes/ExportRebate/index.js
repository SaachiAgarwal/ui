import { injectReducer } from '../../store/reducers';

export default (store) => ({
  path: 'exportRebate',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const ExportRebate = require('./containers/ExportRebateContainer').default;
      const reducer = require('./modules/ExportRebate').default;
      injectReducer(store, { key: 'ExportRebate', reducer });
      cb(null, ExportRebate);
  }, 'ExportRebate');
  },
});
