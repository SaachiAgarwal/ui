import axios from "axios";
import { browserHistory } from "react-router";
import Swal from "sweetalert2";
import { Config, Url } from "config/Config";
import {
  getLocalStorage,
  saveLocalStorage,
  getToken,
  getPages,
  addFullName,
} from "components/Helper";

// ------------------------------------
// Constant
// ------------------------------------
export const SET_ALERT_MESSAGE = "SET_ALERT_MESSAGE";
export const FETCHING = "FETCHING";
export const ERROR = "ERROR";
export const SET_COMPANY_TYPE = 'SET_COMPANY_TYPE';
export const COMPANY_TYPE_SUCCESS = 'COMPANY_TYPE_SUCCESS';
export const GET_EXPORT_REBATE_DOCUMENT = 'GET_EXPORT_REBATE_DOCUMENT';
export const EXPORT_REBATE_TABLE_DATA = 'EXPORT_REBATE_TABLE_DATA';
export const EXPORT_REBATE_DETAIL_STATUS = 'EXPORT_REBATE_DETAIL_STATUS';
export const IS_DECLINE = 'IS_DECLINE';
export const GET_FIBER_QTY = 'GET_FIBER_QTY';
export const TRACE_EXPORT_REBATE = 'TRACE_EXPORT_REBATE';

// ------------------------------------
// Actions
// ------------------------------------
export function setAlertMeassage(status, message, orderStatus = false) {
  return {
    type: SET_ALERT_MESSAGE,
    showAlert: status,
    alertMessage: message,
    status: orderStatus,
  };
}
export function fetching(status) {
  return {
    type: FETCHING,
    fetching: status,
  };
}

export function errorFetching(status) {
    return {
      type: ERROR,
      error: status,
    };
  }

  export function fetchingCompanyTypeSuccess(payload) {
      console.log('cacacscscsas',payload)
    return {
      type: SET_COMPANY_TYPE,
      companyType: payload,
    };
  }

  export function companyTypeSuccess(payload){
    return{
      type: COMPANY_TYPE_SUCCESS,
      companyDetailVal: payload
  
    }
  }

  export function exportRebateDocument(billFile,invoiceFile){
    return{
      type: GET_EXPORT_REBATE_DOCUMENT,
      billFile: billFile,
      invoiceFile: invoiceFile
    }
  }

  export function exportRebateTableData(data,totalRecord){
      return {
          type:EXPORT_REBATE_TABLE_DATA,
          exportRebateTable:data,
          totalRecordCount:totalRecord
      }
  }

  export function setExportRebateDetailStatus(status){
    return {
        type:EXPORT_REBATE_DETAIL_STATUS,
        isActiveExportRebate:status
    }
  }

  export function setIsDcline(status){
    return{
        type:IS_DECLINE,
        isDecline:status
    }
  }

  export function setFiberQty(qty){
      return{
          type:GET_FIBER_QTY,
          fiberQty:qty
      }
  }

  export function traceExportRebate(data){
      return{
          type:TRACE_EXPORT_REBATE,
          traceExportRebate:data
      }
  }

// ------------------------------------
// Action creators
// ------------------------------------


const filterArray = (arr) => {
     return [...new Map(arr.map(item => [item['shipment_id'], item])).values()];
    // return arr
}

export const resetAlertBox = (showAlert, message) => {
    return (dispatch) => {
      dispatch(setAlertMeassage(showAlert, message));
    }
  }

  export const setProps = (status, key) => {

    return (dispatch) => {
      switch (key) {
        case 'isActiveExportRebate':
            dispatch(setExportRebateDetailStatus(status))
            break;
        case 'isDecline':
            dispatch(setIsDcline(status))
            break;    
      }
    }
  }

export const getCompanyType = () => {
    return (dispatch) => {
      dispatch(fetching(true));
      return new Promise((resolve, reject) => {
        axios({
          method: 'get',
          url: `${Config.url}users/get_company_type`,
          headers: { 'token': getToken() }
        }).then(response => {
          if (response.data.error === 1) {
            dispatch(fetching(false));
             dispatch(setAlertMeassage(true, response.data.data, false));
          } else if (response.data.error === 5) {
            dispatch(fetching(false));
             dispatch(setAlertMeassage(true, response.data.data, false));
            browserHistory.push(Url.LOGIN_PAGE);
          } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
            dispatch(fetching(false));
            console.log('=================',response.data)
             dispatch(fetchingCompanyTypeSuccess(response.data));
          }
          resolve(true);
        }).catch(error => {
          dispatch(fetching(false));
           dispatch(errorFetching(true));
           dispatch(setAlertMeassage(true, 'There is no company type.', false));
          browserHistory.push(Url.ERRORS_PAGE);
          reject(true);
        })
      });
    }
  }

  export const getCompany = (company_type_id) => {
    return (dispatch) => {
      // dispatch(fetching(true)); //stop loader on Company dropdown
      return new Promise((resolve, reject) => {
        axios({
          method: 'get',
          url: `${Config.url}users/get_company`,
          params: {
            company_type_id:company_type_id
          },
          headers: {'token': getToken()}
        }).then( response => {
          if (response.data.error === 1) {
            // dispatch(savingOrders(false));
            dispatch(setAlertMeassage(true, response.data.data, false));
          } else if (response.data.error === 5) {
            // dispatch(savingOrders(false));
            dispatch(setAlertMeassage(true, response.data.data, false));
            browserHistory.push(Url.LOGIN_PAGE);
          } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
            // dispatch(savingOrders(false));
            dispatch(companyTypeSuccess(response.data));
          }
          resolve(true);
        }).catch( error => {
          dispatch(savingOrders(false));
          dispatch(errorFetching(true));
          // dispatch(setAlertMeassage(true, 'Something went wrong.', false));
          browserHistory.push(Url.ERRORS_PAGE);
          reject(true);
        })
      });
    }
  }

  export const getAllExportRebateDocument = () => {
    return (dispatch) => {
        dispatch(fetching(true));
        return new Promise((resolve, reject) => {
            axios({
              method: 'post',
              url: `${Config.url}admin/get__all_export_documents`,
              headers: {'token': getToken()}
            }).then( response => {
              if (response.data.error === 1) {
                dispatch(fetching(false));
                // dispatch(savingOrders(false));
                 dispatch(setAlertMeassage(true, response.data.data, false));
              } else if (response.data.error === 5) {
                dispatch(fetching(false));
                // dispatch(savingOrders(false));
                 dispatch(setAlertMeassage(true, response.data.data, false));
                 browserHistory.push(Url.LOGIN_PAGE);
              } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
                dispatch(fetching(false));
                // dispatch(savingOrders(false));
                let modifiedData = response.data.documents.map(item=>{
                    let returnItem = {...item};
                    if(!item.is_approved && !item.is_rejected){
                        returnItem.status = 'Pending'
                    }else if(!item.is_approved && item.is_rejected){
                        returnItem.status = 'Rejected'
                    }else if(item.is_approved && !item.is_rejected){
                        returnItem.status = 'Accepted'
                    }

                    return returnItem
                })
                console.log('modifiedData',modifiedData)
                 dispatch(exportRebateTableData(filterArray(modifiedData),response.data.total_recordcount))
                // dispatch(companyTypeSuccess(response.data));
              }
              resolve(true);
            }).catch( error => {
            //   dispatch(savingOrders(false));
              dispatch(errorFetching(true));
              dispatch(setAlertMeassage(true, 'Something went wrong.', false));
              browserHistory.push(Url.ERRORS_PAGE);
              reject(true);
            })
          });
    }
  }

  export const getExportRebateDocumentByCompanyId = (company_key_id) => {
    return (dispatch) => {
        let data = {
            company_key_id: company_key_id
          }

        dispatch(fetching(true));
        return new Promise((resolve, reject) => {
            axios({
              method: 'post',
              url: `${Config.url}admin/get_export_documents_by_company_keyid`,
              data: data,
              headers: {'token': getToken()}
            }).then( response => {
              if (response.data.error === 1) {
                dispatch(fetching(false));
                // dispatch(savingOrders(false));
                 dispatch(setAlertMeassage(true, response.data.data, false));
              } else if (response.data.error === 5) {
                dispatch(fetching(false));
                // dispatch(savingOrders(false));
                 dispatch(setAlertMeassage(true, response.data.data, false));
                 browserHistory.push(Url.LOGIN_PAGE);
              } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
                dispatch(fetching(false));
                // dispatch(savingOrders(false));
                let modifiedData = response.data.documents.map(item=>{
                    let returnItem = {...item};
                    if(!item.is_approved && !item.is_rejected){
                        returnItem.status = 'Pending'
                    }else if(!item.is_approved && item.is_rejected){
                        returnItem.status = 'Rejected'
                    }else if(item.is_approved && !item.is_rejected){
                        returnItem.status = 'Accepted'
                    }

                    return returnItem
                })
                console.log('modifiedData',modifiedData)
                 dispatch(exportRebateTableData(filterArray(modifiedData),response.data.total_recordcount))
                // dispatch(companyTypeSuccess(response.data));
              }
              resolve(true);
            }).catch( error => {
            //   dispatch(savingOrders(false));
            //   dispatch(errorFetching(true));
              // dispatch(setAlertMeassage(true, 'Something went wrong.', false));
            //   browserHistory.push(Url.ERRORS_PAGE);
              reject(true);
            })
          });
    }
  }

  export const getExportRebateDocument = (shipmentId,status) => {
    return (dispatch) => {
      let endPoint = 'shipment/get_export_rebate_documents';
  
      let data = {
        shipment_id: shipmentId
      };
      dispatch(fetching(true));
      return new Promise((resolve, reject) => {
        axios({
          method: 'post',
          url: Config.url + endPoint,
          data: data,
          headers: {
            'token': getToken()
          }
        }).then(response => {
          if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
            dispatch(fetching(false));

            let billFile = response.data.documents.filter(item=>{
                if(item.is_bill === 1){
                  return item
                }
            })
            let invoiceFile = response.data.documents.filter(item=>{
                  if(item.is_bill != 1){
                    return item
                  }
            })

            if(status === 'Accepted'){
                billFile = billFile.filter(item=>{
                    if(item.is_approved === 1){
                        return item
                    }
                })
                invoiceFile = invoiceFile.filter(item=>{
                    if(item.is_approved === 1){
                        return item
                    }
                })
            }else if(status === 'Rejected'){
                let billRejected = 0;
                let invoiceRejected = 0;
                billFile.map(item => {
                    if(item.is_rejected === 1){
                        billRejected = billRejected + 1
                    }
                })
                invoiceFile.map(item => {
                    if(item.is_rejected === 1){
                        invoiceRejected = invoiceRejected + 1
                    }
                })
                if(billRejected === billFile.length){
                    billFile = [billFile[billFile.length - 1]]
                }else{
                    billFile = billFile.filter(item=>{
                        if(item.is_rejected === 1){
                            return item
                        }
                    })
                }
                if(invoiceRejected === invoiceFile.length){
                    invoiceFile = [invoiceFile[invoiceFile.length - 1]]
                }else{
                    invoiceFile = invoiceFile.filter(item=>{
                        if(item.is_rejected === 1){
                            return item
                        }
                    })
                }
            }else if(status === 'Pending'){
                billFile = billFile.filter(item=>{
                    if(item.is_rejected === 0 && item.is_approved === 0){
                        return item
                    }
                })
                invoiceFile = invoiceFile.filter(item=>{
                    if(item.is_rejected === 0 && item.is_approved === 0){
                        return item
                    }
                })
            }

            dispatch(exportRebateDocument(billFile,invoiceFile))
            // dispatch(uploadFileSuccess(true, response.data.data));
          } else if (response.data.error === 1) {
            dispatch(fetching(false));
  
            // dispatch(uploadFileSuccess(false, response.data.data));
          }
          resolve(true);
        }).catch(error => {
          dispatch(fetching(false));
          dispatch(errorFetching(true));
        })
      });
    }
  }

  export const downloadExportRebateDocument = (document_file_name,document_name) => {
    return (dispatch) => {
      let endPoint = 'shipment/download_export_document';
  
      let data = {
        document_file_name: document_file_name
      };
  
      return new Promise((resolve, reject) => {
        axios({
          method: 'post',
          url: Config.url + endPoint,
          data: data,
          headers: {
            'token': getToken()
          },
          responseType: 'blob',
        }).then(response => {
          console.log('response ', response);
          if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
            dispatch(fetching(false));
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', document_name);
            document.body.appendChild(link);
            link.click();
            resolve(true);
            // dispatch(uploadFileSuccess(true, response.data.data));
          } else if (response.data.error === 1) {
            dispatch(fetching(false));
            reject(true)
            // dispatch(uploadFileSuccess(false, response.data.data));
          }
        }).catch(error => {
          dispatch(fetching(false));
          dispatch(errorFetching(true));
        })
      });
    }
  }

  export const acceptExportRebateDocument = (shipment_id) => {
    return (dispatch) => {
        let endPoint = 'admin/approve_export_document';

        let data = {
            shipment_id: shipment_id
        };

        dispatch(fetching(true));
      return new Promise((resolve, reject) => {
        axios({
          method: 'post',
          url: Config.url + endPoint,
          data: data,
          headers: {
            'token': getToken()
          }
        }).then(response => {
          if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
            dispatch(fetching(false));
            console.log(response.data)
            // let billFile = response.data.documents.filter(item=>{
            //     if(item.is_bill === 1){
            //       return item
            //     }
            // })
            // let invoiceFile = response.data.documents.filter(item=>{
            //       if(item.is_bill != 1){
            //         return item
            //       }
            // })

            dispatch(setAlertMeassage(true, response.data.data, true));
            // dispatch(exportRebateDocument(billFile,invoiceFile))
            // dispatch(uploadFileSuccess(true, response.data.data));
          } else if (response.data.error === 1) {
            dispatch(fetching(false));
  
            // dispatch(uploadFileSuccess(false, response.data.data));
          }
          resolve(true);
        }).catch(error => {
          dispatch(fetching(false));
          dispatch(errorFetching(true));
        })
      });
    }
  }

  export const declineExportRebateDocument = (shipment_id,message) => {
      return (dispatch) => {
        let endPoint = 'admin/reject_export_document';

        let data = {
            shipment_id: shipment_id,
            reject_remarks:message
        };
        
        dispatch(fetching(true));
      return new Promise((resolve, reject) => {
        axios({
          method: 'post',
          url: Config.url + endPoint,
          data: data,
          headers: {
            'token': getToken()
          }
        }).then(response => {
          if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
            dispatch(fetching(false));
            console.log(response.data)
            // let billFile = response.data.documents.filter(item=>{
            //     if(item.is_bill === 1){
            //       return item
            //     }
            // })
            // let invoiceFile = response.data.documents.filter(item=>{
            //       if(item.is_bill != 1){
            //         return item
            //       }
            // })

            dispatch(setAlertMeassage(true, response.data.data, true));
            // dispatch(exportRebateDocument(billFile,invoiceFile))
            // dispatch(uploadFileSuccess(true, response.data.data));
          } else if (response.data.error === 1) {
            dispatch(fetching(false));
  
            // dispatch(uploadFileSuccess(false, response.data.data));
          }
          resolve(true);
        }).catch(error => {
          dispatch(fetching(false));
          dispatch(errorFetching(true));
        })
      });
      }
  }

  export const getFiberQty = (shipment_id) => {
    return (dispatch) => {
        let endPoint = 'admin/get_fibre_qty';

        let data = {
            shipment_id: shipment_id
        };

        dispatch(fetching(true));
      return new Promise((resolve, reject) => {
        axios({
          method: 'post',
          url: Config.url + endPoint,
          data: data,
          headers: {
            'token': getToken()
          }
        }).then(response => {
          if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
            dispatch(fetching(false));
            console.log(response.data)
            // let billFile = response.data.documents.filter(item=>{
            //     if(item.is_bill === 1){
            //       return item
            //     }
            // })
            // let invoiceFile = response.data.documents.filter(item=>{
            //       if(item.is_bill != 1){
            //         return item
            //       }
            // })
            dispatch(setFiberQty(response.data.Quantity[0].p_product_qty))
            // dispatch(setAlertMeassage(true, response.data.data, true));
            // dispatch(exportRebateDocument(billFile,invoiceFile))
            // dispatch(uploadFileSuccess(true, response.data.data));
          } else if (response.data.error === 1) {
            dispatch(fetching(false));
  
            // dispatch(uploadFileSuccess(false, response.data.data));
          }
          resolve(true);
        }).catch(error => {
          dispatch(fetching(false));
          dispatch(errorFetching(true));
        })
      });
    }
  }

  export const getTraceForExportRebate = (shipment_id) =>{
      return (dispatch) => {
          let endPoint = 'shipment/get_trace_for_export_rebate';

          let data = {
              shipment_id:shipment_id
          }

          dispatch(fetching(true));
          return new Promise((resolve, reject) => {
            axios({
              method: 'post',
              url: Config.url + endPoint,
              data: data,
              headers: {
                'token': getToken()
              }
            }).then(response => {
              if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
                dispatch(fetching(false));
                console.log('trace data',response.data)
                let data = (response.data).map(item=>{
                    let returnItem = {...item};
                    if(item.shipment_date){
                        returnItem.shipment_date = (item.shipment_date).split('T')[0]
                    }
                    if(item.received_date){
                        returnItem.received_date = (item.received_date).split('T')[0]
                    }
                    return returnItem;
                })
                dispatch(traceExportRebate(data))
              } else if (response.data.error === 1) {
                dispatch(fetching(false));
                dispatch(setAlertMeassage(true, response.data.data, false));
              }
              resolve(true);
            }).catch(error => {
              dispatch(fetching(false));
              dispatch(errorFetching(true));
            })
          });

      }
  }

  export const action = {
    resetAlertBox,
    setProps
  };

// ------------------------------------
// Action handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_ALERT_MESSAGE]: (state, action) => {
    return {
      ...state,
      showAlert: action.showAlert,
      alertMessage: action.alertMessage,
      status: action.status,
    };
  },
  [FETCHING]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching,
    };
  },
  [ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error,
    };
  },
  [SET_COMPANY_TYPE]: (state, action) => {
    return {
      ...state,
      companyType: action.companyType
    }
  },
  [COMPANY_TYPE_SUCCESS]: (state, action) => {
    return{
      ...state,
      companyDetailVal: action.companyDetailVal
    }
  },
  [GET_EXPORT_REBATE_DOCUMENT]: (state, action) => {
    return{
        ...state,
        billFile: action.billFile,
      invoiceFile: action.invoiceFile
      }
  },
  [EXPORT_REBATE_TABLE_DATA]: (state, action) => {
    return{
        ...state,
        exportRebateTable:action.exportRebateTable,
        totalRecordCount:action.totalRecordCount
      }
  },
  [EXPORT_REBATE_DETAIL_STATUS]: (state, action) => {
    return{
        ...state,
        isActiveExportRebate:action.isActiveExportRebate,
      }
  },
  [IS_DECLINE]: (state, action) => {
      return{
          ...state,
          isDecline:action.isDecline
      }
  },
  [GET_FIBER_QTY]: (state, action) => {
      return{
          ...state,
          fiberQty: action.fiberQty
      }
  },
  [TRACE_EXPORT_REBATE]: (state, action) => {
      return{
          ...state,
          traceExportRebate: action.traceExportRebate
      }
  }
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
    isActiveExportRebate:false,
    billFile:[],
    invoiceFile:[],
    exportRebateTable:[],
    traceExportRebate:[]
};

export default function companyReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
