import React from "react";
import PropTypes from "prop-types";
import { browserHistory } from "react-router";
import ReactLoading from "react-loading";
import NavDrawer from "components/NavDrawer";
import HeaderSection from "components/HeaderSection";
import Footer from "components/Footer";
import Alert from "components/Alert";
import ExportRebateBody from "./ExportRebateBody";
import Modal from "react-modal";
import ExportRebateDocument from "./ExportRebateDocument";
import ExportRebateTable from "./ExportRebateTable";
import options from "components/options";
import { Button } from "reactstrap";
import DeclineModal from "./DeclineModal";

import {
  getDate,
  getSupplierCompanyKeyId,
  getLocalStorage,
  getText,
  removeLocalStorage,
  saveLocalStorage,
  getCompanyId,
} from "components/Helper";

//localization
import LocalizedStrings from "react-localization";
import data from "../../../localization/data";
let strings = new LocalizedStrings(data);

const customStyles = {
  content: {
    top: "0",
    left: "0",
    right: "0",
    bottom: "0",
    marginRight: "0",
    transform: "translate(0, 0)",
  },
};

class ExportRebate extends React.Component {
  constructor(props) {
    super(props);
    this.handleAlertBox = this.handleAlertBox.bind(this);
    this.actionFormatter = this.actionFormatter.bind(this);
    this.backToExportRebate = this.backToExportRebate.bind(this);
    this.handleDecline = this.handleDecline.bind(this);
    this.handleSubmit  = this.handleSubmit.bind(this);
    this.acceptDocument = this.acceptDocument.bind(this);

    this.state = {
      user: {},
      role: "",
      isSuperUser: "",
      //   active: "reviewclaim",
      active: "exportrebate",
      shipmentId:'',
      isActiveExportRebate:false
    };
  }

  async componentWillMount() {
    let loginDetail = getLocalStorage("loginDetail");

    this.props.setGlobalState({
      navIndex: 4,
      navInnerIndex: 0,
      navClass: "Export Rebate",
    });

    this.setState({ user: loginDetail });

    await this.setState({
      role: loginDetail.role,
      isSuperUser: loginDetail.userInfo.is_super_user,
    });

    await this.props.getCompanyType();
    await this.props.getAllExportRebateDocument();
  }

  async handleAlertBox() {
    this.props.resetAlertBox(false, "");
  }

  actionFormatter(row, cell) {
    return (
      <ButtonGroup>
        {/* <Button className="action" title="view" style={{ paddingRight: '0px', paddingLeft: '1px' }} onClick={() => this.qaView(cell)} >{strings.incomingFormatterText.view}</Button> */}
        <Button
          className="action"
          title="view"
          style={{ paddingRight: "0px", paddingLeft: "1px" }}
          onClick={() => this.viewDetailDocument(cell)}
        >
          {strings.incomingFormatterText.view}
        </Button>
      </ButtonGroup>
    );
  }

  async handleDecline(row,status){
    await this.props.setProps(status, "isDecline");
    // await this.props.setProps(status, 'isDecline');
  }

  async viewDetailDocument(cell) {
    
    await this.props.getExportRebateDocument(cell.shipment_id,cell.status);
    // await this.props.setProps(true, "isActiveExportRebate");
    await this.props.getFiberQty(cell.shipment_id)
    await this.setState({
      active: "reviewclaim",
      shipmentId:cell.shipment_id,
      isActiveExportRebate:true
    });
    await this.props.getTraceForExportRebate(cell.shipment_id)
    
  }

  async backToExportRebate(){
    await this.setState({
        active: "exportrebate",
        isActiveExportRebate:false
      });
    // await this.props.setProps(false, "isActiveExportRebate");
  }

  async handleSubmit(value){
    await this.props.setProps(false, "isDecline");
    if(value.message){
        await this.props.declineExportRebateDocument(this.state.shipmentId,value.message)
    }else{
        await this.props.declineExportRebateDocument(this.state.shipmentId,'')
    }
     
  }

  async acceptDocument(){
    await this.props.acceptExportRebateDocument(this.state.shipmentId)
  }


  render() {
    // strings.setLanguage(this.props.currentLanguage);
    // console.log('detail',this.props.isActiveExportRebate)
    return (
      <div>
        {this.props.fetching && (
          <ReactLoading
            type="bubbles"
            style={{
              fill: "#4e5be1e0",
              backgroundRepeat: "no-repeat",
              backgroundPosition: "center",
              backgroundSize: "200px",
              position: "fixed",
              zIndex: 10000,
              width: "6%",
              height: "72%",
              top: "50%",
              left: "50%",
            }}
          />
        )}
        <Alert
          showAlert={
            typeof this.props.showAlert !== "undefined"
              ? this.props.showAlert
              : false
          }
          message={
            typeof this.props.alertMessage !== "undefined"
              ? this.props.alertMessage
              : ""
          }
          handleAlertBox={this.handleAlertBox}
          status={this.props.status}
        />
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-2 col-lg-2 sidebar">
              <NavDrawer
                role={this.state.role}
                isSuperUser={this.state.isSuperUser}
                setGlobalState={this.props.setGlobalState}
                navIndex={this.props.navIndex}
                navInnerIndex={this.props.navInnerIndex}
                navClass={this.props.navClass}
                // handleInboxOutboxOrders={this.handleInboxOutboxOrders}
                // unreadItems={this.props.unreadItems}
                currentLanguage={this.props.currentLanguage}
                setInnerNav={this.props.setInnerNav}
                setParam={this.props.setParam}
                setSearchString={this.props.setSearchString}
                setReportFetching={this.props.setReportFetching}
              />
            </div>
            <div className="col-md-10 col-lg-10 col-lg-offset-2 col-md-offset-2 main">
              {!this.state.isActiveExportRebate ? (
                <HeaderSection
                  active={this.state.active}
                  currentLanguage={this.props.currentLanguage}
                  setCurrentLanguage={this.props.setCurrentLanguage}
                />
              ) : (
                <HeaderSection
                  active={this.state.active}
                  currentLanguage={this.props.currentLanguage}
                  setCurrentLanguage={this.props.setCurrentLanguage}
                />
              )}

              {!this.state.isActiveExportRebate && (
                <ExportRebateBody
                  getCompanyType={this.props.getCompanyType}
                  companyType={this.props.companyType}
                  getCompany={this.props.getCompany}
                  companyDetail={this.props.companyDetail}
                  currentLanguage={this.props.currentLanguage}
                  getExportRebateDocumentByCompanyId={
                    this.props.getExportRebateDocumentByCompanyId
                  }
                />
              )}
              {!this.state.isActiveExportRebate &&
                this.props.exportRebateTable &&
                this.props.exportRebateTable.length > 0 && (
                  //   <div>Data</div>
                  <ExportRebateTable
                    data={this.props.exportRebateTable}
                    pagination={false}
                    search={false}
                    exportCSV={false}
                    haveAction={false}
                    options={options}
                    currentLanguage={this.props.currentLanguage}
                    actionFormatter={this.actionFormatter}
                  />
                )}
              {!this.state.isActiveExportRebate &&
                this.props.exportRebateTable &&
                this.props.exportRebateTable.length === 0 && (
                  <div
                    style={{
                      textAlign: "center",
                      backgroundColor: "#fff",
                      width: "97%",
                      margin: "auto",
                      padding: "20px 8px",
                    }}
                  >
                    No List Available
                  </div>
                )}
              {this.state.isActiveExportRebate && (
                <ExportRebateDocument
                  getExportRebateDocument={this.props.getExportRebateDocument}
                  billFile={this.props.billFile}
                  invoiceFile={this.props.invoiceFile}
                  downloadFile={this.props.downloadExportRebateDocument}
                  handleDecline={this.handleDecline}
                  acceptDocument={this.acceptDocument}
                  fiberQty={this.props.fiberQty}
                  traceExportRebate={this.props.traceExportRebate}
                  currentLanguage={this.props.currentLanguage}
                />
              )}
              {
                  this.state.isActiveExportRebate &&
                  <ButtonGroup>
                        <Button
                        className="action"
                        title="view"
                        style={{ paddingRight: "0px", paddingLeft: "1px",backgroundColor: '#fff',width:'100px',margin:'10px 0'}}
                        onClick={() => this.backToExportRebate()}
                        >
                        {strings.captalise.back}
                        </Button>
                  </ButtonGroup>
              }
              {
                  <Modal
                  isOpen={this.props.isDecline}
                   style={customStyles}
                ariaHideApp={false}
                  >
                        <DeclineModal 
                            handleDecline={this.handleDecline}
                            onSubmit={this.handleSubmit}
                        />
                  </Modal>
              }
              <div className="clearfix"></div>
              <Footer currentLanguage={this.props.currentLanguage} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ExportRebate;
