import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

//localization
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
import Pagination from 'react-paginate';
let strings = new LocalizedStrings(
  data
);


class ExportRebateTable extends Component {
  constructor(props) {
    super(props);
    // this.handlePageClick = this.handlePageClick.bind(this)
  }

//   handlePageClick(value) {
//     this.props.qaCompanyDetail(this.props.company_key_id, value.selected+1, this.props.options.pageSize, '', '', '');
//   }

  render() {
    const { currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);
    const {isApproved, remarks, documentName} = strings.qa

    // console.log('=======',this.props.data)
    return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv}>
          <TableHeaderColumn dataField='shipment_id' hidden={true}>shipment id</TableHeaderColumn>
          <TableHeaderColumn dataField='seller_company_name' isKey={true} autoValue={true} >Company</TableHeaderColumn>
          <TableHeaderColumn dataField='invoice_number' >Invoice number</TableHeaderColumn>
          <TableHeaderColumn dataField='status' >Status</TableHeaderColumn>
          <TableHeaderColumn key="editAction" dataFormat= {this.props.actionFormatter}>{strings.shipmentTableText.action}</TableHeaderColumn>
        </BootstrapTable>
        <div className="pagination-box">
          {/* <Pagination
            previousLabel={strings.prev}
            nextLabel={strings.next}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={this.props.pages}
            pageRangeDisplayed={5}
            onPageChange={this.handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          /> */}
        </div>
      </div>
    );
  }
}
ExportRebateTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default ExportRebateTable;