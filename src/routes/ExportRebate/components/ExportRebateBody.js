import React from "react";
import PropTypes from "prop-types";
import { browserHistory } from "react-router";
import { reduxForm, Field, SubmissionError, FieldArray } from "redux-form";
import { Button } from "reactstrap";
import _ from "lodash";
import DropdownList from "react-widgets/lib/DropdownList";
import "react-widgets/dist/css/react-widgets.css";
import SubmitButtons from "components/SubmitButtons";
import { getCompanyTypeId, getCompanyId } from "components/Helper";
// import QaUploadTableDetail from './QaUploadTableDetail'
// import DeclineQa from './DeclineQa'
import Modal from "react-modal";
import options from "components/options";

//localization
import LocalizedStrings from "react-localization";
import data from "localization/data";
let strings = new LocalizedStrings(data);

const renderCompanyType = ({
  input,
  label,
  data,
  valueField,
  textField,
  customProps,
  meta: { touched, error, invalid, submitFailed },
}) => {
  return (
    <div className={`form-group`}>
      <label className="control-label">{label}</label>
      <div>
        <DropdownList
          {...input}
          data={data}
          onChange={input.onChange}
          onSelect={customProps.fetchCompanyType(input.value)}
        />
        {submitFailed && error && <span className="error-danger">{error}</span>}
      </div>
    </div>
  );
};

const renderDropdownList = ({ input, label, data, valueField, textField, customProps, meta: { touched, error, invalid, submitFailed } }) => {
    return (
      <div className={`form-group`}>
        <label className="control-label">{label}</label>
        <div>
          <DropdownList {...input}
            data={data}
            onChange={input.onChange}
            onSelect={customProps.fetchCompanyDetails(input.value)}
          />
          {submitFailed && error && <span className="error-danger">{error}</span>}
        </div>
      </div>
    );
  }

class ExportRebateBody extends React.Component {
  constructor(props) {
    super(props);

    this.fetchCompanyType = this.fetchCompanyType.bind(this);
    this.fetchCompanyDetails = this.fetchCompanyDetails.bind(this);
    this.viewExportRebteDocument = this.viewExportRebteDocument.bind(this);

    this.state = {
        company_key_id: '',
    };
  }

  fetchCompanyType(value) {
    if (typeof value !== "undefined" && value !== "") {
      let company_type_id = getCompanyTypeId(
        this.props.companyType,
        value,
        false
      );
      this.props.getCompany(company_type_id);
    }
  }

  fetchCompanyDetails(value) {
    if (typeof value !== 'undefined' && value !== '') {
      let company_key_id = getCompanyId(this.props.companyDetail, value, false);
      console.log(company_key_id)
      this.setState({
        company_key_id: company_key_id
      })
      //this.props.qaCompanyDetail(company_key_id);
    }
  }

  async viewExportRebteDocument (){
    await this.props.getExportRebateDocumentByCompanyId(this.state.company_key_id)
  }

  render() {
    // strings.setLanguage(this.props.currentLanguage);
    const { selectCompany, companyType } = strings.setting;
    return (
      <div>
        <div
          className="form-row"
          style={{ paddingTop: "18px", paddingLeft: "14px" }}
        >
          <div className="form-group col-md-4">
            <Field
              className="select-menu"
              name="company_type"
              label={`${companyType} *`}
              component={renderCompanyType}
              data={
                !_.isEmpty(this.props.companyType)
                  ? this.props.companyType.map(function (list) {
                      return list.company_type_name;
                    })
                  : []
              }
              customProps={{
                fetchCompanyType: this.fetchCompanyType,
              }}
            />
          </div>
          <div className="form-group col-md-4">
            <Field
              className="select-menu"
              name="company_key_id"
              label={`${selectCompany} *`}
              component={renderDropdownList}
              data={
                !_.isEmpty(this.props.companyDetail)
                  ? this.props.companyDetail.map(function (list) {
                      return list.company_name;
                    })
                  : []
              }
              customProps={{
                fetchCompanyDetails: this.fetchCompanyDetails,
              }}
            />
          </div>
          <div className='btn-right' style={{ paddingLeft: '48px', paddingTop: '22px' }}>
            <Button onClick={()=> this.viewExportRebteDocument()}> {strings.qa.viewDoc}</Button>
          </div>
        </div>
      </div>
    );
  }
}

export default reduxForm({
  form: "ExportRebateBody",
})(ExportRebateBody);
