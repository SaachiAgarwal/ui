import React from "react";
import PropTypes from "prop-types";
import { Button } from "reactstrap";
import { reduxForm, Field, SubmissionError, FieldArray } from "redux-form";
import _ from "lodash";
import FileDownload from "js-file-download";

import SubmitButtons from "components/SubmitButtons";
import RenderTextArea from "components/RenderTextArea";

//localization
import LocalizedStrings from "react-localization";
import data from "../../../localization/data";

let strings = new LocalizedStrings(data);

export const validate = (values) => {
    const errors = {}
    // if (!values.message) {
    //   errors.message = 'Required'
    // }
    return errors
  }

export const CsvModalHeader = (props) => {
  return (
    <div className="headertext">
        <div>
              <h2 className="heading-model-new">{strings.declineModal.declineDocument}</h2>
              <h3 className="text1">{strings.declineModal.declineDocumentReason}</h3>
            </div>
      <Button
        className="modal-btn"
        onClick={() => props.handleDeclineModal({}, false)}
      >
        X
      </Button>
    </div>
  );
};

class DeclineModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div className="model_popup">
        <div>
          <CsvModalHeader handleDeclineModal={this.props.handleDecline} />
        </div>
        <div>
          <form onSubmit={this.props.handleSubmit}>
            <div>
              <Field
                name="message"
                className="form-control"
                component={RenderTextArea}
                placeholder={"Type here..."}
              />
            </div>
            <div>
                <SubmitButtons
                  submitLabel={strings.declineModal.declineDocument}
                  className="btn btn-primary create"
                  submitting={this.props.submitting}
                />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default reduxForm({
    form: 'DeclineModal',
    validate,
  })(DeclineModal)

