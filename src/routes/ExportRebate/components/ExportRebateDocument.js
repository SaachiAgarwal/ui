import React from "react";
import { Images } from 'config/Config';
import { Button } from "reactstrap";
import ExportRebateDocumentTable from "./ExportRebateDocumentTable";
import options from "components/options";

//localization
import LocalizedStrings from "react-localization";
import data from "../../../localization/data";
let strings = new LocalizedStrings(data);

class ExportRebateDocument extends React.Component{

    constructor(props){
        super(props);
        this.downloadDocument = this.downloadDocument.bind(this);
        this.state = {

        }
    }

    // async componentWillMount() {
    //     await this.props.getExportRebateDocument('2520');
    // }

    async downloadDocument(data){
        await this.props.downloadFile(data.document_file_name,data.document_name)
    }

    render(){
            // console.log('bill file',this.props.billFile);
            // console.log('invooice file',this.props.invoiceFile);
        return (
            <div style={{ paddingTop: "18px", paddingLeft: "14px",backgroundColor:'#fff' }}>
                <div style={{display:'grid',gridTemplateColumns:'1fr 1fr 1fr',gridGap:'10px',width:'95%',margin:'auto'}}>
                    <div style={{display:'grid',gridTemplateColumns:'1fr 1fr',marginTop:'10px'}}>
                        <div style={{fontWeight:'bold'}}>Bill of lading number</div>
                        <div>
                            {
                                this.props.billFile[0].document_number ? this.props.billFile[0].document_number : 'Sorry no bill number'

                            }
                        </div>
                    </div>
                    <div>
                        {
                            this.props.billFile.map(item=>{
                                return(
                                    <div style={{display:'flex',flexDirection:'column',alignItems:'center',cursor: "pointer"}}  onClick={() => this.downloadDocument(item)}>
                                        <img src={Images.pdfIcon} height="50px" width="50px" />
                                        <span style={{fontSize:'.8em'}}>Download</span>
                                    </div>
                                )
                            })
                        }
                        
                    </div>
                    <div style={{display:'grid',gridTemplateColumns:'1fr 1fr',marginTop:'10px'}}>
                        <div style={{fontWeight:'bold'}}>Fiber amount calculated</div>
                        <div>{this.props.fiberQty} kg</div>
                    </div>
                </div>
                <div style={{display:'grid',gridTemplateColumns:'1fr 1fr 1fr',gridGap:'10px',width:'95%',margin:'1em auto'}}>
                    <div style={{display:'grid',gridTemplateColumns:'1fr 1fr',marginTop:'10px'}}>
                        <div style={{fontWeight:'bold'}}>Invoice number</div>
                        <div style={{textAlign:'left'}}>
                            {
                                this.props.invoiceFile[0].document_number ? this.props.invoiceFile[0].document_number : 'Sorry no invoice number'
                            }
                        </div>
                    </div>
                    <div>
                    {
                            this.props.invoiceFile.map(item=>{
                                return(
                                    <div style={{display:'flex',flexDirection:'column',alignItems:'center',cursor: "pointer"}} onClick={() => this.downloadDocument(item)}>
                                        <img src={Images.pdfIcon} height="50px" width="50px" />
                                        <span style={{fontSize:'.8em'}}>Download</span>
                                    </div>
                                )
                            })
                        }
                    </div>
                    <div style={{display:'flex',justifyContent:'space-between',marginTop:'10px'}}>
                        {/* <div style={{fontWeight:'bold'}}>Fiber amount calculated</div>
                        <div>Bill number</div> */}
                    </div>
                </div>
                {
                    this.props.traceExportRebate &&
                    this.props.traceExportRebate.length > 0 &&
                    <div style={{width:'95%',margin:'1em auto',fontWeight:'bold'}}>
                    Audit trail
                </div>
                }
                
                <div>
                    {
                        this.props.traceExportRebate &&
                        this.props.traceExportRebate.length > 0 &&
                        <ExportRebateDocumentTable 
                        data={this.props.traceExportRebate}
                    pagination={false}
                    search={false}
                    exportCSV={false}
                    haveAction={false}
                    options={options}
                    currentLanguage={this.props.currentLanguage}
                        />
                    }
                </div>
                <div style={{display:'flex',justifyContent:'flex-end',padding:'10px',boxSizing:'border-box'}}>
                    <ButtonGroup>
                        <Button onClick={()=>this.props.acceptDocument()} style={{margin:'0 10px',width:'100px'}}>{strings.captalise.accept}</Button>
                        <Button onClick={() => this.props.handleDecline({},true)} style={{width:'100px'}}>{strings.captalise.decline}</Button>
                    </ButtonGroup>
                </div>
            </div>
        )
    }

}

export default ExportRebateDocument;