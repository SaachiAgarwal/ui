import { connect } from "react-redux";
import ExportRebate from  '../components/ExportRebate';


import {
    setToDate,
    setFromDate,
    setInnerNav,
    setParam,
    setSearchString,
    setReportFetching,
    setGlobalState,
    fetchUOM,
    uploadFile,
    handleUnreadItems,
    uploadCertFile,
    setCurrentLanguage,
    viewQaDetails,
    resetqaDataDetail,
  } from "../../../store/app";

  import {
    getCompanyType,
    getCompany,
    getAllExportRebateDocument,
    resetAlertBox,
    getExportRebateDocumentByCompanyId,
    getExportRebateDocument,
    downloadExportRebateDocument,
    setProps,
    acceptExportRebateDocument,
    declineExportRebateDocument,
    getFiberQty,
    getTraceForExportRebate
  } from "../modules/ExportRebate";


  const mapStateToProps = (state) => {
    return {
        showAlert:state.ExportRebate.showAlert,
        alertMessage:state.ExportRebate.alertMessage,
        status:state.ExportRebate.status,
        fetching:state.ExportRebate.fetching,
        error:state.ExportRebate.error,
        currentLanguage : state.app.currentLanguage,
        navIndex: state.app.navIndex,
        navInnerIndex: state.app.navInnerIndex,
        navClass: state.app.navClass,
        companyType:state.ExportRebate.companyType,
        companyDetail:state.ExportRebate.companyDetailVal,
        isActiveExportRebate:state.ExportRebate.isActiveExportRebate,
        billFile:state.ExportRebate.billFile,
        invoiceFile:state.ExportRebate.invoiceFile,
        exportRebateTable:state.ExportRebate.exportRebateTable,
        totalRecordCount: state.ExportRebate.totalRecordCount,
        isDecline:state.ExportRebate.isDecline,
        fiberQty:state.ExportRebate.fiberQty,
        traceExportRebate:state.ExportRebate.traceExportRebate,
    };
  };
  
  const mapDispatchToProps = (dispatch) => {
    return {
        setGlobalState: (value) => dispatch(setGlobalState(value)),
        setCurrentLanguage : (language) => dispatch(setCurrentLanguage(language)),
        setInnerNav: (lang) => dispatch(setInnerNav(lang)),
        setParam: (lang) => dispatch(setParam(lang)),
        setSearchString: (lang) => dispatch(setSearchString(lang)),
        setReportFetching: (lang) => dispatch(setReportFetching(lang)),
        getCompanyType: () => dispatch(getCompanyType()),
        getCompany: (company_type_id) => dispatch(getCompany(company_type_id)),
        getAllExportRebateDocument : () => dispatch(getAllExportRebateDocument()),
        resetAlertBox: (showAlert, message) => dispatch(resetAlertBox(showAlert, message)),
        getExportRebateDocumentByCompanyId: (company_key_id) => dispatch(getExportRebateDocumentByCompanyId(company_key_id)),
        getExportRebateDocument: (shipmentId,status) => dispatch(getExportRebateDocument(shipmentId,status)),
        downloadExportRebateDocument:(document_file_name,document_name) => dispatch(downloadExportRebateDocument(document_file_name,document_name)),
        setProps: (status,key) => dispatch(setProps(status,key)),
        acceptExportRebateDocument:(shipmentId) => dispatch(acceptExportRebateDocument(shipmentId)),
        declineExportRebateDocument: (shipmentId,message) => dispatch(declineExportRebateDocument(shipmentId,message)),
        getFiberQty: (shipmentId) => dispatch(getFiberQty(shipmentId)),
        getTraceForExportRebate: (shipmentId) => dispatch(getTraceForExportRebate(shipmentId)),
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(ExportRebate);
  