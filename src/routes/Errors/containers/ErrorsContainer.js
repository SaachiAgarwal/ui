/** ErrorsContainer
 *
 * @description This is container that manages the states for all order related functionality.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
import { connect } from 'react-redux';

import Errors from '../components/Errors';
import {resetAlertBox} from '../modules/errors.js';
import {setGlobalState, setCurrentLanguage} from '../../../store/app';

const mapStateToProps = (state) => {
  return({
    navIndex: state.app.navIndex,
    navInnerIndex: state.app.navInnerIndex,
    navClass: state.app.navClass,
    fetching: state.Errors.fetching,
    error: state.Errors.error,
    showAlert: state.Errors.showAlert,
    alertMessage: state.Errors.alertMessage,
    status: state.Errors.status,
    currentLanguage : state.app.currentLanguage
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    setGlobalState: (value) => dispatch(setGlobalState(value)),
    resetAlertBox: (showAlert, message) => dispatch(resetAlertBox(showAlert, message)),
    setCurrentLanguage : (language) => dispatch(setCurrentLanguage(language)),
  });
};

export default connect(mapStateToProps, mapDispatchToProps)(Errors);
