/** errors reducer
 *
 * @description This is a reducer responsible for all the create/delete/update/view/fulfill functionality
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
import axios from 'axios';
import { browserHistory, Router } from 'react-router';
// ------------------------------------
// Constant
// ------------------------------------

export const FETCHING = 'FETCHING';
export const ERROR_FETCHING = 'ERROR_FETCHING';
export const SET_ALERT_MESSAGE = 'SET_ALERT_MESSAGE';
// ------------------------------------
// Actions
// ------------------------------------

export function fetching(status) {
  return {
    type: FETCHING,
    fetching: status
  }
}

export function errorFetching(status) {
  return {
    type: ERROR_FETCHING,
    error: status
  }
}

export function setAlertMeassage(status, message, orderStatus=false) {
  return {
    type: SET_ALERT_MESSAGE,
    showAlert: status,
    alertMessage: message,
    status: orderStatus
  }
}
// ------------------------------------
// Action creators
// ------------------------------------

export const resetAlertBox = (showAlert, message) => {
  return (dispatch) => {
    dispatch(setAlertMeassage(showAlert, message));
  }
}

export const actions = {
};

// ------------------------------------
// Action handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [FETCHING]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching
    }
  },
  [ERROR_FETCHING]: (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  fetching: false,
  error: false,
  showAlert: false,
  alertMessage: '',
  status: false,
};

export default function errorsReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
