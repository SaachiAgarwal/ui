import { injectReducer } from '../../store/reducers';

export default (store) => ({
  path: 'error',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const Errors = require('./containers/ErrorsContainer').default;
      const reducer = require('./modules/errors').default;
      injectReducer(store, { key: 'Errors', reducer });
      cb(null, Errors);
  }, 'Errors');
  },
});
