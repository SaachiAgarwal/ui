import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './AuditTrail.scss';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);

class ShipmentProductTable extends Component {
  constructor(props) {
    super(props);

  }



  render() {
    const footerData = [
      [
        {
          label: 'Total',
          columnIndex: 0
        },
        {
          label: 'Total value',
          columnIndex: 2,
          align: 'right',
          formatter: (tableData) => {
            let label = 0;
            for (let i = 0, tableDataLen = tableData.length; i < tableDataLen; i++) {
              label += tableData[i].price;
            }
            return (
              <strong>{label}</strong>
            );
          }
        }
      ]
    ];
    strings.setLanguage(this.props.currentLanguage);
    const { seller, buyer, invoiceNumber, shipmentDate, receivedDate, shipmentStatus } = strings.shipmentTableText;
    const { product, quantity, unit } = strings.addProduct;
    return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options} >
          <TableHeaderColumn BootstrapTable dataField='asset_id' hidden={true} thStyle={{ whiteSpace: 'pre-wrap', backgroundColor: '#fff', fontSize: '12px', border: 'none', borderBottom: '3px solid #f6f7fb', paddingBottom: '0', paddingTop: 0, height: '22px', color: 'rgba(0,0,0,.7)' }} tdStyle={{ whiteSpace: 'pre-wrap' }}>Asset Id</TableHeaderColumn>
          <TableHeaderColumn dataField='seller_company_name' isKey={true} thStyle={{ textAlign: 'start', width: '100px', whiteSpace: 'pre-wrap', backgroundColor: '#fff', fontSize: '12px', border: 'none', borderBottom: '3px solid #f6f7fb', paddingBottom: '0', paddingTop: 0, height: '22px', color: 'rgba(0,0,0,.7)', border: 'none', color: 'rgba(0,0,0,.4)', fontSize: '14px', fontWeight: '500', lineHeight: '1', padding: '20px 8px' }} tdStyle={{ width: '100px', whiteSpace: 'pre-wrap', backgroundColor: '#fff', fontSize: '12px', border: 'none', borderBottom: '3px solid #f6f7fb', paddingBottom: '0', paddingTop: 0, height: '22px', border: 'none', textAlign: 'left!important', fontSize: '14px', fontWeight: '500', lineHeight: '1.5', padding: '20px 8px' }}>{seller}</TableHeaderColumn>
          <TableHeaderColumn dataField='buyer_company_name' thStyle={{ textAlign: 'start', width: '100px', whiteSpace: 'pre-wrap', backgroundColor: '#fff', fontSize: '12px', border: 'none', borderBottom: '3px solid #f6f7fb', paddingBottom: '0', paddingTop: 0, height: '22px', color: 'rgba(0,0,0,.7)', border: 'none', color: 'rgba(0,0,0,.4)', fontSize: '14px', fontWeight: '500', lineHeight: '1', padding: '20px 8px' }} tdStyle={{ width: '100px', whiteSpace: 'pre-wrap', backgroundColor: '#fff', fontSize: '12px', border: 'none', borderBottom: '3px solid #f6f7fb', paddingBottom: '0', paddingTop: 0, height: '22px', border: 'none', textAlign: 'left!important', fontSize: '14px', fontWeight: '500', lineHeight: '1.5', padding: '20px 8px' }}>{buyer}</TableHeaderColumn>
          <TableHeaderColumn dataField='invoice_number' thStyle={{ textAlign: 'start', width: '100px', whiteSpace: 'pre-wrap', backgroundColor: '#fff', fontSize: '12px', border: 'none', borderBottom: '3px solid #f6f7fb', paddingBottom: '0', paddingTop: 0, height: '22px', color: 'rgba(0,0,0,.7)', border: 'none', color: 'rgba(0,0,0,.4)', fontSize: '14px', fontWeight: '500', lineHeight: '1', padding: '20px 8px' }} tdStyle={{ width: '100px', whiteSpace: 'pre-wrap', backgroundColor: '#fff', fontSize: '12px', border: 'none', borderBottom: '3px solid #f6f7fb', paddingBottom: '0', paddingTop: 0, height: '22px', border: 'none', textAlign: 'left!important', fontSize: '14px', fontWeight: '500', lineHeight: '1.5', padding: '20px 8px' }}>{invoiceNumber}#</TableHeaderColumn>
          <TableHeaderColumn dataField='product_name' columnTitle={true} thStyle={{ textAlign: 'start', width: '100px', whiteSpace: 'pre-wrap', backgroundColor: '#fff', fontSize: '12px', border: 'none', borderBottom: '3px solid #f6f7fb', paddingBottom: '0', paddingTop: 0, height: '22px', color: 'rgba(0,0,0,.7)', border: 'none', color: 'rgba(0,0,0,.4)', fontSize: '14px', fontWeight: '500', lineHeight: '1', padding: '20px 8px' }} tdStyle={{ width: '100px', whiteSpace: 'pre-wrap', backgroundColor: '#fff', fontSize: '12px', border: 'none', borderBottom: '3px solid #f6f7fb', paddingBottom: '0', paddingTop: 0, height: '22px', border: 'none', textAlign: 'left!important', fontSize: '14px', fontWeight: '500', lineHeight: '1.5', padding: '20px 8px' }}>{product}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_qty' thStyle={{ textAlign: 'start', width: '100px', whiteSpace: 'pre-wrap', backgroundColor: '#fff', fontSize: '12px', border: 'none', borderBottom: '3px solid #f6f7fb', paddingBottom: '0', paddingTop: 0, height: '22px', color: 'rgba(0,0,0,.7)', border: 'none', color: 'rgba(0,0,0,.4)', fontSize: '14px', fontWeight: '500', lineHeight: '1', padding: '20px 8px' }} tdStyle={{ width: '100px', whiteSpace: 'pre-wrap', textAlign: "right", backgroundColor: '#fff', fontSize: '12px', border: 'none', borderBottom: '3px solid #f6f7fb', paddingBottom: '0', paddingTop: 0, height: '22px', border: 'none', fontSize: '14px', fontWeight: '500', lineHeight: '1.5', padding: '20px 8px' }}>{quantity}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_uom' thStyle={{ textAlign: 'start', width: '100px', whiteSpace: 'pre-wrap', backgroundColor: '#fff', fontSize: '12px', border: 'none', borderBottom: '3px solid #f6f7fb', paddingBottom: '0', paddingTop: 0, height: '22px', color: 'rgba(0,0,0,.7)', border: 'none', color: 'rgba(0,0,0,.4)', fontSize: '14px', fontWeight: '500', lineHeight: '1', padding: '20px 8px' }} tdStyle={{ width: '100px', whiteSpace: 'pre-wrap', backgroundColor: '#fff', fontSize: '12px', border: 'none', borderBottom: '3px solid #f6f7fb', paddingBottom: '0', paddingTop: 0, height: '22px', border: 'none', textAlign: 'left!important', fontSize: '14px', fontWeight: '500', lineHeight: '1.5', padding: '20px 8px' }}>{unit}</TableHeaderColumn>
          <TableHeaderColumn dataField='new_shipment_date' thStyle={{ textAlign: 'start', width: '100px', whiteSpace: 'pre-wrap', backgroundColor: '#fff', fontSize: '12px', border: 'none', borderBottom: '3px solid #f6f7fb', paddingBottom: '0', paddingTop: 0, height: '22px', color: 'rgba(0,0,0,.7)', border: 'none', color: 'rgba(0,0,0,.4)', fontSize: '14px', fontWeight: '500', lineHeight: '1', padding: '20px 8px' }} tdStyle={{ width: '100px', whiteSpace: 'pre-wrap', backgroundColor: '#fff', fontSize: '12px', border: 'none', borderBottom: '3px solid #f6f7fb', paddingBottom: '0', paddingTop: 0, height: '22px', border: 'none', textAlign: 'left!important', fontSize: '14px', fontWeight: '500', lineHeight: '1.5', padding: '20px 8px' }}>{shipmentDate}</TableHeaderColumn>
          <TableHeaderColumn dataField='new_invoice_date' thStyle={{ textAlign: 'start', width: '100px', whiteSpace: 'pre-wrap', backgroundColor: '#fff', fontSize: '12px', border: 'none', borderBottom: '3px solid #f6f7fb', paddingBottom: '0', paddingTop: 0, height: '22px', color: 'rgba(0,0,0,.7)', border: 'none', color: 'rgba(0,0,0,.4)', fontSize: '14px', fontWeight: '500', lineHeight: '1', padding: '20px 8px' }} tdStyle={{ width: '100px', whiteSpace: 'pre-wrap', backgroundColor: '#fff', fontSize: '12px', border: 'none', borderBottom: '3px solid #f6f7fb', paddingBottom: '0', paddingTop: 0, height: '22px', border: 'none', textAlign: 'left!important', fontSize: '14px', fontWeight: '500', lineHeight: '1.5', padding: '20px 8px' }}>{receivedDate}</TableHeaderColumn>
          <TableHeaderColumn dataField='status' hidden={true} tdStyle={{ whiteSpace: 'pre-wrap' }}>{shipmentStatus}</TableHeaderColumn>
          {/* <TableHeaderColumn key="editAction" dataFormat={this.props.auditFormatter} tdStyle={{ whiteSpace: 'pre-wrap'}}></TableHeaderColumn> */}
        </BootstrapTable>
     
      </div>
    );
  }
}

ShipmentProductTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default ShipmentProductTable;