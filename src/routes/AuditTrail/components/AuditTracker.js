import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ShipmentDetailTable from './ShipmentDetailTable';
import ProductDetailTable from './ProductDetailTable';
import ShipmentProductTable from './ShipmentProductTable'
import Tracker from './Tracker';
import Footer from 'components/Footer';
import HeaderSection from 'components/HeaderSection';
import { filterDataByCompanyType, getDate, filterDataByCompanyTypePdf } from 'components/Helper';
import { GoogleApiWrapper, InfoWindow, Map, Marker, Polyline } from 'google-maps-react';
import Geocode from "react-geocode";
import GoogleMapModule from './GoogleMapModule'
//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
import MapLeaflet from './MapLeaflet'

let strings = new LocalizedStrings(
  data
);


export const AuditMaker = (props) => {
  return (
    <div>
      {(!props.isPublicCall) &&
        <Tracker
          blockchainData={props.blockchainData}
          blockchainCompleted={props.blockchainCompleted}// used to show active status on Brand
          shipmentDetail={props.shipmentDetail}
          getDetail={props.getDetail}
          currentLanguage={props.currentLanguage}
          orderAuditVal={props.orderAuditVal}
          onHandleScroll={props.onHandleScroll}
          fetchingAudit={props.fetchingAudit}
          companyKeyDoc={props.companyKeyDoc}
          docData={props.docData}
          downloadDoc={props.downloadDoc}
        />
      }
      {(!_.isEmpty(props.shipmentDetail)) &&
        <div>

          {(!_.isEmpty(props.configData)) &&
            <div>
              {
                ((!_.isEmpty(props.asset_id)) && props.configData.data.sd_MoT_col === 0 && props.configData.data.sd_buyer_col === 0
                  && props.configData.data.sd_invoice_number_col === 0 && props.configData.data.sd_received_date_col === 0
                  && props.configData.data.sd_seller_col === 0 && props.configData.data.sd_shipment_date_col === 0)
                  ? '' :
                  <div>
                    {(!props.isPublicCall) &&
                      <div>
                        {!props.orderAuditVal ? <h2>{strings.createShipment.shipmentDetail}</h2>: <h2>{strings.createShipment.orderDetails}</h2>}
                        <ShipmentDetailTable
                          data={props.shipmentDetail}
                          pagination={true}
                          search={false}
                          exportCSV={false}
                          options={props.options}
                          isForModal={false}
                          auditFormatter={props.auditFormatter}
                          currentLanguage={props.currentLanguage}
                          asset_id={props.asset_id}
                          configData={props.configData}
                          orderAuditVal={props.orderAuditVal}
                        />
                      </div>
                    }
                  </div>
              }
            </div>

          }
          {(!_.isEmpty(props.configData)) &&
            <div>
              {
                ((!_.isEmpty(props.asset_id)) && props.configData.data.pd_description_col === 0 && props.configData.data.pd_product_col === 0
                  && props.configData.data.pd_quantity_col === 0 && props.configData.data.pd_unit_col === 0)
                  ? '' :

                  <div>
                    {(!props.isPublicCall) && (!props.orderAuditVal) &&
                      <div>
                        <h2>{strings.addProduct.productDetail}</h2>
                        <ProductDetailTable
                          data={props.getProducts()}
                          pagination={true}
                          search={false}
                          exportCSV={false}
                          options={props.options}
                          isForModal={false}
                          auditFormatter={props.auditFormatter}
                          asset_id={props.asset_id}
                          configData={props.configData}
                          currentLanguage={props.currentLanguage}
                        />
                      </div>
                    }
                  </div>
              }
            </div>
          }

          <div>
            { !props.orderAuditVal && <h2>{strings.addProduct.geoDetail}</h2>}
            {(!_.isEmpty(props.geolocationData)) && !props.orderAuditVal &&
              // <GoogleMapModule
              // geolocationData={props.geolocationData}
              // resetGeolocationData={props.resetGeolocationData}
              // configData={props.configData}
              // asset_id={props.asset_id}
              // isPublicCall={props.isPublicCall}
              // />
              <MapLeaflet
                geolocationData={props.geolocationData}
                resetGeolocationData={props.resetGeolocationData}
                configData={props.configData}
                asset_id={props.asset_id}
                isPublicCall={props.isPublicCall}
              />
            }

          </div>
        </div>
      }
    </div>
  );
}

export const PdfMaker = (props) => {

  let spDetails = [],
    sender = '',
    buyer = '',
    pdfElmCollection = [];
  props.blockchainCompletedPdf.map(function (element) {
    if (element !== 'Brand') {
      filterDataByCompanyTypePdf(props.blockchainData, element).map(function (ele) {
        for (let i = 0; i < ele.product.length; i++) {
          spDetails.push(ele.product[i])
          for (let key in ele) {
            if (key !== "product") {
              spDetails[0][key] = ele[key]
            }
          }
          pdfElmCollection.push(spDetails[0]);
          spDetails.length = 0;
        }
      })
    }
  })
  console.log(pdfElmCollection);
  return (
    <div>
      <HeaderSection
        active={props.active}
        orderNumber={props.orderNumber}
        isPdfDownload={props.isPdfDownload}
        currentLanguage={props.currentLanguage}
        setCurrentLanguage={props.setCurrentLanguage}
      />
      <Tracker
        blockchainData={props.blockchainData}
        blockchainCompleted={props.blockchainCompleted}// used to show active status on Brand
        shipmentDetail={props.shipmentDetail}
        getDetail={props.getDetail}
        isPdfDownload={props.isPdfDownload}
        sender={sender}
        buyer={buyer}
        setCurrentLanguage={props.setCurrentLanguage}
        currentLanguage={props.currentLanguage}
        onHandleScroll={props.onHandleScroll}
        
      />
      {(!_.isEmpty(props.shipmentDetail)) &&
        <div>
          <ShipmentProductTable
            data={pdfElmCollection}
            pagination={false}
            search={false}
            exportCSV={false}
            options={props.options}
            isForModal={false}
            currentLanguage={props.currentLanguage}
          />
          {/* {pdfElmCollection.map((item, index) =>
            (<div>
              <table className="table">
                <tbody>
                  { index === 0 &&
                  <tr>
                    <th>Seller Company Name</th>
                    <th>Buyer Company Name</th>
                  </tr>
                  }
                  <tr>
                    <td  style={{color: "black"}}>
                      {item.seller_company_bu_name}
                    </td>
                    <td style={{ color: "black"}}>
                      {item.product_name}
                    </td>

                  </tr>
                </tbody>
              </table>
            </div>
            ))
          } */}

        </div>
      }
    </div>
  );
}

class AuditTracker extends React.Component {
  constructor(props) {
    super(props);
    this.getProducts = this.getProducts.bind(this);
    this.isBlockchainCompleted = this.isBlockchainCompleted.bind(this);
    this.onHandleScroll = this.onHandleScroll.bind(this)

    this.state = {
      blockchainCompleted: [],
      blockchainCompletedPdf: []
    }
  }
  componentWillMount() {
    this.isBlockchainCompleted();
  }
  getProducts(sDetail = []) {
    if(!this.props.orderAuditVal && (!_.isEmpty(this.props.shipmentDetail[0].asset_id))){
      let products = [];
      let data = (this.props.isPdfDownload) ? sDetail : this.props.shipmentDetail;
      data.map((item, index) => {
        item.product.map((prd, ind) => {
          prd.index = products.length + 1;
          products.push(prd);
        });
      });
  
      return products;
    }
  
  }

  async isBlockchainCompleted() {
    let data = [],
      dataNew=[],
      pdfNodes = [],
      appendData = []


    await this.setState({ blockchainCompleted: data }); // resetting previous set state
    if(!this.props.orderAuditVal){
      this.props.blockchainData.map((item) => {
        // if (data.indexOf(item.seller_company_type_name) > 0 && item.seller_company_type_name == "Integrated Player") data.push(item.seller_company_type_name);
        // if (data.indexOf(item.seller_company_type_name) < 0) data.push(item.seller_company_type_name);
        
        if (data.indexOf(item.seller_company_type_name) < 0)
        {
          data.push(item.seller_company_type_name);
          if(item.seller_company_name){
            dataNew.push(item.seller_company_name)
          }
          else{
            dataNew.push('')
          }
         
        }
        else if(item.seller_company_type_name === "Integrated Player" && dataNew.indexOf(item.seller_company_name) < 0){
          data.push(item.seller_company_type_name);
          dataNew.push(item.seller_company_name);
        }
    
        
        if (pdfNodes.indexOf(item.seller_company_type_name) < 0 && item.seller_company_type_name !== 'Brand') pdfNodes.push(item.seller_company_type_name);
      });

      console.log(dataNew);
      for(var i = 0; i<data.length; i++){
        appendData.push({seller_company_type_name: data[i], seller_company_name: dataNew[i]})
      }
      console.log(appendData);
      await this.setState({ blockchainCompleted: appendData });
      await this.setState({ blockchainCompletedPdf: data})
      this.props.setPdfNodes(pdfNodes);
    }
    else{
      
      this.props.blockchainData.map((item) => {
        if (data.indexOf(item.order_company_type_name) < 0)
        {
          data.push(item.order_company_type_name);
        }
        else if(item.order_company_type_name === "Integrated Player"){
          data.push(item.order_company_type_name);
        }
    
        if (pdfNodes.indexOf(item.order_company_type_name) < 0 && item.order_company_type_name !== 'Brand') pdfNodes.push(item.order_company_type_name);
      });
      await this.setState({ blockchainCompleted: data });
      this.props.setPdfNodes(pdfNodes);
      console.log("In Order Audit");
    }
    
  }
  onHandleScroll() {
    let temp = ''
    var z = 10
   
    temp = document.getElementById('shipDetail')
   
  
    console.log(temp.getBoundingClientRect().y)
    var x = temp.getBoundingClientRect().x
    var y = temp.getBoundingClientRect().y
 
    window.scrollBy({
      top: y-100,
      left: 0,
      behavior: "smooth"
    })
    
  }

  render() {
    strings.setLanguage(this.props.currentLanguage);
    // if (!_.isEmpty(this.props.shipmentDetail) && !this.props.orderAuditVal) {
    //   console.log(this.props.shipmentDetail)
    //   this.props.shipmentDetail.map((item) => {
    //     item.shipment_date = item.shipment_date.toString().split('T')[0];
    //     item.invoice_date = item.invoice_date.toString().split('T')[0];
    //   });
    // }
    return (
      <div id={'startFromHere'}>
        {(this.props.isPdfDownload) ?
          <PdfMaker
            isPrint={this.props.isPrint}
            blockchainData={this.props.blockchainData}
            blockchainCompleted={this.state.blockchainCompleted}// used to show active status on Brand
            blockchainCompletedPdf={this.state.blockchainCompletedPdf}
            shipmentDetail={this.props.shipmentDetail}
            getDetail={this.props.getDetail}
            options={this.props.options}
            getProducts={this.getProducts}
            active={this.props.active}
            orderNumber={this.props.orderNumber}
            isPdfDownload={this.props.isPdfDownload}
            currentLanguage={this.props.currentLanguage}
            setCurrentLanguage={this.props.setCurrentLanguage}
          />
          :
          <AuditMaker
            blockchainData={this.props.blockchainData}
            blockchainCompleted={this.state.blockchainCompleted}// used to show active status on Brand
            shipmentDetail={this.props.shipmentDetail}
            getDetail={this.props.getDetail}
            options={this.props.options}
            getProducts={this.getProducts}
            asset_id={this.props.asset_id}
            geolocationData={this.props.geolocationData}
            resetGeolocationData={this.props.resetGeolocationData}
            configData={this.props.configData}
            currentLanguage={this.props.currentLanguage}
            isPublicCall={this.props.isPublicCall}
            orderAuditVal = {this.props.orderAuditVal}
            onHandleScroll = {this.onHandleScroll}
            fetchingAudit={this.props.fetchingAudit}
            companyKeyDoc={this.props.companyKeyDoc}
            docData={this.props.docData}
            downloadDoc={this.props.downloadDoc}
          />
        }
      </div>
    );
  }
}

AuditTracker.propTypes = {

}

export default AuditTracker;