import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';

import { getChecked } from 'components/Helper';
//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

import { Graph } from "react-d3-graph";

import Tree from 'react-d3-tree';
import { Images } from 'config/Config';
let strings = new LocalizedStrings(
  data
);


const myConfig = {
  nodeHighlightBehavior: true,

  directed: true,
  staticGraph: true,
  width: "1000px",
  height: '400px',
  maxZoom: '1.0',
  minZoom: '1.0',
  node: {
    color: "blue",
    size: 250,
    highlightStrokeColor: "darkBlue",
    fontSize: 12,
    highlightFontSize: 14
  },
  link: {
    highlightColor: "red",
    color: "#007bffa1",
    fontSize: 10
  },
};




class Tracker extends React.Component {
  constructor(props) {
    super(props);

    this.makeDAG = this.makeDAG.bind(this)
    this.onClickNode = this.onClickNode.bind(this)
    this.state = {
      tree: [],
      dataDag: '',
      nodesLength: ''
    }
  }
  componentDidMount() {
    console.log(this.props.blockchainCompleted);
    console.log(this.props.blockchainData);
    //this.makeTreeData();
    this.makeDAG();
  }

  async makeDAG() {
    const data_acy = {
      nodes: [{ id: "Brand", x: "100", y: "100" }, { id: "Spg1", x: "120", y: "120" }, { id: "Spg2", x: "140", y: "140" }, { id: "Gfm", x: "160", y: "160" }, { id: "BC", x: "180", y: "180" }],
      links: [{ source: "Brand", target: "Spg1" }, { source: "Brand", target: "Spg2" }, { source: "Spg2", target: "Gfm" }, { source: "Spg1", target: "Gfm" }, { source: "Gfm", target: "BC" }],

    };

    let nodes = []
    let links = []
    let nodesX = []
    let nodesY = []
    let nodesXY = []
    let nodesXSort = []
    this.props.blockchainData.map((val, index) => {
      if ((!_.isEmpty(val.asset_id))) {
        nodes.push({ id: val.seller_company_type_name + '[' + val.seller_company_name + ']' }, { id: val.buyer_company_type_name + '[' + val.buyer_company_name + ']' })
        links[index] = { source: val.seller_company_type_name + '[' + val.seller_company_name + ']', target: val.buyer_company_type_name + '[' + val.buyer_company_name + ']' }
      }
    })
    let unique_nodes = nodes
      .map(e => e['id'])

      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)

      // eliminate the dead keys & store unique objects
      .filter(e => nodes[e]).map(e => nodes[e]);


    // console.log(data_acy)
    // console.log(nodes)
    // console.log(unique_nodes)
    // console.log(links)

    // let dataDag = {
    //   nodes: unique_nodes,
    //   links: links
    // }
    for (var i = 0; i < unique_nodes.length; i++) {
      nodesX[i] = (i + 1) * 100;
      //nodesY[i] = Math.floor((Math.random() * 300) + 50);
      if (i % 2 === 0 || i == 0) {
        nodesY[i] = 100 + i * 20
      }
      else if (i === 1) {
        nodesY[i] = 200 + i * 20
      }
      else {
        nodesY[i] = 200 + i * 20
      }
    }

    //nodesXSort = nodesX.sort( function(a,b) { return a - b } );
    for (var i = 0; i < unique_nodes.length; i++) {
      nodesXY.push({ id: unique_nodes[i].id, x: nodesX[i], y: nodesY[i] })
    }
    let dataDag = {
      nodes: nodesXY,
      links: links
    }

    //console.log(nodesXSort)
    console.log(nodesY)
    console.log(dataDag)
    console.log(nodesXY)
    console.log(dataDag.nodes.length),
    this.setState({
      dataDag: dataDag,
      nodesLength: dataDag.nodes.length
    })
    
  }


  componentWillMount() {
    this.setState({
      dataDag: ''
    })
  }

  async onClickNode(props) {
    debugger
    console.log("In node")
    console.log(props);
    let item = props.split('[')
    console.log(item)
    console.log(item[0])
  
    // if (item[0] !== "Brand" && getChecked(this.props.blockchainData, item[0])) {
    //   let company_name = item[1].split(']')
    //   // console.log((company_name))
    //   this.props.getDetail(item[0], this.props.blockchainData, company_name[0], this.props.blockchainCompleted)

    // }
    await this.props.fetchingAudit(true)
    setTimeout(
      function () {
        if (item[0] !== "Brand" && getChecked(this.props.blockchainData, item[0])) {
          let company_name = item[1].split(']')
          // console.log((company_name))
          this.props.getDetail(item[0], this.props.blockchainData, company_name[0], this.props.blockchainCompleted)
    
        }
        this.props.fetchingAudit(false)
      }
        .bind(this),
      300
    );
    
  }

  
  render() {

    strings.setLanguage(this.props.currentLanguage);
    const { details } = strings.shipmentTableText;
    console.log(this.props.blockchainCompleted);
    return (
      <div>


        {(!this.props.isPdfDownload) ?
          <div>
            {this.props.orderAuditVal &&
              // <div className="col-md-12 col-lg-12 order-staps">
              //   {this.props.blockchainCompleted.map((item, index) => {
              //     return (
              //       <div className="staps" key={index}>
              //         <div>
              //           <label>{item.seller_company_type_name}</label>
              //           <span className="checked active"></span>
              //           {(!_.isEmpty(this.props.blockchainData) && (getChecked(this.props.blockchainData, item.seller_company_type_name)) && item.seller_company_type_name !== 'Brand') && <Button className="details" onClick={() => this.props.getDetail(item.seller_company_type_name, this.props.blockchainData, item.seller_company_name, this.props.blockchainCompleted)}>{details}</Button>}
              //         </div>
              //       </div>
              //     );
              //   })}
              // </div>
              
              <div className="col-md-12 col-lg-12 order-staps">
                {this.props.blockchainCompleted.map((item, index) => {
                  return (
                    <div className="staps" key={index}>
                      <div>
                        <label>{item}</label>
                        <span className="checked active"></span>
                        {(!_.isEmpty(this.props.blockchainData) && (getChecked(this.props.blockchainData, item)) && item !== 'Brand') && <Button className="details" onClick={() => this.props.getDetail(item, this.props.blockchainData, this.props.blockchainCompleted)}>{details}</Button>}
                      </div>
                    </div>
                  );
                })}
              </div>

            }
          </div>
          :
          <div className="checked active">
            {/* <p>{`${this.props.sender} ====================================> ${this.props.buyer}`}</p> */}
          </div>
        }
        {this.state.dataDag && !this.props.orderAuditVal && (!this.props.isPdfDownload) &&
          <div className="col-md-12 col-lg-12 order-staps">
            <h2>Audit Trail Graph</h2>
            {
              this.props.companyKeyDoc !== '' && this.props.companyKeyDoc !== undefined && this.props.docData !== undefined && this.props.docData !== '' && this.props.docData.length !== 0 &&
              <div>
                <label style={{position: 'absolute', right: '61px'}}>List of forest entities</label>
                <img src={Images.excelImage} onClick={() => this.props.downloadDoc(this.props.docData[0].document_file_name, this.props.docData[0].document_name)} style={{ position: 'absolute', top: '114px', right: '100px', width: '5%', cursor: 'pointer' }} />
              </div>

            }
            <p style={{fontSize: '12px', fontWeight: 'bold', fontFamily: 'Lemonada'}}>**Hover on Nodes to see Shipment and Product Details.</p>
            {/* <p style={{fontSize: '12px', fontWeight: 'bold', fontFamily: 'Lemonada'}}>**Scroll in/out for zoom in/out.</p> */}
            <div style={this.state.nodesLength < 8 ? {marginBottom: '-100px'}: {marginBottom: '0px'}}>
            <Graph
              id="graph-id" // id is mandatory, if no id is defined rd3g will throw an error
              data={this.state.dataDag}
              config={myConfig}
              directed={true}
              collapsible={true}
              staticGraph={true}
              onClickNode={this.onClickNode}
              // onRightClickNode={onRightClickNode}
              // onClickGraph={onClickGraph}
              // onClickLink={onClickLink}
              // onRightClickLink={onRightClickLink}
              onMouseOverNode={this.onClickNode}
            // onMouseOutNode={onMouseOutNode}
            // onMouseOverLink={onMouseOverLink}
            // onMouseOutLink={onMouseOutLink}
            // onNodePositionChange={onNodePositionChange}
            />
            </div>
          </div>
        }
      </div>
    );
  }
}

Tracker.propTypes = {

}

export default Tracker;