import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);


/** ProductDetailTable
 *
 * @description This class is responsible to display records for audit trail
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class ProductDetailTable extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {currentLanguage} = this.props;
    strings.setLanguage(currentLanguage);
    const {sNo, product, category, type, quantity, unit, lot, } = strings.addProduct;
    const {description} = strings.createOrder;
    return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options}>
          <TableHeaderColumn dataField='product_id' hidden={true}>Id</TableHeaderColumn>
          <TableHeaderColumn dataField='index' isKey={true} autoValue={true}>{sNo}</TableHeaderColumn>
          {((!_.isEmpty(this.props.asset_id))&& this.props.configData.data.pd_product_col === 0) ? false :<TableHeaderColumn dataField='product_name' columnTitle={true}>{product}</TableHeaderColumn>}
          <TableHeaderColumn dataField='category' hidden={true}>{category}</TableHeaderColumn>
          <TableHeaderColumn dataField='type' hidden={true}>{type}</TableHeaderColumn>
          {((!_.isEmpty(this.props.asset_id))&& this.props.configData.data.pd_quantity_col === 0) ? false : <TableHeaderColumn dataField='product_qty'>{quantity}</TableHeaderColumn>}
          {((!_.isEmpty(this.props.asset_id))&& this.props.configData.data.pd_unit_col === 0) ? false : <TableHeaderColumn dataField='product_uom'>{unit}</TableHeaderColumn>}
          {/* <TableHeaderColumn dataField='blend_percentage'>Blend %</TableHeaderColumn> */}
          <TableHeaderColumn dataField='lot' hidden={true}>{lot}#</TableHeaderColumn>
          {((!_.isEmpty(this.props.asset_id))&& this.props.configData.data.pd_description_col === 0) ? false : <TableHeaderColumn dataField='product_description' columnTitle={true}>{description}</TableHeaderColumn>}
          <TableHeaderColumn key="editAction" dataFormat={this.props.auditFormatter}></TableHeaderColumn>
        </BootstrapTable>
      </div>        
    );
  }
}

ProductDetailTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default ProductDetailTable;