import React, { useRef, useEffect } from 'react'
import { Map as LeafletMap, TileLayer, Marker, Polyline, withLeaflet, Popup } from 'react-leaflet';
import L from "leaflet";
import "leaflet-polylinedecorator";
import DivIcon from 'react-leaflet-div-icon';
import { Images } from 'config/Config';
import ReactDOM from 'react-dom'


class Map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      marker: [
        { lat: 28.7041, lng: 77.1025 }, // Delhi
        { lat: 28.7306, lng: 77.7759 }, // Hapur
        { lat: 28.4595, lng: 77.0266 }, //Gurugram
        { lat: 28.4089, lng: 77.3178 }, // faridabad
        { lat: 28.4215, lng: 78.0195 }, // Bulandsher
      ],
      demo_marker: [
        [
          [28.7041, 77.1025],
          [28.7306, 77.7759]
        ],
        [
          [28.7041, 77.1025],
          [28.4595, 77.0266]
        ]

      ],
      newData: '',
      selectedPlace: '',
      activeMarker: '',
      showingInfoWindow: false,

      iconVar: 1,



      company_name: [],
      buyer_company_name: '',
      Invoice_number: '',
      shipment_transport_mode: '',
      shipment_date: '',
      receive_data: '',
      product_name: '',
      product_qty: '',
      product_uom: '',
      product_description: '',
      product: [],
      first_lat: '',
      first_lng: '',
      mergeArray: [],
      prodArray: [],
      sellerDuplicate: 1,
      sellerDuplicateArray: []

    }
    this.onMarkerClick = this.onMarkerClick.bind(this)
    this.onMapClick = this.onMapClick.bind(this)
    this.onTagClick = this.onTagClick.bind(this)
  }

  onTagClick(e) {
    if (e.length !== undefined && e.length > 1) {
      var s2 = e.substr(1);
      console.log("in Tag")
      if (!_.isEmpty(e)) {
        var k = document.getElementById(e)
        console.log(k.innerHTML)
        k.innerHTML = '<td>' + s2 + '</td>'
        // k.appendChild("<p>Hello</p>")
      }
    }

  }

  onMarkerClick = (props, marker, e) => {
    this.setState({
      showingInfoWindow: false,
      activeMarker: null
    });
    // console.log(marker)
    // console.log(props)
    // console.log(props.latlng)
    // console.log(this.props.geolocationData)
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    });




    console.log(this.props.geolocationData)
    let length = this.props.geolocationData.length
    let abc = this
    let temp = 0;


    let indexMult = 0;
    let invMult = 0
    let mergeMult = 0
    let prodMult = 0
    let testCompanySellerMult = []
    let arrayInvMult = []
    let mergeArray = []
    let productArray = []
    let sellerDuplicate = []


    this.props.geolocationData.forEach(function (a, index) {
      // debugger
      if (index < length) {
        if (a.seller_lat === props.latlng.lat && a.seller_lng === props.latlng.lng) {
          temp = 1
          // console.log(temp)
          // console.log(a);
          if (a.Invoice_date !== null) {
            var spdate = (a.Invoice_date.split("T"))[0].split("-");
            a.Invoice_date = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
          }

          if (a.shipment_date !== null) {
            var new_spdate = (a.shipment_date.split("T"))[0].split("-");
            a.shipment_date = new_spdate[2] + "-" + new_spdate[1] + "-" + new_spdate[0];
          }
          if (a.received_date !== null) {
            var new_spdate1 = (a.received_date.split("T"))[0].split("-");
            a.received_date = new_spdate1[2] + "-" + new_spdate1[1] + "-" + new_spdate1[0];
          }

          let nameArray = []
          a.product.forEach((test, index) => {
            nameArray[index] = test.product_name
          })
          // console.log(nameArray)

          testCompanySellerMult[indexMult++] = a.buyer_company_name
          arrayInvMult[invMult++] = a.Invoice_number
          console.log(testCompanySellerMult)


          //Create Table

          mergeArray[mergeMult++] = { seller_company_name: a.seller_company_name, buyer_company_name: a.buyer_company_name, Invoice_number: a.Invoice_number, shipment_transport_mode: a.shipment_transport_mode, shipment_date: a.Invoice_date, receive_data: a.received_date }


          if (sellerDuplicate.indexOf(a.seller_company_name) === -1) sellerDuplicate.push(a.seller_company_name);

          console.log(mergeArray)
          console.log(sellerDuplicate.length)
          console.log(sellerDuplicate)
          a.product.map((val, index) => {
            productArray[prodMult++] = {
              product_name: val.product_name,
              product_qty: val.product_qty,
              product_uom: val.product_uom,
              product_description: val.product_description,
              seller_company_name: a.seller_company_name
            }
          })
          console.log(productArray);

          abc.setState({
            company_name: a.seller_company_name,
            buyer_company_name: testCompanySellerMult,
            Invoice_number: arrayInvMult,
            shipment_transport_mode: a.shipment_transport_mode,
            shipment_date: a.shipment_date,
            receive_data: a.Invoice_date,

            product_name: a.product_name,
            product_qty: a.product_qty,
            product_uom: a.product_uom,
            product_description: a.product_description,
            product: a.product,
            mergeArray: mergeArray,
            prodArray: productArray,
            sellerDuplicate: sellerDuplicate.length,
            sellerDuplicateArray: sellerDuplicate
          })
          //console.log(a.shipment_date);
        }
      }
    })
    if (temp === 0) {
      this.props.geolocationData.forEach(function (a, index) {
        if (index < length) {
          if (a.buyer_lat === props.latlng.lat && a.buyer_lng === props.latlng.lng) {

            // console.log(a);
            if (a.Invoice_date !== null) {
              var spdate = (a.Invoice_date.split("T"))[0].split("-");
              a.Invoice_date = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
            }

            if (a.shipment_date !== null) {
              var new_spdate = (a.shipment_date.split("T"))[0].split("-");
              a.shipment_date = new_spdate[2] + "-" + new_spdate[1] + "-" + new_spdate[0];
            }
            if (a.received_date !== null) {
              var new_spdate1 = (a.received_date.split("T"))[0].split("-");
              a.received_date = new_spdate1[2] + "-" + new_spdate1[1] + "-" + new_spdate1[0];
            }
            let nameArray = []
            a.product.forEach((test, index) => {
              nameArray[index] = test.product_name
            })
            // console.log(temp)
            // console.log(a.buyer_company_name);
            if (sellerDuplicate.indexOf(a.seller_company_name) === -1) sellerDuplicate.push(a.seller_company_name);
            console.log(sellerDuplicate)
            abc.setState({
              company_name: a.buyer_company_name,

              buyer_company_name: '',
              Invoice_number: '',
              shipment_transport_mode: '',
              shipment_date: '',
              receive_data: '',

              product_name: '',
              product_qty: '',
              product_uom: '',
              product_description: '',
              product: '',
              mergeArray: '',
              prodArray: '',
              sellerDuplicate: sellerDuplicate.length,
              sellerDuplicateArray: sellerDuplicate
            })
          }
        }
      })

    }
  }

  componentWillMount() {
    debugger
    console.log(this.props.geolocationData)
    this.getData()
    const abc = this
  }

  onMapClick = (props) => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      });
    }
  }


  getData() {
    //Branching Start
    let reverse = this.props.geolocationData.reverse();
    let tempArray = {}
    reverse.map((val, index) => {
      val.flag = 0
    })
    reverse.map((val, index) => {
      let temp = 0
      if (val.seller_lat !== tempArray.seller_lat || tempArray === '') {
        tempArray = { seller_lat: val.seller_lat, buyer_lat: val.buyer_lat,seller_company_name: val.seller_company_name }
        for (var i = index + 1; i < this.props.geolocationData.length; i++) {

          if (tempArray.seller_company_name === reverse[i].seller_company_name && tempArray.seller_lat === reverse[i].seller_lat && tempArray.buyer_lat === reverse[i].buyer_lat) {
            reverse[i].flag = temp
          }
          else if (tempArray.seller_company_name === reverse[i].seller_company_name && tempArray.seller_lat === reverse[i].seller_lat && tempArray.buyer_lat !== reverse[i].buyer_lat) {
            reverse[i].flag = ++temp
          }
        }
      }
    })
    console.log(reverse)

    let reverseReverse = this.props.geolocationData.reverse()
    console.log(reverseReverse)
    //Branching End

    const abc = this
    let initData = {}
    let newData = []
    let demoo = []
    let test = []
    let marker = []
    let i = 0;
    let j = 0;
    let m = 0;
    let first_lat = '';
    let first_lng = '';
    let company;
    let k;
    let length = this.props.geolocationData.length
    this.props.geolocationData.forEach(function (a, index) {

      let array = { ...initData }
      let p = {}
      //debugger
      if (index < length) {
        if (a.buyer_company_type_name !== "Brand") {
          newData[i++] = a.marker
          test[m++] = [
            { lat: a.seller_lat, lng: a.seller_lng },
            { lat: a.buyer_lat, lng: a.buyer_lng }
          ]
        }


      }
      if (index === length - 1) {

        first_lat = a.seller_lat;
        first_lng = a.seller_lng

      }
    })
    // console.log(first_lat)
    // console.log(test)
    // console.log(newData)
    // console.log(demoo)
    this.setState({
      newData: test,
      first_lat: first_lat,
      first_lng: first_lng
    })

  }
  render() {

    // const PolylineDecorator = withLeaflet(props => {
    //     const polyRef = useRef();
    //     useEffect(() => {
    //       const polyline = polyRef.current.leafletElement; //get native Leaflet polyline
    //       const { map } = polyRef.current.props.leaflet; //get native Leaflet map

    //       L.polylineDecorator(polyline, {
    //         patterns: props.patterns
    //       }).addTo(map);
    //     }, []);
    //     return <Polyline ref={polyRef} {...props} />;
    //   });

    //   const arrow = [
    //     {
    //       offset: "100%",
    //       repeat: "0",
    //       symbol: L.Symbol.arrowHead({
    //         pixelSize: 15,
    //         polygon: false,
    //         pathOptions: { stroke: true }
    //       })
    //     }
    //   ];



    const iconPerson = new L.Icon({
      iconUrl: ("http://maps.google.com/mapfiles/ms/icons/red-dot.png"),
      // shadowUrl: "http://maps.google.com/mapfiles/ms/icons/red-dot.png",
      iconSize: [38, 40], // size of the icon
      html: "<div>Hello</div>",
      iconAnchor: [19, 39], // point of the icon which will correspond to marker's location

    });

    const iconPersonBlue = new L.Icon({
      iconUrl: ("http://maps.google.com/mapfiles/ms/icons/blue-dot.png"),
      // shadowUrl: "http://maps.google.com/mapfiles/ms/icons/red-dot.png",
      iconSize: [38, 40], // size of the icon

      iconAnchor: [19, 39], // point of the icon which will correspond to marker's location

    });



    let test1 = []
    let z = 0;
    this.state.newData.forEach((aaa, index) => {
      aaa.forEach((bb, index) => {
        test1[z++] = bb
      })
    })


    let test2 = []
    let u = 0
    let c = 0
    let test3 = []
    this.props.geolocationData.forEach((a, index) => {
      if (a.buyer_company_type_name !== "Brand") {
        test2[u++] = [
          { lat: a.buyer_lat, lng: a.buyer_lng, flag: a.flag },
          { lat: a.seller_lat, lng: a.seller_lng, flag: a.flag }
        ]

      }
      else {
        test2[u++] = [
          { lat: a.seller_lat, lng: a.seller_lng, flag: a.flag }
        ]
      }
    })


    test2.forEach((aaa, index) => {
      aaa.forEach((bb, index) => {
        test3[c++] = bb
      })
    })


    //console.log(test2)
    console.log(test3)

    let unique1 = []
    //console.log(test1);
    const unique = test3
      .map(e => e['lat'])

      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)

      // eliminate the dead keys & store unique objects
      .filter(e => test3[e]).map(e => test3[e]);
    //console.log(unique)
    // unique.reverse();
    // console.log(unique)

    unique1 = unique.map((val, index) => unique[unique.length - 1 - index]);

    console.log(unique1)
    
    //Branching Start
    var tempIndex = ''
    unique1.map((val, index) => {
      if(val.flag !==0 ){
        let tempVal = 0
        for( var i = index+1 ; i<= index + val.flag + 1; i++){
          tempIndex = index+2
          unique1[i].child = (index+2) + '.' + (++tempVal)
        }
        for(i=i; i< unique1.length; i++){
          unique1[i].newVal = ++tempIndex
        }
      }
    })
    //Branching End
    console.log(unique1)

    var i = 1


    return (
      <LeafletMap

        center={[28.7041, 77.1025]}
        zoom={2}
        maxZoom={10}
        attributionControl={true}
        zoomControl={true}
        doubleClickZoom={true}
        scrollWheelZoom={true}
        dragging={true}
        animate={true}
        easeLinearity={0.35}
        onClick={this.onMapClick}
      >
        <TileLayer
          url='https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png'
        />
        {/* <Marker position={[50, 10]}>
          <Popup>
            <div>Popup for any custom information.</div>
          </Popup>
        </Marker> */}

        {/* {test1.map(marker => (
          (marker.lat === this.state.first_lat && marker.lng === this.state.first_lng) ?
            <Marker
              onClick={this.onMarkerClick}
              position={{ lat: marker.lat, lng: marker.lng }}
              icon={iconPersonBlue}
              bounceOnAdd = {true}
            />
            :
            <Marker
              onClick={this.onMarkerClick}
              position={{ lat: marker.lat, lng: marker.lng }}
              icon={iconPerson}
            />

        ))} */}


        {unique1.map(marker => (
          
          <DivIcon
            onClick={this.onMarkerClick}
            position={{ lat: marker.lat, lng: marker.lng }}

          >
            {(marker.child === undefined || marker.child === '') ? (marker.newVal === undefined || marker.newVal === '') ?<div style={{ paddingLeft: '6px' }}>{i++}</div> : <div style={{ paddingLeft: '6px' }}>{marker.newVal}</div>:<div style={{ paddingLeft: '3px' }}>{marker.child}</div>}
          </DivIcon>

        ))}

        <Polyline positions={this.state.newData} color={'#ef00ffa6'} />
        {
          this.state.showingInfoWindow &&
          <div>
            <Popup
              position={this.state.selectedPlace.latlng}
            >

              <div>
                {this.state.sellerDuplicate === 1 &&
                  <div>
                    {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_seller_col === 0) ? false : <h3 style={{ fontSize: '10px' }}><strong>Company Name/Source :</strong> <span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.company_name}</span></h3>}
                  </div>
                }
                {/* {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_buyer_col === 0) ? false : <h3 style={{ fontSize: '10px', marginTop: '-1px' }}>{(!_.isEmpty(this.state.buyer_company_name )) && <strong> Buyer Name :</strong>}<span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.buyer_company_name}</span></h3>}
                {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_invoice_number_col === 0) ? false : <h3 style={{ fontSize: '10px', marginTop: '-1px' }}>{(!_.isEmpty(this.state.Invoice_number )) && <strong>Invoice number :</strong>} <span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.Invoice_number}</span></h3>}
              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_MoT_col === 0) ? false : <h3 style={{ fontSize: '10px', marginTop: '-1px' }}>{(!_.isEmpty(this.state.shipment_transport_mode )) && <strong>Transport :</strong>} <span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.shipment_transport_mode}</span></h3>}
              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_shipment_date_col === 0) ? false : <h3 style={{ fontSize: '10px', marginTop: '-1px' }}>{(!_.isEmpty(this.state.receive_data )) && <strong>Shipment Date :</strong>} <span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.receive_data}</span></h3>}
                {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_received_date_col === 0) ? false : <h3 style={{ fontSize: '10px', marginTop: '-1px' }}>{(!_.isEmpty(this.state.shipment_date )) && <strong>Receive Date :</strong>}<span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.shipment_date}</span></h3>} */}

                {/* {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_buyer_col === 0) ? false : <h3 style={{ fontSize: '10px', marginTop: '-1px' }}><strong> Buyer Name :</strong><span style={{ fontWeight: '350' }}>{this.state.buyer_company_name}</span></h3>}
                {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_invoice_number_col === 0) ? false : <h3 style={{ fontSize: '10px', marginTop: '-1px' }}><strong>Invoice number :</strong> <span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.Invoice_number}</span></h3>}
                {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_MoT_col === 0) ? false : <h3 style={{ fontSize: '10px', marginTop: '-1px' }}><strong>Transport :</strong> <span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.shipment_transport_mode}</span></h3>}
                {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_shipment_date_col === 0) ? false : <h3 style={{ fontSize: '10px', marginTop: '-1px' }}><strong>Shipment Date :</strong> <span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.receive_data}</span></h3>}
                {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_received_date_col === 0) ? false : <h3 style={{ fontSize: '10px', marginTop: '-1px' }}><strong>Receive Date :</strong><span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.shipment_date}</span></h3>} */}
                {(!_.isEmpty(this.state.mergeArray)) && <hr style={{ borderTop: '1px solid #00000063' }}></hr>}
                {(!_.isEmpty(this.state.mergeArray)) && this.state.mergeArray.map((item, index) =>
                  (<div style={{ marginLeft: "-7px", marginTop: "-5px" }}>
                    {this.state.sellerDuplicate === 1 ?
                      <table className="table table-borderless" style={{ width: "80%" }}>
                        <tbody>
                          {index === 0 &&
                            <tr>
                              {/* {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_seller_col === 0) ? false : <th style={{ fontSize: '10px' }}>Company Name</th>} */}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_buyer_col === 0) ? false : <th style={{ fontSize: '10px' }}>Buyer Name</th>}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_invoice_number_col === 0) ? false : <th style={{ fontSize: '10px' }}>Inv No.</th>}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_MoT_col === 0) ? false : <th style={{ fontSize: '10px' }}>MOT </th>}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_shipment_date_col === 0) ? false : <th style={{ fontSize: '10px' }}>Ship. Date</th>}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_received_date_col === 0) ? false : <th style={{ fontSize: '10px' }}>Receive Date</th>}
                            </tr>
                          }
                          <tr>

                            {/* {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_seller_col === 0) ? false : <td id={index + item.seller_company_name} style={{ fontSize: '10px' }} title={item.seller_company_name} onClick={() => this.onTagClick(index + item.seller_company_name)}>
                            {`${item.seller_company_name.substring(0, 10)}...`}
                          </td>} */}

                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_buyer_col === 0) ? false : <td id={index + item.buyer_company_name} style={{ fontSize: '10px' }} title={item.buyer_company_name} onClick={() => this.onTagClick(index + item.buyer_company_name)}>
                              {`${item.buyer_company_name.substring(0, 10)}...`}
                            </td>}
                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_invoice_number_col === 0) ? false :
                              <td id={index + item.Invoice_number} style={{ fontSize: '10px' }} style={{ width: '21%' }} title={item.Invoice_number} onClick={() => this.onTagClick(index + item.Invoice_number)}>
                                {`${item.Invoice_number.substring(0, 3)}...`}
                              </td>
                            }
                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_MoT_col === 0) ? false :
                              <td style={{ fontSize: '10px' }}>
                                {item.shipment_transport_mode}
                              </td>
                            }
                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_shipment_date_col === 0) ? false :
                              <td style={{ fontSize: '10px' }}>
                                {item.shipment_date}
                              </td>
                            }
                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_received_date_col === 0) ? false :
                              <td style={{ fontSize: '10px' }}>
                                {item.receive_data}
                              </td>
                            }
                          </tr>
                        </tbody>
                      </table>
                      :
                      <table className="table table-borderless" style={{ width: "80%" }}>
                        <tbody>
                          {index === 0 &&
                            <tr>
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_seller_col === 0) ? false : <th style={{ fontSize: '10px' }}>Company Name</th>}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_buyer_col === 0) ? false : <th style={{ fontSize: '10px' }}>Buyer Name</th>}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_invoice_number_col === 0) ? false : <th style={{ fontSize: '10px' }}>Inv No.</th>}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_MoT_col === 0) ? false : <th style={{ fontSize: '10px' }}>MOT </th>}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_shipment_date_col === 0) ? false : <th style={{ fontSize: '10px' }}>Ship. Date</th>}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_received_date_col === 0) ? false : <th style={{ fontSize: '10px' }}>Receive Date</th>}
                            </tr>
                          }
                          <tr>

                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_seller_col === 0) ? false : <td id={index + item.seller_company_name} style={{ fontSize: '10px' }} title={item.seller_company_name} onClick={() => this.onTagClick(index + item.seller_company_name)}>
                              {`${item.seller_company_name.substring(0, 10)}...`}
                            </td>}

                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_buyer_col === 0) ? false : <td id={index + item.buyer_company_name} style={{ fontSize: '10px' }} title={item.buyer_company_name} onClick={() => this.onTagClick(index + item.buyer_company_name)}>
                              {`${item.buyer_company_name.substring(0, 10)}...`}
                            </td>}
                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_invoice_number_col === 0) ? false :
                              <td id={index + item.Invoice_number} style={{ fontSize: '10px' }} style={{ width: '21%' }} title={item.Invoice_number} onClick={() => this.onTagClick(index + item.Invoice_number)}>
                                {`${item.Invoice_number.substring(0, 3)}...`}
                              </td>
                            }
                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_MoT_col === 0) ? false :
                              <td style={{ fontSize: '10px' }}>
                                {item.shipment_transport_mode}
                              </td>
                            }
                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_shipment_date_col === 0) ? false :
                              <td style={{ fontSize: '10px' }}>
                                {item.shipment_date}
                              </td>
                            }
                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_received_date_col === 0) ? false :
                              <td style={{ fontSize: '10px' }}>
                                {item.receive_data}
                              </td>
                            }
                          </tr>
                        </tbody>
                      </table>
                    }
                  </div>
                  ))
                }



                {(!_.isEmpty(this.state.prodArray)) && <hr style={{ borderTop: '1px solid #00000063' }}></hr>}



                {(!_.isEmpty(this.state.prodArray)) && this.state.prodArray.map((item, index) =>
                  (<div style={{ marginLeft: "-7px", marginTop: "-5px" }}>
                    {this.state.sellerDuplicate === 1 ?
                      <table className="table table-borderless" style={{ width: "100%" }}>
                        <tbody>
                          {index === 0 &&
                            <tr>
                              {/* {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_seller_col === 0) ? false : <th style={{ fontSize: '10px' }}>Company Name</th>} */}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_product_col === 0) ? false : <th style={{ fontSize: '10px' }}>Product Name</th>}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_quantity_col === 0) ? false : <th style={{ fontSize: '10px' }}>Product Quantity</th>}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_unit_col === 0) ? false : <th style={{ fontSize: '10px' }}>Product Unit </th>}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_description_col === 0) ? false : <th style={{ fontSize: '10px' }}>Product Description</th>}
                            </tr>
                          }
                          <tr>
                            {/* {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_seller_col === 0) ? false : <td id={index + item.seller_company_name} style={{ fontSize: '10px' }} title={item.seller_company_name} onClick={() => this.onTagClick(index + item.seller_company_name)}>
                              {`${item.seller_company_name.substring(0, 10)}...`}
                            </td>} */}
                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_product_col === 0) ? false : <td id={index + item.product_name} style={{ fontSize: '10px' }} title={item.product_name} onClick={() => this.onTagClick(index + item.product_name)}>
                              {`${item.product_name.substring(0, 5)}...`}
                            </td>}
                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_quantity_col === 0) ? false :
                              <td style={{ fontSize: '10px' }}>
                                {item.product_qty}
                              </td>
                            }
                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_unit_col === 0) ? false :
                              <td id={index + item.product_uom} style={{ fontSize: '10px' }} title={item.product_uom} onClick={() => this.onTagClick(index + item.product_uom)}>
                                {(!_.isEmpty(item.product_uom)) && `${item.product_uom.substring(0, 4)}...`}
                              </td>
                            }
                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_description_col === 0) ? false :
                              <td id={index + item.product_description} style={{ fontSize: '10px' }} title={item.product_description} onClick={() => this.onTagClick(index + item.product_description)}>
                                {(!_.isEmpty(item.product_description)) ? `${item.product_description.substring(0, 10)}...` : 'Nil..'}
                              </td>
                            }
                          </tr>
                        </tbody>
                      </table>
                      :
                      <table className="table table-borderless" style={{ width: "100%" }}>
                        <tbody>
                          {index === 0 &&
                            <tr>
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_seller_col === 0) ? false : <th style={{ fontSize: '10px' }}>Company Name</th>}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_product_col === 0) ? false : <th style={{ fontSize: '10px' }}>Product Name</th>}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_quantity_col === 0) ? false : <th style={{ fontSize: '10px' }}>Product Quantity</th>}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_unit_col === 0) ? false : <th style={{ fontSize: '10px' }}>Product Unit </th>}
                              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_description_col === 0) ? false : <th style={{ fontSize: '10px' }}>Product Description</th>}
                            </tr>
                          }
                          <tr>
                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_seller_col === 0) ? false : <td id={index + item.seller_company_name} style={{ fontSize: '10px' }} title={item.seller_company_name} onClick={() => this.onTagClick(index + item.seller_company_name)}>
                              {`${item.seller_company_name.substring(0, 10)}...`}
                            </td>}
                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_product_col === 0) ? false : <td id={index + item.product_name} style={{ fontSize: '10px' }} title={item.product_name} onClick={() => this.onTagClick(index + item.product_name)}>
                              {`${item.product_name.substring(0, 5)}...`}
                            </td>}
                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_quantity_col === 0) ? false :
                              <td style={{ fontSize: '10px' }}>
                                {item.product_qty}
                              </td>
                            }
                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_unit_col === 0) ? false :
                              <td id={index + item.product_uom} style={{ fontSize: '10px' }} title={item.product_uom} onClick={() => this.onTagClick(index + item.product_uom)}>
                                {(!_.isEmpty(item.product_uom)) && `${item.product_uom.substring(0, 4)}...`}
                              </td>
                            }
                            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_description_col === 0) ? false :
                              <td id={index + item.product_description} style={{ fontSize: '10px' }} title={item.product_description} onClick={() => this.onTagClick(index + item.product_description)}>
                                {(!_.isEmpty(item.product_description)) ? `${item.product_description.substring(0, 10)}...` : 'Nil..'}
                              </td>
                            }
                          </tr>
                        </tbody>
                      </table>
                    }
                  </div>
                  ))
                }
                {/* <h2>{this.state.buyer_company_name}</h2>
            <h2>{this.state.Invoice_number}</h2>
            <h2>{this.state.shipment_transport_mode}</h2>
            <h2>{this.state.shipment_date}</h2>
            <h2>{this.state.receive_data}</h2>
            <h2>{this.state.product_name}</h2>
            <h2>{this.state.product_qty}</h2>
            <h2>{this.state.product_uom}</h2>
            <h2>{this.state.product_description}</h2> */}

                {/* <p>{this.state.address}</p> */}
              </div>
            </Popup>
          </div>
        }
      </LeafletMap>
    );
  }
}

export default Map