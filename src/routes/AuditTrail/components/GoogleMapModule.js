import React from 'react';
import { GoogleApiWrapper, InfoWindow, Map, Marker, Polyline } from 'google-maps-react';
import Geocode from "react-geocode";

class GoogleMapModule extends React.Component {
  constructor(props) {
    super(props);
    //console.log(data.value.shipment);
    this.state = {
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
      testData: [],
      demo_marker1: [],
      address: '',
      demoo: '',
      newData: '',
      company_name: '',

      buyer_company_name: '',
      Invoice_number: '',
      shipment_transport_mode: '',
      shipment_date: '',
      receive_data: '',
      product_name: '',
      product_qty: '',
      product_uom: '',
      product_description: '',
      product: [],
      first_lat: '',
      first_lng: ''

    }

    let initData = {}
    let newData = []
    let i = 0;
    let company;
    // Geocode.setApiKey("AIzaSyDa8IbCs7c-NM1Yw0eoXAqvxFJ27aQ6s58");

    // binding this to event-handler functions
    this.onMarkerClick = this.onMarkerClick.bind(this);
    this.onMapClick = this.onMapClick.bind(this);
  }

  componentWillMount() {
    this.getData()
    const abc = this
  }
  getData() {
    const abc = this
    let initData = {}
    let newData = []
    let demoo = []
    let test = []
    let marker = []
    let i = 0;
    let j = 0;
    let m = 0;
    let first_lat = '';
    let first_lng = '';
    let company;
    let k;
    let length = this.props.geolocationData.length
    this.props.geolocationData.forEach(function (a, index) {

      let array = { ...initData }
      let p = {}
      //debugger
      if (index < length) {
        newData[i++] = a.marker
        test[m++] = [
          { lat: a.seller_lat, lng: a.seller_lng },
          { lat: a.buyer_lat, lng: a.buyer_lng }
        ]

      }
      if (index === length - 1) {

        first_lat = a.seller_lat;
        first_lng = a.seller_lng

      }
    })
    // console.log(first_lat)
    // console.log(test)
    // console.log(newData)
    // console.log(demoo)
    this.setState({
      newData: test,
      first_lat: first_lat,
      first_lng: first_lng
    })

  }
  onMarkerClick = (props, marker, e) => {
    console.log(marker)
    console.log(props)
    console.log(this.props.geolocationData)
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    });
    // console.log(props.position.lat)

    console.log(this.props.geolocationData)
    let length = this.props.geolocationData.length
    let abc = this
    let temp = 0;
    this.props.geolocationData.forEach(function (a, index) {
      // debugger
      if (index < length) {
        if (a.seller_lat === props.position.lat && a.seller_lng === props.position.lng) {
          temp = 1
          // console.log(temp)
          console.log(a);
          if (a.Invoice_date !== null) {
            var spdate = (a.Invoice_date.split("T"))[0].split("-");
            a.Invoice_date = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
          }

          if (a.shipment_date !== null) {
            var new_spdate = (a.shipment_date.split("T"))[0].split("-");
            a.shipment_date = new_spdate[2] + "-" + new_spdate[1] + "-" + new_spdate[0];
          }

          let nameArray = []
          a.product.forEach((test, index) => {
            nameArray[index] = test.product_name
          })
          // console.log(nameArray)


          abc.setState({
            company_name: a.seller_company_name,
            buyer_company_name: a.buyer_company_name,
            Invoice_number: a.Invoice_number,
            shipment_transport_mode: a.shipment_transport_mode,
            shipment_date: a.shipment_date,
            receive_data: a.Invoice_date,

            product_name: a.product_name,
            product_qty: a.product_qty,
            product_uom: a.product_uom,
            product_description: a.product_description,
            product: a.product
          })
          console.log(a.shipment_date);
        }
      }
    })
    if (temp === 0) {
      this.props.geolocationData.forEach(function (a, index) {
        if (index < length) {
          if (a.buyer_lat === props.position.lat && a.buyer_lng === props.position.lng) {

            console.log(a);
            if (a.Invoice_date !== null) {
              var spdate = (a.Invoice_date.split("T"))[0].split("-");
              a.Invoice_date = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
            }

            if (a.shipment_date !== null) {
              var new_spdate = (a.shipment_date.split("T"))[0].split("-");
              a.shipment_date = new_spdate[2] + "-" + new_spdate[1] + "-" + new_spdate[0];
            }
            let nameArray = []
            a.product.forEach((test, index) => {
              nameArray[index] = test.product_name
            })
            // console.log(temp)
            // console.log(a.buyer_company_name);
            abc.setState({
              company_name: a.buyer_company_name,

              buyer_company_name: a.buyer_company_name,
              Invoice_number: a.Invoice_number,
              shipment_transport_mode: a.shipment_transport_mode,
              shipment_date: a.shipment_date,
              receive_data: a.Invoice_date,

              product_name: a.product_name,
              product_qty: a.product_qty,
              product_uom: a.product_uom,
              product_description: a.product_description,
              product: a.product
            })
          }
        }
      })

    }
    // Geocode To fetch address from Coordinates
    Geocode.fromLatLng(props.position.lat, props.position.lng).then(
      response => {
        const address = response.results[0].formatted_address;
        this.setState({
          address: address
        })
      },
      error => {
        console.error(error);
      }
    );


  }
  onMapClick = (props) => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      });
    }
  }
  render() {
    const style = {
      width: '75vw',
      height: '80vh',
      'marginLeft': 'auto',
      'marginRight': 'auto'
    }
    let test1 = []
    let z = 0;
    const google = window.google;
    let bounds = new google.maps.LatLngBounds();
    this.state.newData.forEach((aaa, index) => {
      aaa.forEach((bb, index) => {
        test1[z++] = bb
      })
    })
    console.log(test1)
    // console.log(test1)
    return (
      <div>
        <Map
          item
          xs={12}
          style={style}
          google={this.props.google}
          onClick={this.onMapClick}
          zoom={4}
          initialCenter={{ lat: 28.6139, lng: 77.2090 }}
        >
          {test1.map(marker => (
            (marker.lat === this.state.first_lat && marker.lng === this.state.first_lng) ?
              <Marker
                onClick={this.onMarkerClick}
                title={'Changing Colors Garage'}
                position={{ lat: marker.lat, lng: marker.lng }}
                name={'Changing Colors Garage'}
                animation={google.maps.Animation.BOUNCE}
                icon={"http://maps.google.com/mapfiles/ms/icons/blue-dot.png"}
              />
              :
              <Marker
                onClick={this.onMarkerClick}
                title={'Changing Colors Garage'}
                position={{ lat: marker.lat, lng: marker.lng }}
                name={'Changing Colors Garage'}
                icon={"http://maps.google.com/mapfiles/ms/icons/red-dot.png"}
              />

          ))}

          {/* {test1.map(marker => (
          <Marker
            onClick={this.onMarkerClick}
            title={'Changing Colors Garage'}
            position={{ lat: marker.lat, lng: marker.lng }}
            name={'Changing Colors Garage'}
            icon={"http://maps.google.com/mapfiles/ms/icons/red-dot.png"}
          />
        ))}

        <Marker
          onClick={this.onMarkerClick}
          title={'Changing Colors Garage'}
          position={{ lat: this.state.first_lat, lng: this.state.first_lng }}
          name={'Changing Colors Garage'}
          animation={google.maps.Animation.BOUNCE}
          icon={"http://maps.google.com/mapfiles/ms/icons/blue-dot.png"}
        /> */}


          <InfoWindow
            marker={this.state.activeMarker}
            visible={this.state.showingInfoWindow}>
            <div>
              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_seller_col === 0) ? false : <h3 style={{ fontSize: '14px' }}>Company Name : <span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.company_name}</span></h3>}
              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_buyer_col === 0) ? false : <h3 style={{ fontSize: '14px' }}>Buyer Name : <span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.buyer_company_name}</span></h3>}
              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_invoice_number_col === 0) ? false : <h3 style={{ fontSize: '14px' }}>Invoice number : <span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.Invoice_number}</span></h3>}
              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_MoT_col === 0) ? false : <h3 style={{ fontSize: '14px' }}>Transport : <span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.shipment_transport_mode}</span></h3>}
              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_shipment_date_col === 0) ? false : <h3 style={{ fontSize: '14px' }}>Shipment Date : <span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.receive_data}</span></h3>}
              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_received_date_col === 0) ? false : <h3 style={{ fontSize: '14px' }}>Receive Date : <span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.shipment_date}</span></h3>}

              {/* {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_product_col === 0) ? false : <h3 style={{ fontSize: '18px' }}>Product Name : <span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.product_name}</span></h3>}
            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_quantity_col === 0) ? false : <h3 style={{ fontSize: '18px' }}>Product Qty : <span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.product_qty}</span></h3>}
            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_unit_col === 0) ? false : <h3 style={{ fontSize: '18px' }}>Product Unit : <span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.product_uom}</span></h3>}
            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_description_col === 0) ? false : <h3 style={{ fontSize: '18px' }}>Description: <span style={{ whiteSpace: "nowrap", fontWeight: '350' }}>{this.state.product_description}</span></h3>} */}

              {this.state.product.map((item, index) =>
                (<div>
                  <table className="table table-borderless">
                    <tbody>
                      {index === 0 &&
                        <tr>
                          {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_product_col === 0) ? false : <th>Product Name</th>}
                          {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_quantity_col === 0) ? false : <th>Product Quantity</th>}
                          {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_unit_col === 0) ? false : <th>Product Unit </th>}
                          {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_description_col === 0) ? false : <th>Product Description</th>}
                        </tr>
                      }
                      <tr>

                        {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_product_col === 0) ? false : <td className="ProdData_name">
                          {item.product_name}
                        </td>}
                        {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_quantity_col === 0) ? false :
                          <td className="ProdData_qty" style={{ width: '21%' }}>
                            {item.product_qty}
                          </td>
                        }
                        {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_unit_col === 0) ? false :
                          <td >
                            {item.product_uom}
                          </td>
                        }
                        {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.pd_description_col === 0) ? false :
                          <td >
                            {item.product_description}
                          </td>
                        }
                      </tr>
                    </tbody>
                  </table>
                </div>
                ))
              }
              {/* <h2>{this.state.buyer_company_name}</h2>
            <h2>{this.state.Invoice_number}</h2>
            <h2>{this.state.shipment_transport_mode}</h2>
            <h2>{this.state.shipment_date}</h2>
            <h2>{this.state.receive_data}</h2>
            <h2>{this.state.product_name}</h2>
            <h2>{this.state.product_qty}</h2>
            <h2>{this.state.product_uom}</h2>
            <h2>{this.state.product_description}</h2> */}

              {/* <p>{this.state.address}</p> */}
            </div>
          </InfoWindow>


          {this.state.newData.map((marker, index) => (
            <Polyline
              path={marker}
              geodesic={true}
              options={{
                strokeColor: "blue",
                // strokeOpacity: 0.75,
                strokeWeight: 2.5,
                icons: [
                  {
                    icon: { path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW },
                    offset: "0",
                    repeat: "130px"
                  }
                ]
              }}
            />
          ))}

        </Map>
        {this.props.isPublicCall ? <p style={{ position: 'absolute', top: '92%', left: '241px' }}>Click on the marker to see shipment details *</p> : <p style={{ position: 'absolute', top: '155%', left: '62px' }}>Click on the marker to see shipment details *</p>}
      </div>
    );
  }
}
export default GoogleApiWrapper({
  // apiKey: 'AIzaSyDa8IbCs7c-NM1Yw0eoXAqvxFJ27aQ6s58'
})(GoogleMapModule)