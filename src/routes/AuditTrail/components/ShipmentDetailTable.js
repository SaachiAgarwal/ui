import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);

/** ShipmentDetailTable
 *
 * @description This class is responsible to display records for audit trail
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class ShipmentDetailTable extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    strings.setLanguage(this.props.currentLanguage);
    const { seller, buyer, invoiceNumber, mot, shipmentDate, receivedDate, shipmentStatus, orderCompany, supplierCompany, orderNumber, linkedOrderNumber, invoiceDate, sellerSource } = strings.shipmentTableText;
    return (
      <div className='ordertable' id='shipDetail'>
        <div>
          { !this.props.orderAuditVal ? 
            <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options}>
              <TableHeaderColumn dataField='asset_id' hidden={true}>Asset Id</TableHeaderColumn>
              <TableHeaderColumn dataField='index' isKey={true} hidden={true} autoValue={true}>Serial. No.</TableHeaderColumn>
              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_seller_col === 0) ? false : <TableHeaderColumn dataField='seller_company_name' columnTitle={true}>{sellerSource}</TableHeaderColumn>}
              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_buyer_col === 0) ? false : <TableHeaderColumn dataField='buyer_company_name' columnTitle={true}>{buyer}</TableHeaderColumn>}
              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_invoice_number_col === 0) ? false : <TableHeaderColumn dataField='invoice_number' columnTitle={true}>{invoiceNumber}#</TableHeaderColumn>}
              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_MoT_col === 0) ? false : <TableHeaderColumn dataField='shipment_transport_mode' columnTitle={true}>{mot}</TableHeaderColumn>}
              {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_shipment_date_col === 0) ? false : <TableHeaderColumn dataField='new_invoice_date'>{invoiceDate}</TableHeaderColumn>}
              {/* {((!_.isEmpty(this.props.asset_id))&& this.props.configData.data.sd_received_date_col === 0) ? false :<TableHeaderColumn dataField='new_invoice_date'>{receivedDate}</TableHeaderColumn>} */}
              <TableHeaderColumn dataField='status' hidden={true}>{shipmentStatus}</TableHeaderColumn>
              <TableHeaderColumn key="editAction" dataFormat={this.props.auditFormatter}></TableHeaderColumn>
            </BootstrapTable>
            :
            <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options}>
            <TableHeaderColumn dataField='asset_id' hidden={true}>Asset Id</TableHeaderColumn>
            <TableHeaderColumn dataField='index' isKey={true} hidden={true} autoValue={true}>Serial. No.</TableHeaderColumn>
            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_seller_col === 0) ? false : <TableHeaderColumn dataField='order_company_name' columnTitle={true}>{orderCompany}</TableHeaderColumn>}
            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_buyer_col === 0) ? false : <TableHeaderColumn dataField='supplier_company_name' columnTitle={true}>{supplierCompany}</TableHeaderColumn>}
            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_invoice_number_col === 0) ? false : <TableHeaderColumn dataField='order_number' columnTitle={true}>{orderNumber}</TableHeaderColumn>}
            {((!_.isEmpty(this.props.asset_id)) && this.props.configData.data.sd_invoice_number_col === 0) ? false : <TableHeaderColumn dataField='linked_order_number' columnTitle={true}>{linkedOrderNumber}</TableHeaderColumn>}
            {/* {((!_.isEmpty(this.props.asset_id))&& this.props.configData.data.sd_received_date_col === 0) ? false :<TableHeaderColumn dataField='new_invoice_date'>{receivedDate}</TableHeaderColumn>} */}
            <TableHeaderColumn dataField='status' hidden={true}>{shipmentStatus}</TableHeaderColumn>
            <TableHeaderColumn key="editAction" dataFormat={this.props.auditFormatter}></TableHeaderColumn>
          </BootstrapTable>

          }
        </div>
      </div>
    );
  }
}

ShipmentDetailTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default ShipmentDetailTable;