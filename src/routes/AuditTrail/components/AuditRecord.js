import React, { Component } from 'react';
import PropTypes from 'prop-types';
import QRCode from 'qrcode-react';
import { Button } from 'reactstrap';
import Pagination from 'react-paginate';

import { Images, Config } from 'config/Config';
import { getEntityId } from 'components/Helper';
//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

let strings = new LocalizedStrings(
  data
);



class AuditRecord extends React.Component {
  constructor(props) {
    super(props);
    this.download = this.download.bind(this);
    this.handlePageClick = this.handlePageClick.bind(this);
    this.TestCell = this.TestCell.bind(this)
    this.state = {
      url: '',
      scanurl: '',
      orderListing: false
    }
    this.auditViewFormatter = this.auditViewFormatter.bind(this)
    this.downloadList = this.downloadList.bind(this)
    this.gridClick = this.gridClick.bind(this)
    this.listClick = this.listClick.bind(this)
  }

  async download(item) {
    // debugger
    console.log(item);
    var id = item.asset_id;
    const canvas = document.querySelector(`#${CSS.escape(item.asset_id)}` + ' > canvas ');
    // this.refs.QR.href = canvas.toDataURL();
    // await this.setState({ url: this.refs.QR.href });
    await this.setState({ url: canvas.toDataURL() });
  }

  async downloadList(item) {
    // debugger
    console.log(item);
    var id = item.asset_id;
    const canvas = document.querySelector(`#${CSS.escape(item.asset_id)}` + ' > canvas ');
    // this.refs.QR.href = canvas.toDataURL();
    // await this.setState({ url: this.refs.QR.href });
    await this.setState({ url: canvas.toDataURL() });

  }

  handlePageClick(value) {
    this.props.sendData(value.selected + 1)
    if (this.props.role === 'Birla Cellulose') {
      // this.props.setCurrentNewPage(value.selected + 1)
      let defaultEntityId = getEntityId(this.props.brandEntities, this.props.entityName);
      this.props.fetchBrandsOrders(defaultEntityId, this.props.fromDt, this.props.toDt, this.props.searchText, value.selected + 1, this.props.options.pageSize);
    } else {
      // this.props.setCurrentNewPage(value.selected + 1)
      this.props.fetchOrders('outgoingOrders', this.props.fromDt, this.props.toDt, this.props.searchText, value.selected + 1, this.props.options.pageSize);
    }
  }

  auditViewFormatter(row, cell) {
    // debugger
    return (
      <ButtonGroup>
        <Button className="action" onClick={() => this.props.viewAuditRecord(cell)} >{strings.incomingFormatterText.view}</Button>
        {/* <Button className="action" onClick={() => this.download(cell)} >Download QR</Button> */}
        <div>
          <div id={cell.asset_id} className="right HpQrcode" style={{ display: "none" }} >
            <QRCode
              value={(cell.asset_id !== null) ? `${Config.scanurl}audit?asset_id=${cell.asset_id}` : ''}
              bgColor="white"
              fgColor="black"
              size={480}
              level={'H'}
            />
          </div>
          <a style={{ textAlign: 'center', backgroundColor: '#4e79e1', color: 'white', width: '112%', borderRadius: '8%' }} ref={"QR"} href={this.state.url} onClick={() => this.downloadList(cell)} download={`${cell.order_number}`}>{strings.shipmentStatus.downloadQR}</a>
        </div>
      </ButtonGroup>
    );
  }
  async TestCell(cell) {
    console.log(cell)
  }

  async gridClick() {
    // this.setState({
    //   orderListing: false
    // })

    await this.props.fetchingAudit(true)
    setTimeout(
      function () {
        this.props.orderListingVal(false)
        this.props.fetchingAudit(false)
      }
        .bind(this),
      1000
    );

  }

  async listClick() {
    // this.setState({
    //   orderListing: true
    // })

    await this.props.fetchingAudit(true)
    setTimeout(
      function () {
        this.props.orderListingVal(true)
        this.props.fetchingAudit(false)
      }
        .bind(this),
      1000
    );

  }
  render() {
    strings.setLanguage(this.props.currentLanguage);
    const { shipmentStatus } = strings.shipmentTableText;
    const { purchaseOrder, supplier, orderDate } = strings.createOrder;
    return (
      <div>
        <div className="col-md-5" style={{ marginLeft: '1%' }}>
          <img src={Images.gridImage} style={{ width: '10%' }} data-toggle="tooltip" title="Grid View" onClick={() => this.gridClick()} />
          <img src={Images.listImage} style={{ width: '6%' }} data-toggle="tooltip" title="List View" onClick={() => this.listClick()} />
        </div>


        <div className="col-md-12 col-lg-12 order-barcode">
          {!this.props.orderListing ?
            <div className="row">
              {(!_.isEmpty(this.props.orders) && this.props.orders.map((item, index) => {
                return (
                  <div className="col-md-4 col-detail" key={index}>
                    <div className="left" onClick={() => this.props.viewAuditRecord(item)}>
                      <QRCode
                        value={(item.asset_id !== null) ? `${Config.scanurl}audit?asset_id=${item.asset_id}` : ''}
                        bgColor="white"
                        fgColor="black"
                        size={3840}
                        level={'H'}
                      />
                    </div>
                    <div className="right" onClick={() => this.props.viewAuditRecord(item)}>
                      {item.order_number.length > 15 ?
                        <p><span>{purchaseOrder}</span> <p style={{ display: 'inline' }} data-toggle="tooltip" title={item.order_number} >{`${item.order_number.substring(0, 15)}...`}</p></p>
                        :
                        <p><span>{purchaseOrder}</span> <p style={{ display: 'inline' }}  >{item.order_number}</p></p>
                      }

                      {item.receiver_company_name.length > 20 ?
                        <p><span>{supplier}</span> <p style={{ display: 'inline' }} data-toggle="tooltip" title={item.receiver_company_name} >{`${item.receiver_company_name.substring(0, 20)}...`}</p></p>
                        :
                        <p><span>{supplier}</span> <p style={{ display: 'inline' }}  >{item.receiver_company_name}</p></p>
                      }

                      <p><span>{orderDate}</span> {item.new_order_date}</p>
                      <p><span>{shipmentStatus}</span> {item.shipment_status}</p>
                    </div>
                    <div style={{ clear: "both" }}>
                      <div id={item.asset_id} className="right HpQrcode" style={{ display: "none" }}>
                        <QRCode
                          value={(item.asset_id !== null) ? `${Config.scanurl}audit?asset_id=${item.asset_id}` : ''}
                          bgColor="white"
                          fgColor="black"
                          size={480}
                          level={'H'}
                        />
                      </div>
                      <a ref={"QR"} href={this.state.url} className="qr-ancher" onClick={() => this.download(item)} download={`${item.order_number}`}>{strings.shipmentStatus.download}</a>
                    </div>
                  </div>


                );

              }))}

            </div>
            :
            <div style={{ marginTop: '-2%' }}>
              {(!_.isEmpty(this.props.orders)) &&
                <BootstrapTable id="exportTable" data={this.props.orders}  >
                  <TableHeaderColumn dataField='asset_id' hidden={true}>Asset ID</TableHeaderColumn>
                  <TableHeaderColumn dataField='order_id' hidden={true} isKey={true}>Order ID</TableHeaderColumn>
                  <TableHeaderColumn dataField='order_number' >{purchaseOrder}</TableHeaderColumn>
                  <TableHeaderColumn dataField='receiver_company_name' columnTitle={true}>{supplier}</TableHeaderColumn>
                  <TableHeaderColumn dataField='new_order_date'>{orderDate}</TableHeaderColumn>
                  <TableHeaderColumn dataField='shipment_status'>{shipmentStatus}</TableHeaderColumn>
                  <TableHeaderColumn key="editAction" dataFormat={this.auditViewFormatter}>{strings.shipmentTableText.action}</TableHeaderColumn>

                </BootstrapTable>
              }
            </div>

          }
          <div className="pagination-box">
            <Pagination
              previousLabel={strings.prev}
              nextLabel={strings.next}
              breakLabel={"..."}
              breakClassName={"break-me"}
              pageCount={this.props.pages}
              pageRangeDisplayed={5}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"}
            />
          </div>
        </div>
      </div>
    );
  }
}

AuditRecord.propTypes = {

}

export default AuditRecord;