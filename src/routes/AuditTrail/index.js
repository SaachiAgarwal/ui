import { injectReducer } from '../../store/reducers';
import {checkPageRestriction} from '../index';

export default (store) => ({
  path: 'audit',
  onEnter: (nextState, replace) => {
    checkPageRestriction(nextState, replace, () => {})
  },
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const AuditTrail = require('./containers/AuditTrailContainer').default;
      const reducer = require('./modules/audittrail').default;
      injectReducer(store, { key: 'AuditTrail', reducer });
      cb(null, AuditTrail);
  }, 'AuditTrail');
  },
});
