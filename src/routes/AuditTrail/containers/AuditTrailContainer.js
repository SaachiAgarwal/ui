/** AuditTrailContainer
 *
 * @description This is container that manages the states for all audit related functionality.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
import { connect } from 'react-redux';

import AuditTrail from '../components/AuditTrail';
import {setProp,setAuditRecordDetail, fetchOrders, resetAlertBox, getDetail, fetchBrandEntities, fetchBrandsOrders, setCurrentNewPage, setGeoRecordDetail, resetGeolocationData, setConfigDetail, falseAuditError, orderListingVal, fetchingAudit, resetBlockChainData, getDocData, downloadDoc} from '../modules/audittrail';
import {setInnerNav,setParam,setSearchString,setReportFetching,setGlobalState, setCurrentLanguage} from '../../../store/app';

const mapStateToProps = (state) => {
  return({
    navIndex: state.app.navIndex,
    navInnerIndex: state.app.navInnerIndex,
    navClass: state.app.navClass,
    fetching: state.AuditTrail.fetching,
    error: state.AuditTrail.error,
    status: state.AuditTrail.status,
    showAlert: state.AuditTrail.showAlert,
    alertMessage: state.AuditTrail.alertMessage,
    orders: state.AuditTrail.orders,
    viewAuditDetail: state.AuditTrail.viewAuditDetail,
    isTracker: state.AuditTrail.isTracker,
    blockchainData: state.AuditTrail.blockchainData,
    shipmentDetail: state.AuditTrail.shipmentDetail,
    totalRecordCount: state.AuditTrail.totalRecordCount,
    pages: state.AuditTrail.pages,
    brandEntities: state.AuditTrail.brandEntities,
    currentNewPage: state.AuditTrail.currentNewPage,
    geolocationData: state.AuditTrail.geolocationData,
    configData: state.AuditTrail.configData,
    currentLanguage : state.app.currentLanguage,
    auditErrorVal : state.AuditTrail.auditErrorVal,
    orderAuditVal: state.AuditTrail.orderAuditVal,
    orderListing: state.AuditTrail.orderListing,
    companyKeyDoc: state.AuditTrail.companyKeyDoc,
    docData: state.AuditTrail.docData
    /*sender: state.AuditTrail.sender,
    buyer: state.AuditTrail.buyer,
    index: state.AuditTrail.index,*/
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    setGlobalState: (value) => dispatch(setGlobalState(value)),
    setProp: (status, key) => dispatch(setProp(status, key)),
    resetAlertBox: (showAlert, message) => dispatch(resetAlertBox(showAlert, message)),
    setAuditRecordDetail: (row, isPublicCall, order_id) => dispatch(setAuditRecordDetail(row, isPublicCall, order_id)),
    fetchOrders: (key, frmDt, toDt, query, currentPage, pageSize) => dispatch(fetchOrders(key, frmDt, toDt, query, currentPage, pageSize)),
    fetchBrandsOrders: (entityId, frmDt, toDt, query, currentPage, pageSize) => dispatch(fetchBrandsOrders(entityId, frmDt, toDt, query, currentPage, pageSize)),
    getDetail: (company_type_name, blockchainData, completeBlchn) => dispatch(getDetail(company_type_name, blockchainData, completeBlchn)),
    fetchBrandEntities: () => dispatch(fetchBrandEntities()),
    setCurrentNewPage: (page) => dispatch(setCurrentNewPage(page)),
    setGeoRecordDetail: (row, isPublicCall) => dispatch(setGeoRecordDetail(row, isPublicCall)),
    resetGeolocationData: (value) => dispatch(resetGeolocationData(value)),
    setConfigDetail: (row, isPublicCall) => dispatch(setConfigDetail(row, isPublicCall)),
    setCurrentLanguage : (language) => dispatch(setCurrentLanguage(language)),
    setInnerNav: (lang) => dispatch(setInnerNav(lang)),
    setParam: (lang) => dispatch(setParam(lang)),
    setSearchString: (lang) => dispatch(setSearchString(lang)),
    setReportFetching: (lang) => dispatch(setReportFetching(lang)),
    falseAuditError: (val) => dispatch(falseAuditError(val)),
    orderListingVal: (val) => dispatch(orderListingVal(val)),
    fetchingAudit: (val) => dispatch(fetchingAudit(val)),
    resetBlockChainData: (val) => dispatch(resetBlockChainData(val)),
    getDocData: (val) => dispatch(getDocData(val)),
    downloadDoc: (val1, val2) => dispatch(downloadDoc(val1, val2))
  });
};

export default connect(mapStateToProps, mapDispatchToProps)(AuditTrail);
