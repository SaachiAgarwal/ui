/** shipments reducer
*
* @description This is a reducer responsible for all the create/delete/update/view/fulfill functionality
*
* @author Ketan Kumar<ketan@incaendo.com>
* @link http://www.incaendo.com
* @copyright (c) 2018, Grant Thornton India LLP.
*/
import axios from 'axios';
import { browserHistory } from 'react-router';

import { Config, Url } from 'config/Config';
import { getLocalStorage, saveLocalStorage, getToken, sleep, filterByDate, filterDataByCompanyType, getPages, isBlockchainCompleted, getBuyerSenderDetail, filterIpDataByCompanyType, getCompanyIdForPulp } from 'components/Helper';
// ------------------------------------
// Constant
// ------------------------------------
export const AUDIT_RECORD_DETAIL_SUCCESS = 'AUDIT_RECORD_DETAIL_SUCCESS';
export const SET_AUDIT_TRACKER_STATE = 'SET_AUDIT_TRACKER_STATE';
export const FETCHING = 'FETCHING';
export const ERROR = 'ERROR';
export const ORDERS_RECEIVED_SUCCESS = 'ORDERS_RECEIVED_SUCCESS';
export const SET_ALERT_MESSAGE = 'SET_ALERT_MESSAGE';
export const BLOCKCHAIN_RECEIVED_SUCCESS = 'BLOCKCHAIN_RECEIVED_SUCCESS';
export const SAVE_SHIPMENT_DETAIL = 'SAVE_SHIPMENT_DETAIL';
export const SET_TOTAL_RECORD_COUNT = 'SET_TOTAL_RECORD_COUNT';
export const SET_TOTAL_PAGES = 'SET_TOTAL_PAGES';
export const BRAND_ENTITIES_SUCCESS = 'BRAND_ENTITIES_SUCCESS';
export const SET_SENDER_BUYER_DETAIL = 'SET_SENDER_BUYER_DETAIL';
export const SET_CURRENT_NEW_PAGE = 'SET_CURRENT_NEW_PAGE'
export const SET_GEOLOCATION_DATA = 'SET_GEOLOCATION_DATA'
export const RESET_GEOLOCATION_DATA = 'RESET_GEOLOCATION_DATA'
export const SET_CONFIG_DATA = 'SET_CONFIG_DATA'
export const AUDIT_ERROR_VAL = 'AUDIT_ERROR_VAL'
export const FALSE_AUDIT_ERROR_VAL = 'FALSE_AUDIT_ERROR_VAL'
export const SET_ORDER_AUDIT_VAL = 'SET_ORDER_AUDIT_VAL'
export const ORDER_LISTING_VAL = 'ORDER_LISTING_VAL'
export const RESET_BLOCKCHAIN_DATA = 'RESET_BLOCKCHAIN_DATA'
export const COMPANY_KEY_DOC = 'COMPANY_KEY_DOC'
export const UPLOAD_DOC_DATE = 'UPLOAD_DOC_DATE'
// ------------------------------------
// Actions
// ------------------------------------
export function fetchingAudit(status) {
  return {
    type: FETCHING,
    fetching: status,
  };
}

export function errorFetching(status) {
  return {
    type: ERROR,
    error: status,
  };
}

export function setAlertMeassage(status, message, orderStatus = false) {
  return {
    type: SET_ALERT_MESSAGE,
    showAlert: status,
    alertMessage: message,
    status: orderStatus
  };
}

export function ordersReceivedSuccess(payload) {
  return {
    type: ORDERS_RECEIVED_SUCCESS,
    orders: payload,
  };
}

export function auditRecordDetailSuccess(payload) {
  return {
    type: AUDIT_RECORD_DETAIL_SUCCESS,
    viewAuditDetail: payload,
  };
}

export function setAuditTrackerState(status) {
  return {
    type: SET_AUDIT_TRACKER_STATE,
    isTracker: status,
  };
}

export function blockchainReceivedSuccess(payload) {
  return {
    type: BLOCKCHAIN_RECEIVED_SUCCESS,
    blockchainData: payload,
  };
}

export function saveShipmentDetail(payload) {
  return {
    type: SAVE_SHIPMENT_DETAIL,
    shipmentDetail: payload,
  };
}

export function setTotalRecordCount(payload) {
  return {
    type: SET_TOTAL_RECORD_COUNT,
    totalRecordCount: payload,
  }
}

export function setTotalPages(payload) {
  return {
    type: SET_TOTAL_PAGES,
    pages: payload,
  }
}

export function fetchBrandEntitiesSuccess(payload) {
  return {
    type: BRAND_ENTITIES_SUCCESS,
    brandEntities: payload,
  }
}

export function setSenderBuyerDetail(payload) {
  return {
    type: SET_SENDER_BUYER_DETAIL,
    sender: payload.sender,
    buyer: payload.buyer,
    index: payload.index,
  }
}
export function setCurrentNewPage(payload) {
  return {
    type: SET_CURRENT_NEW_PAGE,
    currentNewPage: payload
  }
}
export function setGeolocationData(payload) {
  return {
    type: SET_GEOLOCATION_DATA,
    geolocationData: payload
  }
}
export function resetGeolocationData(payload) {
  return {
    type: RESET_GEOLOCATION_DATA,
    resetGeolocationData: payload
  }
}
export function setConfigData(payload) {
  return {
    type: SET_CONFIG_DATA,
    configData: payload
  }
}

export function auditError(payload) {
  return {
    type: AUDIT_ERROR_VAL,
    auditErrorVal: payload
  }
}

export function falseAuditError(payload) {
  return {
    type: FALSE_AUDIT_ERROR_VAL,
    auditErrorVal: payload
  }
}
export function setOrderAudit(payload) {
  return {
    type: SET_ORDER_AUDIT_VAL,
    orderAuditVal: payload
  }
}
export function orderListingVal(payload) {
  return {
    type: ORDER_LISTING_VAL,
    orderListing: payload
  }
}

export function resetBlockChainData(payload) {
  return {
    type: RESET_BLOCKCHAIN_DATA,
    blockchainData: payload
  }
}

export function getCompanyKeyDoc(key){
  return{
    type: COMPANY_KEY_DOC,
    companyKeyDoc: key
  }
}
export function uploadDocData(payload){
  return{
    type: UPLOAD_DOC_DATE,
    docData: payload
  }
}
// ------------------------------------
// Action creators
// ------------------------------------
export const setProp = (status, key) => {
  return (dispatch) => {
    switch (key) {
      case 'isTracker':
        dispatch(setAuditTrackerState(status));
        break;
      case 'blockchainData':
        dispatch(blockchainReceivedSuccess(status));
        break;
    }
  }
}

export const resetAlertBox = (showAlert, message) => {
  return (dispatch) => {
    dispatch(setAlertMeassage(showAlert, message));
  }
}


export const fetchBrandEntities = () => {
  return (dispatch) => {
    let endPoint = 'masters/get_all_brands';
    let method = 'get';

    return new Promise((resolve, reject) => {
      axios({
        method: method,
        url: Config.url + endPoint,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetchingAudit(false));
          dispatch(fetchBrandEntitiesSuccess(response.data.data));
        } else if (response.data.error === 1) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        console.log(error)
        dispatch(fetchingAudit(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'There is no brand entity.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const setAuditRecordDetail = (record, isPublicCall, order_id) => {
  return (dispatch) => {
    dispatch(fetchingAudit(true));
    let assetId = record.asset_id;
    let endPoint = 'shipment/audittrail';
    let method = 'post';
    if (isPublicCall) {
      assetId = record;
      endPoint = 'public/audittrail';
      method = 'get';
    }
    return new Promise((resolve, reject) => {
      axios({
        method: method,
        url: Config.url + endPoint,
        data: {
          asset_id: assetId,
          order_id: order_id
        },
        params: {
          asset_id: assetId
        },
        headers: { 'token': getToken() }
      }).then(response => {
        console.log('response =========', response);
        if (response.data.error === 1) {
          dispatch(fetchingAudit(false));
          dispatch(auditError(true))
          // dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetchingAudit(false));
          dispatch(auditRecordDetailSuccess(record));
          dispatch(setAuditTrackerState(true));
          if (response.data.is_Order_Trace === 'undefined' || response.data.is_Order_Trace !== 1) {
            dispatch(setOrderAudit(false))
            if (response.data.value.shipment.length === 5) {
              response.data.value.shipment.push({});
            }
            let completeShipment = isBlockchainCompleted(response.data.value.shipment);
            completeShipment.map((item) => {
              item["new_shipment_date"] = '';
              item["new_invoice_date"] = '';
              if (item.shipment_date != undefined && item.shipment_date !== "") {
                var spdate = (item.shipment_date.split("T"))[0].split("-"),
                  newspdate = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
                item["new_shipment_date"] = newspdate;
              }
              if (item.invoice_date != undefined) {
                var rvdate = (item.invoice_date.split("T"))[0].split("-"),
                  newrvdate = rvdate[2] + "-" + rvdate[1] + "-" + rvdate[0];
                item["new_invoice_date"] = newrvdate;
              }
            })
   
            // dispatch(saveShipmentDetail(filterDataByCompanyType(response.data.value.shipment, 'Birla Cellulose')));
            if(response.data.value.shipment.length ===1 ){
              dispatch(saveShipmentDetail(filterDataByCompanyType(response.data.value.shipment, response.data.value.shipment[0].seller_company_name)));

            }
            else{
              dispatch(saveShipmentDetail(filterDataByCompanyType(response.data.value.shipment, response.data.value.shipment[response.data.value.shipment.length-2].seller_company_name)));
            }

            dispatch(blockchainReceivedSuccess(completeShipment));

          }
          else {
            dispatch(setOrderAudit(true));
            dispatch(blockchainReceivedSuccess(response.data.orderDetail));
            dispatch(saveShipmentDetail((response.data.orderDetail)));
          }
        }
        else if (response.data.error === 1) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }


        resolve(true);
      }).catch(error => {
        dispatch(fetchingAudit(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const setGeoRecordDetail = (record, isPublicCall) => {
  return (dispatch) => {
    dispatch(fetchingAudit(true));
    let assetId = record.asset_id;
    let endPoint = 'shipment/geo_data';
    let method = 'post';
    if (isPublicCall) {
      assetId = record;
      endPoint = 'public/geo_data';
      method = 'get';
    }
    return new Promise((resolve, reject) => {
      axios({
        method: method,
        url: Config.url + endPoint,
        data: {
          asset_id: assetId
        },
        params: {
          asset_id: assetId
        },
        headers: { 'token': getToken() }
      }).then(response => {
        // console.log('response =========', response);
        if (response.data.error === 1) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetchingAudit(false));
          let companyKeyId = getCompanyIdForPulp(response.data)
          dispatch(setGeolocationData(response.data));
          dispatch(getCompanyKeyDoc(companyKeyId))
        }
        resolve(true);

      }).catch(error => {
        dispatch(fetchingAudit(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const setConfigDetail = (record, isPublicCall) => {
  return (dispatch) => {
    dispatch(fetchingAudit(true));
    let assetId = record.asset_id;
    let endPoint = 'shipment/get_audit_display';
    let method = 'get';
    if (isPublicCall) {
      assetId = record;
      endPoint = 'public/get_audit_display';
      method = 'get';
    }
    return new Promise((resolve, reject) => {
      axios({
        method: method,
        url: Config.url + endPoint,
        data: {
          asset_id: assetId
        },
        params: {
          asset_id: assetId
        },
        headers: { 'token': getToken() }
      }).then(response => {
        // console.log("Configure details")
        // console.log('response =========', response);
        if (response.data.error === 1) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetchingAudit(false));
          dispatch(setConfigData(response.data));
        }
        resolve(true);

      }).catch(error => {
        dispatch(fetchingAudit(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}



export const fetchOrders = (key, frmDt = '', toDt = '', query = '', currentPage, pageSize) => {
  return (dispatch) => {
    if (!query) dispatch(fetchingAudit(true));
    let requestMethod = 'post';
    let data = {};
    data = {
      search_text: query,
      from_date: frmDt,
      to_date: toDt,
      page_size: pageSize,
      page_number: currentPage
    }

    let endPoint = (key === 'incomingOrders') ? 'orders/incoming_orders' : 'orders/outgoing_orders';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetchingAudit(false));
          let totalPage = getPages(response.data.total_recordcount, pageSize);
          dispatch(setTotalRecordCount(response.data.total_recordcount));
          dispatch(setTotalPages(totalPage));
          response.data.data.map((item) => {
            item["new_order_date"] = '';
            if (item.order_date != undefined) {
              var spdate = (item.order_date.split("T"))[0].split("-"),
                newspdate = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
              item["new_order_date"] = newspdate;
            }
          })
          dispatch(ordersReceivedSuccess(filterByDate(response.data).data));
        } else if (response.data.error === 1) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetchingAudit(false));
        dispatch(errorFetching(true));
        dispatch(setAlertMeassage(true, 'No Orders Found.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const fetchBrandsOrders = (entityId, frmDt = '', toDt = '', query = '', currentPage, pageSize) => {
  return (dispatch) => {
    if (!query) dispatch(fetchingAudit(true));
    let requestMethod = 'post';
    let data = {};
    data = {
      brand_id: entityId,
      search_text: query,
      from_date: frmDt,
      to_date: toDt,
      page_size: pageSize,
      page_number: currentPage
    }

    let endPoint = 'orders/brand_orders';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        // console.log('response ', response);
        if (response.data.error === 1) {
          dispatch(fetchingAudit(false));
          // if(response.data.data !== "Sql error"){
          //   dispatch(setAlertMeassage(true, response.data.data, false));
          // }
        } else if (response.data.error === 5) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetchingAudit(false));
          let totalPage = getPages(response.data.total_recordcount, pageSize);
          dispatch(setTotalRecordCount(response.data.total_recordcount));
          dispatch(setTotalPages(totalPage));
          response.data.data.map((item) => {
            item["new_order_date"] = '';
            if (item.order_date != undefined) {
              var spdate = (item.order_date.split("T"))[0].split("-"),
                newspdate = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
              item["new_order_date"] = newspdate;
            }
          })
          dispatch(ordersReceivedSuccess(filterByDate(response.data).data));
        } else if (response.data.error === 1) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        console.log(error)
        dispatch(fetchingAudit(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'No Orders Found.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const getDetail = (company_type_name, blockchainData, seller_company_name, complBlchn) => {
  return (dispatch) => {
    if (company_type_name === "Integrated Player") {
      dispatch(saveShipmentDetail(filterIpDataByCompanyType(blockchainData, company_type_name, seller_company_name)));
    }
    else {
      // dispatch(saveShipmentDetail(filterDataByCompanyType(blockchainData, company_type_name)));
      dispatch(saveShipmentDetail(filterIpDataByCompanyType(blockchainData, company_type_name, seller_company_name)));

    }
  }
}

export const getDocData = (company_key_id) => {
  return (dispatch) => {
    let data = {
      company_key_id: company_key_id
    }
    dispatch(fetchingAudit(true));
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: `${Config.url}admin/get_forest_file`,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response);
        if (response.data.error === 1) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetchingAudit(false));
          dispatch(uploadDocData(response.data.documents));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetchingAudit(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'There is no company type.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}
export const downloadDoc = (document_file_name, document_name) => {
  return (dispatch) => {
    dispatch(fetchingAudit(true));
    let endPoint = 'admin/download_linked_forest_file';
    let data = {
      file_name: document_file_name
    };
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() },
        responseType: 'blob',
      }).then(response => {
        console.log(response)
        if (response.data.error === 1) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetchingAudit(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetchingAudit(false));
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          link.href = url;
          link.setAttribute('download', document_name);
          document.body.appendChild(link);
          link.click();

          resolve(true);
        }
      }).catch(error => {
        dispatch(fetchingAudit(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}
export const actions = {
  setProp,
  setAuditRecordDetail,
  getDetail
};

// ------------------------------------
// Action handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [FETCHING]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching,
    }
  },
  [ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error,
    }
  },
  [SET_ALERT_MESSAGE]: (state, action) => {
    return {
      ...state,
      showAlert: action.showAlert,
      alertMessage: action.alertMessage,
      status: action.status,
    }
  },
  [ORDERS_RECEIVED_SUCCESS]: (state, action) => {
    return {
      ...state,
      orders: action.orders,
    }
  },
  [AUDIT_RECORD_DETAIL_SUCCESS]: (state, action) => {
    return {
      ...state,
      viewAuditDetail: action.viewAuditDetail,
    }
  },
  [SET_AUDIT_TRACKER_STATE]: (state, action) => {
    return {
      ...state,
      isTracker: action.isTracker,
    }
  },
  [BLOCKCHAIN_RECEIVED_SUCCESS]: (state, action) => {
    return {
      ...state,
      blockchainData: action.blockchainData,
    }
  },
  [SAVE_SHIPMENT_DETAIL]: (state, action) => {
    return {
      ...state,
      shipmentDetail: action.shipmentDetail,
    }
  },
  [SET_TOTAL_RECORD_COUNT]: (state, action) => {
    return {
      ...state,
      totalRecordCount: action.totalRecordCount,
    }
  },
  [SET_TOTAL_PAGES]: (state, action) => {
    return {
      ...state,
      pages: action.pages,
    }
  },
  [BRAND_ENTITIES_SUCCESS]: (state, action) => {
    return {
      ...state,
      brandEntities: action.brandEntities,
    }
  },
  [SET_SENDER_BUYER_DETAIL]: (state, action) => {
    return {
      ...state,
      sender: action.sender,
      buyer: action.buyer,
      index: action.index,
    }
  },
  [SET_CURRENT_NEW_PAGE]: (state, action) => {
    return {
      ...state,
      currentNewPage: action.currentNewPage
    }
  },
  [SET_GEOLOCATION_DATA]: (state, action) => {
    return {
      ...state,
      geolocationData: action.geolocationData
    }
  },
  [RESET_GEOLOCATION_DATA]: (state, action) => {
    return {
      ...state,
      geolocationData: action.resetGeolocationData
    }
  },
  [SET_CONFIG_DATA]: (state, action) => {
    return {
      ...state,
      configData: action.configData
    }
  },
  [AUDIT_ERROR_VAL]: (state, action) => {
    return {
      ...state,
      auditErrorVal: action.auditErrorVal
    }
  },
  [FALSE_AUDIT_ERROR_VAL]: (state, action) => {
    return {
      ...state,
      auditErrorVal: action.auditErrorVal
    }
  },
  [SET_ORDER_AUDIT_VAL]: (state, action) => {
    return {
      ...state,
      orderAuditVal: action.orderAuditVal
    }
  },
  [ORDER_LISTING_VAL]: (state, action) => {
    return {
      ...state,
      orderListing: action.orderListing
    }
  },
  [RESET_BLOCKCHAIN_DATA]: (state, action) => {
    return {
      ...state,
      blockchainData: action.blockchainData
    }
  },
  [COMPANY_KEY_DOC]: (state, action) => {
    return{
      ...state,
      companyKeyDoc: action.companyKeyDoc
    }
  },
  [UPLOAD_DOC_DATE]: (state, action) => {
    return{
      ...state,
      docData: action.docData
    }
  }
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  fetching: false,
  error: false,
  status: false,
  showAlert: false,
  alertMessage: '',
  orders: [],
  viewAuditDetail: [],
  isTracker: false,
  blockchainData: [],
  shipmentDetail: [],
  totalRecordCount: 0,
  pages: 1,
  brandEntities: [],
  sender: '',
  buyer: '',
  index: 1,
  currentNewPage: 1,
  geolocationData: [],
  configData: '',
  auditErrorVal: false,
  orderAuditVal: false,
  orderListing: false,
  companyKeyDoc: '',
  docData: ''
};

export default function shipmentsReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
