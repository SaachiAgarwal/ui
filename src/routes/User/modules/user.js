import axios from 'axios';
import { browserHistory } from 'react-router';

import {Config, Url} from 'config/Config';
import {getLocalStorage, saveLocalStorage, getToken, getPages, addFullName} from 'components/Helper';
// ------------------------------------
// Constant
// ------------------------------------
export const FETCHING = 'FETCHING';
export const ERROR = 'ERROR';
export const SET_ALERT_MESSAGE = 'SET_ALERT_MESSAGE';
export const USER_RECEIVED_SUCCESS = 'USER_RECEIVED_SUCCESS';
export const SET_TOTAL_RECORDS_COUNT = 'SET_TOTAL_RECORDS_COUNT';
export const SET_TOTAL_PAGES = 'SET_TOTAL_PAGES';
export const SET_USER_FORM_STATE = 'SET_USER_FORM_STATE';
export const FETCHING_COMPANY_TYPE_SUCCESS = 'FETCHING_COMPANY_TYPE_SUCCESS';
export const COMPANY_TYPE_SUCCESS = 'COMPANY_TYPE_SUCCESS';
export const VIEW_USER_DETAIL = 'VIEW_USER_DETAIL';
export const SET_REVIEW_USER_STATE = 'SET_REVIEW_USER_STATE';
export const SET_EDIT_STATE = 'SET_EDIT_STATE';
export const SET_CURRENT_NEW_PAGE = 'SET_CURRENT_NEW_PAGE'
// ------------------------------------
// Actions
// ------------------------------------
export function setAlertMeassage(status, message, orderStatus=false) {
  return {
    type: SET_ALERT_MESSAGE,
    showAlert: status,
    alertMessage: message,
    status: orderStatus
  };
}

export function fetching(status) {
  return {
    type: FETCHING,
    fetching: status,
  };
}

export function errorFetching(status) {
  return {
    type: ERROR,
    error: status,
  };
}

export function userReceivedSuccess(payload) {
  return {
    type: USER_RECEIVED_SUCCESS,
    userDetail: payload,
  };
}

export function setTotalRecordCount(payload) {
  return {
    type: SET_TOTAL_RECORDS_COUNT,
    totalRecordCount: payload,
  };
}

export function setTotalPages(payload) {
  return {
    type: SET_TOTAL_PAGES,
    pages: payload,
  };
}

export function setUserFormState(status) {
  return {
    type: SET_USER_FORM_STATE,
    isUserFormOpen: status,
  };
}

export function fetchingCompanyTypeSuccess(payload) {
  return {
    type: FETCHING_COMPANY_TYPE_SUCCESS,
    companyType: payload,
  };
}

export function companyTypeSuccess(payload) {
  return {
    type: COMPANY_TYPE_SUCCESS,
    companyDetail: payload,
  };
}

export function viewUserDetail(payload) {
  return {
    type: VIEW_USER_DETAIL,
    viewUserDetail: payload,
  };
}

export function setReviewUserState(status) {
  return {
    type: SET_REVIEW_USER_STATE,
    isReviewUser: status,
  };
}

export function setEditState(status) {
  return {
    type: SET_EDIT_STATE,
    isEdit: status,
  };
}
export function setCurrentNewPage(payload){
  return{
    type: SET_CURRENT_NEW_PAGE,
    currentNewPage: payload
  }
}

// ------------------------------------
// Action creators
// ------------------------------------
export const resetAlertBox = (showAlert, message) => {
  return (dispatch) => {
    dispatch(setAlertMeassage(showAlert, message));
  }
}

export const setProps = (status, key) => {
  return (dispatch) => {
    switch(key) {
      case 'isUserFormOpen':
        dispatch(setUserFormState(status));
        break;
      case 'isReviewUser':
        dispatch(setReviewUserState(status));
        break;
      case 'isEdit':
        dispatch(setEditState(status));
        break;
    }
  }  
}

export const getUserDetails = (query='', currentPage=1, pageSize=10) => {
  return (dispatch) => {
    if (!query) dispatch(fetching(true));
    let requestMethod = 'post';
    let data = {};
    data = {
      search_text: query,
      page_number: currentPage,
      page_size: pageSize
    }

   let endPoint = 'admin/list_users';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          let users = addFullName(response.data.data);
          let totalPage = getPages(response.data.total_recordcount);
          dispatch(userReceivedSuccess(users));
          dispatch(setTotalRecordCount(response.data.total_recordcount));
          dispatch(setTotalPages(totalPage));
          resolve(true);
        }
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'No users found.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const getCompanyType = () => {
  return (dispatch) => {
    // dispatch(fetching(true)); // stop loader on company type dropdown
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: `${Config.url}users/get_company_type`,
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(fetchingCompanyTypeSuccess(response.data));
        }
        resolve(true);
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'There is no company type.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const getCompany = (company_type_id) => {
  return (dispatch) => {
    // dispatch(fetching(true)); //stop loader on Company dropdown
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: `${Config.url}users/get_company`,
        params: {
          company_type_id:company_type_id
        },
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(companyTypeSuccess(response.data));
        }
        resolve(true);
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'Something went wrong.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const addUser = (values) => {
  return (dispatch) => {
   dispatch(fetching(true));
   let requestMethod = 'post';
   let data = values;
   let endPoint = 'users/register';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(setUserFormState(false));
          dispatch(setAlertMeassage(true, response.data.data, true));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'User can not be added.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const updateUser = (values) => {
  return (dispatch) => {
   dispatch(fetching(true));
   let requestMethod = 'post';
   let data = values;
   let endPoint = 'admin/update_user';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(setUserFormState(false));
          dispatch(setAlertMeassage(true, response.data.data, true));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'User can not be updated.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const viewUserDetails = (userId) => {
  return (dispatch) => {
   dispatch(fetching(true));
   let requestMethod = 'get';
   let data = {user_id: userId};
   let endPoint = 'admin/user_details';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        params: data,
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          let lStorage = {
            user_id: response.data.data.user_id
          }
          saveLocalStorage('userDetail', lStorage);
          dispatch(viewUserDetail(response.data.data));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch( error => {
        console.log(error);
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'Can not get user Detail.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const deleteUser = (userId) => {
  return (dispatch) => {
   dispatch(fetching(true));
   let requestMethod = 'post';
   let data = {user_id: userId};
   let endPoint = 'admin/delete_user';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, true));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch( error => {
        console.log(error);
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'User can not be deleted.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const action = {
  getUserDetails,
};

// ------------------------------------
// Action handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_ALERT_MESSAGE]: (state, action) => {
    return {
      ...state,
      showAlert: action.showAlert,
      alertMessage: action.alertMessage,
      status: action.status
    }
  },
  [FETCHING]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching
    }
  },
  [ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
  [USER_RECEIVED_SUCCESS]: (state, action) => {
    return {
      ...state,
      userDetail: action.userDetail
    }
  },
  [SET_TOTAL_RECORDS_COUNT]: (state, action) => {
    return {
      ...state,
      totalRecordCount: action.totalRecordCount
    }
  },
  [SET_TOTAL_PAGES]: (state, action) => {
    return {
      ...state,
      pages: action.pages
    }
  },
  [SET_USER_FORM_STATE]: (state, action) => {
    return {
      ...state,
      isUserFormOpen: action.isUserFormOpen
    }
  },
  [FETCHING_COMPANY_TYPE_SUCCESS]: (state, action) => {
    return {
      ...state,
      companyType: action.companyType
    }
  },
  [COMPANY_TYPE_SUCCESS]: (state, action) => {
    return {
      ...state,
      companyDetail: action.companyDetail
    }
  },
  [VIEW_USER_DETAIL]: (state, action) => {
    return {
      ...state,
      viewUserDetail: action.viewUserDetail
    }
  },
  [SET_REVIEW_USER_STATE]: (state, action) => {
    return {
      ...state,
      isReviewUser: action.isReviewUser
    }
  },
  [SET_EDIT_STATE]: (state, action) => {
    return {
      ...state,
      isEdit: action.isEdit
    }
  },
  [SET_CURRENT_NEW_PAGE]: (state, action) => {
    return{
      ...state,
      currentNewPage: action.currentNewPage
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  showAlert: false,
  status: false,
  alertMessage: '',
  fetching: false,
  error: false,
  userDetail: [],
  totalRecordCount: 0,
  pages: 1,
  isUserFormOpen: false,
  companyType: [],
  companyDetail: [],
  viewUserDetail: [],
  isReviewUser: false,
  isEdit: false,
  currentNewPage: 1
};

export default function userReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
