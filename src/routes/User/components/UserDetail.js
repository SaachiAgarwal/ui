import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Pagination from 'react-paginate';

/** UserDetail
 *
 * @description This class is responsible to display user records
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class UserDetail extends Component {
  constructor(props) {
    super(props);
    this.handlePageClick = this.handlePageClick.bind(this);
  }

  handlePageClick(value) {
    this.props.getUserDetails(this.props.searchText, value.selected + 1, this.props.options.pageSize);
    this.props.setCurrentNewPage(value.selected + 1)
  }

  render() {
    return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options}>
          <TableHeaderColumn dataField='user_id' hidden={true} isKey={true}>Order Id</TableHeaderColumn>
          <TableHeaderColumn dataField='full_name'>Full Name</TableHeaderColumn>
          <TableHeaderColumn dataField='company_name'>Company</TableHeaderColumn>
          <TableHeaderColumn dataField='company_type_name'>Role</TableHeaderColumn>
          <TableHeaderColumn dataField='email'>Email ID</TableHeaderColumn>
          <TableHeaderColumn key="editAction" dataFormat={this.props.userFormatter}>Action</TableHeaderColumn>
        </BootstrapTable>
        <div className="pagination-box">
          <Pagination
            initialPage={this.props.currentNewPage - 1}
            previousLabel={`Prev`}
            nextLabel={`Next`}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={this.props.pages}
            pageRangeDisplayed={5}
            onPageChange={this.handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        </div>
      </div>
    );
  }
}

UserDetail.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default UserDetail;