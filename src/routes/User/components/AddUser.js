import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Button } from 'reactstrap';
import _ from 'lodash';
import DropdownList from 'react-widgets/lib/DropdownList';
import 'react-widgets/dist/css/react-widgets.css';

import renderField from './renderField';
import SubmitButtons from 'components/SubmitButtons';
import { getCompanyTypeId, getCompanyId } from 'components/Helper';


/** func Validate
 * description validate method will validate all the control fields of form based on provided criteria
 *
 * return Array of Errors
 */
const validate = values => {
  const errors = {}
  /*if (!values.company_type) {
    errors.company_type = 'Required'
  }*/
  if (!values.company_key_id) {
    errors.company_key_id = 'Required'
  }
  if (!values.first_name) {
    errors.first_name = 'Required'
  }
  if (!values.last_name) {
    errors.last_name = 'Required'
  }
  /*if (!values.phone_number) {
    errors.phone_number = 'Required'
  } else if (values.phone_number.length !== 10) {
    errors.phone_number = 'Invalid phone number'
  }*/
  if (!values.email) {
    errors.email = 'Required'
  }
  if (!values.password) {
    errors.password = 'Required'
  } else if (values.password !== values.confirm_password) {
    errors.confirm_password = 'Password does not match'
  }
  if (!values.confirm_password) {
    errors.confirm_password = 'Required'
  }
  return errors
}

const renderDropdownList = ({ input, label, data, valueField, textField, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className={`form-group`}>
      <label className="control-label">{label}</label>
      <div>
        <DropdownList {...input}
          data={data}
          onChange={input.onChange}
        />
        {submitFailed && error && <span className="error-danger">{error}</span>}
      </div>
    </div>
  );
}

const renderCompanyType = ({ input, label, data, valueField, textField, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className={`form-group`}>
      <label className="control-label">{label}</label>
      <div>
        <DropdownList {...input}
          data={data}
          onChange={input.onChange}
          onSelect={customProps.fetchCompanyType(input.value)}
        />
        {submitFailed && error && <span className="error-danger">{error}</span>}
      </div>
    </div>
  );
}

const rendereMailField = ({ input, label, data, valueField, textField,type, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className={`form-group`}>
      <label className="control-label">{label}</label>
      <div>
        <input {...input}
          className="form-control"
          type={type}
          autoComplete="off"
          autoCorrect="off"
          spellCheck="off"
          multiple
          />

        <div className="help-block">
          {touched && (error && <span className="error-danger">
            <i className="fa fa-exclamation-circle">{error}</i></span>)}
        </div>
      </div>
    </div>
  );
}

/** func readOnly
 *
 * @description This method returns a readOnly component for fields
 *
 * return A component
 */
const readOnly = ({ input, label, type, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className="btn-bs-file">
      <span className='form-control'>
        <p>{customProps.value}</p>
        {(input.name === 'linkTo') &&
          <span>
            <img src={Images.fileUploadIcon} alt="" data-toggle="modal" data-target="#myModal" />
          </span>
        }
      </span>
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

/** AddUser
 *
 * @description This class is a based class for User form functionality
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class AddUser extends React.Component {
  constructor(props) {
    super(props);
    this.fetchCompanyType = this.fetchCompanyType.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);

    this.state = {
      initData: {},
      emails: [],
    }
  }

  componentWillMount() {
    this.props.setActivePage('adduser');
    if (!this.props.isEdit) this.props.getCompanyType();
  }

  componentDidMount() {
    if (this.props.isEdit) this.handleInitialize();
  }

  handleInitialize() {
    if (!_.isEmpty(this.props.viewUserDetail)) {
      const initData = {
        company_type: getCompanyTypeId(this.props.companyType, this.props.viewUserDetail.company_type_id, true),
        company_key_id: getCompanyId(this.props.companyDetail, this.props.viewUserDetail.company_key_id, true),
        first_name: this.props.viewUserDetail.first_name,
        last_name: this.props.viewUserDetail.last_name,
        phone_number: this.props.viewUserDetail.phone_number,
        email: this.props.viewUserDetail.email,
        notify_usersemail: this.props.viewUserDetail.notify_users_email,
        password: '......',
        confirm_password: '......',
      };

      this.setState({ initData: initData });
      this.props.initialize(initData);
    }
  }

  handleEmailChange(value){
    let emails=value
    //this.setState({emails:emails})
  }

  componentWillUnmount() {
    this.props.setActivePage('user');
    this.props.setProps(false, 'isUserFormOpen');
    this.props.setProps(false, 'isEdit');
  }

  fetchCompanyType(value) {
    if (typeof value !== 'undefined' && value !== '') {
      let company_type_id = getCompanyTypeId(this.props.companyType, value, false);
      this.props.getCompany(company_type_id);
    }
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div className='col-md-12 col-lg-12 create-order'>
        <h2>User Details</h2>
        <div>
          <form onSubmit={handleSubmit}>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  className="select-menu"
                  name="company_type"
                  label="Company Type"
                  component={renderCompanyType}
                  data={(!_.isEmpty(this.props.companyType)) ? this.props.companyType.map(function (list) { return list.company_type_name }) : []}
                  customProps={{
                    fetchCompanyType: this.fetchCompanyType
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  className="select-menu"
                  name="company_key_id"
                  label="Select company *"
                  component={renderDropdownList}
                  data={(!_.isEmpty(this.props.companyDetail)) ? this.props.companyDetail.map(function (list) { return list.company_name }) : []}
                />
              </div>
            </div>
            <h2>Contact Details</h2>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='first_name'
                  type="text"
                  component={renderField}
                  label="First Name *"
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name='last_name'
                  type="text"
                  component={renderField}
                  label="Last Name *"
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='phone_number'
                  type="text"
                  component={renderField}
                  label="Phone"
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name='email'
                  type="email"
                  component={renderField}
                  label="Email *"
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name='notify_usersemail'
                  type="email"
                  component={rendereMailField}
                  label="Notify User Email"
                  customProps={{
                    handleEmailChange:this.handleEmailChange,
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.emails : ''
                  }}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <label>Password</label>
                <Field
                  name='password'
                  type="password"
                  component={(!this.props.isEdit) ? renderField : readOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.password : '......'
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <label>Confirm Password</label>
                <Field
                  name='confirm_password'
                  type="password"
                  component={(!this.props.isEdit) ? renderField : readOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.confirm_password : '......'
                  }}
                />
              </div>
            </div>
            <div>
              <SubmitButtons
                submitLabel={'Add'}
                className='btn btn-primary create'
                submitting={this.props.submitting}
              />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

AddUser.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default reduxForm({
  form: 'AddUser',
  validate,
})(AddUser)