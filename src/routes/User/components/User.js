import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Button } from 'reactstrap';
import _ from 'lodash';
import Swal from 'sweetalert2';

import Loader from 'components/Loader';
import options from 'components/options';
import Alert from "components/Alert";
import HeaderSection from 'components/HeaderSection';
import NavDrawer from 'components/NavDrawer';
import { getLocalStorage, getCompanyId } from 'components/Helper';
import UserDetail from './UserDetail';
import AddUser from './AddUser';
import ReviewUser from './ReviewUser';
import Footer from 'components/Footer';
import 'css/style.scss';
import ReactLoading from 'react-loading';

/** User
 *
 * @description This class is a based class for User stuff
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class User extends React.Component {
  constructor(props) {
    super(props);
    this.confirm = this.confirm.bind(this);
    this.handleAlertBox = this.handleAlertBox.bind(this);
    this.handleInboxOutboxOrders = this.handleInboxOutboxOrders.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.openUserForm = this.openUserForm.bind(this);
    this.userFormatter = this.userFormatter.bind(this);
    this.viewUser = this.viewUser.bind(this);
    this.editUser = this.editUser.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
    this.createUser = this.createUser.bind(this);
    this.back = this.back.bind(this);
    this.setActivePage = this.setActivePage.bind(this);

    this.state = {
      active: 'user',
      role: '',
      isSuperUser: '',
      searchText: '',
      previousActivePage: '',
    }
  }

  async componentWillMount() {
    let loginDetail = getLocalStorage('loginDetail');
    await this.setState({
      role: loginDetail.role,
      isSuperUser: loginDetail.userInfo.is_super_user
    });
    this.props.setGlobalState({
      navIndex: 8,
      navInnerIndex: 0,
      navClass: 'User',
    });
    // getting user details
    this.props.getUserDetails();
  }
  componentWillUnmount() {
    this.props.setCurrentNewPage(1)
  }

  async handleSearch(query) {
    await this.setState({ searchText: query });
    if (query.length >= 2 || query.length === 0) this.props.getUserDetails(this.state.searchText, options.currentPage, options.pageSize);
  }

  async handleAlertBox() {
    this.props.resetAlertBox(false, "");
  }

  handleInboxOutboxOrders(classType, innerType) {
    if (classType === 'orders' && innerType === 'inbox') {
    } else if (classType === 'orders' && innerType === 'outbox') {
    } else if (classType === 'shipments' && innerType === 'inbox') {
    } else if (classType === 'Audit Trail') {
    } else if (classType === 'logout') {
      removeLocalStorage('loginDetail');
      browserHistory.push(Url.HOME_PAGE);
    }
  }

  async setActivePage(state) {
    this.setState({ active: state });
  }

  async openUserForm() {
    await this.setState({ previousActivePage: this.state.active });
    await this.setState({ active: 'adduser' });
    this.props.setProps(true, 'isUserFormOpen');
  }

  confirm(id = '', key = '', msg, confirmText, cancelText) {
    Swal({
      title: 'Warning!',
      text: `${msg}`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmText,
      cancelButtonText: cancelText
    }).then(async (result) => {
      this.props.setProps(false, 'isConfirm');
      if (result.value) {
        if (key === 'deleteUser') {
          await this.props.deleteUser(id);
          this.props.getUserDetails();
        }
      }
    })
  }

  async viewUser(row) {
    await this.setState({ previousActivePage: this.state.active });
    await this.props.viewUserDetails(row.user_id);
    await this.props.getCompanyType();// return companyType
    await this.props.getCompany(this.props.viewUserDetail.company_type_id);// return companyDetail
    this.props.setProps(true, 'isReviewUser');
    this.props.setProps(true, 'isUserFormOpen');
  }

  async editUser(row) {
    await this.setState({ previousActivePage: this.state.active });
    await this.props.viewUserDetails(row.user_id);
    await this.props.getCompanyType();// return companyType
    await this.props.getCompany(this.props.viewUserDetail.company_type_id);// return companyDetail
    this.props.setProps(true, 'isEdit');
    this.props.setProps(true, 'isUserFormOpen');
  }

  deleteUser(row) {
    this.confirm(row.user_id, 'deleteUser', 'Do you really want to delete', 'Yes delete it', 'No keep it');
  }

  back() {
    this.setState({ active: this.state.previousActivePage });
    this.props.setProps(false, 'isUserFormOpen');
    this.props.getUserDetails('', this.props.currentNewPage, options.PageSize)
  }

  async createUser(values) {
    let data = {
      first_name: values.first_name,
      last_name: values.last_name,
      email: values.email,
      phone_number: values.phone_number,
      password: values.password,
      company_key_id: getCompanyId(this.props.companyDetail, values.company_key_id, false),
      phone_number: values.phone_number,
      notify_usersemail: values.notify_usersemail
    }

    if (this.props.isEdit) {
      let lStorage = getLocalStorage('userDetail');
      data.user_id = lStorage.user_id;
      await this.props.updateUser(data);
    } else {
      await this.props.addUser(data);
    }
    this.props.getUserDetails();
  }

  userFormatter(row, cell) {
    return (
      <ButtonGroup>
        <Button className="action" onClick={() => this.viewUser(cell)} >view</Button>
        <Button className="action" onClick={() => this.editUser(cell)} >edit</Button>
        <Button className="action delete" onClick={() => this.deleteUser(cell)} >delete</Button>
      </ButtonGroup>
    );
  }

  render() {
    return (
      <div>
        {/* <Loader loading={this.props.fetching} /> */}
        {this.props.fetching &&
          <ReactLoading type="bubbles" style={{
            fill: '#4e5be1e0',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundSize: '200px',
            position: 'fixed',
            zIndex: 10000,
            width: '6%',
            height: '72%',
            top: '50%',
            left: '50%'
          }} />
        }
        <Alert
          showAlert={(typeof this.props.showAlert !== "undefined" ? this.props.showAlert : false)}
          message={(typeof this.props.alertMessage !== "undefined" ? this.props.alertMessage : "")}
          handleAlertBox={this.handleAlertBox}
          status={this.props.status}
        />
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-2 col-lg-2 sidebar">
              <NavDrawer
                role={this.state.role}
                isSuperUser={this.state.isSuperUser}
                setGlobalState={this.props.setGlobalState}
                navIndex={this.props.navIndex}
                navInnerIndex={this.props.navInnerIndex}
                navClass={this.props.navClass}
                handleInboxOutboxOrders={this.handleInboxOutboxOrders}
                currentLanguage={this.props.currentLanguage}
                setInnerNav={this.props.setInnerNav}
                setParam={this.props.setParam}
                setSearchString={this.props.setSearchString}
                setReportFetching={this.props.setReportFetching}
              />
            </div>
            <div className="col-md-10 col-lg-10 col-lg-offset-2 col-md-offset-2 main">
              <HeaderSection
                active={this.state.active}
                searchText={this.state.searchText}
                handleSearch={this.handleSearch}
                currentLanguage={this.props.currentLanguage}
                setCurrentLanguage={this.props.setCurrentLanguage}
              />
              {(!this.props.isUserFormOpen) ?
                <div>
                  <UserDetail
                    data={this.props.userDetail}
                    pagination={false}
                    search={false}
                    exportCSV={false}
                    options={options}
                    userFormatter={this.userFormatter}
                    getUserDetails={this.props.getUserDetails}
                    pages={this.props.pages}
                    totalRecordCount={this.props.totalRecordCount}
                    searchText={this.state.searchText}
                    setCurrentNewPage={this.props.setCurrentNewPage}
                    currentNewPage={this.props.currentNewPage}
                  />
                  <Button className='add-button' hidden={(this.props.isUserFormOpen)} onClick={() => this.openUserForm()}>+</Button>
                </div>
                :
                <div>
                  {(!this.props.isReviewUser) ?
                    <AddUser
                      onSubmit={this.createUser}
                      setActivePage={this.setActivePage}
                      companyType={this.props.companyType}
                      companyDetail={this.props.companyDetail}
                      getCompanyType={this.props.getCompanyType}
                      getCompany={this.props.getCompany}
                      setProps={this.props.setProps}
                      isEdit={this.props.isEdit}
                      viewUserDetail={this.props.viewUserDetail}
                    />
                    :
                    <ReviewUser
                      setActivePage={this.setActivePage}
                      setProps={this.props.setProps}
                      companyType={this.props.companyType}
                      companyDetail={this.props.companyDetail}
                      viewUserDetail={this.props.viewUserDetail}
                      companyDetail={this.props.companyDetail}
                    />
                  }
                  <div className='btn-right'>
                    <Button className={(this.props.isReviewUser) ? "btn btn-primary reviewCancel" : "btn btn-primary  create cancel"} onClick={this.back}>Back</Button>
                  </div>
                </div>
              }
              <div className="clearfix"></div>
              <Footer
                currentLanguage={this.props.currentLanguage}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

User.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default User;