import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Button } from 'reactstrap';
import _ from 'lodash';

import renderField from './renderField';
import SubmitButtons from 'components/SubmitButtons';
import { getCompanyTypeId, getCompanyId } from 'components/Helper';

/** func renderLink
 *
 * @description This method returns a field object having a span to achieve onClick functionality. Simply we can not use
 *   onClick on field wrapper.
 *
 * return A component
 */
const renderLink = ({ input, label, type, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className="btn-bs-file">
      <span className='form-control'>
        <p>{customProps.value}</p>
        {(input.name === 'linkTo') &&
          <span>
            <img src={Images.fileUploadIcon} alt="" data-toggle="modal" data-target="#myModal" />
          </span>
        }
      </span>
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

const rendereMailField = ({ input, label, data, valueField, textField,type, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className={`form-group`}>
      <label className="control-label">{label}</label>
      <div>
        <input {...input}
          className="form-control"
          type={type}
          autoComplete="off"
          autoCorrect="off"
          spellCheck="off"
          multiple
          />

        <div className="help-block">
          {touched && (error && <span className="error-danger">
            <i className="fa fa-exclamation-circle">{error}</i></span>)}
        </div>
      </div>
    </div>
  );
}

/** ReviewUser
 *
 * @description This class is a based class to view user detail
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class ReviewUser extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      initData: {},
    }
  }

  componentDidMount() {
    this.props.setActivePage('reviewUser');
    this.handleInitialize();
  }

  handleInitialize() {
    if (!_.isEmpty(this.props.viewUserDetail)) {
      const initData = {
        company_type: getCompanyTypeId(this.props.companyType, this.props.viewUserDetail.company_type_id, true),
        company_key_id: getCompanyId(this.props.companyDetail, this.props.viewUserDetail.company_key_id, true),
        first_name: this.props.viewUserDetail.first_name,
        last_name: this.props.viewUserDetail.last_name,
        phone_number: this.props.viewUserDetail.phone_number,
        email: this.props.viewUserDetail.email,
        notify_usersemail: this.props.viewUserDetail.notify_users_email,
      };

      this.setState({ initData: initData });
      this.props.initialize(initData);
    }
  }

  componentWillUnmount() {
    this.props.setProps(false, 'isReviewUser');
    this.props.setProps(false, 'isUserFormOpen');
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div className='col-md-12 col-lg-12 create-order'>
        <h2>User Details</h2>
        <div>
          <form onSubmit={handleSubmit}>
            <div className="form-row">
              <div className="form-group col-md-4">
                <label>Company Type</label>
                <Field
                  className="select-menu"
                  name="company_type"
                  label="Company Type"
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.company_type : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <label>Select Company</label>
                <Field
                  className="select-menu"
                  name="company_key_id"
                  label="select company *"
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.company_key_id : ''
                  }}
                />
              </div>
            </div>
            <h2>Contact Details</h2>
            <div className="form-row">
              <div className="form-group col-md-4">
                <label>First Name</label>
                <Field
                  name='first_name'
                  type="text"
                  label="First Name *"
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.first_name : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <label>Last Name</label>
                <Field
                  name='last_name'
                  type="text"
                  label="Last Name *"
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.last_name : ''
                  }}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <label>Phone</label>
                <Field
                  name='phone_number'
                  type="text"
                  label="Phone *"
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.phone_number : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <label>Email</label>
                <Field
                  name='email'
                  type="email"
                  label="Email *"
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.email : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name='notify_usersemail'
                  type="email"
                  component={rendereMailField}
                  label="Notify User Email"
                  customProps={{
                    handleEmailChange: this.handleEmailChange,
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.emails : ''
                  }}
                />
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

ReviewUser.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default reduxForm({
  form: 'ReviewUser',
})(ReviewUser)