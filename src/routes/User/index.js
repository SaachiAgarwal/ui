import { injectReducer } from '../../store/reducers';

export default (store) => ({
  path: 'user',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const User = require('./containers/UserContainer').default;
      const reducer = require('./modules/user').default;
      injectReducer(store, { key: 'User', reducer });
      cb(null, User);
  }, 'User');
  },
});
