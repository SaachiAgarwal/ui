/** UserContainer
 *
 * @description This is container that manages the states for all User related stuff.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */

import { connect } from 'react-redux';

import User from '../components/User';
import {getUserDetails, resetAlertBox, setProps, addUser, updateUser, getCompanyType, getCompany, viewUserDetails, deleteUser, setCurrentNewPage} from '../modules/user';
import {setReportFetching,setSearchString,setParam,setInnerNav,setGlobalState, setCurrentLanguage} from '../../../store/app';

const mapStateToProps = (state) => {
  return({
    navIndex: state.app.navIndex,
    navInnerIndex: state.app.navInnerIndex,
    navClass: state.app.navClass,
    showAlert: state.User.showAlert,
    status: state.User.status,
    alertMessage: state.User.alertMessage,
    fetching: state.User.fetching,
    error: state.User.error,
    userDetail: state.User.userDetail,
    totalRecordCount: state.User.totalRecordCount,
    pages: state.User.pages,
    isUserFormOpen: state.User.isUserFormOpen,
    companyType: state.User.companyType,
    companyDetail: state.User.companyDetail,
    viewUserDetail: state.User.viewUserDetail,
    isReviewUser: state.User.isReviewUser,
    isEdit: state.User.isEdit,
    currentNewPage: state.User.currentNewPage,
    currentLanguage: state.app.currentLanguage
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    setGlobalState: (value) => dispatch(setGlobalState(value)),
    resetAlertBox: (status, message) => dispatch(resetAlertBox(status, message)),
    getUserDetails: (query, currentPage, pageSize) => dispatch(getUserDetails(query, currentPage, pageSize)),
    setProps: (status, key) => dispatch(setProps(status, key)),
    addUser: (values) => dispatch(addUser(values)),
    updateUser: (values) => dispatch(updateUser(values)),
    getCompanyType: () => dispatch(getCompanyType()),
    getCompany: (id) => dispatch(getCompany(id)),
    viewUserDetails: (userId) => dispatch(viewUserDetails(userId)),
    deleteUser: (userId) => dispatch(deleteUser(userId)),
    setCurrentNewPage: (page) => dispatch(setCurrentNewPage(page)),
    setCurrentLanguage: (lang) => dispatch(setCurrentLanguage(lang)),
    setInnerNav: (lang) => dispatch(setInnerNav(lang)),
    setParam: (lang) => dispatch(setParam(lang)),
    setSearchString: (lang) => dispatch(setSearchString(lang)),
    setReportFetching: (lang) => dispatch(setReportFetching(lang))
  });
};

export default connect(mapStateToProps, mapDispatchToProps)(User);
