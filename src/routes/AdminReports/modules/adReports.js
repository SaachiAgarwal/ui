import axios from 'axios';
import { browserHistory } from 'react-router';

import {Config, Url} from 'config/Config';
import {getToken} from 'components/Helper';
// ------------------------------------
// Constant
// ------------------------------------
export const SET_ALERT_MESSAGE = 'SET_ALERT_MESSAGE';
export const FETCHING = 'FETCHING';
export const FETCHING_ERROR = 'FETCHING_ERROR';

// ------------------------------------
// Actions
// ------------------------------------
export function setAlertMeassage(status, message, orderStatus=false) {
  return {
    type: SET_ALERT_MESSAGE,
    showAlert: status,
    alertMessage: message,
    status: orderStatus
  };
}

export function fetching (status) {
  return {
    type: FETCHING,
    fetching: status
  }
}

export function errorFetching (status) {
  return {
    type: FETCHING_ERROR,
    error: status
  }
}

// ------------------------------------
// Action creators
// ------------------------------------
export const resetAlertBox = (showAlert, message) => {
  return (dispatch) => {
    dispatch(setAlertMeassage(showAlert, message));
  }
}


export const downloadReport = () => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'admin/admin_reports';
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: Config.url + endPoint,
        headers: { 'token': getToken() },
        responseType: 'blob',
      }).then(response => {
        console.log(response)
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          //browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          link.href = url;
          
          link.setAttribute('download', 'admin_reports.xlsx');
          document.body.appendChild(link);
          link.click();
  
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}





export const action = {
  
};
// ------------------------------------
// Action handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_ALERT_MESSAGE]: (state, action) => {
    return {
      ...state,
      showAlert: action.showAlert,
      alertMessage: action.alertMessage,
      status: action.status,
    }
  },
  [FETCHING]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching
    }
  },
  [FETCHING_ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  showAlert: false,
  alertMessage: '',
  status: false,
  fetching: false,
  error: false,

};

export default function adReportsReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
