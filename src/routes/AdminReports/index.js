import { injectReducer } from '../../store/reducers';
import {checkPageRestriction} from  '../index';

export default (store) => ({
  path: 'adminreports',
  onEnter: (nextState, replace) => {
    checkPageRestriction(nextState, replace, () => {})
  },
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const AdminReports = require('./containers/adReportsContainer').default;
      const reducer = require('./modules/adReports').default;
      injectReducer(store, { key: 'AdminReports', reducer});
      cb(null, AdminReports);
  }, 'AdminReports');
  },
});
