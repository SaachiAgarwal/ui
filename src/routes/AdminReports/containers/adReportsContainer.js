/** SettingsContainer
 *
 * @description This is container that manages the states for all settings related stuff.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */

import { connect } from 'react-redux';

import AdminReports from '../components/AdminReports';
import {updateConfiguration, resetAlertBox, setConfigDetail, downloadReport} from '../modules/adReports';
import {setInnerNav,setParam,setSearchString,setReportFetching,setGlobalState, setCurrentLanguage} from '../../../store/app';

const mapStateToProps = (state) => {
  return({
    navIndex: state.app.navIndex,
    navInnerIndex: state.app.navInnerIndex,
    navClass: state.app.navClass,
    fetching: state.AdminReports.fetching,
    error: state.AdminReports.error,
    showAlert: state.AdminReports.showAlert,
    alertMessage: state.AdminReports.alertMessage,
    status: state.AdminReports.status,
    currentLanguage: state.app.currentLanguage
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    setGlobalState: (value) => dispatch(setGlobalState(value)),
    resetAlertBox: (showAlert, message) => dispatch(resetAlertBox(showAlert, message)),
    setCurrentLanguage: (lang) => dispatch(setCurrentLanguage(lang)),
    setInnerNav: (lang) => dispatch(setInnerNav(lang)),
    setParam: (lang) => dispatch(setParam(lang)),
    setSearchString: (lang) => dispatch(setSearchString(lang)),
    setReportFetching: (lang) => dispatch(setReportFetching(lang)),
    downloadReport: () => dispatch(downloadReport())
  });
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminReports);
