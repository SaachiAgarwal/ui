import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Button } from 'reactstrap';
import { Images } from 'config/Config';
import _ from 'lodash';

import Loader from 'components/Loader';
import Alert from "components/Alert";
import HeaderSection from 'components/HeaderSection';
import NavDrawer from 'components/NavDrawer';
import { getLocalStorage } from 'components/Helper';

import { Card, CardHeader, CardBody, CardFooter } from 'react-simple-card';

import Footer from 'components/Footer';
import 'css/style.scss';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
import { point } from 'leaflet';

let strings = new LocalizedStrings(
  data
);

/** Settings
 *
 * @description This class is a based class for settings stuff
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class Download extends React.Component {
  constructor(props) {
    super(props);
    this.download = this.download.bind(this)
  }

  async download() {
    console.log("in download")
    this.props.downloadReport()
  }
  render() {
    // const {currentLanguage}  = this.props;
    // strings.setLanguage(currentLanguage);
    return (
      <div style={{ paddingLeft: '1%' }}>
        {/*         
        <Button
            
            className="btn btn-primary"
            onClick={() => this.download()}
          >
            Download
          </Button> */}
        <div onClick={() => this.download()} style={{ cursor: 'pointer', width: '145px' }} >
          <Card className="card-1" style={{ borderRadius: '9px', width: '100%', height: '170px', backgroundImage:`url(${Images.csvImage})`,backgroundRepeat: 'no-repeat', backgroundSize: '140px' }}  >

            <CardBody>
              <div style={{ fontSize: '18px', fontWeight: 'bold', fontFamily: 'Lemonada', color: '#4e78e1', paddingTop: '59%' }} >
                Download Admin Reports
             </div>
              {/* <Button
                className="btn btn-link"
                onClick={() => this.download()}
                style={{ marginTop: '8%' }}
              >
                Download
          </Button> */}

            </CardBody>


          </Card>
        </div>
      </div>
    );
  }
}

Download.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default Download;