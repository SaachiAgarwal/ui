/** login reducer
 *
 * @description This is a reducer responsible for all the login/regestering/forgot functionality
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
import axios from 'axios';
import { reset } from 'redux-form';
import { browserHistory } from 'react-router';
import _ from 'lodash';

import { saveLocalStorage, getLocalStorage, getToken } from 'components/Helper';
import { Config, Url } from 'config/Config';

//----------------------------
// Constants
//----------------------------

export const FETCHING_FORM_DETAIL = 'FETCHING_FORM_DETAIL';
export const ERROR_FORM_DETAIL = 'ERROR_FORM_DETAIL';
export const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
export const SET_ALERT_MESSAGE = "SET_ALERT_MESSAGE";
export const SENDING_EMAIL = "SENDING_EMAIL";
export const SENDING_EMAIL_ERROR = "SENDING_EMAIL_ERROR";
export const CHANGE_TC = "CHANGE_TC"

//-----------------------------
// Actions
//-----------------------------
export function fetchingFormDetail(status) {
  return {
    type: FETCHING_FORM_DETAIL,
    fetching: status
  }
}

export function errorFormDetail(status) {
  return {
    type: ERROR_FORM_DETAIL,
    error: status
  }
}

export function userLoginSuccess(payload) {
  return {
    type: USER_LOGIN_SUCCESS,
    loginDetail: payload
  }
}

export function setAlertMeassage(status, message, orderStatus=false) {
  return {
    type: SET_ALERT_MESSAGE,
    showAlert: status,
    alertMessage: message,
    status: orderStatus
  };
}

export function sendingMail(status) {
  return {
    type: SENDING_EMAIL,
    fetching: status,
  };
}

export function sendingMailError(status) {
  return {
    type: SENDING_EMAIL_ERROR,
    error: status,
  };
}

export function changeTcStatus(status){
  return{
    type: CHANGE_TC,
    tcLogin: status
  }
}
// ------------------------------------
// Actions creators
// ------------------------------------

export const loginUser = (values) => {
  return (dispatch) => {
    dispatch(fetchingFormDetail(true));
    axios({
      method: 'post',
      url: `${Config.url}users/authenticate`,
      data: values,
      headers: {'token': getToken()}
    }).then( response => {
      // console.log(response.data);
      if (!_.isEmpty(response) && typeof response.data !== 'undefined' && !_.isEmpty(response.data) && response.data.error !== 1) {
        console.log('user detail',response.data)
        let loginDetail = {
          token: response.data.token,
          userInfo: response.data.user_info,
          role: (response.data.user_info.is_super_user) ? 'Admin' : (response.data.user_info.is_accounts) ? 'Account' : response.data.user_info.company_type_name,
          loginTime: new Date().getTime() 
        }
        saveLocalStorage('loginDetail', loginDetail);
        dispatch(fetchingFormDetail(false));
        dispatch(userLoginSuccess(response.data));
        if (loginDetail.userInfo.is_super_user) {
          browserHistory.push(Url.COMPANY_PAGE);
        } else if(response.data.user_info.is_accounts) {
          console.log('sadsadsadsadsaddsadsads.......................')
          browserHistory.push(Url.EXPORT_REBATE);          
        }else{
          browserHistory.push(Url.ORDER_PAGE);
        }
      } else if (response.data.error === 1) {
        dispatch(fetchingFormDetail(false));
        dispatch(setAlertMeassage(true, response.data.data, false));
      }
      else {
        dispatch(fetchingFormDetail(false));
        dispatch(setAlertMeassage(true, 'Invalid login or password.', false));
      }
    }).catch( error => {
      dispatch(fetchingFormDetail(false));
      dispatch(errorFormDetail(true));
      // dispatch(setAlertMeassage(true, 'Invalid login or password.', false));
      browserHistory.push(Url.ERRORS_PAGE);
    })
  };
};

export const resetAlertBox = (showAlert, message) => {
  return (dispatch) => {
    dispatch(setAlertMeassage(showAlert, message));
  }
}

export const sendMail = (email) => {
  return (dispatch) => {
    dispatch(sendingMail(true));
    axios({
      method: 'post',
      url: `${Config.url}users/forgot_password`,
      data: {
        email:email
      },
      headers: {'token': getToken()}
    }).then( response => {
      if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
        dispatch(sendingMail(false));
        dispatch(setAlertMeassage(true, response.data.data, true));
      } else if (response.data.error === 1) {
        dispatch(sendingMail(false));
        dispatch(setAlertMeassage(true, response.data.data, false));
      }

    }).catch( error => {
      dispatch(sendingMail(false));
      dispatch(sendingMailError(true));
      // dispatch(setAlertMeassage(true, 'Mail cna not send.', false));
      browserHistory.push(Url.ERRORS_PAGE);
    })
  };
}

export const actions = {
  loginUser,
  resetAlertBox,
  // getCompanyType,
  // fetchCompanyType
};

const ACTION_HANDLERS = {
  [FETCHING_FORM_DETAIL]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching
    };
  },
  [ERROR_FORM_DETAIL]: (state, action) => {
    return {
      ...state,
      error: action.error
    };
  },
  [USER_LOGIN_SUCCESS]: (state, action) => {
    return {
      ...state,
      loginDetail: action.loginDetail,
    };
  },
  [SET_ALERT_MESSAGE]: (state, action) => {
    return {
      ...state,
      showAlert: action.showAlert,
      alertMessage: action.alertMessage,
      status: action.status
    };
  },
  [SENDING_EMAIL]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching,
    };
  },
  [SENDING_EMAIL_ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error,
    };
  },
  [CHANGE_TC]: (state, action) => {
    return{
      ...state,
      tcLogin: action.tcLogin
    }
  }
};


const initialState = {
  fetching: false,
  error: false,
  loginDetail: {},
  showAlert: false,
  status: false,
  alertMessage: '',
  tcLogin: true
};

export default function loginReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}