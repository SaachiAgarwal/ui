/** LoginContainer
 *
 * @description This is container that manages the states for all login related functionality.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

import Login from '../components/Login';
import {loginUser, resetAlertBox, sendMail, changeTcStatus, fetchingFormDetail} from '../modules/login';
import { loadingImage, setCurrentLanguage, setInnerNav, setParam, setSearchString, setReportFetching, setToDate,setFromDate } from "../../../store/app";

const mapStateToProps = (state) => {
  return({
    loader: state.app.loader,
    fetching: state.login.fetching,
    isLoggedIn: state.login.isLoggedIn,
    showAlert: state.login.showAlert,
    alertMessage: state.login.alertMessage,
    status: state.login.status,
    tcLogin: state.login.tcLogin
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    loadingImage: (status) => dispatch(loadingImage(status)),
  	loginUser: (values) => dispatch(loginUser(values)),
    resetAlertBox: (showAlert, message) => dispatch(resetAlertBox(showAlert, message)),
    sendMail: (email) => dispatch(sendMail(email)),
    setInnerNav: (lang) => dispatch(setInnerNav(lang)),
    setParam: (lang) => dispatch(setParam(lang)),
    setSearchString: (lang) => dispatch(setSearchString(lang)),
    setReportFetching: (lang) => dispatch(setReportFetching(lang)),
    setCurrentLanguage : (lang) => dispatch(setCurrentLanguage(lang)),
    setToDate : (lang) => dispatch(setToDate(lang)),
    setFromDate : (lang) => dispatch(setFromDate(lang)),
    changeTcStatus: (status) => dispatch(changeTcStatus(status)),
    fetchingFormDetail: (val) => dispatch(fetchingFormDetail(val))

  });
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
