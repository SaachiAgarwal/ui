import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Form, Modal } from 'reactstrap';
import { Link } from 'react-router/lib';
import { reduxForm, Field } from 'redux-form';

import SubmitButtons from 'components/SubmitButtons';
import renderField from './renderField';
import ForgotPassword from './ForgotPassword';
import { Images } from 'config/Config';
import 'css/style.scss';

/** func Validate
 * description validate method will validate all the control fields of form based on provided criteria
 *
 * return Array of Errors
 */
const validate = values => {
  const errors = {}
  if (!values.email) {
    errors.email = 'Required'
  }
  if (!values.password) {
    errors.password = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = '  Invalid email';
  }
  return errors
}

/** LoginForm
 *
 * @description This is Login form component
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.sendMail = this.sendMail.bind(this);

    this.state = {
      modal: false,
      tcLogin: this.props.tcLogin
    };
    this.toggle = this.toggle.bind(this);
    this.onChangeTC = this.onChangeTC.bind(this)
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  sendMail(value) {
    if (!_.isEmpty(value)) this.setState({ modal: false });
    this.props.sendMail(value.email);
    console.log(`send mail is working`);
  }
  async onChangeTC(e) {
    console.log(e.target.value)
    let change = this.props.tcLogin
    this.props.changeTcStatus(!change)
  }



  render() {
    const { handleSubmit } = this.props;
    return (
      <div>
        <Row>
          <div className='col-12 col-sm-12 col-xl-12'>
            <h4 className='title col-12 col-sm-12 col-xl-12'><span style={{ fontSize: '40px', color: '#709f27', fontFamily: 'Great Vibes', fontWeight: 'bold', paddingLeft: '29%' }}>{'GreenTrack'}</span></h4>
            <div style={{position: 'absolute',top: '90px', right: '146px', fontSize: '12px',color: '#709f2c', fontWeight: 'bold'}}><p>Birla Cellulose Traceability Solution</p></div>
            <div style={{position: 'absolute',top: '107px', right: '192px', fontSize: '9px',color: '#709f2c', fontWeight: 'bold'}}><p>Powered by Blockchain</p></div>
            <Form className='mt-2' onSubmit={handleSubmit}>
              <div className='col-12 col-sm-12 col-xl-12'>
                <Row>
                  <Col className='col-12 col-sm-12 col-xl-12'>
                    <Field
                      name='email'
                      className='form-control'
                      type='email'
                      component={renderField}
                      label={'Email'}
                    />
                  </Col>
                  <Col className='col-12 col-sm-12 col-xl-12'>
                    <Field
                      name='password'
                      className='form-control'
                      type='password'
                      component={renderField}
                      label={'Password'}
                    />
                  </Col>
                  <Col className='col-12 col-sm-12 col-xl-12'>
                    <div>
                      <label>Terms & Conditions</label>
                      <div style={{ paddingLeft: '2%', display: 'inline' }}></div>
                      <input type="checkbox"
                        className="conf-checkbox"
                        style={{ paddingLeft: '2px' }}
                        checked={this.props.tcLogin}
                        onChange={(e) => this.onChangeTC(e)}
                      />
                      <p style={{ fontSize: '8px' }}>By clicking on login, you are agreeing to adding correct and ethical information only. Any deviation from accuracy in information added by you or any action undertaken on the platform by you, will solely be your responsibility.</p>
                    </div>
                  </Col>
                </Row>
              </div>
              <div className='col-8 col-sm-8 col-xl-8'>
                <SubmitButtons
                  submitLabel={'Login'}
                  className='btn btn-primary'
                  submitting={this.props.submitting}
                />
              </div>
              <Link to="" className="btn btn-link forgot-pass" onClick={this.toggle}>{this.props.buttonLabel}{'Forgot your password'} ?
                </Link>
              <Modal isOpen={this.state.modal} fade={false} toggle={this.toggle} className={this.props.className}
                style={{
                  width: '214px',
                  left: '220px'
                }}
              >
                <ForgotPassword
                  onSubmit={this.sendMail}
                  toggle={this.toggle}
                  loadingImage={this.props.loadingImage}
                />
              </Modal>
            </Form>
          </div>
        </Row>
      </div>
    );
  }
}

LoginForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool
};

export default reduxForm({
  form: 'LoginForm',
  validate,
})(LoginForm)
