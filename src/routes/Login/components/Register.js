import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'reactstrap';
import { reduxForm, Field } from 'redux-form';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
import DropdownList from 'react-widgets/lib/DropdownList';
import 'react-widgets/dist/css/react-widgets.css';

import RenderField from 'components/RenderField';
import NormalizePhone from 'components/NormalizePhone';
import SubmitButtons from 'components/SubmitButtons';
import {getCompanyTypeId} from 'components/Helper';

/** func Validate
 * description validate method will validate all the control fields of form based on provided criteria
 *
 * return Array of Errors
 */
const validate = values => {
  const errors = {}
  if (!values.company_type) {
    errors.company_type = 'Required'
  }
  if (!values.first_name) {
    errors.first_name = 'Required'
  }
  if (!values.last_name) {
    errors.last_name = 'Required'
  } 
  if (!values.email) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = '  Invalid email';
  }
  if (!values.password) {
    errors.password = 'Required'
  } 
  /*else if (values.password.length < 6) {
    errors.password = 'Password must be 6 character long';
  }*/
  if (!values.confirmpassword) {
    errors.confirmpassword = 'Required'
  } else if(values.password != values.confirmpassword) {
    errors.confirmpassword = 'Confirm password not matched'
  }
  return errors
}

const renderDropdownList = ({ input, data, valueField, textField, customProps }) => {
  return (
    <DropdownList {...input}
      data={data}
      onChange={input.onChange}
      onSelect={customProps.fetchCompanyType(input.value)} 
    />
  ); 
}

/** RegisterForm
 *
 * @description This is a Register component
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class RegisterForm extends React.Component {
  constructor(props) {
    super(props);
    this.fetchCompanyType = this.fetchCompanyType.bind(this);
    this.customMethod = this.customMethod.bind(this);
  }

  componentWillMount() {
    this.props.getCompanyType();
  }

  fetchCompanyType(value) {
    if (typeof value !== 'undefined' && value !== '') {
      let company_type_id = getCompanyTypeId(this.props.companyType, value);
      this.props.getCompany(company_type_id);
    }
  }

  customMethod(id) {
    console.log('customMethod ', id);
  }

  render() {
    let RoleOptions = [{role: 'Role1'},{role: 'Role2'},{role: 'Role3'},{role: 'Role4'},{role: 'Role5'},{role: 'Role6'}];
    const { handleSubmit } = this.props;
    return (
      <div className='col-12 col-sm-12 col-xl-12 register-box'>
        <h4 className='title col-12 col-sm-12 col-xl-12'><span>Please Register below</span></h4>
        <Form className='mt-2 text-left' onSubmit={handleSubmit}>
          <div className='col-12 col-sm-12 col-xl-12 box-bg py-2'>
            <Row>
              <div className='col-12 col-sm-12 col-xl-12 mb-2'>
                <Row>
                  <Col className='text-center col-6 col-sm-6 col-xl-6 pr-1'>
                    <Field 
                      name="company_type"
                      label="select Role"
                      component={renderDropdownList}
                      data={this.props.companyType.map(function(list) {return list.company_type_name;})}
                      customProps={{
                        fetchCompanyType: this.fetchCompanyType
                      }}
                    />
                  </Col>
                  <Col className='text-center col-6 col-sm-6 col-xl-6 pl-1'>
                    <Field name="company_key_id" component="select" className='form-control'>
                      <option>{'Select Company'}</option>
                      {this.props.companyDetail.map((company, i) => {
                        return (
                          <option value={company.company_key_id} key={i} onChange={() => this.customMethod(company.company_key_id)}>{company.company_name}</option>
                        );
                      })}
                    </Field>
                  </Col>
                </Row>
              </div>
              <div className='col-12 col-sm-12 col-xl-12 mb-2'>
                <Row>
                  <Col className='text-center col-6 col-sm-6 col-xl-6 pr-1'>
                    <Field
                      name='first_name'
                      className='form-control'
                      type='text'
                      component={RenderField}
                      label='First Name'
                    />
                  </Col>
                  <Col className='text-center col-6 col-sm-6 col-xl-6 pl-1'>
                    <Field
                      name='last_name'
                      className='form-control'
                      type='text'
                      component={RenderField}
                      label='Last Name'
                    />
                  </Col>
                </Row>
              </div>
              <div className='col-12 col-sm-12 col-xl-12'>
                <Row>
                  <Col className='col-12 col-sm-12 col-xl-12 mb-1'>
                    <Field
                      name='email'
                      className='form-control'
                      type='email'
                      component={RenderField}
                      label='Email'
                    />
                  </Col>
                  <Col className='col-12 col-sm-12 col-xl-12 mb-1'>
                    <Field
                      name='password'
                      className='form-control'
                      type='password'
                      component={RenderField}
                      label='Password (min. 6 characters)'
                    />
                  </Col>
                  <Col className='col-12 col-sm-12 col-xl-12 mb-1'>
                    <Field
                      name='confirmpassword'
                      className='form-control'
                      type='password'
                      component={RenderField}
                      label='Confirm Password'
                    />
                  </Col>
                </Row>
              </div>
            </Row>
          </div>
          <div>
            <div className='col-12 col-sm-12 col-xl-12 register-btn-group'>
              <SubmitButtons
                submitLabel='Register'
                className='btn btn-primary register-btn'
                submitting={this.props.submitting}
              />
            </div>
          </div>
        </Form>
      </div>
    );
  }
}

RegisterForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool
};

export default reduxForm({
  form: 'RegisterForm',
  validate,
})(RegisterForm)
