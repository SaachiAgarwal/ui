import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory, Link } from 'react-router';
import { Row, Col, Modal } from 'reactstrap';

import LoginForm from './LoginForm';
import NavBar from 'components/navbar/NavBar';
import Data from 'components/navbar/Data';
import InnerHeader from 'components/InnerHeader';
import Alert from "components/Alert";
import Loader from 'components/Loader';
import { Images } from 'config/Config';
import { saveLocalStorage, getLocalStorage } from 'components/Helper';
import Logo from 'components/Logo';
import 'css/style.scss';
import Swal from 'sweetalert2';
import ReactLoading from 'react-loading';
import { Animated } from "react-animated-css";
/** Login
 *
 * @description This class is a base class for Login/Register/Forgot pwd functionality
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.saveLoginDetail = this.saveLoginDetail.bind(this);
    this.handleAlertBox = this.handleAlertBox.bind(this);
    this.goTo = this.goTo.bind(this);
    this.confirm = this.confirm.bind(this)
  }

  async componentDidMount() {
    console.log("IN mount")
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.fetching === false && this.props.loader === true) {
      this.props.loadingImage(false);
    }
  }

  saveLoginDetail(values) {
    if (this.props.tcLogin) {
      this.props.setCurrentLanguage('en');
      this.props.loadingImage(true);
      this.props.loginUser(values);

      //resetting reports values.
      this.props.setInnerNav(false);
      this.props.setParam('');
      this.props.setSearchString('');
      this.props.setReportFetching(false);
      this.props.setFromDate('');
      this.props.setToDate('');
    }
    else {
      this.confirm()
    }

  }

  handleAlertBox() {
    this.props.resetAlertBox(false, "");
  }

  goTo() {
    browserHistory.goBack();
  }
  async confirm() {
    Swal({
      text: 'Please Accept Terms & Conditions',
      type: 'warning',
    })
  }
  async componentWillUnmount() {
    console.log("In Unmount Login")
    //this.props.changeTcStatus(false)
  }

  render() {
    return (
      <div className='col-12 px-0'>
        <Alert
          showAlert={(typeof this.props.showAlert !== "undefined" ? this.props.showAlert : false)}
          message={(typeof this.props.alertMessage !== "undefined" ? this.props.alertMessage : "")}
          handleAlertBox={this.handleAlertBox}
          status={this.props.status}
        />
        {/* <Loader loading={this.props.fetching} /> */}
        {this.props.fetching &&
          <ReactLoading type="bubbles" style={{
            fill: '#4e5be1e0',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundSize: '200px',
            position: 'fixed',
            zIndex: 10000,
            width: '6%',
            height: '72%',
            top: '50%',
            left: '50%'
          }} />
        }
        <div>
          <Animated animationIn="fadeIn" animationOut="fadeOut" animationInDuration={1000} isVisible={!this.props.fetching}>
            <div style={{ position: 'absolute', top: '-19px', backgroundColor: 'white' }}>
              <img style={{ height: '100px', width: '45%' }} src={Images.valChain} />

            </div>
            <div>
              <img style={{ position: 'absolute', top: '74px', height: '503px', width: '800px' }} src={Images.livaLogin} />
            </div>



            <div className='col-md-5 col-lg-5 col-md-offset-3 login-form'>
              <div className='col-12 col-sm-12 col-xl-12'>
                <LoginForm
                  {...this.props}
                  onSubmit={this.saveLoginDetail}
                  sendMail={this.props.sendMail}
                  tcLogin={this.props.tcLogin}
                  changeTcStatus={this.props.changeTcStatus}
                />
                <Logo />
              </div>
            </div>
          </Animated>
        </div>
      </div>
    );
  }
}

export default Login;
