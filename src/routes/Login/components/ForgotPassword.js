import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Form, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { reduxForm, Field } from 'redux-form';

import RenderField from 'components/RenderField';
import SubmitButtons from 'components/SubmitButtons';

/** func Validate
 * description validate method will validate all the control fields of form based on provided criteria
 *
 * return Array of Errors
 */
const validate = values => {
  const errors = {}
  if (!values.email) {
    errors.email = 'Required'
  }
  return errors
}

/** ForgotPassword
 *
 * @description This class contains ForgotPassword form
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div>
        <ModalHeader toggle={this.toggle}>Forgot Password</ModalHeader>
        <ModalBody>
          <p >Please enter your email. You will receive a link to reset password</p>
          <Form onSubmit={handleSubmit}>
            <Field
              name='email'
              className='form-control'
              type='email'
              component={RenderField}
              label={'Email'}
            />
        <div className="d-flex justify-content-center">

          <SubmitButtons
            submitLabel={'Submit'}
            className='btn btn-primary'
            submitting={this.props.submitting}

          />
        </div>
          </Form>
        </ModalBody>


        {/* <ModalBody className='p-0'>
          <div className='col-12'>
            <Row>
            <div class="d-flex justify-content-center">
              <div className='col-12 forgot-wrapper'>
                <div className='col-12  forgot-box'>
                  <h4 className='title col-12 red-text text-uppercase text-center'><span>Forgot Password</span></h4>
                  <p className='my-3 col-8 text-center'>Please enter your email. You will receive a link to reset password</p>
                  <Form onSubmit={handleSubmit}>
                    <div className='col-12'>
                      <Row>
                        <Col className='col-4'>
                          <Field
                            name='email'
                            className='form-control'
                            type='email'
                            component={RenderField}
                            label={'Email'}
                          />
                        </Col>
                      </Row>
                    </div>
                    <div className='col-8 col-sm-8 col-xl-8'>
                      <SubmitButtons
                        submitLabel={'Submit'}
                        className='btn btn-primary'
                        submitting={this.props.submitting}
                      />
                    </div>
                  </Form>
                </div>
              </div>
              </div>
            </Row>
          </div>
        </ModalBody> */}
      </div>
    );
  };
}

ForgotPassword.propTypes = {

};

export default reduxForm({
  form: 'ForgotPassword',
  validate,
})(ForgotPassword)