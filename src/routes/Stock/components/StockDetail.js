import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Pagination from 'react-paginate';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
let strings = new LocalizedStrings(data);
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import options from 'components/options';

// import renderField from './renderField';

/** StockDetail
 *
 * @description This class is responsible to display a shipment list of stocks
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */


export const renderField = ({ input, label, type, placeholder, meta: { touched, error, invalid } }) => (
  <div className={`form-group ${touched && invalid ? 'has-error' : ''}`}>
    <label className="control-label">{label}</label>
    <div>
      <input {...input}
        className="form-control"
        type={type}
        autoComplete="off"
        autoCorrect="off"
        spellCheck="off"
        placeholder={placeholder}
      />

      <div className="help-block">
        {touched && (error && <span className="error-danger">
          <i className="fa fa-exclamation-circle">{error}</i></span>)}
      </div>
    </div>
  </div>
)

class StockDetail extends Component {
  constructor(props) {
    super(props);
    this.handlePageClick = this.handlePageClick.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);

    this.state = {
      query: ''
    }
  }

  handlePageClick(value) {
    this.props.fetchStocks('', value.selected + 1, this.props.options.pageSize);
  }

  async onSearchChange(query){
    await this.setState({
      query: query
    })
    await this.props.fetchStocks(this.state.query,options.currentPage, options.pageSize);
  }


  render() {
    const { currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);
    const { blend, product, type, quantity, unit, lot, productSource } = strings.addProduct;
    const { action } = strings.shipmentTableText;
    // console.log(blend);
    return (
      <div>
      <div className='col-12 col-sm-12 col-xl-12 mb-2'>
        <Row>
          <Col className="text-center col-4 col-sm-4 col-xl-6 pr-1">
            <Field
              name='product'
              type="text"
              component={renderField}
              label={strings.OrderFilterText.Search}
              value={this.props.searchText}
              onChange={(e) => this.onSearchChange(e.target.value)} 
            />
          </Col>
        </Row>
      </div>
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options}>
          <TableHeaderColumn dataField='stock_lot_id' hidden={true}>Stock Id</TableHeaderColumn>
          <TableHeaderColumn dataField='invoice_number' columnTitle={true}>{strings.shipmentTableText.invoiceNumber}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_name' isKey={true} columnTitle={true}>{product}</TableHeaderColumn>
          <TableHeaderColumn dataField='supplier_company_name' columnTitle={true}>{strings.shipmentTableText.supplier}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_type_name'>{type}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_qty'>{quantity}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_uom'>{unit}</TableHeaderColumn>
          <TableHeaderColumn dataField='lot_number'>{lot}#</TableHeaderColumn>
          {/* <TableHeaderColumn dataField='supplier_company_name'>Supplier</TableHeaderColumn> */}
          {(this.props.loginData.role === "Birla Cellulose") ? <TableHeaderColumn dataField='product_source'>{productSource}</TableHeaderColumn> : false}
          {(this.props.display) ? false : <TableHeaderColumn dataField='blend_percentage' >{blend} </TableHeaderColumn>}
          <TableHeaderColumn key="editAction" hidden={true} dataFormat={this.props.outgoingFormatter}>{action}</TableHeaderColumn>
        </BootstrapTable>
        <div className="pagination-box">
          <Pagination
            previousLabel={`${strings.prev}`}
            nextLabel={`${strings.next}`} breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={this.props.pages}
            pageRangeDisplayed={5}
            onPageChange={this.handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        </div>
      </div>
      </div>
    );
  }
}

StockDetail.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default reduxForm({
  form: 'StockDetail',
})(StockDetail)