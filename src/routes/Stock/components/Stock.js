import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import Modal from 'react-modal';
import { Button, Row } from 'reactstrap';

import Loader from 'components/Loader';
import Alert from "components/Alert";
import HeaderSection from 'components/HeaderSection';
import NavDrawer from 'components/NavDrawer';
import options from 'components/options';
import StockDetail from './StockDetail';
import { getLocalStorage, getText } from 'components/Helper';
import CsvModal from 'components/CsvModal';
import Footer from 'components/Footer';
import 'css/style.scss';
import '../../Orders/components/Orders.scss';
import ReactLoading from 'react-loading';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);


//  Custom styles for link modal
const customStyles = {
  content: {
    top: '0',
    left: '0',
    right: '0',
    bottom: '0',
    marginRight: '0',
    transform: 'translate(0, 0)'
  }
};

class Stock extends React.Component {
  constructor(props) {
    super(props);
    this.fetch = this.fetch.bind(this);
    this.handleAlertBox = this.handleAlertBox.bind(this);
    this.handleCsvModal = this.handleCsvModal.bind(this);
    this.handleSearch = this.handleSearch.bind(this);

    this.state = {
      active: 'stock',
      role: '',
      isSuperUser: '',
      activeCsvModal: 'upload',
      searchText: ''
    }
  }

  async componentWillMount() {
    let loginDetail = getLocalStorage('loginDetail');
    await this.setState({
      role: loginDetail.role,
      isSuperUser: loginDetail.userInfo.is_super_user
    });
    this.props.setGlobalState({
      navIndex: 3,
      navInnerIndex: 0,
      navClass: 'Stock',
    });
    this.props.checkCsvExcelBtn();
    this.props.fetchStocks(this.state.searchText, options.currentPage, options.pageSize);
  }

  async handleAlertBox() {
    this.props.resetAlertBox(false, "");
  }

  componentWillUnmount() {
    this.props.setProps(false, 'isUpload');
    this.props.setProps(false, 'isDownload');
  }

  async fetch() {
    await this.props.checkCsvExcelBtn();
    this.props.fetchStocks(this.state.searchText, options.currentPage, options.pageSize);
  }

  handleInboxOutboxOrders(classType, innerType) {
    if (classType === 'orders' && innerType === 'inbox') {
    } else if (classType === 'orders' && innerType === 'outbox') {
    } else if (classType === 'shipments' && innerType === 'inbox') {
    } else if (classType === 'Audit Trail') {
    } else if (classType === 'logout') {
      removeLocalStorage('loginDetail');
      browserHistory.push(Url.HOME_PAGE);
    }
  }

  async handleCsvModal(status, key) {
    if (key === 'upload') {
      await this.setState({ activeCsvModal: 'upload' });
      this.props.setProps(status, 'isUpload');
      this.props.setGlobalState({ isUploaded: false });
    } else {
      await this.setState({ activeCsvModal: 'download' });
      this.props.setProps(status, 'isDownload');
      this.props.setGlobalState({ isUploaded: false });
    }
  }

  async handleSearch(query) {
    await this.setState({ searchText: query });
    if (query.length >= 2 || query.length === 0) this.props.fetchStocks(this.state.searchText, options.currentPage, options.pageSize);
  }

  render() {
    // console.log(this.props.haveCsvExcelBtn);
    strings.setLanguage(this.props.currentLanguage);

    let loginData = getLocalStorage('loginDetail'),
      dispbutn = (loginData.role === "Spinner" || loginData.role === "Birla Cellulose") ? true : false
    return (
      <div>
        {/* <Loader loading={this.props.fetching} /> */}
        {this.props.fetching &&
          <ReactLoading type="bubbles" style={{
            fill: '#4e5be1e0',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundSize: '200px',
            position: 'fixed',
            zIndex: 10000,
            width: '6%',
            height: '72%',
            top: '50%',
            left: '50%'
          }} />
        }
        <Alert
          showAlert={(typeof this.props.showAlert !== "undefined" ? this.props.showAlert : false)}
          message={(typeof this.props.alertMessage !== "undefined" ? this.props.alertMessage : "")}
          handleAlertBox={this.handleAlertBox}
          status={this.props.status}
        />
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-2 col-lg-2 sidebar">
              <NavDrawer
                role={this.state.role}
                isSuperUser={this.state.isSuperUser}
                setGlobalState={this.props.setGlobalState}
                navIndex={this.props.navIndex}
                navInnerIndex={this.props.navInnerIndex}
                navClass={this.props.navClass}
                handleInboxOutboxOrders={this.handleInboxOutboxOrders}
                currentLanguage={this.props.currentLanguage}
                setInnerNav={this.props.setInnerNav}
                setParam={this.props.setParam}
                setSearchString={this.props.setSearchString}
                setReportFetching={this.props.setReportFetching}
              />
            </div>
            <div className="col-md-10 col-lg-10 col-lg-offset-2 col-md-offset-2 main">
              <HeaderSection
                active={this.state.active}
                searchText={this.state.searchText}
                handleSearch={this.handleSearch}
                setCurrentLanguage={this.props.setCurrentLanguage}
                currentLanguage={this.props.currentLanguage}
              />
              <Modal
                isOpen={this.props.isUpload || this.props.isDownload}
                style={customStyles}
                ariaHideApp={false}
              >
                <CsvModal
                  owner={'Stocks'}
                  role={this.state.role}
                  handleCsvModal={this.handleCsvModal}
                  getText={getText}
                  activeCsvModal={this.state.activeCsvModal}
                  uploadFile={this.props.uploadFile}
                  updateView={this.fetch}
                  viewToBeUpdated={''}
                  data={this.props.viewStockDetail}
                  isUploaded={this.props.isUploaded}
                  msg={this.props.msg}
                  currentLanguage={this.props.currentLanguage}
                />
              </Modal>
              <StockDetail
                data={this.props.viewStockDetail}
                pagination={false}
                search={false}
                exportCSV={false}
                options={options}
                isForModal={false}
                fetchStocks={this.props.fetchStocks}
                totalRecordCount={this.props.totalRecordCount}
                pages={this.props.pages}
                loginData={loginData}
                display={dispbutn}
                currentLanguage={this.props.currentLanguage}
              />
              {(this.props.haveCsvExcelBtn === 0 && this.state.role !== 'Brand') &&
                <div>
                  <Row>
                    <Button className='upload-button' onClick={() => this.handleCsvModal(true, 'upload')}>{strings.btnGrpText.uploadCSV}</Button>
                    <Button className='download-button' onClick={() => this.handleCsvModal(true, 'download')}>{strings.btnGrpText.downloadCSV}</Button>
                  </Row>
                </div>
              }
              <div className="clearfix"></div>
              <Footer
                currentLanguage={this.props.currentLanguage}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Stock;

