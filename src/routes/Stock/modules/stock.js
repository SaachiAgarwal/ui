import axios from 'axios';
import { browserHistory } from 'react-router';

import {Config, Url} from 'config/Config';
import {getToken, getPages} from 'components/Helper';
// ------------------------------------
// Constant
// ------------------------------------
export const SET_ALERT_MESSAGE = 'SET_ALERT_MESSAGE';
export const FETCHING = 'FETCHING';
export const ERROR = 'ERROR';
export const FETCHING_STOCK_SUCCESS = 'FETCHING_STOCK_SUCCESS';
export const SET_UPLOAD_STATUS = 'SET_UPLOAD_STATUS';
export const SET_DOWNLOAD_STATUS = 'SET_DOWNLOAD_STATUS';
export const CHECK_STOCK_UPLOADED_BTN = 'CHECK_STOCK_UPLOADED_BTN';
export const SET_TOTAL_RECORD_COUNT = 'SET_TOTAL_RECORD_COUNT';
export const SET_TOTAL_PAGES = 'SET_TOTAL_PAGES';
//STOCKS
export const SET_STOCK_SUMMARY = 'SET_STOCK_SUMMARY';
export const SET_STOCK_MONTHWISE = 'SET_STOCK_MONTHWISE';
export const SET_STOCK_PRODUCTWISE = 'SET_STOCK_PRODUCTWISE'
export const SET_STOCK_SUPPLIERWISE = 'SET_STOCK_SUPPLIERWISE'

// ------------------------------------
// Actions
// ------------------------------------

export function setAlertMeassage(status, message, orderStatus=false) {
  return {
    type: SET_ALERT_MESSAGE,
    showAlert: status,
    alertMessage: message,
    status: orderStatus
  };
}

export function fetching(status) {
  return {
    type: FETCHING,
    fetching: status,
  };
}

export function errorFetching(status) {
  return {
    type: ERROR,
    error: status,
  };
}

export function fetchingStockSuccess(payload) {
  return {
    type: FETCHING_STOCK_SUCCESS,
    viewStockDetail: payload,
  };
}

export function setUploadStatus(status) {
  return {
    type: SET_UPLOAD_STATUS,
    isUpload: status,
  };
}

export function setDownloadStatus(status) {
  return {
    type: SET_DOWNLOAD_STATUS,
    isDownload: status,
  };
}

export function checkStockUploadedSuccess(status) {
  return {
    type: CHECK_STOCK_UPLOADED_BTN,
    haveCsvExcelBtn: status,
  };
}

export function setTotalRecordCount(payload) {
  return {
    type: SET_TOTAL_RECORD_COUNT,
    totalRecordCount: payload,
  }
}

export function setTotalPages(payload) {
  return {
    type: SET_TOTAL_PAGES,
    pages: payload,
  }
}

// ------------------------------------
// Action creators
// ------------------------------------
export const resetAlertBox = (showAlert, message) => {
  return (dispatch) => {
    dispatch(setAlertMeassage(showAlert, message));
  }
}

export const setProps = (status, key) => {
  return (dispatch) =>{
    switch(key) {
      case 'isUpload':
        dispatch(setUploadStatus(status));
        break;
      case 'isDownload':
        dispatch(setDownloadStatus(status));
        break;
    }
  }
}

export const fetchStocks = (query='', currentPage=1, pageSize=10) => {
  return (dispatch) => {
    if (!query) dispatch(fetching(true));
    dispatch(fetching(true));
    let requestMethod = 'post';
    let endPoint = 'shipment/view_stocks';
    let data = {};
    data = {
      search_text: query,
      page_size: pageSize,
      page_number: currentPage,
    }

    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          let totalPage = getPages(response.data.total_recordcount);
          dispatch(fetchingStockSuccess(response.data.data));
          dispatch(setTotalRecordCount(response.data.total_recordcount));
          dispatch(setTotalPages(totalPage));
          resolve(true);
            
        } else if(response.data.error === 1) {
          dispatch(setAlertMeassage(true, response.data.data, false));
          resolve(true);
        }
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'No stocks found.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const checkCsvExcelBtn = () => {
  return (dispatch) => {
    dispatch(fetching(true));
    let requestMethod = 'get';
    let endPoint = 'admin/check_stock_uploaded';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(checkStockUploadedSuccess(response.data.data.isUploaded));
          resolve(true);
            
        } else if(response.data.error === 1) {
          dispatch(setAlertMeassage(true, response.data.data, false));
          resolve(true);
        }
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'No stocks found.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const actions = {
  resetAlertBox,
  fetchStocks
}

// ------------------------------------
// Action handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_ALERT_MESSAGE]: (state, action) => {
    return {
      ...state,
      showAlert: action.showAlert,
      alertMessage: action.alertMessage,
      status: action.status,
    }
  },
  [FETCHING]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching,
    }
  },
  [ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error,
    }
  },
  [FETCHING_STOCK_SUCCESS]: (state, action) => {
    return {
      ...state,
      viewStockDetail: action.viewStockDetail,
    }
  },
  [SET_UPLOAD_STATUS]: (state, action) => {
    return {
      ...state,
      isUpload: action.isUpload,
    }
  },
  [SET_DOWNLOAD_STATUS]: (state, action) => {
    return {
      ...state,
      isDownload: action.isDownload,
    }
  },
  [CHECK_STOCK_UPLOADED_BTN]: (state, action) => {
    return {
      ...state,
      haveCsvExcelBtn: action.haveCsvExcelBtn,
    }
  },
  [SET_TOTAL_RECORD_COUNT]: (state, action) => {
    return {
      ...state,
      totalRecordCount: action.totalRecordCount,
    }
  },
  [SET_TOTAL_PAGES]: (state, action) => {
    return {
      ...state,
      pages: action.pages,
    }
  },
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  fetching: false,
  error: false,
  status: false,
  showAlert: false,
  alertMessage: '',
  viewStockDetail: [],
  isUpload: false,
  isDownload: false,
  haveCsvExcelBtn: false,
  totalRecordCount: 0,
  pages: 1,
};

export default function stockReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
