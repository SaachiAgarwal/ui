import { connect } from 'react-redux';

import Stock from '../components/Stock';
import {fetchStocks, resetAlertBox, setProps, checkCsvExcelBtn} from '../modules/stock';
import {setReportFetching,setSearchString,setParam,setInnerNav,setGlobalState, uploadFile, setCurrentLanguage} from '../../../store/app';

const mapStateToProps = (state) => {
  return({
    navIndex: state.app.navIndex,
    navInnerIndex: state.app.navInnerIndex,
    navClass: state.app.navClass,
    isUploaded: state.app.isUploaded,
    msg: state.app.msg,
    fetching: state.Stock.fetching,
    error: state.Stock.error,
    status: state.Stock.status,
    showAlert: state.Stock.showAlert,
    alertMessage: state.Stock.alertMessage,
    viewStockDetail: state.Stock.viewStockDetail,
    isUpload: state.Stock.isUpload,
    isDownload: state.Stock.isDownload,
    haveCsvExcelBtn: state.Stock.haveCsvExcelBtn,
    totalRecordCount: state.Stock.totalRecordCount,
    pages: state.Stock.pages,
    currentLanguage : state.app.currentLanguage,
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    setGlobalState: (value) => dispatch(setGlobalState(value)),
    resetAlertBox: (showAlert, message) => dispatch(resetAlertBox(showAlert, message)),
    fetchStocks: (query, currentPage, pageSize) => dispatch(fetchStocks(query, currentPage, pageSize)),
    uploadFile: (file, owner) => dispatch(uploadFile(file, owner)),
    setProps: (status, key) => dispatch(setProps(status, key)),
    checkCsvExcelBtn: () => dispatch(checkCsvExcelBtn()),
    setCurrentLanguage : (language) => dispatch(setCurrentLanguage(language)),
    setInnerNav: (lang) => dispatch(setInnerNav(lang)),
    setParam: (lang) => dispatch(setParam(lang)),
    setSearchString: (lang) => dispatch(setSearchString(lang)),
    setReportFetching: (lang) => dispatch(setReportFetching(lang)),

  });
};

export default connect(mapStateToProps, mapDispatchToProps)(Stock);
