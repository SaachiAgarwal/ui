import { injectReducer } from '../../store/reducers';
import {checkPageRestriction} from '../index';

export default (store) => ({
  path: 'stock',
  onEnter: (nextState, replace) => {
    checkPageRestriction(nextState, replace, () => {})
  },
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const Stock = require('./containers/StockContainer').default;
      const reducer = require('./modules/stock').default;
      injectReducer(store, { key: 'Stock', reducer });
      cb(null, Stock);
  }, 'Stock');
  },
});
