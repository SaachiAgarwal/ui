import { injectReducer } from '../../store/reducers';

export default (store) => ({
  path: 'products',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const Products = require('./containers/ProductsContainer').default;
      const reducer = require('./modules/products').default;
      injectReducer(store, { key: 'Products', reducer });
      cb(null, Products);
  }, 'Products');
  },
});
