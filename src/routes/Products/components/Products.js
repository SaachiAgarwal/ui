import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Button } from 'reactstrap';
import _ from 'lodash';
import Swal from 'sweetalert2';

import Loader from 'components/Loader';
import options from 'components/options';
import Alert from "components/Alert";
import HeaderSection from 'components/HeaderSection';
import NavDrawer from 'components/NavDrawer';
import { getLocalStorage, getProductTypeId, getUOM } from 'components/Helper';
import ProductDetail from './ProductDetail';
import AddProduct from './AddProduct';
import ReviewProduct from './ReviewProduct';
import Footer from 'components/Footer';
import 'css/style.scss';
import ReactLoading from 'react-loading';

/** Products
 *
 * @description This class is a based class for Products stuff
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class Products extends React.Component {
  constructor(props) {
    super(props);
    this.confirm = this.confirm.bind(this);
    this.handleAlertBox = this.handleAlertBox.bind(this);
    this.handleInboxOutboxOrders = this.handleInboxOutboxOrders.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.setActivePage = this.setActivePage.bind(this);
    this.openProductForm = this.openProductForm.bind(this);
    this.productFormatter = this.productFormatter.bind(this);
    this.back = this.back.bind(this);
    this.createProduct = this.createProduct.bind(this);
    // this.viewProduct = this.viewProduct.bind(this);
    // this.editProduct = this.editProduct.bind(this);
    // this.deleteProduct = this.deleteProduct.bind(this);

    this.state = {
      active: 'product',
      role: '',
      isSuperUser: '',
      searchText: '',
      previousActivePage: '',
    }
  }

  async componentWillMount() {
    let loginDetail = getLocalStorage('loginDetail');
    await this.setState({
      role: loginDetail.role,
      isSuperUser: loginDetail.userInfo.is_super_user
    });
    this.props.setGlobalState({
      navIndex: 9,
      navInnerIndex: 0,
      navClass: 'Company',
    });
    // getting products details
    this.props.getProductDetails();
  }

  componentWillUnmount() {
    this.props.setCurrentNewPage(1)
  }

  async handleSearch(query) {
    await this.setState({ searchText: query });
    if (query.length >= 2 || query.length === 0) this.props.getProductDetails(this.state.searchText, options.currentPage, options.pageSize);
  }

  async handleAlertBox() {
    this.props.resetAlertBox(false, "");
  }

  handleInboxOutboxOrders(classType, innerType) {
    if (classType === 'orders' && innerType === 'inbox') {
    } else if (classType === 'orders' && innerType === 'outbox') {
    } else if (classType === 'shipments' && innerType === 'inbox') {
    } else if (classType === 'Audit Trail') {
    } else if (classType === 'logout') {
      removeLocalStorage('loginDetail');
      browserHistory.push(Url.HOME_PAGE);
    }
  }

  async setActivePage(state) {
    this.setState({ active: state });
  }

  async openProductForm() {
    await this.setState({ previousActivePage: this.state.active });
    await this.setState({ active: 'addProducts' });
    this.props.setProps(true, 'isProductFormOpen');
  }

  confirm(id = '', key = '', msg, confirmText, cancelText) {
    // console.log('id ', id);
    Swal({
      title: 'Warning!',
      text: `${msg}`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmText,
      cancelButtonText: cancelText
    }).then(async (result) => {
      this.props.setProps(false, 'isConfirm');
      if (result.value) {
        if (key === 'deleteProduct') {
          await this.props.deleteProduct(id);
          this.props.getProductDetails();
        }
      }
    })
  }

  async viewProduct(row) {
    await this.setState({ previousActivePage: this.state.active });
    await this.props.viewProductDetail(row.product_id);
    await this.props.getProductType();
    this.props.setProps(true, 'isReviewProduct');
    this.props.setProps(true, 'isProductFormOpen');
  }

  async editProduct(row) {
    await this.setState({ previousActivePage: this.state.active });
    await this.props.viewProductDetail(row.product_id);
    await this.props.getProductType();
    this.props.setProps(true, 'isEdit');
    this.props.setProps(true, 'isProductFormOpen');
  }

  deleteProduct(row) {
    // console.log('row ', row);
    this.confirm(row.product_id, 'deleteProduct', 'Do you really want to delete', 'Yes delete it', 'No keep it');
  }

  back() {
    this.setState({ active: this.state.previousActivePage });
    this.props.setProps(false, 'isProductFormOpen');
    this.props.getProductDetails('', this.props.currentNewPage, options.PageSize)
  }

  async createProduct(values) {
    let data = {
      product_name: values.product_name,
      product_type_id: getProductTypeId(values.product_type_name, this.props.productType, false),
      product_uom: values.product_uom,
      product_description: values.product_description,
    }
    if (this.props.isEdit) data.product_id = getLocalStorage("products").product_id;

    await this.props.addProduct(data, this.props.isEdit);
    this.props.getProductDetails();
  }

  productFormatter(row, cell) {
    return (
      <ButtonGroup>
        <Button className="action" onClick={() => this.viewProduct(cell)} >view</Button>
        <Button className="action" onClick={() => this.editProduct(cell)} >edit</Button>
        <Button className="action delete" onClick={() => this.deleteProduct(cell)} >delete</Button>
      </ButtonGroup>
    );
  }

  render() {
    return (
      <div>
        {/* <Loader loading={(this.props.fetching !== undefined && this.props.fetchingGlobal !== undefined) ? (this.props.fetching || this.props.fetchingGlobal) : this.props.fetching} /> */}
        {this.props.fetching &&
          <ReactLoading type="bubbles" style={{
            fill: '#4e5be1e0',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundSize: '200px',
            position: 'fixed',
            zIndex: 10000,
            width: '6%',
            height: '72%',
            top: '50%',
            left: '50%'
          }} />
        }
        <Alert
          showAlert={(typeof this.props.showAlert !== "undefined" ? this.props.showAlert : false)}
          message={(typeof this.props.alertMessage !== "undefined" ? this.props.alertMessage : "")}
          handleAlertBox={this.handleAlertBox}
          status={this.props.status}
        />
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-2 col-lg-2 sidebar">
              <NavDrawer
                role={this.state.role}
                isSuperUser={this.state.isSuperUser}
                setGlobalState={this.props.setGlobalState}
                navIndex={this.props.navIndex}
                navInnerIndex={this.props.navInnerIndex}
                navClass={this.props.navClass}
                handleInboxOutboxOrders={this.handleInboxOutboxOrders}
                currentLanguage={this.props.currentLanguage}
                setInnerNav={this.props.setInnerNav}
                setParam={this.props.setParam}
                setSearchString={this.props.setSearchString}
                setReportFetching={this.props.setReportFetching}
              />
            </div>
            <div className="col-md-10 col-lg-10 col-lg-offset-2 col-md-offset-2 main">
              <HeaderSection
                active={this.state.active}
                searchText={this.state.searchText}
                handleSearch={this.handleSearch}
                currentLanguage={this.props.currentLanguage}
                setCurrentLanguage={this.props.setCurrentLanguage}
              />
              {(!this.props.isProductFormOpen) ?
                <div>
                  <ProductDetail
                    data={this.props.productsDetail}
                    pagination={false}
                    search={false}
                    exportCSV={false}
                    options={options}
                    productFormatter={this.productFormatter}
                    getProductDetails={this.props.getProductDetails}
                    totalRecordCount={this.props.totalRecordCount}
                    pages={this.props.pages}
                    searchText={this.state.searchText}
                    setCurrentNewPage={this.props.setCurrentNewPage}
                    currentNewPage={this.props.currentNewPage}
                  />
                  <Button className='add-button' hidden={(this.props.isProductFormOpen)} onClick={() => this.openProductForm()}>+</Button>
                </div>
                :
                <div>
                  {(!this.props.isReviewProduct) ?
                    <AddProduct
                      onSubmit={this.createProduct}
                      setActivePage={this.setActivePage}
                      setProps={this.props.setProps}
                      getProductType={this.props.getProductType}
                      productType={this.props.productType}
                      fetchUOM={this.props.fetchUOM}
                      uom={this.props.uom}
                      isEdit={this.props.isEdit}
                      viewProductInfo={this.props.viewProductInfo}
                    />
                    :
                    <ReviewProduct
                      setActivePage={this.setActivePage}
                      setProps={this.props.setProps}
                      isEdit={this.props.isEdit}
                      productType={this.props.productType}
                      viewProductInfo={this.props.viewProductInfo}
                    />
                  }
                  <div className='btn-right'>
                    <Button className={(this.props.isReviewProduct) ? "btn btn-primary reviewCancel" : "btn btn-primary  create cancel"} onClick={this.back}>Back</Button>
                  </div>
                </div>
              }
              <div className="clearfix"></div>
              <Footer
                currentLanguage={this.props.currentLanguage}

              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Products.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default Products;