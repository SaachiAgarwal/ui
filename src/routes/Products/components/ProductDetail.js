import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Pagination from 'react-paginate';

/** ProductDetail
 *
 * @description This class is responsible to display products records
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.handlePageClick = this.handlePageClick.bind(this);
  }

  handlePageClick(value) {
    this.props.getProductDetails(this.props.searchText, value.selected + 1, this.props.options.pageSize);
    this.props.setCurrentNewPage(value.selected + 1)
  }

  render() {
    return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options}>
          <TableHeaderColumn dataField='product_id' hidden={true} isKey={true}>Order Id</TableHeaderColumn>
          <TableHeaderColumn dataField='product_name'>Product Name</TableHeaderColumn>
          <TableHeaderColumn dataField='company_type_name'>Company Type</TableHeaderColumn>
          <TableHeaderColumn key="editAction" dataFormat={this.props.productFormatter}>Action</TableHeaderColumn>
        </BootstrapTable>
        <div className="pagination-box">
          <Pagination
            initialPage={this.props.currentNewPage - 1}
            previousLabel={"Prev"}
            nextLabel={"Next"}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={this.props.pages}
            pageRangeDisplayed={5}
            onPageChange={this.handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        </div>
      </div>
    );
  }
}

ProductDetail.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default ProductDetail;