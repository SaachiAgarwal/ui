import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Button } from 'reactstrap';
import _ from 'lodash';
import DropdownList from 'react-widgets/lib/DropdownList';
import 'react-widgets/dist/css/react-widgets.css';

import renderField from './renderField';
import SubmitButtons from 'components/SubmitButtons';
import {getProductTypeId} from 'components/Helper';

/** func Validate
 * description validate method will validate all the control fields of form based on provided criteria
 *
 * return Array of Errors
 */
const validate = values => {
  const errors = {}
  if (!values.product_name) {
    errors.product_name = 'Required'
  }
  if (!values.product_type_name) {
    errors.product_type_name = 'Required'
  }
  if (!values.product_uom) {
    errors.product_uom = 'Required'
  }
  return errors
}

const renderDropdownList = ({ input, label, data, valueField, textField, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className={`form-group ${touched && invalid ? '' : ''}`}>
      <label  className="control-label">{label}</label>
      <div>
        <DropdownList {...input}
          data={data}
          onChange={input.onChange} 
        />
        {submitFailed && error && <span className="error-danger">{error}</span>}
      </div>
    </div>
  ); 
}

/** AddProduct
 *
 * @description This class is a based class for Product form functionality
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class AddProduct extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      initData: {},
    }
  }

  componentWillMount() {
    this.props.setActivePage('addProduct');
    if (!this.props.isEdit) 
      this.props.getProductType();
      this.props.fetchUOM();
  }

  componentDidMount() {
    if (this.props.isEdit) this.handleInitialize();
  }

  handleInitialize() {
    if (!_.isEmpty(this.props.viewProductInfo)) {
      const initData = {
        product_name: this.props.viewProductInfo.product_name,
        product_type_name: getProductTypeId(this.props.viewProductInfo.product_type_id, this.props.productType, true),
        product_uom: this.props.viewProductInfo.product_uom,
        product_description: this.props.viewProductInfo.product_description,
      };

      this.setState({initData: initData});
      this.props.initialize(initData);
    }
  }

  componentWillUnmount() {
    this.props.setActivePage('product');
    this.props.setProps(false, 'isProductFormOpen');
    this.props.setProps(false, 'isEdit');
  }

  render() {
    const {handleSubmit} = this.props;
    return (
      <div className='col-md-12 col-lg-12 create-order'>
        <div>
          <form onSubmit={handleSubmit}>
            <div className="form-row">
              <div className="form-group col-md-6">
                <Field
                  name='product_name'
                  type="text"
                  component={renderField}
                  label="Product Name *"
                />
              </div>
              <div className="form-group col-md-6">
                <Field
                  className="select-menu" 
                  name="product_type_name"
                  label="Product Type *"
                  component={renderDropdownList}
                  data={(!_.isEmpty(this.props.productType)) ? this.props.productType.map(function(list) {return list.product_type_name}) : []}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6">
                <Field
                  className="select-menu" 
                  name="product_uom"
                  label="UOM *"
                  component={renderDropdownList}
                  data={(!_.isEmpty(this.props.uom)) ? this.props.uom.map(function(list) {return list.uom}) : []}
                />
              </div>
              <div className="form-group col-md-6">
                <Field
                  name='product_description'
                  type="text"
                  component={renderField}
                  label="Product description"
                />
              </div>
            </div>
            <div>
              <SubmitButtons
                submitLabel={'Add'}
                className='btn btn-primary create'
                submitting={this.props.submitting}
              />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

AddProduct.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default reduxForm({
  form: 'AddProduct',
  validate,
})(AddProduct)