import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Button } from 'reactstrap';
import _ from 'lodash';

import SubmitButtons from 'components/SubmitButtons';
import {getCompanyTypeId, getProductTypeId, getUOM} from 'components/Helper';

/** func renderLink
 *
 * @description This method returns a field object having a span to achieve onClick functionality. Simply we can not use
 *   onClick on field wrapper.
 *
 * return A component
 */
const renderLink = ({ input, label, type, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className="btn-bs-file">
      <span className='form-control'>
        <p>{customProps.value}</p>
        {(input.name === 'linkTo') &&
          <span>
            <img src={Images.fileUploadIcon} alt=""  data-toggle="modal" data-target="#myModal"/>
          </span>
        }
      </span>
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

/** ReviewProduct
 *
 * @description This class is a based class for Product form functionality
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class ReviewProduct extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      initData: {},
    }
  }

  componentWillMount() {
    this.props.setActivePage('reviewProduct');
  }

  componentDidMount() {
    this.handleInitialize();
  }

  handleInitialize() {
    if (!_.isEmpty(this.props.viewProductInfo)) {
      const initData = {
        product_name: this.props.viewProductInfo.product_name,
        product_type_name: getProductTypeId(this.props.viewProductInfo.product_type_id, this.props.productType, true),
        product_uom: this.props.viewProductInfo.product_uom,
        product_description: this.props.viewProductInfo.product_description,
      };

      this.setState({initData: initData});
      this.props.initialize(initData);
    }
  }

  componentWillUnmount() {
    this.props.setProps(false, 'isReviewProduct');
    this.props.setProps(false, 'isProductFormOpen');
  }

  render() {
    const {handleSubmit} = this.props;
    return (
      <div className='col-md-12 col-lg-12 create-order'>
        <div>
          <form onSubmit={handleSubmit}>
            <div className="form-row">
              <div className="form-group col-md-6">
                <label>Product Name</label>
                <Field
                  name='product_name'
                  type="text"
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.product_name : ''
                  }}
                />
              </div>
              <div className="form-group col-md-6">
              <label>Product Type</label>
                <Field
                  className="select-menu" 
                  name="product_type_name"
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.product_type_name : ''
                  }}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6">
                <label>UOM</label>
                <Field
                  className="select-menu" 
                  name="product_uom"
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.product_uom : ''
                  }}
                />
              </div>
              <div className="form-group col-md-6">
                <label>Product description</label>
                <Field
                  name='product_description'
                  type="text"
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.product_description : ''
                  }}
                />
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

ReviewProduct.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default reduxForm({
  form: 'ReviewProduct',
})(ReviewProduct)