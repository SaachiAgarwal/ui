import axios from 'axios';
import { browserHistory } from 'react-router';

import {Config, Url} from 'config/Config';
import {getLocalStorage, saveLocalStorage, getToken, getPages, addFullName} from 'components/Helper';
// ------------------------------------
// Constant
// ------------------------------------
export const SET_ALERT_MESSAGE = 'SET_ALERT_MESSAGE';
export const FETCHING = 'FETCHING';
export const ERROR = 'ERROR';
export const SET_TOTAL_RECORDS_COUNT = 'SET_TOTAL_RECORDS_COUNT';
export const SET_TOTAL_PAGES = 'SET_TOTAL_PAGES';
export const COMPANY_RECEIVED_SUCCES = 'COMPANY_RECEIVED_SUCCES';
export const SET_PRODUCT_FORM_STATE = 'SET_PRODUCT_FORM_STATE';
export const SET_REVIEW_PRODUCT_STATE = 'SET_REVIEW_PRODUCT_STATE';
export const SET_EDIT_STATE = 'SET_EDIT_STATE';
export const PRODUCTS_RECEIVED_SUCCESS = 'PRODUCTS_RECEIVED_SUCCESS';
export const SET_PRODUCT_TYPE = 'SET_PRODUCT_TYPE';
export const SET_PRODUCT_DETAIL = 'SET_PRODUCT_DETAIL';
export const SET_CURRENT_NEW_PAGE = 'SET_CURRENT_NEW_PAGE'
// ------------------------------------
// Actions
// ------------------------------------
export function setAlertMeassage(status, message, orderStatus=false) {
  return {
    type: SET_ALERT_MESSAGE,
    showAlert: status,
    alertMessage: message,
    status: orderStatus
  };
}

export function fetching(status) {
  return {
    type: FETCHING,
    fetching: status,
  };
}

export function errorFetching(status) {
  return {
    type: ERROR,
    error: status,
  };
}

export function setTotalRecordCount(payload) {
  return {
    type: SET_TOTAL_RECORDS_COUNT,
    totalRecordCount: payload,
  };
}

export function setTotalPages(payload) {
  return {
    type: SET_TOTAL_PAGES,
    pages: payload,
  };
}

export function setProductFormState(status) {
  return {
    type: SET_PRODUCT_FORM_STATE,
    isProductFormOpen: status,
  };
}

export function setReviewProductState(status) {
  return {
    type: SET_REVIEW_PRODUCT_STATE,
    isReviewProduct: status,
  };
}

export function setEditState(status) {
  return {
    type: SET_EDIT_STATE,
    isEdit: status,
  };
}

export function productsReceivedSuccess(payload) {
  return {
    type: PRODUCTS_RECEIVED_SUCCESS,
    productsDetail: payload,
  };
}

export function fetchingProductTypeSuccess(payload) {
  return {
    type: SET_PRODUCT_TYPE,
    productType: payload,
  };
}

export function setProductDetail(payload) {
  return {
    type: SET_PRODUCT_DETAIL,
    viewProductInfo: payload,
  };
}

export function setCurrentNewPage(payload){
  return{
    type: SET_CURRENT_NEW_PAGE,
    currentNewPage: payload
  }
}

// ------------------------------------
// Action creators
// ------------------------------------
export const resetAlertBox = (showAlert, message) => {
  return (dispatch) => {
    dispatch(setAlertMeassage(showAlert, message));
  }
}

export const setProps = (status, key) => {
  return (dispatch) => {
    switch(key) {
      case 'isProductFormOpen':
        dispatch(setProductFormState(status));
        break;
      case 'isReviewProduct':
        dispatch(setReviewProductState(status));
        break;
      case 'isEdit':
        dispatch(setEditState(status));
        break;
    }
  }  
}

export const getProductDetails = (query='', currentPage=1, pageSize=10) => {
  return (dispatch) => {
    // console.log('query  ', query);
    if (!query) dispatch(fetching(true));
    let requestMethod = 'post';
    let data = {};
    data = {
      search_text: query,
      page_size: pageSize,
      page_number: currentPage
    }

   let endPoint = 'admin/list_products';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          let totalPage = getPages(response.data.total_recordcount);
          dispatch(productsReceivedSuccess(response.data.data));
          dispatch(setTotalRecordCount(response.data.total_recordcount));
          dispatch(setTotalPages(totalPage));
        }
        resolve(true);
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'No products found.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const getProductType = () => {
  return (dispatch) => {
    dispatch(fetching(true));
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: `${Config.url}masters/get_product_type`,
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(fetchingProductTypeSuccess(response.data));
        }
        resolve(true);
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'There is no company type.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const addProduct = (values, isEdit) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let requestMethod = (!isEdit) ? 'post' : 'post';
    let endPoint = (!isEdit) ? 'admin/create_product' : 'admin/update_product';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: values,
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(setProductFormState(false));
          dispatch(setAlertMeassage(true, response.data.data, true));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'Product can not be added.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const viewProductDetail = (productId) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let requestMethod = 'get';
    let endPoint = 'admin/product_details';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        params: {
          product_id: productId
        },
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(setProductFormState(false));
          let lStorage = {
            product_id: response.data.data.product_id,
          }
          saveLocalStorage('products', lStorage);
          dispatch(setProductDetail(response.data.data));
        }
        resolve(true);
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'There is no such product.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const deleteProduct = (productId) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let requestMethod = 'post';
    let endPoint = 'admin/delete_product';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: {
          product_id: productId
        },
        headers: {'token': getToken()}
      }).then( response => {
        console.log(response);
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, true));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'product can not be deleted.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const action = {
  resetAlertBox,
  setProps,
};

// ------------------------------------
// Action handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_ALERT_MESSAGE]: (state, action) => {
    return {
      ...state,
      showAlert: action.showAlert,
      alertMessage: action.alertMessage,
      status: action.status
    }
  },
  [FETCHING]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching
    }
  },
  [ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
  [SET_TOTAL_RECORDS_COUNT]: (state, action) => {
    return {
      ...state,
      totalRecordCount: action.totalRecordCount
    }
  },
  [SET_TOTAL_PAGES]: (state, action) => {
    return {
      ...state,
      pages: action.pages
    }
  },
  [SET_PRODUCT_FORM_STATE]: (state, action) => {
    return {
      ...state,
      isProductFormOpen: action.isProductFormOpen
    }
  },
  [SET_REVIEW_PRODUCT_STATE]: (state, action) => {
    return {
      ...state,
      isReviewProduct: action.isReviewProduct
    }
  },
  [SET_EDIT_STATE]: (state, action) => {
    return {
      ...state,
      isEdit: action.isEdit
    }
  },
  [PRODUCTS_RECEIVED_SUCCESS]: (state, action) => {
    return {
      ...state,
      productsDetail: action.productsDetail
    }
  },
  [SET_PRODUCT_TYPE]: (state, action) => {
    return {
      ...state,
      productType: action.productType
    }
  },
  [SET_PRODUCT_DETAIL]: (state, action) => {
    return {
      ...state,
      viewProductInfo: action.viewProductInfo
    }
  },
  [SET_CURRENT_NEW_PAGE]: (state, action) => {
    return{
      ...state,
      currentNewPage: action.currentNewPage
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  showAlert: false,
  status: false,
  alertMessage: '',
  fetching: false,
  error: false,
  totalRecordCount: 0,
  pages: 1,
  isProductFormOpen: false,
  isReviewProduct: false,
  isEdit: false,
  productsDetail: [],
  productType: [],
  viewProductInfo: {},
  currentNewPage: 1
};

export default function productReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
