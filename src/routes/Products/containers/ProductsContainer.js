/** ProductsContainer
 *
 * @description This is container that manages the states for all Products related stuff.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */

import { connect } from 'react-redux';

import Products from '../components/Products';
import {resetAlertBox, setProps, getProductDetails, getProductType, addProduct, viewProductDetail, deleteProduct, setCurrentNewPage} from '../modules/products';
import {setInnerNav, setParam,setSearchString,setReportFetching,setGlobalState, fetchUOM, setCurrentLanguage} from '../../../store/app';

const mapStateToProps = (state) => {
  return({
    navIndex: state.app.navIndex,
    navInnerIndex: state.app.navInnerIndex,
    navClass: state.app.navClass,
    uom: state.app.uom,
    fetchingGlobal: state.app.fetchingGlobal,
    showAlert: state.Products.showAlert,
    status: state.Products.status,
    alertMessage: state.Products.alertMessage,
    fetching: state.Products.fetching,
    error: state.Products.error,
    totalRecordCount: state.Products.totalRecordCount,
    pages: state.Products.pages,
    isProductFormOpen: state.Products.isProductFormOpen,
    isReviewProduct: state.Products.isReviewProduct,
    isEdit: state.Products.isEdit,
    productsDetail: state.Products.productsDetail,
    productType: state.Products.productType,
    viewProductInfo: state.Products.viewProductInfo,
    currentNewPage: state.Products.currentNewPage,
    currentLanguage: state.app.currentLanguage
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    setGlobalState: (value) => dispatch(setGlobalState(value)),
    fetchUOM: (type) => dispatch(fetchUOM(type)),
    resetAlertBox: (showAlert, message) => dispatch(resetAlertBox(showAlert, message)),
    setProps: (status, key) => dispatch(setProps(status, key)),
    getProductDetails: (query, currentPage, pageSize) => dispatch(getProductDetails(query, currentPage, pageSize)),
    getProductType: () => dispatch(getProductType()),
    addProduct: (data, isEdit) => dispatch(addProduct(data, isEdit)),
    viewProductDetail: (productId) => dispatch(viewProductDetail(productId)),
    deleteProduct: (productId) => dispatch(deleteProduct(productId)),
    setCurrentNewPage: (page) => dispatch(setCurrentNewPage(page)),
    setCurrentLanguage: (lang) => dispatch(setCurrentLanguage(lang)),
    setInnerNav: (lang) => dispatch(setInnerNav(lang)),
    setParam: (lang) => dispatch(setParam(lang)),
    setSearchString: (lang) => dispatch(setSearchString(lang)),
    setReportFetching: (lang) => dispatch(setReportFetching(lang)),

  });
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);
