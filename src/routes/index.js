import { browserHistory } from 'react-router';
import CoreLayout from '../layouts/PageLayout/PageLayout';
import Home from './Home';
import Login from './Login';
import OrdersRoute from './Orders';
import ShipmentsRoute from './Shipments';
import AuditTrail from './AuditTrail';
import StockRoute from './Stock';
import ProcessLossRoute from './ProcessLoss';
import ScanRoute from './Scan';
import SettingsRoute from './Settings';
import UserRoute from './User';
import CompanyRoute from './Company';
import BusinessRoute from './BusinessUnit';
import ProductsRoute from './Products';
import ErrorsRoute from './Errors';
import MaintenanceRoute from './Maintenance';
import Configuration from './Configuration'
import Qa from './Qa'
import {Url, APP} from 'config/Config';
import Reports from './Reports';
import AdminReports from './AdminReports';
import ExportRebate from './ExportRebate';
import { getLocalStorage, saveLocalStorage } from 'components/Helper';

export function checkPageRestriction(nextState, replace, callback) {
  let defaultUrl = '/';
  let access = false;
  let lStorage = getLocalStorage('loginDetail');
  let date1 = lStorage.loginTime;
  let date2 = new Date().getTime();
  var res = Math.abs(date1 - date2) / 1000;
  var minutes = Math.floor(res / 60) % 60;

  if (minutes < 240) {
    access = true;
  }

  if (APP.maintenance) {
    access = false;
    defaultUrl = Url.MAINTENANCE_PAGE;
  }

  const pathname = nextState.location.pathname;
  switch (pathname) {
    case Url.LOGIN_PAGE:
      access = true;
      break;
    case '/':
      access = true;
      break;
    case Url.BRAND_PAGE:
      access = access;
      break;
    case Url.ORDER_PAGE:
      access = access;
      break;
    case Url.SHIPMENT_PAGE:
      access = access;
      break;
    case Url.STOCK_PAGE:
      access = access;
      break;
    case Url.REPORTS_PAGE:
      access = access;
      break;
    case Url.AUDIT_PAGE:
      if (APP.maintenance) {
        access = false;
        defaultUrl = Url.MAINTENANCE_PAGE;
      } else {
        access = true;
      }
      break;
    case Url.PROCESS_LOSS_PAGE:
      access = access;
      break;
    case Url.SCANNER_PAGE:
      access = access;
      break;
    case Url.SETTINGS_PAGE:
      access = access;
      break;
    case Url.USER_PAGE:
      access = access;
      break;
    case Url.COMPANY_PAGE:
      access = access;
      break;
    case Url.BUSINESS_PAGE:
      access = access;
      break;
    case Url.PRODUCTS_PAGE:
      access = access;
      break;
    case Url.EXPORT_REBATE:
      access = access;
      break;  
    case Url.ERRORS_PAGE:
      if (APP.maintenance) {
        access = false;
        defaultUrl = Url.MAINTENANCE_PAGE;
      } else {
        access = true;
      }
      break;
    case Url.MAINTENANCE_PAGE:
      access = true;
      break;
    case Url.SETTINGS123_PAGE:
      access = true;
    case Url.QA_PAGE:
      access = true
    case Url.ADMIN_REPORTS:
      access = true
  }

  if (access === false) {
    console.log('access denied !!!');
    window.location.href = defaultUrl;
  } else {
    return callback()
  }
}

export const createRoutes = (store) => ({
  path: '/',
  component: CoreLayout,
  indexRoute: Login(store),
  onEnter: checkPageRestriction,
  childRoutes: [
    Login(store),
    OrdersRoute(store),
    ShipmentsRoute(store),
    AuditTrail(store),
    StockRoute(store),
    ProcessLossRoute(store),
    ScanRoute(store),
    SettingsRoute(store),
    UserRoute(store),
    CompanyRoute(store),
    BusinessRoute(store),
    ProductsRoute(store),
    ErrorsRoute(store),
    MaintenanceRoute(store),
    Configuration(store),
    Qa(store),
    Reports(store),
    AdminReports(store),
    ExportRebate(store)
  ]
})

export default createRoutes;
