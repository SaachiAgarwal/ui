import axios from 'axios';
import { browserHistory } from 'react-router';

import {Config, Url} from 'config/Config';
import {getLocalStorage, saveLocalStorage, getToken, getPages, addFullName} from 'components/Helper';
// ------------------------------------
// Constant
// ------------------------------------
export const SET_ALERT_MESSAGE = 'SET_ALERT_MESSAGE';
export const FETCHING = 'FETCHING';
export const ERROR = 'ERROR';
export const SET_TOTAL_RECORDS_COUNT = 'SET_TOTAL_RECORDS_COUNT';
export const SET_TOTAL_PAGES = 'SET_TOTAL_PAGES';
export const BUSINESS_RECEIVED_SUCCES = 'BUSINESS_RECEIVED_SUCCES';
export const SET_BUSINESS_FORM_STATE = 'SET_BUSINESS_FORM_STATE';
export const SET_REVIEW_BUSINESS_STATE = 'SET_REVIEW_BUSINESS_STATE';
export const SET_EDIT_STATE = 'SET_EDIT_STATE';
export const SET_COMPANY_LIST = 'SET_COMPANY_LIST';
export const SET_BUSINESS_DETAIL = 'SET_BUSINESS_DETAIL';
export const SET_CURRENT_NEW_PAGE = 'SET_CURRENT_NEW_PAGE'

// ------------------------------------
// Actions
// ------------------------------------
export function setAlertMeassage(status, message, orderStatus=false) {
  return {
    type: SET_ALERT_MESSAGE,
    showAlert: status,
    alertMessage: message,
    status: orderStatus
  };
}

export function fetching(status) {
  return {
    type: FETCHING,
    fetching: status,
  };
}

export function errorFetching(status) {
  return {
    type: ERROR,
    error: status,
  };
}

export function setTotalRecordCount(payload) {
  return {
    type: SET_TOTAL_RECORDS_COUNT,
    totalRecordCount: payload,
  };
}

export function setTotalPages(payload) {
  return {
    type: SET_TOTAL_PAGES,
    pages: payload,
  };
}

export function businessReceivedSuccess(payload) {
  return {
    type: BUSINESS_RECEIVED_SUCCES,
    businessDetail: payload,
  };
}

export function setBusinessFormState(status) {
  return {
    type: SET_BUSINESS_FORM_STATE,
    isBusinessFormOpen: status,
  };
}

export function setReviewBusinessState(status) {
  return {
    type: SET_REVIEW_BUSINESS_STATE,
    isReviewBusiness: status,
  };
}

export function setEditState(status) {
  return {
    type: SET_EDIT_STATE,
    isEdit: status,
  };
}

export function fetchingCompaniesSuccess(payload) {
  return {
    type: SET_COMPANY_LIST,
    companiesList: payload,
  };
}

export function setBusinessDetail(payload) {
  return {
    type: SET_BUSINESS_DETAIL,
    viewBusinessInfo: payload,
  };
}

// ------------------------------------
// Action creators
// ------------------------------------
export const resetAlertBox = (showAlert, message) => {
  return (dispatch) => {
    dispatch(setAlertMeassage(showAlert, message));
  }
}
export function setCurrentNewPage(payload){
  return{
    type: SET_CURRENT_NEW_PAGE,
    currentNewPage: payload
  }
}

export const setProps = (status, key) => {
  return (dispatch) => {
    switch(key) {
      case 'isBusinessFormOpen':
        dispatch(setBusinessFormState(status));
        break;
      case 'isReviewBusiness':
        dispatch(setReviewBusinessState(status));
        break;
      case 'isEdit':
        dispatch(setEditState(status));
        break;
    }
  }  
}

export const getBusinessDetails = (query='', currentPage=1, pageSize=10) => {
  return (dispatch) => {
    if (!query) dispatch(fetching(true));
    let requestMethod = 'post';
    let data = {};
    data = {
      search_text: query,
      page_size: pageSize,
      page_number: currentPage
    }

   let endPoint = 'admin/list_company_bu';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          let totalPage = getPages(response.data.total_recordcount);
          dispatch(businessReceivedSuccess(response.data.data));
          dispatch(setTotalRecordCount(response.data.total_recordcount));
          dispatch(setTotalPages(totalPage));
          resolve(true);
        }
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        dispatch(setAlertMeassage(true, 'No business unit found.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const getCompanies = () => {
  return (dispatch) => {
    dispatch(fetching(true));
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: `${Config.url}admin/list_companies`,
        data: {
          page_size: 9999
        },
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(fetchingCompaniesSuccess(response.data.data));
        }
        resolve(true);
      }).catch( error => {
        console.log(error)
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        dispatch(setAlertMeassage(true, 'There is no company type.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const addBusiness = (values, isEdit) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let requestMethod = 'post';
    let endPoint = (!isEdit) ? 'admin/create_company_bu' : 'admin/update_company_bu';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: values,
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(setBusinessFormState(false));
          dispatch(setAlertMeassage(true, response.data.data, true));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'Business unit can not be added.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const viewBusinessDetail = (companyId) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let requestMethod = 'get';
    let endPoint = 'admin/company_bu_details';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        params: {
          company_bu_id: companyId
        },
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(setBusinessFormState(false));
          let lStorage = {
            company_bu_id: response.data.data.company_bu_id
          }
          saveLocalStorage('businessDetail', lStorage);
          dispatch(setBusinessDetail(response.data.data));
        }
        resolve(true);
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'There is no company type.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const deleteBusiness = (companyId) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let requestMethod = 'post';
    let endPoint = 'admin/delete_company_bu';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: {
          company_bu_id: companyId
        },
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, true));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'Business unit can not be deleted.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const action = {
  resetAlertBox,
  setProps,
  getBusinessDetails,
  getCompanies,
  addBusiness,
  viewBusinessDetail,
  deleteBusiness,
};

// ------------------------------------
// Action handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_ALERT_MESSAGE]: (state, action) => {
    return {
      ...state,
      showAlert: action.showAlert,
      alertMessage: action.alertMessage,
      status: action.status
    }
  },
  [FETCHING]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching
    }
  },
  [ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
  [SET_TOTAL_RECORDS_COUNT]: (state, action) => {
    return {
      ...state,
      totalRecordCount: action.totalRecordCount
    }
  },
  [SET_TOTAL_PAGES]: (state, action) => {
    return {
      ...state,
      pages: action.pages
    }
  },
  [BUSINESS_RECEIVED_SUCCES]: (state, action) => {
    return {
      ...state,
      businessDetail: action.businessDetail
    }
  },
  [SET_BUSINESS_FORM_STATE]: (state, action) => {
    return {
      ...state,
      isBusinessFormOpen: action.isBusinessFormOpen
    }
  },
  [SET_REVIEW_BUSINESS_STATE]: (state, action) => {
    return {
      ...state,
      isReviewBusiness: action.isReviewBusiness
    }
  },
  [SET_EDIT_STATE]: (state, action) => {
    return {
      ...state,
      isEdit: action.isEdit
    }
  },
  [SET_COMPANY_LIST]: (state, action) => {
    return {
      ...state,
      companiesList: action.companiesList
    }
  },
  [SET_BUSINESS_DETAIL]: (state, action) => {
    return {
      ...state,
      viewBusinessInfo: action.viewBusinessInfo
    }
  },
  [SET_CURRENT_NEW_PAGE]: (state, action) => {
    return{
      ...state,
      currentNewPage: action.currentNewPage
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  showAlert: false,
  status: false,
  alertMessage: '',
  fetching: false,
  error: false,
  totalRecordCount: 0,
  pages: 1,
  businessDetail: [],
  isBusinessFormOpen: false,
  isReviewBusiness: false,
  isEdit: false,
  companiesList: [],
  viewBusinessInfo: {},
  currentNewPage: 1
};

export default function companyReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
