/** BusinessContainer
 *
 * @description This is container that manages the states for all Business related stuff.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */

import { connect } from 'react-redux';

import Business from '../components/Business';
import {resetAlertBox, setProps, getBusinessDetails, getCompanies, addBusiness, viewBusinessDetail, deleteBusiness, setCurrentNewPage} from '../modules/business';
import {setInnerNav,setParam,setSearchString,setReportFetching,setGlobalState, setCurrentLanguage} from '../../../store/app';

const mapStateToProps = (state) => {
  return({
    navIndex: state.app.navIndex,
    navInnerIndex: state.app.navInnerIndex,
    navClass: state.app.navClass,
    showAlert: state.Business.showAlert,
    status: state.Business.status,
    alertMessage: state.Business.alertMessage,
    fetching: state.Business.fetching,
    error: state.Business.error,
    totalRecordCount: state.Business.totalRecordCount,
    pages: state.Business.pages,
    businessDetail: state.Business.businessDetail,
    isBusinessFormOpen: state.Business.isBusinessFormOpen,
    isReviewBusiness: state.Business.isReviewBusiness,
    isEdit: state.Business.isEdit,
    companiesList: state.Business.companiesList,
    viewBusinessInfo: state.Business.viewBusinessInfo,
    currentNewPage: state.Business.currentNewPage,
    currentLanguage: state.app.currentLanguage
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    setGlobalState: (value) => dispatch(setGlobalState(value)),
    resetAlertBox: (showAlert, message) => dispatch(resetAlertBox(showAlert, message)),
    setProps: (status, key) => dispatch(setProps(status, key)),
    getBusinessDetails: (query, currentPage, pageSize) => dispatch(getBusinessDetails(query, currentPage, pageSize)),
    getCompanies: () => dispatch(getCompanies()),
    addBusiness: (values, isEdit) => dispatch(addBusiness(values, isEdit)),
    viewBusinessDetail: (companyId) => dispatch(viewBusinessDetail(companyId)),
    deleteBusiness: (companyId) => dispatch(deleteBusiness(companyId)),
    setCurrentNewPage: (page) => dispatch(setCurrentNewPage(page)),
    setCurrentLanguage: (lang) => dispatch(setCurrentLanguage(lang)),
    setInnerNav: (lang) => dispatch(setInnerNav(lang)),
    setParam: (lang) => dispatch(setParam(lang)),
    setSearchString: (lang) => dispatch(setSearchString(lang)),
    setReportFetching: (lang) => dispatch(setReportFetching(lang)),
  });
};

export default connect(mapStateToProps, mapDispatchToProps)(Business);
