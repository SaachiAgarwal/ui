import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Button } from 'reactstrap';
import _ from 'lodash';

import renderOnly from 'components/renderOnly';
import SubmitButtons from 'components/SubmitButtons';
import {getCompanyTypeId, getCompanyId, getBusinessCompanyId} from 'components/Helper';

/** ReviewBusiness
 *
 * @description This class is a based class to view Business detail
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class ReviewBusiness extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      initData: {},
    }
  }

  componentWillMount() {
    this.props.setActivePage('reviewbusiness');
  }

  componentDidMount() {
    this.handleInitialize();
  }

  handleInitialize() {
    if (!_.isEmpty(this.props.viewBusinessInfo)) {
      const initData = {
        company_name: this.props.viewBusinessInfo.company_bu_name,
        company_type_name: getBusinessCompanyId(this.props.companiesList, this.props.viewBusinessInfo.company_id, true),
        country: this.props.viewBusinessInfo.country,
        state: this.props.viewBusinessInfo.state,
        city: this.props.viewBusinessInfo.city,
        zip: this.props.viewBusinessInfo.zip,
        company_address: this.props.viewBusinessInfo.company_bu_address,
        phone_number: this.props.viewBusinessInfo.phone_number,
        email_address: this.props.viewBusinessInfo.email_address,
      };

      this.setState({initData: initData});
      this.props.initialize(initData);
    }
  }

  componentWillUnmount() {
    this.props.setActivePage('business');
    this.props.setProps(false, 'isReviewBusiness');
    this.props.setProps(false, 'isBusinessFormOpen');
  }

  render() {
    const {handleSubmit} = this.props;
    return (
      <div className='col-md-12 col-lg-12 create-order'>
        <h2>Business Details</h2>
        <div>
          <form onSubmit={handleSubmit}>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  className="select-menu" 
                  name="company_name"
                  label="Company Name"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.company_name : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  className="select-menu" 
                  name="company_type_name"
                  label="Company Type"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.company_type_name : ''
                  }}
                />
              </div>
            </div>
            <h2>Location Details</h2>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='country'
                  type="text"
                  label="Country"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.country : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name='state'
                  type="text"
                  label="State"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.state : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name="city"
                  type="text"
                  label="City"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.city : ''
                  }}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='zip'
                  type="text"
                  label="Zip"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.zip : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name='company_address'
                  type="text"
                  label="Address"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.company_address : ''
                  }}
                />
              </div>
            </div>
            <h2>Contact Details</h2>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='phone_number'
                  type="text"
                  label="Phone"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.phone_number : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name='email_address'
                  type="email"
                  label="Email"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.email_address : ''
                  }}
                />
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

ReviewBusiness.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default reduxForm({
  form: 'ReviewBusiness',
})(ReviewBusiness)