import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Button } from 'reactstrap';
import _ from 'lodash';
import Swal from 'sweetalert2';

import Loader from 'components/Loader';
import options from 'components/options';
import Alert from "components/Alert";
import HeaderSection from 'components/HeaderSection';
import NavDrawer from 'components/NavDrawer';
import { getLocalStorage, getBusinessCompanyId } from 'components/Helper';
import BusinessDetail from './BusinessDetail';
import AddBusiness from './AddBusiness';
import ReviewBusiness from './ReviewBusiness';
import Footer from 'components/Footer';
import 'css/style.scss';
import ReactLoading from 'react-loading';

/** Business
 *
 * @description This class is a based class for Business stuff
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class Business extends React.Component {
  constructor(props) {
    super(props);
    this.confirm = this.confirm.bind(this);
    this.handleAlertBox = this.handleAlertBox.bind(this);
    this.handleInboxOutboxOrders = this.handleInboxOutboxOrders.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.setActivePage = this.setActivePage.bind(this);
    this.openBusinessForm = this.openBusinessForm.bind(this);
    this.businessFormatter = this.businessFormatter.bind(this);
    this.back = this.back.bind(this);
    this.createBusiness = this.createBusiness.bind(this);
    this.viewBusiness = this.viewBusiness.bind(this);
    this.editBusiness = this.editBusiness.bind(this);
    this.deleteBusiness = this.deleteBusiness.bind(this);

    this.state = {
      active: 'business',
      role: '',
      isSuperUser: '',
      searchText: '',
      previousActivePage: '',
    }
  }

  async componentWillMount() {
    let loginDetail = getLocalStorage('loginDetail');
    await this.setState({
      role: loginDetail.role,
      isSuperUser: loginDetail.userInfo.is_super_user
    });
    this.props.setGlobalState({
      navIndex: 7,
      navInnerIndex: 0,
      navClass: 'Business',
    });
    // getting Business details
    this.props.getBusinessDetails();

  }
  componentWillUnmount() {
    this.props.setCurrentNewPage(1)
  }


  async handleSearch(query) {
    await this.setState({ searchText: query });
    if (query.length >= 2 || query.length === 0) this.props.getBusinessDetails(this.state.searchText, options.currentPage, options.pageSize);
  }

  async handleAlertBox() {
    this.props.resetAlertBox(false, "");
  }

  handleInboxOutboxOrders(classType, innerType) {
    if (classType === 'orders' && innerType === 'inbox') {
    } else if (classType === 'orders' && innerType === 'outbox') {
    } else if (classType === 'shipments' && innerType === 'inbox') {
    } else if (classType === 'Audit Trail') {
    } else if (classType === 'logout') {
      removeLocalStorage('loginDetail');
      browserHistory.push(Url.HOME_PAGE);
    }
  }

  async setActivePage(state) {
    await this.setState({ active: state });
  }

  async openBusinessForm() {
    await this.setState({ previousActivePage: this.state.active });
    await this.setState({ active: 'addBusiness' });
    this.props.setProps(true, 'isBusinessFormOpen');
  }

  confirm(id = '', key = '', msg, confirmText, cancelText) {
    Swal({
      title: 'Warning!',
      text: `${msg}`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmText,
      cancelButtonText: cancelText
    }).then(async (result) => {
      this.props.setProps(false, 'isConfirm');
      if (result.value) {
        if (key === 'deleteBusiness') {
          await this.props.deleteBusiness(id);
          this.props.getBusinessDetails();
        }
      }
    })
  }

  async viewBusiness(row) {
    await this.setState({ previousActivePage: this.state.active });
    await this.props.viewBusinessDetail(row.company_bu_id);
    await this.props.getCompanies();
    this.props.setProps(true, 'isReviewBusiness');
    this.props.setProps(true, 'isBusinessFormOpen');
  }

  async editBusiness(row) {
    await this.setState({ previousActivePage: this.state.active });
    await this.props.viewBusinessDetail(row.company_bu_id);
    await this.props.getCompanies();
    this.props.setProps(true, 'isEdit');
    this.props.setProps(true, 'isBusinessFormOpen');
  }

  deleteBusiness(row) {
    this.confirm(row.company_bu_id, 'deleteBusiness', 'Do you really want to delete', 'Yes delete it', 'No keep it');
  }

  async back() {
    this.setState({ active: this.state.previousActivePage });
    this.props.setProps(false, 'isBusinessFormOpen');
    await this.props.getBusinessDetails('', this.props.currentNewPage, options.PageSize)
  }

  async createBusiness(values) {
    let data = {
      company_bu_name: values.company_bu_name,
      company_id: getBusinessCompanyId(this.props.companiesList, values.company_name, false),
      country: values.country,
      state: values.state,
      city: values.city,
      zip: values.zip,
      company_bu_address: values.company_address,
      phone_number: values.phone_number,
      email_address: values.email_address,
    }

    if (this.props.isEdit) data.company_bu_id = getLocalStorage("businessDetail").company_bu_id;

    await this.props.addBusiness(data, this.props.isEdit);
    await this.props.getBusinessDetails('', this.props.currentNewPage, options.PageSize)
  }

  businessFormatter(row, cell) {
    return (
      <ButtonGroup>
        <Button className="action" onClick={() => this.viewBusiness(cell)} >view</Button>
        <Button className="action" onClick={() => this.editBusiness(cell)} >edit</Button>
        <Button className="action delete" onClick={() => this.deleteBusiness(cell)} >delete</Button>
      </ButtonGroup>
    );
  }

  render() {
    return (
      <div>
        {/* <Loader loading={this.props.fetching} /> */}
        {this.props.fetching &&
          <ReactLoading type="bubbles" style={{
            fill: '#4e5be1e0',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundSize: '200px',
            position: 'fixed',
            zIndex: 10000,
            width: '6%',
            height: '72%',
            top: '50%',
            left: '50%'
          }} />
        }
        <Alert
          showAlert={(typeof this.props.showAlert !== "undefined" ? this.props.showAlert : false)}
          message={(typeof this.props.alertMessage !== "undefined" ? this.props.alertMessage : "")}
          handleAlertBox={this.handleAlertBox}
          status={this.props.status}
        />
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-2 col-lg-2 sidebar">
              <NavDrawer
                role={this.state.role}
                isSuperUser={this.state.isSuperUser}
                setGlobalState={this.props.setGlobalState}
                navIndex={this.props.navIndex}
                navInnerIndex={this.props.navInnerIndex}
                navClass={this.props.navClass}
                handleInboxOutboxOrders={this.handleInboxOutboxOrders}
                currentLanguage={this.props.currentLanguage}
                setInnerNav={this.props.setInnerNav}
                setParam={this.props.setParam}
                setSearchString={this.props.setSearchString}
                setReportFetching={this.props.setReportFetching}
              />
            </div>
            <div className="col-md-10 col-lg-10 col-lg-offset-2 col-md-offset-2 main">
              <HeaderSection
                active={this.state.active}
                searchText={this.state.searchText}
                handleSearch={this.handleSearch}
                currentLanguage={this.props.currentLanguage}
                setCurrentLanguage={this.props.setCurrentLanguage}
              />
              {(!this.props.isBusinessFormOpen) ?
                <div>
                  <BusinessDetail
                    data={this.props.businessDetail}
                    pagination={false}
                    search={false}
                    exportCSV={false}
                    options={options}
                    businessFormatter={this.businessFormatter}
                    getBusinessDetails={this.props.getBusinessDetails}
                    totalRecordCount={this.props.totalRecordCount}
                    pages={this.props.pages}
                    searchText={this.state.searchText}
                    setCurrentNewPage={this.props.setCurrentNewPage}
                    currentNewPage={this.props.currentNewPage}
                  />
                  <Button className='add-button' hidden={(this.props.isBusinessFormOpen)} onClick={() => this.openBusinessForm()}>+</Button>
                </div>
                :
                <div>
                  {(!this.props.isReviewBusiness) ?
                    <AddBusiness
                      onSubmit={this.createBusiness}
                      setActivePage={this.setActivePage}
                      setProps={this.props.setProps}
                      getCompanies={this.props.getCompanies}
                      companiesList={this.props.companiesList}
                      isEdit={this.props.isEdit}
                      viewBusinessInfo={this.props.viewBusinessInfo}
                    />
                    :
                    <ReviewBusiness
                      setActivePage={this.setActivePage}
                      setProps={this.props.setProps}
                      isEdit={this.props.isEdit}
                      companiesList={this.props.companiesList}
                      viewBusinessInfo={this.props.viewBusinessInfo}
                    />
                  }
                  <div className='btn-right'>
                    <Button className={(this.props.isReviewBusiness) ? "btn btn-primary reviewCancel" : "btn btn-primary  create cancel"} onClick={this.back}>Back</Button>
                  </div>
                </div>
              }
              <div className="clearfix"></div>
              <Footer
                currentLanguage={this.props.currentLanguage}

              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Business.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default Business;