import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Button } from 'reactstrap';
import _ from 'lodash';
import DropdownList from 'react-widgets/lib/DropdownList';
import 'react-widgets/dist/css/react-widgets.css';

import renderField from './renderField';
import SubmitButtons from 'components/SubmitButtons';
import {getBusinessCompanyId} from 'components/Helper';

/** func Validate
 * description validate method will validate all the control fields of form based on provided criteria
 *
 * return Array of Errors
 */
const validate = values => {
  const errors = {}
  if (!values.company_bu_name) {
    errors.company_bu_name = 'Required'
  }
  if (!values.company_name) {
    errors.company_name = 'Required'
  }
  return errors
}

const renderDropdownList = ({ input, label, data, valueField, textField, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className={`form-group ${touched && invalid ? '' : ''}`}>
      <label  className="control-label">{label}</label>
      <div>
        <DropdownList {...input}
          data={data}
          onChange={input.onChange} 
        />
        {submitFailed && error && <span className="error-danger">{error}</span>}
      </div>
    </div>
  ); 
}

/** AddBusiness
 *
 * @description This class is a based class for Business form functionality
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class AddBusiness extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      initData: {},
    }
  }

  componentWillMount() {
    this.props.setActivePage('addBusiness');
    if (!this.props.isEdit) this.props.getCompanies();
  }

  componentDidMount() {
    if (this.props.isEdit) this.handleInitialize();
  }

  handleInitialize() {
    if (!_.isEmpty(this.props.viewBusinessInfo)) {
      const initData = {
        company_bu_name: this.props.viewBusinessInfo.company_bu_name,
        company_name: getBusinessCompanyId(this.props.companiesList, this.props.viewBusinessInfo.company_id, true),
        country: this.props.viewBusinessInfo.country,
        state: this.props.viewBusinessInfo.state,
        city: this.props.viewBusinessInfo.city,
        zip: this.props.viewBusinessInfo.zip,
        company_address: this.props.viewBusinessInfo.company_bu_address,
        phone_number: this.props.viewBusinessInfo.phone_number,
        email_address: this.props.viewBusinessInfo.email_address,
      };

      this.setState({initData: initData});
      this.props.initialize(initData);
    }
  }

  componentWillUnmount() {
    this.props.setActivePage('business');
    this.props.setProps(false, 'isBusinessFormOpen');
    this.props.setProps(false, 'isEdit');
  }

  render() {
    const {handleSubmit, companiesList} = this.props;
    return (
      <div className='col-md-12 col-lg-12 create-order'>
        <h2>Business Details</h2>
        <div>
          <form onSubmit={handleSubmit}>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='company_bu_name'
                  type="text"
                  component={renderField}
                  label="Business Unit Name *"
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  className="select-menu" 
                  name="company_name"
                  label="Company *"
                  component={renderDropdownList}
                  data={(!_.isEmpty(companiesList)) ? companiesList.map(function(list) {return list.company_name}) : []}
                />
              </div>
            </div>
            <h2>Locations Details</h2>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name="country"
                  type="text"
                  label="Country"
                  component={renderField}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name="state"
                  type="text"
                  label="State"
                  component={renderField}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name="city"
                  type="text"
                  label="City"
                  component={renderField}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='zip'
                  type="text"
                  component={renderField}
                  label="Zip"
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name='company_address'
                  type="text"
                  component={renderField}
                  label="Address"
                />
              </div>
            </div>
            <h2>Contact Details</h2>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='phone_number'
                  type="text"
                  component={renderField}
                  label="Phone"
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name='email_address'
                  type="email"
                  component={renderField}
                  label="Email"
                />
              </div>
            </div>
            <div>
              <SubmitButtons
                submitLabel={'Add'}
                className='btn btn-primary create'
                submitting={this.props.submitting}
              />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

AddBusiness.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default reduxForm({
  form: 'AddBusiness',
  validate,
})(AddBusiness)