import { injectReducer } from '../../store/reducers';

export default (store) => ({
  path: 'business',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const Business = require('./containers/BusinessContainer').default;
      const reducer = require('./modules/business').default;
      injectReducer(store, { key: 'Business', reducer });
      cb(null, Business);
  }, 'Business');
  },
});
