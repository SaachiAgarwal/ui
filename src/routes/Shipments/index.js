import { injectReducer } from '../../store/reducers';
import {checkPageRestriction} from '../index';

export default (store) => ({
  path: 'shipments',
  onEnter: (nextState, replace) => {
    checkPageRestriction(nextState, replace, () => {})
  },
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const Shipments = require('./containers/ShipmentsContainer').default;
      const reducer = require('./modules/shipments').default;
      injectReducer(store, { key: 'Shipments', reducer });
      cb(null, Shipments);
  }, 'Shipments');
  },
});
