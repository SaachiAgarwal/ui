/** shipments reducer
 *
 * @description This is a reducer responsible for all the create/delete/update/view/fulfill functionality
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
import axios from 'axios';
import { browserHistory } from 'react-router';

import { Config, Url } from 'config/Config';
import { getLocalStorage, saveLocalStorage, getToken, sleep, filterByDate, getPages } from 'components/Helper';
// ------------------------------------
// Constant
// ------------------------------------
export const SET_ALERT_MESSAGE = 'SET_ALERT_MESSAGE';
export const SHOW_CONFIRM_MESSAGE = 'SHOW_CONFIRM_MESSAGE';
export const FETCHING = 'FETCHING';
export const ERROR_FETCHING = 'ERROR_FETCHING';
export const INCOMING_SHIPMENT_SUCCESS = 'INCOMING_SHIPMENT_SUCCESS';
export const OUTGOING_SHIPMENT_SUCCESS = 'OUTGOING_SHIPMENT_SUCCESS';
export const SET_FORM_STATE = 'SET_FORM_STATE';
export const FETCHING_BUYER_SUCCESS = 'FETCHING_BUYER_SUCCESS';
export const FETCHING_BUYER_UNIT = 'FETCHING_BUYER_UNIT';
export const FETCHING_SELLER_UNIT = 'FETCHING_SELLER_UNIT';
export const CREATE_SHIPMENT_SUCCESS = 'CREATE_SHIPMENT_SUCCESS';
export const SET_EDIT_STATE = 'SET_EDIT_STATE';
export const SET_SAVE_STATE = 'SET_SAVE_STATE';
export const SET_ADD_PRODUCT_STATE = 'SET_ADD_PRODUCT_STATE';
export const FETCHING_PRODUCTS_SUCCESS = 'FETCHING_PRODUCTS_SUCCESS';
export const ADDING_PRODUCTS_SUCCESS = 'ADDING_PRODUCTS_SUCCESS';
export const VIEW_SHIPMENT_SUCCESS = 'VIEW_SHIPMENT_SUCCESS';
export const SET_REVIEW_STATE = 'SET_REVIEW_STATE';
export const SHIPMENT_PLACED_SUCCESS = 'SHIPMENT_PLACED_SUCCESS';
export const PULP_SHIPMENT_PLACED_SUCCESS = 'PULP_SHIPMENT_PLACED_SUCCESS';
export const RESET_PROPS = 'RESET_PROPS';
export const SET_PRODUCT_EDIT_STATE = 'SET_PRODUCT_EDIT_STATE';
export const RESTE_ALREADY_SET_PRODUCTS = 'RESTE_ALREADY_SET_PRODUCTS';
export const SHIPMENT_DELETING_SUCCESS = 'SHIPMENT_DELETING_SUCCESS';
export const INCOMING_RECEIVED_SUCCESS = 'INCOMING_RECEIVED_SUCCESS';
export const LINKING_SHIPMENT_SUCCESS = 'LINKING_SHIPMENT_SUCCESS';
export const SET_LOT_EDIT_STATE = 'SET_LOT_EDIT_STATE';
export const SHOULD_PRODUCT_FORM_CLOSE = 'shouldProductFormClose';
export const RESET_FORM_STATE = 'RESET_FORM_STATE';
export const VIEW_STOCK_SUCCESS = 'VIEW_STOCK_SUCCESS';
export const VIEW_LOT_DETAIL = 'VIEW_LOT_DETAIL';
export const LOT_DELETED_SUCCESS = 'LOT_DELETED_SUCCESS';
export const SET_IS_LOT_EDIT = 'SET_IS_LOT_EDIT';
export const SET_UPLOAD_STATUS = 'SET_UPLOAD_STATUS';
export const SET_DOWNLOAD_STATUS = 'SET_DOWNLOAD_STATUS';
export const SET_DOWNLOAD_CATALOGUE_STATUS = 'SET_DOWNLOAD_CATALOGUE_STATUS';
export const SET_CONFIRMATION_CALL_FOR = 'SET_CONFIRMATION_CALL_FOR';
export const RECEIVED_INCOMING_BUYER_ORDER = 'RECEIVED_INCOMING_BUYER_ORDER';
export const SET_DECLINE_STATE = 'SET_DECLINE_STATE';
export const SET_RECEIVE_STATE = 'SET_RECEIVE_STATE';
export const SET_PRODUCT_QTYS = 'SET_PRODUCT_QTYS';
export const CHECK_LOT_STATUS = 'CHECK_LOT_STATUS';
export const SET_TOTAL_RECORD_COUNT = 'SET_TOTAL_RECORD_COUNT';
export const SET_TOTAL_PAGES = 'SET_TOTAL_PAGES';
export const VALIDATE_STOCK_SUCCESS = 'VALIDATE_STOCK_SUCCESS';
export const SET_PERMISSION_STATE = 'SET_PERMISSION_STATE';
export const SET_WITH_ALERT_STATE = 'SET_WITH_ALERT_STATE';
export const SET_STOCK_LOT_DETAIL = 'SET_STOCK_LOT_DETAIL';
export const SET_SHIPQYT_SUCCESS = 'SET_SHIPQYT_SUCCESS';
export const SET_TOTAL_COUNT_OUTSHIP = 'SET_TOTAL_COUNT_OUTSHIP';
export const SHOW_PRODUCT = 'SHOW_PRODUCT';
export const AUTO_ORDER_SUCCESS = 'AUTO_ORDER_SUCCESS';
export const COMPANY_ROLE_SUCCESS = 'COMPANY_ROLE_SUCCESS';
export const SET_COMPANY_DETAIL = 'SET_COMPANY_DETAIL';
export const ERROR = 'ERROR';
export const SET_CURRENT_NEW_PAGE = 'SET_CURRENT_NEW_PAGE';
export const SET_CERT_UPLOAD_STATUS = 'SET_CERT_UPLOAD_STATUS';
export const SET_PDF_STATUS = 'SET_PDF_STATUS'
export const SET_CERT_LOGIN = 'SET_CERT_LOGIN'
export const SET_PDF_RECEIVE_STATUS = 'SET_PDF_RECEIVE_STATUS'
export const SET_JOB_ACTIVE = 'SET_JOB_ACTIVE'
export const RESET_JOB_ACTIVE = 'RESET_JOB_ACTIVE'
export const EXTERNAL_JOB_STATUS = 'EXTERNAL_JOB_STATUS'
export const SET_PRODUCT_STATUS_DETAIL = 'SET_PRODUCT_STATUS_DETAIL'
export const CREATE_EXTERNAL_SUCCESS = 'CREATE_EXTERNAL_SUCCESS'
export const VIEW_PRODUCT_INTERNAL_SUCCESS = 'VIEW_PRODUCT_INTERNAL_SUCCESS'
export const VIEW_EXTERNAL_DETAIL_VAL = 'VIEW_EXTERNAL_DETAIL_VAL'
export const VIEW_UNIQUE_EXTERNAL_DETAIL_VAL = 'VIEW_UNIQUE_EXTERNAL_DETAIL_VAL'
export const RESET_VIEW_UNIQUE_EXTERNAL_SHIPMENT_VAL = 'RESET_VIEW_UNIQUE_EXTERNAL_SHIPMENT_VAL'
export const RESET_VIEW_EXTERNAL_PRODUCT = 'RESET_VIEW_EXTERNAL_PRODUCT'
export const VIEW_UNIQUE_PRODUCT_DETAIL = 'VIEW_UNIQUE_PRODUCT_DETAIL'
export const RESET_UNIQUE_PRODUCT_DETAIL = 'RESET_UNIQUE_PRODUCT_DETAIL'
export const RESET_EXTERNAL_DATA = 'RESET_EXTERNAL_DATA'
export const RESET_EXTERNAL_DETAIL_VAL = 'RESET_EXTERNAL_DETAIL_VAL'
export const IS_EDIT_TEXT = 'IS_EDIT_TEXT'
export const COMPANY_NAME_EXT_VAL = 'COMPANY_NAME_EXT_VAL'
export const SET_COMPANY_EXTERNAL_VENDOR_ID = 'SET_COMPANY_EXTERNAL_VENDOR_ID'
export const SET_COMPANY_EXT_DETAIL = 'SET_COMPANY_EXT_DETAIL'
export const RESET_EXTERNAL_VENDOR_CHECK = 'RESET_EXTERNAL_VENDOR_CHECK'
export const EXT_BUYER_CHECK = 'EXT_BUYER_CHECK'
export const INVOICE_CHECK_VAL = 'INVOICE_CHECK_VAL'
export const RESET_INVOICE_CHECK = 'RESET_INVOICE_CHECK'
export const CHECK_FOR_EXT_BUYER = 'CHECK_FOR_EXT_BUYER'
export const IS_EDIT_VENDOR_VAL = 'IS_EDIT_VENDOR_VAL'
export const FETCH_PRODUCT_EXT = 'FETCH_PRODUCT_EXT'
export const IS_EXT_NUMBER = 'IS_EXT_NUMBER'
export const CERTIFICATE_NUMBER_VAL = 'CERTIFICATE_NUMBER_VAL'
export const FIBRE_DATA_VAL = 'FIBRE_DATA_VAL'
export const SHOW_AUTO_CONFIRM_MESSAGE = 'SHOW_AUTO_CONFIRM_MESSAGE'
export const SET_AUTO_PRODUCT_QTY = 'SET_AUTO_PRODUCT_QTY'
export const OUTGOING_DOWNLOAD_SHIPMENT_SUCCESS = 'OUTGOING_DOWNLOAD_SHIPMENT_SUCCESS'
export const INCOMING_DOWNLOAD_SHIPMENT_SUCCESS = 'INCOMING_DOWNLOAD_SHIPMENT_SUCCESS'
export const SET_CURRENT_LOT_NEW_PAGE = 'SET_CURRENT_LOT_NEW_PAGE'
export const PULP_COMPANY_INFO = 'PULP_COMPANY_INFO'
export const  SET_EXPORT_STATE = 'SET_EXPORT_STATE'
export const GET_EXPORT_REBATE_DOCUMENT = 'GET_EXPORT_REBATE_DOCUMENT'
export const RESET_EXPORT = 'RESET_EXPORT'
export const RESET_BILL_FILE = 'RESET_BILL_FILE'
// ------------------------------------
// Actions
// ------------------------------------
export function setAlertMeassage(status, message, orderStatus = false) {
  return {
    type: SET_ALERT_MESSAGE,
    showAlert: status,
    alertMessage: message,
    status: orderStatus
  };
}

export function showConfirmMessage(status, msg) {
  return {
    type: SHOW_CONFIRM_MESSAGE,
    isConfirm: status,
    confirmMsg: msg
  };
}

export function fetching(status) {
  return {
    type: FETCHING,
    fetching: status
  }
}

export function errorFetching(status) {
  return {
    type: ERROR_FETCHING,
    error: status
  }
}

export function incomingShipmentSuccess(payload) {
  return {
    type: INCOMING_SHIPMENT_SUCCESS,
    incomingShipment: payload
  }
}

export function outgoingShipmentSuccess(payload) {
  return {
    type: OUTGOING_SHIPMENT_SUCCESS,
    outgoingShipment: payload
  }
}

export function setFormState(status) {
  return {
    type: SET_FORM_STATE,
    isShipmentFormOpen: status
  }
}

export function fetchingBuyerSuccess(payload) {
  return {
    type: FETCHING_BUYER_SUCCESS,
    buyerCompniesList: payload
  }
}

export function showProductLinkSuccess(payload) {
  return {
    type: SHOW_PRODUCT,
    showProductAfterLink: payload
  }
}

export function fetchingBuyerUnit(payload) {
  return {
    type: FETCHING_BUYER_UNIT,
    buyerUnitList: payload
  }
}

export function fetchingSellerUnit(payload) {
  return {
    type: FETCHING_SELLER_UNIT,
    sellerUnitList: payload
  }
}

export function createShipmentSuccess(payload) {
  return {
    type: CREATE_SHIPMENT_SUCCESS,
    shipmentDetail: payload
  }
}

export function setEditState(status) {
  return {
    type: SET_EDIT_STATE,
    isEdit: status,
  };
}

export function setSaveState(status) {
  return {
    type: SET_SAVE_STATE,
    isSave: status,
  };
}

export function setAddProductState(status) {
  return {
    type: SET_ADD_PRODUCT_STATE,
    isAddProduct: status,
  };
}

export function fetchingProductsSuccess(payload) {
  return {
    type: FETCHING_PRODUCTS_SUCCESS,
    products: payload,
  };
}

export function AddingProductSuccess(payload) {
  return {
    type: ADDING_PRODUCTS_SUCCESS,
    addedProductDetail: payload
  };
}

export function viewShipmentSuccess(payload) {
  return {
    type: VIEW_SHIPMENT_SUCCESS,
    viewShipmentDetail: payload,
  };
}

export function setReviewState(state) {
  return {
    type: SET_REVIEW_STATE,
    isReviewShipment: state,
  };
}

export function shipmentPlacedSuccess(payload) {
  return {
    type: SHIPMENT_PLACED_SUCCESS,
    shipmentPlacedData: payload,
  };
}

export function pulpshipmentPlacedSuccess(payload) {
  return {
    type: PULP_SHIPMENT_PLACED_SUCCESS,
    pulpshipmentPlacedData: payload,
  };
}

export function autoOrderSuccess(payload) {
  return {
    type: AUTO_ORDER_SUCCESS,
    autoOrderSuccess: payload,
  };
}

export function resetAlreadySetProps() {
  return {
    type: RESET_PROPS,
    viewShipmentDetail: []
  };
}

export function setProductEditState(status) {
  return {
    type: SET_PRODUCT_EDIT_STATE,
    isProductEdit: status
  };
}

export function resetAlreadySetProducts() {
  return {
    type: RESTE_ALREADY_SET_PRODUCTS,
    products: [],
    viewLotDetail: [],
  };
}

export function shipmentDeletedSuccess(payload) {
  return {
    type: SHIPMENT_DELETING_SUCCESS,
    shipmentDeletedData: payload,
  };
}

export function incomingReceivedSuccess(payload) {
  return {
    type: INCOMING_RECEIVED_SUCCESS,
    incomingOrders: payload,
  };
}

export function linkingShipmentSuccess(payload) {
  return {
    type: LINKING_SHIPMENT_SUCCESS,
    linkedShipmentDetail: payload
  }
}

export function setLotEditState(status) {
  return {
    type: SET_LOT_EDIT_STATE,
    isAddLot: status
  }
}

export function shouldProductFormClose(status) {
  return {
    type: SHOULD_PRODUCT_FORM_CLOSE,
    shouldProductFormClose: status
  }
}

export function resetFormState() {
  return {
    type: RESET_FORM_STATE,
    buyerCompniesList: [],
    sellerUnitList: [],
    buyerUnitList: []
  }
}

export function viewStockSuccess(payload) {
  return {
    type: VIEW_STOCK_SUCCESS,
    stockLot: payload
  }
}

export function lotDetailSuccess(payload) {
  return {
    type: VIEW_LOT_DETAIL,
    viewLotDetail: payload
  }
}

export function lotDeletedSuccess(payload) {
  return {
    type: LOT_DELETED_SUCCESS,
    deletedLotDetail: payload
  }
}

export function setIsLotEdit(status) {
  return {
    type: SET_IS_LOT_EDIT,
    isLotEdit: status
  }
}

export function setUploadStatus(status) {
  return {
    type: SET_UPLOAD_STATUS,
    isUpload: status
  }
}

export function setDownloadStatus(status) {
  return {
    type: SET_DOWNLOAD_STATUS,
    isDownload: status
  }
}

export function setDownloadCatalogueStatus(status) {
  return {
    type: SET_DOWNLOAD_CATALOGUE_STATUS,
    isDownloadCatalogue: status
  }
}

export function setConfirmationCallFor(status) {
  return {
    type: SET_CONFIRMATION_CALL_FOR,
    confirmationCallFor: status
  }
}

export function receivedBuyerIncomingOrders(payload) {
  return {
    type: RECEIVED_INCOMING_BUYER_ORDER,
    buyerIncomingOrders: payload
  }
}

export function setDeclineState(status) {
  return {
    type: SET_DECLINE_STATE,
    isDecline: status
  }
}

export function setExportState(status){
  return {
    type: SET_EXPORT_STATE,
    isExport: status
  }
}

export function setReceiveState(status) {
  return {
    type: SET_RECEIVE_STATE,
    isReceive: status
  }
}

export function setProductQuantities(consume_productqty, expected_consume_product_qty, shipment_product_qty) {
  return {
    type: SET_PRODUCT_QTYS,
    consumedPrdQty: consume_productqty,
    expectedConsumedQty: expected_consume_product_qty,
    shipmentPrdQty: shipment_product_qty
  }
}

export function checkLotStatusSuccess(status) {
  return {
    type: CHECK_LOT_STATUS,
    isLotNoBlank: status,
  }
}

export function setTotalRecordCount(payload) {
  return {
    type: SET_TOTAL_RECORD_COUNT,
    totalRecordCount: payload,
  }
}

export function setTotalCountOutShip(payload) {
  return {
    type: SET_TOTAL_COUNT_OUTSHIP,
    totalCountOutShip: payload,
  }
}

export function setTotalPages(payload) {
  return {
    type: SET_TOTAL_PAGES,
    pages: payload,
  }
}

export function validateStockSuccess(status) {
  return {
    type: VALIDATE_STOCK_SUCCESS,
    isStockValid: status,
  }
}

export function setPermissionState(status) {
  return {
    type: SET_PERMISSION_STATE,
    isPermission: status,
  }
}

export function setWithAlertState(status) {
  return {
    type: SET_WITH_ALERT_STATE,
    withAlert: status,
  }
}

export function setStockLotDetail(payload) {
  return {
    type: SET_STOCK_LOT_DETAIL,
    stockLotDetail: payload,
  }
}

export function receiveShipmentQtySuccess(payload) {
  return {
    type: SET_SHIPQYT_SUCCESS,
    receiveShipSuccess: payload,
  }
}

export function companyRoleSuccess(payload) {
  return {
    type: COMPANY_ROLE_SUCCESS,
    companyRole: payload,
  };
}

export function setCompanyDetail(payload) {
  return {
    type: SET_COMPANY_DETAIL,
    viewCompanyInfo: payload,
  };
}
export function isCertUploadStatus(payload) {
  return {
    type: SET_CERT_UPLOAD_STATUS,
    isCertUpload: payload
  }
}
export function setPdfStatus(payload) {
  return {
    type: SET_PDF_STATUS,
    pdfStatus: payload
  }
}
export function setCertLogin(payload) {
  return {
    type: SET_CERT_LOGIN,
    certLogin: payload
  }
}
export function setPdfReceiveStatus(payload) {
  return {
    type: SET_PDF_RECEIVE_STATUS,
    pdfReceiveStatus: payload
  }
}
export function setJobActive(payload) {
  return {
    type: SET_JOB_ACTIVE,
    isJobActive: payload
  }
}

export function resetJobActive(payload) {
  return {
    type: RESET_JOB_ACTIVE,
    isJobActive: payload
  }
}
export function externalJobStatus(payload) {
  return {
    type: EXTERNAL_JOB_STATUS,
    jobStatus: payload
  }
}
export function setProductStatus(payload) {
  return {
    type: SET_PRODUCT_STATUS_DETAIL,
    isProduct: payload
  }
}
export function createExternalSuccess(payload) {
  return {
    type: CREATE_EXTERNAL_SUCCESS,
    externalData: payload
  }
}
export function viewExternalDetail(payload) {
  return {
    type: VIEW_EXTERNAL_DETAIL_VAL,
    externalView: payload
  }
}
export function viewUniqueExternalDetail(payload) {
  return {
    type: VIEW_UNIQUE_EXTERNAL_DETAIL_VAL,
    uniqueExternalView: payload
  }
}
export function viewUniqueProductDetail(payload) {
  return {
    type: VIEW_UNIQUE_PRODUCT_DETAIL,
    uniqueProductView: payload
  }
}
export function resetUniqueProductDetail(payload) {
  return {
    type: RESET_UNIQUE_PRODUCT_DETAIL,
    uniqueProductView: payload
  }
}
export function resetExternalDetail(payload) {
  return {
    type: RESET_EXTERNAL_DETAIL_VAL,
    externalView: payload
  }
}
export function pulpCompany(payload){
  return{
    type: PULP_COMPANY_INFO,
    companyDropPulpInfo: payload
  }
}
// ------------------------------------
// Action creators
// ------------------------------------
export const resetAlertBox = (showAlert, message) => {
  return (dispatch) => {
    dispatch(setAlertMeassage(showAlert, message));
  }
}
export function setCurrentNewPage(payload) {
  return {
    type: SET_CURRENT_NEW_PAGE,
    currentNewPage: payload
  }
}

export function setCurrentLotNewPage(payload) {
  return {
    type: SET_CURRENT_LOT_NEW_PAGE,
    currentNewLotPage: payload
  }
}

export function viewProductExternalSuccess(payload) {
  return {
    type: VIEW_PRODUCT_INTERNAL_SUCCESS,
    productDetail: payload
  }
}
export function resestViewUniqueExternalShipment(payload) {
  return {
    type: RESET_VIEW_UNIQUE_EXTERNAL_SHIPMENT_VAL,
    uniqueExternalView: payload
  }
}
export function resetViewExternalProduct(payload) {
  return {
    type: RESET_VIEW_EXTERNAL_PRODUCT,
    productDetail: payload

  }
}
export function resetExternalData(payload) {
  return {
    type: RESET_EXTERNAL_DATA,
    externalData: payload
  }
}

export function isEditTextVal(payload) {
  return {
    type: IS_EDIT_TEXT,
    isEditText: payload
  }
}
export function companyNameExt(val) {
  return {
    type: COMPANY_NAME_EXT_VAL,
    companyNameExtVal: val
  }
}
export function setCompanyExternalVendorId(val) {
  return {
    type: SET_COMPANY_EXTERNAL_VENDOR_ID,
    companyExtVendorId: val
  }
}
export function companyExtDetail(payload) {
  return {
    type: SET_COMPANY_EXT_DETAIL,
    extVendorCheck: payload
  }
}
export function resetExternVendorCheck(payload) {
  return {
    type: RESET_EXTERNAL_VENDOR_CHECK,
    extVendorCheck: payload
  }
}
export function extBuyerCheck(payload) {
  return {
    type: EXT_BUYER_CHECK,
    extBuyerCheckVal: payload
  }
}
export function invoiceCheck(payload) {
  return {
    type: INVOICE_CHECK_VAL,
    invoiceCheckVal: payload
  }
}
export function resetInvoiceCheck(payload) {
  return {
    type: RESET_INVOICE_CHECK,
    invoiceCheckVal: payload
  }
}
export function checkForExtBuyer(payload) {
  return {
    type: CHECK_FOR_EXT_BUYER,
    isExtBuyer: payload
  }
}

export function isEditVendor(payload) {
  return {
    type: IS_EDIT_VENDOR_VAL,
    isEditVendorVal: payload
  }
}

export function fetchingProductsExt(payload) {
  return {
    type: FETCH_PRODUCT_EXT,
    productsExt: payload
  }
}
export function isExtNumberFunc(payload) {
  return {
    type: IS_EXT_NUMBER,
    isExtNumber: payload
  }
}
export function certificateNumberVal(payload) {
  return {
    type: CERTIFICATE_NUMBER_VAL,
    certificateNumber: payload
  }
}
export function fibreDataVal(payload) {
  return {
    type: FIBRE_DATA_VAL,
    fibreData: payload
  }
}

export function showAutoConfirmMessage(status, msg) {
  return {
    type: SHOW_AUTO_CONFIRM_MESSAGE,
    isAutoConfirm: status,
    confirmAutoMsg: msg
  };
}

export function setAutoProductQuantities(consumedAutoQty, shipmentAutoQty) {
  return{
    type: SET_AUTO_PRODUCT_QTY,
    consumedAutoQty: consumedAutoQty,
    shipmentAutoQty: shipmentAutoQty
  }
}
export function outgoingDownloadShipmentSuccess(payload){
  return{
    type: OUTGOING_DOWNLOAD_SHIPMENT_SUCCESS,
    outgoingDownloadShipment: payload
  }
}
export function incomingDownloadShipmentSuccess(payload){
  return{
    type: INCOMING_DOWNLOAD_SHIPMENT_SUCCESS,
    incomingDownloadShipment: payload
  }
}

export function exportRebateDocument(billFile,invoiceFile){
  console.log('dsadas',billFile,invoiceFile)
  return{
    type: GET_EXPORT_REBATE_DOCUMENT,
    billFile: billFile,
    invoiceFile: invoiceFile
  }
}

export function resetBillFile(){
  return{
    type: RESET_BILL_FILE,
    billFile:[],
    invoiceFile: [],
  }
}

// export function resetExport(){
//   return{
//     type: RESET_EXPORT,
//     billFile: [],
//     invoiceFile: []
//   }
// }

export const fetchShipment = (shipmentType, currentPage = 1, pageSize = 10, frmDt = '', toDt = '', query = '') => {
  return (dispatch, getState) => {
    // console.log('***********fetching start***********');
    const { fetchingFromReport, searchString, fromDate, toDate, param } = getState().app;

    if (!query) dispatch(fetching(true));
    let requestMethod = 'post';
    let data = {};
    if (fetchingFromReport) {
      data = {
        search_text: searchString,
        page_size: pageSize,
        page_number: currentPage,
        from_date: fromDate,
        to_date: toDate,
        db_param: param
      }
    } else {

      data = {
        search_text: query,
        page_size: pageSize,
        page_number: currentPage,
        from_date: frmDt,
        to_date: toDt,
      }
    }

    let endPoint = (shipmentType === 'outgoingShipment') ? 'shipment/outgoing_shipment' : 'shipment/incoming_shipment';
    // console.log(data, endPoint);
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        setTimeout(
          function () {
            console.log(response);
            // dispatch(fetching(false));
            if (response.data.error === 1) {
              dispatch(setAlertMeassage(true, response.data.data, false));
            } else if (response.data.error === 5) {
              dispatch(setAlertMeassage(true, response.data.data, false));
              browserHistory.push(Url.LOGIN_PAGE);
            } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
              response.data.data.map((item) => {
                item["new_invoice_date"] = '';
                item["new_receive_date"] = '';
                if (item.invoice_date != undefined && item.invoice_date !== "") {
                  var spdate = (item.invoice_date.split("T"))[0].split("-");
                  item["new_invoice_date"] = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
                }
                if (item.received_date != undefined && item.received_date !== "") {
                  var spdate = (item.received_date.split("T"))[0].split("-");
                  item["new_receive_date"] = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
                }
              })
              let respFilterByDate = filterByDate(response.data, 'shipment');
              dispatch(setTotalCountOutShip(response.data.total_recordcount));
              let totalPage = getPages(response.data.total_recordcount, pageSize);
              dispatch(setTotalPages(totalPage));
              dispatch(setTotalRecordCount(response.data.total_recordcount));
              (shipmentType === 'outgoingShipment') ? dispatch(outgoingShipmentSuccess(respFilterByDate.data)) : dispatch(incomingShipmentSuccess(respFilterByDate.data));
              // console.log('***********fetching end***********')
              dispatch(fetching(false));
              resolve(true);
    
            }
          }
            .bind(this),
          500
        );
     
      }).catch(error => {
        console.log(error);
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'No shipment found.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}




export const fetchDownloadShipment = (shipmentType, currentPage = 1, pageSize = 10, frmDt = '', toDt = '', query = '') => {
  return (dispatch, getState) => {
    // console.log('***********fetching start***********');
    const { fetchingFromReport, searchString, fromDate, toDate, param } = getState().app;

    if (!query) dispatch(fetching(true));
    let requestMethod = 'post';
    let data = {};
    if (fetchingFromReport) {
      data = {
        search_text: searchString,
        from_date: fromDate,
        to_date: toDate,
        db_param: param
      }
    } else {

      data = {
        search_text: query,
        from_date: frmDt,
        to_date: toDt,
      }
    }

    let endPoint = (shipmentType === 'outgoingShipment') ? 'shipment/download_outgoing_shipment' : 'shipment/download_incoming_shipment';
    // console.log(data, endPoint);
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response);
        dispatch(fetching(false));
        if (response.data.error === 1) {
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          response.data.data.map((item) => {
            item["new_invoice_date"] = '';
            item["new_receive_date"] = '';
            if (item.invoice_date != undefined && item.invoice_date !== "") {
              var spdate = (item.invoice_date.split("T"))[0].split("-");
              item["new_invoice_date"] = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
            }
            if (item.received_date != undefined && item.received_date !== "") {
              var spdate = (item.received_date.split("T"))[0].split("-");
              item["new_receive_date"] = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
            }
          })
          let respFilterByDate = filterByDate(response.data, 'shipment');
          dispatch(setTotalCountOutShip(response.data.total_recordcount));
          let totalPage = getPages(response.data.total_recordcount, pageSize);
          dispatch(setTotalPages(totalPage));
          dispatch(setTotalRecordCount(response.data.total_recordcount));
          (shipmentType === 'outgoingShipment') ? dispatch(outgoingDownloadShipmentSuccess(respFilterByDate.data)) : dispatch(incomingDownloadShipmentSuccess(respFilterByDate.data));
          // console.log('***********fetching end***********')
          resolve(true);

        }
      }).catch(error => {
        console.log(error);
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'No shipment found.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const openShipmentForm = (status) => {
  return (dispatch) => {
    dispatch(setFormState(status));
  }
}

export const setProps = (status, key) => {
  return (dispatch) => {
    switch (key) {
      case 'isEdit':
        dispatch(setEditState(status));
        break;
      case 'isSave':
        dispatch(setSaveState(status));
        break;
      case 'isAddProduct':
        dispatch(setAddProductState(status));
        break;
      case 'isReviewShipment':
        dispatch(setReviewState(status));
        break;
      case 'isProductEdit':
        dispatch(setProductEditState(status));
        break;
      case 'isAddLot':
        dispatch(setLotEditState(status));
        break;
      case 'shouldProductFormClose':
        dispatch(shouldProductFormClose(status));
        break;
      case 'isLotEdit':
        dispatch(setIsLotEdit(status));
        break;
      case 'doProductEmpty':
        dispatch(fetchingProductsSuccess(status));
        break;
      case 'isUpload':
        dispatch(setUploadStatus(status));
        break;
      case 'isDownload':
        dispatch(setDownloadStatus(status));
        break;
      case 'isDownloadCatalogue':
        dispatch(setDownloadCatalogueStatus(status));
        break;
      case 'isConfirm':
        dispatch(showConfirmMessage(status, ''));
        break;
      case 'confirmationCallFor':
        dispatch(setConfirmationCallFor(status));
        break;
      case 'isDecline':
        dispatch(setDeclineState(status));
        break;
      case 'isReceive':
        dispatch(setReceiveState(status));
        break;
      case 'isPermission':
        dispatch(setPermissionState(status));
        break;
      case 'withAlert':
        dispatch(setWithAlertState(status));
        break;
      case 'doBuyerEmpty':
        dispatch(fetchingBuyerSuccess(status));
        break;
      case 'showProductAfterLink':
        dispatch(showProductLinkSuccess(status));
        break;
      case 'isCertUpload':
        dispatch(isCertUploadStatus(status));
        break;
      case 'isAutoConfirm':
        dispatch(showAutoConfirmMessage(status, ''));
        break;
      case 'isExport':
        dispatch(setExportState(status));
        break;  
    }
  }
}

export const resetProps = () => {
  return (dispatch) => {
    dispatch(resetAlreadySetProps());
  }
}

export const resetProducts = () => {
  return (dispatch) => {
    dispatch(resetAlreadySetProducts());
  }
}

export const fetchBuyer = (company_type_id) => {
  return (dispatch) => {
    let requestMethod = 'get';
    let endPoint = 'masters/get_buyer_companies';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        params: {
          company_type_id: company_type_id
        },
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetchingBuyerSuccess(response.data.data));
          resolve(true);

        } else if (response.data.error === 1) {
          dispatch(setAlertMeassage(true, 'No shipment found.', false));
          resolve(true);
        }
      }).catch(error => {
        console.log(error)
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const fetchBuyerUnit = (company_key_id = '') => {
  return (dispatch) => {
    let requestMethod = 'get';
    let endPoint = 'masters/get_company_bu';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        params: {
          buyer_company_key_id: company_key_id
        },
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetchingBuyerUnit(response.data));
          resolve(true);
        } else if (response.data.error === 1) {
          dispatch(setAlertMeassage(true, response.data.data, false));
          resolve(true);
        }
      }).catch(error => {
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const fetchSellerUnit = () => {
  return (dispatch) => {
    let requestMethod = 'get';
    let endPoint = 'masters/get_company_bu';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetchingSellerUnit(response.data));
          resolve(true);
        } else if (response.data.error === 1) {
          dispatch(setAlertMeassage(true, response.data.data, false));
          resolve(true);
        }
      }).catch(error => {
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const fetchProducts = (queryString = '') => {
  return (dispatch) => {
    axios({
      method: 'get',
      url: `${Config.url}masters/get_shipment_products`,
      headers: { 'token': getToken() }
    }).then(response => {
      if (response.data.error === 1) {
        dispatch(setAlertMeassage(true, response.data.data, false));
      } else if (response.data.error === 5) {
        dispatch(setAlertMeassage(true, response.data.data, false));
        browserHistory.push(Url.LOGIN_PAGE);
      } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
        dispatch(fetchingProductsSuccess(response.data));
      }
    }).catch(error => {
      dispatch(errorFetching(true));
      browserHistory.push(Url.ERRORS_PAGE);
    });
  }
}
export const fetchProductsExt = (company_key_id) => {
  return (dispatch) => {

    axios({
      method: 'get',
      url: `${Config.url}masters/get_ext_products`,
      params: {
        company_key_id: company_key_id
      },
      headers: { 'token': getToken() }
    }).then(response => {
      console.log(response)
      if (response.data.error === 1) {
        dispatch(setAlertMeassage(true, response.data.data, false));
      } else if (response.data.error === 5) {
        dispatch(setAlertMeassage(true, response.data.data, false));
        browserHistory.push(Url.LOGIN_PAGE);
      } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
        dispatch(fetchingProductsExt(response.data));
      }
    }).catch(error => {
      dispatch(errorFetching(true));
      browserHistory.push(Url.ERRORS_PAGE);
    });
  }
}

export const viewCompanyDetail = (companyId) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let requestMethod = 'get';
    let endPoint = 'admin/company_detail';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        params: {
          company_key_id: companyId
        },
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          //dispatch(fetching(false));
          if (response.data.data === "Sql error") {
            resolve(true)
          }
          else {
            dispatch(setAlertMeassage(true, response.data.data, false));
          }

        } else if (response.data.error === 5) {
          //dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          //dispatch(setCompanyFormState(false));
          //if (response.data.data.roles !== undefined) dispatch(setRoleCheckboxState(true));
          // let lStorage = {
          //   company_name: response.data.data.company_name,
          //   company_type_id: response.data.data.company_type_id,
          //   company_key_id: response.data.data.company_key_id,
          //   is_external: response.data.data.is_external
          // }

          dispatch(companyExtDetail(response.data))
          // saveLocalStorage('companyDetail', lStorage);
          //dispatch(setCompanyDetail(response.data.data));
        }
        resolve(true);
      }).catch(error => {
        console.log(error)
        //dispatch(fetching(false));
        //dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'There is no company type.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const fetchCompanyRole = (company) => {
  return (dispatch) => {
    let requestMethod = 'get';
    let endPoint = 'admin/company_role';
    if (company === 'Integrated Player') {
      return new Promise((resolve, reject) => {
        axios({
          method: requestMethod,
          url: Config.url + endPoint,
          headers: { 'token': getToken() }
        }).then(response => {
          if (response.data.error === 1) {
            //dispatch(fetching(false));
            dispatch(setAlertMeassage(true, response.data.data, false));
          } else if (response.data.error === 5) {
            //dispatch(fetching(false));
            dispatch(setAlertMeassage(true, response.data.data, false));
            browserHistory.push(Url.LOGIN_PAGE);
          } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
            //dispatch(fetching(false));
            dispatch(companyRoleSuccess(response.data.data));
            //dispatch(setRoleCheckboxState(true));
          } else if (response.data.error === 1) {
            //dispatch(fetching(false));
            dispatch(setAlertMeassage(true, response.data.data, false));
          }
          resolve(true);
        }).catch(error => {
          console.log(error);
          //dispatch(fetching(false));
          //dispatch(errorFetching(true));
          // dispatch(setAlertMeassage(true, 'Role can not be fetched.', false));
          //browserHistory.push(Url.ERRORS_PAGE);
          reject(true);
        })
      });
    } else {
      //dispatch(setRoleCheckboxState(false));
    }
  }
}

export const createShipments = (values, isEdit) => {
  return (dispatch) => {
    console.log(values);
    dispatch(fetching(true));
    dispatch(errorFetching(false));
    if (values.seller_company_bu_id === "Select Seller Unit") {
      values.seller_company_bu_id = ''
    }
    if (values.buyer_company_bu_id === "Select Buyer Unit") {
      values.buyer_company_bu_id = ''
    }
    if (values.shipment_description === 'Select Transport Mode') {
      values.shipment_description = ''
    }
    let data = {
      invoice_number: values.invoice_number,
      invoice_date: values.invoice_date,
      shipment_transport_mode: values.shipment_transport_mode,
      shipment_description: values.shipment_description,
      buyer_company_key_id: values.buyer_company_key_id,
      buyer_company_bu_id: values.buyer_company_bu_id,
      seller_company_bu_id: values.seller_company_bu_id,
      shipment_id: values.shipment_id,
      linked_forest_key_id: values.linked_forest_key_id
    }
    let requestMethod = 'post';
    let endPoint = 'shipment/create_shipment';
    endPoint = (isEdit === true) ? 'shipment/update_shipment' : 'shipment/create_shipment';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response)
        if (response.data.error === 1) {
          dispatch(fetching(false));
          // Invoice number already exists
          dispatch(invoiceCheck(response.data.data))
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(createShipmentSuccess(response.data));
          if (typeof response.data.data !== 'undefined' && typeof response.data.data.shipment_id !== 'undefined') {
            let shipments = {
              shipment_id: response.data.data.shipment_id
            }
            saveLocalStorage('shipments', shipments);
            dispatch(setSaveState(true));
            dispatch(setEditState(true));
            // dispatch(setAlertMeassage(true, 'Shipment created successfully', true));
          } else if (typeof response.data.msg !== 'undefined') {
            // dispatch(setAlertMeassage(true, response.data.msg, false));
          } else if (typeof response.data.data !== 'undefined') {
            // dispatch(setAlertMeassage(true, response.data.data, true));
          }
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(errorFetching(true));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const updateReceiveShipmentQty = (shipment_product_id, product_receiveqty) => {
  const values = {
    shipment_product_id: shipment_product_id,
    product_receiveqty: product_receiveqty
  }
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/update_shipment_product_receive_qty'
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: values,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(receiveShipmentQtySuccess(response.data));
        } else if (response.data.error === 1) {
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const addProducts = (values, isProductEdit = "") => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = (isProductEdit) ? 'shipment/update_shipment_product' : 'shipment/add_shipment_product';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: values,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(AddingProductSuccess(response.data));
        } else if (response.data.error === 1) {
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const viewShipment = (id, key) => {
  return (dispatch) => {
    dispatch(fetching(true));
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: `${Config.url}shipment/view_shipment`,
        params: {
          shipment_id: id
        },
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          if (response.data.shipmentDetail.invoice_date != undefined) {
            var spdate = (response.data.shipmentDetail.invoice_date.split("T"))[0].split("-");
            response.data.shipmentDetail["new_invoice_date"] = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
          }
          if (response.data.shipmentDetail.order_expected_date != undefined) {
            var spdate = (response.data.shipmentDetail.order_expected_date.split("T"))[0].split("-");
            response.data.shipmentDetail["new_order_expected_date"] = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
          }
          dispatch(fetching(false));
          dispatch(viewShipmentSuccess(response.data));
          let shipments = {
            shipment_id: response.data.shipmentDetail.shipment_id,
            asset_id: response.data.shipmentDetail.asset_id,
            buyer_company_key_id: response.data.shipmentDetail.buyer_company_key_id,
            invoice_number: response.data.shipmentDetail.invoice_number,
          }
          saveLocalStorage('shipments', shipments);
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const receiveShipment = (id) => {
  return (dispatch) => {
    dispatch(fetching(true));
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: `${Config.url}shipment/receive_shipment`,
        data: {
          shipment_id: id
        },
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const placeShipment = (id, asset_id, withAlert, buyer_company_key_id, invoice_number) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let data = {
      shipment_id: id,
      asset_id: asset_id,
      with_alert: withAlert,
      recv_company_key_id: buyer_company_key_id,
      invoice_number: invoice_number
    }
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: `${Config.url}shipment/place_shipment`,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          if (response.data.data === "Please contact admin.Blockchain server is down.") {
            resolve(true)
          }
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          let placedData = {
            data: response.data.data,
            isPlaced: true
          }
          saveLocalStorage('placedDataShipment', placedData);
          dispatch(fetching(false));
          dispatch(shipmentPlacedSuccess(response.data));
          dispatch(setAlertMeassage(true, response.data.data, true));
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const deleteShipment = (id, key) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let data = (key === 'review') ? { shipment_product_id: id } : { shipment_id: id };
    let endPoint = (key === 'review') ? 'shipment/delete_shipment_product' : 'shipment/delete_shipment';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(shipmentDeletedSuccess(response.data));
          dispatch(setAlertMeassage(true, response.data.data, true));
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const cancelShipment = (shipmentDetail, openDeclineModalFor) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = (openDeclineModalFor === 'incomingShipment') ? 'shipment/reject_shipment': 'shipment/cancel_shipment' ;
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: shipmentDetail,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, true));
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const getIncomingOrders = () => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'orders/incoming_orders';
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: Config.url + endPoint,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          let respFilterByDate = filterByDate(response);
          dispatch(fetching(false));
          dispatch(incomingReceivedSuccess(respFilterByDate.data));
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const getBuyerIncomingOrders = (buyerCompanyKeyId, currentPage = 1, pageSize = 5, fromDt = '', toDt = '') => {
  return (dispatch) => {
    dispatch(fetching(true));
    let data = {
      buyer_company_key_id: buyerCompanyKeyId,
      page_number: currentPage,
      page_size: pageSize,
      from_date: fromDt,
      to_date: toDt
    }
    let endPoint = 'orders/incoming_buyer_order';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          response.data.data.map((item) => {
            item["new_order_date"] = '';
            if (item.order_date != undefined) {
              var spdate = (item.order_date.split("T"))[0].split("-");
              item["new_order_date"] = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
            }
          })
          let respFilterByDate = filterByDate(response.data);
          dispatch(fetching(false));
          let totalPage = getPages(response.data.total_recordcount, pageSize);
          dispatch(setTotalRecordCount(response.data.total_recordcount));
          dispatch(setTotalPages(totalPage));
          dispatch(receivedBuyerIncomingOrders(respFilterByDate.data));
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const linkShipment = (linkShipmentData, isLinked) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = (isLinked === 1) ? 'shipment/unlink_shipment' : 'shipment/link_shipment';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: linkShipmentData,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(linkingShipmentSuccess(response.data));
          if (typeof response.data.data !== 'undefined') {
            // dispatch(setAlertMeassage(true, response.data.data, true));
          }
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const viewStocks = (currentPage = 1, pageSize = 5, query = '') => {
  return (dispatch) => {
    dispatch(fetching(true));
    let data = {
      search_text: query,
      page_size: pageSize,
      page_number: currentPage
    }
    let requestMethod = 'post';
    let endPoint = 'shipment/view_stocks';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          let totalPage = getPages(response.data.total_recordcount, pageSize);
          dispatch(setTotalRecordCount(response.data.total_recordcount));
          dispatch(setTotalPages(totalPage));
          dispatch(viewStockSuccess(response.data.data));
          resolve(true);
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(viewStockSuccess([]));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const addLotToProduct = (stockData) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/add_shipment_product/lots';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: stockData,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(setLotEditState(false)); //setting isAddLot false to close stocklot popup if stockLot added successfully
          dispatch(shouldProductFormClose(true)); //setting shouldProductFormClose true to go back when to create shipment form when user click add button from AddProduct screen. it simply means lot added successfully and now when user will clicks to add button again user must come back to createshipment form
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const viewShipmentProductDetail = (shipment_product_id) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/shipment_product_lot';
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: Config.url + endPoint,
        params: {
          shipment_product_id: shipment_product_id
        },
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(lotDetailSuccess(response.data.data));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const deleProductLot = (shipment_product_lot_id) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/delete_shipment_product/lot';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: {
          shipment_product_lot_id: shipment_product_lot_id
        },
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(lotDeletedSuccess(response.data.data));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const updateLot = (updateLotDetail) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/update_shipment_product/lot';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: updateLotDetail,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          // dispatch(setAlertMeassage(true, response.data.data, true));
          dispatch(setIsLotEdit(false));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const compareQty = (orderId, shipmentId) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let data = {
      order_id: orderId,
      shipment_id: shipmentId
    };
    let endPoint = 'shipment/validate_shipment_qty';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          if (response.data.data.b_error) {
            dispatch(showConfirmMessage(true, response.data.data.b_message));
            dispatch(setProductQuantities(response.data.data.consume_productqty, response.data.data.expected_consume_product_qty, response.data.data.shipment_product_qty))
          }
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const generateAutoOrder = (shipmentId, userId, orderNumber) => {
  return (dispatch) => {
    dispatch(fetching(true));
    console.log(orderNumber)
    let data = {
      shipment_id: shipmentId,
      user_id: userId,
      order_number: orderNumber
    };
    let endPoint = 'shipment/auto_order';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          (response.data.error == 0) ? dispatch(autoOrderSuccess(true)) : dispatch(autoOrderSuccess(false));
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const pulpshipment = (shipmentId, forestId) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let data = {
      shipment_id: shipmentId,
      linked_forest_key_id: forestId
    };
    let endPoint = 'shipment/generate_forest_shipment';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));

          dispatch(pulpshipmentPlacedSuccess(response.data));
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const checkForBlankLotNo = (shipmentId) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/check_shipment_lot';
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: Config.url + endPoint,
        params: {
          shipment_id: shipmentId
        },
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(checkLotStatusSuccess(response.data.data.blank_lot));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const fetchShipmentLotDetail = (shipmentId) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/shipment_lot';
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: Config.url + endPoint,
        params: {
          shipment_id: shipmentId
        },
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(lotDetailSuccess(response.data.data));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const validateStock = (shipmentId) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/validate_stock';
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: Config.url + endPoint,
        params: {
          shipment_id: shipmentId
        },
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response);
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(validateStockSuccess(response.data.data['@b_error']));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const getLotDetail = (consumeStockLotId) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/stocklot_by_id';
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: Config.url + endPoint,
        params: {
          consume_stock_lot_id: consumeStockLotId
        },
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(setStockLotDetail(response.data.data));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}
export const qaDeleteDoc = (shipment_documentid, document_file_name) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/delete_document';
    let data = {
      shipment_documentid: shipment_documentid,
      document_file_name: document_file_name
    };
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response)
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, true));
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const qaDownloadDoc = (document_file_name, document_name) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/download_document';
    let data = {
      document_file_name: document_file_name
    };
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() },
        responseType: 'blob',
      }).then(response => {
        console.log(response)
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          link.href = url;
          link.setAttribute('download', document_name);
          document.body.appendChild(link);
          link.click();

          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}


export const createExternalJob = (shipment_id, invoice_number, invoice_date, order_number, shipment_transport_mode, shipment_description, seller_company_key_id, shipment_date) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/create_ext_vendor_shipment';
    let data = {
      shipment_id: shipment_id,
      invoice_number: invoice_number,
      invoice_date: invoice_date,
      order_number: order_number,
      shipment_transport_mode: shipment_transport_mode,
      shipment_description: shipment_description,
      seller_company_key_id: seller_company_key_id,
      shipment_date: shipment_date
    };
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response)
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(createExternalSuccess(response.data));
          // if (typeof response.data.data !== 'undefined' && typeof response.data.data.shipment_id !== 'undefined') {
          //   let shipments = {
          //     shipment_id: response.data.data.shipment_id
          //   }
          //   saveLocalStorage('shipments', shipments);
          //   dispatch(setSaveState(true));
          //   dispatch(setEditState(true));
          // dispatch(setAlertMeassage(true, 'Shipment created successfully', true));
        } else if (typeof response.data.msg !== 'undefined') {
          // dispatch(setAlertMeassage(true, response.data.msg, false));
        } else if (typeof response.data.data !== 'undefined') {
          // dispatch(setAlertMeassage(true, response.data.data, true));
        }
        else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(errorFetching(true));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);

      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const addExternalProduct = (ext_vendor_shipment_id, shipment_id, product_id, ext_product_description, ext_product_qty, ext_product_uom, ext_blend_percentage) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/add_ext_vendor_shipment_product';
    let data = {
      ext_vendor_shipment_id: ext_vendor_shipment_id,
      shipment_id: shipment_id,
      product_id: product_id,
      ext_product_description: ext_product_description,
      ext_product_qty: ext_product_qty,
      ext_product_uom: ext_product_uom,
      ext_blend_percentage: ext_blend_percentage
    };
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response)
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          //dispatch(createExternalSuccess(response.data));
          // if (typeof response.data.data !== 'undefined' && typeof response.data.data.shipment_id !== 'undefined') {
          //   let shipments = {
          //     shipment_id: response.data.data.shipment_id
          //   }
          //   saveLocalStorage('shipments', shipments);
          //   dispatch(setSaveState(true));
          //   dispatch(setEditState(true));
          // dispatch(setAlertMeassage(true, 'Shipment created successfully', true));
        } else if (typeof response.data.msg !== 'undefined') {
          // dispatch(setAlertMeassage(true, response.data.msg, false));
        } else if (typeof response.data.data !== 'undefined') {
          // dispatch(setAlertMeassage(true, response.data.data, true));
        }
        else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(errorFetching(true));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);

      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const viewExternalProduct = (ext_vendor_shipment_id) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/view_external_shipment_product'
    let data = {
      ext_vendor_shipment_id: ext_vendor_shipment_id
    }
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response)
        if (response.data.error === 1) {
          dispatch(fetching(false));


          dispatch(setAlertMeassage(true, response.data.data, false));


        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          // if (response.data.shipmentDetail.invoice_date != undefined) {
          //   var spdate = (response.data.shipmentDetail.invoice_date.split("T"))[0].split("-");
          //   response.data.shipmentDetail["new_invoice_date"] = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
          // }
          // if (response.data.shipmentDetail.order_expected_date != undefined) {
          //   var spdate = (response.data.shipmentDetail.order_expected_date.split("T"))[0].split("-");
          //   response.data.shipmentDetail["new_order_expected_date"] = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
          // }
          dispatch(fetching(false));
          dispatch(viewProductExternalSuccess(response.data));
          // let shipments = {
          //   shipment_id: response.data.shipmentDetail.shipment_id,
          //   asset_id: response.data.shipmentDetail.asset_id,
          //   buyer_company_key_id: response.data.shipmentDetail.buyer_company_key_id,
          //   invoice_number: response.data.shipmentDetail.invoice_number,
          // }
          // saveLocalStorage('shipments', shipments);
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const onDeleteExtProd = (ext_vendor_shipment_product_id) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/delete_external_shipment_product';
    let data = {
      ext_vendor_shipment_product_id: ext_vendor_shipment_product_id
    }
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          // dispatch(shipmentDeletedSuccess(response.data));
          //dispatch(setAlertMeassage(true, response.data.data, true));
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}


export const onUpdateExtProd = (ext_vendor_shipment_product_id, ext_vendor_shipment_id, product_id, ext_product_description, ext_product_qty, ext_product_uom, ext_blend_percentage) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/update_ext_vendor_shipment_product';
    let data = {
      ext_vendor_shipment_product_id: ext_vendor_shipment_product_id,
      ext_vendor_shipment_id: ext_vendor_shipment_id,
      product_id: product_id,
      ext_product_description: ext_product_description,
      ext_product_qty: ext_product_qty,
      ext_product_uom: ext_product_uom,
      ext_blend_percentage: ext_blend_percentage
    }
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response)
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          // dispatch(shipmentDeletedSuccess(response.data));
          dispatch(setAlertMeassage(true, response.data.data, true));
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const viewExternalShipment = (shipment_id) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/view_external_shipment';
    let data = {
      shipment_id: shipment_id
    }
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response)
        // dispatch(fetching(false))
        if (response.data.error === 1) {
          if (response.data.data === "Sql error") {
            dispatch(fetching(false));
            resolve(true)
          }
          else {
            dispatch(fetching(false));
            dispatch(setAlertMeassage(true, response.data.data, false));
          }

        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(viewExternalDetail(response.data));
          //dispatch(setAlertMeassage(true, response.data.data, true));
        }
        resolve(true);

      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const viewUniqueExternalShipment = (ext_vendor_shipment_id) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/view_external_vendor_by_shipment_id';
    let data = {
      ext_vendor_shipment_id: ext_vendor_shipment_id
    }
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response)
        // dispatch(fetching(false))
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(viewUniqueExternalDetail(response.data));
          //dispatch(setAlertMeassage(true, response.data.data, true));
        }
        resolve(true);

      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const onUpdateExtVendor = (ext_vendor_shipment_id, shipment_id, invoice_number, invoice_date, order_number, shipment_transport_mode, shipment_description, seller_company_key_id, shipment_date) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/update_ext_vendor_shipment';
    let data = {
      ext_vendor_shipment_id: ext_vendor_shipment_id,
      shipment_id: shipment_id,
      invoice_number: invoice_number,
      invoice_date: invoice_date,
      order_number: order_number,
      shipment_transport_mode: shipment_transport_mode,
      shipment_description: shipment_description,
      seller_company_key_id: seller_company_key_id,
      shipment_date: shipment_date
    }
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response)
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          // dispatch(shipmentDeletedSuccess(response.data));
          dispatch(setAlertMeassage(true, response.data.data, true));
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}
export const onDeleteVendor = (ext_vendor_shipment_id, shipment_id) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/delete_external_shipment';
    let data = {
      ext_vendor_shipment_id: ext_vendor_shipment_id,
      shipment_id: shipment_id
    }
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          // dispatch(shipmentDeletedSuccess(response.data));
          //dispatch(setAlertMeassage(true, response.data.data, true));
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const viewUniqueExternalProduct = (ext_vendor_shipment_product_id) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/view_external_shipment_product_by_product_id';
    let data = {
      ext_vendor_shipment_product_id: ext_vendor_shipment_product_id
    }
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response)
        // dispatch(fetching(false))
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(viewUniqueProductDetail(response.data));
          //dispatch(setAlertMeassage(true, response.data.data, true));
        }
        resolve(true);

      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const createExternalCompany = (company_name) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/create_external_vendor_company';
    let data = {
      company_name: company_name
    }
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response)
        //dispatch(fetching(false))
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(setCompanyExternalVendorId(response.data));
          //dispatch(setAlertMeassage(true, response.data.data, true));
        }
        resolve(true);

      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}



export const getCertificateNumber = (shipment_id) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/certificate_number';
    let data = {
      shipment_id: shipment_id
    }
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response)
        //dispatch(fetching(false))
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(certificateNumberVal(response.data));
          //dispatch(setAlertMeassage(true, response.data.data, true));
        }
        resolve(true);

      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}


export const getFibreData = (shipment_id) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/fibre_detail';
    let data = {
      shipment_id: shipment_id
    }
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response)
        // dispatch(fetching(false))
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(fibreDataVal(response.data));
          //dispatch(setAlertMeassage(true, response.data.data, true));
        }
        resolve(true);

      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}



export const compareAutoQty = (shipmentId) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let data = {
      shipment_id: shipmentId
    };
    let endPoint = 'shipment/auto_order_validate_shipment_qty';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response)
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          if (response.data.data.b_error) {
            dispatch(showAutoConfirmMessage(true, response.data.data.b_message));
            dispatch(setAutoProductQuantities(response.data.data.consume_productqty, response.data.data.shipment_product_qty))
          }
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}
export const downloadShipmentProduct = () => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'masters/get_shipment_products_catalogue';
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: Config.url + endPoint,
        headers: { 'token': getToken() },
        responseType: 'blob',
      }).then(response => {
        console.log(response)
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          //browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          link.href = url;
          
          link.setAttribute('download', 'shipment_product_catalogue.xlsx');
          document.body.appendChild(link);
          link.click();
  
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}
export const downloadShipmentCompany = () => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'masters/get_buyer_companies_catalogue';
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: Config.url + endPoint,
        headers: { 'token': getToken() },
        responseType: 'blob',
      }).then(response => {
        console.log(response)
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          //browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          link.href = url;
          
          link.setAttribute('download', 'shipment_company_catalogue.xlsx');
          document.body.appendChild(link);
          link.click();
  
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const getPulpCompany = () => {
  return (dispatch) => {
    dispatch(fetching(true));
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: `${Config.url}users/get_company`,
        params: {
          company_type_id: 11
        },
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response);
        dispatch(fetching(false));
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(pulpCompany(response.data))
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'There is no company type.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const uploadExportRebateDocument = (file,shipment_id, appendFile,is_bill) =>{
  return (dispatch) => {
    dispatch(fetching(true));
    // console.log('file',file);
    // console.log('shipment_id',shipment_id)
    // console.log('appendFile',appendFile)
    const data = new FormData();
    data.append('upfile', file);
    data.append('shipmentid', shipment_id);
    data.append('document_filename', appendFile)
    data.append('is_bill',is_bill)
    let endPoint = 'shipment/upload_export_rebate_documents';
    console.log('checking bill type',is_bill)

    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: {
          'token': getToken(),
          'Content-Type': 'multipart/form-data'
        }
      }).then(response => {
        console.log('response ', response);
        if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          console.log(response.data.data)
          // dispatch(uploadFileSuccess(true, response.data.data));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));

          // dispatch(uploadFileSuccess(false, response.data.data));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
      })
    });
  }
}

export const getExportRebateDocument = (shipmentId) => {
  return (dispatch) => {
    let endPoint = 'shipment/get_export_rebate_documents';

    let data = {
      shipment_id: shipmentId
    };

    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: {
          'token': getToken()
        }
      }).then(response => {
        console.log('response ', response);
        if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          console.log(response.data)
          let billFile = response.data.documents.filter(item=>{
              if(item.is_bill === 1){
                return item
              }
          })
          let invoiceFile = response.data.documents.filter(item=>{
                if(item.is_bill != 1){
                  return item
                }
          })
          dispatch(exportRebateDocument(billFile,invoiceFile))
          // dispatch(uploadFileSuccess(true, response.data.data));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));

          // dispatch(uploadFileSuccess(false, response.data.data));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
      })
    });
  }
}

export const downloadExportRebateDocument = (document_file_name,document_name) => {
  return (dispatch) => {
    let endPoint = 'shipment/download_export_document';

    let data = {
      document_file_name: document_file_name
    };

    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: {
          'token': getToken()
        },
        responseType: 'blob',
      }).then(response => {
        console.log('response ', response);
        if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          link.href = url;
          link.setAttribute('download', document_name);
          document.body.appendChild(link);
          link.click();
          resolve(true);
          // dispatch(uploadFileSuccess(true, response.data.data));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          reject(true)
          // dispatch(uploadFileSuccess(false, response.data.data));
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
      })
    });
  }
}

export const deleteExportRebateDocument = (export_rebate_id,document_file_name) =>{
  return (dispatch) => {
    let endPoint = 'shipment/delete_export_rebate_document';

    let data = {
      export_rebate_id: export_rebate_id,
      document_file_name:document_file_name
    };

    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: {
          'token': getToken()
        },
      }).then(response => {
        if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));

          resolve(true);
          // dispatch(uploadFileSuccess(true, response.data.data));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          reject(true)
          // dispatch(uploadFileSuccess(false, response.data.data));
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
      })
    });
  }
}

export const submitExportRebateDocument = (shipment_id,billData) => {
  return (dispatch) => {
    let endPoint = 'shipment/submit_export_rebate_documents';

    let data = {
      shipment_id: shipment_id,
      ...billData
    };

    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: {
          'token': getToken()
        },
      }).then(response => {
        if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, true));
          resolve(true);
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, true));
          reject(true)
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        dispatch(setAlertMeassage(true, response.data.data, true));
      })
    });
  }
}




export const setCreateFormState = () => {
  return (dispatch) => {
    dispatch(resetFormState());
  }
}

export const resetBill = () => {
  return (dispatch) => {
    dispatch(resetBillFile());
  }
}

export const actions = {
  fetchShipment,
  openShipmentForm,
  setProps,
  fetchBuyer,
  fetchBuyerUnit,
  fetchSellerUnit,
  createShipments,
  getIncomingOrders,
  linkShipment,
  setCreateFormState,
  viewStocks,
  addLotToProduct,
  viewShipmentProductDetail,
  deleProductLot,
  checkForBlankLotNo,
  uploadExportRebateDocument,
  getExportRebateDocument,
  downloadExportRebateDocument,
  deleteExportRebateDocument,
  submitExportRebateDocument,
  resetBill
};

// ------------------------------------
// Action handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_ALERT_MESSAGE]: (state, action) => {
    return {
      ...state,
      showAlert: action.showAlert,
      alertMessage: action.alertMessage,
      status: action.status,
    }
  },
  [SHOW_CONFIRM_MESSAGE]: (state, action) => {
    return {
      ...state,
      isConfirm: action.isConfirm,
      confirmMsg: action.confirmMsg,
    }
  },
  [FETCHING]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching
    }
  },
  [ERROR_FETCHING]: (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
  [INCOMING_SHIPMENT_SUCCESS]: (state, action) => {
    return {
      ...state,
      incomingShipment: action.incomingShipment
    }
  },
  [OUTGOING_SHIPMENT_SUCCESS]: (state, action) => {
    return {
      ...state,
      outgoingShipment: action.outgoingShipment
    }
  },
  [SET_FORM_STATE]: (state, action) => {
    return {
      ...state,
      isShipmentFormOpen: action.isShipmentFormOpen
    }
  },
  [FETCHING_BUYER_SUCCESS]: (state, action) => {
    return {
      ...state,
      buyerCompniesList: action.buyerCompniesList
    }
  },
  [SHOW_PRODUCT]: (state, action) => {
    return {
      ...state,
      showProductAfterLink: action.showProductAfterLink
    }
  },
  [FETCHING_BUYER_UNIT]: (state, action) => {
    return {
      ...state,
      buyerUnitList: action.buyerUnitList
    }
  },
  [FETCHING_SELLER_UNIT]: (state, action) => {
    return {
      ...state,
      sellerUnitList: action.sellerUnitList
    }
  },
  [CREATE_SHIPMENT_SUCCESS]: (state, action) => {
    return {
      ...state,
      shipmentDetail: action.shipmentDetail
    }
  },
  [SET_EDIT_STATE]: (state, action) => {
    return {
      ...state,
      isEdit: action.isEdit
    }
  },
  [SET_SAVE_STATE]: (state, action) => {
    return {
      ...state,
      isSave: action.isSave
    }
  },
  [SET_ADD_PRODUCT_STATE]: (state, action) => {
    return {
      ...state,
      isAddProduct: action.isAddProduct
    }
  },
  [FETCHING_PRODUCTS_SUCCESS]: (state, action) => {
    return {
      ...state,
      products: action.products
    }
  },
  [ADDING_PRODUCTS_SUCCESS]: (state, action) => {
    return {
      ...state,
      addedProductDetail: action.addedProductDetail
    }
  },
  [VIEW_SHIPMENT_SUCCESS]: (state, action) => {
    return {
      ...state,
      viewShipmentDetail: action.viewShipmentDetail
    }
  },
  [SET_REVIEW_STATE]: (state, action) => {
    return {
      ...state,
      isReviewShipment: action.isReviewShipment
    }
  },
  [SHIPMENT_PLACED_SUCCESS]: (state, action) => {
    return {
      ...state,
      shipmentPlacedData: action.shipmentPlacedData
    }
  },
  [PULP_SHIPMENT_PLACED_SUCCESS]: (state, action) => {
    return {
      ...state,
      pulpshipmentPlacedData: action.pulpshipmentPlacedData
    }
  },
  [AUTO_ORDER_SUCCESS]: (state, action) => {
    return {
      ...state,
      autoOrderSuccess: action.autoOrderSuccess
    }
  },
  [RESET_PROPS]: (state, action) => {
    return {
      ...state,
      viewShipmentDetail: action.viewShipmentDetail
    }
  },
  [SET_PRODUCT_EDIT_STATE]: (state, action) => {
    return {
      ...state,
      isProductEdit: action.isProductEdit
    }
  },
  [RESTE_ALREADY_SET_PRODUCTS]: (state, action) => {
    return {
      ...state,
      products: action.products,
      viewLotDetail: action.viewLotDetail
    }
  },
  [SHIPMENT_DELETING_SUCCESS]: (state, action) => {
    return {
      ...state,
      shipmentDeletedData: action.shipmentDeletedData
    }
  },
  [INCOMING_RECEIVED_SUCCESS]: (state, action) => {
    return {
      ...state,
      incomingOrders: action.incomingOrders
    }
  },
  [LINKING_SHIPMENT_SUCCESS]: (state, action) => {
    return {
      ...state,
      linkedShipmentDetail: action.linkedShipmentDetail
    }
  },
  [SET_LOT_EDIT_STATE]: (state, action) => {
    return {
      ...state,
      isAddLot: action.isAddLot
    }
  },
  [SHOULD_PRODUCT_FORM_CLOSE]: (state, action) => {
    return {
      ...state,
      shouldProductFormClose: action.shouldProductFormClose
    }
  },
  [RESET_FORM_STATE]: (state, action) => {
    return {
      ...state,
      buyerCompniesList: action.buyerCompniesList,
      sellerUnitList: action.sellerUnitList,
      buyerUnitList: action.buyerUnitList,
    }
  },
  [VIEW_STOCK_SUCCESS]: (state, action) => {
    return {
      ...state,
      stockLot: action.stockLot,
    }
  },
  [VIEW_LOT_DETAIL]: (state, action) => {
    return {
      ...state,
      viewLotDetail: action.viewLotDetail,
    }
  },
  [LOT_DELETED_SUCCESS]: (state, action) => {
    return {
      ...state,
      deletedLotDetail: action.deletedLotDetail,
    }
  },
  [SET_IS_LOT_EDIT]: (state, action) => {
    return {
      ...state,
      isLotEdit: action.isLotEdit,
    }
  },
  [SET_UPLOAD_STATUS]: (state, action) => {
    return {
      ...state,
      isUpload: action.isUpload,
    }
  },
  [SET_DOWNLOAD_STATUS]: (state, action) => {
    return {
      ...state,
      isDownload: action.isDownload,
    }
  },
  [SET_DOWNLOAD_CATALOGUE_STATUS]: (state, action) => {
    return {
      ...state,
      isDownloadCatalogue: action.isDownloadCatalogue,
    }
  },
  [SET_CONFIRMATION_CALL_FOR]: (state, action) => {
    return {
      ...state,
      confirmationCallFor: action.confirmationCallFor,
    }
  },
  [RECEIVED_INCOMING_BUYER_ORDER]: (state, action) => {
    return {
      ...state,
      buyerIncomingOrders: action.buyerIncomingOrders,
    }
  },
  [SET_DECLINE_STATE]: (state, action) => {
    return {
      ...state,
      isDecline: action.isDecline,
    }
  },
  [SET_EXPORT_STATE]: (state, action) => {
    return{
      ...state,
      isExport: action.isExport,
    }
  },
  [SET_RECEIVE_STATE]: (state, action) => {
    return {
      ...state,
      isReceive: action.isReceive,
    }
  },
  [SET_PRODUCT_QTYS]: (state, action) => {
    return {
      ...state,
      consumedPrdQty: action.consumedPrdQty,
      expectedConsumedQty: action.expectedConsumedQty,
      shipmentPrdQty: action.shipmentPrdQty,
    }
  },
  [CHECK_LOT_STATUS]: (state, action) => {
    return {
      ...state,
      isLotNoBlank: action.isLotNoBlank,
    }
  },
  [SET_TOTAL_RECORD_COUNT]: (state, action) => {
    return {
      ...state,
      totalRecordCount: action.totalRecordCount,
    }
  },
  [SET_TOTAL_COUNT_OUTSHIP]: (state, action) => {
    return {
      ...state,
      totalCountOutShip: action.totalCountOutShip,
    }
  },
  [SET_TOTAL_PAGES]: (state, action) => {
    return {
      ...state,
      pages: action.pages,
    }
  },
  [VALIDATE_STOCK_SUCCESS]: (state, action) => {
    return {
      ...state,
      isStockValid: action.isStockValid,
    }
  },
  [SET_PERMISSION_STATE]: (state, action) => {
    return {
      ...state,
      isPermission: action.isPermission,
    }
  },
  [SET_WITH_ALERT_STATE]: (state, action) => {
    return {
      ...state,
      withAlert: action.withAlert,
    }
  },
  [SET_STOCK_LOT_DETAIL]: (state, action) => {
    return {
      ...state,
      stockLotDetail: action.stockLotDetail,
    }
  },
  [SET_SHIPQYT_SUCCESS]: (state, action) => {
    return {
      ...state,
      receiveShipSuccess: action.receiveShipSuccess,
    }
  },
  [ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
  [COMPANY_ROLE_SUCCESS]: (state, action) => {
    return {
      ...state,
      companyRole: action.companyRole
    }
  },
  [SET_COMPANY_DETAIL]: (state, action) => {
    return {
      ...state,
      viewCompanyInfo: action.viewCompanyInfo
    }
  },
  [SET_CURRENT_NEW_PAGE]: (state, action) => {
    return {
      ...state,
      currentNewPage: action.currentNewPage
    }
  },
  [SET_CERT_UPLOAD_STATUS]: (state, action) => {
    return {
      ...state,
      isCertUpload: action.isCertUpload
    }
  },
  [SET_PDF_STATUS]: (state, action) => {
    return {
      ...state,
      pdfStatus: action.pdfStatus
    }
  },
  [SET_CERT_LOGIN]: (state, action) => {
    return {
      ...state,
      certLogin: action.certLogin
    }
  },
  [SET_PDF_RECEIVE_STATUS]: (state, action) => {
    return {
      ...state,
      pdfReceiveStatus: action.pdfReceiveStatus
    }

  },
  [EXTERNAL_JOB_STATUS]: (state, action) => {
    return {
      ...state,
      jobStatus: action.jobStatus
    }
  },
  [SET_JOB_ACTIVE]: (state, action) => {
    return {
      ...state,
      isJobActive: action.isJobActive
    }
  },
  [RESET_JOB_ACTIVE]: (state, action) => {
    return {
      ...state,
      isJobActive: action.isJobActive
    }
  },
  [EXTERNAL_JOB_STATUS]: (state, action) => {
    return {
      ...state,
      jobStatus: action.jobStatus
    }
  },
  [SET_PRODUCT_STATUS_DETAIL]: (state, action) => {
    return {
      ...state,
      isProduct: action.isProduct
    }
  },
  [CREATE_EXTERNAL_SUCCESS]: (state, action) => {
    return {
      ...state,
      externalData: action.externalData
    }
  },
  [VIEW_PRODUCT_INTERNAL_SUCCESS]: (state, action) => {
    return {
      ...state,
      productDetail: action.productDetail
    }
  },
  [VIEW_EXTERNAL_DETAIL_VAL]: (state, action) => {
    return {
      ...state,
      externalView: action.externalView
    }
  },
  [VIEW_UNIQUE_EXTERNAL_DETAIL_VAL]: (state, action) => {
    return {
      ...state,
      uniqueExternalView: action.uniqueExternalView
    }
  },
  [RESET_VIEW_UNIQUE_EXTERNAL_SHIPMENT_VAL]: (state, action) => {
    return {
      ...state,
      uniqueExternalView: action.uniqueExternalView
    }
  },
  [RESET_VIEW_EXTERNAL_PRODUCT]: (state, action) => {
    return {
      ...state,
      productDetail: action.productDetail
    }
  },
  [VIEW_UNIQUE_PRODUCT_DETAIL]: (state, action) => {
    return {
      ...state,
      uniqueProductView: action.uniqueProductView
    }
  },
  [RESET_UNIQUE_PRODUCT_DETAIL]: (state, action) => {
    return {
      ...state,
      uniqueProductView: action.uniqueProductView
    }
  },
  [RESET_EXTERNAL_DATA]: (state, action) => {
    return {
      ...state,
      externalData: action.externalData
    }
  },
  [RESET_EXTERNAL_DETAIL_VAL]: (state, action) => {
    return {
      ...state,
      externalView: action.externalView
    }
  },
  [IS_EDIT_TEXT]: (state, action) => {
    return {
      ...state,
      isEditText: action.isEditText
    }
  },
  [COMPANY_NAME_EXT_VAL]: (state, action) => {
    return {
      ...state,
      companyNameExtVal: action.companyNameExtVal
    }
  },
  [SET_COMPANY_EXTERNAL_VENDOR_ID]: (state, action) => {
    return {
      ...state,
      companyExtVendorId: action.companyExtVendorId
    }
  },
  [SET_COMPANY_EXT_DETAIL]: (state, action) => {
    return {
      ...state,
      extVendorCheck: action.extVendorCheck
    }
  },
  [RESET_EXTERNAL_VENDOR_CHECK]: (state, action) => {
    return {
      ...state,
      extVendorCheck: action.extVendorCheck
    }
  },
  [EXT_BUYER_CHECK]: (state, action) => {
    return {
      ...state,
      extBuyerCheckVal: action.extBuyerCheckVal
    }
  },
  [INVOICE_CHECK_VAL]: (state, action) => {
    return {
      ...state,
      invoiceCheckVal: action.invoiceCheckVal
    }
  },
  [RESET_INVOICE_CHECK]: (state, action) => {
    return {
      ...state,
      invoiceCheckVal: action.invoiceCheckVal
    }
  },
  [CHECK_FOR_EXT_BUYER]: (state, action) => {
    return {
      ...state,
      isExtBuyer: action.isExtBuyer
    }
  },
  [IS_EDIT_VENDOR_VAL]: (state, action) => {
    return {
      ...state,
      isEditVendorVal: action.isEditVendorVal
    }
  },
  [FETCH_PRODUCT_EXT]: (state, action) => {
    return {
      ...state,
      productsExt: action.productsExt
    }
  },
  [IS_EXT_NUMBER]: (state, action) => {
    return {
      ...state,
      isExtNumber: action.isExtNumber
    }
  },
  [CERTIFICATE_NUMBER_VAL]: (state, action) => {
    return {
      ...state,
      certificateNumber: action.certificateNumber
    }
  },
  [FIBRE_DATA_VAL]: (state, action) => {
    return {
      ...state,
      fibreData: action.fibreData
    }
  },
  [SHOW_AUTO_CONFIRM_MESSAGE]: (state, action) => {
    return {
      ...state,
      isAutoConfirm: action.isAutoConfirm,
      confirmAutoMsg: action.confirmAutoMsg,
    }
  },
  [SET_AUTO_PRODUCT_QTY]: (state, action) => {
    return{
      ...state,
      consumedAutoQty: action.consumedAutoQty,
      shipmentAutoQty: action.shipmentAutoQty
    }
  },
  [OUTGOING_DOWNLOAD_SHIPMENT_SUCCESS]: (state, action) => {
    return{
      ...state,
      outgoingDownloadShipment: action.outgoingDownloadShipment
    }
  },
  [INCOMING_DOWNLOAD_SHIPMENT_SUCCESS]: (state, action) => {
    return{
      ...state,
      incomingDownloadShipment: action.incomingDownloadShipment
    }
  },
  [SET_CURRENT_LOT_NEW_PAGE]: (state, action) => {
    return{
      ...state,
      currentNewLotPage: action.currentNewLotPage
    }
  },
  [PULP_COMPANY_INFO]: (state, action) => {
    return{
      ...state,
      companyDropPulpInfo: action.companyDropPulpInfo
    }
  },
  [GET_EXPORT_REBATE_DOCUMENT]:(state, action) => {
    return{
      ...state,
      billFile: action.billFile,
      invoiceFile:action.invoiceFile
    }
  },
  [RESET_BILL_FILE]: (state, action) => {
    return{
      ...state,
      billFile: action.billFile,
      invoiceFile:action.invoiceFile,
      isExportModalReset:true
    }
  }
  // [RESET_EXPORT]: (state, action) => {
  //   return{
  //     ...state,
  //     billFile: action.billFile,
  //     invoiceFile:action.invoiceFile
  //   }
  // }
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  fetching: false,
  error: false,
  status: false,
  isConfirm: false,
  confirmMsg: '',
  showAlert: false,
  alertMessage: '',
  incomingShipment: [],
  outgoingShipment: [],
  isShipmentFormOpen: false,
  isAddProduct: false,
  isEdit: false,
  isSave: false,
  buyerCompniesList: [],
  sellerUnitList: [],
  buyerUnitList: [],
  shipmentDetail: [],
  products: [],
  addedProductDetail: [],
  viewShipmentDetail: [],
  isReviewShipment: false,
  shipmentPlacedData: {},
  isProductEdit: false,
  shipmentDeletedData: [],
  incomingOrders: [],
  linkedShipmentDetail: [],
  isAddLot: false,
  shouldProductFormClose: false,
  stockLot: [],
  viewLotDetail: [],
  deletedLotDetail: '',
  isLotEdit: false,
  isUpload: false,
  isDownload: false,
  isDownloadCatalogue: false,
  confirmationCallFor: 'shipFormTable',
  buyerIncomingOrders: [],
  isDecline: false,
  isExport:false,
  consumedPrdQty: '',
  expectedConsumedQty: '',
  shipmentPrdQty: '',
  isLotNoBlank: 0,
  totalRecordCount: 0,
  totalCountOutShip: 0,
  pages: 1,
  isStockValid: false,
  isPermission: false,
  withAlert: false,
  stockLotDetail: {},
  isReceive: false,
  receiveShipSuccess: {},
  showProductAfterLink: false,
  autoOrderSuccess: false,
  pulpshipmentPlacedData: '',
  currentNewPage: 1,
  isCertUpload: false,
  pdfStatus: false,
  certLogin: '',
  pdfReceiveStatus: false,
  isJobActive: '',
  jobStatus: false,
  isProduct: false,
  externalData: '',
  productDetail: '',
  externalView: '',
  uniqueExternalView: '',
  uniqueProductView: '',
  isEditText: false,
  companyNameExtVal: '',
  companyExtVendorId: '',
  extVendorCheck: '',
  extBuyerCheckVal: false,
  invoiceCheckVal: '',
  isExtBuyer: false,
  isEditVendorVal: false,
  productsExt: [],
  isExtNumber: '',
  certificateNumber: '',
  fibreData: '',
  isAutoConfirm: false,
  confirmAutoMsg: '',
  consumedAutoQty: '',
  shipmentAutoQty: '',
  outgoingDownloadShipment: '',
  incomingDownloadShipment: '',
  currentNewLotPage: 1,
  companyDropPulpInfo: '',
  billFile:[],
  invoiceFile:[],
  isExportModalReset:false
};

export default function shipmentsReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
