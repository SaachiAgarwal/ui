/** ShipmentsContainer
 *
 * @description This is container that manages the states for all shipment related functionality.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
import { connect } from "react-redux";

import Shipments from "../components/Shipments";

import {
  setToDate,
  setFromDate,
  setInnerNav,
  setParam,
  setSearchString,
  setReportFetching,
  setGlobalState,
  fetchUOM,
  uploadFile,
  handleUnreadItems,
  uploadCertFile,
  setCurrentLanguage,
  viewQaDetails,
  resetqaDataDetail,
} from "../../../store/app";

import {
  viewCompanyDetail,
  fetchCompanyRole,
  resetAlertBox,
  fetchShipment,
  openShipmentForm,
  fetchBuyer,
  fetchBuyerUnit,
  fetchSellerUnit,
  createShipments,
  setProps,
  fetchProducts,
  addProducts,
  viewShipment,
  receiveShipment,
  placeShipment,
  cancelShipment,
  resetProps,
  resetProducts,
  deleteShipment,
  getIncomingOrders,
  linkShipment,
  setCreateFormState,
  viewStocks,
  addLotToProduct,
  viewShipmentProductDetail,
  deleProductLot,
  updateLot,
  compareQty,
  getBuyerIncomingOrders,
  checkForBlankLotNo,
  fetchShipmentLotDetail,
  validateStock,
  pulpshipment,
  getLotDetail,
  generateAutoOrder,
  updateReceiveShipmentQty,
  setCurrentNewPage,
  setPdfStatus,
  setCertLogin,
  setPdfReceiveStatus,
  qaDeleteDoc,
  qaDownloadDoc,
  setAlertMeassage,
  setJobActive,
  resetJobActive,
  externalJobStatus,
  setProductStatus,
  createExternalJob,
  addExternalProduct,
  viewExternalProduct,
  onDeleteExtProd,
  onUpdateExtProd,
  viewExternalShipment,
  viewUniqueExternalShipment,
  resestViewUniqueExternalShipment,
  onUpdateExtVendor,
  resetViewExternalProduct,
  onDeleteVendor,
  viewUniqueExternalProduct,
  resetUniqueProductDetail,
  resetExternalData,
  resetExternalDetail,
  isEditTextVal,
  createExternalCompany,
  companyNameExt,
  resetExternVendorCheck,
  extBuyerCheck,
  resetInvoiceCheck,
  checkForExtBuyer,
  isEditVendor,
  fetchProductsExt,
  isExtNumberFunc,
  getCertificateNumber,
  getFibreData,
  compareAutoQty,
  fetchDownloadShipment,
  setCurrentLotNewPage,
  downloadShipmentProduct,
  downloadShipmentCompany,
  getPulpCompany,
  uploadExportRebateDocument,
  getExportRebateDocument,
  downloadExportRebateDocument,
  deleteExportRebateDocument,
  submitExportRebateDocument,
  resetBill
  // resetExport
} from "../modules/shipments";

const mapStateToProps = (state) => {
  return {
    navIndex: state.app.navIndex,
    navInnerIndex: state.app.navInnerIndex,
    navClass: state.app.navClass,
    uom: state.app.uom,
    isUploaded: state.app.isUploaded,
    msg: state.app.msg,
    unreadItems: state.app.unreadItems,
    isConfirm: state.Shipments.isConfirm,
    confirmMsg: state.Shipments.confirmMsg,
    showAlert: state.Shipments.showAlert,
    alertMessage: state.Shipments.alertMessage,
    status: state.Shipments.status,
    fetching: state.Shipments.fetching,
    error: state.Shipments.error,
    outgoingShipment: state.Shipments.outgoingShipment,
    incomingShipment: state.Shipments.incomingShipment,
    isShipmentFormOpen: state.Shipments.isShipmentFormOpen,
    isAddProduct: state.Shipments.isAddProduct,
    buyerCompniesList: state.Shipments.buyerCompniesList,
    sellerUnitList: state.Shipments.sellerUnitList,
    buyerUnitList: state.Shipments.buyerUnitList,
    shipmentDetail: state.Shipments.shipmentDetail,
    isEdit: state.Shipments.isEdit,
    isSave: state.Shipments.isSave,
    products: state.Shipments.products,
    addedProductDetail: state.Shipments.addedProductDetail,
    viewShipmentDetail: state.Shipments.viewShipmentDetail,
    isReviewShipment: state.Shipments.isReviewShipment,
    isProductEdit: state.Shipments.isProductEdit,
    shipmentDeletedData: state.Shipments.shipmentDeletedData,
    incomingOrders: state.Shipments.incomingOrders,
    buyerIncomingOrders: state.Shipments.buyerIncomingOrders,
    linkedShipmentDetail: state.Shipments.linkedShipmentDetail,
    isAddLot: state.Shipments.isAddLot,
    shouldProductFormClose: state.Shipments.shouldProductFormClose,
    stockLot: state.Shipments.stockLot,
    viewLotDetail: state.Shipments.viewLotDetail,
    isLotEdit: state.Shipments.isLotEdit,
    isUpload: state.Shipments.isUpload,
    isDownload: state.Shipments.isDownload,
    isDownloadCatalogue: state.Shipments.isDownloadCatalogue,
    shouldShipmentPlaced: state.Shipments.shouldShipmentPlaced,
    confirmationCallFor: state.Shipments.confirmationCallFor,
    isDecline: state.Shipments.isDecline,
    isExport: state.Shipments.isExport,
    isReceive: state.Shipments.isReceive,
    consumedPrdQty: state.Shipments.consumedPrdQty,
    expectedConsumedQty: state.Shipments.expectedConsumedQty,
    shipmentPrdQty: state.Shipments.shipmentPrdQty,
    isLotNoBlank: state.Shipments.isLotNoBlank,
    totalRecordCount: state.Shipments.totalRecordCount,
    totalCountOutShip: state.Shipments.totalCountOutShip,
    pages: state.Shipments.pages,
    isStockValid: state.Shipments.isStockValid,
    isPermission: state.Shipments.isPermission,
    withAlert: state.Shipments.withAlert,
    stockLotDetail: state.Shipments.stockLotDetail,
    receiveShipSuccess: state.Shipments.receiveShipSuccess,
    showProductAfterLink: state.Shipments.showProductAfterLink,
    autoOrderSuccess: state.Shipments.autoOrderSuccess,
    pulpshipmentPlacedData: state.Shipments.pulpshipmentPlacedData,
    companyRole: state.Shipments.companyRole,
    viewCompanyInfo: state.Shipments.viewCompanyInfo,
    currentNewPage: state.Shipments.currentNewPage,
    isCertUpload: state.Shipments.isCertUpload,
    currentLanguage: state.app.currentLanguage,
    pdfStatus: state.Shipments.pdfStatus,
    certLogin: state.Shipments.certLogin,
    pdfReceiveStatus: state.Shipments.pdfReceiveStatus,
    qaData: state.app.qaData,
    fromDate: state.app.fromDate,
    toDate: state.app.toDate,
    fetchingFromReport: state.app.fetchingFromReport,
    nav: state.app.nav,
    isProduct: state.app.isProduct,
    jobStatus: state.Shipments.jobStatus,
    isJobActive: state.Shipments.isJobActive,
    jobStatus: state.Shipments.jobStatus,
    isProduct: state.Shipments.isProduct,
    externalData: state.Shipments.externalData,
    productDetail: state.Shipments.productDetail,
    externalView: state.Shipments.externalView,
    uniqueExternalView: state.Shipments.uniqueExternalView,
    uniqueProductView: state.Shipments.uniqueProductView,
    isEditText: state.Shipments.isEditText,
    companyNameExtVal: state.Shipments.companyNameExtVal,
    companyExtVendorId: state.Shipments.companyExtVendorId,
    extVendorCheck: state.Shipments.extVendorCheck,
    extBuyerCheckVal: state.Shipments.extBuyerCheckVal,
    invoiceCheckVal: state.Shipments.invoiceCheckVal,
    isExtBuyer: state.Shipments.isExtBuyer,
    isEditVendorVal: state.Shipments.isEditVendorVal,
    productsExt: state.Shipments.productsExt,
    isExtNumber: state.Shipments.isExtNumber,
    certificateNumber: state.Shipments.certificateNumber,
    fibreData: state.Shipments.fibreData,
    isAutoConfirm: state.Shipments.isAutoConfirm,
    confirmAutoMsg: state.Shipments.confirmAutoMsg,
    consumedAutoQty: state.Shipments.consumedAutoQty,
    shipmentAutoQty: state.Shipments.shipmentAutoQty,
    outgoingDownloadShipment: state.Shipments.outgoingDownloadShipment,
    incomingDownloadShipment: state.Shipments.incomingDownloadShipment,
    currentNewLotPage: state.Shipments.currentNewLotPage,
    companyDropPulpInfo: state.Shipments.companyDropPulpInfo,
    billFile: state.Shipments.billFile,
    invoiceFile: state.Shipments.invoiceFile,
    isExportModalReset:state.Shipments.isExportModalReset,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleUnreadItems: (type, id) => dispatch(handleUnreadItems(type, id)),
    setGlobalState: (innerIndex) => dispatch(setGlobalState(innerIndex)),
    resetAlertBox: (showAlert, message) =>
      dispatch(resetAlertBox(showAlert, message)),
    fetchShipment: (
      shipmentType,
      currentPage,
      pageSize,
      frmDt,
      toDt,
      query,
      state
    ) =>
      dispatch(
        fetchShipment(shipmentType, currentPage, pageSize, frmDt, toDt, query)
      ),
    openShipmentForm: (status) => dispatch(openShipmentForm(status)),
    setProps: (status, key) => dispatch(setProps(status, key)),
    fetchBuyer: (company_type_id) => dispatch(fetchBuyer(company_type_id)),
    fetchBuyerUnit: (company_key_id) =>
      dispatch(fetchBuyerUnit(company_key_id)),
    fetchSellerUnit: () => dispatch(fetchSellerUnit()),
    createShipments: (values, isEdit) =>
      dispatch(createShipments(values, isEdit)),
    fetchProducts: (product) => dispatch(fetchProducts(product)),
    addProducts: (values, isProductEdit) =>
      dispatch(addProducts(values, isProductEdit)),
    viewShipment: (orderId, key) => dispatch(viewShipment(orderId, key)),
    receiveShipment: (shipment_id) => dispatch(receiveShipment(shipment_id)),
    placeShipment: (
      shipemtId,
      asset_id,
      withAlert,
      buyer_company_key_id,
      invoice_number
    ) =>
      dispatch(
        placeShipment(
          shipemtId,
          asset_id,
          withAlert,
          buyer_company_key_id,
          invoice_number
        )
      ),
    cancelShipment: (shipmentDetail, openDeclineModalFor) =>
      dispatch(cancelShipment(shipmentDetail, openDeclineModalFor)),
    resetProps: () => dispatch(resetProps()),
    resetProducts: () => dispatch(resetProducts()),
    deleteShipment: (orderId, key) => dispatch(deleteShipment(orderId, key)),
    getIncomingOrders: () => dispatch(getIncomingOrders()),
    getBuyerIncomingOrders: (
      buyerCompanyKeyId,
      currentPage,
      pageSize,
      fromDt,
      toDt
    ) =>
      dispatch(
        getBuyerIncomingOrders(
          buyerCompanyKeyId,
          currentPage,
          pageSize,
          fromDt,
          toDt
        )
      ),
    linkShipment: (data, isLinked) => dispatch(linkShipment(data, isLinked)),
    setCreateFormState: () => dispatch(setCreateFormState()),
    viewStocks: (currentPage, pageSize, query) =>
      dispatch(viewStocks(currentPage, pageSize, query)),
    addLotToProduct: (stockData) => dispatch(addLotToProduct(stockData)),
    viewShipmentProductDetail: (shipment_product_id) =>
      dispatch(viewShipmentProductDetail(shipment_product_id)),
    deleProductLot: (shipment_product_lot_id) =>
      dispatch(deleProductLot(shipment_product_lot_id)),
    updateLot: (updateLotDetail) => dispatch(updateLot(updateLotDetail)),
    fetchUOM: (type) => dispatch(fetchUOM(type)),
    uploadFile: (file, owner) => dispatch(uploadFile(file, owner)),
    uploadCertFile: (file, owner, id, appendFile) =>
      dispatch(uploadCertFile(file, owner, id, appendFile)),
    compareQty: (orderId, shipmentId) =>
      dispatch(compareQty(orderId, shipmentId)),
    checkForBlankLotNo: (shipmentId) =>
      dispatch(checkForBlankLotNo(shipmentId)),
    fetchShipmentLotDetail: (shipmentId) =>
      dispatch(fetchShipmentLotDetail(shipmentId)),
    validateStock: (shipmentId) => dispatch(validateStock(shipmentId)),
    pulpshipment: (shipmentId, forestId) =>
      dispatch(pulpshipment(shipmentId, forestId)),
    getLotDetail: (consumeStockLotId) =>
      dispatch(getLotDetail(consumeStockLotId)),
    updateReceiveShipmentQty: (values, isProductEdit) =>
      dispatch(updateReceiveShipmentQty(values, isProductEdit)),
    generateAutoOrder: (shipmentId, userId, orderNumber) =>
      dispatch(generateAutoOrder(shipmentId, userId, orderNumber)),
    fetchCompanyRole: (company) => dispatch(fetchCompanyRole(company)),
    viewCompanyDetail: (companyId) => dispatch(viewCompanyDetail(companyId)),

    setCurrentNewPage: (page) => dispatch(setCurrentNewPage(page)),
    setCurrentLanguage: (language) => dispatch(setCurrentLanguage(language)),
    setPdfStatus: (value) => dispatch(setPdfStatus(value)),
    setCertLogin: (value) => dispatch(setCertLogin(value)),
    setPdfReceiveStatus: (value) => dispatch(setPdfReceiveStatus(value)),
    viewQaDetails: (shipment_id) => dispatch(viewQaDetails(shipment_id)),
    qaDeleteDoc: (shipment_document_id, document_file_name) =>
      dispatch(qaDeleteDoc(shipment_document_id, document_file_name)),
    qaDownloadDoc: (document_file_name, document_name) =>
      dispatch(qaDownloadDoc(document_file_name, document_name)),
    resetqaDataDetail: (value) => dispatch(resetqaDataDetail(value)),
    setAlertMeassage: (status, message, order) =>
      dispatch(setAlertMeassage(status, message, order)),
    setInnerNav: (lang) => dispatch(setInnerNav(lang)),
    setParam: (lang) => dispatch(setParam(lang)),
    setSearchString: (lang) => dispatch(setSearchString(lang)),
    setReportFetching: (lang) => dispatch(setReportFetching(lang)),
    setFromDate: (lang) => dispatch(setFromDate(lang)),
    setToDate: (lang) => dispatch(setToDate(lang)),

    externalJobStatus: (status) => dispatch(externalJobStatus(status)),
    setJobActive: (value) => dispatch(setJobActive(value)),
    resetJobActive: (value) => dispatch(resetJobActive(value)),
    externalJobStatus: (value) => dispatch(externalJobStatus(value)),
    setProductStatus: (value) => dispatch(setProductStatus(value)),
    createExternalJob: (
      ext_vendor_shipment_id,
      shipment_id,
      invoice_number,
      invoice_date,
      order_number,
      shipment_transport_mode,
      shipment_description,
      seller_companyname,
      shipment_date
    ) =>
      dispatch(
        createExternalJob(
          ext_vendor_shipment_id,
          shipment_id,
          invoice_number,
          invoice_date,
          order_number,
          shipment_transport_mode,
          shipment_description,
          seller_companyname,
          shipment_date
        )
      ),
    addExternalProduct: (
      shipment_id,
      ext_product_name,
      ext_product_description,
      ext_product_qty,
      ext_product_uom,
      ext_blend_percentage
    ) =>
      dispatch(
        addExternalProduct(
          shipment_id,
          ext_product_name,
          ext_product_description,
          ext_product_qty,
          ext_product_uom,
          ext_blend_percentage
        )
      ),
    viewExternalProduct: (ext_vendor_shipment_id) =>
      dispatch(viewExternalProduct(ext_vendor_shipment_id)),
    onDeleteExtProd: (ext_vendor_shipment_product_id) =>
      dispatch(onDeleteExtProd(ext_vendor_shipment_product_id)),
    onUpdateExtProd: (
      ext_vendor_shipment_product_id,
      shipment_id,
      ext_product_name,
      ext_product_description,
      ext_product_qty,
      ext_product_uom,
      ext_blend_percentage
    ) =>
      dispatch(
        onUpdateExtProd(
          ext_vendor_shipment_product_id,
          shipment_id,
          ext_product_name,
          ext_product_description,
          ext_product_qty,
          ext_product_uom,
          ext_blend_percentage
        )
      ),
    viewExternalShipment: (shipment_id) =>
      dispatch(viewExternalShipment(shipment_id)),
    viewUniqueExternalShipment: (ext_vendor_shipment_id) =>
      dispatch(viewUniqueExternalShipment(ext_vendor_shipment_id)),
    resestViewUniqueExternalShipment: (val) =>
      dispatch(resestViewUniqueExternalShipment(val)),
    onUpdateExtVendor: (
      ext_vendor_shipment_id,
      shipment_id,
      invoice_number,
      invoice_date,
      order_number,
      shipment_transport_mode,
      shipment_description,
      seller_company_name,
      shipment_date
    ) =>
      dispatch(
        onUpdateExtVendor(
          ext_vendor_shipment_id,
          shipment_id,
          invoice_number,
          invoice_date,
          order_number,
          shipment_transport_mode,
          shipment_description,
          seller_company_name,
          shipment_date
        )
      ),
    resetViewExternalProduct: (val) => dispatch(resetViewExternalProduct(val)),
    onDeleteVendor: (ext_vendor_shipment_id, shipment_id) =>
      dispatch(onDeleteVendor(ext_vendor_shipment_id, shipment_id)),
    viewUniqueExternalProduct: (ext_vendor_shipment_product_id) =>
      dispatch(viewUniqueExternalProduct(ext_vendor_shipment_product_id)),
    resetUniqueProductDetail: (val) => dispatch(resetUniqueProductDetail(val)),
    resetExternalData: (val) => dispatch(resetExternalData(val)),
    resetExternalDetail: (val) => dispatch(resetExternalDetail(val)),
    isEditTextVal: (status) => dispatch(isEditTextVal(status)),
    createExternalCompany: (company_name) =>
      dispatch(createExternalCompany(company_name)),
    companyNameExt: (val) => dispatch(companyNameExt(val)),
    resetExternVendorCheck: (val) => dispatch(resetExternVendorCheck(val)),
    extBuyerCheck: (val) => dispatch(extBuyerCheck(val)),
    resetInvoiceCheck: (val) => dispatch(resetInvoiceCheck(val)),
    checkForExtBuyer: (val) => dispatch(checkForExtBuyer(val)),
    isEditVendor: (val) => dispatch(isEditVendor(val)),
    fetchProductsExt: (company_key_id) =>
      dispatch(fetchProductsExt(company_key_id)),
    isExtNumberFunc: (val) => dispatch(isExtNumberFunc(val)),
    getCertificateNumber: (shipment_id) =>
      dispatch(getCertificateNumber(shipment_id)),
    getFibreData: (shipment_id) => dispatch(getFibreData(shipment_id)),
    compareAutoQty: (shipment_id) => dispatch(compareAutoQty(shipment_id)),
    setCurrentLotNewPage: (val) => dispatch(setCurrentLotNewPage(val)),
    fetchDownloadShipment: (
      shipmentType,
      currentPage,
      pageSize,
      frmDt,
      toDt,
      query,
      state
    ) =>
      dispatch(
        fetchDownloadShipment(
          shipmentType,
          currentPage,
          pageSize,
          frmDt,
          toDt,
          query
        )
      ),
    downloadShipmentProduct: () => dispatch(downloadShipmentProduct()),
    downloadShipmentCompany: () => dispatch(downloadShipmentCompany()),
    getPulpCompany: () => dispatch(getPulpCompany()),
    uploadExportRebateDocument: (file, shipment_id, appendFile, is_bill) =>
      dispatch(
        uploadExportRebateDocument(file, shipment_id, appendFile, is_bill)
      ),
    getExportRebateDocument: (shipmentId) =>
      dispatch(getExportRebateDocument(shipmentId)),
      downloadExportRebateDocument:(document_file_name,document_name) => dispatch(downloadExportRebateDocument(document_file_name,document_name)),
      deleteExportRebateDocument:(export_rebate_id,document_file_name) => dispatch(deleteExportRebateDocument(export_rebate_id,document_file_name)),
      submitExportRebateDocument : (shipment_id,data) => dispatch(submitExportRebateDocument(shipment_id,data)),
      resetBill : () => dispatch(resetBill())
      // resetExport: () => dispatch(resetExport()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Shipments);
