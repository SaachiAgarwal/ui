import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Pagination from 'react-paginate';

//localization
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
let strings = new LocalizedStrings(
  data
);


// import Pagination from 'components/Pagination';
import { Images } from 'config/Config';

/** IncomingShipmentTable
 *
 * @description This class is responsible to display a orders list of incoming orders
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class VendorProductTable extends Component {
  constructor(props) {
    super(props);
    // this.handlePageClick = this.handlePageClick.bind(this);
    // this.rowStyleFormat = this.rowStyleFormat.bind(this);
    //props.setCurrentNewPage(1)
    // this.state = {
    //   pageCount: 2
    // }
  }

  //   componentWillMount() {
  //     this.props.setActivePage('inshipments');
  //     //this.props.setCurrentNewPage(1)
  //   }
  //   handlePageClick(value) {
  //     this.props.fetchShipment('incomingShipment', value.selected+1, this.props.options.pageSize, '', '', this.props.searchText);
  //     this.props.setCurrentNewPage(value.selected+1)
  //   }

  //   rowStyleFormat(row, rowIdx) {
  //     return { fontWeight: row !== undefined && !row.is_read ? 'bold' : '' };
  //   }

  render() {
    const { currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);
    const {productName,unit} = strings.addProduct;
    const {description} = strings.createOrder;
    const {productQuantity} = strings.vendor;

    return (
      <div className='ordertable' >
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options} trStyle={this.rowStyleFormat}>
          <TableHeaderColumn dataField='ext_vendor_shipment_product_id' isKey={true} hidden={true}>Shipment Id</TableHeaderColumn>
          <TableHeaderColumn dataField='product_name' columnTitle={true} >{productName}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_qty' columnTitle={true}>{productQuantity}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_uom'>{unit}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_description' columnTitle={true}>{description}</TableHeaderColumn>
          <TableHeaderColumn key="editAction" dataFormat={this.props.productFormatter}>{strings.shipmentTableText.action}</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}

VendorProductTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default VendorProductTable;