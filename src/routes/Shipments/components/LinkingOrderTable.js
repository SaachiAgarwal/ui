import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Pagination from 'react-paginate';
//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);



/** LinkingOrdersTable
 *
 * @description This class is responsible to display a orders list of incoming orders for link model
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class LinkingOrderTable extends Component {
  constructor(props) {
    super(props);
    this.handlePageClick = this.handlePageClick.bind(this);
  }

  static defaultProps = {
    linkShipmentOptions: {
      sizePerPage: 5,
      pageSize: 5,
      pageStartIndex: 1,
      paginationSize: 3,
      prePage: 'Prev',
      nextPage: 'Next',
      firstPage: 'First',
      lastPage: 'Last',
      clearSearch: true,
      hideSizePerPage: true,
      onRowClick: this.editFormatter
    }
  }

  async handlePageClick(value) {
   await this.props.getBuyerIncomingOrders(this.props.buyerCompanyKeyId, value.selected+1, this.props.linkShipmentOptions.pageSize);
    await this.props.setCurrentLotNewPage(value.selected+1)
    await console.log(this.props.currentNewLotPage)
  }

  componentWillUnmount(){
     this.props.setCurrentLotNewPage(1)
  }

  render() {
    strings.setLanguage(this.props.currentLanguage);
    const {orderDate ,buyer, purchaseOrder, Action, linkedOrder}  = strings.OrdersTableText;
     return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.linkShipmentOptions}>
          <TableHeaderColumn dataField='order_id' hidden={true}>Order Id</TableHeaderColumn>
          <TableHeaderColumn dataField='new_order_date'>{orderDate}</TableHeaderColumn>
          <TableHeaderColumn dataField='sender_company_name'>{buyer}</TableHeaderColumn>
          <TableHeaderColumn dataField='order_number' isKey={true}>{purchaseOrder}</TableHeaderColumn>
          <TableHeaderColumn key="editAction" dataFormat={this.props.linkingFormatter}>{Action}</TableHeaderColumn>
          <TableHeaderColumn dataFormat={this.props.checkAssetOrder}>{linkedOrder}</TableHeaderColumn>

        </BootstrapTable>
        <div className="pagination-box">
          <Pagination
            initialPage = {this.props.currentNewLotPage-1}
            previousLabel={strings.prev}
            nextLabel={strings.next}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={this.props.pages}
            pageRangeDisplayed={5}
            onPageChange={this.handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        </div>
      </div>        
    );
  }
}

LinkingOrderTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  linkShipmentOptions: PropTypes.object.isRequired,
};

export default LinkingOrderTable;