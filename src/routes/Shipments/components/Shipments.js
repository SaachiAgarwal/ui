import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Button, Row } from 'reactstrap';
import Modal from 'react-modal';
import Swal from 'sweetalert2';
import FileDownload from 'js-file-download'
import Loader from 'components/Loader';
import Alert from "components/Alert";
import HeaderSection from 'components/HeaderSection';
import NavDrawer from 'components/NavDrawer';
import OutgoingShipmentTable from './OutgoingShipmentTable';
import IncomingShipmentTable from './IncomingShipmentTable';
import CreateShipment from './CreateShipment';
import ReviewShipment from './ReviewShipment';
import options from 'components/options';
import LinkModal from './LinkModal';
import LotModal from './LotModal';
import DeclineModal from './DeclineModal';
import PermissionModal from './PermissionModal';
import LinkAutoOrderModal from './LinkAutoOrderModal';
import CsvModal from 'components/CsvModal';
import Footer from 'components/Footer';
import ShipmentFilter from './ShipmentFilter'
import { getLocalStorage, getBuyerCompanyKeyId, getDate, getText, saveLocalStorage, getForestId } from 'components/Helper';
import 'css/style.scss';
import '../../Orders/components/Orders.scss';
import ReceiveModal from './ReceiveModal'
import { Images } from 'config/Config';
import CertModal from 'components/CertModal';
import PdfModal from './PdfModal'
import PdfReceiveModal from './PdfReceiveModal'
import ExternalJob from './ExternalJob'
import ReactLoading from 'react-loading';
import ExportModal from './ExportModal';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
let strings = new LocalizedStrings(
  data
);


//  Custom styles for link modal
const customStyles = {
  content: {
    top: '0',
    left: '0',
    right: '0',
    bottom: '0',
    marginRight: '0',
    transform: 'translate(0, 0)'
  }
};

export const BtnGrp = (props) => {
  return (
    <div>
      <Button className='add-button' hidden={props.callFor === 'inShipment' || props.role === 'Brand' ? true : false} onClick={() => props.openShipmentForm()}>+</Button>
      <Row>
        <Button className='upload-button' hidden={props.callFor === 'inShipment' || props.role === 'Brand' ? true : false} onClick={() => props.handleCsvModal(true, 'upload')}>{strings.btnGrpText.uploadCSV}</Button>
        <Button className='download-button' hidden={props.callFor === 'inShipment' || props.role === 'Brand' ? true : false} onClick={() => props.handleCsvModal(true, 'download')}>{strings.btnGrpText.downloadCSV}</Button>
        <Button className='download-button' hidden={props.callFor === 'inShipment' || props.role === 'Brand' ? true : false} onClick={() => props.handleCsvModal(true, 'downloadCatalogue')}>{strings.btnGrpText.shipmentCatalogue}</Button>
        <Button className={(props.callFor === 'inShipment' || props.role === 'Brand') ? 'upload-button' : 'download-button'} onClick={() => props.downloadShipmentList(props.callFor)}>{strings.btnGrpText.downloadShipments}</Button>
      </Row>
      <Row>
        {(props.callFor !== 'inShipment') &&
          <p className="col-md-6 disclaimer">{strings.btnGrpText.disclaimer}</p>
        }
      </Row>
    </div>
  );
}

export const OutgoingShipments = (props) => {
  return (
    <div>
      <ShipmentFilter
        role={props.role}
        handleFilterSearch={props.handleFilterSearch}
        handleFilterDateChange={props.handleFilterDateChange}
        fetchBrandsOrders={props.fetchBrandsOrders}
        options={props.options}
        pages={props.pages}
        fromDt={props.fromDt}
        toDt={props.toDt}
        searchText={props.searchText}
        entityName={props.entityName}
        currentLanguage={props.currentLanguage}
      />
      <OutgoingShipmentTable
        data={props.outgoingShipment}
        pagination={false}
        search={false}
        exportCSV={false}
        options={props.options}
        isForModal={false}
        outgoingFormatter={props.outgoingFormatter}
        setActivePage={props.setActivePage}
        totalRecordCount={props.totalRecordCount}
        pages={props.pages}
        searchText={props.searchText}
        fetchShipment={props.fetchShipment}
        setCurrentNewPage={props.setCurrentNewPage}
        currentNewPage={props.currentNewPage}
        currentLanguage={props.currentLanguage}
        filterfromDt = {props.filterfromDt}
        filtertoDt = {props.filtertoDt}
      />
    </div>
  );
}

export const IncomingShipment = (props) => {
  return (
    <div>
      <ShipmentFilter
        role={props.role}
        handleFilterSearch={props.handleFilterSearch}
        handleFilterDateChange={props.handleFilterDateChange}
        fetchBrandsOrders={props.fetchBrandsOrders}
        options={props.options}
        pages={props.pages}
        fromDt={props.fromDt}
        toDt={props.toDt}
        searchText={props.searchText}
        entityName={props.entityName}
        currentLanguage={props.currentLanguage}
      />
      <IncomingShipmentTable
        data={props.incomingShipment}
        pagination={false}
        search={false}
        exportCSV={false}
        options={props.options}
        isForModal={false}
        incomingFormatter={props.incomingFormatter}
        setActivePage={props.setActivePage}
        totalRecordCount={props.totalRecordCount}
        pages={props.pages}
        searchText={props.searchText}
        fetchShipment={props.fetchShipment}
        setCurrentNewPage={props.setCurrentNewPage}
        currentNewPage={props.currentNewPage}
        currentLanguage={props.currentLanguage}
        filterfromDt = {props.filterfromDt}
        filtertoDt = {props.filtertoDt}

      />
    </div>
  );
}

/** Shipments
 *
 * @description This class is a based class for orders functionality
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class Shipments extends React.Component {
  constructor(props) {
    super(props);
    this.confirm = this.confirm.bind(this);
    this.setActivePage = this.setActivePage.bind(this);
    this.handleAlertBox = this.handleAlertBox.bind(this);
    this.handleLinkModal = this.handleLinkModal.bind(this);
    this.handleEditChange = this.handleEditChange.bind(this);
    this.handleDeclineModal = this.handleDeclineModal.bind(this);
    this.handlePermissionModal = this.handlePermissionModal.bind(this);
    this.handleAutoOrderModel = this.handleAutoOrderModel.bind(this);
    this.handleInboxOutboxOrders = this.handleInboxOutboxOrders.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.incomingFormatter = this.incomingFormatter.bind(this);
    this.outgoingFormatter = this.outgoingFormatter.bind(this);
    this.linkingFormatter = this.linkingFormatter.bind(this);
    this.lotFormatter = this.lotFormatter.bind(this);
    this.linkShipment = this.linkShipment.bind(this);
    this.viewShipment = this.viewShipment.bind(this);
    this.exportShipment = this.exportShipment.bind(this);
    this.receiveShipment = this.receiveShipment.bind(this);
    this.handlereceiveShipment = this.handlereceiveShipment.bind(this);
    this.editShipment = this.editShipment.bind(this);
    this.back = this.back.bind(this);
    this.edit = this.edit.bind(this);
    this.delete = this.delete.bind(this);
    this.deleteShipment = this.deleteShipment.bind(this);
    this.placeShipment = this.placeShipment.bind(this);
    this.cancelShipment = this.cancelShipment.bind(this);
    this.createShipments = this.createShipments.bind(this);
    this.addProduct = this.addProduct.bind(this);
    this.setCurrentShipmentId = this.setCurrentShipmentId.bind(this);
    this.resetParentState = this.resetParentState.bind(this);
    this.addLot = this.addLot.bind(this);
    this.addLotToProduct = this.addLotToProduct.bind(this);
    this.editLot = this.editLot.bind(this);
    this.handleCsvModal = this.handleCsvModal.bind(this);
    this.placeShipmentAfter = this.placeShipmentAfter.bind(this);
    this.updateView = this.updateView.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.resetFilter = this.resetFilter.bind(this);
    this.downloadShipmentList = this.downloadShipmentList.bind(this);
    this.handleUnreadItems = this.handleUnreadItems.bind(this);
    this.openShipmentForm = this.openShipmentForm.bind(this);
    this.handleAutoOrderModal = this.handleAutoOrderModal.bind(this);
    this.handleFilterDateChange = this.handleFilterDateChange.bind(this);
    this.handleFilterSearch = this.handleFilterSearch.bind(this);
    this.handleCertModal = this.handleCertModal.bind(this);
    this.pdfCert = this.pdfCert.bind(this)
    this.pdfReceiveCert = this.pdfReceiveCert.bind(this)
    this.qaDelete = this.qaDelete.bind(this)
    this.qaFormatter = this.qaFormatter.bind(this)
    this.qaDownload = this.qaDownload.bind(this)
    this.externalJob = this.externalJob.bind(this)
    this.vendorFormatter = this.vendorFormatter.bind(this)
    this.onVendorEdit = this.onVendorEdit.bind(this)
    this.onVendorDelete = this.onVendorDelete.bind(this)
    this.confirmLot = this.confirmLot.bind(this)
    this.confirmConsume = this.confirmConsume.bind(this)
    this.submitExportRebate = this.submitExportRebate.bind(this)
    this.reset = this.reset.bind(this)
    this.setDataForTable = this.setDataForTable.bind(this)

    this.state = {
      openModal: false,
      outboxShipment: false,
      callFor: 'inShipment',
      active: 'inshipments',
      fromDt: '',
      toDt: '',
      role: '',
      filterfromDt: '',
      filtertoDt: '',
      isSuperUser: '',
      user: {},
      currentShipmentId: '',
      currentRow: {},
      isLinked: 0,
      isPlaced: 0,
      previousShipmentState: '',
      isViewIncomingShipment: false,
      activeCsvModal: 'upload',
      showProductAfterLink: false,
      openDeclineModalFor: 'incomingShipment',
      searchText: '',
      btnType: 'cancelBtn',
      owner: 'Shipment',
      reviewShipmentCallFor: 'incomingShipment',
      reviewIncommingshipment: null,
      activeCertModal: 'upload',
      certShipmentId: '',
      pdfCertStatus: false,
      shipment: '',
      buyerList: '',
      company_key_id: '',
      external_shipment_id: '',
      ext_vendor_shipment_id: '',
      state_vendor_company_id: '',
      isProductCreated: false,
      buyer_name_for_add_product: '',
      exportModalDetail:{}
    }
  }
  async componentDidMount() {
    console.log((getLocalStorage('loginDetail').role))
  }

  async componentWillMount() {

    console.log(`unmount is called`);
    this.props.handleUnreadItems('shipments');
    let user = getLocalStorage('loginDetail');
    this.props.setGlobalState({
      navIndex: 1,
      navInnerIndex: (user.role === 'Pulp' || this.props.nav) ? 1 : 0,
      navClass: strings.NavdrawerOptions[1].value
    });
    this.setState({ user: user });
    console.log(this.state.openDeclineModalFor);
    if (user.role !== 'Pulp' && !this.props.nav) {
      console.log("called for fetch")
      this.props.fetchShipment('incomingShipment');
      this.setState({
        openModal: false,
        outboxShipment: false,
        callFor: 'inShipment',
        role: user.role,
        isSuperUser: user.userInfo.is_super_user
      })
    } else {
      await this.setState({
        outboxShipment: true,
        openModal: false,
        callFor: 'outShipment',
        active: 'outshipments',
        reviewShipmentCallFor: 'outgoingShipment',
        openDeclineModalFor: 'outgoingShipment',
        isPlaced: 0,
        searchText: '',
        role: user.role,
        isSuperUser: user.userInfo.is_super_user
      });
      this.props.openShipmentForm(false);
      this.props.fetchShipment('outgoingShipment');
      // console.log(this.props.qaData)
    }
  }

  componentWillUnmount() {
    this.props.openShipmentForm(false);
    this.props.setCurrentNewPage(1)
    this.props.resetqaDataDetail('')
    this.props.resestViewUniqueExternalShipment('')
    this.props.resetViewExternalProduct('')
    this.props.resetExternalDetail('')
    this.props.isEditTextVal(false);
  }

  updateView() {
    this.props.fetchShipment('outgoingShipment');
  }

  confirm(key, callingFrom = '', msg = '', confirmBtnText, cancelBtnText) {
    let message = (key === 'place') ? `${this.props.confirmMsg}(~ ${this.props.expectedConsumedQty} kg)` : msg;
    Swal({
      title: 'Warning!',
      text: `${message}`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: `${confirmBtnText}`,
      cancelButtonText: `${cancelBtnText}`
    }).then(async (result) => {
      this.props.setProps(false, 'isConfirm');
      if (result.value) {
        if (key === 'place') {
          // setting a props to open permission modal
          await this.props.setProps(true, 'withAlert');

          this.handlePermissionModal(true);
        }
        else if (key === 'placeAuto') {
          this.handleAutoOrderModal(true)
        }

        else if (key === 'delete') {
          if (callingFrom === 'fromTable') {
            await this.props.deleteShipment(this.state.currentShipmentId);
            this.props.fetchShipment('outgoingShipment');
          } else if (callingFrom === 'fromReview') {
            await this.props.deleteShipment(this.state.currentShipmentId);
            this.props.fetchShipment('outgoingShipment');
            this.props.openShipmentForm(false);
            this.props.setProps(false, 'isReviewShipment');
          }
        }
      }
    })
  }

  async handleAutoOrderModel() {
    this.props.setProps(status, 'isPermission');
  }

  async placeShipmentAfter(value) {
    await this.props.isEditTextVal(false);
    await this.props.checkForExtBuyer(false);
    let lStorage = getLocalStorage('shipments');
    if (this.props.confirmationCallFor === 'shipFromReview') {
      //removing this.props.viewShipmentDetail.is_lot === 1 in incorporate SAPI changes
      if (this.state.user.role === 'Pulp'){
        console.log(this.props.viewShipmentDetail.shipmentDetail.linked_forest_key_id);
        await this.props.pulpshipment(lStorage.shipment_id, this.props.viewShipmentDetail.shipmentDetail.linked_forest_key_id);
      }
      await this.props.placeShipment(lStorage.shipment_id, lStorage.asset_id, this.props.withAlert, lStorage.buyer_company_key_id, lStorage.invoice_number);
      // this.props.fetchShipment('outgoingShipment');
      this.props.fetchShipment('outgoingShipment', this.props.currentNewPage, options.pageSize, this.state.fromDt, this.state.toDt);
      this.props.openShipmentForm(false);
      this.props.setProps(false, 'isReviewShipment');
      this.props.setProps(false, 'isPermission');
    } else if (this.props.confirmationCallFor === 'shipFromTable') {
      //removing this.props.viewShipmentDetail.is_lot === 1 in incorporate SAPI changes
      if (this.state.user.role === 'Pulp')
        await this.props.pulpshipment(lStorage.shipment_id, this.props.viewShipmentDetail.shipmentDetail.linked_forest_key_id);
      await this.props.placeShipment(lStorage.shipment_id, lStorage.asset_id, this.props.withAlert, lStorage.buyer_company_key_id, lStorage.invoice_number);
      // this.props.fetchShipment('outgoingShipment');
      this.props.fetchShipment('outgoingShipment', this.props.currentNewPage, options.pageSize, this.state.fromDt, this.state.toDt);
      this.props.setProps(false, 'isPermission');
      this.props.setProps(false, 'withAlert');
    }

    await this.props.resetqaDataDetail('')
  }

  async autoOrderPermission(value) {


  }

  setActivePage(status) {
    this.setState({ active: status });
  }

  async handleUnreadItems(shipmentId) {
    // Updating count to db
    await this.props.handleUnreadItems('shipments', shipmentId);
    // Refreshing count
    this.props.handleUnreadItems('shipments');
    // Refreshing shipment list to reset bold row
    console.log("called for fetch")

    this.props.fetchShipment('incomingShipment', options.currentNewPage, options.pageSize, '', '', this.state.searchText);
  }

  async handleSearch(query) {
    await this.setState({ searchText: query });
    if (this.state.active === 'inshipments') {
      if (query.length >= 2 || query.length === 0) this.props.fetchShipment('incomingShipment', options.currentPage, options.pageSize, '', '', query);
    } else {
      if (query.length >= 2 || query.length === 0) this.props.fetchShipment('outgoingShipment', options.currentPage, options.pageSize, '', '', query);
    }
  }

  async handleFilterSearch(query) {
    await this.setState({ searchText: query });
    if (this.state.active === 'inshipments') {
      if (query.length >= 2 || query.length === 0) this.props.fetchShipment('incomingShipment', options.currentPage, options.pageSize, this.state.filterfromDt, this.state.filtertoDt, query);
    } else {
      if (query.length >= 2 || query.length === 0) this.props.fetchShipment('outgoingShipment', options.currentPage, options.pageSize, this.state.filterfromDt, this.state.filtertoDt, query);
    }
  }

  async handleFilterDateChange(date, key) {
    let filterfromDt = '';
    let filtertoDt = '';
    if (key === 'fromDate' && key !== 'toDate') {
      filterfromDt = Object.values(date).reduce((total, val, index) => (index <= 9) ? total + val : total);
      await this.setState({ filterfromDt: filterfromDt });
    } else if (key === 'toDate' && key !== 'fromDate') {
      filtertoDt = Object.values(date).reduce((total, val, index) => (index <= 9) ? total + val : total);
      await this.setState({ filtertoDt: filtertoDt });
    }

    (this.state.active === 'inshipments') ?
      this.props.fetchShipment('incomingShipment', options.currentPage, options.pageSize, this.state.filterfromDt, this.state.filtertoDt, this.state.searchText) :
      this.props.fetchShipment('outgoingShipment', options.currentPage, options.pageSize, this.state.filterfromDt, this.state.filtertoDt, this.state.searchText);
  }

  async handleAlertBox() {
    this.props.resetAlertBox(false, "");
  }

  handleLinkModal(key) {
    (key === 'open') ? this.setState({ openModal: true }) : this.setState({ openModal: false });
  }

  async handleCsvModal(status, key) {
    if (key === 'upload') {
      await this.setState({
        activeCsvModal: 'upload',
        owner: 'Shipment'
      });
      this.props.setProps(status, 'isUpload');
      this.props.setGlobalState({ isUploaded: false });
    } else if (key === 'downloadCatalogue') {
      await this.setState({
        activeCsvModal: 'downloadCatalogue',
        owner: 'Shipment_Catalogue'
      });
      this.props.setProps(status, 'isDownloadCatalogue');
      this.props.setGlobalState({ isUploaded: false });
      if (status) {
        await this.props.fetchProducts();
        await this.props.fetchBuyer(this.state.user.userInfo.company_type_id);
      }
    } else {
      await this.setState({
        activeCsvModal: 'download',
        owner: 'Shipment'
      });
      this.props.setProps(status, 'isDownload');
      this.props.setGlobalState({ isUploaded: false });
    }
  }


  async handleCertModal(status, key) {
    // debugger

    //console.log(cell)
    let lStorage = getLocalStorage('shipments');
    // console.log(lStorage)
    if (key === 'upload') {
      await this.setState({
        activeCertModal: 'upload',
        owner: 'Shipment',
        certShipmentId: lStorage.shipment_id
      });
      this.props.setProps(status, 'isCertUpload');
      this.props.setGlobalState({ isUploaded: false });
    }

  }

  async handleDeclineModal(row, status, btnType) {
    console.log(btnType);
    await this.setState({ btnType: btnType });
    if (!_.isEmpty(row)) {
      this.setState({
        currentShipmentId: row.shipment_id,
        currentRow: row,
      });
    }

    this.props.setProps(status, 'isDecline');
    if (btnType === 'reasonBtn') {
      await this.setState({ openDeclineModalFor: 'reason' });
    } else if (btnType === 'cancelBtn') {
      console.log(`condition to be called`);
      await this.setState({ openDeclineModalFor: 'outgoingShipment' });
    }
    else {
      console.log(`inside decline fetch from incoming`);
      //this.handleUnreadItems(row.shipment_id);
      await this.setState({ openDeclineModalFor: 'incomingShipment' });
    }

    (this.state.openDeclineModalFor === 'incomingShipment') ?
      this.props.fetchShipment('incomingShipment', this.props.currentNewPage, options.pageSize, this.state.fromDt, this.state.toDt) :
      this.props.fetchShipment('outgoingShipment', this.props.currentNewPage, options.pageSize, this.state.fromDt, this.state.toDt);

  }

  async exportShipment(row,status){
    await this.setState({
      exportModalDetail:row
    })
    if(row.shipment_id != undefined){
      await this.props.viewShipment(row.shipment_id, '');
    }
    this.props.setProps(status, 'isExport');
  }

  async submitExportRebate(values){

    if(this.props.billFile.length > 0 && this.props.billFile[0].is_rejected === 1 && !this.props.isExportModalReset){
        console.log('bill rejected',this.props.billFile[0].is_rejected === 1)
        await this.props.resetBill()
    }else{
      let lStorage = getLocalStorage('shipments');
      let data = {...values,invoice_number:this.state.exportModalDetail.invoice_number};
      await this.props.submitExportRebateDocument(lStorage.shipment_id,data)
      await this.setState({
        exportModalDetail:{}
      })
      this.props.setProps(false, 'isExport');
    }
  }

  async reset(values){
    console.log('calling reset',values);
  }

  setDataForTable(preArr){
    let arr= [];
    if(this.props.isExportModalReset){
      arr = preArr.filter(item=>{
        if(item.is_rejected != 1){
          return item
        }
      })
    }else{
      if(this.props.billFile.length > 1){
        let rejectedCount = 0;
        preArr.map(item=>{
          if(item.is_rejected === 1){
            rejectedCount = rejectedCount + 1;
          }
        })
        if(rejectedCount === preArr.length){
          arr = [preArr[preArr.length - 1]]
        }else{
          arr = preArr.filter(item=>{
            if(item.is_rejected != 1){
              return item
            }
          })
        }
        
      }else if(this.props.invoiceFile.length > 1){
        let rejectedCount = 0;
        preArr.map(item=>{
          if(item.is_rejected === 1){
            rejectedCount = rejectedCount + 1;
          }
        })
        if(rejectedCount === preArr.length){
          arr = [preArr[preArr.length - 1]]
        }else{
          arr = preArr.filter(item=>{
            if(item.is_rejected != 1){
              return item
            }
          })
        }
      }else{
        arr = preArr
      }
    }
    return arr
  }

  async handlePermissionModal(status) {
    //await this.props.setProps(status, 'isPermission');
    await this.placeShipmentAfter(status)
  }

  async handleAutoOrderModal(status) {
    await this.props.setProps(status, 'showProductAfterLink');
    //await this.placeShipmentAfter(status)
  }

  async addLot(status) {
    if (status === 'close') {
      await this.props.setProps(false, 'isAddLot');
      await this.props.setProps(false, 'isLotEdit');
    } else {
      await this.props.setProps(true, 'isAddLot');
    }
  }

  async handleDateChange(date, key) {
    let fromDt = '';
    let toDt = '';
    if (key === 'fromDate' && key !== 'toDate') {
      fromDt = Object.values(date).reduce((total, val, index) => (index <= 9) ? total + val : total);
      await this.setState({ fromDt: fromDt });
    } else if (key === 'toDate' && key !== 'fromDate') {
      toDt = Object.values(date).reduce((total, val, index) => (index <= 9) ? total + val : total);
      await this.setState({ toDt: toDt });
    }

    if (this.state.fromDt !== '' && this.state.toDt !== '' && this.state.fromDt >= this.state.toDt) {
      alert('From-Date must be less then To-Date');
    } else if (this.state.fromDt !== '' && this.state.toDt !== '') {
      let buyerCompanyKeyId = getLocalStorage('BuyerCompanyKeyId');
      this.props.getBuyerIncomingOrders(buyerCompanyKeyId, options.currentPage, options.modalPageSize, this.state.fromDt, this.state.toDt);
    }
  }

  async resetFilter() {
    await this.setState({
      fromDt: '',
      toDt: ''
    });
  }

  handleInboxOutboxOrders(classType, innerType) {
    if (classType === 'orders' && innerType === 'inbox') {
    } else if (classType === 'orders' && innerType === 'outbox') {
    } else if (classType === 'shipments' && innerType === 'inbox') {
      this.props.setCurrentNewPage(1)
      this.setState({
        outboxShipment: false,
        openModal: false,
        callFor: 'inShipment',
        active: 'inshipments',
        reviewShipmentCallFor: 'incomingShipment',
        openDeclineModalFor: 'incomingShipment',
        isPlaced: 0,
        searchText: ''
      });
      this.props.openShipmentForm(false);
      this.props.fetchShipment('incomingShipment');
    } else if (classType === 'shipments' && innerType === 'outbox') {
      this.props.setCurrentNewPage(1)
      this.setState({
        outboxShipment: true,
        openModal: false,
        callFor: 'outShipment',
        active: 'outshipments',
        reviewShipmentCallFor: 'outgoingShipment',
        openDeclineModalFor: 'outgoingShipment',
        isPlaced: 0,
        searchText: '',
      });
      this.props.openShipmentForm(false);
      this.props.fetchShipment('outgoingShipment');
    } else if (classType === 'logout') {
      this.props.setCurrentLanguage('en');
      this.setState({
        outboxShipment: false,
        openModal: false,
        callFor: 'logout',
        active: 'logout'
      });
      removeLocalStorage('loginDetail');
      browserHistory.push(Url.HOME_PAGE);
    }
  }

  async viewShipment(row, key, type) {
    console.log('dadas')
    // debugger
    let company_type_id_ext = getLocalStorage('loginDetail').userInfo.company_type_id
    await this.props.fetchBuyer(company_type_id_ext);
    await this.props.viewShipment(row.shipment_id, key);
    // let company_key_id = getBuyerCompanyKeyId(this.props.buyerCompniesList, this.props.viewShipmentDetail.shipmentDetail.buyer_company_key_id, null);
    // console.log(company_key_id)
    // if(company_key_id){
    //   await this.props.viewCompanyDetail(company_key_id);
    // }
    await this.setState({
      isLinked: row.is_linked,
      isPlaced: row.is_shipped,
      reviewShipmentCallFor: key,
      reviewIncommingshipment: null
    });
    if (type === 'incomingShipment') {
      this.handleUnreadItems(row.shipment_id);
      await this.props.viewShipment(row.shipment_id, key);
      await this.setState({ isViewIncomingShipment: true, reviewIncommingshipment: type });
      //added to get buyerList
      //await this.props.fetchBuyer(this.state.user.userInfo.company_type_id);



    } else {
      await this.props.viewShipment(row.shipment_id, key);
      await this.props.fetchBuyer(this.state.user.userInfo.company_type_id);
      let company_key_id = getBuyerCompanyKeyId(this.props.buyerCompniesList, this.props.viewShipmentDetail.shipmentDetail.buyer_company_key_id, null);
      if (company_key_id) {
        await this.props.viewCompanyDetail(company_key_id);
      }
      await this.props.fetchBuyerUnit(company_key_id); // buyer unit
      await this.props.fetchSellerUnit(); // seller unit
    }
    this.props.openShipmentForm(true);
    this.setState({
      previousShipmentState: this.state.active,
      active: 'reviewShipment',
    });
    this.props.setProps(true, 'isReviewShipment');
    await this.props.viewQaDetails(row.shipment_id)
  }

  async editShipment(row, key) {
    // await console.log(this.state.user)
    await this.setState({ isLinked: row.is_linked, reviewIncommingshipment: null });
    await this.props.viewShipment(row.shipment_id, key);
    await this.props.fetchBuyer(this.state.user.userInfo.company_type_id);
    let company_key_id = getBuyerCompanyKeyId(this.props.buyerCompniesList, this.props.viewShipmentDetail.shipmentDetail.buyer_company_key_id, null);
    console.log(company_key_id);
    await this.props.viewCompanyDetail(company_key_id);
    await console.log(this.props.extVendorCheck.data.is_external);
    this.setState({ company_key_id: company_key_id })
    await this.props.fetchBuyerUnit(company_key_id); // buyer unit
    await this.props.fetchSellerUnit(); // seller unit
    saveLocalStorage('BuyerCompanyKeyId', this.props.viewShipmentDetail.shipmentDetail.buyer_company_key_id);
    this.handleEditChange('mount');
    this.props.openShipmentForm(true);
    this.setState({ active: 'createShipment' });
    // console.log(row.shipement_id);
    await this.props.viewQaDetails(row.shipment_id)
    // console.log(this.props.qaData)


  }

  async deleteShipment(row) {
    await this.setState({ currentShipmentId: row.shipment_id });
    this.props.resetProps();
    this.confirm('delete', 'fromTable', 'Do you really want to delete this shipment ?', 'Yes, delete it', 'No, keep it');
  }

  async externalJob() {
    let lStorage = getLocalStorage('shipments');
    // await console.log(lStorage.shipment_id);
    this.setState({
      external_shipment_id: lStorage.shipment_id
    })
    // await console.log(this.props.externalData)
    await this.props.externalJobStatus(true)
  }

  async placeShipment(row, shipFromReview) {
    // debugger
    if (shipFromReview) {
      this.props.setProps('shipFromReview', 'confirmationCallFor');
      let lStorage = getLocalStorage('shipments');
      let userDetail = getLocalStorage('loginDetail').userInfo.company_roles.indexOf('GM')
      await this.props.viewShipment(lStorage.shipment_id);
      //await this.props.checkForBlankLotNo(lStorage.shipment_id);
      if (this.props.viewShipmentDetail.products !== 'undefined' && this.props.viewShipmentDetail.products.length > 0) {
        if (!this.props.isLotNoBlank) {
          if (this.state.user.role === 'Birla Cellulose' || this.state.user.role === 'Pulp') {
            if (this.props.viewShipmentDetail.shipmentDetail.is_linked === 1) {
              this.handlePermissionModal(true);
            }
            else {
              //this.props.resetAlertBox(true, 'You must link your shipment.');
              this.handleAutoOrderModal(true);
            }
          }
          else if (this.props.viewShipmentDetail.is_lot !== 1) {
            this.props.resetAlertBox(true, 'You must add at least one lot.');
          } else if ((getLocalStorage('loginDetail').role !== "Garment Manufacturer" && getLocalStorage('loginDetail').role !== "Spinner" && getLocalStorage('loginDetail').role !== "Grey Fabric Manufacturer" && getLocalStorage('loginDetail').role !== "Finish Fabric Manufacturer" && getLocalStorage('loginDetail').role !== "Integrated Player" && getLocalStorage('loginDetail').role !== "Pulp" && this.props.viewShipmentDetail.shipmentDetail.is_linked !== 1 && userDetail < 0)) {
            // console.log(getLocalStorage('loginDetail').role);
            if ((userDetail === -1 && this.props.viewShipmentDetail.shipmentDetail.is_linked !== 1)) {
              this.props.resetAlertBox(true, 'You must link your shipment.');
            }
          }
          else if (((getLocalStorage('loginDetail').role === "Garment Manufacturer" || getLocalStorage('loginDetail').role === "Spinner" || getLocalStorage('loginDetail').role === "Grey Fabric Manufacturer" || getLocalStorage('loginDetail').role === "Finish Fabric Manufacturer" || getLocalStorage('loginDetail').role === "Integrated Player" || getLocalStorage('loginDetail').role === "Pulp") && this.props.viewShipmentDetail.shipmentDetail.is_linked !== 1)) {
            //this.handleAutoOrderModal(true)

            //await this.props.compareAutoQty(lStorage.shipment_id);

            if (this.props.viewShipmentDetail.is_lot === 1) {
              await this.props.validateStock(lStorage.shipment_id);
              if (!this.props.isStockValid) {
                await this.props.compareAutoQty(lStorage.shipment_id);
                if (!this.props.isAutoConfirm) {
                  // place shipment if isConfirm is false (it means there is no error)
                  await this.handleAutoOrderModal(true)
                } else {
                  if (this.props.consumedAutoQty < this.props.shipmentAutoQty) {
                    this.props.resetAlertBox(true, this.props.confirmAutoMsg);
                  } else {

                    // otherwise show confirm alert
                    this.confirm('placeAuto', '', '', 'Yes, place it', 'No, keep it');
                    //this.handleAutoOrderModal(true)
                  }
                  await this.props.setProps(false, 'isAutoConfirm');
                }
              }
              else {
                this.props.resetAlertBox(true, 'Insufficient stock available.');
              }
            }



          } else if (this.props.viewShipmentDetail.is_lot === 1 && this.props.viewShipmentDetail.shipmentDetail.is_linked === 1) {

            if (!this.props.autoOrderSuccess) {
              await this.props.validateStock(lStorage.shipment_id); //Added To Remove Validation after Auto ORders
            }

            if (!this.props.isStockValid) {
              await this.props.compareQty(this.props.viewShipmentDetail.shipmentDetail.order_id, lStorage.shipment_id);
              if (!this.props.isConfirm) {
                // place shipment if isConfirm is false (it means there is no error)
                this.handlePermissionModal(true);
              } else {
                if (this.props.consumedPrdQty < this.props.shipmentPrdQty) {
                  this.props.resetAlertBox(true, `${this.props.confirmMsg} (~ ${this.props.expectedConsumedQty} kg)`);
                } else {

                  // otherwise show confirm alert
                  this.confirm('place', '', '', 'Yes, place it', 'No, keep it');
                }
              }
            }
            else {
              this.props.resetAlertBox(true, 'Insufficient stock available.');
            }
          }
          else if (this.props.autoOrderSuccess) {
            //debugger
            //await this.props.validateStock(lStorage.shipment_id);
            if (!this.props.isStockValid) {
              await this.props.compareQty(this.props.viewShipmentDetail.shipmentDetail.order_id, lStorage.shipment_id);
              if (!this.props.isConfirm) {
                // place shipment if isConfirm is false (it means there is no error)
                this.handlePermissionModal(true);
              } else {
                if (this.props.consumedPrdQty < this.props.shipmentPrdQty) {
                  this.props.resetAlertBox(true, `${this.props.confirmMsg} (~ ${this.props.expectedConsumedQty} kg)`);
                } else {

                  // otherwise show confirm alert
                  this.confirm('place', '', '', 'Yes, place it', 'No, keep it');
                }
              }
            } else {
              this.props.resetAlertBox(true, 'Insufficient stock available.');
            }

          }
        } else {
          this.props.resetAlertBox(true, 'Please add shipment product lot number.');
        }
      }
      else {
        this.props.resetAlertBox(true, 'Please add atleast one product');
      }
    } else {
      // this.props.setProps('shipFromTable', 'confirmationCallFor');
      // await this.props.viewShipment(row.shipment_id);
      // await this.props.checkForBlankLotNo(row.shipment_id);
      // if (!this.props.isLotNoBlank) {
      //   if (this.state.user.role === 'Birla Cellulose') {
      //     //this.handlePermissionModal(true);
      //     this.handleAutoOrderModal(true)
      //   } else if (this.props.viewShipmentDetail.is_lot !== 1) {
      //     this.props.resetAlertBox(true, 'You must add at least one lot.');
      //   } else if (this.state.user.role === 'Garment Manufacturer') {
      //     //this.handlePermissionModal(true);
      //     this.handleAutoOrderModal(true)
      //   } else if (this.props.viewShipmentDetail.shipmentDetail.is_linked !== 1) {
      //     this.props.resetAlertBox(true, 'You must link your shipment.');
      //   } else if (this.props.viewShipmentDetail.is_lot === 1 && this.props.viewShipmentDetail.shipmentDetail.is_linked === 1) {
      //     await this.props.validateStock(row.shipment_id);
      //     if (!this.props.isStockValid) {
      //       await this.props.compareQty(this.props.viewShipmentDetail.shipmentDetail.order_id, row.shipment_id);
      //       if (!this.props.isConfirm) {
      //         this.handlePermissionModal(true);
      //         // place shipment if isConfirm is false (it means there is no error)


      //       } else {
      //         if (this.props.consumedPrdQty < this.props.shipmentPrdQty) {
      //           this.props.resetAlertBox(true, `${this.props.confirmMsg} (~ ${this.props.expectedConsumedQty} kg)`);
      //         } else {

      //           // otherwise show confirm alert
      //           this.confirm('place', '', '', 'Yes, place it', 'No, keep it');
      //         }
      //       }
      //     } else {
      //       this.props.resetAlertBox(true, 'Insufficient stock available.');
      //     }
      //   }
      //   // }
      // } else {
      //   this.props.resetAlertBox(true, 'Please add shipment product lot number.');
      // }


      this.props.setProps('shipFromTable', 'confirmationCallFor');
      let lStorage = getLocalStorage('shipments');
      let userDetail = getLocalStorage('loginDetail').userInfo.company_roles.indexOf('GM')
      await this.props.viewShipment(row.shipment_id);
      //await this.props.checkForBlankLotNo(row.shipment_id);
      if (this.props.viewShipmentDetail.products !== 'undefined' && this.props.viewShipmentDetail.products.length > 0) {
        if (!this.props.isLotNoBlank) {
          if (this.state.user.role === 'Birla Cellulose' || this.state.user.role === 'Pulp') {
            if (this.props.viewShipmentDetail.shipmentDetail.is_linked === 1) {
              this.handlePermissionModal(true);
            }
            else {
              //this.props.resetAlertBox(true, 'You must link your shipment.');
              this.handleAutoOrderModal(true);
            }
          }
          else if (this.props.viewShipmentDetail.is_lot !== 1) {
            this.props.resetAlertBox(true, 'You must add at least one lot.');
          } else if ((getLocalStorage('loginDetail').role !== "Garment Manufacturer" && getLocalStorage('loginDetail').role !== "Spinner" && getLocalStorage('loginDetail').role !== "Grey Fabric Manufacturer" && getLocalStorage('loginDetail').role !== "Finish Fabric Manufacturer" && getLocalStorage('loginDetail').role !== "Integrated Player" && this.props.viewShipmentDetail.shipmentDetail.is_linked !== 1 && userDetail < 0)) {
            // console.log(getLocalStorage('loginDetail').role);
            if ((userDetail === -1 && this.props.viewShipmentDetail.shipmentDetail.is_linked !== 1)) {
              this.props.resetAlertBox(true, 'You must link your shipment.');
            }
          }
          else if (((getLocalStorage('loginDetail').role === "Garment Manufacturer" || getLocalStorage('loginDetail').role === "Spinner" || getLocalStorage('loginDetail').role === "Grey Fabric Manufacturer" || getLocalStorage('loginDetail').role === "Finish Fabric Manufacturer" || getLocalStorage('loginDetail').role === "Integrated Player") && this.props.viewShipmentDetail.shipmentDetail.is_linked !== 1)) {
            //this.handleAutoOrderModal(true)

            //await this.props.compareAutoQty(lStorage.shipment_id);

            if (this.props.viewShipmentDetail.is_lot === 1) {
              await this.props.validateStock(row.shipment_id);
              if (!this.props.isStockValid) {
                await this.props.compareAutoQty(row.shipment_id);
                if (!this.props.isAutoConfirm) {
                  // place shipment if isConfirm is false (it means there is no error)
                  await this.handleAutoOrderModal(true)
                } else {
                  if (this.props.consumedAutoQty < this.props.shipmentAutoQty) {
                    this.props.resetAlertBox(true, this.props.confirmAutoMsg);
                  } else {

                    // otherwise show confirm alert
                    this.confirm('placeAuto', '', '', 'Yes, place it', 'No, keep it');
                    //this.handleAutoOrderModal(true)
                  }
                  await this.props.setProps(false, 'isAutoConfirm');
                }
              }
              else {
                this.props.resetAlertBox(true, 'Insufficient stock available.');
              }
            }



          } else if (this.props.viewShipmentDetail.is_lot === 1 && this.props.viewShipmentDetail.shipmentDetail.is_linked === 1) {

            if (!this.props.autoOrderSuccess) {
              await this.props.validateStock(row.shipment_id); //Added To Remove Validation after Auto ORders
            }

            if (!this.props.isStockValid) {
              await this.props.compareQty(this.props.viewShipmentDetail.shipmentDetail.order_id, row.shipment_id);
              if (!this.props.isConfirm) {
                // place shipment if isConfirm is false (it means there is no error)
                this.handlePermissionModal(true);
              } else {
                if (this.props.consumedPrdQty < this.props.shipmentPrdQty) {
                  this.props.resetAlertBox(true, `${this.props.confirmMsg} (~ ${this.props.expectedConsumedQty} kg)`);
                } else {

                  // otherwise show confirm alert
                  this.confirm('place', '', '', 'Yes, place it', 'No, keep it');
                }
              }
            }
            else {
              this.props.resetAlertBox(true, 'Insufficient stock available.');
            }
          }
          else if (this.props.autoOrderSuccess) {
            //debugger
            //await this.props.validateStock(lStorage.shipment_id);
            if (!this.props.isStockValid) {
              await this.props.compareQty(this.props.viewShipmentDetail.shipmentDetail.order_id, row.shipment_id);
              if (!this.props.isConfirm) {
                // place shipment if isConfirm is false (it means there is no error)
                this.handlePermissionModal(true);
              } else {
                if (this.props.consumedPrdQty < this.props.shipmentPrdQty) {
                  this.props.resetAlertBox(true, `${this.props.confirmMsg} (~ ${this.props.expectedConsumedQty} kg)`);
                } else {

                  // otherwise show confirm alert
                  this.confirm('place', '', '', 'Yes, place it', 'No, keep it');
                }
              }
            } else {
              this.props.resetAlertBox(true, 'Insufficient stock available.');
            }

          }
        } else {
          this.props.resetAlertBox(true, 'Please add shipment product lot number.');
        }
      }
      else {
        this.props.resetAlertBox(true, 'Please add atleast one product');
      }



    }
  }

  async cancelShipment(value) {
    let shipmentDetail = {
      shipment_id: this.state.currentShipmentId,
      cancel_reason: value.message,
      invoice_number: this.state.currentRow.invoice_number,
      recv_company_key_id: (this.state.openDeclineModalFor === 'incomingShipment') ? this.state.currentRow.buyer_company_key_id : this.state.currentRow.seller_company_key_id
    }
    await this.props.cancelShipment(shipmentDetail, this.state.openDeclineModalFor);
    this.handleDeclineModal({}, false, this.state.btnType);
    console.log("called for fetch")
    //(this.state.openDeclineModalFor === 'incomingShipment') ? this.props.fetchShipment('incomingShipment') : this.props.fetchShipment('outgoingShipment')
  }

  async back() {
    // debugger
    await this.setState({
      filterfromDt: '', 
      filtertoDt: ''

    })
    await this.props.resetqaDataDetail('');
    await this.props.resetExternVendorCheck('');
    // await this.props.isEditTextVal(false);
    // await this.props.checkForExtBuyer(false)
    if (this.props.jobStatus === true && this.props.isProduct === true) {
      await this.props.setProductStatus(false)
      //await this.props.resetExternalData('')
    }
    else if (this.props.jobStatus === true) {

      let buyerDetail = getLocalStorage('buyerDetail');
      //let buyer_company_key_id = getBuyerCompanyKeyId(this.state.buyerList, buyerDetail, false);
      await this.props.setProps([], 'doBuyerEmpty');
      let buyer_company_key_id = getBuyerCompanyKeyId(this.props.buyerCompniesList, buyerDetail, false);
      let lStorage = getLocalStorage('shipments');
      await this.props.viewShipment(lStorage.shipment_id);
      await this.props.fetchBuyer(this.state.user.userInfo.company_type_id);
      let company_key_id = getBuyerCompanyKeyId(this.props.buyerCompniesList, this.props.viewShipmentDetail.shipmentDetail.buyer_company_key_id, null);
      await this.props.fetchBuyerUnit(company_key_id); // buyer unit
      await this.props.fetchSellerUnit(); // seller unit



      this.state.shipment.buyer_company_key_id = buyer_company_key_id;
      this.state.shipment.invoice_date = (this.state.shipment.invoice_date.split('T'))[0];
      let loginDetails = getLocalStorage('loginDetail');

      //let lStorage = getLocalStorage('shipments');
      this.state.shipment.shipment_id = lStorage.shipment_id;
      //await this.props.createShipments(this.state.shipment, this.props.isEdit);

      // calling viewShipment api is must to show updated review page
      await this.props.viewShipment(getLocalStorage('shipments').shipment_id);

      // must call fetchShipment to show update outgoing orders list after edit orders
      this.props.fetchShipment('outgoingShipment', options.currentPage, options.modalPageSize, this.state.fromDt, this.state.toDt);
      this.props.setProps(true, 'isReviewShipment');
      this.props.externalJobStatus(false)
      await this.props.viewQaDetails(getLocalStorage('shipments').shipment_id)
      await this.props.resetViewExternalProduct('')
      await this.props.resetExternalData('')
      await this.props.resetUniqueProductDetail('')
      await this.props.resestViewUniqueExternalShipment('')

    }

    else if (this.props.isAddProduct) {
      this.setActivePage('createShipment');
      this.props.setProps(false, 'isAddProduct');
      this.setState({
        isProductCreated: true
      })
    } else {
      await this.props.isEditTextVal(false);
      await this.props.checkForExtBuyer(false)
      this.props.openShipmentForm(false);
      this.props.setProps(false, 'isReviewShipment');
      this.props.resetExternalDetail('')
    }

    (this.state.openDeclineModalFor === 'incomingShipment') ?
      this.props.fetchShipment('incomingShipment', this.props.currentNewPage, options.pageSize, this.state.fromDt, this.state.toDt) :
      this.props.fetchShipment('outgoingShipment', this.props.currentNewPage, options.pageSize, this.state.fromDt, this.state.toDt);
  }

  async edit() {
    let lStorage = getLocalStorage('shipments');
    await this.props.viewShipment(lStorage.shipment_id);
    await this.props.fetchBuyer(this.state.user.userInfo.company_type_id);
    let company_key_id = getBuyerCompanyKeyId(this.props.buyerCompniesList, this.props.viewShipmentDetail.shipmentDetail.buyer_company_key_id, null);
    await this.props.fetchBuyerUnit(company_key_id); // buyer unit
    await this.props.fetchSellerUnit(); // seller unit
    saveLocalStorage('BuyerCompanyKeyId', this.props.viewShipmentDetail.shipmentDetail.buyer_company_key_id);

    // setting isLinked state accordingly
    this.setState({ isLinked: this.props.viewShipmentDetail.shipmentDetail.is_linked });
    this.handleEditChange('mount');
    this.props.setProps(false, 'isReviewShipment');
    this.props.openShipmentForm(true);
    this.setState({ active: 'createShipment' });
  }

  async delete() {
    let lStorage = getLocalStorage('shipments');
    await this.setState({ currentShipmentId: lStorage.shipment_id });
    this.props.resetProps();
    this.confirm('delete', 'fromReview', 'Do you really want to delete this shipment ?', 'Yes, delete it', 'No, keep it');
  }

  async handleEditChange(mode) {
    if (mode === 'mount') {
      await this.props.setProps(true, 'isEdit');
    } else if (mode === 'unmount') {
      this.setState({
        isLinked: 0,
        showProductAfterLink: false
      });
      await this.props.setProps(false, 'isEdit');
      await this.props.setProps(false, 'isSave');
      await this.props.setProps(false, 'isLotEdit');

      this.props.resetProps();
    }
  }

  async openShipmentForm() {
    // setting buyerCompniesList to empty when user visit the createShipment page every time. we can do the same from
    // willUnmount of createShipment but if we do so then buyerCompniesList would not be available in createShipment
    // method to get buyer_company_key_id. in that case we will need to call fetchBuyer once again from createShipment 
    // method. so by doing this we save a http hit and user will always get a empty autocomplete every time after visit 
    // createShipment page
    this.props.setProps([], 'doBuyerEmpty');
    // comment this line after replacing buyer dropdown to Autocomplete
    // await this.props.fetchBuyer(this.state.user.userInfo.company_type_id); // will uncomment
    this.props.openShipmentForm(true);
    this.setState({ active: 'createShipment' });
    this.props.setProps(false, 'isReviewShipment');
    saveLocalStorage('buyerDetail', '');
  }

  async addProduct() {
    await this.props.setProps(true, 'isAddProduct');
    await this.setState({
      active: 'addProduct',
      showProductAfterLink: false
    });
  }

  async setCurrentShipmentId(id) {
    await this.setState({ currentShipmentId: id });
  }

  resetParentState() {
    this.setState({
      isViewIncomingShipment: false,
      active: this.state.previousShipmentState
    });
    this.props.resetProps();
    this.props.setProps(false, 'isReviewShipment');
  }

  async confirmLot() {
    Swal({
      title: 'Please Add Lot to proceed',
      type: 'warning',
    })
  }

  async createShipments(shipment) {
    if((getLocalStorage('loginDetail').role) === "Pulp"){
      shipment.linked_forest_key_id = getForestId(this.props.companyDropPulpInfo, shipment.pulp_company)
    }
    console.log(shipment)
    let loginData = getLocalStorage('loginDetail')
    // debugger
    console.log(`*******create shipment form************`);
    console.log(shipment);
    console.log(this.props.companyNameExtVal)
    console.log(this.props.viewShipmentDetail);
    if (this.props.isEdit) {
      await this.props.viewShipment(getLocalStorage('shipments').shipment_id);
    }
    if (this.props.viewShipmentDetail.is_lot === 0 && this.props.isEdit && (loginData.role !== "Birla Cellulose" && loginData.role !== "Pulp")) {

      await this.confirmLot();
    }
    else {
      await this.props.fetchBuyer(this.state.user.userInfo.company_type_id);
      if ((!_.isEmpty(this.props.viewShipmentDetail))) {
        let company_key_id = getBuyerCompanyKeyId(this.props.buyerCompniesList, this.props.viewShipmentDetail.shipmentDetail.buyer_company_key_id, null);
        console.log(company_key_id);
        await this.props.viewCompanyDetail(company_key_id);
      }

      await this.setState({ shipment: shipment, buyerList: this.props.buyerCompniesList })
      let buyerDetail = getLocalStorage('buyerDetail');
      let buyer_company_key_id = getBuyerCompanyKeyId(this.props.buyerCompniesList, buyerDetail, false);
      shipment.buyer_company_key_id = buyer_company_key_id;
      shipment.invoice_date = (shipment.invoice_date.split('T'))[0];
      let loginDetails = getLocalStorage('loginDetail');
      if ((buyerDetail === '' || buyerDetail === undefined) && this.props.isExtBuyer === false) {
        this.props.resetAlertBox(true, 'Please choose buyer name');

        //     console.log(shipment);

        //   await console.log(this.props.companyExtVendorId.vendor_company_key_id)
        //   if (this.props.isEdit) {
        //     this.setState({ active: 'reviewShipment' });
        //     let lStorage = getLocalStorage('shipments');
        //     shipment.shipment_id = lStorage.shipment_id;
        //     shipment.buyer_company_key_id = this.props.companyExtVendorId.vendor_company_key_id
        //     console.log(shipment);
        //     //await this.props.createShipments(shipment, this.props.isEdit);
        //     await this.props.generateAutoOrder(getLocalStorage('shipments').shipment_id, getLocalStorage('loginDetail').userInfo.user_id, shipment.shipment_text)
        //     // calling viewShipment api is must to show updated review page
        //     await this.props.viewShipment(getLocalStorage('shipments').shipment_id);
        //     await console.log(this.props.viewShipmentDetail);
        //     // must call fetchShipment to show update outgoing orders list after edit orders
        //     this.props.fetchShipment('outgoingShipment', options.currentPage, options.modalPageSize, this.state.fromDt, this.state.toDt);
        //     this.props.setProps(true, 'isReviewShipment');
        //   } else {

        //     //await this.props.createExternalCompany(this.props.companyNameExtVal)
        //     await this.props.createExternalCompany(shipment.ext_buyer_new);
        //     if(this.props.companyExtVendorId.vendor_company_key_id === null){
        //       this.props.resetAlertBox(true, 'This company already exits. Please to back to normal process.');
        //     }
        //     else{


        //     if(this.props.invoiceCheckVal === 'Invoice number already exists'){

        //       shipment.buyer_company_key_id = this.state.state_vendor_company_id
        //       console.log(shipment.buyer_company_key_id);
        //       await this.props.createShipments(shipment, this.props.isEdit);
        //       await this.props.isEditTextVal(true);
        //       await this.props.resetInvoiceCheck('')
        //     }
        //     else{
        //       shipment.buyer_company_key_id = this.props.companyExtVendorId.vendor_company_key_id
        //       console.log(shipment.buyer_company_key_id);
        //       await this.setState({
        //         state_vendor_company_id: shipment.buyer_company_key_id
        //       })
        //       await this.props.createShipments(shipment, this.props.isEdit);
        //       console.log(this.props.invoiceCheckVal);
        //       if(this.props.invoiceCheckVal !== 'Invoice number already exists'){
        //         await this.props.isEditTextVal(true);
        //       }

        //     }


        //     if (!this.props.error) {
        //       // it is must to call here to show updated shipment list if user just create the shipment and not adding any product and come back
        //       this.props.fetchShipment('outgoingShipment', options.currentPage, options.modalPageSize, this.state.fromDt, this.state.toDt);

        //       // calling viewShipment is must here to show the link/unlik button based on is_linked key
        //       this.props.viewShipment(getLocalStorage('shipments').shipment_id);
        //     }
        //   }
        // }

      }
      else if (this.props.isExtBuyer) {
        if (buyerDetail === '' || buyerDetail === undefined) {
          //this.props.resetAlertBox(true, 'Please choose buyer name');

          console.log(shipment);

          await console.log(this.props.companyExtVendorId.vendor_company_key_id)
          if (this.props.isEdit) {
            this.setState({ active: 'reviewShipment' });
            let lStorage = getLocalStorage('shipments');
            shipment.shipment_id = lStorage.shipment_id;
            shipment.buyer_company_key_id = this.props.companyExtVendorId.vendor_company_key_id
            console.log(shipment);
            // this.setState({
            //   buyer_name_for_add_product: shipment.shipment_text
            // })
            if (this.state.isProductCreated) {
              console.log(this.props.isExtNumber)
              shipment.shipment_text = this.props.isExtNumber
              this.setState({
                isProductCreated: false
              })
            }
            //await this.props.createShipments(shipment, this.props.isEdit);
            await this.props.generateAutoOrder(getLocalStorage('shipments').shipment_id, getLocalStorage('loginDetail').userInfo.user_id, shipment.shipment_text)
            // calling viewShipment api is must to show updated review page
            await this.props.viewShipment(getLocalStorage('shipments').shipment_id);
            await console.log(this.props.viewShipmentDetail);
            // must call fetchShipment to show update outgoing orders list after edit orders
            this.props.fetchShipment('outgoingShipment', options.currentPage, options.modalPageSize, this.state.fromDt, this.state.toDt);
            this.props.setProps(true, 'isReviewShipment');
          } else {

            //await this.props.createExternalCompany(this.props.companyNameExtVal)
            await this.props.createExternalCompany(shipment.ext_buyer_new);
            if (this.props.companyExtVendorId.vendor_company_key_id === null) {
              this.props.resetAlertBox(true, 'This company already exists. Please to back to normal process.');
            }
            else {


              if (this.props.invoiceCheckVal === 'Invoice number already exists') {

                shipment.buyer_company_key_id = this.state.state_vendor_company_id
                console.log(shipment.buyer_company_key_id);
                await this.props.createShipments(shipment, this.props.isEdit);
                await this.props.isEditTextVal(true);
                await this.props.resetInvoiceCheck('')
              }
              else {
                shipment.buyer_company_key_id = this.props.companyExtVendorId.vendor_company_key_id
                console.log(shipment.buyer_company_key_id);
                await this.setState({
                  state_vendor_company_id: shipment.buyer_company_key_id
                })
                await this.props.createShipments(shipment, this.props.isEdit);
                console.log(this.props.invoiceCheckVal);
                if (this.props.invoiceCheckVal !== 'Invoice number already exists') {
                  await this.props.isEditTextVal(true);
                }

              }


              if (!this.props.error) {
                // it is must to call here to show updated shipment list if user just create the shipment and not adding any product and come back
                this.props.fetchShipment('outgoingShipment', options.currentPage, options.modalPageSize, this.state.fromDt, this.state.toDt);

                // calling viewShipment is must here to show the link/unlik button based on is_linked key
                this.props.viewShipment(getLocalStorage('shipments').shipment_id);
              }
            }
          }

        }
      }

      else if (shipment.buyer_company_key_id === undefined || shipment.buyer_company_key_id === '') {
        this.props.resetAlertBox(true, 'Please choose correct buyer name');
      } else
        if (this.props.isEdit) {
          var test = 0
          var test1 = 0
          if (this.props.extVendorCheck.data.is_external) {

            console.log(this.state.isLinked)
            console.log(this.props.viewShipmentDetail.shipmentDetail.order_number);
            console.log(this.props.viewShipmentDetail.shipmentDetail.order_id)
            console.log(shipment);
            console.log(this.props.viewShipmentDetail.shipmentDetail.buyer_company_name)
            console.log(shipment.buyer_company_key_name)
            console.log(this.props.companyNameExtVal);
            if (this.props.viewShipmentDetail.shipmentDetail.buyer_company_name !== shipment.ext_buyer_new && (!_.isEmpty(shipment.ext_buyer_new))) {
              await this.props.createExternalCompany(shipment.ext_buyer_new);
              shipment.buyer_company_key_id = this.props.companyExtVendorId.vendor_company_key_id
              test = 1
              test1 = 1
            }
            if (this.props.viewShipmentDetail.shipmentDetail.order_number !== shipment.shipment_text) {
              let lStorage = getLocalStorage('shipments'),
                linkShipmentData = {
                  shipment_id: lStorage.shipment_id,
                  order_id: this.props.viewShipmentDetail.shipmentDetail.order_id,
                };
              if (this.state.isLinked) {
                await this.props.linkShipment(linkShipmentData, this.state.isLinked);
              }
              // await this.props.generateAutoOrder(getLocalStorage('shipments').shipment_id, getLocalStorage('loginDetail').userInfo.user_id, shipment.shipment_text)
              test = 1
            }
            if (test === 1) {
              await this.props.generateAutoOrder(getLocalStorage('shipments').shipment_id, getLocalStorage('loginDetail').userInfo.user_id, shipment.shipment_text)
            }
          }
          this.setState({ active: 'reviewShipment' });
          let lStorage = getLocalStorage('shipments');
          shipment.shipment_id = lStorage.shipment_id;
          if (test1 === 1) {
            await this.props.createShipments(shipment, this.props.isEdit);
          }
          if (!this.props.extVendorCheck.data.is_external) {
            await this.props.createShipments(shipment, this.props.isEdit);
          }

          // calling viewShipment api is must to show updated review page
          await this.props.viewShipment(getLocalStorage('shipments').shipment_id);

          // must call fetchShipment to show update outgoing orders list after edit orders
          this.props.fetchShipment('outgoingShipment', options.currentPage, options.modalPageSize, this.state.fromDt, this.state.toDt);
          this.props.setProps(true, 'isReviewShipment');
        } else {
          await this.props.createShipments(shipment, this.props.isEdit);

          if (!this.props.error) {
            // it is must to call here to show updated shipment list if user just create the shipment and not adding any product and come back
            this.props.fetchShipment('outgoingShipment', options.currentPage, options.modalPageSize, this.state.fromDt, this.state.toDt);

            // calling viewShipment is must here to show the link/unlik button based on is_linked key
            this.props.viewShipment(getLocalStorage('shipments').shipment_id);
          }
        }
    }

  }

  async linkShipment(row, order_number) {
    if (!_.isEmpty(this.props.viewShipmentDetail.shipmentDetail) && this.props.viewShipmentDetail.shipmentDetail.order_number !== null && this.props.viewShipmentDetail.shipmentDetail.order_number !== row.order_number) {
      this.props.resetAlertBox(true, 'First unlink the linked order');
    } else {
      //await this.setState({ showProductAfterLink: true })
      let lStorage = getLocalStorage('shipments'),
        linkShipmentData = {
          shipment_id: lStorage.shipment_id,
          order_id: row.order_id,
        };
      let user = getLocalStorage('loginDetail');
      // performing link and unlink shipment
      await this.props.linkShipment(linkShipmentData, this.state.isLinked);

      // this.line is working as a switch. it close the link modal when user link/unlink the shipment
      (this.props.viewShipmentDetail.shipmentDetail.order_number === row.order_number) ?
        await this.setState({ openModal: true, isLinked: 1 }) :
        await this.setState({ openModal: false, isLinked: 0 });

      // performing link and unlink shipment
      await this.props.linkShipment(linkShipmentData, this.state.isLinked);

      // fetch updated viewOrderDetail to render the form with updated data
      await this.props.viewShipment(lStorage.shipment_id, 'outgoingShipment');

      // fetch outgoing orders to reset isLinked state accordingly-----, options.currentPage, options.modalPageSize, this.state.fromDt, this.state.toDt);
      await this.props.fetchShipment('outgoingShipment', options.currentPage, this.props.totalCountOutShip, '', '', '');
      this.props.outgoingShipment.map(async (item) => {
        if (item.shipment_id === lStorage.shipment_id) {
          await this.setState({ isLinked: item.is_linked });
          await this.setState({ showProductAfterLink: false })
        } else {
          await this.setState({ showProductAfterLink: true })
        }
      });
      let buyerCompanyKeyId = getLocalStorage('BuyerCompanyKeyId');
      await this.props.getBuyerIncomingOrders(buyerCompanyKeyId, this.props.currentNewLotPage, options.modalPageSize, this.state.fromDt, this.state.toDt);

      // if (this.state.isLinked !== 1) {
      //   // open again the link modal if user unlink the shipment to reset the link/unlink button value 
      //   this.setState({ openModal: true });
      //   this.props.fetchShipment('outgoingShipment', options.currentPage, options.modalPageSize, this.state.fromDt, this.state.toDt);
      // }

    }
  }

  async receiveShipment(cell) {


    this.props.setProps(status, 'isReceive');
    cell.preventDefault()

    //await this.handleUnreadItems(this.state.currentShipmentId);
    await this.props.receiveShipment(this.state.currentShipmentId);
    await this.handlereceiveShipment(false)


  }

  async handlereceiveShipment(cell, status) {
    // fetch updated viewOrderDetail to render the form with updated data
    if (cell.shipment_id !== undefined)
      await this.props.viewShipment(cell.shipment_id, 'outgoingShipment');

    await this.setState({
      currentShipmentId: cell.shipment_id
    });
    await this.props.setProps(status, 'isReceive');
    //this.props.fetchShipment('incomingShipment', options.currentPage, options.pageSize, this.state.fromDt, this.state.toDt)
    this.props.fetchShipment('incomingShipment', this.props.currentNewPage, options.pageSize, this.state.fromDt, this.state.toDt)
  }

  async addLotToProduct(row) {
    let lStorage = getLocalStorage('shipments');
    let stockData = {
      shipment_product_id: lStorage.shipment_product_id,
      stock_lot_id: row.stock_lot_id,
      consume_product_qty: row.product_qty
    }
    await this.props.addLotToProduct(stockData);

    // after addLot successfully getting productShipmentEditDetail to display the added lot table
    await this.props.viewShipmentProductDetail(lStorage.shipment_product_id);
    

    //Add Lot After Product
    console.log(this.props.viewLotDetail[0].consume_stock_lot_id)
    
    let n = this.props.viewLotDetail.length

    await this.props.getLotDetail(this.props.viewLotDetail[n-1].consume_stock_lot_id);
    saveLocalStorage('selectedLotDetail', this.props.viewLotDetail[n-1]);
    // await this.props.setProps(true, 'isLotEdit');
    this.confirmConsume()
  }
  async confirmConsume(){
    Swal({
      title: 'Warning!',
      text: "Do you want to consume entire lot",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'Edit lot'
    }).then(async (result) => {
      if (result.value) {

      }
      else{
          await this.props.setProps(true, 'isLotEdit');
      }
  
    })
  }

  async editLot(values) {
    let lStorage = getLocalStorage('shipments');
    let lotDetail = getLocalStorage('selectedLotDetail');
    let updateLotDetail = {
      consume_product_qty: values.lot_qty,
      shipment_product_lot_id: lotDetail.shipment_product_lot_id
    }
    await this.props.updateLot(updateLotDetail);

    // after editLot successfully getting productShipmentEditDetail to display the added lot table
    await this.props.viewShipmentProductDetail(lStorage.shipment_product_id);
  }

  async download(list, type, fileName) {
    let data = 'invoice_number,supplier,shipment_date,linked_purchase_order,shipment_status,buyer,certificate_number,product_name,product_qty,product_qty_received,product_unit,product_description,\n';
    if (!_.isEmpty(list)) {
      list.map((item) => {
        data += `${item.invoice_number},`
        data += `${item.seller_company_name},`
        data += `${item.invoice_date},`
        data += `${item.linked_PO},`
        data += `${item.Status},`
        data += `${item.buyer_company_name},`
        data += `${item.certificate ? item.certificate : ''},`
        data += `${item.product_name},`
        data += `${item.product_qty},`
        data += `${item.product_qty_received},`
        data += `${item.product_uom},`
        data += `${item.product_description},`
        data += '\n';
      });
    }

    FileDownload(data, fileName);
    await this.props.fetchShipment(type, options.currentPage, options.pageSize, '', '', this.state.searchText);
  }

  async downloadShipmentList(key) {
    let data = '',
      fileName = 'Shipment.csv',
      type = 'incomingShipment';

    if (key === 'inShipment') {
      fileName = 'IncomingShipmentList.csv';
      type = 'incomingShipment';
      await this.props.fetchDownloadShipment(type, options.currentPage, this.props.totalRecordCount, '', '', this.state.searchText);
      this.download(this.props.incomingDownloadShipment, type, fileName);
    } else {
      fileName = 'OutgoingShipmentList.csv';
      type = 'outgoingShipment';
      await this.props.fetchDownloadShipment(type, options.currentPage, this.props.totalRecordCount, '', '', this.state.searchText);
      this.download(this.props.outgoingDownloadShipment, type, fileName);
    }
  }
  async pdfCert(row) {
    let userDetail = getLocalStorage('loginDetail')
    await this.props.setCertLogin(userDetail);
    // console.log(userDetail.role);
    await this.props.viewShipment(row.shipment_id);
    await this.props.getCertificateNumber(row.shipment_id);
    await this.props.getFibreData(row.shipment_id);
    await this.props.setPdfStatus(true)
    await this.setState({
      pdfCertStatus: true
    })
    await console.log(this.props.certificateNumber)
    await console.log(this.props.fibreData)
    // console.log(this.props.viewShipmentDetail);

  }

  async pdfReceiveCert(row) {
    let userDetail = getLocalStorage('loginDetail')
    await this.props.setCertLogin(userDetail);
    // console.log(userDetail.role);
    await this.props.viewShipment(row.shipment_id);
    await this.props.getCertificateNumber(row.shipment_id);
    await this.props.getFibreData(row.shipment_id);
    await this.props.setPdfReceiveStatus(true)
    // console.log(this.props.viewShipmentDetail);

  }

  async qaDelete(cell) {
    console.log(cell)
    await this.props.qaDeleteDoc(cell.shipment_document_id, cell.document_file_name);
    await this.props.viewQaDetails(cell.shipment_id);
    await console.log(this.props.qaData);
    await console.log(this.props.qaData.documents.length)

  }

  async qaDownload(cell) {
    console.log(cell)
    await this.props.qaDownloadDoc(cell.document_file_name, cell.document_name)
  }


  incomingFormatter(row, cell) {
    return (
      <ButtonGroup>
        <Button className="action" title="view" onClick={() => this.viewShipment(cell, '', 'incomingShipment')} >{strings.incomingFormatterText.view}</Button>
        {(cell.is_received !== 1 && cell.is_rejected !== 1 && cell.is_cancelled !== 1) && <Button className="action" title="view" onClick={() => this.handlereceiveShipment(cell, true)} >{strings.incomingFormatterText.receive}</Button>}
        {(cell.is_received !== 1 && cell.is_rejected !== 1 && cell.is_cancelled !== 1) && <Button className="action delete" onClick={() => this.handleDeclineModal(cell, true, 'declineBtn')} >{strings.incomingFormatterText.decline}</Button>}
        {(cell.is_rejected === 1 || cell.is_cancelled === 1) && <Button className="action" onClick={() => this.handleDeclineModal(cell, true, 'reasonBtn')} >{strings.incomingFormatterText.reason}</Button>}
        {(cell.is_received === 1 && cell.is_rejected !== 1 && cell.is_cancelled !== 1 && (getLocalStorage('loginDetail').role !== "Birla Cellulose")) && <Button className="action" title="certificate" onClick={() => this.pdfReceiveCert(cell)} ><img className="certi-image" src={Images.certificateImage} /></Button>}
      </ButtonGroup>
    );
  }
  
  outgoingFormatter(row, cell) {

    return (
      <ButtonGroup>
        <Button className="action" title="view" onClick={() => this.viewShipment(cell)} >{strings.outgoingFormatterText.view}</Button>
        {(cell.is_shipped !== 1) && <Button className="action" onClick={() => this.editShipment(cell)} >{strings.outgoingFormatterText.edit}</Button>}
        {(cell.is_shipped !== 1) && <Button className="action delete" onClick={() => this.deleteShipment(cell)} >{strings.outgoingFormatterText.delete}</Button>}
        {(cell.is_shipped !== 1) && <Button className="action" onClick={() => this.placeShipment(cell, false)} >{strings.outgoingFormatterText.ship}</Button>}
        {(cell.is_rejected === 1 || cell.is_cancelled === 1) && <Button className="action" onClick={() => this.handleDeclineModal(cell, true, 'reasonBtn')} >{strings.outgoingFormatterText.reason}</Button>}
        {(cell.is_received !== 1 && cell.is_cancelled !== 1 && cell.is_shipped === 1 && cell.is_rejected !== 1) && <Button className="action delete" onClick={() => this.handleDeclineModal(cell, true, 'cancelBtn')} >{strings.outgoingFormatterText.cancel}</Button>}
        {(cell.is_received === 1 && cell.is_cancelled !== 1 && cell.is_rejected !== 1 && (getLocalStorage('loginDetail').role !== "Pulp")) && <Button className="action" title="certificate" onClick={() => this.pdfCert(cell)} ><img className="certi-image" src={Images.certificateImage} /></Button>}
        {/* {(cell.is_received === 1 || cell.is_shipped === 1) && <Button className="action export" title="export" onClick={()=>this.exportShipment(cell,true,'exportBtn')} >{strings.outgoingFormatterText.export}</Button>} */}
        
      </ButtonGroup>
    );
  }
  //{(cell.is_received !== 1 && cell.is_shipped === 1) && <Button className="action" onClick={() => this.handleCertModal(cell, true, 'upload')}><img className="certi-image" src = {Images.certificateImage} /></Button>}
  linkingFormatter(row, cell) {
    return (
      <ButtonGroup>
        <Button className="action" onClick={() => this.linkShipment(cell, this.props.viewShipmentDetail.shipmentDetail.order_number)} >{(!_.isEmpty(this.props.viewShipmentDetail.shipmentDetail)) ? ((cell.order_number === this.props.viewShipmentDetail.shipmentDetail.order_number) ? `${strings.outgoingFormatterText.unlink}` : `${strings.outgoingFormatterText.link}`) : `${strings.outgoingFormatterText.link}`}</Button>
      </ButtonGroup>
    );
  }
  qaFormatter(row, cell) {
    return (
      <ButtonGroup>
        <Button className="action" title="view" onClick={() => this.qaDownload(cell)} >{strings.outgoingFormatterText.view}</Button>
        {cell.is_shipped === 0 && <Button className="action delete" title="delete" onClick={() => this.qaDelete(cell)}>{strings.outgoingFormatterText.delete}</Button>}
      </ButtonGroup>
    );
  }

  lotFormatter(row, cell) {
    return (
      <ButtonGroup>
        <Button className="action" onClick={() => this.addLotToProduct(cell)} >{(!_.isEmpty(this.props.viewShipmentDetail.shipmentDetail)) ? ((cell.order_number === this.props.viewShipmentDetail.shipmentDetail.order_number) ? 'remove' : 'add') : 'add'}</Button>
      </ButtonGroup>
    );
  }
  vendorFormatter(row, cell) {
    return (
      <ButtonGroup>
        <Button className="action" title="edit" onClick={() => this.onVendorEdit(cell)} >{strings.outgoingFormatterText.edit}</Button>
        <Button className="action delete" title="delete" onClick={() => this.onVendorDelete(cell)} >{strings.outgoingFormatterText.delete}</Button>
      </ButtonGroup>
    );
  }

  async onVendorEdit(cell) {
    await this.setState({
      ext_vendor_shipment_id: cell.ext_vendor_shipment_id
    })
    // console.log(cell.ext_vendor_shipment_id)
    await this.props.viewUniqueExternalShipment(cell.ext_vendor_shipment_id);
    // await console.log(this.props.uniqueExternalView);
    await this.props.externalJobStatus(true)
    await this.props.viewExternalProduct(cell.ext_vendor_shipment_id)
  }

  async onVendorDelete(cell) {

    // console.log(cell)
    let lStorage = getLocalStorage('shipments');
    // console.log(lStorage.shipment_id)
    await this.props.onDeleteVendor(cell.ext_vendor_shipment_id, lStorage.shipment_id)
    await this.props.viewExternalShipment(lStorage.shipment_id);
  }

  render() {
    strings.setLanguage(this.props.currentLanguage);
    return (
      <div>
        {/* <Loader loading={(this.props.fetching !== undefined) ? this.props.fetching : false} /> */}
        {this.props.fetching &&
          <ReactLoading type="bubbles" style={{
            fill: '#4e5be1e0',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundSize: '200px',
            position: 'fixed',
            zIndex: 10000,
            width: '6%',
            height: '72%',
            top: '50%',
            left: '50%'
          }} />
        }
        <Alert
          showAlert={(typeof this.props.showAlert !== "undefined" ? this.props.showAlert : false)}
          message={(typeof this.props.alertMessage !== "undefined" ? this.props.alertMessage : "")}
          handleAlertBox={this.handleAlertBox}
          status={this.props.status}
        />
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-2 col-lg-2 sidebar">
              <NavDrawer
                role={this.state.role}
                isSuperUser={this.state.isSuperUser}
                setGlobalState={this.props.setGlobalState}
                navIndex={this.props.navIndex}
                navInnerIndex={this.props.navInnerIndex}
                navClass={this.props.navClass}
                handleInboxOutboxOrders={this.handleInboxOutboxOrders}
                unreadItems={this.props.unreadItems}
                currentLanguage={this.props.currentLanguage}
                setInnerNav={this.props.setInnerNav}
                setParam={this.props.setParam}
                setSearchString={this.props.setSearchString}
                setReportFetching={this.props.setReportFetching}
              />
            </div>
            <div className="col-md-10 col-lg-10 col-lg-offset-2 col-md-offset-2 main">
              {(this.props.isJobActive) ?
                <HeaderSection
                  active={this.props.isJobActive}
                  searchText={this.state.searchText}
                  handleSearch={this.handleSearch}
                  setCurrentLanguage={this.props.setCurrentLanguage}
                  HeaderArray={strings.HeaderArray}
                  currentLanguage={this.props.currentLanguage}

                />
                :
                <HeaderSection
                  active={this.state.active}
                  searchText={this.state.searchText}
                  handleSearch={this.handleSearch}
                  setCurrentLanguage={this.props.setCurrentLanguage}
                  HeaderArray={strings.HeaderArray}
                  currentLanguage={this.props.currentLanguage}

                />
              }
              <Modal
                isOpen={this.state.openModal}
                style={customStyles}
                ariaHideApp={false}
              >
                <LinkModal
                  handleLinkModal={this.handleLinkModal}
                  getBuyerIncomingOrders={this.props.getBuyerIncomingOrders}
                  orders={this.props.buyerIncomingOrders} // shipment will be linked to buyer incoming order
                  options={options}
                  linkingFormatter={this.linkingFormatter}
                  handleDateChange={this.handleDateChange}
                  pages={this.props.pages}
                  fromDt={this.state.fromDt}
                  toDt={this.state.toDt}
                  resetFilter={this.resetFilter}
                  currentLanguage={this.props.currentLanguage}
                  setCurrentLotNewPage={this.props.setCurrentLotNewPage}
                  currentNewLotPage={this.props.currentNewLotPage}
                />
              </Modal>
              <Modal
                isOpen={this.props.isAddLot || this.props.isLotEdit}
                style={customStyles}
                ariaHideApp={false}
              >
                <LotModal
                  onSubmit={this.editLot}
                  addLot={this.addLot}
                  getIncomingOrders={this.props.getIncomingOrders}
                  orders={this.props.incomingOrders} // shipment will be linked to incoming order
                  viewStocks={this.props.viewStocks} // method to get stock lot
                  stockLot={this.props.stockLot}
                  options={options}
                  isLotEdit={this.props.isLotEdit}
                  lotFormatter={this.lotFormatter}
                  pages={this.props.pages}
                  stockLotDetail={this.props.stockLotDetail}
                  currentLanguage={this.props.currentLanguage}
                />
              </Modal>
              <Modal
                isOpen={this.props.isUpload || this.props.isDownload || this.props.isDownloadCatalogue}
                style={customStyles}
                ariaHideApp={false}
              >
                <CsvModal
                  owner={this.state.owner}
                  role={this.state.role}
                  handleCsvModal={this.handleCsvModal}
                  getText={getText}
                  activeCsvModal={this.state.activeCsvModal}
                  uploadFile={this.props.uploadFile}
                  updateView={this.props.fetchShipment}
                  viewToBeUpdated={'outgoingShipment'}
                  data={this.props.outgoingShipment}
                  isUploaded={this.props.isUploaded}
                  msg={this.props.msg}
                  productsList={this.props.products}
                  compniesList={this.props.buyerCompniesList}
                  currentLanguage={this.props.currentLanguage}
                  downloadShipmentProduct={this.props.downloadShipmentProduct}
                  downloadShipmentCompany={this.props.downloadShipmentCompany}
                
                />
              </Modal>

              <Modal
                isOpen={this.props.isCertUpload}
                style={customStyles}
                ariaHideApp={false}
              >
                <CertModal
                  owner={this.state.owner}
                  role={this.state.role}
                  handleCertModal={this.handleCertModal}
                  getText={getText}
                  activeCertModal={this.state.activeCertModal}
                  uploadFile={this.props.uploadFile}
                  uploadCertFile={this.props.uploadCertFile}
                  updateView={this.props.fetchShipment}
                  viewToBeUpdated={'outgoingShipment'}
                  data={this.props.outgoingShipment}
                  isUploaded={this.props.isUploaded}
                  msg={this.props.msg}
                  productsList={this.props.products}
                  compniesList={this.props.buyerCompniesList}
                  certShipmentId={this.state.certShipmentId}
                  viewQaDetails={this.props.viewQaDetails}
                  qaData={this.props.qaData}
                  setAlertMeassage={this.props.setAlertMeassage}
                  currentLanguage={this.props.currentLanguage}
                />
              </Modal>

              <Modal
              isOpen={this.props.isExport}
              style={customStyles}
              ariaHideApp={false}
              >
                <ExportModal 
                exportShipment={this.exportShipment}
                currentLanguage={this.props.currentLanguage}
                setAlertMeassage={this.props.setAlertMeassage}
                uploadExportRebateDocument = {this.props.uploadExportRebateDocument}
                exportModalDetail = {this.state.exportModalDetail}
                getExportRebateDocument = {this.props.getExportRebateDocument}
                billFile={this.setDataForTable(this.props.billFile)}
                invoiceFile={this.setDataForTable(this.props.invoiceFile)}
                downloadDocument={this.props.downloadExportRebateDocument}
                deleteDocument={this.props.deleteExportRebateDocument}
                fetching={this.props.fetching}
                onSubmit={this.submitExportRebate}
                // resetExport={this.props.resetExport}
                />
              </Modal>

              <Modal
                isOpen={this.props.isDecline}
                style={customStyles}
                ariaHideApp={false}
              >
                <DeclineModal
                  onSubmit={this.cancelShipment}
                  handleDeclineModal={this.handleDeclineModal}
                  openDeclineModalFor={this.state.openDeclineModalFor}
                  currentShipmentId={this.state.currentShipmentId}
                  btnType={this.state.btnType}
                  viewShipment={this.props.viewShipment}
                  viewShipmentDetail={this.props.viewShipmentDetail}
                  currentLanguage={this.props.currentLanguage}
                />
              </Modal>

              <Modal
                isOpen={this.props.isReceive}
                style={customStyles}
                ariaHideApp={false}
              >
                <ReceiveModal
                  onSubmit={this.receiveShipment}
                  handlereceiveShipment={this.handlereceiveShipment}
                  viewShipment={this.viewShipment}
                  currentShipmentId={this.state.currentShipmentId}
                  viewShipmentDetail={this.props.viewShipmentDetail}
                  updateReceiveShipmentQty={this.props.updateReceiveShipmentQty}
                  pagination={false}
                  search={false}
                  exportCSV={false}
                  options={options}
                  isForModal={false}
                  outgoingFormatter={this.outgoingFormatter}
                  setActivePage={this.setActivePage}
                  totalRecordCount={this.props.totalRecordCount}
                  pages={this.props.pages}
                  currentLanguage={this.props.currentLanguage}
                  resetAlertBox={this.props.resetAlertBox}
                />
              </Modal>


              <Modal
                isOpen={this.props.isPermission}
                style={customStyles}
                ariaHideApp={false}
              >
                <PermissionModal
                  onSubmit={this.placeShipmentAfter}
                  handlePermissionModal={this.handlePermissionModal}
                  currentLanguage={this.props.currentLanguage}
                />
              </Modal>
              <Modal
                isOpen={this.props.showProductAfterLink}
                style={customStyles}
                ariaHideApp={false}
              >
                <LinkAutoOrderModal
                  onSubmit={this.autoOrderPermission}
                  viewShipment={this.props.viewShipment}
                  handleAutoOrderModal={this.handleAutoOrderModal}
                  handlePermissionModal={this.handlePermissionModal}
                  handleLinkModal={this.handleLinkModal}
                  generateAutoOrder={this.props.generateAutoOrder}
                  viewShipmentDetail={this.props.viewShipmentDetail}
                  placeShipment={this.placeShipment}
                  currentLanguage={this.props.currentLanguage}
                />
              </Modal>
              {(this.props.pdfStatus === true) ?
                <PdfModal
                  viewShipmentDetail={this.props.viewShipmentDetail}
                  resetParentState={this.resetParentState}
                  resetProps={this.props.resetProps}
                  setPdfStatus={this.props.setPdfStatus}
                  setCertLogin={this.props.setCertLogin}
                  certLogin={this.props.certLogin}
                  getCertificateNumber={this.props.getCertificateNumber}
                  certificateNumber={this.props.certificateNumber}
                  fibreData={this.props.fibreData}
                /> : ''
              }


              {(this.props.pdfReceiveStatus === true) ?
                <PdfReceiveModal
                  viewShipmentDetail={this.props.viewShipmentDetail}
                  resetParentState={this.resetParentState}
                  resetProps={this.props.resetProps}
                  setPdfReceiveStatus={this.props.setPdfReceiveStatus}
                  setCertLogin={this.props.setCertLogin}
                  certLogin={this.props.certLogin}
                  getCertificateNumber={this.props.getCertificateNumber}
                  certificateNumber={this.props.certificateNumber}
                  fibreData={this.props.fibreData}
                /> : ''
              }
              {(!this.props.isShipmentFormOpen) ?
                <div>
                  {(this.state.outboxShipment) ?
                    <OutgoingShipments
                      outgoingShipment={this.props.outgoingShipment}
                      pagination={false}
                      search={false}
                      exportCSV={false}
                      options={options}
                      isForModal={false}
                      outgoingFormatter={this.outgoingFormatter}
                      setActivePage={this.setActivePage}
                      totalRecordCount={this.props.totalRecordCount}
                      pages={this.props.pages}
                      searchText={this.state.searchText}
                      fetchShipment={this.props.fetchShipment}
                      handleFilterSearch={this.handleFilterSearch}
                      handleFilterDateChange={this.handleFilterDateChange}
                      setCurrentNewPage={this.props.setCurrentNewPage}
                      currentNewPage={this.props.currentNewPage}
                      currentLanguage={this.props.currentLanguage}
                      filterfromDt = {this.state.filterfromDt}
                      filtertoDt = {this.state.filtertoDt}

                    />
                    :
                    <IncomingShipment
                      incomingShipment={this.props.incomingShipment}
                      pagination={false}
                      search={false}
                      exportCSV={false}
                      options={options}
                      isForModal={false}
                      incomingFormatter={this.incomingFormatter}
                      setActivePage={this.setActivePage}
                      totalRecordCount={this.props.totalRecordCount}
                      pages={this.props.pages}
                      searchText={this.state.searchText}
                      fetchShipment={this.props.fetchShipment}
                      handleFilterSearch={this.handleFilterSearch}
                      handleFilterDateChange={this.handleFilterDateChange}
                      setCurrentNewPage={this.props.setCurrentNewPage}
                      currentNewPage={this.props.currentNewPage}
                      currentLanguage={this.props.currentLanguage}
                      filterfromDt = {this.state.filterfromDt}
                      filtertoDt = {this.state.filtertoDt}

                    />
                  }
                  <BtnGrp
                    callFor={this.state.callFor}
                    role={this.state.role}
                    openShipmentForm={this.openShipmentForm}
                    handleCsvModal={this.handleCsvModal}
                    downloadShipmentList={this.downloadShipmentList}
                  />
                </div>
                :
                <div>
                  {
                    (!this.props.jobStatus) &&
                    <div>
                      {(!this.props.isReviewShipment) ?
                        <CreateShipment
                          onSubmit={this.createShipments}
                          role={this.state.role}
                          handleEditChange={this.handleEditChange}
                          handleLinkModal={this.handleLinkModal}
                          isAddProduct={this.props.isAddProduct}
                          isEdit={this.props.isEdit}
                          addProduct={this.addProduct}
                          setProps={this.props.setProps}
                          fetchBuyerUnit={this.props.fetchBuyerUnit}
                          fetchSellerUnit={this.props.fetchSellerUnit}
                          buyerCompniesList={this.props.buyerCompniesList}
                          buyerUnitList={this.props.buyerUnitList}
                          sellerUnitList={this.props.sellerUnitList}
                          fetchProducts={this.props.fetchProducts}
                          products={this.props.products}
                          addProducts={this.props.addProducts}
                          addedProductDetail={this.props.addedProductDetail}
                          viewShipment={this.props.viewShipment}
                          viewShipmentDetail={this.props.viewShipmentDetail}
                          setCurrentShipmentId={this.setCurrentShipmentId}
                          options={options}
                          resetProducts={this.props.resetProducts}
                          isProductEdit={this.props.isProductEdit}
                          deleteShipment={this.props.deleteShipment}
                          fetchShipment={this.props.fetchShipment}
                          isAddLot={this.props.isAddLot}
                          shouldProductFormClose={this.props.shouldProductFormClose}
                          addLot={this.addLot}
                          setCreateFormState={this.props.setCreateFormState}
                          viewShipmentProductDetail={this.props.viewShipmentProductDetail}
                          viewLotDetail={this.props.viewLotDetail}
                          deleProductLot={this.props.deleProductLot}
                          resetAlertBox={this.props.resetAlertBox}
                          fetchUOM={this.props.fetchUOM}
                          uom={this.props.uom}
                          setActivePage={this.setActivePage}
                          showProductAfterLink={this.state.showProductAfterLink}
                          companyId={this.state.user.userInfo.company_type_id}
                          fetchBuyer={this.props.fetchBuyer}
                          getLotDetail={this.props.getLotDetail}
                          viewCompanyInfo={this.props.viewCompanyInfo}
                          companyRole={this.props.companyRole}
                          viewCompanyDetail={this.props.viewCompanyDetail}
                          fetchCompanyRole={this.props.fetchCompanyRole}
                          currentLanguage={this.props.currentLanguage}
                          isEditTextVal={this.props.isEditTextVal}
                          isEditText={this.props.isEditText}
                          companyNameExt={this.props.companyNameExt}
                          extVendorCheck={this.props.extVendorCheck}
                          checkForExtBuyer={this.props.checkForExtBuyer}
                          isExtBuyer={this.props.isExtBuyer}
                          isEditVendor={this.props.isEditVendor}
                          isEditVendorVal={this.props.isEditVendorVal}
                          isExtNumberFunc={this.props.isExtNumberFunc}
                          isProductCreated={this.state.isProductCreated}
                          isExtNumber={this.props.isExtNumber}
                          confirmLot={this.confirmLot}
                          getPulpCompany={this.props.getPulpCompany}
                          companyDropPulpInfo={this.props.companyDropPulpInfo}
                        />
                        :
                        <ReviewShipment
                          role={this.state.role}
                          buyerCompniesList={this.props.buyerCompniesList}
                          buyerUnitList={this.props.buyerUnitList}
                          sellerUnitList={this.props.sellerUnitList}
                          options={options}
                          viewShipmentDetail={this.props.viewShipmentDetail}
                          resetParentState={this.resetParentState}
                          resetProps={this.props.resetProps}
                          isViewIncomingShipment={this.state.isViewIncomingShipment}
                          setActivePage={this.setActivePage}
                          fetchShipmentLotDetail={this.props.fetchShipmentLotDetail}
                          viewLotDetail={this.props.viewLotDetail}
                          reviewShipmentCallFor={this.state.reviewShipmentCallFor}
                          reviewIncommingshipment={this.state.reviewIncommingshipment}
                          isPlaced={this.state.isPlaced}
                          outboxShipment={this.state.outboxShipment}
                          currentLanguage={this.props.currentLanguage}
                          qaData={this.props.qaData}
                          incomingFormatter={this.incomingFormatter}
                          qaFormatter={this.qaFormatter}
                          qaDelete={this.qaDelete}
                          buyerList={this.state.buyerList}
                          viewExternalShipment={this.props.viewExternalShipment}
                          externalView={this.props.externalView}
                          vendorFormatter={this.vendorFormatter}
                          extVendorCheck={this.props.extVendorCheck}
                          isEditText={this.props.isEditText}
                        />
                      }
                    </div>
                  }

                  <div>
                    {this.props.jobStatus &&
                      <ExternalJob
                        onSubmit={this.createExternal}
                        setActivePage={this.setActivePage}
                        currentLanguage={this.props.currentLanguage}
                        setCurrentLanguage={this.props.setCurrentLanguage}
                        currentLanguage={this.props.currentLanguage}
                        setJobActive={this.props.setJobActive}
                        resetJobActive={this.props.resetJobActive}
                        externalJobStatus={this.props.externalJobStatus}
                        role={this.state.role}
                        handleEditChange={this.handleEditChange}
                        handleLinkModal={this.handleLinkModal}
                        isAddProduct={this.props.isAddProduct}
                        isEdit={this.props.isEdit}
                        addProduct={this.addProduct}
                        setProps={this.props.setProps}
                        fetchBuyerUnit={this.props.fetchBuyerUnit}
                        fetchSellerUnit={this.props.fetchSellerUnit}
                        buyerCompniesList={this.props.buyerCompniesList}
                        buyerUnitList={this.props.buyerUnitList}
                        sellerUnitList={this.props.sellerUnitList}
                        fetchProducts={this.props.fetchProducts}
                        products={this.props.products}
                        addProducts={this.props.addProducts}
                        addedProductDetail={this.props.addedProductDetail}
                        viewShipment={this.props.viewShipment}
                        viewShipmentDetail={this.props.viewShipmentDetail}
                        setCurrentShipmentId={this.setCurrentShipmentId}
                        options={options}
                        resetProducts={this.props.resetProducts}
                        isProductEdit={this.props.isProductEdit}
                        deleteShipment={this.props.deleteShipment}
                        fetchShipment={this.props.fetchShipment}
                        isAddLot={this.props.isAddLot}
                        shouldProductFormClose={this.props.shouldProductFormClose}
                        addLot={this.addLot}
                        setCreateFormState={this.props.setCreateFormState}
                        viewShipmentProductDetail={this.props.viewShipmentProductDetail}
                        viewLotDetail={this.props.viewLotDetail}
                        deleProductLot={this.props.deleProductLot}
                        resetAlertBox={this.props.resetAlertBox}
                        fetchUOM={this.props.fetchUOM}
                        uom={this.props.uom}
                        setActivePage={this.setActivePage}
                        showProductAfterLink={this.state.showProductAfterLink}
                        companyId={this.state.user.userInfo.company_type_id}
                        fetchBuyer={this.props.fetchBuyer}
                        getLotDetail={this.props.getLotDetail}
                        viewCompanyInfo={this.props.viewCompanyInfo}
                        companyRole={this.props.companyRole}
                        viewCompanyDetail={this.props.viewCompanyDetail}
                        fetchCompanyRole={this.props.fetchCompanyRole}
                        currentLanguage={this.props.currentLanguage}

                        setProductStatus={this.props.setProductStatus}
                        isProduct={this.props.isProduct}
                        external_shipment_id={this.state.external_shipment_id}
                        createExternalJob={this.props.createExternalJob}
                        externalData={this.props.externalData}
                        addExternalProduct={this.props.addExternalProduct}
                        viewExternalProduct={this.props.viewExternalProduct}
                        productDetail={this.props.productDetail}
                        onDeleteExtProd={this.props.onDeleteExtProd}
                        onUpdateExtProd={this.props.onUpdateExtProd}
                        uniqueExternalView={this.props.uniqueExternalView}
                        onUpdateExtVendor={this.props.onUpdateExtVendor}
                        ext_vendor_shipment_id={this.state.ext_vendor_shipment_id}
                        resestViewUniqueExternalShipment={this.props.resestViewUniqueExternalShipment}
                        viewUniqueExternalProduct={this.props.viewUniqueExternalProduct}
                        uniqueProductView={this.props.uniqueProductView}
                        resetUniqueProductDetail={this.props.resetUniqueProductDetail}
                        resetExternalData={this.props.resetExternalData}
                        back={this.back}
                        fetchProductsExt={this.props.fetchProductsExt}
                        productsExt={this.props.productsExt}
                      />
                    }


                  </div>
                  <div className='btn-right'>
                    {/* {(this.props.externalData !== "" || this.props.uniqueExternalView.vendors !== undefined) ?   */}
                    {(!this.props.productDetail.Products) && <Button className={(this.props.isReviewShipment) ? "btn btn-primary reviewReview" : "btn btn-primary  create non-cancel"} onClick={this.back}>{strings.scan.back}</Button>}
                    {(this.props.isReviewShipment && this.state.isPlaced === 0) && <Button className={(this.props.isReviewShipment) ? "btn btn-primary reviewReview" : "btn btn-primary  create non-cancel"} onClick={() => this.handleCertModal(true, 'upload')}>{strings.outgoingFormatterText.upload}</Button>}
                    {(this.props.isReviewShipment && this.state.isPlaced === 0) && <Button className={(this.props.isReviewShipment) ? "btn btn-primary reviewReview" : "btn btn-primary  create non-cancel"} onClick={this.edit}>{strings.captalise.edit}</Button>}
                    {(this.props.isReviewShipment && this.state.isPlaced === 0) && <Button className={(this.props.isReviewShipment) ? "btn btn-primary reviewCancel" : "btn btn-primary  create cancel"} onClick={this.delete}>{strings.captalise.delete}</Button>}
                    {(this.props.isReviewShipment && this.state.isPlaced === 0) && <Button className={(this.props.isReviewShipment) ? "btn btn-primary reviewReview" : "btn btn-primary  create non-cancel"} onClick={() => this.placeShipment({}, true)}>{strings.captalise.ship}</Button>}
                    {((this.props.isReviewShipment) && this.state.isPlaced === 0 && (this.state.user.role === "Garment Manufacturer")) && <Button className={(this.props.isReviewShipment) ? "btn btn-primary reviewReview" : "btn btn-primary  create non-cancel"} style={{ marginRight: '-3%' }} onClick={this.externalJob}>{strings.vendor.externalVendor}</Button>}
                  </div>
                </div>
              }
              <div className="clearfix"></div>
              <Footer
                currentLanguage={this.props.currentLanguage}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Shipments;