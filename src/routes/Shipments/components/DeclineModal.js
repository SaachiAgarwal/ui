import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import _ from 'lodash';
import FileDownload from 'js-file-download';

import SubmitButtons from 'components/SubmitButtons';
import RenderTextArea from 'components/RenderTextArea';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);


export const validate = (values) => {
  const errors = {}
  if (!values.message) {
    errors.message = 'Required'
  }
  return errors
}

export const CsvModalHeader = (props) => {
  const buttons = ['declineBtn', 'cancelBtn'];
  return (
    <div className="headertext">
      {(!_.isEmpty(props.viewShipmentDetail)) &&
        <div>
          {(buttons.indexOf(props.btnType) > -1) ?
            <div>
              <h2 className="heading-model-new">{(props.btnType === 'declineBtn') ? strings.declineModal.declineShipment : strings.declineModal.cancelledShipment}</h2>
              <h3 className="text1">{(props.btnType === 'declineBtn') ? strings.declineModal.declineReasonShipment : strings.declineModal.cancelReasonShipment}</h3>
            </div>
            :
            <div>
              <h2 className="heading-model-new">{(props.viewShipmentDetail.shipmentDetail.is_cancelled === 0) ? strings.declineModal.rejectionReason : strings.declineModal.cancellationReason}</h2>
            </div>
          }
          <Button className="modal-btn" onClick={() => props.handleDeclineModal({}, false, props.btnType)}>X</Button>
        </div>
      }
    </div>
  );
}

/** LinkModal
 *
 * @description This class having a modal that will display a list of incoming orders and having a filter on that list
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class DeclineModal extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.viewShipment(this.props.currentShipmentId);
  }

  componentWillUnmount() {
    this.props.handleDeclineModal({}, false, this.props.btnType);
  }

  render() {
    strings.setLanguage(this.props.currentLanguage);
    const { handleSubmit, onChange, viewShipmentDetail, btnType } = this.props;
    const { status, rejected, date, reason, cancelled, declineShipment, cancelledShipment } = strings.declineModal;
    return (
      <div className="model_popup">
        <div>
          <CsvModalHeader
            handleDeclineModal={this.props.handleDeclineModal}
            openDeclineModalFor={this.props.openDeclineModalFor}
            btnType={btnType}
            viewShipmentDetail={viewShipmentDetail}
          />
        </div>
        <div>
          {(this.props.openDeclineModalFor !== 'reason') ?
            <form onSubmit={handleSubmit}>
              <div>
                <Field
                  name='message'
                  className='form-control'
                  component={RenderTextArea}
                  placeholder={"Type here..."}
                />
              </div>
              <div>
                { (btnType === 'declineBtn') ?
                  <SubmitButtons
                    submitLabel={declineShipment}
                    className='btn btn-primary create'
                    submitting={this.props.submitting}
                  />
                  :
                  <SubmitButtons
                  submitLabel={cancelledShipment}
                  className='btn btn-primary create'
                  submitting={this.props.submitting}
                />
                }
              </div>
            </form>
            :
            <div>
              {(!_.isEmpty(viewShipmentDetail)) &&
                <div>
                  <p>{status}: {(viewShipmentDetail.shipmentDetail.is_cancelled === 0) ? rejected : cancelled}</p>
                  <p>{date}: {(viewShipmentDetail.shipmentDetail.cancellation_date !== undefined && viewShipmentDetail.shipmentDetail.cancellation_date !== null) ? viewShipmentDetail.shipmentDetail.cancellation_date.toString().split('T')[0] : (viewShipmentDetail.shipmentDetail.rejected_date !== undefined && viewShipmentDetail.shipmentDetail.rejected_date !== null) && viewShipmentDetail.shipmentDetail.rejected_date.toString().split('T')[0]}</p>
                  <p>{reason}: {(viewShipmentDetail.shipmentDetail.cancellation_reason !== undefined && viewShipmentDetail.shipmentDetail.cancellation_reason !== null) ? viewShipmentDetail.shipmentDetail.cancellation_reason : (viewShipmentDetail.shipmentDetail.rejected_reason !== undefined && viewShipmentDetail.shipmentDetail.rejected_reason !== null) && viewShipmentDetail.shipmentDetail.rejected_reason}</p>
                </div>
              }
            </div>
          }
        </div>
      </div>
    );
  }
}

export default reduxForm({
  form: 'DeclineModal',
  validate,
})(DeclineModal)