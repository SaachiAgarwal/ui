import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button } from "reactstrap";
import { reduxForm, Field } from "redux-form";
import SubmitButtons from "components/SubmitButtons";
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
// import RenderField from "components/RenderField";
import { getLocalStorage, getBuyerCompanyKeyId, getDate, getText, saveLocalStorage, getForestId } from 'components/Helper';

//localization
import LocalizedStrings from "react-localization";
import data from "../../../localization/data";

let strings = new LocalizedStrings(data);


const columnHover = (cell, row, enumObject, rowIndex) => {
    return cell
  }

export default class ExportTable extends Component{
    constructor(props) {
        super(props);
      }


      render() {
          return (
            <div className='ordertable'>
            <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options}>
                <TableHeaderColumn dataField='shipment_id' hidden={true}>shipment_id</TableHeaderColumn>
                <TableHeaderColumn dataField='created_date' hidden={true} >created_date</TableHeaderColumn>
                <TableHeaderColumn dataField='document_file_name' hidden={true} isKey={true} autoValue={true} >bill_file_name</TableHeaderColumn>
                <TableHeaderColumn dataField='document_name' tooltip={true} columnTitle={columnHover}>{this.props.type === 'bill' ? 'Bill Name' : 'Invoice Name'}</TableHeaderColumn>
                <TableHeaderColumn dataField='export_rebate_id' hidden={true}>export_rebate_id</TableHeaderColumn>
                <TableHeaderColumn key="editAction" dataFormat= {this.props.uploadFormatter}>Action</TableHeaderColumn>
            </BootstrapTable>
          </div>
          )
      }
}