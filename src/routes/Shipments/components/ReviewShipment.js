import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';

import SubmitButtons from 'components/SubmitButtons';
import { Images } from 'config/Config';
import { getBuyerCompanyKeyId, getUnit, getLocalStorage } from 'components/Helper';
import ViewShipmentTable from './ViewShipmentTable';
import ViewShipmentOutTable from './ViewShipmentOutTable'
import AddLotsTable from './AddLotsTable';
import { debuglog } from 'util';
import QaUploadTable from './QaUploadTable'
import ExternalViewTable from './ExternalViewTable'

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);


/** func renderLink
 *
 * @description This method returns a field object having a span to achieve onClick functionality. Simply we can not use
 *   onClick on field wrapper.
 *
 * return A component
 */
const renderLink = ({ input, label, type, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className="btn-bs-file">
      <span className='form-control'>
        <p>{customProps.value}</p>
        {(input.name === 'linkTo') &&
          <span>
            <img src={Images.fileUploadIcon} alt="" data-toggle="modal" data-target="#myModal" />
          </span>
        }
      </span>
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

/** CreateOrder
 *
 * @description This class is responsible to display a form to create order
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class ReviewShipment extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      initData: {},
      qaData: {},
      isVendor: 0
    }

  }

  async componentWillMount() {
    let lStorage = getLocalStorage('shipments');
    if (this.props.reviewShipmentCallFor === 'incomingShipment') {
      this.props.setActivePage('inboxReviewShipment');
    } else {
      if (this.props.isPlaced) {
        this.props.setActivePage('placedReviewShipment');
      } else {
        this.props.setActivePage('outboxReviewShipment');
      }
    }
    this.props.fetchShipmentLotDetail(lStorage.shipment_id);
    await this.props.viewExternalShipment(lStorage.shipment_id);
    // await console.log(this.props.externalView);
  }

  componentDidMount() {

    this.handleInitialize();
    console.log(this.props.viewShipmentDetail);

    if (!_.isEmpty(this.props.extVendorCheck.data)) {
      console.log(this.props.extVendorCheck.data.is_external)
      this.setState({
        isVendor: this.props.extVendorCheck.data.is_external
      })
    }

    //this.handleInitializeQa();
  }

  componentWillUnmount() {
    this.props.resetParentState();
    this.props.resetProps();
  }

  handleInitialize() {
    let loginData = getLocalStorage('loginDetail')
    let buyer_company_key_name;
    if (!_.isEmpty(this.props.viewShipmentDetail)) {
      if(loginData.role !== "Brand"){
        
        if (this.props.buyerCompniesList.length !== 0) {
          buyer_company_key_name = getBuyerCompanyKeyId(this.props.buyerCompniesList, this.props.viewShipmentDetail.shipmentDetail.buyer_company_key_id, true);
        }
        else {
          buyer_company_key_name = getBuyerCompanyKeyId(this.props.buyerList, this.props.viewShipmentDetail.shipmentDetail.buyer_company_key_id, true);
        }
      }
      let buyerUnit = getUnit(this.props.buyerUnitList, this.props.viewShipmentDetail.shipmentDetail.buyer_company_bu_id);
      let sellerUnit = getUnit(this.props.sellerUnitList, this.props.viewShipmentDetail.shipmentDetail.seller_company_bu_id);
      let linked_forest_name = this.props.viewShipmentDetail.shipmentDetail.linked_forest_name || ''
      const initData = {
        shipment_id: this.props.viewShipmentDetail.shipmentDetail.shipment_id,
        invoice_number: this.props.viewShipmentDetail.shipmentDetail.invoice_number,
        buyer_company_key_name: (!this.props.isViewIncomingShipment) ? buyer_company_key_name : this.props.viewShipmentDetail.shipmentDetail.seller_company_name,
        buyer_company_bu_id: (!this.props.isViewIncomingShipment) ? buyerUnit : this.props.viewShipmentDetail.shipmentDetail.buyer_company_bu_name,
        seller_company_bu_id: (!this.props.isViewIncomingShipment) ? sellerUnit : this.props.viewShipmentDetail.shipmentDetail.seller_company_bu_name,
        linkTo: this.props.viewShipmentDetail.shipmentDetail.order_number,
        invoice_date: this.props.viewShipmentDetail.shipmentDetail.new_invoice_date,
        shipment_description: this.props.viewShipmentDetail.shipmentDetail.shipment_description,
        shipment_transport_mode: this.props.viewShipmentDetail.shipmentDetail.shipment_transport_mode,
        products: this.props.viewShipmentDetail.products,
        buyer_company_name: this.props.viewShipmentDetail.shipmentDetail.buyer_company_name,
        shipment_text: this.props.viewShipmentDetail.shipmentDetail.order_number,
        linked_forest_name: linked_forest_name
      };
      initData.products.map((item, index) => {
        initData.products[index].index = index + 1;
        initData.products[index].shipment_id = initData.shipment_id;
      });
      this.setState({ initData: initData });
      this.props.initialize(initData);
    }
  }
  async handleInitializeQa() {
    // console.log(this.props.qaData.documents)
    let qaData = this.props.qaData.documents
    qaData.map((item, index) => {
      if (item.is_approved === 1) {
        item.is_approved = "YES"
      }
      else if (item.is_approved === 0) {
        item.is_approved = "NO"
      }
    })
    // console.log(qaData)
    await this.setState({
      qaData: qaData
    })

    // console.log(qaData)

  }



  render() {

    const { handleSubmit, currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);
    let loginData = getLocalStorage('loginDetail'),
      dispbutn = ((loginData.role === "Spinner" && this.props.reviewIncommingshipment === null)) ? false : (loginData.role === "Birla Cellulose" || (loginData.role === "Spinner" && this.props.reviewIncommingshipment === "incomingShipment") || loginData.role === "Pulp") ? true : false;
    return (
      <div className='col-md-12 col-lg-12 create-order'>
        <h2>{strings.createShipment.shipmentDetail}</h2>
        <div>
          <form onSubmit={handleSubmit}>
            <div className="form-row">
              <div className="form-group col-md-4">
                <label>{strings.createShipment.invoiceNumber}</label>
                <Field
                  name='invoice_number'
                  type="text"
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.invoice_number : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <label>{strings.createShipment.invoiceDate}</label>
                <Field
                  className="select-menu"
                  name="invoice_date"
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.invoice_date : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4 ">
                <label>{(this.props.isViewIncomingShipment) ? `${strings.shipmentTableText.supplier}` : `${strings.shipmentTableText.buyer}`}</label>
                <Field
                  name='buyer_company_key_name'
                  type='text'
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData.buyer_company_key_name)) ? this.state.initData.buyer_company_key_name : this.state.initData.buyer_company_name
                  }}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <label>{strings.createShipment.sellerUnit}</label>
                <Field
                  name='seller_company_bu_id'
                  type='date'
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.seller_company_bu_id : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <label>{strings.createShipment.buyerUnit}</label>
                <Field
                  name='buyer_company_bu_id'
                  type='date'
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.buyer_company_bu_id : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <label>{strings.createShipment.transportMode}</label>
                <Field
                  name='shipment_transport_mode'
                  type='date'
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.shipment_transport_mode : ''
                  }}
                />
              </div>
            </div>
            {(this.state.initData.linked_forest_name ) &&
            <div className="form-row">
                <div className="form-group col-md-4">
                <label>Linked Forest*</label>
                  <Field
                    className="select-menu"
                    name="linked_forest_name"
                    
                    component={renderLink}
                    customProps={{
                      value: (!_.isEmpty(this.state.initData)) ? this.state.initData.linked_forest_name : ''
                    }}
                  />
                </div>
               

              </div>
                }
            <div className="form-row">
              <div className="form-group col-md-8">
                <label>{strings.createShipment.shipmentDescription}</label>
                <Field
                  name='shipment_description'
                  type="text"
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.shipment_description : ''
                  }}
                />
              </div>
              {(this.props.role !== 'Brand') && !this.props.isEditText && this.state.isVendor === 0 &&
                <div className="form-group col-md-4 ">
                  <label>{strings.createOrder.linkBuyer}</label>
                  <Field
                    name='linkTo'
                    type='text'
                    component={renderLink}
                    customProps={{
                      value: (!_.isEmpty(this.state.initData)) ? this.state.initData.linkTo : ''
                    }}
                  />
                </div>
              }

              {((this.props.isEditText) || (this.state.isVendor === 1)) &&
                <div className="form-group col-md-4">
                  <label>External Purchase Order</label>
                  <Field
                    name='shipment_text'
                    type="text"
                    component={renderLink}
                    customProps={{
                      value: (!_.isEmpty(this.state.initData)) ? this.state.initData.shipment_text : ''
                    }}
                  />
                </div>
              }
            </div>
            {(!_.isEmpty(this.state.initData.products)) &&
              <div className="col-md-12 nopad">
                <h2>{strings.addProduct.productDetail}</h2>
                {this.props.outboxShipment ?
                  <ViewShipmentOutTable
                    data={this.state.initData.products}
                    pagination={false}
                    search={false}
                    exportCSV={false}
                    options={this.props.options}
                    isViewIncomingShipment={this.props.isViewIncomingShipment}
                    role={this.props.role}
                    disptable={dispbutn}
                    currentLanguage={this.props.currentLanguage}
                  />
                  :
                  <ViewShipmentTable
                    data={this.state.initData.products}
                    pagination={false}
                    search={false}
                    exportCSV={false}
                    options={this.props.options}
                    isViewIncomingShipment={this.props.isViewIncomingShipment}
                    role={this.props.role}
                    disptable={dispbutn}
                    currentLanguage={this.props.currentLanguage}
                  />
                }
              </div>
            }
            {(!_.isEmpty(this.props.viewLotDetail) && !this.props.isViewIncomingShipment) && //when reviewing outgoing shipment
              <div>
                <h2>{strings.shipmentTableText.lotDetail}</h2>
                <AddLotsTable
                  data={this.props.viewLotDetail}
                  pagination={false}
                  search={false}
                  exportCSV={false}
                  options={this.props.options}
                  haveAction={false}
                  display={dispbutn}
                  loginData={loginData}
                  currentLanguage={this.props.currentLanguage}
                />
              </div>
            }

            {(!_.isEmpty(this.props.qaData))&& (this.props.qaData.documents.length !== 0) &&
              <div>
                <h2>{strings.qa.qaUploadDoc}</h2>
                {this.props.qaData.documents.map((item, index) => {
                  this.props.qaData.documents[index].index = index + 1;
                  if (item.is_approved === 1) {
                    item.is_approved = "Approved"
                  }
                  else if (item.is_approved === 0) {
                    item.is_approved = "Rejected"
                  }
                  else if (item.is_approved === null) {
                    item.is_approved = "Not Responded"
                  }
                }) &&
                  <QaUploadTable
                    data={this.props.qaData.documents}
                    pagination={false}
                    search={false}
                    exportCSV={false}
                    options={this.props.options}
                    haveAction={false}
                    display={dispbutn}
                    loginData={loginData}
                    currentLanguage={this.props.currentLanguage}
                    qaFormatter={this.props.qaFormatter}
                    incomingFormatter={this.props.incomingFormatter}
                    qaDelete={this.props.qaDelete}
                  />
                }
              </div>

            }
            {
              this.props.externalView.documents &&
              <div>
                <h2>{strings.vendor.vendorDetail}</h2>
                {this.props.externalView.documents.map((a, index) => {
                  var spdate = (a.shipment_date.split("T"))[0].split("-");
                  a.shipment_date = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
                }) &&
                  <ExternalViewTable
                    data={this.props.externalView.documents}
                    pagination={false}
                    search={false}
                    exportCSV={false}
                    options={this.props.options}
                    haveAction={false}
                    display={dispbutn}
                    loginData={loginData}
                    currentLanguage={this.props.currentLanguage}
                    vendorFormatter={this.props.vendorFormatter}
                  // qaFormatter={this.props.qaFormatter}
                  // incomingFormatter={this.props.incomingFormatter}
                  // qaDelete={this.props.qaDelete}
                  />

                }
              </div>
            }
          </form>
        </div>
      </div>
    );
  }
}

ReviewShipment.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
};

export default reduxForm({
  form: 'ReviewShipment'
})(ReviewShipment)