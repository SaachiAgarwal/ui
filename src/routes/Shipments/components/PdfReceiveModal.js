import React, { Component } from 'react'
import html2canvas from 'html2canvas'
import jspdf from 'jspdf'
import { Images } from 'config/Config';

export default class PdfModal extends Component {
    constructor(props) {
        super(props);
        //this.getData = this.getData.bind(this)
        this.getNewData = this.getNewData.bind(this)

        this.state = {
            value: 10
        }
    }

    printDocument() {
        const input = document.getElementById('divToPrint');
        html2canvas(input)
            .then((canvas) => {
                const imgData = canvas.toDataURL('image/png');
                const pdf = new jspdf();
                pdf.addImage(imgData, 'JPEG', 0, 0);
                // pdf.output('dataurlnewwindow');
                pdf.save("download.pdf");
            });
    }
    componentDidMount() {
        // console.log(this.props.viewShipmentDetail)
        // console.log(this.props.certLogin.role)
        this.getNewData()
        this.props.setPdfReceiveStatus(false);
    }
    componentWillUnmount() {

        // this.props.resetParentState();
        this.props.resetProps();
        this.props.setCertLogin('');

    }

    // async getData(){
    //     const input = document.getElementById('divToPrint');
    //     input.style.visibility = "visible"
    //     await html2canvas(input)
    //         .then((canvas) => {
    //             const imgData = canvas.toDataURL('image/png');
    //             const pdf = new jspdf();
    //             pdf.addImage(imgData, 'JPEG', 0, 0);
    //             // pdf.output('dataurlnewwindow');
    //             pdf.save("download.pdf");
    //         });
    //         const new_input = document.getElementById('divToPrint');
    //         new_input.style.visibility = "hidden"
    //         new_input.style.height = "0"
    // }

    getNewData() {
        var doc = new jspdf();
        var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
        var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
        var ship_description = ''
        var loginDetail = ''
        var product_qty = []
        var product_unit = []
        var append_qty = []
        var product_name = []
        var fibre_detail = []
        var i = 0
        var j = 0
        var k = 0
        var m = 0
        var space = 0
        var space1 = 0
        var randomNumber = Math.floor(Math.random() * 1000000);
        var shipment_date = ''
        if (this.props.viewShipmentDetail.shipmentDetail.shipment_date !== null) {
            var new_spdate = (this.props.viewShipmentDetail.shipmentDetail.shipment_date.split("T"))[0].split("-");
            shipment_date = new_spdate[2] + "-" + new_spdate[1] + "-" + new_spdate[0];
        }

        console.log(shipment_date);
        var img1 = new Image()
        img1.src = Images.grasimLIcon
        console.log(img1)
        var img2 = new Image()
        var img3 = new Image()

        var footerImage = new Image()
        footerImage.src = Images.footerImage

        var headerImage = new Image()
        headerImage.src = Images.headerImage

        var certHeader = new Image()
        certHeader.src = Images.certHeader

        var bcAudit = new Image()
        bcAudit.src = Images.bcAudit
        img2.src = Images.birlaLIcon
        img3.src = Images.livaLIcon


        // console.log(this.props.viewShipmentDetail.shipmentDetail.invoice_number)
        // console.log(this.props.viewShipmentDetail.shipmentDetail.new_invoice_date)
        // console.log(this.props.viewShipmentDetail.products[0].product_qty)
        var invoice_number = (`${this.props.viewShipmentDetail.shipmentDetail.invoice_number}`)
        var linked_PO = (`${this.props.viewShipmentDetail.shipmentDetail.order_number}`)
        var new_invoice_date = (`${this.props.viewShipmentDetail.shipmentDetail.new_invoice_date}`)
        var buyer_company_name = (`${this.props.viewShipmentDetail.shipmentDetail.buyer_company_name}`)
        var seller_company_name = (`${this.props.viewShipmentDetail.shipmentDetail.seller_company_name}`)
        var certificateNumber = (`${this.props.certificateNumber.data}`)
        var new_shipment_date = (`${shipment_date}`)
        var randomNumberNew = (`${randomNumber}`)
        //var fibre_detail = (`${this.props.fibreData}`)
        if (this.props.viewShipmentDetail.shipmentDetail.shipment_description) {
            var ship_description = (`${this.props.viewShipmentDetail.shipmentDetail.shipment_description}`)
        }
        else {
            var ship_description = "Nil.."
        }
        this.props.viewShipmentDetail.products.forEach((item, index) => {
            // console.log(item)
            product_qty[i++] = (`${item.product_qty_received}`)
            product_unit[j++] = (`${item.product_uom}`)
            product_name[k++] = (`${item.product_name}`)
        })
        this.props.fibreData.forEach((item, index) => {
            fibre_detail[m++] = (`${this.props.fibreData[index]}`)
        })
        product_qty.forEach((item, index) => {
            append_qty[index] = product_qty[index] + ' ' + product_unit[index]
        })

        var wrapSellerCompany = doc.splitTextToSize(seller_company_name, 120)

        space = (product_name.length * 5)
        space1 = ((fibre_detail.length-1) * 5)
        console.log(space)
        console.log(fibre_detail)
        doc.addImage(headerImage, 'jpg', 0, 0, 230, 70)
        doc.addImage(certHeader, 'jpg', 80, 60, 50, 20)
        doc.addImage(bcAudit, 'jpg', 7, 80, 70, 40)
        doc.setFontSize(12);
        doc.setTextColor(104, 38, 0)
        doc.text("Traceability Certificate Number", 80, 100);

        doc.text("{", 80, 107);
        if (this.props.certLogin.role === "Garment Manufacturer") {
            doc.text("FF", 82, 107);
        }
        else if (this.props.certLogin.role === "Grey Fabric Manufacturer") {
            doc.text("SY", 82, 107);
        }
        else if (this.props.certLogin.role === "Finish Fabric Manufacturer") {
            doc.text("GF", 82, 107);
        }
        else if (this.props.certLogin.role === "Spinner") {
            doc.text("SF", 82, 107);
        }
        else if(this.props.certLogin.role === "Brand"){
            doc.text("GM", 82, 107);
        }
        else if(this.props.certLogin.role === "Forest"){
            doc.text("FW", 82, 107);
        }
        else if(this.props.certLogin.role === "Pulp"){
            doc.text("DP", 82, 107);
        }
        else if(this.props.certLogin.role === "Integrated Player"){
            doc.text("IP", 82, 107);
        }
        
        doc.text("}--{", 89, 107);
        doc.text(new_shipment_date, 95, 107);
        doc.text("}--{", 117, 107);
        doc.text(certificateNumber, 123, 107);
        doc.text("}", 136, 107);
        doc.setTextColor(0, 0, 0)
        doc.setFontSize(10);
     



        doc.setFontType("bold");
        doc.text("Invoice Number", 10, 130)
        doc.text(":", 50,130)
        doc.setFontType("normal")
        doc.text(invoice_number, 52, 130);

        // doc.setFontType("bold");
        // doc.text("Buyer Name :", 88, 130)
        // doc.setFontType("normal")
        // doc.text(buyer_company_name, 113, 130);

        doc.setFontType("bold");
        doc.text("Linked Purchase Order", 10, 135)  
        doc.text(":", 50,135)                        
        doc.setFontType("normal")
        doc.text(linked_PO, 52, 135);

        doc.setFontType("bold");
        doc.text("Date of Invoice", 10, 140)  
        doc.text(":", 50,140)                        
        doc.setFontType("normal")
        doc.text(new_invoice_date, 52, 140);                               

        // doc.setFontType("bold");
        // doc.text("Supplier Partner :", 88, 135)
        // doc.setFontType("normal")
        // doc.text(wrapSellerCompany, 119, 135);


        doc.setFontType("bold");
        doc.text("Buyer's Name", 10, 145)
        doc.text(":", 50,145)
        doc.setFontType("normal")
        doc.text(buyer_company_name, 52, 145);

        doc.setFontType("bold");
        doc.text("Supplier's Name", 10, 150)
        doc.text(":", 50,150)
        doc.setFontType("normal")
        doc.text(seller_company_name, 52, 150);

        doc.setFontType("bold");
        doc.text("Shipment Description", 10, 155)
        doc.text(":", 50,155)
        doc.setFontType("normal")
        doc.text(ship_description, 52, 155);


        doc.setFontType("bold");
        doc.text("Product Composition :", 10, 165)
        doc.setFontType("normal")
        doc.text(fibre_detail, 10, 170);

        doc.setFontType("bold");
        doc.text("Products", 10, (`${180 + space1}`));
        doc.setFontType("normal")
        doc.text(product_name, 10, (`${185 + space1}`));


        doc.setFontType("bold");
        doc.text("Quantity", 170, (`${180 + space1}`))
        doc.setFontType("normal")
        doc.text(append_qty, 170, (`${185 + space1}`));




        doc.text("The blockchain-enabled “Chain of Custody” (CoC) platform – GreenTrack confirms that the aforementioned product is", 10, (`${190 + space + space1}`));
        doc.text("made using", 10, (`${195 + space + space1}`));
        doc.setFontType("bold");
        doc.text("below Fibre (s) ** ",31, (`${195 + space + space1}`) )
        doc.setFontType("normal")
        doc.text("and the material flow from forest till current stage has been mapped and verified for", 61, (`${195 + space + space1}`) )
        doc.text("traceability throughout the supply chain", 10, (`${200 + space + space1}`));

        doc.setFontType("bold");
        doc.text("Fibre** :", 10, (`${210 + space + space1}`))
        doc.setFontType("normal")
        doc.text(fibre_detail, 10, (`${215 + space + space1}`));


        doc.setFontSize(10);
        //doc.text("( This is auto generated certificate from a Block-chain technology based layer. Does not require a signature. )", 10,  (`${140 + space}`));
        // doc.text("", 10, (`${170 + space}`));

        // doc.setFontSize(10);
        // doc.text("Copyright © 2018 Grasim Industries Limited. All rights reserved.", 30, 190);

        // doc.addImage(img1,'png', 140,180,20,20);
        // doc.addImage(img2,'png', 160,180,20,20);
        // doc.addImage(img3,'png', 180,180,20,20);

        doc.addImage(footerImage, 'png', 39, 260, 120, 33)


        doc.save("certificate.pdf");

    }

    render() {
        return (
            <div >


            </div>);
    }
}
