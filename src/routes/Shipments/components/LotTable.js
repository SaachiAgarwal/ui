import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row } from 'reactstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Pagination from 'react-paginate';
import { Images } from 'config/Config';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);


/** LotTable
 *
 * @description This class is responsible to display a orders list of stock cunsumption for lot model
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class LinkingOrderTable extends Component {
  constructor(props) {
    super(props);
    this.handlePageClick = this.handlePageClick.bind(this);
    this.imageFormatter = this.imageFormatter.bind(this);
  }

  static defaultProps = {
    linkOrderOptions: {
      sizePerPage: 5,
      pageSize: 5,
      pageStartIndex: 1,
      paginationSize: 3,
      prePage: 'Prev',
      nextPage: 'Next',
      firstPage: 'First',
      lastPage: 'Last',
      clearSearch: true,
      hideSizePerPage: true,
      onRowClick: this.editFormatter
    }
  }

  handlePageClick(value) {
    this.props.viewStocks(value.selected + 1, this.props.linkOrderOptions.pageSize, this.props.filterText);
  }

  imageFormatter(cell, row) {
    return (
        <img className="pencil-image" src={Images.pencilImage} />
    )
  }

  render() {
    strings.setLanguage(this.props.currentLanguage);
    const {lot, product, quantity, blend, unit, consumedQuantityText} = strings.addProduct;
    return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.linkOrderOptions} cellEdit={{
          mode: 'click', blurToSave: true,
        }}>
          <TableHeaderColumn dataField='stock_lot_id' hidden={true}>Order Id</TableHeaderColumn>
          <TableHeaderColumn dataField='invoice_number' editable={false} columnTitle={true}>{strings.shipmentTableText.invoiceNumber}</TableHeaderColumn>
          <TableHeaderColumn dataField='lot_number' editable={false} columnTitle={true} isKey={true}>{lot}#</TableHeaderColumn>
          <TableHeaderColumn dataField='product_name' editable={false} editable={false} columnTitle={true}>{product}</TableHeaderColumn>
          <TableHeaderColumn dataField='supplier_company_name' columnTitle={true} editable={false}>{strings.shipmentTableText.supplier}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_qty' editable={false} >{quantity}</TableHeaderColumn>
          {/* <TableHeaderColumn dataFormat={this.imageFormatter} editable={false}></TableHeaderColumn> */}
          {(this.props.display) ? false : <TableHeaderColumn dataField='blend_percentage' editable={false}>{blend}</TableHeaderColumn>}
          <TableHeaderColumn dataField='product_uom' editable={false}>{unit}</TableHeaderColumn>
          <TableHeaderColumn key="editAction" dataFormat={this.props.lotFormatter} editable={false}>{strings.shipmentTableText.action}</TableHeaderColumn>
        </BootstrapTable>
        <div className="pagination-box">
          <Pagination
            previousLabel={strings.prev}
            nextLabel={strings.next}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={this.props.pages}
            pageRangeDisplayed={5}
            onPageChange={this.handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        </div>
        <Row>
          <p style={{whiteSpace: "nowrap"}} className="col-md-6 disclaimer">*. {consumedQuantityText}</p>
        </Row>
      </div>
    );
  }
}

LinkingOrderTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  linkOrderOptions: PropTypes.object.isRequired,
};

export default LinkingOrderTable;