import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

//localization
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
let strings = new LocalizedStrings(
  data
);


/** AddShipmentTable
 *
 * @description This class is responsible to display added orderd as outgoing shipment
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class AddShipmentTable extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    strings.setLanguage(this.props.currentLanguage);
    const {sNo, product,productDescription,quantity, unit,blend} = strings.addProduct;
    return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options}>
          <TableHeaderColumn dataField='order_product_id' hidden={true}>Order Id</TableHeaderColumn>
          <TableHeaderColumn dataField='shipment_id' hidden={true}>Shipment Id</TableHeaderColumn>
          <TableHeaderColumn dataField='index' isKey={true} autoValue={true}>{sNo}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_name' columnTitle={true}>{product}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_description' columnTitle={true}>{productDescription}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_qty'>{quantity}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_uom'>{unit}</TableHeaderColumn>
          {(this.props.dispTable) ? false : <TableHeaderColumn dataField='blend_percentage' >{blend}</TableHeaderColumn>}
          <TableHeaderColumn key="editAction" dataFormat={this.props.addShipmentFormatter}>{strings.OrdersTableText.Action}</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}

AddShipmentTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default AddShipmentTable;