import React from "react";
import PropTypes from "prop-types";
import { Button } from "reactstrap";
import { reduxForm, Field } from "redux-form";
import SubmitButtons from "components/SubmitButtons";
import ExportTable from './ExportTable';
// import RenderField from "components/RenderField";
import { getLocalStorage, getBuyerCompanyKeyId, getDate, getText, saveLocalStorage, getForestId } from 'components/Helper';
import options from 'components/options';
//localization
import LocalizedStrings from "react-localization";
import data from "../../../localization/data";

let strings = new LocalizedStrings(data);

const RenderField = ({ input, label, type,defaultValue, meta: { touched, error, invalid } }) => {
  return (
    <div className={`form-group ${touched && invalid ? 'has-error' : ''}`}>
      <label  className="control-label">{}</label>
      <div>
        <input {...input} className="form-control" placeholder={label} type={type} value={defaultValue} />
        <div className="help-block" />
        {touched && (error && <span className="error-danger">
          <i className="fa fa-exclamation-circle">{error}</i></span>) }
      </div>
    </div>
  );
};

export const CsvModalHeader = (props) => {
  const buttons = ["declineBtn", "cancelBtn"];
  return (
    <div className="headertext">
      <Button
        className="modal-btn"
        onClick={() => props.exportShipment({}, false)}
      >
        X
      </Button>
    </div>
  );
};

const validate = (value) => {
  const errors = {};
  // if (!value.bill_number) {
  //   errors.bill_number = "Required";
  // }
  // if (!value.invoiceNo) {
  //   errors.invoiceNo = "Required";
  // }
  return errors;
};


class ExportModal extends React.Component {
  constructor(props) {
    super(props);
    this.openFileExplorer = this.openFileExplorer.bind(this);
    this.onFileSelected = this.onFileSelected.bind(this);
    this.uploadFormatter = this.uploadFormatter.bind(this);
    this.openInvoicesExplorer = this.openInvoicesExplorer.bind(this);
    this.onDownloadDoc = this.onDownloadDoc.bind(this);
    this.onDeleteDoc = this.onDeleteDoc.bind(this);

    this.state = {
      selectedFile: null,
      selectedInvoice:null,
      loaded: 0,
      strip1: "",
      strip2: "",
      appendFile: "",
      shipmentId:''
    };
  }

  openFileExplorer() {
    this.refs.fileUploader.click();
  }

  openInvoicesExplorer() {
    this.refs.invoiceUploader.click();
  }

  async onFileSelected(event,isBill) {
    let lStorage = getLocalStorage('shipments');
    if(isBill) {
      await this.setState({ selectedFile: event.target.files[0], shipmentId:lStorage.shipment_id });
    }else{
      await this.setState({ selectedInvoice: event.target.files[0], shipmentId:lStorage.shipment_id });
    }
    
    
    var today = new Date();
    var date = today.getDate() + "-" + (today.getMonth() + 1) + "-" + today.getFullYear();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    let appendFile; 
    if(isBill){
      appendFile = this.state.selectedFile.name + '_' + this.state.shipmentId+ '_' + date;
    }else{
      appendFile = this.state.selectedInvoice.name + '_' + this.state.shipmentId+ '_' + date;
    }
    
    await this.setState({
      appendFile: appendFile
    })

    if(isBill){
      console.log('bill  upload')
      if(this.state.selectedFile !== undefined){
        if(this.state.selectedFile.size > 10485760){
          this.props.setAlertMeassage(true, "Size should be less than 10MB", false)
        }else{
          await this.props.uploadExportRebateDocument(this.state.selectedFile,lStorage.shipment_id,this.state.appendFile,1)
          await this.props.getExportRebateDocument(lStorage.shipment_id)
        }
      }
    }else{
      console.log('invoice  upload')
      if (this.state.selectedInvoice !== undefined) {
        if (this.state.selectedInvoice.size > 10485760) {
          this.props.setAlertMeassage(true, "Size should be less than 10MB", false)
        }
        else {
            await this.props.uploadExportRebateDocument(this.state.selectedInvoice,lStorage.shipment_id,this.state.appendFile,0)
            await this.props.getExportRebateDocument(lStorage.shipment_id)
          // await this.props.uploadCertFile(this.state.selectedFile, this.props.owner, this.props.certShipmentId, this.state.appendFile);
          // this.props.updateView(this.props.viewToBeUpdated);
        }
      }
    }
  }

  async onDownloadDoc(cell){
    await this.props.downloadDocument(cell.document_file_name,cell.document_name)
  }

  async onDeleteDoc(cell){
    let lStorage = getLocalStorage('shipments');
    await this.props.deleteDocument(cell.export_rebate_id,cell.document_file_name)
    await this.props.getExportRebateDocument(lStorage.shipment_id)
  }

  uploadFormatter(row, cell) {
    return (
      <ButtonGroup >
        {/* <Button className="action" title="view" style={{ paddingRight: '0px', paddingLeft: '1px' }} onClick={() => this.onDownloadDoc(cell)}>Download</Button>
        <Button className="action" title="approve" style={{ paddingRight: '0px' }} onClick={() => this.onDeleteDoc(cell)}>Delete</Button> */}
        <Button className="action" title="view" style={{ paddingRight: '0px', paddingLeft: '1px' }} onClick={() => this.onDownloadDoc(cell)}>Download</Button>
        <Button className="action" title="approve" style={{ paddingRight: '0px' }} onClick={() => this.onDeleteDoc(cell)}>Delete</Button>
      </ButtonGroup>
    );
  }

 async componentDidMount() {
  let lStorage = getLocalStorage('shipments');
  console.log('local storage shipments',lStorage.shipment_id)
    await this.props.getExportRebateDocument(lStorage.shipment_id)
  }

  submitExportRebate = (e) => {
    console.log('sadsada',e.target.name)
  }


  render() {
    strings.setLanguage(this.props.currentLanguage);
    let {handleSubmit}  = this.props;
    let {invoice_number} = this.props.exportModalDetail

    return (
      <div className="model_popup" style={{ backgroundColor: "#f8f9fa" }}>
        <div>
          <CsvModalHeader exportShipment={this.props.exportShipment} />
        </div>
        <div style={{ textAlign: "center", fontWeight: "bold" }}>
          {strings.exportModal.exportRebaitClaim}
        </div>
        <form onSubmit={handleSubmit}>
          <div style={{ margin: "1.5em 0" }}>
            <div style={{ textDecoration: "underline", fontWeight: "bold" }}>
              {strings.exportModal.billOfLoading}
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                // alignItems: "center",
                // marginTop: "1em",
              }}
            >
              <div style={{display:'flex',alignItems:'flex-end'}}>
                <Button
                  className="choose-file"
                  style={{ marginBottom: "10px" }}
                  onClick={this.openFileExplorer}
                  disabled={this.props.billFile && this.props.billFile.length > 0 && true}
                >
                  {strings.exportModal.uploadDocument} +
                </Button>
                
                {/* {strings.exportModal.uploadDocument} */}
              </div>
              <div style={{width:'60%',display: "flex",alignItems:'center'}}>
              {
                  this.props.billFile && this.props.billFile.length > 0 &&
                  <ExportTable 
                    data={this.props.billFile}
                    pagination={false}
                    search={false}
                    exportCSV={false}
                    haveAction={false}
                    options = {options}
                    uploadFormatter={this.uploadFormatter}
                    type='bill'
                  />
                }
                {
                  this.props.billFile && this.props.billFile.length === 0 &&
                  'Document list will appear here'
                }
              </div>
              
            </div>
            <div style={{display: "flex", alignItems: "center"}}>
              <div>{strings.exportModal.billOfLoadingNo}</div>&nbsp;&nbsp;
                <div>
                  {
                    this.props.billFile.length > 0 &&
                    this.props.billFile[0].document_number &&
                    <Field
                    name="bill_number"
                    type="text"
                    defaultValue={this.props.billFile[0].document_number}
                    component={RenderField}
                    input={{ disabled: true }}
                    placeholder={"Type here..."}
                  /> 
                  }
                  {
                    this.props.billFile.length > 0 &&
                    !this.props.billFile[0].document_number &&
                    <Field
                    name="bill_number"
                    type="text"
                    component={RenderField}
                    placeholder={"Type here..."} />
                  }
                  {
                    this.props.billFile.length === 0 &&
                    <Field
                    name="bill_number"
                    type="text"
                    component={RenderField}
                    placeholder={"Type here..."}
                  />
                  }
                </div>
              </div>
          </div>
          <div style={{ margin: "1.5em 0" }}>
            <div style={{ textDecoration: "underline", fontWeight: "bold" }}>
              {strings.exportModal.invoiceDetail}
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                // alignItems: "center"
              }}
            >
              <div style={{display:'flex',alignItems:'flex-end'}}>
              <Button 
              className="choose-file"  
              style={{marginBottom: "10px"}} 
              onClick={this.openInvoicesExplorer} 
              disabled={this.props.invoiceFile && this.props.invoiceFile.length > 0 && true}
              >{strings.exportModal.uploadDocument}  +</Button>
              </div>

              <div style={{width:'60%',display: "flex",alignItems:'center'}}>
              {
                  this.props.invoiceFile && this.props.invoiceFile.length > 0 &&
                  <ExportTable 
                    data={this.props.invoiceFile}
                    pagination={false}
                    search={false}
                    exportCSV={false}
                    haveAction={false}
                    options = {options}
                    uploadFormatter={this.uploadFormatter}
                    type='invoice'
                  />
                }
                {
                  this.props.invoiceFile && this.props.invoiceFile.length === 0 &&
                  'Document list will appear here'
                }
              </div>
            </div>
            <div style={{display: "flex", alignItems: "center"}}>
            <div>{strings.exportModal.invoiceNumber}</div>&nbsp;&nbsp;
                <div>
                  <Field
                    name="invoice_number"
                    type='text'
                    defaultValue={invoice_number}
                    input={{ disabled: true}}
                    component={RenderField}
                    placeholder={"Type here..."}
                  />
                </div>
            </div>
          </div>
          <div style={{ margin: "1.5em 0", display: "flex" }}>
            <div style={{ textDecoration: "underline", fontWeight: "bold" }}>
              {strings.exportModal.status}
            </div>
            <div style={{ marginLeft: "2em" }}>{this.props.billFile[0]&&this.props.billFile[0].is_approved ? <span style={{color:'green'}}>Accepted</span> :this.props.billFile[0]&& this.props.billFile[0].is_rejected ? <span style={{color:'red'}}>Rejected</span> : <span style={{color:'orange'}}>Not available</span> }</div>
          </div>
          <SubmitButtons
            submitLabel={this.props.billFile[0]&& this.props.billFile[0].is_rejected ? strings.exportModal.new  : strings.exportModal.submit}
            className="btn btn-primary create"
            submitting={false}
          />
        </form>
        <input
          type="file"
          name="bill"
          ref={"fileUploader"}
          onChange={(e)=>this.onFileSelected(e,true)}
          style={{ display: "none" }}
        />
        <input
          type="file"
          name="invoice"
          ref={"invoiceUploader"}
          onChange={(e)=>this.onFileSelected(e,false)}
          style={{ display: "none" }}
        />
      </div>
    );
  }
}

export default reduxForm({
  form: "ExportModal",
  validate,
})(ExportModal);
