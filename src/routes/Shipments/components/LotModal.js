import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import DropdownList from 'react-widgets/lib/DropdownList';
import 'react-widgets/dist/css/react-widgets.css';

import LotTable from './LotTable';
import SubmitButtons from 'components/SubmitButtons';
import RenderField from 'components/RenderField';
import { getLocalStorage } from 'components/Helper';
//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);


let ltDetail = {};

export const LinkModalHeader = (props) => {
  return (
    <div className="headertext">
      {(!props.isLotEdit) ?
        <div>
          {strings.linkModal.selectStock}<br></br>
          {strings.linkModal.selectStockDescription}
        </div>
        :
        <div>
          {strings.linkModal.lotEdit}
        </div>
      }
      <Button className="modal-btn" onClick={() => props.addLot('close')}>X</Button>
    </div>
  );
}

const renderDropdownList = ({ input, data, valueField, textField, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div>
      <DropdownList {...input}
        data={data}
        onChange={input.onChange}
      />
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
};

const renderField = ({ input, label, type, customProps, meta: { touched, error, invalid } }) => {
  return (
    <div className={`form-group ${touched && invalid ? 'has-error' : ''}`}>
      <label className="control-label">{label}</label>
      <div>
        <input {...input} className="form-control" type={type} onInput={customProps.handleFunc(input.value)} />
      </div>
    </div>
  );
}

const validate = values => {
  const errors = {}
  if (!values.lot_qty) {
    errors.lot_qty = 'Required'
  } else if (values.lot_qty > ltDetail.product_qty) {
    errors.lot_qty = 'updated qty must be less than available qty'
  }

  return errors;
}

/** func renderLink
 *
 * @description This method returns a field object having a span to achieve onClick functionality. Simply we can not use
 *   onClick on field wrapper.
 *
 * return A component
 */
const renderLink = ({ input, label, type, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className="btn-bs-file">
      <span className='form-control'>
        <p>{customProps.value}</p>
        {(input.name === 'linkTo') &&
          <span>
            <img src={Images.fileUploadIcon} alt="" data-toggle="modal" data-target="#myModal" />
          </span>
        }
      </span>
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

/** LinkModal
 *
 * @description This class having a modal that will display a list of incoming orders and having a filter on that list
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class LinkModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      filterText: '',
      initData: {}
    }
  }

  async componentWillMount() {
    ltDetail = this.props.stockLotDetail;
    await this.props.viewStocks(this.props.options.currentPage, this.props.options.modalPageSize, this.state.filterText);
  }

  componentDidMount() {
    this.handleInitialize();
  }

  async handleInitialize() {
    if (!_.isEmpty(this.props.stockLotDetail)) {
      const initData = {
        lot_qty: (!_.isEmpty(getLocalStorage('selectedLotDetail'))) ? getLocalStorage('selectedLotDetail').consume_product_qty : ''
      }

      await this.setState({ initData: initData });
      this.props.initialize(initData);
    }
  }

  async lotFilter(query) {
    await this.setState({ filterText: query });
    this.props.viewStocks(this.props.options.currentPage, this.props.options.modalPageSize, this.state.filterText);
  }

  componentWillUnmount() {
    this.setState({ filterText: '' });
  }

  render() {
    const { handleSubmit, onChange, stockLotDetail } = this.props;
    strings.setLanguage(this.props.currentLanguage);

    return (
      <div className={(!this.props.isLotEdit) ? "modal" : "model_popup"}>
        <Row>
          <LinkModalHeader
            addLot={this.props.addLot}
            isLotEdit={this.props.isLotEdit}
          />
        </Row>
        {(this.props.isLotEdit) ?
          <div>
            <form onSubmit={handleSubmit}>
              <div className='col-12 col-sm-12 col-xl-12 mb-2'>
                <Row>
                  <Col className="text-center col-12 col-sm-12 col-xl-12 pr-1">
                    <label>{strings.linkModal.availableQuantity}</label>
                    <Field
                      name='available_lot_qty'
                      type="number"
                      component={renderLink}
                      label={strings.linkModal.availableQuantity}
                      customProps={{
                        value: (!_.isEmpty(stockLotDetail)) ? stockLotDetail.product_qty : ''
                      }}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col className="text-center col-12 col-sm-12 col-xl-12 pr-1">
                    <label style={{marginTop: '22px' , paddingRight:'1%'}}>{strings.linkModal.lotConsume}</label>
                    <div style={{marginTop: '-4%'}}>
                    <Field
                      name='lot_qty'
                      type="number"
                      component={RenderField}
                      label={strings.linkModal.quantity}
                    />
                    </div>
                  </Col>
                </Row>
              </div>
              <div className='col-12 col-sm-12 col-xl-12 mb-2'>
                <Col className="text-center col-9 col-sm-9 col-xl-9">
                  <SubmitButtons
                    submitLabel={strings.linkModal.update}
                    className='btn btn-primary create'
                    submitting={this.props.submitting}
                  />
                </Col>
              </div>
            </form>
          </div>
          :
          <div>
            <div className='col-12 col-sm-12 col-xl-12 mb-2'>
              <Row>
                <Col className="text-center col-12 col-sm-12 col-xl-12 pr-1">
                  <input
                    type="text"
                    className="lot-filter-search"
                    placeholder={strings.OrderFilterText.Search}
                    value={this.state.filterText}
                    onChange={(e) => this.lotFilter(e.target.value)}
                  />
                </Col>
              </Row>
            </div>
            <LotTable
              data={this.props.stockLot}
              pagination={false}
              search={false}
              exportCSV={false}
              isForModal={false}
              lotFormatter={this.props.lotFormatter}
              viewStocks={this.props.viewStocks}
              pages={this.props.pages}
              filterText={this.state.filterText}
              currentLanguage={this.props.currentLanguage}

            />
          </div>
        }
      </div>
    );
  }
}

export default reduxForm({
  form: 'LinkModal',
  validate,
})(LinkModal)