import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
import RenderDatePicker from 'components/RenderDatePicker';
import DropdownList from 'react-widgets/lib/DropdownList';
import 'react-widgets/dist/css/react-widgets.css';
import Autocomplete from "react-autocomplete";
import SubmitButtons from 'components/SubmitButtons';
import renderField from './renderField';
import { Images } from 'config/Config';
import { getBuyerCompanyKeyId, getProductId, getLocalStorage, saveLocalStorage, getCurrentDate } from 'components/Helper';
import AddShipmentTable from './AddShipmentTable';
import AddProduct from './AddProduct';
import options from 'components/options';

//localization
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
let strings = new LocalizedStrings(
  data
);

/** func Validate
 * description validate method will validate all the control fields of form based on provided criteria
 *
 * return Array of Errors
 */
const validate = values => {
  let loginData = getLocalStorage('loginDetail')
  const errors = {}
  if (!values.invoice_number) {
    errors.invoice_number = 'Required'
  }
  if (!values.invoice_date) {
    errors.invoice_date = 'Required'
  } else if (values.invoice_date) {
    let date = values.invoice_date.substring(0, 10);
    if (date > getCurrentDate()) {
      errors.invoice_date = 'Date must not be greater then today'
    }
  }
  if (!values.buyer_company_key_name) {
    errors.buyer_company_key_name = 'Required'
  }
  if ((loginData.role === "Garment Manufacturer") || (loginData.role === "Spinner") || (loginData.role === "Grey Fabric Manufacturer") || (loginData.role === "Finish Fabric Manufacturer") || (loginData.role === "Birla Cellulose") || (loginData.role === "Integrated Player") || (loginData.role === "Pulp")) {

  } else if ((loginData.role !== "Garment Manufacturer") || (loginData.role !== "Birla Cellulose") || (loginData.role !== "Spinner")|| (loginData.role !== "Grey Fabric Manufacturer") || (loginData.role !== "Finish Fabric Manufacturer") || (loginData.role !== "Pulp") || (loginData.role === "Integrated Player")) {

    // console.log(loginData.role)
    if (!values.linkTo) {
      errors.linkTo = 'Required'
    }

  }
  // if (!values.buyer_company_bu_id) {
  //   errors.buyer_company_bu_id = 'Required'
  // }
  // if (!values.seller_company_bu_id) {
  //   errors.seller_company_bu_id = 'Required'
  // }
  // if (!values.shipment_transport_mode) {
  //   errors.shipment_transport_mode = 'Required'
  // }
  if(!values.pulp_company){
    errors.pulp_company = 'Required'
  }
  return errors
}

/** func renderItems
 *
 * @description This method push an object of the defined input controls into fields array to display a field array of 
 *   certain inputs
 *
 * return Array of control fields
 */
const renderItems = ({ fields, customProps, meta: { error, submitFailed } }) => {
  return (
    <div>
      <button type="button" className="btn btn-primary" onClick={customProps.addProduct}>
        {strings.createShipment.addProduct} <span className="glyphicon glyphicon-plus"></span>
      </button>
      {submitFailed && error && <span>{error}</span>}
    </div>
  );
}

/** func renderLink
 *
 * @description This method returns a field object having a span to achieve onClick functionality. Simply we can not use
 *   onClick on field wrapper.
 *
 * return A component
 */
const renderLink = ({ input, label, type, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className="btn-bs-file">
      <span className='form-control'>
        <p>{customProps.linked_order_number}</p>
        <span onClick={() => customProps.handleLinkModal('open')}>
          <img src={Images.fileUploadIcon} alt="" data-toggle="modal" data-target="#myModal" />
        </span>
      </span>
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

const renderDropdownList = ({ input, data, valueField, textField, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div>
      <DropdownList {...input}
        data={data}
        onChange={input.onChange}
        onSelect={customProps.fetchUnitDetail(input.value)}
      />
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

const renderSelectField = ({ input, label, type, meta: { touched, error, invalid }, children }) => {
  return (
    // ${(touched && invalid )? 'has-error' : ''}`
    <div className={`form-group `}>
      <label className="control-label">{label}</label>
      <div>
        <select className="input" {...input}>
          {children}
        </select>

        <div className="help-block">
          {touched && (error && <span className="error-danger"><i className="fa fa-exclamation-circle">{error}</i></span>)}
        </div>
      </div>
    </div>
  );
}
const renderDropdownPulp = ({ input, data, valueField, textField, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div>
      <DropdownList {...input}
        data={data}
        onChange={input.onChange}
        // onSelect={customProps.handlePulpDrop(input.value)}
      />
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}
/** CreateShipment
 *
 * @description This class is responsible to display a form to create shipment
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class CreateShipment extends React.Component {
  constructor(props) {
    super(props);
    this.fetchUnitDetail = this.fetchUnitDetail.bind(this);
    this.createProducts = this.createProducts.bind(this);
    this.fetchAutoProducts = this.fetchAutoProducts.bind(this);
    this.selectedProduct = this.selectedProduct.bind(this);
    this.showMenu = this.showMenu.bind(this);
    this.addShipmentFormatter = this.addShipmentFormatter.bind(this);
    this.editProductShipment = this.editProductShipment.bind(this);
    this.deleteProductShipment = this.deleteProductShipment.bind(this);
    this.setParentState = this.setParentState.bind(this);
    this.handleRole = this.handleRole.bind(this);
    this.onChangeBuyer = this.onChangeBuyer.bind(this)
    this.onChangeBuyerVal = this.onChangeBuyerVal.bind(this)
    this.onHandleExtNumber = this.onHandleExtNumber.bind(this)

    this.state = {
      initData: {},
      editFormData: {},
      inputvalue: '',
      selectedProduct: '',
      selectedBuyer: '',
      userRole: false,
      isVendor: 0,
      changeBuyer: false
    }
  }

  static defaultProps = {
    transportMode: [
      {
        key: 'railway',
        value: 'Rail Transport'
      },
      {
        key: 'road',
        value: 'Road Transport'
      },
      {
        key: 'air',
        value: 'Air Transport'
      },
      {
        key: 'water',
        value: 'Ship Transport'
      },
    ]
  }

  async setParentState(key, status) {
    let obj = {};
    obj[key] = status;
    await this.setState(obj);
  }

  async componentWillMount() {
    console.log("in will mount")
    this.props.setActivePage('createShipment');
    if (this.props.isEdit) {
      let lStorage = getLocalStorage('shipments');
      await this.props.viewShipment(lStorage.shipment_id);
    }
    this.handleRole()
  }

  async componentDidMount() {
    await this.props.getPulpCompany();
    // if(this.props.isProductCreated){
    //   this.props.viewShipmentDetail.shipmentDetail.order_number = this.props.isExtNumber
    // }
    console.log(this.props.viewShipmentDetail);
    console.log(this.props.isEditText);
    console.log(this.state.isVendor)
    console.log(this.props.companyDropPulpInfo);
    console.log((getLocalStorage('loginDetail').role))
    // let company_type_id_ext = getLocalStorage('loginDetail').userInfo.company_type_id
    // await this.props.fetchBuyer(company_type_id_ext);
    // await this.props.viewShipment(row.shipment_id, key);
    // let company_key_id = getBuyerCompanyKeyId(this.props.buyerCompniesList, this.props.viewShipmentDetail.shipmentDetail.buyer_company_key_id, null);
    // console.log(company_key_id)
    // await this.props.viewCompanyDetail(company_key_id);

    if (!_.isEmpty(this.props.extVendorCheck.data)) {
      console.log(this.props.extVendorCheck.data.is_external)
      this.setState({
        isVendor: this.props.extVendorCheck.data.is_external
      })
    }

    if (this.props.isEdit)
      this.handleInitialize();
  }

  componentWillUnmount() {
    this.props.setProps(false, 'isProductEdit');
    this.props.setProps(false, 'isAddProduct');
    this.props.setProps(false, 'isAddLot');
    this.props.setCreateFormState();
    this.props.handleEditChange('unmount');
    this.selectedBuyer([], 'unmount');
  }

  componentWillReceiveProps() {
    console.log("in receive props")
    console.log(this.props.showProductAfterLink);
    console.log(this.props.isAddProduct)
    if (this.props.showProductAfterLink || this.props.isAddProduct) {
      this.handleInitialize();
    }
  }

  async handleRole() {
    let loginData = getLocalStorage('loginDetail');
    await this.props.fetchCompanyRole(loginData.role)
    await this.props.viewCompanyDetail(loginData.userInfo.company_key_id)
    if (loginData.role === "Integrated Player") {
      if (!_.isEmpty(this.props.viewCompanyInfo) && !_.isEmpty(this.props.companyRole)) {
        var userRole = this.props.viewCompanyInfo.roles.some((val) => (val.company_role_id === (this.props.companyRole.filter((val) => (val.company_rolename === "Garment Manufacturer") ? val.company_role_id : false))[0].company_role_id) ? true : false)
        await this.setState({ userRole: userRole })
      }
    }
  }
  async handleInitialize() {
    console.log("in initiale");
    this.props.setCurrentShipmentId(this.props.viewShipmentDetail.shipmentDetail.shipment_id);
    if (!_.isEmpty(this.props.viewShipmentDetail)) {
      if(!this.props.isAddProduct){
        let buyer_company_key_name = getBuyerCompanyKeyId(this.props.buyerCompniesList, this.props.viewShipmentDetail.shipmentDetail.buyer_company_key_id, true);
      let initData = {}
      let linked_forest_name = this.props.viewShipmentDetail.shipmentDetail.linked_forest_name || ''

      initData = {
        shipment_id: this.props.viewShipmentDetail.shipmentDetail.shipment_id,
        invoice_number: this.props.viewShipmentDetail.shipmentDetail.invoice_number,
        buyer_company_key_name: buyer_company_key_name,
        buyer_company_bu_id: this.props.viewShipmentDetail.shipmentDetail.buyer_company_bu_id,
        seller_company_bu_id: this.props.viewShipmentDetail.shipmentDetail.seller_company_bu_id,
        linkTo: this.props.viewShipmentDetail.shipmentDetail.order_number,
        invoice_date: this.props.viewShipmentDetail.shipmentDetail.invoice_date.toString().split('T')[0],
        shipment_description: this.props.viewShipmentDetail.shipmentDetail.shipment_description,
        shipment_transport_mode: this.props.viewShipmentDetail.shipmentDetail.shipment_transport_mode,
        products: this.props.viewShipmentDetail.products,
        shipment_text: this.props.viewShipmentDetail.shipmentDetail.order_number,
        ext_buyer_new: this.props.viewShipmentDetail.shipmentDetail.buyer_company_name,
        pulp_company: linked_forest_name

      };
      initData.products.map((item, index) => {
        initData.products[index].index = index + 1;
        initData.products[index].shipment_id = initData.shipment_id;
      });
      await this.setState({
        initData: initData,
        companyName: buyer_company_key_name
      });
      saveLocalStorage('buyerDetail', buyer_company_key_name);
      this.props.initialize(initData);
      }
      else{
        let buyer_company_key_name = getBuyerCompanyKeyId(this.props.buyerCompniesList, this.props.viewShipmentDetail.shipmentDetail.buyer_company_key_id, true);
        let initData = {}
        let linked_forest_name = this.props.viewShipmentDetail.shipmentDetail.linked_forest_name || ''

        initData = {
          shipment_id: this.props.viewShipmentDetail.shipmentDetail.shipment_id,
          invoice_number: this.props.viewShipmentDetail.shipmentDetail.invoice_number,
          buyer_company_key_name: buyer_company_key_name,
          buyer_company_bu_id: this.props.viewShipmentDetail.shipmentDetail.buyer_company_bu_id,
          seller_company_bu_id: this.props.viewShipmentDetail.shipmentDetail.seller_company_bu_id,
          linkTo: this.props.viewShipmentDetail.shipmentDetail.order_number,
          invoice_date: this.props.viewShipmentDetail.shipmentDetail.invoice_date.toString().split('T')[0],
          shipment_description: this.props.viewShipmentDetail.shipmentDetail.shipment_description,
          shipment_transport_mode: this.props.viewShipmentDetail.shipmentDetail.shipment_transport_mode,
          products: this.props.viewShipmentDetail.products,
          shipment_text: this.props.isExtNumber,
          ext_buyer_new: this.props.viewShipmentDetail.shipmentDetail.buyer_company_name,
          pulp_company: linked_forest_name
  
        };
        initData.products.map((item, index) => {
          initData.products[index].index = index + 1;
          initData.products[index].shipment_id = initData.shipment_id;
        });
        await this.setState({
          initData: initData,
          companyName: buyer_company_key_name
        });
        saveLocalStorage('buyerDetail', buyer_company_key_name);
        this.props.initialize(initData);
      }
      
      
    }
  }

  async deleteProductShipment(row) {
    await this.props.deleteShipment(row.shipment_product_id, 'review');
    // fetch incoming orders to reset isLinked state accordingly
    await this.props.fetchShipment('outgoingShipment', options.currentPage, options.modalPageSize, this.state.fromDt, this.state.toDt);
    // fetch updated viewOrderDetail to render the form with updated data
    await this.props.viewShipment(row.shipment_id);
    this.handleInitialize();
  }

  async editProductShipment(row) {
    // fetch product before editing. product array is needed to find out product_id when update product
    this.props.fetchProducts();

    let lStorage = getLocalStorage('shipments');
    lStorage.shipment_product_id = row.shipment_product_id;
    this.props.viewShipmentProductDetail(row.shipment_product_id);
    saveLocalStorage('shipments', lStorage); // storing shipment_product_lot_id 
    this.setState({
      editFormData: row,
      selectedProduct: row.product_name,
    });
    this.props.setProps(true, 'isProductEdit');// opening in edit state
    this.props.setProps(true, 'isAddProduct');// opening product form
    this.props.setProps(true, 'shouldProductFormClose');// opening product form
  }

  addShipmentFormatter(row, cell) {
    return (
      <ButtonGroup>
        <Button className="action" onClick={() => this.editProductShipment(cell, 'review')} >{strings.outgoingFormatterText.edit}</Button>
        <Button className="action delete" onClick={() => this.deleteProductShipment(cell, 'review')} >{strings.outgoingFormatterText.delete}</Button>
      </ButtonGroup>
    );
  }

  fetchUnitDetail(value) {
    let company_key_id = getBuyerCompanyKeyId(this.props.buyerCompniesList, value, false);
    saveLocalStorage('BuyerCompanyKeyId', company_key_id);
    if (typeof value !== 'undefined' && value !== '') {
      this.props.fetchBuyerUnit(company_key_id); // buyer unit
      this.props.fetchSellerUnit(); // seller unit
    }
  }

  fetchAutoProducts(product) {
    this.setState({ inputvalue: product });
    if (product.length > 1) {
      this.props.fetchProducts();
    } else {
      this.props.setProps([], 'doProductEmpty');
    }
  }

  async selectedProduct(product, mode = '') {
    if (mode === 'unmount') {
      this.setState({
        inputvalue: '',
        selectedProduct: ''
      });
    } else {
      let productDetail = {};
      if (product != "product not found") {
        await this.setState({ selectedProduct: product });
        this.setState({ inputvalue: product });
        saveLocalStorage('productDetail', product);
      }
    }
  }

  showMenu() {
    if (_.isEmpty(this.props.products)) {
      return (
        "{'none'}"
      );
    }
  }

  async createProducts(values) {
    let loginData = getLocalStorage('loginDetail')
    // debugger
    console.log(this.props.viewShipmentDetail);
    let lStorage = getLocalStorage('shipments');
    if (this.state.selectedProduct.length <= 0) {
      this.props.resetAlertBox(true, 'Please choose correct product');
    } else {
      let product_id
      (this.props.isProductEdit) ?
        product_id = this.state.editFormData.product_id
        : product_id = getProductId(this.props.products, this.state.selectedProduct);
      values.product_id = product_id;
      values.shipment_id = lStorage.shipment_id;

      if (this.props.isProductEdit) values.shipment_product_id = lStorage.shipment_product_id;

      await this.props.addProducts(values, this.props.isProductEdit);
      await this.props.viewShipment(lStorage.shipment_id);
      await console.log(this.props.viewShipmentDetail.products.length)
      this.handleInitialize();

      if (this.props.addedProductDetail.shipment_product_id !== undefined && this.props.addedProductDetail.shipment_product_id !== null) {
        lStorage.shipment_product_id = this.props.addedProductDetail.shipment_product_id;
        saveLocalStorage('shipments', lStorage);
        this.props.setProps(true, 'isProductEdit');
      }

      // close product form when user is Birla Cellulose because there is not add lot button for 'Birla Cellulose' so there is no sence to stop user on add product screen if product added successfully. 
      if (this.props.role === 'Birla Cellulose' || this.props.role === 'Pulp') await this.props.setProps(true, 'shouldProductFormClose');
      if(this.props.viewShipmentDetail.is_lot ===0 && (loginData.role !== "Birla Cellulose" && loginData.role !== "Pulp")){ 
        await this.props.addLot('open')
      }
      
      // close product form accordingly based on shouldProductFormClose prop
      if (this.props.shouldProductFormClose) {
        await this.props.setProps(false, 'shouldProductFormClose');
        await this.props.setProps(false, 'isAddProduct');
      }
    }
  }

  async onChangeBuyer(status) {
    this.setState({
      changeBuyer: true
    })
    this.props.checkForExtBuyer(status)
  }
  async onChangeBuyerVal() {

  }
  async onHandleExtNumber(e){
    this.props.isExtNumberFunc(e.target.value)
  }

  returnAutoComplete() {
    return (
      <div className="customAutoComplete">
        <Autocomplete
          className="form-control"
          ref="autocomplete"
          menuStyle={{ display: this.props.showMenu, minWidth: '100%', }}
          inputProps={{ placeholder: "Start typing three letters..." }}
          items={this.props.buyerCompniesList}
          getItemValue={item => item.company_name}
          shouldItemRender={this.matchBuyer}
          renderItem={(item, highlighted) =>
            <div className='col px-2'
              key={item.product_id}
              style={{
                backgroundColor: "white", width: "100%", color: "black", paddingBottom: '5px',
                paddingTop: '5px', maxHeight: '150px', overflowY: 'auto'
              }}
            >
              {item.company_name}
            </div>
          }
          value={this.state.companyName}
          onChange={e => this.handleBuyer(e.target.value)}
          onSelect={value => this.selectedBuyer(value)}
        />
      </div>
    );
  }

  async selectedBuyer(buyer, mode = '') {
    if (mode === 'unmount') {
      this.setState({
        companyName: '',
        selectedBuyer: ''
      });
    } else {
      let buyerDetail = {};
      if (buyer != "product not found") {
        this.fetchUnitDetail(buyer);
        await this.setState({ selectedBuyer: buyer });
        this.setState({ companyName: buyer });
        saveLocalStorage('buyerDetail', buyer);
      }
    }
  }

  async handleBuyer(buyer) {
    await this.setState({ companyName: buyer });
    await this.props.companyNameExt(buyer)
    if (buyer.length > 1) {
      this.props.fetchBuyer(this.props.companyId);
    } else {
      this.props.setProps([], 'doBuyerEmpty');
    }
  }

  matchBuyer(state, value) {
    //console.log(state.company_name);
    return (
      state.company_name.toLowerCase().indexOf(value.toLowerCase()) !== -1
    );
  }

  render() {
    const { handleSubmit, currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);
    let dispbutn = ((this.props.role === "Spinner" && this.props.reviewIncommingshipment === null)) ? false : (this.props.role === "Birla Cellulose" || (this.props.role === "Spinner" && this.props.reviewIncommingshipment === "incomingShipment")|| this.props.role === "Pulp") ? true : false;
    return (
      <div>
        {(!this.props.isAddProduct) ?
          <div className='col-md-12 col-lg-12 create-order'>
            <h2>{strings.createShipment.shipmentDetail}</h2>
            <div>
              <form onSubmit={handleSubmit}>
                <div className="form-row">
                  <div className="form-group col-md-4">
                    <Field
                      name='invoice_number'
                      type="text"
                      component={renderField}
                      label={`${strings.createShipment.invoiceNumber} *`}
                      placeholder="Enter invoice"
                    />
                  </div>
                  <div className="form-group col-md-4">
                    <label>{`${strings.createShipment.invoiceDate} *`}</label>
                    <Field
                      name='invoice_date'
                      type='date'
                      component={RenderDatePicker}
                    />
                  </div>
                  <div className="form-group col-md-4">
                    {!this.props.isExtBuyer && this.state.isVendor === 0 ?
                      <div className="autocomplete">
                        <label>{`${strings.shipmentTableText.buyer} *`}</label>
                        {this.returnAutoComplete()}
                      </div> :

                      <div >
                        <Field
                          name='ext_buyer_new'
                          type="text"
                          component={renderField}
                          placeholder="External buyer name"
                          label="Custom Buyer *"
                        />
                      </div>

                    }
                    { (!this.props.isEdit) &&
                      <div>
                    {
                      !this.props.isExtBuyer && this.state.isVendor === 0 ?

                        <ButtonGroup>
                          <p style={{ color: '#007bff' }}>Click here to add custom buyer</p><Button className="action" title="add" style={{ color: '#ff0000cc' , marginTop: '-14%', left: '104%', border: 'none' }} onClick={() => this.onChangeBuyer(true)}>add</Button>
                        </ButtonGroup>
                        :
                        
                        <ButtonGroup>
                          <p style={{ color: '#007bff' }}>Click here to go back to normal process</p><Button className="action" title="back" style={{ color: '#ff0000cc', marginTop: '-14%', left: '104%', border: 'none' }} onClick={() => this.onChangeBuyer(false)}>back</Button>
                        </ButtonGroup>
                    }
                    </div>
                    }
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-4">
                    <label>{strings.createShipment.sellerUnit}</label>
                    <Field name="seller_company_bu_id" component="select" label="Seller Unit" className='form-control'>
                      <option>{'Select Seller Unit'}</option>
                      {this.props.sellerUnitList.map((seller, i) => {
                        return (
                          <option value={seller.company_bu_id} key={i}>{seller.company_bu_name}</option>
                        );
                      })}
                    </Field>
                  </div>
                  <div className="form-group col-md-4">
                    <label>{strings.createShipment.buyerUnit}</label>
                    <Field name="buyer_company_bu_id" component="select" className='form-control'>
                      <option>{'Select Buyer Unit'}</option>
                      {this.props.buyerUnitList.map((buyer, i) => {
                        return (
                          <option value={buyer.company_bu_id} key={i}>{buyer.company_bu_name}</option>
                        );
                      })}
                    </Field>
                  </div>
                  <div className="form-group col-md-4">
                    <label>{`${strings.createShipment.transportMode}`}</label>
                    <Field name="shipment_transport_mode" component="select" className='form-control'>
                      <option>{'Select Transport Mode'}</option>
                      {strings.transportMode.map((mode, i) => {
                        return (
                          <option value={mode.value} key={i}>{mode.value}</option>
                        );
                      })}
                    </Field>
                  </div>
                </div>
                {(getLocalStorage('loginDetail').role) === "Pulp" && 
                <div className="form-row">
                <div className="form-group col-md-4">
                  <label>Linked Forest*</label>
                  <Field
                    className="select-menu"
                    name="pulp_company"
                    component={renderDropdownPulp}
                    data={(!_.isEmpty(this.props.companyDropPulpInfo)) ? this.props.companyDropPulpInfo.map(function (list) { return list.company_name }) : []}
                    // customProps={{
                    //   handlePulpDrop: this.handlePulpDrop
                    // }}
                  />
                  </div>
                  
                </div>
                 }
                <div className="form-row">
                  <div className="form-group col-md-8">
                    <Field
                      name='shipment_description'
                      type="text"
                      component={renderField}
                      label={strings.createShipment.shipmentDescription}
                    />
                  </div>
                  {(this.props.isEdit && this.props.role !== 'Brand' && !this.props.isEditText && this.state.isVendor === 0) &&
                    <div className="form-group col-md-4 ">
                      <label>{(this.props.role === 'Garment Manufacturer' || this.state.userRole) ? `${strings.createOrder.linkBuyer}` : `${strings.createOrder.linkBuyer} *`}</label>
                      <Field
                        name='linkTo'
                        type='text'
                        component={renderLink}
                        customProps={{
                          handleLinkModal: this.props.handleLinkModal,
                          linked_order_number: (!_.isEmpty(this.props.viewShipmentDetail)) ? this.props.viewShipmentDetail.shipmentDetail.order_number : ''
                        }}
                      />
                    </div>
                  }
                  {((this.props.isEditText) || (this.state.isVendor === 1)) &&
                    <div className="form-group col-md-4">
                      <Field
                        name='shipment_text'
                        type="text"
                        component={renderField}
                        label="External Purchase Order No."
                        onChange={(e) => this.onHandleExtNumber(e)}
                      />
                    </div>
                  }

                </div>
                {(this.props.isEdit) &&
                  <div>
                    {(!_.isEmpty(this.state.initData) && !_.isEmpty(this.state.initData.products)) &&
                      <div className="col-md-12 nopad">
                        <h2>{strings.addProduct.productDetail}</h2>
                        <AddShipmentTable
                          data={this.state.initData.products}
                          pagination={false}
                          search={false}
                          exportCSV={false}
                          options={this.props.options}
                          addShipmentFormatter={this.addShipmentFormatter}
                          dispTable={dispbutn}
                          currentLanguage={this.props.currentLanguage}
                        />
                      </div>
                    }
                    <FieldArray
                      name="products"
                      component={renderItems}
                      customProps={{
                        addProduct: this.props.addProduct,
                      }}
                    />
                  </div>
                }
                <div>
                  <SubmitButtons
                    submitLabel={(!this.props.isEdit) ? strings.createOrder.next : strings.setting.create}
                    className='btn btn-primary create'
                    submitting={this.props.submitting}
                  />
                </div>
              </form>
            </div>
          </div>
          :
          <div>
            <AddProduct
              onSubmit={this.createProducts}
              role={this.props.role}
              selectedProduct={this.selectedProduct}
              showMenu={this.showMenu}
              inputvalue={this.state.inputvalue}
              options={this.props.options}
              products={this.props.products}
              fetchProducts={this.props.fetchProducts}
              fetchAutoProducts={this.fetchAutoProducts}
              resetProducts={this.props.resetProducts}
              setProps={this.props.setProps}
              isProductEdit={this.props.isProductEdit}
              editFormData={this.state.editFormData}
              isAddLot={this.props.isAddLot}
              addLot={this.props.addLot}
              viewShipmentProductDetail={this.props.viewShipmentProductDetail}
              viewLotDetail={this.props.viewLotDetail}
              deleProductLot={this.props.deleProductLot}
              setParentState={this.setParentState}
              fetchUOM={this.props.fetchUOM}
              uom={this.props.uom}
              setActivePage={this.props.setActivePage}
              getLotDetail={this.props.getLotDetail}
              currentLanguage={this.props.currentLanguage}
              viewShipmentDetail={this.props.viewShipmentDetail}
              
            />
          </div>
        }
      </div>
    );
  }
}

CreateShipment.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
};

export default reduxForm({
  form: 'CreateShipment',
  validate,
})(CreateShipment)