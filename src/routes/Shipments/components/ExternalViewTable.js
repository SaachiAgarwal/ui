import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

//localization
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
let strings = new LocalizedStrings(
  data
);


/** AddShipmentTable
 *
 * @description This class is responsible to display added orderd as outgoing shipment
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class ExternalViewTable extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);
    const {vendor, orderNumber} = strings.vendor;
    const {action,shipmentDate, invoiceNumber} = strings.shipmentTableText;

    return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options}>
          <TableHeaderColumn dataField='ext_vendor_shipment_id' hidden={true} isKey={true} >Shipment Id</TableHeaderColumn>
          <TableHeaderColumn dataField='Invoice_number'>{invoiceNumber}</TableHeaderColumn>
          <TableHeaderColumn dataField='seller_company_name' columnTitle={true}>{vendor}</TableHeaderColumn>
          <TableHeaderColumn dataField='order_number'>{orderNumber}</TableHeaderColumn>
          <TableHeaderColumn dataField='shipment_date'>{shipmentDate}</TableHeaderColumn>
          <TableHeaderColumn dataField='shipment_description' columnTitle={true}>{strings.createShipment.shipmentDescription}</TableHeaderColumn>
          <TableHeaderColumn key="editAction" dataFormat={this.props.vendorFormatter}>{action}</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}
ExternalViewTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default ExternalViewTable;