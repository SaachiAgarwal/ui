import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

//localization
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
let strings = new LocalizedStrings(
  data
);


/** AddShipmentTable
 *
 * @description This class is responsible to display added orderd as outgoing shipment
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class QaUploadTable extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);

    return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options}>
          <TableHeaderColumn dataField='shipment_document_id' hidden={true}>Shipment Document ID</TableHeaderColumn>
          <TableHeaderColumn dataField='index' >{strings.addProduct.sNo}</TableHeaderColumn>
          <TableHeaderColumn dataField='document_name' isKey={true} autoValue={true} >{strings.qa.documentName}</TableHeaderColumn>
          <TableHeaderColumn dataField='is_approved'>{strings.qa.isApproved}</TableHeaderColumn>
          <TableHeaderColumn dataField='remarks'>{strings.qa.remarks}</TableHeaderColumn>
          <TableHeaderColumn key="editAction" dataFormat= {this.props.qaFormatter}>{strings.OrdersTableText.Action}</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}
QaUploadTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default QaUploadTable;