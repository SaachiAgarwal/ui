import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import _ from 'lodash';
import FileDownload from 'js-file-download';

import SubmitButtons from 'components/SubmitButtons';
import RenderTextArea from 'components/RenderTextArea';

import axios from 'axios';
import { Config, Url } from 'config/Config';
import Pagination from 'react-paginate';

import { Images } from 'config/Config';

import { getLocalStorage, saveLocalStorage, getToken, sleep, filterByDate, getPages } from 'components/Helper';
// import { TableHeaderColumn } from 'material-ui';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

//import ViewData from './ViewData'
//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);



export const validate = (values) => {
  const errors = {}
  if (!values.message) {
    errors.message = 'Required'
  }
  return errors
}

export const CsvModalHeader = (props) => {
  const buttons = ['declineBtn', 'cancelBtn'];
  return (
    <div className="headertext">
      <div>
        <h2 className="heading-model-new">{strings.receiveModal.accept}</h2>
        <h3 className="text1">{strings.receiveModal.acceptStatus}</h3>
      </div>
    </div>
  );
}
class ReceiveModal extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      initdata: {},
    }

    this.onChangeQty = this.onChangeQty.bind(this);
    this.fetchQty = this.fetchQty.bind(this);
  }

  async onChangeQty(e, thiz) {

    let array = { ...this.state.initdata };
    array[thiz.shipment_product_id] = {qty:e.target.value, product_qty:thiz.product_qty}
    
    await this.setState({
      initdata: { ...array },
    })
    

    // console.log(this.state.product_receiveqty)
  }
  async fetchQty(shipment_product_id, thiz) {
    if(thiz.state.initdata[shipment_product_id]["qty"]<=thiz.state.initdata[shipment_product_id]["product_qty"]){
    await this.props.updateReceiveShipmentQty(shipment_product_id, thiz.state.initdata[shipment_product_id]["qty"])
    }
    else{
      //alert("Accepted quantity should be less than Product quantity")
      this.props.resetAlertBox(true, 'Accepted quantity should be less than Product quantity');
    }
  }
  
  render() {
    const handleSubmit = this.props.onSubmit;
    strings.setLanguage(this.props.currentLanguage);

    return (
      <div className="model_popup_qty">
        <div>
          <CsvModalHeader />
        </div>
        {this.props.viewShipmentDetail.products.map((item, index) =>
          (<div>
            <table className="table table-borderless">
              <tbody>
                <tr>
                  <td className="ProdData_name">
                    {item.product_name}
                  </td>
                  <td className="ProdData_qty">
                    {item.product_qty_received}
                  </td>
                  <td >
                    {item.product_uom}
                  </td>
                  <td className="ProdData_text_qty">
                    <input type="number"
                      className="form-control ProdData_size_qty "
                      id={item.product_qty}
                      onChange={(e) => this.onChangeQty(e, item)}
                    />
                  </td>
                  <td>
                    <img className="pencil-image" src={Images.pencilImage} />
                  </td>
                  <td>
                    <input type="button" value={strings.save} className="btn btn-primary" id={"btn_" + item.product_id} onClick={() => this.fetchQty(item.shipment_product_id, this)} />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          ))
        }
        <div>
          <form onSubmit={handleSubmit}>
            <div>
              <SubmitButtons
                submitLabel={strings.linkAutoOrderModal.accept}
                className='btn btn-primary create'
                submitting={this.props.submitting}
              />
            </div>
          </form>
        </div>
        <Button className="modal-btn" onClick={() => this.props.handlereceiveShipment(false)}>X</Button>
      </div>
    );
  }
}

export default reduxForm({
  form: 'ReceiveModal',
  validate,
})(ReceiveModal)
