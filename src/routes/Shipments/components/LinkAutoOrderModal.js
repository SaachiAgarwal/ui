import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import _ from 'lodash';
import FileDownload from 'js-file-download';
const { DOM: { input, select, textarea } } = React;

import SubmitButtons from 'components/SubmitButtons';
import { getLocalStorage } from 'components/Helper';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);




export const validate = (values) => {
  const errors = {}
  if (!values.message) {
    errors.message = 'Required'
  }
  return errors
}

export const CsvModalHeader = (props) => {
  return (
    <div>
      <div className="headertext">
        <h2 style={{ "paddingTop": "20px" }}>{strings.autoGenerate}</h2>
        {/*<h3 className="text1">Please provide details for declining this shipment.</h3>*/}
      </div>
      <Button className="modal-btn" onClick={() => props.handleAutoOrderModal(false, this)}>X</Button>
    </div>
  );
}

/** PermissionModal
 *
 * @description This class having a modal that will display a permission check that you are agreed with terms nd condition
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class LinkAutoOrderModal extends React.Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.generateAutoOrder = this.generateAutoOrder.bind(this);

    this.state = {
      isChecked: false,
      isDisabled: true,
      value: '',
      initData:{}
    }
  }

  async handleInputChange(event) {
    this.setState({
      isChecked: !this.state.isChecked,
      isDisabled: !this.state.isDisabled,
      value: event.target.value
    });
    if (event.target.value == 'accept') {
      await this.generateAutoOrder() //generate auto order
      // await this.props.handlePermissionModal(true);
    }
    this.props.handleAutoOrderModal(false);
  }

  async generateAutoOrder() {
    await this.props.generateAutoOrder(getLocalStorage('shipments').shipment_id, getLocalStorage('loginDetail').userInfo.user_id)
    await this.props.viewShipment(getLocalStorage('shipments').shipment_id);
    this.props.placeShipment('',true)
  }

  componentWillUnmount() {
    this.props.handleAutoOrderModal(false);
  }

  render() {
    strings.setLanguage(this.props.currentLanguage);
    const {accept, decline, autoLinkDescription} = strings.linkAutoOrderModal;
 
    const { handleSubmit } = this.props;
    return (
      <div className="model_popup">
        <div>
          <CsvModalHeader
            handleAutoOrderModal={this.props.handleAutoOrderModal}
          />
        </div>
        <div>
          <form onSubmit={handleSubmit}>
            <div>
              <label>{accept} </label>
              <input
                name="permission"
                type="checkbox"
                value="accept"
                style={{ "marginLeft": "15px" }}
                onChange={this.handleInputChange}
              />
              <label style={{ "marginLeft": "10px" }}> {decline} </label>
              <input
                name="permission"
                type="checkbox"
                value="decline"
                style={{ "marginLeft": "15px" }}
                onChange={this.handleInputChange}
              />
            </div>
          </form>
        </div>
        <div>
          <h5 style={{ "paddingTop": "80px" }}>*{autoLinkDescription}</h5>
        </div>
      </div>

    );
  }
}

// export default PermissionModal;

export default reduxForm({
  form: 'LinkAutoOrderModal',
  validate,
})(LinkAutoOrderModal)