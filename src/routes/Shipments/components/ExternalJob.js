import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
import DropdownList from 'react-widgets/lib/DropdownList';
import 'react-widgets/dist/css/react-widgets.css';
import _ from 'lodash';
import Autocomplete from "react-autocomplete";

import SubmitButtons from 'components/SubmitButtons';
import renderField from './renderField';
import { getSupplierCompanyKeyId, getCategory, getType } from 'components/Helper';

import HeaderSection from 'components/HeaderSection';
//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
import RenderDatePicker from 'components/RenderDatePicker';
import AddProduct from './AddProduct';
import AddExternalProduct from './AddExternalProduct'
import VendorProductTable from './VendorProductTable'
import options from 'components/options';
import { getBuyerCompanyKeyId, getProductId, getLocalStorage, saveLocalStorage, getCurrentDate } from 'components/Helper';
let strings = new LocalizedStrings(
  data
);


/** func Validate
 * description validate method will validate all the control fields of form based on provided criteria
 *
 * return Array of Errors
 */
const validate = values => {
  const errors = {}
  if (!values.inv_number) {
    errors.inv_number = 'Required'
  }
  if (!values.external_vendor) {
    errors.external_vendor = 'Required'
  }
  if (!values.ship_trans) {
    errors.ship_trans = 'Required'
  }
  if (!values.inv_date) {
    errors.inv_date = 'Required'
  }
  if (!values.inv_date) {
    errors.ship_date = 'Required'
  }
  return errors
}

const renderDropdownList = ({ input, data, valueField, textField, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div>
      <DropdownList {...input}
        data={data}
        onChange={input.onChange}
      />
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

/** CreateOrder
 *
 * @description This class is responsible to display a form to create order
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class ExternalJob extends React.Component {
  constructor(props) {
    super(props);
    // this.showMenu = this.showMenu.bind(this);
    // this.returnAutoComplete = this.returnAutoComplete.bind(this);
    // this.matchProducts = this.matchProducts.bind(this);

    this.state = {
      initData: {},
      blendpercentage: "100",
      active: 'addProduct',
      isProduct: false,
      data: '',
      ext_vendor_shipment_product_id: '',
      submitting: false,
      selectedBuyer: '',
      companyName: '',
      inputvalue: '',
      selectedProduct: '',
      isEdit: ''
    }
    this.props.setJobActive('externalJob')
    this.onSubmit = this.onSubmit.bind(this)
    this.onAddProduct = this.onAddProduct.bind(this)
    this.productFormatter = this.productFormatter.bind(this)
    this.onDeleteProduct = this.onDeleteProduct.bind(this)
    this.onEditProduct = this.onEditProduct.bind(this)
    this.fetchUnitDetail = this.fetchUnitDetail.bind(this)
    this.fetchAutoProducts = this.fetchAutoProducts.bind(this)
    this.selectedProduct = this.selectedProduct.bind(this);
  }

  // setSubmit = () => {
  //   console.log(`set submit is called`);
  //   this.setState({ submitting: true })
  // }

  async componentWillUnmount() {
    await this.props.resetJobActive('')
    await this.props.externalJobStatus(false)
    await this.props.resestViewUniqueExternalShipment('')
  }

  async onSubmit(values) {
    // debugger
    console.log(this.state.companyName);
    //let buyer_company_key_id = getBuyerCompanyKeyId(this.props.buyerCompniesList, buyerDetail, false);
    let lStorage = getLocalStorage('shipments');
    //let company_key_id = getLocalStorage('BuyerCompanyKeyId')
    // let company_key_id = getBuyerCompanyKeyId(this.props.buyerCompniesList, value, false);
    let company_key_id = getBuyerCompanyKeyId(this.props.buyerCompniesList, this.state.companyName, false);
    if(company_key_id !== ''){

    let lVendor = getLocalStorage('vendorDetails');
    console.log(lVendor)
    // console.log(lStorage.shipment_id)
    // console.log(this.props.external_shipment_id)
     console.log(values)
    let inv_date = values.inv_date.toString().split('T')[0]
    let ship_date = values.ship_date.toString().split('T')[0]
    //(shipment_id, invoice_number, invoice_date, order_number, shipment_transport_mode, shipment_description, seller_companyname, shipment_date )
    if (this.props.uniqueExternalView.vendors) {
      await this.props.onUpdateExtVendor(this.props.ext_vendor_shipment_id, lStorage.shipment_id, values.inv_number, inv_date, values.order_number, values.ship_trans, values.ship_desc, company_key_id, ship_date)
    }
    else {
      await this.props.createExternalJob(lStorage.shipment_id, values.inv_number, inv_date, values.order_number, values.ship_trans, values.ship_desc, company_key_id, ship_date)
    }
   
    this.setState({
      submitting: true
    })
    // await console.log(this.props.externalData.data.ext_vendor_shipmentid);
    }
    else{
      this.props.resetAlertBox(true, 'Please choose buyer name');
    }
    
  }

  async componentDidMount() {
    await this.props.fetchBuyer(3)
    // this.setState({ submitting: false })
    // console.log(this.props.uniqueExternalView.vendors)
    if (this.props.uniqueExternalView.vendors)
      this.handleInitialize();
      console.log(this.props.productsExt)
  }

  async onAddProduct() {
    this.setState({
      isProduct: true
    })
    this.props.setProductStatus(true)
  }

  returnAutoComplete() {
    return (
      <div className="customAutoComplete">
        <Autocomplete
          className="form-control"
          ref="autocomplete"
          menuStyle={{ display: this.props.showMenu, minWidth: '100%', }}
          inputProps={{ placeholder: "Start typing three letters..." }}
          items={this.props.buyerCompniesList}
          getItemValue={item => item.company_name}
          shouldItemRender={this.matchBuyer}
          renderItem={(item, highlighted) =>
            <div className='col px-2'
              key={item.product_id}
              style={{
                backgroundColor: "white", width: "100%", color: "black", paddingBottom: '5px',
                paddingTop: '5px', maxHeight: '150px', overflowY: 'auto'
              }}
            >
              {item.company_name}
            </div>
          }
           value={this.state.companyName}
          onChange={e => this.handleBuyer(e.target.value)}
          onSelect={value => this.selectedBuyer(value)}
        />
      </div>
    );
  }

  showMenu() {
    if (_.isEmpty(this.props.products)) {
      return (
        "{'none'}"
      );
    }
  }

  matchBuyer(state, value) {
    //console.log(state.company_name);
    return (
      state.company_name.toLowerCase().indexOf(value.toLowerCase()) !== -1
    );
  }

  async handleBuyer(buyer) {
    await this.setState({ companyName: buyer });
    // await this.props.companyNameExt(buyer)
    if (buyer.length > 1) {
      this.props.fetchBuyer(3);
    } else {
      this.props.setProps([], 'doBuyerEmpty');
    }
  }

  async selectedBuyer(buyer, mode = '') {
    if (mode === 'unmount') {
      this.setState({
        companyName: '',
        selectedBuyer: ''
      });
    } else {
      let buyerDetail = {};
      if (buyer != "product not found") {
        this.fetchUnitDetail(buyer);
        await this.setState({ selectedBuyer: buyer });
        this.setState({ companyName: buyer });
        saveLocalStorage('buyerDetail', buyer);
      }
    }
  }

  fetchUnitDetail(value) {
    let company_key_id = getBuyerCompanyKeyId(this.props.buyerCompniesList, value, false);
    saveLocalStorage('BuyerCompanyKeyId', company_key_id);
    if (typeof value !== 'undefined' && value !== '') {
      this.props.fetchBuyerUnit(company_key_id); // buyer unit
      this.props.fetchSellerUnit(); // seller unit
    }
  }
  async handleInitialize() {
    if (!_.isEmpty(this.props.uniqueExternalView.vendors)) {
      let external_vendor = this.props.uniqueExternalView.vendors.seller_company_name
      let initData = {}
      initData = {
        inv_number: this.props.uniqueExternalView.vendors.Invoice_number,
        external_vendor: this.props.uniqueExternalView.vendors.seller_company_name,
        ship_trans: this.props.uniqueExternalView.vendors.shipment_transport_mode,
        order_number: this.props.uniqueExternalView.vendors.order_number,
        inv_date: this.props.uniqueExternalView.vendors.invoice_date,
        ship_date: this.props.uniqueExternalView.vendors.shipment_date,
        ship_desc: this.props.uniqueExternalView.vendors.shipment_description,
      };
      //await this.props.setParentState('inputvalue', this.props.editFormData.product_name);
      this.setState({ initData: initData,
        companyName: external_vendor
      });
      saveLocalStorage('vendorDetails', external_vendor);
      this.props.initialize(initData);
    }
  }

  productFormatter(row, cell) {
    return (
      <ButtonGroup>
        <Button className="action" title="edit" onClick={() => this.onEditProduct(cell)}>{strings.outgoingFormatterText.edit}</Button>
        <Button className="action delete" title="delete" onClick={() => this.onDeleteProduct(cell)}>{strings.outgoingFormatterText.delete}</Button>
      </ButtonGroup>
    );
  }

  async onDeleteProduct(cell) {

    // console.log(cell.ext_vendor_shipment_product_id)
    await this.props.onDeleteExtProd(cell.ext_vendor_shipment_product_id)

    if (this.props.uniqueExternalView.vendors !== undefined) {
      await this.props.viewExternalProduct(this.props.ext_vendor_shipment_id)
    }
    else {
      await this.props.viewExternalProduct(this.props.externalData.data.ext_vendor_shipmentid)
    }


  }
  async onEditProduct(cell) {
    let data = {
      product_description: cell.product_description,
      product_name: cell.product_name,
      product_qty: cell.product_qty,
      product_uom: cell.product_uom,
    }
    await this.setState({
      data: data,
      ext_vendor_shipment_product_id: cell.ext_vendor_shipment_product_id
    })
    await this.props.viewUniqueExternalProduct(cell.ext_vendor_shipment_product_id);
    // await console.log(this.props.uniqueProductView.documents);
    await this.props.setProductStatus(true)
    // console.log(cell)
  }


  fetchAutoProducts(product) {
    this.setState({ inputvalue: product });
    if (product.length > 1) {
      this.props.fetchProducts();
    } else {
      this.props.setProps([], 'doProductEmpty');
    }
  }

  async selectedProduct(product, mode = '') {
    if (mode === 'unmount') {
      this.setState({
        inputvalue: '',
        selectedProduct: ''
      });
    } else {
      let productDetail = {};
      if (product != "product not found") {
        await this.setState({ selectedProduct: product });
        this.setState({ inputvalue: product });
        saveLocalStorage('productDetail', product);
      }
    }
  }
  render() {
    const { handleSubmit, currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);
    const { invoiceNumber, transportMode, invoiceDate } = strings.createShipment;
    const { externalVendor, orderNumber } = strings.vendor;
    return (

      <div>
        <div className='col-md-12 col-lg-12 create-order'>
          {!this.props.isProduct ?
            <div>
              <h2>{}</h2>
              <form onSubmit={handleSubmit(this.onSubmit)}>
                <div className="form-row">
                  {/* {strings.createShipment.addProduct} */}
                  <div className="form-group col-md-4" style={{ paddingLeft: '5px' }}>
                    <Field
                      name="inv_number"
                      type="text"
                      label={`${invoiceNumber} *`}
                      component={renderField}
                      placeholder="Enter invoice No."
                    />
                  </div>
                  <div className="form-group col-md-4" style={{ paddingLeft: '5px' }}>
                    {/* <Field
                      name="external_vendor"
                      type="text"
                      label={`${externalVendor} *`}
                      component={renderField}
                      placeholder="Enter External Vendor"
                    /> */}
                    <div className="autocomplete">
                      <label>External Vendor</label>
                      {this.returnAutoComplete()}
                    </div>
                  </div>
                  <div className="form-group col-md-4">
                    <Field
                      name="ship_trans"
                      type="text"
                      label={`${transportMode} *`}
                      component={renderField}
                      placeholder="Transport Mode"
                    />
                  </div>

                  <div className="form-group col-md-4">
                    <Field
                      name="order_number"
                      type="text"
                      label={orderNumber}
                      component={renderField}
                      placeholder="Enter Order No."
                    />
                  </div>
                  <div className="form-group col-md-4">
                    <label>{`${invoiceDate} *`}</label>
                    <Field
                      name="inv_date"
                      type="number"
                      component={RenderDatePicker}
                    />
                  </div>

                  <div className="form-group col-md-4">
                    <label>{`${strings.shipmentTableText.shipmentDate} *`}</label>
                    <Field
                      name="ship_date"
                      type="number"
                      component={RenderDatePicker}
                    />
                  </div>

                  <div className="form-group col-md-4">
                    <Field
                      name="ship_desc"
                      type="text"
                      label={strings.createOrder.description}
                      component={renderField}
                      placeholder="Enter Description"
                    />
                  </div>
                </div>
                <div>
                  {(this.props.externalData !== "" || this.props.uniqueExternalView.vendors !== undefined) && <button className="btn btn-primary create" onClick={this.onAddProduct} >{`${strings.createShipment.addProduct} +`}</button>}
                </div>
                <div>
                  {/* {(this.props.externalData !== "" || this.props.uniqueExternalView.vendors !== undefined) ? <input type='submit' value='Create  > ' className="btn btn-primary create" style={{ left: '79%', marginBottom: '2px', width: '16%', top: '87%', position: 'absolute' }} />: <input type='submit' value='Create  > ' className="btn btn-primary create" style={{ left: '79%', marginBottom: '2px', width: '16%', top: '84%', position: 'absolute' }} />} */}
                  <SubmitButtons
                    submitLabel={strings.createOrder.create}
                    className='btn btn-primary create'
                    submitting={this.state.submitting}
                  // onClick = { () =>  this.setSubmit}
                  />
                </div>
                {((!_.isEmpty(this.props.productDetail.Products)) && !this.props.isProduct) &&
                  <div className='btn-right'>
                    <Button className="btn btn-primary  create non-cancel btn btn-secondary" onClick={this.props.back} >{strings.captalise.back}</Button>
                  </div>
                }
              </form>
            </div>
            :
            <div>
              <AddExternalProduct
                onSubmit={this.createProducts}
                role={this.props.role}
                //selectedProduct={this.selectedProduct}
                //showMenu={this.showMenu}
                //inputvalue={this.state.inputvalue}
                options={this.props.options}
                products={this.props.products}
                fetchProducts={this.props.fetchProducts}
                fetchAutoProducts={this.fetchAutoProducts}
                resetProducts={this.props.resetProducts}
                setProps={this.props.setProps}
                isProductEdit={this.props.isProductEdit}
                isAddLot={this.props.isAddLot}
                addLot={this.props.addLot}
                viewShipmentProductDetail={this.props.viewShipmentProductDetail}
                viewLotDetail={this.props.viewLotDetail}
                deleProductLot={this.props.deleProductLot}
                setParentState={this.setParentState}
                fetchUOM={this.props.fetchUOM}
                uom={this.props.uom}
                setActivePage={this.props.setActivePage}
                getLotDetail={this.props.getLotDetail}
                currentLanguage={this.props.currentLanguage}
                setProductStatus={this.props.setProductStatus}
                isProduct={this.props.isProduct}
                external_shipment_id={this.props.external_shipment_id}
                addExternalProduct={this.props.addExternalProduct}
                viewExternalProduct={this.props.viewExternalProduct}
                externalData={this.props.externalData}
                productDetail={this.props.productDetail}
                productFormatter={this.props.productFormatter}
                data={this.state.data}
                onUpdateExtProd={this.props.onUpdateExtProd}
                uniqueExternalView={this.props.uniqueExternalView}
                ext_vendor_shipment_product_id={this.state.ext_vendor_shipment_product_id}
                ext_vendor_shipment_id={this.props.ext_vendor_shipment_id}
                uniqueProductView={this.props.uniqueProductView}
                resetUniqueProductDetail={this.props.resetUniqueProductDetail}
                resetExternalData={this.props.resetExternalData}
                back={this.props.back}
                fetchAutoProducts={this.fetchAutoProducts}
                inputvalue={this.state.inputvalue}
                selectedProduct={this.selectedProduct}
                fetchProductsExt={this.props.fetchProductsExt}
                productsExt={this.props.productsExt}
                resetAlertBox={this.props.resetAlertBox}
              />
            </div>
          }


        </div >
        <div className="col-md-12 nopad">
          {((!_.isEmpty(this.props.productDetail.Products)) && !this.props.isProduct) &&
            <div>
              <h2 style={{ paddingLeft: '1%', paddingTop: '1%', marginBottom: '0%' }}>{strings.addProduct.productDetail}</h2>
              <VendorProductTable
                data={this.props.productDetail.Products}
                pagination={false}
                search={false}
                exportCSV={false}
                haveAction={false}
                pages={this.props.pages}
                currentLanguage={this.props.currentLanguage}
                options={options}
                productFormatter={this.productFormatter}
              />
            </div>
          }
        </div>
      </div>
    );
  }
}

ExternalJob.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
};

export default reduxForm({
  form: 'ExternalJob',
  validate,
})(ExternalJob)