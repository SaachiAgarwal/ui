import React from 'react';
import PropTypes from 'prop-types';
import {Button} from 'reactstrap';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import _ from 'lodash';
import FileDownload from 'js-file-download';
const  { DOM: { input, select, textarea } } = React;

import SubmitButtons from 'components/SubmitButtons';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);


export const validate = (values) => {
  const errors = {}
  if (!values.message) {
    errors.message = 'Required'
  }
  return errors
}

export const CsvModalHeader = (props) => {
  return(
    <div>
      <div className="headertext">
        <h2 className="heading-model-new">{strings.permissionModal.placeShipment}</h2>
        {/*<h3 className="text1">Please provide details for declining this shipment.</h3>*/}
      </div>
      <Button className="modal-btn" onClick={() => props.handlePermissionModal(false)}>X</Button>
    </div>
  );
}

/** PermissionModal
 *
 * @description This class having a modal that will display a permission check that you are agreed with terms nd condition
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class PermissionModal extends React.Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);

    this.state = {
      isChecked: false,
      isDisabled: true,
    }
  }

  handleInputChange(value) {
    this.setState({
      isChecked: !this.state.isChecked,
      isDisabled: !this.state.isDisabled,
    });
  }

  componentWillUnmount() {
    this.props.handlePermissionModal(false);
  }

  render() {
    const { handleSubmit } = this.props;
    strings.setLanguage(this.props.currentLanguage);
    const {terms , termsdescription, permission, placeShipment} = strings.permissionModal;
    return(
      <div className="model_popup">
        <div>
          <CsvModalHeader
            handlePermissionModal={this.props.handlePermissionModal}
          />
        </div>
        <div>
          <form onSubmit={handleSubmit}>
            <div>
              <label>{terms} </label>
              <input
                name={permission}
                type="checkbox"
                style={{"marginLeft": "15px"}}
                checked={this.state.isChecked}
                onChange={this.handleInputChange} 
              />
              <h3 className="text1">{termsdescription}</h3>
            </div>
            <div>
              <SubmitButtons
                submitLabel={placeShipment}
                className='btn btn-primary'
                customClass='permission'
                submitting={this.state.isDisabled}
              />
            </div>
          </form>
        </div>
      </div>

    );
  }
}

// export default PermissionModal;

export default reduxForm({
  form: 'PermissionModal',
  validate,
})(PermissionModal)