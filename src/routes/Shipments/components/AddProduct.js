import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
import DropdownList from 'react-widgets/lib/DropdownList';
import 'react-widgets/dist/css/react-widgets.css';
import _ from 'lodash';
import Autocomplete from "react-autocomplete";

import SubmitButtons from 'components/SubmitButtons';
import renderField from './renderField';
import AddLotsTable from './AddLotsTable';
import { getSupplierCompanyKeyId, getCategory, getType, getLocalStorage, saveLocalStorage } from 'components/Helper';
import { Images } from 'config/Config';

//localization
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
let strings = new LocalizedStrings(
  data
);


/** func Validate
 * description validate method will validate all the control fields of form based on provided criteria
 *
 * return Array of Errors
 */
const validate = values => {
  const errors = {}
  if (!values.product_name) {
    errors.product_name = 'Required'
  }
  if (!values.order_number) {
    errors.order_number = 'Required'
  }
  if (!values.product_uom) {
    errors.product_uom = 'Required'
  }
  if (!values.product_qty) {
    errors.product_qty = 'Required'
  }
  // if (!values.product_lot_number) {
  //   errors.product_lot_number = 'Required'
  // }
  if (values.blend_percentage > 100) {
    errors.blend_percentage = 'Invalid percentage'
  }
  if (!values.glm) {
    errors.glm = 'Required'
  }
  if (!values.mpg) {
    errors.mpg = 'Required'
  }
  return errors
}

/** func renderLot
 *
 * @description This method push an object of the defined input controls into fields array to display a field array of 
 *   certain inputs
 *
 * return Array of control fields
 */
const renderLot = ({ fields, customProps, meta: { error, submitFailed } }) => {
  return (
    <div>
      <button type="button" className="btn btn-primary" onClick={() => customProps.addLot('open')}>
        {strings.addLot} <span className="glyphicon glyphicon-plus"></span>
      </button>
      {submitFailed && error && <span>{error}</span>}
    </div>
  );
}

const renderDropdownList = ({ input, data, valueField, textField, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div>
      <DropdownList {...input}
        data={data}
        onChange={input.onChange}
      />
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

/** CreateOrder
 *
 * @description This class is responsible to display a form after create shipment successfully
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.showMenu = this.showMenu.bind(this);
    this.addLotFormatter = this.addLotFormatter.bind(this);
    this.editLot = this.editLot.bind(this);
    this.deleteLot = this.deleteLot.bind(this);
    this.returnAutoComplete = this.returnAutoComplete.bind(this);
    this.matchProducts = this.matchProducts.bind(this);

    this.state = {
      initData: {},
      blendpercentage: "100"
    }
  }

  componentWillMount() {
    this.props.setActivePage('addProduct');
    this.props.fetchUOM('shp');
  }

  componentWillReceiveProps(props) {
    if (typeof this.props.inputvalue !== "undefined" || this.props.inputvalue !== "") {
      this.setState({ errorMsg: "" });
    }
  }

  componentDidMount() {
    if (this.props.isProductEdit)
      this.handleInitialize();
  }

  componentWillUnmount() {
    this.props.setProps(false, 'isProductEdit');
    this.props.resetProducts();
    this.props.selectedProduct('', 'unmount');
  }

  async handleInitialize() {
    if (!_.isEmpty(this.props.editFormData)) {
      const initData = {
        shipment_id: this.props.editFormData.shipment_id,
        shipment_product_id: this.props.editFormData.shipment_product_id,
        product_id: this.props.editFormData.product_id,
        product_name: this.props.editFormData.product_name,
        product_qty: this.props.editFormData.product_qty,
        product_uom: this.props.editFormData.product_uom,
        product_description: this.props.editFormData.product_description,
        product_lot_number: this.props.editFormData.shipment_product_lot_no,
        glm: this.props.editFormData.GLM,
        mpg: this.props.editFormData.MPG,
        blend_percentage: this.props.editFormData.blend_percentage,
      };
      await this.props.setParentState('inputvalue', this.props.editFormData.product_name);
      this.setState({ initData: initData, blendpercentage: this.props.editFormData.blend_percentage });
      this.props.initialize(initData);
    }
  }

  showMenu() {
    if (empty(this.props.products)) {
      return (
        "{'none'}"
      );
    }
  }
  renderBlendPer = ({ input, label, defvalue, type, meta: { touched, error, invalid } }) => (
    <div className={`form-group ${touched && invalid ? 'has-error' : ''}`}>
      <label className="control-label">{label}{(label === 'Glm *' || label === 'Mpg *') && <img className="image-info" src={Images.infoIcon} />}</label>
      <div>
        <input {...input}
          className="form-control"
          type={type}
          value={defvalue}
          autoComplete="off"
          autoCorrect="off"
          spellCheck="off"
          onChange={(e) => { this.setState({ blendpercentage: e.target.value }) }}
        />
        <div className="help-block">
          {touched && (error && <span className="error-danger">
            <i className="fa fa-exclamation-circle">{error}</i></span>)}
        </div>
      </div>
    </div>
  )

  returnAutoComplete() {
    return (
      <div className="customAutoComplete">
        <Autocomplete
          className="form-control"
          ref="autocomplete"
          menuStyle={{ display: this.props.showMenu, minWidth: '100%', }}
          inputProps={{ placeholder: "Start typing three letters..." }}
          items={this.props.products}
          getItemValue={item => item.product_name}
          shouldItemRender={this.matchProducts}
          renderItem={(item, highlighted) =>
            <div className='col px-2'
              key={item.product_id}
              style={{
                backgroundColor: "white", width: "100%", color: "black", paddingBottom: '5px',
                paddingTop: '5px', maxHeight: '150px', overflowY: 'auto'
              }}
            >
              {item.product_name}
            </div>
          }
          value={this.props.inputvalue}
          onChange={e => this.props.fetchAutoProducts(e.target.value)}
          onSelect={value => this.props.selectedProduct(value)}
        />
      </div>
    );
  }

  matchProducts(state, value) {
    return (
      state.product_name.toLowerCase().indexOf(value.toLowerCase()) !== -1
    );
  }

  async editLot(cell) {
    await this.props.getLotDetail(cell.consume_stock_lot_id);
    saveLocalStorage('selectedLotDetail', cell);
    await this.props.setProps(true, 'isLotEdit');
  }

  async deleteLot(cell) {
    let lStorage = getLocalStorage('shipments');
    await this.props.deleProductLot(cell.shipment_product_lot_id);

    // getting the lot detail after delete lot
    await this.props.viewShipmentProductDetail(lStorage.shipment_product_id);
  }

  addLotFormatter(row, cell) {
    return (
      <div>
        <img style={{ position: "absolute", left: "77%", bottom: "10%", width: "3%", borderRadius: "11px" }} src={Images.arrowGif1} />
        <ButtonGroup>
          <Button className="action" onClick={() => this.editLot(cell)} >{strings.outgoingFormatterText.editQuantity}</Button>
          <Button className="action delete" onClick={() => this.deleteLot(cell)} >{strings.outgoingFormatterText.delete}</Button>
        </ButtonGroup>
      </div>

    );
  }

  render() {
    const { handleSubmit, currentLanguage } = this.props;
    let loginData = getLocalStorage('loginDetail'),
      dispbutn = (loginData.role === "Birla Cellulose" || loginData.role === "Pulp") ? true : false
    strings.setLanguage(currentLanguage);


    return (
      <div className='col-md-12 col-lg-12 create-order'>
        <h2>{strings.addProduct.productDetail}</h2>
        <div>
          <form onSubmit={handleSubmit}>
            <div className="form-row">
              <div className="form-group col-md-8">
                {(this.props.role !== 'Brand') ?
                  <div className="autocomplete">
                    <label>{strings.addProduct.product} *</label>
                    {this.returnAutoComplete()}
                  </div>
                  :
                  <Field
                    name='product_name'
                    type="text"
                    component={renderField}
                    label={`${strings.addProduct.product} *`}
                  />
                }
              </div>
              <div className="form-group col-md-4">
                <Field
                  name='product_lot_number'
                  type="text"
                  component={renderField}
                  label={`${strings.addProduct.finishedGoods}`}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='product_qty'
                  type="number"
                  component={renderField}
                  label={`${strings.addProduct.quantity} *`}
                />
              </div>
              <div className="form-group col-md-4">
                <label>{strings.addProduct.unit} *</label>
                <Field
                  name="product_uom"
                  component={renderDropdownList}
                  data={(!_.isEmpty(this.props.uom)) ? this.props.uom.map(function (data) { return data.uom }) : []}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name='product_description'
                  type="text"
                  component={renderField}
                  label={strings.addProduct.productDescription}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name="order_product_id"
                  className="form-control"
                  type="hidden"
                  component={renderField}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name="product_id"
                  className="form-control"
                  type="hidden"
                  component={renderField}
                />
              </div>
            </div>

            {(dispbutn) ? false :
              (<div className="form-row" style={{ marginTop: "-30px" }}>
                <div className="form-group col-md-4">
                  <Field
                    name='blend_percentage'
                    value="100"
                    type="number"
                    component={this.renderBlendPer}
                    label={`${strings.addProduct.blend}`}
                    defvalue={this.state.blendpercentage}
                  />
                </div>
              </div>)}
            {(this.props.isProductEdit) &&
              <div>
                {(!_.isEmpty(this.props.viewLotDetail)) &&
                  <div className="col-md-12 nopad">
                    <h2>{strings.addProduct.selectedLots}</h2>
                    <AddLotsTable
                      data={this.props.viewLotDetail}
                      pagination={false}
                      search={false}
                      exportCSV={false}
                      options={this.props.options}
                      addLotFormatter={this.addLotFormatter}
                      haveAction={true}
                      loginData={loginData}
                      currentLanguage={this.props.currentLanguage}
                    />
                  </div>
                }
                <FieldArray
                  name="products"
                  component={renderLot}
                  customProps={{
                    addLot: this.props.addLot,
                  }}
                />
              </div>
            }
            <div>
               
                <SubmitButtons
                  submitLabel={strings.setting.add}
                  className='btn btn-primary create'
                  submitting={this.props.submitting}
                />
              
            </div>
          </form>
        </div>
      </div>
    );
  }
}

AddProduct.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
};

export default reduxForm({
  form: 'AddProduct',
  validate,
})(AddProduct)