import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';

import LinkingOrderTable from './LinkingOrderTable';
import RenderDatePicker from 'components/RenderDatePicker';
import {getLocalStorage} from 'components/Helper';
import { Images } from 'config/Config';

export const LinkModalHeader = (props) => {
  return(
    <Button className="modal-btn" onClick={() => props.handleLinkModal('close')}>X</Button>
  );
}

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);


/** LinkModal
 *
 * @description This class having a modal that will display a list of incoming orders and having a filter on that list
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class LinkModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      buyerCompanyKeyId: ''
    }
    this.checkAssetOrder = this.checkAssetOrder.bind(this)
  }

  async componentWillMount() {
    let buyerCompanyKeyId = getLocalStorage('BuyerCompanyKeyId');
    await this.setState({buyerCompanyKeyId: buyerCompanyKeyId});
    await this.props.getBuyerIncomingOrders(buyerCompanyKeyId,this.props.options.currentPage, this.props.options.modalPageSize, this.props.fromDt, this.props.toDt);
  }
  checkAssetOrder(row, cell) {
    console.log(cell)
    return (
      <ButtonGroup>
        {(!_.isEmpty(cell.asset_id)) && <img title="This order is linked to a brand's order" style={{width: '15px'}} src={Images.assetIdImage}  />}
      </ButtonGroup>
      
    );
  }

  componentWillUnmount() {
    this.props.resetFilter();
  }

  render() {
    strings.setLanguage(this.props.currentLanguage);
    const { handleSubmit } = this.props;
    const {select, From , To} = strings.OrderFilterText;
     return(
      <div className="modal">
        <Row>
          <LinkModalHeader 
            handleLinkModal={this.props.handleLinkModal}
          />
        </Row>
        <Row>
          <form onSubmit={handleSubmit}>
            <div className='col-12 col-sm-12 col-xl-12 mb-2'>
              <Row>
                <Col className="text-center date-picker col-6 col-sm-6 col-xl-6 pr-1">
                  <label>{From}</label>
                  <Field
                    name='from_date'
                    type='date'
                    component={RenderDatePicker}
                    label='Select'
                    onChange={(date) => this.props.handleDateChange(date, 'fromDate')}
                  />
                </Col>
                <Col className="text-center date-picker col-6 col-sm-6 col-xl-6 pr-1">
                  <label>{To}</label>
                  <Field
                    name='to_date'
                    type='date'
                    component={RenderDatePicker}
                    label={select}
                    onChange={(date) => this.props.handleDateChange(date, 'toDate')}
                  />
                </Col>
              </Row>
            </div>
          </form>
        </Row>
        <LinkingOrderTable
          data={this.props.orders}
          pagination={false}
          search={false}
          exportCSV={false}
          isForModal={false}
          linkingFormatter={this.props.linkingFormatter}
          getBuyerIncomingOrders={this.props.getBuyerIncomingOrders}
          buyerCompanyKeyId={this.state.buyerCompanyKeyId}
          pages={this.props.pages}
          currentLanguage = {this.props.currentLanguage}
          checkAssetOrder = {this.checkAssetOrder}
          setCurrentLotNewPage={this.props.setCurrentLotNewPage}
          currentNewLotPage={this.props.currentNewLotPage}
        />
      </div>
    );
  }
}

export default reduxForm({
  form: 'LinkModal',
})(LinkModal)