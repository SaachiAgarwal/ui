import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Pagination from 'react-paginate';

//localization
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
let strings = new LocalizedStrings(
  data
);


// import Pagination from 'components/Pagination';
import {Images} from 'config/Config';

/** IncomingShipmentTable
 *
 * @description This class is responsible to display a orders list of incoming orders
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class IncomingShipmentTable extends Component {
  constructor(props) {
    super(props);
    // this.handlePageClick = this.handlePageClick.bind(this);
    // this.rowStyleFormat = this.rowStyleFormat.bind(this);
    // //props.setCurrentNewPage(1)
    // this.state = {
    //   pageCount: 2
    // }
  }

//   componentWillMount() {
//     this.props.setActivePage('inshipments');
//     //this.props.setCurrentNewPage(1)
//   }
//   handlePageClick(value) {
//     this.props.fetchShipment('incomingShipment', value.selected+1, this.props.options.pageSize, '', '', this.props.searchText);
//     this.props.setCurrentNewPage(value.selected+1)
//   }

//   rowStyleFormat(row, rowIdx) {
//     return { fontWeight: row !== undefined && !row.is_read ? 'bold' : '' };
//   }

  render() {
    const { currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);

    return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options} trStyle={this.rowStyleFormat}>
          <TableHeaderColumn dataField='shipment_id' hidden={true}>Shipment Id</TableHeaderColumn>
          <TableHeaderColumn dataField='invoice_number' isKey={true}>{strings.shipmentTableText.invoiceNumber}</TableHeaderColumn>
          <TableHeaderColumn dataField='seller_company_name' columnTitle={true}>{strings.shipmentTableText.supplier}</TableHeaderColumn>
          <TableHeaderColumn dataField='new_invoice_date'>{strings.shipmentTableText.shipmentDate}</TableHeaderColumn>
          <TableHeaderColumn dataField='linked_PO'>{strings.shipmentTableText.linkedPO}</TableHeaderColumn>
          <TableHeaderColumn dataField='Status'>{strings.shipmentTableText.shipmentStatus}</TableHeaderColumn>
          <TableHeaderColumn dataField='buyer_company_name' columnTitle={true} >{strings.shipmentTableText.buyer}</TableHeaderColumn>
          <TableHeaderColumn key="editAction" dataFormat={this.props.incomingFormatter}>{strings.shipmentTableText.action}</TableHeaderColumn>
        </BootstrapTable>
        <div className="pagination-box">
          <Pagination
            initialPage = {this.props.currentNewPage-1}
            previousLabel={strings.prev}
            nextLabel={strings.next}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={this.props.pages}
            pageRangeDisplayed={5}
            onPageChange={this.handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        </div>
      </div>        
    );
  }
}

IncomingShipmentTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default IncomingShipmentTable;