/** ErrorsContainer
 *
 * @description This is container that manages the states for all order related functionality.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
import { connect } from 'react-redux';

import Maintenance from '../components/Maintenance';
import {resetAlertBox} from '../modules/maintenance.js';
import {setGlobalState} from '../../../store/app';

const mapStateToProps = (state) => {
  return({
    navIndex: state.app.navIndex,
    navInnerIndex: state.app.navInnerIndex,
    navClass: state.app.navClass,
    fetching: state.Maintenance.fetching,
    error: state.Maintenance.error,
    showAlert: state.Maintenance.showAlert,
    alertMessage: state.Maintenance.alertMessage,
    status: state.Maintenance.status,
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    setGlobalState: (value) => dispatch(setGlobalState(value)),
    resetAlertBox: (showAlert, message) => dispatch(resetAlertBox(showAlert, message)),
  });
};

export default connect(mapStateToProps, mapDispatchToProps)(Maintenance);
