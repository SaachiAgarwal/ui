import { injectReducer } from '../../store/reducers';

export default (store) => ({
  path: 'maintenance',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const Maintenance = require('./containers/MaintenanceContainer').default;
      const reducer = require('./modules/maintenance').default;
      injectReducer(store, { key: 'Maintenance', reducer });
      cb(null, Maintenance);
  }, 'Maintenance');
  },
});
