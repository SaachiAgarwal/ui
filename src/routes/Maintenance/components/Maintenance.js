import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Button, Row } from 'reactstrap';

import {Url} from 'config/Config';
import NavBar from 'components/navbar/NavBar';
import Loader from 'components/Loader';
import Alert from "components/Alert";
import HeaderSection from 'components/HeaderSection';
import NavDrawer from 'components/NavDrawer';
import Footer from 'components/Footer';
import {getLocalStorage, removeLocalStorage} from 'components/Helper';
import 'css/style.scss';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);


/** Maintenance
 *
 * @description This class is a based class for Maintenance functionality
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class Maintenance extends React.Component {
  constructor(props) {
    super(props);
    this.setActivePage = this.setActivePage.bind(this);
    this.handleAlertBox = this.handleAlertBox.bind(this);
    this.handleInboxOutboxOrders = this.handleInboxOutboxOrders.bind(this);

    this.state = {
      active: 'inbox',
      role: '',
      isSuperUser: '',
      user: {},
    }
  }

  async componentWillMount() {
    let loginDetail = getLocalStorage('loginDetail');
    this.props.setGlobalState({
      navIndex: 0,
      navInnerIndex: (loginDetail.role === 'Brand') ? 1 : 0,
      navClass: 'Orders'
    });
    this.setState({
      user: loginDetail,
      role: loginDetail.role,
      isSuperUser: loginDetail.userInfo.is_super_user
    });
  }

  

  setActivePage(status) {
    this.setState({active: status});
  }

  async handleAlertBox(){
    this.props.resetAlertBox(false, "");
  }

  handleInboxOutboxOrders(classType, innerType) {
    if (classType === 'orders' && innerType === 'inbox') {
    } else if (classType === 'orders' && innerType === 'outbox') {
    } else if (classType === 'shipments' && innerType === 'inbox') {
    } else if (classType === 'Audit Trail') {
    } else if (classType === 'logout') {
      removeLocalStorage('loginDetail');
      browserHistory.push(Url.HOME_PAGE);
    }
  }

  render() {
    return (
      <div>
        <Loader loading={this.props.fetching} />
        <Alert
          showAlert={(typeof this.props.showAlert !== "undefined" ? this.props.showAlert : false)}
          message={(typeof this.props.alertMessage !== "undefined" ? this.props.alertMessage : "")}
          handleAlertBox={this.handleAlertBox}
          status={this.props.status}
        />
        <div className="container-fluid">
          <div className="row">
            {/*<div className="col-md-2 col-lg-2 sidebar">
              <NavDrawer
                role={this.state.role}
                isSuperUser={this.state.isSuperUser}
                setGlobalState={this.props.setGlobalState}
                navIndex={this.props.navIndex}
                navInnerIndex={this.props.navInnerIndex}
                navClass={this.props.navClass}
                handleInboxOutboxOrders={this.handleInboxOutboxOrders}
              />
            </div>*/}
            <div className="col-md-10 col-lg-10 col-lg-offset-2 col-md-offset-2 main">
              <h3 style={{"textAlign": "center","paddingRight": "260px","fontWeight": "bold"}}>This site is under maintenance</h3>
              {/*<HeaderSection 
                active={this.state.active}
                searchText={this.state.searchText}
                handleSearch={this.handleSearch}
              />
               <div className="clearfix"></div>
              <Footer />*/}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Maintenance;