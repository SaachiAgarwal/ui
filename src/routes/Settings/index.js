import { injectReducer } from '../../store/reducers';
import {checkPageRestriction} from  '../index';

export default (store) => ({
  path: 'settings',
  onEnter: (nextState, replace) => {
    checkPageRestriction(nextState, replace, () => {})
  },
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const Settings = require('./containers/SettingsContainer').default;
      const reducer = require('./modules/settings').default;
      injectReducer(store, { key: 'Settings', reducer });
      cb(null, Settings);
  }, 'Settings');
  },
});
