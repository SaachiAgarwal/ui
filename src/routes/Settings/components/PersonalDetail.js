import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Row, Col, Label} from 'reactstrap';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';

import renderField from './renderField';
import SubmitButtons from 'components/SubmitButtons';
import {getUsreInfo} from 'components/Helper';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
let strings = new LocalizedStrings(
  data
);


class PersonalDetail extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    let userInfo = getUsreInfo();
    this.setState({'userInfo': userInfo});
  }

  render() {
    const { handleSubmit, currentLanguage } = this.props;
    const { userInfo } = this.state;
    strings.setLanguage(currentLanguage);
    const {fullName, organization, organizationType, phoneNumber, emailId} = strings.setting;
 
    return (
      <div className="setting-detail">
        <h2>{fullName} &nbsp;<span className="detail-span">{userInfo.full_name}</span></h2>
        <h2>{organization} &nbsp;<span className="detail-span">{userInfo.company_name}</span></h2>
        <h2>{organizationType} &nbsp;<span className="detail-span">{userInfo.company_type_name}</span></h2>
        <h2>{phoneNumber} &nbsp;<span className="detail-span">{userInfo.phone_number}</span></h2>
        <h2>{emailId} &nbsp;<span className="detail-span">{userInfo.email}</span></h2>
      </div>
    );
  }
}

PersonalDetail.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
};

export default PersonalDetail;