import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Row, Col, Label} from 'reactstrap';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';

import renderField from './renderField';
import SubmitButtons from 'components/SubmitButtons';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
let strings = new LocalizedStrings(
  data
);


/** func Validate
 * description validate method will validate all the control fields of form based on provided criteria
 *
 * return Array of Errors
 */
const validate = values => {
  const errors = {}
  if (!values.old_password) {
    errors.old_password = 'Required'
  }
  if (!values.new_password) {
    errors.new_password = 'Required'
  }
  if (!values.confirm_password) {
    errors.confirm_password = 'Required'
  } else if (values.confirm_password !== values.new_password) {
    errors.confirm_password = 'Password does not match'
  }
  return errors
}

class PasswordSetting extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { currentLanguage, handleSubmit } = this.props;
    strings.setLanguage(currentLanguage);
    const { oldPassword,newPassword,confirmPassword  } = strings.setting;
    return (
      <div>
        <form onSubmit={handleSubmit}>
          <div className="form-row">
            <div className="form-group col-md-4">
              <Field
                name='old_password'
                type="text"
                component={renderField}
                label={oldPassword}
              />
            </div>
            <div className="form-group col-md-4">
              <Field
                name='new_password'
                type="text"
                component={renderField}
                label={newPassword}
              />
            </div>
            <div className="form-group col-md-4">
              <Field
                name='confirm_password'
                type="text"
                component={renderField}
                label={confirmPassword}
              />
            </div>
          </div>
          <div>
            <SubmitButtons
              submitLabel={strings.change}
              className='btn btn-primary create'
              submitting={this.props.submitting}
            />
          </div>
        </form>
      </div>
    );
  }
}

PasswordSetting.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
};

export default reduxForm({
  form: 'PasswordSetting',
  validate
})(PasswordSetting)