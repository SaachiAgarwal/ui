import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Button } from 'reactstrap';
import _ from 'lodash';

import Loader from 'components/Loader';
import Alert from "components/Alert";
import HeaderSection from 'components/HeaderSection';
import NavDrawer from 'components/NavDrawer';
import { getLocalStorage } from 'components/Helper';
import PasswordSetting from './PasswordSetting';
import PersonalDetail from './PersonalDetail';
import Footer from 'components/Footer';
import 'css/style.scss';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
let strings = new LocalizedStrings(
  data
);


/** Settings
 *
 * @description This class is a based class for settings stuff
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.handleAlertBox = this.handleAlertBox.bind(this);
    this.handleInboxOutboxOrders = this.handleInboxOutboxOrders.bind(this);
    this.changePassword = this.changePassword.bind(this);

    this.state = {
      active: 'settings',
      role: '',
      isSuperUser: '',
    }
  }

  async componentWillMount() {
    let loginDetail = getLocalStorage('loginDetail');
    await this.setState({
      role: loginDetail.role,
      isSuperUser: loginDetail.userInfo.is_super_user
    });
    this.props.setGlobalState({
      navIndex: 5,
      navInnerIndex: 0,
      navClass: 'Settings',
    });
  }

  async handleAlertBox() {
    this.props.resetAlertBox(false, "");
  }

  handleInboxOutboxOrders(classType, innerType) {
    if (classType === 'orders' && innerType === 'inbox') {
    } else if (classType === 'orders' && innerType === 'outbox') {
    } else if (classType === 'shipments' && innerType === 'inbox') {
    } else if (classType === 'Audit Trail') {
    } else if (classType === 'logout') {
      this.props.setCurrentLanguage('en');
      removeLocalStorage('loginDetail');
      browserHistory.push(Url.HOME_PAGE);
    }
  }

  changePassword(values) {
    let lStorage = getLocalStorage('loginDetail');
    values.user_id = lStorage.userInfo.user_id;
    this.props.updatePassword(values);
  }

  render() {
    const {currentLanguage} =  this.props;
    strings.setLanguage(currentLanguage);
    const {personalDetail, changePassword} = strings.setting
     return (
      <div>
        <Loader loading={false} />
        <Alert
          showAlert={(typeof this.props.showAlert !== "undefined" ? this.props.showAlert : false)}
          message={(typeof this.props.alertMessage !== "undefined" ? this.props.alertMessage : "")}
          handleAlertBox={this.handleAlertBox}
          status={this.props.status}
        />
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-2 col-lg-2 sidebar">
              <NavDrawer
                role={this.state.role}
                isSuperUser={this.state.isSuperUser}
                setGlobalState={this.props.setGlobalState}
                navIndex={this.props.navIndex}
                navInnerIndex={this.props.navInnerIndex}
                navClass={this.props.navClass}
                handleInboxOutboxOrders={this.handleInboxOutboxOrders}
                currentLanguage = {this.props.currentLanguage} 
                setInnerNav = {this.props.setInnerNav}
                setParam = {this.props.setParam}
                setSearchString = {this.props.setSearchString}
                setReportFetching = {this.props.setReportFetching}
              />
            </div>
            <div className="col-md-10 col-lg-10 col-lg-offset-2 col-md-offset-2 main">
              <HeaderSection
                active={this.state.active}
                currentLanguage={this.props.currentLanguage}
                setCurrentLanguage={this.props.setCurrentLanguage}
              />
              <h2>{strings.setting.personalDetail}</h2>
              <PersonalDetail
                currentLanguage={this.props.currentLanguage}

              />
              <h2>{strings.setting.changePassword}</h2>
              <PasswordSetting
                onSubmit={this.changePassword}
                currentLanguage={this.props.currentLanguage}
              />
              <div className="clearfix"></div>
              <Footer 
                currentLanguage = {this.props.currentLanguage}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Settings.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default Settings;