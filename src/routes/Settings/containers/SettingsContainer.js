/** SettingsContainer
 *
 * @description This is container that manages the states for all settings related stuff.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */

import { connect } from 'react-redux';

import Settings from '../components/Settings';
import {updatePassword, resetAlertBox} from '../modules/settings';
import {setReportFetching,setSearchString,setParam,setInnerNav,setGlobalState, setCurrentLanguage} from '../../../store/app';

const mapStateToProps = (state) => {
  return({
    navIndex: state.app.navIndex,
    navInnerIndex: state.app.navInnerIndex,
    navClass: state.app.navClass,
    fetching: state.Settings.fetching,
    error: state.Settings.error,
    showAlert: state.Settings.showAlert,
    alertMessage: state.Settings.alertMessage,
    status: state.Settings.status,
    currentLanguage : state.app.currentLanguage,

  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    setGlobalState: (value) => dispatch(setGlobalState(value)),
    resetAlertBox: (showAlert, message) => dispatch(resetAlertBox(showAlert, message)),
    updatePassword: (value) => dispatch(updatePassword(value)),
    setCurrentLanguage : (language) => dispatch(setCurrentLanguage(language)),
    setInnerNav: (lang) => dispatch(setInnerNav(lang)),
    setParam: (lang) => dispatch(setParam(lang)),
    setSearchString: (lang) => dispatch(setSearchString(lang)),
    setReportFetching: (lang) => dispatch(setReportFetching(lang)),

  });
};

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
