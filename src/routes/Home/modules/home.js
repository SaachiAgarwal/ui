import axios from 'axios';
import Config from 'config/Config';
// ------------------------------------
// Constant
// ------------------------------------
// ------------------------------------
// Actions
// ------------------------------------

// ------------------------------------
// Action creators
// ------------------------------------

// ------------------------------------
// Action handlers
// ------------------------------------
const ACTION_HANDLERS = {
}

export const action = {
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
};

export default function homeReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
