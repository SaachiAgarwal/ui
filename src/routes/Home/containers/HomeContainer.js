import { connect } from 'react-redux';

import HomeView from '../components/HomeView';

const mapStateToProps = (state) => {
  return({
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
  });
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeView);
