import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';

import NavBar from 'components/navbar/NavBar';
import Data from 'components/navbar/Data';
import InnerHeader from 'components/InnerHeader';
import './HomeView.scss';

class HomeView extends React.Component {
  constructor(props) {
    super(props);
    this.goTo = this.goTo.bind(this); 
  }

  goTo() {
    browserHistory.goBack();
  }

  render() {
    return(
      <div className="base-container">
        <InnerHeader goTo={this.goTo} />
        <NavBar items={Data}/>
        {'Do Some Stuff Here'}
      </div>
    );
  }
}

export default HomeView;

