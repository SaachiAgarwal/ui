/** scan reducer
 *
 * @description This is a reducer responsible for all the QR code functionality
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
import axios from 'axios';
import Config from 'config/Config';
// ------------------------------------
// Constant
// ------------------------------------
// ------------------------------------
// Actions
// ------------------------------------
// ------------------------------------
// Action creators
// ------------------------------------

// ------------------------------------
// Action handlers
// ------------------------------------
const ACTION_HANDLERS = {
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
};

export default function scanReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
