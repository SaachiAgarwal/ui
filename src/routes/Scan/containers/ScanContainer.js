/** ScanContainer
 *
 * @description This is container that manages the QR code reading functionality.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
import { connect } from 'react-redux';

import Scanner from '../components/Scanner';
import {setInnerNav,setParam,setSearchString,setReportFetching,setGlobalState, fetchUOM, uploadFile,setCurrentLanguage} from '../../../store/app';

const mapStateToProps = (state) => {
  return({
    navIndex: state.app.navIndex,
    navInnerIndex: state.app.navInnerIndex,
    navClass: state.app.navClass,
    currentLanguage : state.app.currentLanguage,
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    setGlobalState: (innerIndex) => dispatch(setGlobalState(innerIndex)),
    setCurrentLanguage : (language) => dispatch(setCurrentLanguage(language)),
    setInnerNav: (lang) => dispatch(setInnerNav(lang)),
    setParam: (lang) => dispatch(setParam(lang)),
    setSearchString: (lang) => dispatch(setSearchString(lang)),
    setReportFetching: (lang) => dispatch(setReportFetching(lang)),

    
  });
};

export default connect(mapStateToProps, mapDispatchToProps)(Scanner);
