import { injectReducer } from '../../store/reducers';
import {checkPageRestriction} from '../index';

export default (store) => ({
  path: 'scan',
  onEnter: (nextState, replace) => {
    checkPageRestriction(nextState, replace, () => {})
  },
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const Scan = require('./containers/ScanContainer').default;
      const reducer = require('./modules/scan').default;
      injectReducer(store, { key: 'Scan', reducer });
      cb(null, Scan);
  }, 'Scan');
  },
});
