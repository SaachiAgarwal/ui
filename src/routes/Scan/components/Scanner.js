import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Button } from 'reactstrap';
import QrReader from "react-qr-reader";

import HeaderSection from 'components/HeaderSection';
import NavDrawer from 'components/NavDrawer';
import {getLocalStorage} from 'components/Helper';
import Config from 'config/Config';
import 'css/style.scss';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);


/** Scanner
 *
 * @description This class is a based class for orders functionality
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class Scanner extends React.Component {
  constructor(props) {
    super(props);
    this.handleInboxOutboxOrders = this.handleInboxOutboxOrders.bind(this);
    this.handleScanner = this.handleScanner.bind(this);
    this.handleScan = this.handleScan.bind(this);
    this.handleError = this.handleError.bind(this);

    this.state = {
      role: '',
      isSuperUser: '',
      active: 'actual',
      open: false,
      delay: 300,
      result: "No result",
    }
  }

  componentWillMount() {
    this.props.setGlobalState({
      navIndex: 14,
      navInnerIndex: 0,
      navClass: 'Scan QR Code'
    });

    let loginDetail = getLocalStorage('loginDetail');
    this.setState({
      role: (loginDetail.userInfo.is_super_user) ? 'Admin' : loginDetail.role,
      isSuperUser: loginDetail.userInfo.is_super_user,
      active: 'scan'
    })
  }

  async handleScanner() {
    this.setState((state) => ({open: !state.open}));
  }

  handleScan(data) {
    if (data) {
      this.setState({
        result: data
      });
    }
    if (!_.isEmpty(data)) {
      let endpoint = 'audit?asset_id='+data;
      let slug = Config.url + endpoint;
      browserHistory.push(slug);
    }
  }

  handleError(err) {
    console.error(err);
  }

  handleInboxOutboxOrders(classType, innerType) {
    if (classType === 'orders' && innerType === 'inbox') {
    } else if (classType === 'orders' && innerType === 'outbox') {
    } else if (classType === 'shipments' && innerType === 'inbox') {
    } else if (classType === 'shipments' && innerType === 'outbox') {
    } else if (classType === 'logout') {
      removeLocalStorage('loginDetail');
      browserHistory.push(Url.HOME_PAGE);
    }
  }

  render() {
    strings.setLanguage(this.props.currentLanguage);

    return (
      <div>
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-2 col-lg-2 sidebar">
              <NavDrawer
                role={this.state.role}
                isSuperUser={this.state.isSuperUser}
                setGlobalState={this.props.setGlobalState}
                navIndex={this.props.navIndex}
                navInnerIndex={this.props.navInnerIndex}
                navClass={this.props.navClass}
                handleInboxOutboxOrders={this.handleInboxOutboxOrders}
                currentLanguage = {this.props.currentLanguage} 
                setInnerNav = {this.props.setInnerNav}
                setParam = {this.props.setParam}
                setSearchString = {this.props.setSearchString}
                setReportFetching = {this.props.setReportFetching}
              />
            </div>
            <div className="col-md-10 col-lg-10 col-lg-offset-2 col-md-offset-2 main">
              <HeaderSection 
                active={this.state.active}
                currentLanguage = {this.props.currentLanguage} 
                setCurrentLanguage = {this.props.setCurrentLanguage}
              />
              <Button className='scan-button' onClick={() => this.handleScanner()}>{strings.scan.scans}</Button>
              {(this.state.open) &&
                <div className="col-md-6 col-md-offset-2">
                  <QrReader
                    delay={this.state.delay}
                    onError={this.handleError}
                    onScan={this.handleScan}
                    style={{ width: "100%" }}
                  />
                </div>
              }
              {/*<p>{this.state.result}</p>*/}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Scanner;