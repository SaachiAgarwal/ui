import { injectReducer } from '../../store/reducers';
import {checkPageRestriction} from  '../index';

export default (store) => ({
  path: 'qa',
  onEnter: (nextState, replace) => {
    checkPageRestriction(nextState, replace, () => {})
  },
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const Qa = require('./containers/QaContainer').default;
      const reducer = require('./modules/qa').default;
      injectReducer(store, { key: 'Qa', reducer});
      cb(null, Qa);
  }, 'Qa');
  },
});
