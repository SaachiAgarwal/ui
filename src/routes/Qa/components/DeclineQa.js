import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import _ from 'lodash';
import FileDownload from 'js-file-download';

import SubmitButtons from 'components/SubmitButtons';
import RenderTextArea from 'components/RenderTextArea';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);


export const validate = (values) => {
  const errors = {}
  if (!values.message) {
    errors.message = 'Required'
  }
  return errors
}

export const CsvModalHeader = (props) => {
  const buttons = ['declineBtn', 'cancelBtn'];
  return (
    <div className="headertext">
      <div>
        <h2 className="heading-model-new">Decline QA</h2>
        <h3 className="text1">Decline Reason</h3>
      </div>

      <Button className="modal-btn" onClick = {props.onClose}>X</Button>
    </div>
  );
}

/** LinkModal
 *
 * @description This class having a modal that will display a list of incoming orders and having a filter on that list
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class DeclineQa extends React.Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeReason = this.onChangeReason.bind(this);
    this.onClose = this.onClose.bind(this);
    this.state = {
      reason: ''
    }
  }

  componentWillMount() {
    //this.props.qaCompanyDetail(this.props.company_key_id);
  }

  componentWillUnmount() {
    this.props.isDeclineQaDetails(false);
    this.props.qaCompanyDetail(this.props.company_key_id);
    
  }

  async onChangeReason(e){
    this.setState({
      reason: e.target.value
    })
  }

  async onClose(){
    await this.props.qaCompanyDetail(this.props.company_key_id);
    await this.props.isDeclineQaDetails(false);
  }
  

  async onSubmit(e){
    e.preventDefault()
    // console.log("Submitted");
    // console.log(this.props.shipment_document_id)
    // console.log(this.state.reason);
    await this.props.qaRejectDetail(this.props.shipment_document_id, this.state.reason);
    await this.props.qaCompanyDetail(this.props.company_key_id);
    await this.props.isDeclineQaDetails(false);
  }

  render() {
    // strings.setLanguage(this.props.currentLanguage);
    // const { handleSubmit, onChange, viewShipmentDetail, btnType } = this.props;
    // const { status, rejected, date, reason } = strings.declineModal;
    return (
      <div className="model_popup">
        <div>
          <CsvModalHeader
          onClose = {this.onClose}
          />
        </div>
        <div>
          <form onSubmit={this.onSubmit}>
            <div style={{paddingTop: '11px'}}>
              {/* <Field
                name='message'
                className='form-control'
                component={RenderTextArea}
                placeholder={"Type here..."}
              /> */}
              <input type="text"
                name='message'
                className='form-control'
                placeholder={"Reason..."}
                onChange = {(e) => this.onChangeReason(e)}
              />
            </div>
            <div>
              {/* <SubmitButtons
                submitLabel={"Decline Document"}
                className='btn btn-primary create'
                submitting={this.props.submitting}
              /> */}
              <div className="form-group" style={{paddingTop: '15px', paddingLeft: '69%'}}>
                <input type='submit' value='Decline Shipment >' className="btn btn-primary" />
              </div>

            </div>
          </form>

        </div>
      </div>
    );
  }
}

export default reduxForm({
  form: 'DeclineQa',
  validate,
})(DeclineQa)