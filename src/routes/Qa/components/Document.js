import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Button } from 'reactstrap';
import _ from 'lodash';
import DropdownList from 'react-widgets/lib/DropdownList';
import 'react-widgets/dist/css/react-widgets.css';

import SubmitButtons from 'components/SubmitButtons';
import { getCompanyTypeId, getCompanyId } from 'components/Helper';
import QaUploadTableDetail from './QaUploadTableDetail'
import DeclineQa from './DeclineQa'
import Modal from 'react-modal';
import options from 'components/options';

//localization 
import LocalizedStrings from 'react-localization';
import data from 'localization/data';
let strings = new LocalizedStrings(
  data
);


const customStyles = {
  content: {
    top: '0',
    left: '0',
    right: '0',
    bottom: '0',
    marginRight: '0',
    transform: 'translate(0, 0)'
  }
};

const renderCompanyType = ({ input, label, data, valueField, textField, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className={`form-group`}>
      <label className="control-label">{label}</label>
      <div>
        <DropdownList {...input}
          data={data}
          onChange={input.onChange}
          onSelect={customProps.fetchCompanyType(input.value)}
        />
        {submitFailed && error && <span className="error-danger">{error}</span>}
      </div>
    </div>
  );
}

const renderDropdownList = ({ input, label, data, valueField, textField, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className={`form-group`}>
      <label className="control-label">{label}</label>
      <div>
        <DropdownList {...input}
          data={data}
          onChange={input.onChange}
          onSelect={customProps.fetchCompanyDetails(input.value)}
        />
        {submitFailed && error && <span className="error-danger">{error}</span>}
      </div>
    </div>
  );
}

class Document extends React.Component {
  constructor(props) {
    super(props);
    this.fetchCompanyType = this.fetchCompanyType.bind(this);
    this.fetchCompanyDetails = this.fetchCompanyDetails.bind(this);
    //   this.handleEmailChange = this.handleEmailChange.bind(this);
    this.viewQaCompanyDetail = this.viewQaCompanyDetail.bind(this);
    this.qaApproveFormatter = this.qaApproveFormatter.bind(this);
    this.qaApprove = this.qaApprove.bind(this);
    this.qaReject = this.qaReject.bind(this);
    this.qaView = this.qaView.bind(this);
    // this.qaFormatter = this.qaFormatter.bind(this);
    this.state = {
      company_key_id: '',
      isDecline: 0,
      shipment_document_id: ''
    }
    this.getData = this.getData.bind(this)
  }

  componentDidMount() {
    this.getData()
  }

  async getData() {
    await this.props.getCompanyType()
    await console.log(this.props.companyType)
  }

  fetchCompanyType(value) {
    if (typeof value !== 'undefined' && value !== '') {
      let company_type_id = getCompanyTypeId(this.props.companyType, value, false);
      this.props.getCompany(company_type_id);
    }
  }

  fetchCompanyDetails(value) {
    if (typeof value !== 'undefined' && value !== '') {
      let company_key_id = getCompanyId(this.props.companyDetail, value, false);
      console.log(company_key_id)
      this.setState({
        company_key_id: company_key_id
      })
      //this.props.qaCompanyDetail(company_key_id);
    }
  }

  async viewQaCompanyDetail() {
    await this.props.resetQaComany('')
    await this.props.qaCompanyDetail(this.state.company_key_id)
  }

  qaApproveFormatter(row, cell) {
    return (
      <ButtonGroup >
        <Button className="action" title="view" style={{ paddingRight: '0px', paddingLeft: '1px' }} onClick={() => this.qaView(cell)} >{strings.incomingFormatterText.view}</Button>
        {cell.is_approved === "Not Responded" && <Button className="action" title="approve" style={{ paddingRight: '0px' }} onClick={() => this.qaApprove(cell)} >{strings.approve}</Button>}
        {cell.is_approved === "Not Responded" && <Button className="action delete" title="reject" style={{ paddingRight: '0px' }} onClick={() => this.qaReject(cell)}>{strings.reject}</Button>}
      </ButtonGroup>
    );
  }
  // qaFormatter(row, cell) {
  //   return (
  //     <ButtonGroup>
  //       <Button className="action" title="view" onClick={() => this.qaDownload(cell)} >view</Button>
  //       <Button className="action delete" title="delete" onClick={() => this.qaDelete(cell)}>delete</Button>
  //     </ButtonGroup>
  //   );
  // }
  async qaApprove(cell) {
    // console.log(cell)
    await this.props.qaApproveDetail(cell.shipment_document_id)
    await this.props.qaCompanyDetail(this.state.company_key_id);
  }
  async qaReject(cell) {
    // console.log(cell)
    await this.props.isDeclineQaDetails(true)
    this.setState({
      isDecline: 1,
      shipment_document_id: cell.shipment_document_id
    })
  }
  async qaView(cell) {
    // console.log(cell)
    //await this.props.getDocumentName(cell.document_name)
    await this.props.qaDownloadDoc(cell.document_file_name, cell.document_name)
  }

  render() {
    strings.setLanguage(this.props.currentLanguage);
    const {selectCompany, companyType} = strings.setting;
    return (
      <div>
        <div className="form-row" style={{ paddingTop: '18px', paddingLeft: '14px' }}>
          <div className="form-group col-md-4">
            <Field
              className="select-menu"
              name="company_type"
              label={`${companyType} *`}
              component={renderCompanyType}
              data={(!_.isEmpty(this.props.companyType)) ? this.props.companyType.map(function (list) { return list.company_type_name }) : []}
              customProps={{
                fetchCompanyType: this.fetchCompanyType
              }}
            />
          </div>
          <div className="form-group col-md-4">
            <Field
              className="select-menu"
              name="company_key_id"
              label={`${selectCompany} *`}
              component={renderDropdownList}
              data={(!_.isEmpty(this.props.companyDetail)) ? this.props.companyDetail.map(function (list) { return list.company_name }) : []}
              customProps={{
                fetchCompanyDetails: this.fetchCompanyDetails
              }}
            />
          </div>
          <div className='btn-right' style={{ paddingLeft: '48px', paddingTop: '22px' }}>
            <Button onClick={() => this.viewQaCompanyDetail()}> {strings.qa.viewDoc}</Button>
          </div>
        </div>
        <div>
          {(!_.isEmpty(this.props.companyDetailDoc)) &&
            <div>
              <h2>{strings.qa.qaUploadDoc}</h2>
              {this.props.companyDetailDoc.documents.map((item, index) => {
                if (item.is_approved === 1) {
                  item.is_approved = "Approved"
                }
                else if (item.is_approved === 0) {
                  item.is_approved = "Rejected"
                }
                else if(item.is_approved === null){
                  item.is_approved = "Not Responded"
                }
              }) &&
                <QaUploadTableDetail
                  data={this.props.companyDetailDoc.documents}
                  pagination={false}
                  search={false}
                  exportCSV={false}
                  haveAction={false}
                  pages={this.props.pages}
                  // currentLanguage={this.props.currentLanguage}
                  qaApproveFormatter={this.qaApproveFormatter}
                  company_key_id = {this.state.company_key_id}
                  qaCompanyDetail = {this.props.qaCompanyDetail}
                  options = {options}
                  currentLanguage = {this.props.currentLanguage}
                />
              }
            </div>
          }
        </div>
        <div>
          <Modal
            isOpen={this.props.isDeclineQa}
            style={customStyles}
            ariaHideApp={false}
          >
            <DeclineQa
            shipment_document_id={this.state.shipment_document_id}
            company_key_id={this.state.company_key_id}
            qaRejectDetail={this.props.qaRejectDetail}
            isDeclineQaDetails = {this.props.isDeclineQaDetails}
            qaCompanyDetail = {this.props.qaCompanyDetail}
            />
          </Modal>
        </div>
      </div>
    )
  }
}

export default reduxForm({
  form: 'Document'
})(Document)