import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

//localization
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
import Pagination from 'react-paginate';
let strings = new LocalizedStrings(
  data
);


/** AddShipmentTable
 *
 * @description This class is responsible to display added orderd as outgoing shipment
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class QaUploadTable extends Component {
  constructor(props) {
    super(props);
    this.handlePageClick = this.handlePageClick.bind(this)
  }

  handlePageClick(value) {
    this.props.qaCompanyDetail(this.props.company_key_id, value.selected+1, this.props.options.pageSize, '', '', '');
  }

  render() {
    const { currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);
    const {isApproved, remarks, documentName} = strings.qa

    return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv}>
          <TableHeaderColumn dataField='shipment_document_id' hidden={true}>Shipment Document ID</TableHeaderColumn>
          <TableHeaderColumn dataField='ID' isKey={true} autoValue={true} >{strings.addProduct.sNo}</TableHeaderColumn>
          <TableHeaderColumn dataField='invoice_number' >{strings.createShipment.invoiceNumber}</TableHeaderColumn>
          <TableHeaderColumn dataField='document_name'  >{documentName}</TableHeaderColumn>
          <TableHeaderColumn dataField='is_approved'>{isApproved}</TableHeaderColumn>
          <TableHeaderColumn dataField='remarks' columnTitle={true} >{remarks}</TableHeaderColumn>
          <TableHeaderColumn key="editAction" dataFormat= {this.props.qaApproveFormatter}>{strings.shipmentTableText.action}</TableHeaderColumn>
        </BootstrapTable>
        <div className="pagination-box">
          <Pagination
            previousLabel={strings.prev}
            nextLabel={strings.next}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={this.props.pages}
            pageRangeDisplayed={5}
            onPageChange={this.handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        </div>
      </div>
    );
  }
}
QaUploadTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default QaUploadTable;