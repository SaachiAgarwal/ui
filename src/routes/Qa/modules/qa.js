import axios from 'axios';
import { browserHistory } from 'react-router';

import {Config, Url} from 'config/Config';
import {getToken, getPages} from 'components/Helper';
// ------------------------------------
// Constant
// ------------------------------------
export const SET_ALERT_MESSAGE = 'SET_ALERT_MESSAGE';
export const FETCHING = 'FETCHING';
export const FETCHING_ERROR = 'FETCHING_ERROR';
export const CONFIG_DATA = 'CONFIG_DATA'
export const FETCHING_COMPANY_TYPE_SUCCESS = 'FETCHING_COMPANY_TYPE_SUCCESS'
export const COMPANY_TYPE_SUCCESS = 'COMPANY_TYPE_SUCCESS'
export const QA_COMPANY_DETAIL_DOC = 'QA_COMPANY_DETAIL_DOC'
export const IS_DECLINE_QA_STATUS = 'IS_DECLINE_QA_STATUS'
export const RESET_QA_COMPANY_DETAIL = 'RESET_QA_COMPANY_DETAIL'
export const SET_TOTAL_PAGES = 'SET_TOTAL_PAGES'
// ------------------------------------
// Actions
// ------------------------------------
export function setAlertMeassage(status, message, orderStatus=false) {
  return {
    type: SET_ALERT_MESSAGE,
    showAlert: status,
    alertMessage: message,
    status: orderStatus
  };
}

export function fetching (status) {
  return {
    type: FETCHING,
    fetching: status
  }
}

export function errorFetching (status) {
  return {
    type: FETCHING_ERROR,
    error: status
  }
}
export function setConfigData (payload){
  return{
    type: CONFIG_DATA,
    configData: payload
  }
}
export function fetchingCompanyTypeSuccess(payload) {
  return {
    type: FETCHING_COMPANY_TYPE_SUCCESS,
    companyType: payload,
  };
}
export function companyTypeSuccess(payload) {
  return {
    type: COMPANY_TYPE_SUCCESS,
    companyDetail: payload,
  };
}
export function qaCompanyDetailDoc(payload) {
  return{
    type: QA_COMPANY_DETAIL_DOC,
    companyDetailDoc: payload
  }
}
export function isDeclineQaDetails(payload){
  return{
    type: IS_DECLINE_QA_STATUS,
    isDeclineQa: payload
  }
}
export function resetQaComany(payload){
  return{
    type: RESET_QA_COMPANY_DETAIL,
    companyDetailDoc: payload
  }
}
export function setTotalPages(payload) {
  return {
    type: SET_TOTAL_PAGES,
    pages: payload,
  }
}

// ------------------------------------
// Action creators
// ------------------------------------
export const resetAlertBox = (showAlert, message) => {
  return (dispatch) => {
    dispatch(setAlertMeassage(showAlert, message));
  }
}

export const updateConfiguration = (value) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/update_audit_display';
    axios({
      method: 'post',
      url: Config.url + endPoint,
      data: value,
      headers: {'token': getToken()}
    }).then(response => {
      if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
        dispatch(fetching(false));
        dispatch(setAlertMeassage(true, response.data.data, true));
        browserHistory.push(Url.CONFIGURATION_PAGE);
      } else if (response.data.error === 1) {
        dispatch(setAlertMeassage(true, response.data.data, false));
      }
    }).catch(error => {
      dispatch(fetching(false));
      dispatch(errorFetching(true));
      // dispatch(setAlertMeassage(true, 'Password can not be updated.', false));
      browserHistory.push(Url.ERRORS_PAGE);
    });
  }
}

export const setConfigDetail = () => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/get_audit_display';
    let method = 'get';
    return new Promise((resolve, reject) => {
      axios({
        method: method,
        url: Config.url + endPoint,
        headers: { 'token': getToken() }
      }).then(response => {
        // console.log("Configure details")
        // console.log('response =========', response);
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        }else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(setConfigData(response.data));
        }
        resolve(true);

      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const getCompanyType = () => {
  return (dispatch) => {
    // dispatch(fetching(true)); // stop loader on company type dropdown
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: `${Config.url}users/get_company_type`,
        headers: {'token': getToken()}
      }).then( response => {
        // console.log(response)
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(fetchingCompanyTypeSuccess(response.data));
        }
        resolve(true);
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'There is no company type.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}
export const getCompany = (company_type_id) => {
  return (dispatch) => {
    // dispatch(fetching(true)); //stop loader on Company dropdown
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: `${Config.url}users/get_company`,
        params: {
          company_type_id:company_type_id
        },
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(companyTypeSuccess(response.data));
        }
        resolve(true);
      }).catch( error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'Something went wrong.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}


export const qaCompanyDetail = (company_key_id, currentPage = 1, pageSize = '10', frmDt = '', toDt = '', query = '') => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'admin/admin_get_documents';
    let data = {
      company_key_id: company_key_id,
      from_date: frmDt,
      to_date: toDt,
      search_text: query, 
      page_size: pageSize, 
      page_number: currentPage
    };
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        // console.log(response)
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          let totalPage = getPages(response.data.total_recordcount, pageSize);
          //console.log(totalPage)
          dispatch(setTotalPages(totalPage));
          // dispatch(setAlertMeassage(true, response.data.data, true));
          dispatch(qaCompanyDetailDoc(response.data))
          resolve(true);
        }
      }).catch(error => {
        console.log(error)
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const qaDownloadDoc = (document_file_name, document_name) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'shipment/download_document';
    let data = {
      document_file_name: document_file_name
    };
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() },
        responseType: 'blob',
      }).then(response => {
        // console.log(response)
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          link.href = url;
          link.setAttribute('download', document_name);
          document.body.appendChild(link);
          link.click();

          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}


export const qaApproveDetail = (shipment_document_id) => {
  return (dispatch) => {
    // console.log(shipment_document_id)
    dispatch(fetching(true));
    let endPoint = 'admin/approve_document';
    let data = {
      shipment_document_id: shipment_document_id
    };
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        // console.log(response)
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, true));
          resolve(true);
        }
      }).catch(error => {
        console.log(error)
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}


export const qaRejectDetail = (shipment_document_id, reject_remarks) => {
  return (dispatch) => {
    // console.log(shipment_document_id)
    dispatch(fetching(true));
    let endPoint = 'admin/reject_document';
    let data = {
      shipment_document_id: shipment_document_id,
      reject_remarks: reject_remarks
    };
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        // console.log(response)
        dispatch(fetching(false))
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, true));
          resolve(true);
        }
      }).catch(error => {
        console.log(error)
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const action = {
  updateConfiguration,
  setConfigDetail,
  getCompanyType,
  qaCompanyDetail,
  qaDownloadDoc
};
// ------------------------------------
// Action handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_ALERT_MESSAGE]: (state, action) => {
    return {
      ...state,
      showAlert: action.showAlert,
      alertMessage: action.alertMessage,
      status: action.status,
    }
  },
  [FETCHING]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching
    }
  },
  [FETCHING_ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
  [CONFIG_DATA]: (state, action) => {
    // debugger
    return{
      ...state,
      configData: action.configData
    }
  },
  [FETCHING_COMPANY_TYPE_SUCCESS]: (state, action) => {
    return {
      ...state,
      companyType: action.companyType
    }
  },
  [COMPANY_TYPE_SUCCESS]: (state, action) => {
    return {
      ...state,
      companyDetail: action.companyDetail
    }
  },
  [QA_COMPANY_DETAIL_DOC]: (state, action) => {
    return{
      ...state,
      companyDetailDoc: action.companyDetailDoc
    }
  },
  [IS_DECLINE_QA_STATUS]: (state, action) => {
    return{
      ...state,
      isDeclineQa: action.isDeclineQa
    }
  },
  [RESET_QA_COMPANY_DETAIL]: (state, action) => {
    return{
      ...state,
      companyDetailDoc: action.companyDetailDoc
    }
  },
  [SET_TOTAL_PAGES]: (state, action) => {
    return {
      ...state,
      pages: action.pages,
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  showAlert: false,
  alertMessage: '',
  status: false,
  fetching: false,
  error: false,
  configData: '',
  companyType: [],
  companyDetail: [],
  companyDetailDoc: '',
  isDeclineQa: false,
  pages: 1,
};

export default function qaReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
