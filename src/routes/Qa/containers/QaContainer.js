/** SettingsContainer
 *
 * @description This is container that manages the states for all settings related stuff.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */

import { connect } from 'react-redux';

import Qa from '../components/Qa';
import {updateConfiguration, resetAlertBox, setConfigDetail, getCompanyType, getCompany, qaCompanyDetail, qaDownloadDoc, qaApproveDetail, qaRejectDetail, isDeclineQaDetails, resetQaComany} from '../modules/qa'
import {setInnerNav,setParam,setSearchString,setReportFetching,setGlobalState, setCurrentLanguage} from '../../../store/app';

const mapStateToProps = (state) => {
  return({
    navIndex: state.app.navIndex,
    navInnerIndex: state.app.navInnerIndex,
    navClass: state.app.navClass,
    fetching: state.Qa.fetching,
    error: state.Qa.error,
    showAlert: state.Qa.showAlert,
    alertMessage: state.Qa.alertMessage,
    status: state.Qa.status,
    configData: state.Qa.configData,
    currentLanguage: state.app.currentLanguage,
    companyType: state.Qa.companyType,
    companyDetail: state.Qa.companyDetail,
    companyDetailDoc: state.Qa.companyDetailDoc,
    isDeclineQa: state.Qa.isDeclineQa,
    pages: state.Qa.pages,
    currentLanguage :  state.app.currentLanguage
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    setGlobalState: (value) => dispatch(setGlobalState(value)),
    resetAlertBox: (showAlert, message) => dispatch(resetAlertBox(showAlert, message)),
    updateConfiguration: (value) => dispatch(updateConfiguration(value)),
    setConfigDetail: () => dispatch(setConfigDetail()),
    setCurrentLanguage: (lang) => dispatch(setCurrentLanguage(lang)),
    getCompanyType: () => dispatch(getCompanyType()),
    getCompany: (company_type_id) => dispatch(getCompany(company_type_id)),
    qaCompanyDetail: (company_key_id, currentPage, pageSize, frmDt, toDt, query,) => dispatch(qaCompanyDetail(company_key_id, currentPage, pageSize, frmDt, toDt, query,)),
    qaDownloadDoc: (document_file_name, document_name) => dispatch(qaDownloadDoc(document_file_name, document_name)),
    qaApproveDetail: (shipment_document_id) => dispatch(qaApproveDetail(shipment_document_id)),
    isDeclineQaDetails: (status) => dispatch(isDeclineQaDetails(status)),
    qaRejectDetail: (shipment_document_id, reject_remarks) => dispatch(qaRejectDetail(shipment_document_id, reject_remarks)),
    resetQaComany: (value) => dispatch(resetQaComany(value)),
    setInnerNav: (lang) => dispatch(setInnerNav(lang)),
    setParam: (lang) => dispatch(setParam(lang)),
    setSearchString: (lang) => dispatch(setSearchString(lang)),
    setReportFetching: (lang) => dispatch(setReportFetching(lang)),

  });
};

export default connect(mapStateToProps, mapDispatchToProps)(Qa);
