/** OrdersContainer
 *
 * @description This is container that manages the states for all order related functionality.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
import { connect } from 'react-redux';

import Orders from '../components/Orders';

import {setToDate, setFromDate,setSearchString,setParam,setInnerNav,setGlobalState, fetchUOM, uploadFile, handleUnreadItems, setCurrentLanguage,setReportFetching } from '../../../store/app';

import { viewCompanyDetail, fetchOrders, fetchCompanyRole, openCreateOrderForm, saveOrders, fetchSupplierCompnies, fetchProducttType, fetchProductCategory, deleteOrder, viewOrder, resetAlertBox, placeOrder, cancelOrder, setProps, linkOrder, resetProps, fetchProducts, resetProducts, setAddProductState, addProducts, getLinkOrders, setCurrentNewPage, poEditStatus, setJobActive, resetJobActive, externalJobStatus, setProductStatus, fetchDownloadOrders, fetchingOrders, supplierCompanyType, getCompany, companyKeyId, fetchProductsByID, resetCompanyKeyId, downloadOrderProduct, downloadOrderCompany } from '../modules/orders.js';

const mapStateToProps = (state) => {
  return ({
    navIndex: state.app.navIndex,
    navInnerIndex: state.app.navInnerIndex,
    navClass: state.app.navClass,
    uom: state.app.uom,
    isUploaded: state.app.isUploaded,
    msg: state.app.msg,
    unreadItems: state.app.unreadItems,
    fetching: state.Orders.fetching,
    error: state.Orders.error,
    showAlert: state.Orders.showAlert,
    alertMessage: state.Orders.alertMessage,
    status: state.Orders.status,
    outgoingOrders: state.Orders.outgoingOrders,
    incomingOrders: state.Orders.incomingOrders,
    isOrderFormOpen: state.Orders.isOrderFormOpen,
    supplierCompniesList: state.Orders.supplierCompniesList,
    productTypeList: state.Orders.productTypeList,
    productCategoryList: state.Orders.productCategoryList,
    orderDeletedData: state.Orders.orderDeletedData,
    viewOrderDetail: state.Orders.viewOrderDetail,
    savedOrdersDetail: state.Orders.savedOrdersDetail,
    linkedOrdersDetail: state.Orders.linkedOrdersDetail,
    isAddProduct: state.Orders.isAddProduct,
    isEdit: state.Orders.isEdit,
    isSave: state.Orders.isSave,
    isReviewOrder: state.Orders.isReviewOrder,
    products: state.Orders.products,
    addedProductDetail: state.Orders.addedProductDetail,
    isProductEdit: state.Orders.isProductEdit,
    isUpload: state.Orders.isUpload,
    isDownload: state.Orders.isDownload,
    isDownloadCatalogue: state.Orders.isDownloadCatalogue,
    totalRecordCount: state.Orders.totalRecordCount,
    pages: state.Orders.pages,
    isDecline: state.Orders.isDecline,
    linkOrders: state.Orders.linkOrders,
    showCreateOrderBtn: state.Orders.showCreateOrderBtn,
    companyRole: state.Orders.companyRole,
    viewCompanyInfo: state.Orders.viewCompanyInfo,
    currentNewPage: state.Orders.currentNewPage,
    currentLanguage : state.app.currentLanguage,
    isPoEdit: state.Orders.isPoEdit,
    fromDate : state.app.fromDate,
    toDate : state.app.toDate,
    fetchingFromReport : state.app.fetchingFromReport,
    nav : state.app.nav,
    isJobActive: state.Orders.isJobActive,
    jobStatus: state.Orders.jobStatus,
    isProduct: state.Orders.isProduct,
    outgoingDownloadOrder: state.Orders.outgoingDownloadOrder,
    incomingDownloadOrder: state.Orders.incomingDownloadOrder,
    supplierCompanyList: state.Orders.supplierCompanyList,
    companyDetailVal: state.Orders.companyDetailVal,
    companyKeyIdVal: state.Orders.companyKeyIdVal
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    handleUnreadItems: (type, id) => dispatch(handleUnreadItems(type, id)),
    setGlobalState: (value) => dispatch(setGlobalState(value)),
    resetAlertBox: (showAlert, message) => dispatch(resetAlertBox(showAlert, message)),
    fetchOrders: (orderType, currentPage, pageSize, frmDt, toDt, query) => dispatch(fetchOrders(orderType, currentPage, pageSize, frmDt, toDt, query)),
    openCreateOrderForm: (status) => dispatch(openCreateOrderForm(status)),
    saveOrders: (item, isEdit) => dispatch(saveOrders(item, isEdit)),
    linkOrder: (data, isLinked) => dispatch(linkOrder(data, isLinked)),
    fetchSupplierCompnies: (company_type_id) => dispatch(fetchSupplierCompnies(company_type_id)),
    fetchProducttType: () => dispatch(fetchProducttType()),
    fetchProductCategory: () => dispatch(fetchProductCategory()),
    deleteOrder: (orderId, key) => dispatch(deleteOrder(orderId, key)),
    viewOrder: (orderId, key) => dispatch(viewOrder(orderId, key)),
    placeOrder: (orderId, orderNumber, key) => dispatch(placeOrder(orderId, orderNumber, key)),
    cancelOrder: (orderDetail) => dispatch(cancelOrder(orderDetail)),
    setProps: (status, key) => dispatch(setProps(status, key)),
    resetProps: () => dispatch(resetProps()),
    fetchProducts: (queryString) => dispatch(fetchProducts(queryString)),
    resetProducts: () => dispatch(resetProducts()),
    setAddProductState: (status) => dispatch(setAddProductState(status)),
    addProducts: (values, isProductEdit) => dispatch(addProducts(values, isProductEdit)),
    editProduct: (productId) => dispatch(editProduct(productId)),
    fetchUOM: (type) => dispatch(fetchUOM(type)),
    uploadFile: (file, owner) => dispatch(uploadFile(file, owner)),
    fetchCompanyRole: (company) => dispatch(fetchCompanyRole(company)),
    viewCompanyDetail: (companyId) => dispatch(viewCompanyDetail(companyId)),
    getLinkOrders: (currentPage, pageSize, frmDt, toDt) => dispatch(getLinkOrders(currentPage, pageSize, frmDt, toDt)),
    setCurrentNewPage: (page) => dispatch(setCurrentNewPage(page)),
    setCurrentLanguage : (language) => dispatch(setCurrentLanguage(language)),
    poEditStatus: (status) => dispatch(poEditStatus(status)),

    setInnerNav: (lang) => dispatch(setInnerNav(lang)),
    setParam: (lang) => dispatch(setParam(lang)),
    setSearchString: (lang) => dispatch(setSearchString(lang)),
    setReportFetching: (lang) => dispatch(setReportFetching(lang)),
    setFromDate: (lang) => dispatch(setFromDate(lang)),
    setToDate: (lang) => dispatch(setToDate(lang)),

    setJobActive: (value) => dispatch(setJobActive(value)),
    resetJobActive: (value) => dispatch(resetJobActive(value)),
    externalJobStatus: (value) => dispatch(externalJobStatus(value)),
    setProductStatus: (value) => dispatch(setProductStatus(value)),
    fetchDownloadOrders: (orderType, currentPage, pageSize, frmDt, toDt, query) => dispatch(fetchDownloadOrders(orderType, currentPage, pageSize, frmDt, toDt, query)),
    fetchingOrders: (val) => dispatch(fetchingOrders(val)),
    supplierCompanyType: () => dispatch(supplierCompanyType()),
    getCompany: (val) => dispatch(getCompany(val)),
    companyKeyId: (val) => dispatch(companyKeyId(val)),
    fetchProductsByID: (val) => dispatch(fetchProductsByID(val)),
    resetCompanyKeyId: () => dispatch(resetCompanyKeyId()),
    downloadOrderProduct: () => dispatch(downloadOrderProduct()), 
    downloadOrderCompany: () => dispatch(downloadOrderCompany())
  });
};

export default connect(mapStateToProps, mapDispatchToProps)(Orders);
