/** order reducer
 *
 * @description This is a reducer responsible for all the create/delete/update/view/fulfill functionality
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
import axios from 'axios';
import { browserHistory, Router } from 'react-router';

import { Config, Url } from 'config/Config';
import { getLocalStorage, saveLocalStorage, getToken, sleep, filterByDate, getPages } from 'components/Helper';
// ------------------------------------
// Constant
// ------------------------------------

export const FETCHING_ORDERS = 'FETCHING_ORDERS';
export const OUTGOING_RECEIVED_SUCCESS = 'OUTGOING_RECEIVED_SUCCESS';
export const INCOMING_RECEIVED_SUCCESS = 'INCOMING_RECEIVED_SUCCESS';
export const ERROR_FETCHING_ORDERS = 'ERROR_FETCHING_ORDERS';
export const SET_FORM_STATE = 'SET_FORM_STATE';
export const SAVING_ORDERS = 'SAVING_ORDERS';
export const ORDERS_SAVE_SUCCESS = 'ORDERS_SAVE_SUCCESS';
export const ERROR_SAVING_ORDERS = 'ERROR_SAVING_ORDERS';
export const LINKING_ORDER = 'LINKING_ORDER';
export const LINKING_ORDER_SUCCESS = 'LINKING_ORDER_SUCCESS';
export const LINKING_ORDER_ERROR = 'LINKING_ORDER_ERROR';
export const SET_ALERT_MESSAGE = 'SET_ALERT_MESSAGE';
export const FETCHING_SUPPLIER_COMPNIES = 'FETCHING_SUPPLIER_COMPNIES';
export const FETCHING_SUPPLIER_COMPNIES_SUCCESS = 'FETCHING_SUPPLIER_COMPNIES_SUCCESS';
export const FETCHING_SUPPLIER_COMPNIES_ERROR = 'FETCHING_SUPPLIER_COMPNIES_ERROR';
export const FETCHING_PRODUCT_TYPE = 'FETCHING_PRODUCT_TYPE';
export const FETCHING_PRODUCT_TYPE_SUCCESS = 'FETCHING_PRODUCT_TYPE_SUCCESS';
export const FETCHING_PRODUCT_TYPE_ERROR = 'FETCHING_PRODUCT_TYPE_ERROR';
export const FETCHING_PRODUCT_CATEGORY = 'FETCHING_PRODUCT_CATEGORY';
export const FETCHING_PRODUCT_CATEGORY_SUCCESS = 'FETCHING_PRODUCT_CATEGORY_SUCCESS';
export const FETCHING_PRODUCT_CATEGORY_ERROR = 'FETCHING_PRODUCT_CATEGORY_ERROR';
export const ORDER_DELETING = 'ORDER_DELETING';
export const ORDER_DELETING_SUCCESS = 'ORDER_DELETING_SUCCESS';
export const ORDER_DELETING_ERROR = 'ORDER_DELETING_ERROR';
export const FETCHING_VIEW_ORDER = 'FETCHING_VIEW_ORDER';
export const VIEW_ORDER_SUCCESS = 'VIEW_ORDER_SUCCESS';
export const VIEW_ORDER_ERROR = 'VIEW_ORDER_ERROR';
export const ORDER_PLACING = 'ORDER_PLACING';
export const ORDER_PLACING_SUCCESS = 'ORDER_PLACING_SUCCESS';
export const ORDER_PLACING_ERROR = 'ORDER_PLACING_ERROR';
export const ORDER_CANCELING = 'ORDER_CANCELING';
export const ORDER_CANCELING_ERROR = 'ORDER_CANCELING_ERROR';
export const SET_EDIT_STATE = 'SET_EDIT_STATE';
export const SET_SAVE_STATE = 'SET_SAVE_STATE';
export const SET_REVIEW_STATE = 'SET_REVIEW_STATE';
export const RESET_PROPS = 'RESET_PROPS';
export const FETCHING_PRODUCTS = 'FETCHING_PRODUCTS';
export const FETCHING_PRODUCTS_SUCCESS = 'FETCHING_PRODUCTS_SUCCESS';
export const FETCHING_PRODUCTS_ERROR = 'FETCHING_PRODUCTS_ERROR';
export const RESTE_ALREADY_SET_PRODUCTS = 'RESTE_ALREADY_SET_PRODUCTS';
export const SETTING_ADD_PRODUCT_STATE = 'SETTING_ADD_PRODUCT_STATE';
export const ADDING_PRODUCTS = 'ADDING_PRODUCTS';
export const ADDING_PRODUCTS_SUCCESS = 'ADDING_PRODUCTS_SUCCESS';
export const ADDING_PRODUCTS_ERROR = 'ADDING_PRODUCTS_ERROR';
export const SET_PRODUCT_EDIT_STATE = 'SET_PRODUCT_EDIT_STATE';
export const SET_UPLOAD_STATUS = 'SET_UPLOAD_STATUS';
export const SET_DOWNLOAD_STATUS = 'SET_DOWNLOAD_STATUS';
export const SET_DOWNLOAD_CATALOGUE_STATUS = 'SET_DOWNLOAD_CATALOGUE_STATUS';
export const SET_TOTAL_RECORD_COUNT = 'SET_TOTAL_RECORD_COUNT';
export const SET_TOTAL_PAGES = 'SET_TOTAL_PAGES';
export const SET_DECLINE_STATE = 'SET_DECLINE_STATE';
export const FETCHING_LINK_ORDERS = 'FETCHING_LINK_ORDERS';
export const LINK_ORDERS_ERROR = 'LINK_ORDERS_ERROR';
export const LINK_ORDER_SUCCESS = 'LINK_ORDER_SUCCESS';
export const SET_CREATE_ORDER_BTN_STATE = 'SET_CREATE_ORDER_BTN_STATE';
export const COMPANY_ROLE_SUCCESS = 'COMPANY_ROLE_SUCCESS';
export const SET_COMPANY_DETAIL = 'SET_COMPANY_DETAIL';
export const ERROR = 'ERROR';
export const SET_CURRENT_NEW_PAGE = 'SET_CURRENT_NEW_PAGE'
export const PO_EDIT_STATUS_DETAIL = 'PO_EDIT_STATUS_DETAIL'
export const SET_JOB_ACTIVE = 'SET_JOB_ACTIVE'
export const RESET_JOB_ACTIVE = 'RESET_JOB_ACTIVE'
export const EXTERNAL_JOB_STATUS = 'EXTERNAL_JOB_STATUS'
export const SET_PRODUCT_STATUS_DETAIL = 'SET_PRODUCT_STATUS_DETAIL'
export const OUTGOING_DOWNLOAD_ORDER = 'OUTGOING_DOWNLOAD_ORDER'
export const INCOMING_DOWNLOAD_ORDER = 'INCOMING_DOWNLOAD_ORDER'
export const SUPPLIER_COMPANY_TYPE_VAL = 'SUPPLIER_COMPANY_TYPE_VAL'
export const COMPANY_TYPE_SUCCESS = 'COMPANY_TYPE_SUCCESS'
export const COMPANY_KEY_ID = 'COMPANY_KEY_ID'
export const RESET_COMPANY_KEY_ID = 'RESET_COMPANY_KEY_ID'
// ------------------------------------
// Actions
// ------------------------------------

export function fetchingOrders(status) {
  return {
    type: FETCHING_ORDERS,
    fetching: status
  }
}

export function outgoingReceivedSuccess(payload) {
  return {
    type: OUTGOING_RECEIVED_SUCCESS,
    outgoingOrders: payload
  }
}

export function incomingReceivedSuccess(payload) {
  return {
    type: INCOMING_RECEIVED_SUCCESS,
    incomingOrders: payload
  }
}

export function errorFetchingOrders(status) {
  return {
    type: ERROR_FETCHING_ORDERS,
    error: status
  }
}

export function setFormState(status) {
  return {
    type: SET_FORM_STATE,
    isOrderFormOpen: status
  }
}

export function savingOrders(status) {
  return {
    type: SAVING_ORDERS,
    fetching: status
  }
}

export function ordersSavedSuccess(payload) {
  return {
    type: ORDERS_SAVE_SUCCESS,
    savedOrdersDetail: payload
  }
}

export function errorSavingOrders(status) {
  return {
    type: ERROR_SAVING_ORDERS,
    error: status
  }
}

export function linkingOrder(status) {
  return {
    type: LINKING_ORDER,
    fetching: status
  }
}

export function linkingOrderSuccess(payload) {
  return {
    type: LINKING_ORDER_SUCCESS,
    linkedOrdersDetail: payload
  }
}

export function linkingOrderError(status) {
  return {
    type: LINKING_ORDER_ERROR,
    error: status
  }
}

export function setAlertMeassage(status, message, orderStatus = false) {
  return {
    type: SET_ALERT_MESSAGE,
    showAlert: status,
    alertMessage: message,
    status: orderStatus
  };
}

export function fetchingSupplierCompnies(status) {
  return {
    type: FETCHING_SUPPLIER_COMPNIES,
    fetching: status,
  };
}

export function fetchingSupplierCompniesSuccess(payload) {
  return {
    type: FETCHING_SUPPLIER_COMPNIES_SUCCESS,
    supplierCompniesList: payload,
  };
}

export function fetchingSupplierCompniesError(status) {
  return {
    type: FETCHING_SUPPLIER_COMPNIES_ERROR,
    error: status,
  };
}

export function fetchingProductType(status) {
  return {
    type: FETCHING_PRODUCT_TYPE,
    fetching: status,
  };
}

export function fetchingProductTypeSuccess(payload) {
  return {
    type: FETCHING_PRODUCT_TYPE_SUCCESS,
    productTypeList: payload,
  };
}

export function fetchingProductTypeError(status) {
  return {
    type: FETCHING_PRODUCT_TYPE_ERROR,
    error: status,
  };
}

export function fetchingProductCategory(status) {
  return {
    type: FETCHING_PRODUCT_CATEGORY,
    fetching: status,
  };
}

export function fetchingProductCategorySuccess(payload) {
  return {
    type: FETCHING_PRODUCT_CATEGORY_SUCCESS,
    productCategoryList: payload,
  };
}

export function fetchingProductCategoryError(status) {
  return {
    type: FETCHING_PRODUCT_CATEGORY_ERROR,
    error: status,
  };
}

export function orderDeleting(status) {
  return {
    type: ORDER_DELETING,
    fetching: status,
  };
}

export function orderDeletingSuccess(payload) {
  return {
    type: ORDER_DELETING_SUCCESS,
    orderDeletedData: payload,
  };
}

export function orderDeletingError(status) {
  return {
    type: ORDER_DELETING_ERROR,
    error: status,
  };
}

export function fetchingViewOrder(status) {
  return {
    type: FETCHING_VIEW_ORDER,
    fetching: status,
  };
}

export function viewOrderSuccess(payload) {
  return {
    type: VIEW_ORDER_SUCCESS,
    viewOrderDetail: payload,
  };
}

export function viewOrderError(status) {
  return {
    type: VIEW_ORDER_ERROR,
    error: status,
  };
}

export function orderPlacing(status) {
  return {
    type: ORDER_PLACING,
    fetching: status,
  };
}

export function orderPlacingSuccess(payload) {
  return {
    type: ORDER_PLACING_SUCCESS,
    orderPlacedData: payload,
  };
}

export function orderPlacingError(status) {
  return {
    type: ORDER_PLACING_ERROR,
    error: status,
  };
}

export function orderCanceling(status) {
  return {
    type: ORDER_CANCELING,
    fetching: status,
  };
}

export function orderCancelingError(status) {
  return {
    type: ORDER_CANCELING_ERROR,
    error: status,
  };
}

export function setEditState(status) {
  return {
    type: SET_EDIT_STATE,
    isEdit: status,
  };
}

export function setSaveState(status) {
  return {
    type: SET_SAVE_STATE,
    isSave: status,
  };
}

export function setReviewState(status) {
  return {
    type: SET_REVIEW_STATE,
    isReviewOrder: status,
  };
}

export function resetAlreadySetProps() {
  return {
    type: RESET_PROPS,
    viewOrderDetail: []
  };
}

export function settingAddProductState(status) {
  return {
    type: SETTING_ADD_PRODUCT_STATE,
    isAddProduct: status
  };
}

export function fetchingProducts(status) {
  return {
    type: FETCHING_PRODUCTS,
    fetching: status
  };
}

export function fetchingProductsSuccess(payload) {
  return {
    type: FETCHING_PRODUCTS_SUCCESS,
    products: payload
  };
}

export function fetchingProductsError(status) {
  return {
    type: FETCHING_PRODUCTS_ERROR,
    error: status
  };
}

export function resetAlreadySetProducts() {
  return {
    type: RESTE_ALREADY_SET_PRODUCTS,
    products: []
  };
}

export function AddingProduct(status) {
  return {
    type: ADDING_PRODUCTS,
    fetching: status
  };
}

export function AddingProductSuccess(payload) {
  return {
    type: ADDING_PRODUCTS_SUCCESS,
    addedProductDetail: payload
  };
}

export function AddingProductError(status) {
  return {
    type: ADDING_PRODUCTS_ERROR,
    error: status
  };
}

export function setProductEditState(status) {
  return {
    type: SET_PRODUCT_EDIT_STATE,
    isProductEdit: status
  };
}

export function setUploadStatus(status) {
  return {
    type: SET_UPLOAD_STATUS,
    isUpload: status
  }
}

export function setDownloadStatus(status) {
  return {
    type: SET_DOWNLOAD_STATUS,
    isDownload: status
  }
}

export function setDownloadCatalogueStatus(status) {
  return {
    type: SET_DOWNLOAD_CATALOGUE_STATUS,
    isDownloadCatalogue: status
  }
}

export function setTotalRecordCount(payload) {
  return {
    type: SET_TOTAL_RECORD_COUNT,
    totalRecordCount: payload,
  }
}

export function setTotalPages(payload) {
  return {
    type: SET_TOTAL_PAGES,
    pages: payload,
  }
}

export function setDeclineState(status) {
  return {
    type: SET_DECLINE_STATE,
    isDecline: status
  }
}

export function fetchingLinkOrders(status) {
  return {
    type: FETCHING_LINK_ORDERS,
    fetching: status
  }
}

export function LinkOrdersError(status) {
  return {
    type: LINK_ORDERS_ERROR,
    error: status
  }
}

export function fetchingLinkOrdersSuccess(payload) {
  return {
    type: LINK_ORDER_SUCCESS,
    linkOrders: payload
  }
}

export function setCreateOrderBtnState(status) {
  return {
    type: SET_CREATE_ORDER_BTN_STATE,
    showCreateOrderBtn: status
  }
}

export function errorFetching(status) {
  return {
    type: ERROR,
    error: status,
  };
}

export function companyRoleSuccess(payload) {
  return {
    type: COMPANY_ROLE_SUCCESS,
    companyRole: payload,
  };
}

export function setCompanyDetail(payload) {
  return {
    type: SET_COMPANY_DETAIL,
    viewCompanyInfo: payload,
  };
}
export function poEditStatus(payload) {
  return {
    type: PO_EDIT_STATUS_DETAIL,
    isPoEdit: payload
  }
}
export function setJobActive(payload){
  return{
    type: SET_JOB_ACTIVE,
    isJobActive: payload
  }
}

export function resetJobActive(payload){
  return{
    type: RESET_JOB_ACTIVE,
    isJobActive: payload
  }
}
export function externalJobStatus(payload){
  return{
    type: EXTERNAL_JOB_STATUS,
    jobStatus: payload
  }
}
export function setProductStatus(payload){
  return{
    type: SET_PRODUCT_STATUS_DETAIL,
    isProduct: payload
  }
}
// ------------------------------------
// Action creators
// ------------------------------------

export const resetAlertBox = (showAlert, message) => {
  return (dispatch) => {
    dispatch(setAlertMeassage(showAlert, message));
  }
}
export function setCurrentNewPage(payload) {
  return {
    type: SET_CURRENT_NEW_PAGE,
    currentNewPage: payload
  }
}

export function outgoingDownloadReceivedSuccess(payload){
  return{
    type: OUTGOING_DOWNLOAD_ORDER,
    outgoingDownloadOrder: payload
  }
}

export function incomingDownloadReceivedSuccess(payload){
  return{
    type: INCOMING_DOWNLOAD_ORDER,
    incomingDownloadOrder: payload
  }
}
export function supplierCompanyTypeVal(payload){
  return{
    type: SUPPLIER_COMPANY_TYPE_VAL,
    supplierCompanyList: payload
  }
}
export function companyTypeSuccess(payload){
  return{
    type: COMPANY_TYPE_SUCCESS,
    companyDetailVal: payload

  }
}
export function companyKeyId(payload){
  return{
    type: COMPANY_KEY_ID,
    companyKeyIdVal: payload
  }
}
export function resetCompanyKeyId(){
  return{
    type: RESET_COMPANY_KEY_ID,
    companyKeyIdVal: ''
  }
}

export const fetchOrders = (orderType, currentPage = 1, pageSize = 10, frmDt = '', toDt = '', query = '') => {
  return (dispatch, getState) => {
    // console.log(`fetching start_________________________________________`);
    // console.log(getState().app);
    const { fetchingFromReport, searchString, fromDate, toDate, param } = getState().app;

    if (!query) dispatch(fetchingOrders(true));
    let requestMethod = 'post';
    let data = {};
    if (fetchingFromReport) {
      data = {
        search_text: searchString,
        page_size: pageSize,
        page_number: currentPage,
        from_date: fromDate,
        to_date: toDate,
        db_param : param
      }
    } else {

      data = {
        search_text: query,
        page_size: pageSize,
        page_number: currentPage,
        from_date: frmDt,
        to_date: toDt,
      }
    }
    let endPoint = (orderType === 'outgoingOrders') ? 'orders/outgoing_orders' : 'orders/incoming_orders';
    // console.log(data, Config.url + endPoint);

    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        // console.log(response);
        if (response.data.error === 1) {
          dispatch(fetchingOrders(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetchingOrders(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          response.data.data.map((item) => {
            item["new_order_date"] = '';
            if (item.order_date != undefined) {
              var spdate = (item.order_date.split("T"))[0].split("-");
              item["new_order_date"] = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
            }
          })
          dispatch(fetchingOrders(false));
          let respFilterByDate = filterByDate(response.data);
          let totalPage = getPages(response.data.total_recordcount);
          (orderType === 'outgoingOrders') ? dispatch(outgoingReceivedSuccess(respFilterByDate.data)) : dispatch(incomingReceivedSuccess(respFilterByDate.data));
          dispatch(setTotalRecordCount(response.data.total_recordcount));
          dispatch(setTotalPages(totalPage));
        }
        // console.log(`fetching end_________________________________________`);

        resolve(true);
      }).catch(error => {
        // dispatch(fetchingOrders(false));
        // dispatch(errorFetchingOrders(true));
        // dispatch(setAlertMeassage(true, 'No order found.', false));
        // browserHistory.push(Url.ERRORS_PAGE);
        // reject(true);
      })
    });
  }
}

export const fetchDownloadOrders = (orderType, currentPage = 1, pageSize = 10, frmDt = '', toDt = '', query = '') => {
  return (dispatch, getState) => {
    // console.log(`fetching start_________________________________________`);
    // console.log(getState().app);
    const { fetchingFromReport, searchString, fromDate, toDate, param } = getState().app;

    if (!query) dispatch(fetchingOrders(true));
    let requestMethod = 'post';
    let data = {};
    if (fetchingFromReport) {
      data = {
        search_text: searchString,
        from_date: fromDate,
        to_date: toDate,
        db_param : param
      }
    } else {

      data = {
        search_text: query,
        from_date: frmDt,
        to_date: toDt,
      }
    }
    let endPoint = (orderType === 'outgoingOrders') ? 'orders/download_outgoing_orders' : 'orders/download_incoming_orders';
    // console.log(data, Config.url + endPoint);

    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        // console.log(response);
        if (response.data.error === 1) {
          dispatch(fetchingOrders(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetchingOrders(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          response.data.data.map((item) => {
            item["new_order_date"] = '';
            if (item.order_date != undefined) {
              var spdate = (item.order_date.split("T"))[0].split("-");
              item["new_order_date"] = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
            }
          })
          dispatch(fetchingOrders(false));
          let respFilterByDate = filterByDate(response.data);
          let totalPage = getPages(response.data.total_recordcount);
          (orderType === 'outgoingOrders') ? dispatch(outgoingDownloadReceivedSuccess(respFilterByDate.data)) : dispatch(incomingDownloadReceivedSuccess(respFilterByDate.data));
          dispatch(setTotalRecordCount(response.data.total_recordcount));
          dispatch(setTotalPages(totalPage));
        }
        // console.log(`fetching end_________________________________________`);

        resolve(true);
      }).catch(error => {
        // dispatch(fetchingOrders(false));
        // dispatch(errorFetchingOrders(true));
        // dispatch(setAlertMeassage(true, 'No order found.', false));
        // browserHistory.push(Url.ERRORS_PAGE);
        // reject(true);
      })
    });
  }
}

export const openCreateOrderForm = (status) => {
  return (dispatch) => {
    dispatch(setFormState(status));
  }
}

export const viewCompanyDetail = (companyId) => {
  return (dispatch) => {
    //dispatch(fetching(true));
    let requestMethod = 'get';
    let endPoint = 'admin/company_detail';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        params: {
          company_key_id: companyId
        },
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          //dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          //dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          //dispatch(fetching(false));
          //dispatch(setCompanyFormState(false));
          //if (response.data.data.roles !== undefined) dispatch(setRoleCheckboxState(true));
          let lStorage = {
            company_name: response.data.data.company_name,
            company_type_id: response.data.data.company_type_id,
            company_key_id: response.data.data.company_key_id
          }
          saveLocalStorage('companyDetail', lStorage);
          dispatch(setCompanyDetail(response.data.data));
        }
        resolve(true);
      }).catch(error => {
        //dispatch(fetching(false));
        //dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'There is no company type.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const fetchCompanyRole = (company) => {
  return (dispatch) => {
    let requestMethod = 'get';
    let endPoint = 'admin/company_role';
    if (company === 'Integrated Player') {
      return new Promise((resolve, reject) => {
        axios({
          method: requestMethod,
          url: Config.url + endPoint,
          headers: { 'token': getToken() }
        }).then(response => {
          if (response.data.error === 1) {
            //dispatch(fetching(false));
            dispatch(setAlertMeassage(true, response.data.data, false));
          } else if (response.data.error === 5) {
            //dispatch(fetching(false));
            dispatch(setAlertMeassage(true, response.data.data, false));
            browserHistory.push(Url.LOGIN_PAGE);
          } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
            //dispatch(fetching(false));
            dispatch(companyRoleSuccess(response.data.data));
            //dispatch(setRoleCheckboxState(true));
          } else if (response.data.error === 1) {
            //dispatch(fetching(false));
            dispatch(setAlertMeassage(true, response.data.data, false));
          }
          resolve(true);
        }).catch(error => {
          console.log(error);
          //dispatch(fetching(false));
          //dispatch(errorFetching(true));
          // dispatch(setAlertMeassage(true, 'Role can not be fetched.', false));
          //browserHistory.push(Url.ERRORS_PAGE);
          reject(true);
        })
      });
    } else {
      //dispatch(setRoleCheckboxState(false));
    }
  }
}

export const saveOrders = (orders, isEdit) => {
  return (dispatch) => {
    dispatch(savingOrders(true));
    dispatch(errorSavingOrders(false));
    let endPoint = '';
    endPoint = (isEdit === true) ? 'orders/update_order' : 'orders/create_order';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: orders,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(savingOrders(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(savingOrders(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(savingOrders(false));
          dispatch(ordersSavedSuccess(response.data));
          if (typeof response.data.order_id !== 'undefined') {
            let orders = {
              order_id: response.data.order_id
            }
            saveLocalStorage('orders', orders);
            dispatch(setSaveState(true));
            dispatch(setEditState(true));
            dispatch(setCreateOrderBtnState(true));
          }
          if (typeof response.data.msg !== 'undefined') {
            dispatch(setEditState(false));
            dispatch(setSaveState(false));
            dispatch(setAlertMeassage(true, response.data.msg));
          }
        } else if (response.data.error === 1) {
          dispatch(savingOrders(false));
          dispatch(errorSavingOrders(true));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(savingOrders(false));
        dispatch(errorSavingOrders(true));
        // dispatch(setAlertMeassage(true, 'Order not saved.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const linkOrder = (linkOrderData, isLinked) => {
  return (dispatch) => {
    dispatch(linkingOrder(true));
    let endPoint = (isLinked === 1) ? 'orders/unlink_order' : 'orders/link_order';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: linkOrderData,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(linkingOrder(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(linkingOrder(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(linkingOrder(false));
          dispatch(linkingOrderSuccess(response.data));
        }
        resolve(true);
      }).catch(error => {
        dispatch(linkingOrder(false));
        dispatch(linkingOrderError(true));
        // dispatch(setAlertMeassage(true, 'Order can not be linked', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const fetchSupplierCompnies = (company_type_id) => {
  return (dispatch) => {
    // dispatch(fetchingSupplierCompnies(true));
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: `${Config.url}masters/get_supplier_companies`,
        params: {
          company_type_id: company_type_id
        },
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetchingSupplierCompnies(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetchingSupplierCompnies(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetchingSupplierCompnies(false));
          dispatch(fetchingSupplierCompniesSuccess(response.data.data, true));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetchingSupplierCompnies(false));
        dispatch(fetchingSupplierCompniesError(true));
        // dispatch(setAlertMeassage(true, 'No Supplier Compnies Found.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const fetchProducttType = () => {
  return (dispatch) => {
    dispatch(fetchingProductType(true));
    axios({
      method: 'get',
      url: `${Config.url}masters/get_product_type`,
      headers: { 'token': getToken() }
    }).then(response => {
      if (response.data.error === 1) {
        dispatch(fetchingProductType(false));
        dispatch(setAlertMeassage(true, response.data.data, false));
      } else if (response.data.error === 5) {
        dispatch(fetchingProductType(false));
        dispatch(setAlertMeassage(true, response.data.data, false));
        browserHistory.push(Url.LOGIN_PAGE);
      } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
        dispatch(fetchingProductType(false));
        dispatch(fetchingProductTypeSuccess(response.data));
      }
    }).catch(error => {
      dispatch(fetchingProductType(false));
      dispatch(fetchingProductTypeError(true));
      // dispatch(setAlertMeassage(true, 'No Prouct Type Found.', false));
      browserHistory.push(Url.ERRORS_PAGE);
    })
  }
}

export const fetchProductCategory = () => {
  return (dispatch) => {
    dispatch(fetchingProductCategory(true));
    axios({
      method: 'get',
      url: `${Config.url}masters/get_product_category`,
      headers: { 'token': getToken() }
    }).then(response => {
      if (response.data.error === 1) {
        dispatch(fetchingProductCategory(false));
        dispatch(setAlertMeassage(true, response.data.data, false));
      } else if (response.data.error === 5) {
        dispatch(fetchingProductCategory(false));
        dispatch(setAlertMeassage(true, response.data.data, false));
        browserHistory.push(Url.LOGIN_PAGE);
      } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
        dispatch(fetchingProductCategory(false));
        dispatch(fetchingProductCategorySuccess(response.data));
      }
    }).catch(error => {
      dispatch(fetchingProductCategory(false));
      dispatch(fetchingProductCategoryError(true));
      // dispatch(setAlertMeassage(true, 'No Product Category Found.', false));
      browserHistory.push(Url.ERRORS_PAGE);
    })
  }
}

export const deleteOrder = (id, key) => {
  return (dispatch) => {
    dispatch(orderDeleting(true));
    let data = (key === 'review') ? { order_product_id: id } : { order_id: id };
    let endPoint = (key === 'review') ? 'orders/delete_order_product' : 'orders/delete_order';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(orderDeleting(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(orderDeleting(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(orderDeleting(false));
          dispatch(orderDeletingSuccess(response.data));
          dispatch(setAlertMeassage(true, response.data.data, true));
          resolve(true);
        }
      }).catch(error => {
        dispatch(orderDeleting(false));
        dispatch(orderDeletingError(true));
        // dispatch(setAlertMeassage(true, 'Some Error Occured.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const viewOrder = (id, key) => {
  return (dispatch) => {
    dispatch(fetchingViewOrder(true));
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: `${Config.url}orders/view_order`,
        params: {
          order_id: id
        },
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetchingViewOrder(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetchingViewOrder(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          if (response.data.orderDetail.order_date != undefined) {
            var spdate = (response.data.orderDetail.order_date.split("T"))[0].split("-");
            response.data.orderDetail["new_order_date"] = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
          }
          if (response.data.orderDetail.order_expected_date != undefined) {
            var spdate = (response.data.orderDetail.order_expected_date.split("T"))[0].split("-");
            response.data.orderDetail["new_order_expected_date"] = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
          }
          dispatch(fetchingViewOrder(false));
          dispatch(viewOrderSuccess(response.data));
          if (!response.data.products.length > 0) dispatch(setCreateOrderBtnState(true));
          let orders = {
            order_id: response.data.orderDetail.order_id,
            order_number: response.data.orderDetail.order_number,
            order_company_key_id: response.data.orderDetail.order_company_key_id
          }
          saveLocalStorage('orders', orders);
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetchingViewOrder(false));
        dispatch(viewOrderError(true));
        // dispatch(setAlertMeassage(true, 'Some Error Occured.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const placeOrder = (orderId, orderNumber, key) => {
  return (dispatch) => {
    dispatch(orderPlacing(true));
    let data = {
      order_id: orderId,
      order_number: orderNumber,
      recv_company_key_id: key
    }

    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: `${Config.url}orders/place_order`,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(orderPlacing(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(orderPlacing(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          let placedData = {
            data: response.data.data,
            isPlaced: true
          }
          saveLocalStorage('placedData', placedData);
          dispatch(orderPlacing(false));
          dispatch(orderPlacingSuccess(response.data));
          dispatch(setAlertMeassage(true, response.data.data, true));
          resolve(true);
        }
      }).catch(error => {
        dispatch(orderPlacing(false));
        dispatch(orderPlacingError(true));
        // dispatch(setAlertMeassage(true, 'Some Error Occured.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const cancelOrder = (orderDetail) => {
  return (dispatch) => {
    dispatch(orderCanceling(true));
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: `${Config.url}orders/cancel_order`,
        data: orderDetail,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(orderCanceling(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(orderCanceling(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(orderCanceling(false));
          dispatch(setAlertMeassage(true, response.data.data, true));
          resolve(true);
        }
      }).catch(error => {
        dispatch(orderCanceling(false));
        dispatch(orderCancelingError(true));
        // dispatch(setAlertMeassage(true, 'Some Error Occured.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const setProps = (status, key) => {
  return (dispatch) => {
    switch (key) {
      case 'isEdit':
        dispatch(setEditState(status));
        break;
      case 'isSave':
        dispatch(setSaveState(status));
        break;
      case 'isReviewOrder':
        dispatch(setReviewState(status));
        break;
      case 'isProductEdit':
        dispatch(setProductEditState(status));
        break;
      case 'doProductEmpty':
        dispatch(fetchingProductsSuccess(status));
        break;
      case 'isUpload':
        dispatch(setUploadStatus(status));
        break;
      case 'isDownload':
        dispatch(setDownloadStatus(status));
        break;
      case 'isDownloadCatalogue':
        dispatch(setDownloadCatalogueStatus(status));
        break;
      case 'isDecline':
        dispatch(setDeclineState(status));
        break;
      case 'showCreateOrderBtn':
        dispatch(setCreateOrderBtnState(status));
        break;
      case 'doSupplierEmpty':
        dispatch(fetchingSupplierCompniesSuccess(status));
        break;
    }
  }
}

export const resetProps = () => {
  return (dispatch) => {
    dispatch(resetAlreadySetProps());
  }
}

export const resetProducts = () => {
  return (dispatch) => {
    dispatch(resetAlreadySetProducts());
  }
}

export const setAddProductState = (status) => {
  return (dispatch) => {
    dispatch(settingAddProductState(status));
  }
}

export const fetchProducts = (queryString = '') => {
  return (dispatch) => {
    axios({
      method: 'get',
      url: `${Config.url}masters/get_products`,
      headers: { 'token': getToken() }
    }).then(response => {
      if (response.data.error === 1) {
        dispatch(setAlertMeassage(true, response.data.data, false));
      } else if (response.data.error === 5) {
        dispatch(setAlertMeassage(true, response.data.data, false));
        browserHistory.push(Url.LOGIN_PAGE);
      } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
        dispatch(fetchingProductsSuccess(response.data));
      }
    }).catch(error => {
      dispatch(fetchingProductsError(true));
      // dispatch(setAlertMeassage(true, 'Some Error Occured.', false));
      browserHistory.push(Url.ERRORS_PAGE);
    });
  }
}

export const addProducts = (values, isProductEdit) => {
  return (dispatch) => {
    dispatch(AddingProduct(true));
    let endPoint = (isProductEdit) ? 'orders/update_order/product' : 'orders/add_product_order';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: values,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(AddingProduct(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(AddingProduct(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(AddingProduct(false));
          dispatch(AddingProductSuccess(response.data));
          dispatch(setCreateOrderBtnState(false));
        } else if (response.data.error === 1) {
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(AddingProduct(false));
        dispatch(AddingProductError(true));
        // dispatch(setAlertMeassage(true, 'Some Error Occured.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const getLinkOrders = (currentPage = 1, pageSize = 5, frmDt = '', toDt = '') => {
  return (dispatch) => {
    dispatch(fetchingLinkOrders(true));
    let data = {
      page_number: currentPage,
      page_size: pageSize,
      from_date: frmDt,
      to_date: toDt
    };
    let endPoint = 'orders/list_linking_orders';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetchingLinkOrders(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetchingLinkOrders(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          response.data.data.map((item) => {
            item["new_order_date"] = '';
            if (item.order_date != undefined) {
              var spdate = (item.order_date.split("T"))[0].split("-");
              item["new_order_date"] = spdate[2] + "-" + spdate[1] + "-" + spdate[0];
            }
          })
          dispatch(fetchingLinkOrders(false));
          let respFilterByDate = filterByDate(response.data);
          let totalPage = getPages(response.data.total_recordcount, pageSize);
          dispatch(setTotalRecordCount(response.data.total_recordcount));
          dispatch(setTotalPages(totalPage));
          dispatch(fetchingLinkOrdersSuccess(respFilterByDate.data));
        } else if (response.data.error === 1) {
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetchingLinkOrders(false));
        dispatch(LinkOrdersError(true));
        // dispatch(setAlertMeassage(true, 'Some Error Occured.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const supplierCompanyType = () => {
  return (dispatch) => {
    dispatch(savingOrders(true));
    dispatch(errorSavingOrders(false));
    let endPoint = '';
    endPoint = 'masters/get_supplier_type'
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: Config.url + endPoint,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response)
        if (response.data.error === 1) {
          dispatch(savingOrders(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(savingOrders(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(savingOrders(false));
          dispatch(supplierCompanyTypeVal(response.data))
        } else if (response.data.error === 1) {
          dispatch(savingOrders(false));
          dispatch(errorSavingOrders(true));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(savingOrders(false));
        dispatch(errorSavingOrders(true));
        // dispatch(setAlertMeassage(true, 'Order not saved.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}
export const getCompany = (company_type_id) => {
  return (dispatch) => {
    // dispatch(fetching(true)); //stop loader on Company dropdown
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: `${Config.url}users/get_company`,
        params: {
          company_type_id:company_type_id
        },
        headers: {'token': getToken()}
      }).then( response => {
        if (response.data.error === 1) {
          dispatch(savingOrders(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(savingOrders(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(savingOrders(false));
          dispatch(companyTypeSuccess(response.data));
        }
        resolve(true);
      }).catch( error => {
        dispatch(savingOrders(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'Something went wrong.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const fetchProductsByID = (company_role_id) => {
  return (dispatch) => {
    let data = {
      company_role_id: company_role_id
    }
    let endPoint = 'masters/get_products_by_role_id';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetchingProductsSuccess(response.data));
        }
        resolve(true);
      }).catch(error => {
        dispatch(AddingProduct(false));
        dispatch(AddingProductError(true));
        // dispatch(setAlertMeassage(true, 'Some Error Occured.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const downloadOrderProduct = () => {
  return (dispatch) => {
    dispatch(fetchingOrders(true));
    let endPoint = 'masters/get_order_products_catalogue';
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: Config.url + endPoint,
        headers: { 'token': getToken() },
        responseType: 'blob',
      }).then(response => {
        console.log(response)
        if (response.data.error === 1) {
          dispatch(fetchingOrders(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetchingOrders(false));
          dispatch(fetchingOrders(true, response.data.data, false));
          //browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetchingOrders
            (false));
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          link.href = url;
          
          link.setAttribute('download', 'order_product_catalogue.xlsx');
          document.body.appendChild(link);
          link.click();
  
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetchingOrders(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const downloadOrderCompany = () => {
  return (dispatch) => {
    dispatch(fetchingOrders(true));
    let endPoint = 'masters/get_supplier_companies_catalogue';
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: Config.url + endPoint,
        headers: { 'token': getToken() },
        responseType: 'blob',
      }).then(response => {
        console.log(response)
        if (response.data.error === 1) {
          dispatch(fetchingOrders(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetchingOrders(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          //browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetchingOrders(false));
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          link.href = url;
          
          link.setAttribute('download', 'order_company_catalogue.xlsx');
          document.body.appendChild(link);
          link.click();
  
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetchingOrders(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const actions = {
  resetAlertBox,
  fetchOrders,
  openCreateOrderForm,
  saveOrders,
  fetchSupplierCompnies,
  fetchProducttType,
  fetchProductCategory,
  deleteOrder,
  viewOrder,
  placeOrder,
  setProps,
  fetchProducts,
  resetProducts,
  setAddProductState,
  addProducts
};

// ------------------------------------
// Action handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [FETCHING_ORDERS]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching
    }
  },
  [OUTGOING_RECEIVED_SUCCESS]: (state, action) => {
    return {
      ...state,
      outgoingOrders: action.outgoingOrders
    }
  },
  [INCOMING_RECEIVED_SUCCESS]: (state, action) => {
    return {
      ...state,
      incomingOrders: action.incomingOrders
    }
  },
  [ERROR_FETCHING_ORDERS]: (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
  [SET_FORM_STATE]: (state, action) => {
    return {
      ...state,
      isOrderFormOpen: action.isOrderFormOpen
    }
  },
  [SAVING_ORDERS]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching
    }
  },
  [ORDERS_SAVE_SUCCESS]: (state, action) => {
    return {
      ...state,
      savedOrdersDetail: action.savedOrdersDetail
    }
  },
  [ERROR_SAVING_ORDERS]: (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
  [LINKING_ORDER]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching
    }
  },
  [LINKING_ORDER_SUCCESS]: (state, action) => {
    return {
      ...state,
      linkedOrdersDetail: action.linkedOrdersDetail
    }
  },
  [LINKING_ORDER_ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
  [SET_ALERT_MESSAGE]: (state, action) => {
    return {
      ...state,
      showAlert: action.showAlert,
      alertMessage: action.alertMessage,
      status: action.status,
    }
  },
  [FETCHING_SUPPLIER_COMPNIES]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching,
    }
  },
  [FETCHING_SUPPLIER_COMPNIES_SUCCESS]: (state, action) => {
    return {
      ...state,
      supplierCompniesList: action.supplierCompniesList,
    }
  },
  [FETCHING_SUPPLIER_COMPNIES_ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error,
    }
  },
  [FETCHING_PRODUCT_TYPE]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching,
    }
  },
  [FETCHING_PRODUCT_TYPE_SUCCESS]: (state, action) => {
    return {
      ...state,
      productTypeList: action.productTypeList,
    }
  },
  [FETCHING_PRODUCT_TYPE_ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error,
    }
  },
  [FETCHING_PRODUCT_CATEGORY]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching,
    }
  },
  [FETCHING_PRODUCT_CATEGORY_SUCCESS]: (state, action) => {
    return {
      ...state,
      productCategoryList: action.productCategoryList,
    }
  },
  [FETCHING_PRODUCT_CATEGORY_ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error,
    }
  },
  [ORDER_DELETING]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching,
    }
  },
  [ORDER_DELETING_SUCCESS]: (state, action) => {
    return {
      ...state,
      orderDeletedData: action.orderDeletedData,
    }
  },
  [ORDER_DELETING_ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error,
    }
  },
  [FETCHING_VIEW_ORDER]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching,
    }
  },
  [VIEW_ORDER_SUCCESS]: (state, action) => {
    return {
      ...state,
      viewOrderDetail: action.viewOrderDetail,
    }
  },
  [VIEW_ORDER_ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error,
    }
  },
  [ORDER_PLACING]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching,
    }
  },
  [ORDER_PLACING_SUCCESS]: (state, action) => {
    return {
      ...state,
      orderPlacedData: action.orderPlacedData,
    }
  },
  [ORDER_PLACING_ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error,
    }
  },
  [ORDER_CANCELING]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching,
    }
  },
  [ORDER_CANCELING_ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error,
    }
  },
  [SET_EDIT_STATE]: (state, action) => {
    return {
      ...state,
      isEdit: action.isEdit,
    }
  },
  [SET_SAVE_STATE]: (state, action) => {
    return {
      ...state,
      isSave: action.isSave,
    }
  },
  [SET_REVIEW_STATE]: (state, action) => {
    return {
      ...state,
      isReviewOrder: action.isReviewOrder,
    }
  },
  [RESET_PROPS]: (state, action) => {
    return {
      ...state,
      viewOrderDetail: action.viewOrderDetail,
    }
  },
  [FETCHING_PRODUCTS]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching,
    }
  },
  [FETCHING_PRODUCTS_SUCCESS]: (state, action) => {
    return {
      ...state,
      products: action.products,
    }
  },
  [FETCHING_PRODUCTS_ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error,
    }
  },
  [RESTE_ALREADY_SET_PRODUCTS]: (state, action) => {
    return {
      ...state,
      products: action.products,
    }
  },
  [SETTING_ADD_PRODUCT_STATE]: (state, action) => {
    return {
      ...state,
      isAddProduct: action.isAddProduct,
    }
  },
  [ADDING_PRODUCTS]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching,
    }
  },
  [ADDING_PRODUCTS_SUCCESS]: (state, action) => {
    return {
      ...state,
      addedProductDetail: action.addedProductDetail,
    }
  },
  [ADDING_PRODUCTS_ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error,
    }
  },
  [SET_PRODUCT_EDIT_STATE]: (state, action) => {
    return {
      ...state,
      isProductEdit: action.isProductEdit,
    }
  },
  [SET_UPLOAD_STATUS]: (state, action) => {
    return {
      ...state,
      isUpload: action.isUpload,
    }
  },
  [SET_DOWNLOAD_STATUS]: (state, action) => {
    return {
      ...state,
      isDownload: action.isDownload,
    }
  },
  [SET_DOWNLOAD_CATALOGUE_STATUS]: (state, action) => {
    return {
      ...state,
      isDownloadCatalogue: action.isDownloadCatalogue,
    }
  },
  [SET_TOTAL_RECORD_COUNT]: (state, action) => {
    return {
      ...state,
      totalRecordCount: action.totalRecordCount,
    }
  },
  [SET_TOTAL_PAGES]: (state, action) => {
    return {
      ...state,
      pages: action.pages,
    }
  },
  [SET_DECLINE_STATE]: (state, action) => {
    return {
      ...state,
      isDecline: action.isDecline,
    }
  },
  [FETCHING_LINK_ORDERS]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching,
    }
  },
  [LINK_ORDER_SUCCESS]: (state, action) => {
    return {
      ...state,
      linkOrders: action.linkOrders,
    }
  },
  [LINK_ORDERS_ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error,
    }
  },
  [SET_CREATE_ORDER_BTN_STATE]: (state, action) => {
    return {
      ...state,
      showCreateOrderBtn: action.showCreateOrderBtn,
    }
  },
  [ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
  [COMPANY_ROLE_SUCCESS]: (state, action) => {
    return {
      ...state,
      companyRole: action.companyRole
    }
  },
  [SET_COMPANY_DETAIL]: (state, action) => {
    return {
      ...state,
      viewCompanyInfo: action.viewCompanyInfo
    }
  },
  [SET_CURRENT_NEW_PAGE]: (state, action) => {
    return {
      ...state,
      currentNewPage: action.currentNewPage
    }
  },
  [PO_EDIT_STATUS_DETAIL]: (state, action) => {
    return {
      ...state,
      isPoEdit: action.isPoEdit
    }
  },
  [SET_JOB_ACTIVE]: (state, action) => {
    return{
      ...state,
      isJobActive: action.isJobActive
    }
  },
  [RESET_JOB_ACTIVE]: (state, action) => {
    return{
      ...state,
      isJobActive: action.isJobActive
    }
  },
  [EXTERNAL_JOB_STATUS]: (state, action) => {
    return{
      ...state,
      jobStatus: action.jobStatus
    }
  },
  [SET_PRODUCT_STATUS_DETAIL]: (state, action) => {
    return{
      ...state,
      isProduct: action.isProduct
    }
  },
  [OUTGOING_DOWNLOAD_ORDER]: (state, action) => {
    return{
      ...state,
      outgoingDownloadOrder: action.outgoingDownloadOrder
    }
  },
  [INCOMING_DOWNLOAD_ORDER]: (state, action) => {
    return{
      ...state,
      incomingDownloadOrder: action.incomingDownloadOrder
    }
  },
  [SUPPLIER_COMPANY_TYPE_VAL]: (state, action) => {
    return{
      ...state,
      supplierCompanyList: action.supplierCompanyList
    }
  },
  [COMPANY_TYPE_SUCCESS]: (state, action) => {
    return{
      ...state,
      companyDetailVal: action.companyDetailVal
    }
  },
  [COMPANY_KEY_ID]: (state, action) => {
    return{
      ...state,
      companyKeyIdVal: action.companyKeyIdVal
    }
  },
  [RESET_COMPANY_KEY_ID]: (state, action) => {
    return{
      ...state,
      companyKeyIdVal: action.companyKeyIdVal
    }
  }
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  companyRole: [],
  viewCompanyInfo: {},
  fetching: false,
  error: false,
  outgoingOrders: [],
  incomingOrders: [],
  isOrderFormOpen: false,
  showAlert: false,
  savedOrdersDetail: [],
  linkedOrdersDetail: [],
  alertMessage: '',
  supplierCompniesList: [],
  productTypeList: [],
  productCategoryList: [],
  orderDeletedData: "",
  viewOrderDetail: [],
  orderPlacedData: "",
  isAddProduct: false,
  isEdit: false,
  isSave: false,
  isReviewOrder: false,
  products: [],
  addedProductDetail: [],
  status: false,
  isProductEdit: false,
  isUpload: false,
  isDownload: false,
  isDownloadCatalogue: false,
  totalRecordCount: 0,
  pages: 1,
  isDecline: false,
  linkOrders: [],
  showCreateOrderBtn: false,
  currentNewPage: 1,
  isPoEdit: false,
  isJobActive: '',
  jobStatus: false,
  isProduct: false,
  outgoingDownloadOrder: '',
  incomingDownloadOrder: '',
  supplierCompanyList: '',
  companyDetailVal: '',
  companyKeyIdVal: ''
};

export default function ordersReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
