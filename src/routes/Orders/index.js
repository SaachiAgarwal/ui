import { injectReducer } from '../../store/reducers';
import {checkPageRestriction} from '../index';

export default (store) => ({
  path: 'orders',
  onEnter: (nextState, replace) => {
    checkPageRestriction(nextState, replace, () => {})
  },
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const Orders = require('./containers/OrdersContainer').default;
      const reducer = require('./modules/orders').default;
      injectReducer(store, { key: 'Orders', reducer });
      cb(null, Orders);
  }, 'Orders');
  },
});
