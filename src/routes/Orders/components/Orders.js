import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Button, Row } from 'reactstrap';
import Modal from 'react-modal';
import Swal from 'sweetalert2';
import FileDownload from 'js-file-download';

import { Url } from 'config/Config';
import NavBar from 'components/navbar/NavBar';
import Loader from 'components/Loader';
import Alert from "components/Alert";
import Data from 'components/navbar/Data';
import InnerHeader from 'components/InnerHeader';
import HeaderSection from 'components/HeaderSection';
import NavDrawer from 'components/NavDrawer';
import OutgoingOrdersTable from './OutgoingOrdersTable';
import IncomingOrdersTable from './IncomingOrdersTable';
import options from 'components/options';
import CreateOrder from './CreateOrder';
import LinkModal from './LinkModal';
import Footer from 'components/Footer';
import SubmitButtons from 'components/SubmitButtons';
import ReviewOrder from './ReviewOrder';
import CsvModal from 'components/CsvModal';
import DeclineModal from './DeclineModal';
import OrderFilter from './OrderFilter';
import { getDate, getSupplierCompanyKeyId, getLocalStorage, getText, removeLocalStorage, saveLocalStorage, getCompanyId } from 'components/Helper';
import 'css/style.scss';
import './Orders.scss';
import ExternalJob from './ExternalJob'
import { Images } from 'config/Config';


//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
import PoEdit from './PoEdit'
import ReactLoading from 'react-loading';

let strings = new LocalizedStrings(
  data
);

//  Custom styles for link modal
const customStyles = {
  content: {
    top: '0',
    left: '0',
    right: '0',
    bottom: '0',
    marginRight: '0',
    transform: 'translate(0, 0)'
  }
};

export const BtnGrp = (props) => {
  return (
    <div>
      <Button className='add-button' hidden={props.callFor === 'inOrder'  ? true : false} onClick={() => {
        props.openOrderForm();
        props.setReportFetching(false)
        // console.log(`fetching details`)
      }
      }>+</Button>
      <Row>
        <Button className='upload-button' hidden={props.callFor === 'inOrder'  ? true : false} onClick={() => props.handleCsvModal(true, 'upload')}>{strings.btnGrpText.uploadCSV}</Button>
        <Button className='download-button' hidden={props.callFor === 'inOrder'  ? true : false} onClick={() => props.handleCsvModal(true, 'download')}>{strings.btnGrpText.downloadCSV}</Button>
        <Button className='download-button' hidden={props.callFor === 'inOrder'  ? true : false} onClick={() => props.handleCsvModal(true, 'downloadCatalogue')}>{strings.btnGrpText.orderCatalogue}</Button>
        <Button className={(props.callFor === 'inOrder') ? 'upload-button' : 'download-button'} onClick={() => props.downloadOrderList(props.callFor)}>{strings.btnGrpText.downloadOrders}</Button>
      </Row>
      <Row>
        {(props.callFor !== 'inOrder') &&
          <div>
            <p style={{ whiteSpace: "nowrap" }} className="col-md-6 disclaimer">{strings.btnGrpText.disclaimer}</p>
          </div>
        }
      </Row>
    </div>
  );
}

export const OutgoingOrder = (props) => {
  return (
    <div>
      <OrderFilter
        role={props.role}
        handleFilterSearch={props.handleFilterSearch}
        handleFilterDateChange={props.handleFilterDateChange}
        fetchBrandsOrders={props.fetchBrandsOrders}
        options={props.options}
        pages={props.pages}
        fromDt={props.fromDt}
        toDt={props.toDt}
        searchText={props.searchText}
        entityName={props.entityName}
        filterfromDt={props.filterfromDt}
        filtertoDt={props.filtertoDt}
        currentLanguage={props.currentLanguage}
        fetchingFromReport={props.fetchingFromReport}
        fromDate={props.fromDate}
        toDate={props.toDate}
      />
      <OutgoingOrdersTable
        data={props.outgoingOrders}
        pagination={false}
        search={false}
        exportCSV={false}
        options={props.options}
        isForModal={false}
        outgoingFormatter={props.outgoingFormatter}
        setActivePage={props.setActivePage}
        totalRecordCount={props.totalRecordCount}
        pages={props.pages}
        searchText={props.searchText}
        fetchOrders={props.fetchOrders}
        handleDateChange={props.handleDateChange}
        setCurrentNewPage={props.setCurrentNewPage}
        currentNewPage={props.currentNewPage}
        currentLanguage={props.currentLanguage}
        fetchingFromReport={props.fetchingFromReport}
        fromDate={props.fromDate}
        toDate={props.toDate}
        checkOrderOutbox={props.checkOrderOutbox}
        filterfromDt = {props.filterfromDt}
        filtertoDt = {props.filtertoDt}
      />
    </div>
  );
}

export const IncomingOrder = (props) => {
  return (
    <div>
      <OrderFilter
        role={props.role}
        handleFilterSearch={props.handleFilterSearch}
        handleFilterDateChange={props.handleFilterDateChange}
        brandEntities={props.brandEntities}
        fetchBrandsOrders={props.fetchBrandsOrders}
        options={props.options}
        pages={props.pages}
        fromDt={props.fromDt}
        toDt={props.toDt}
        searchText={props.searchText}
        entityName={props.entityName}
        filterfromDt={props.filterfromDt}
        filtertoDt={props.filtertoDt}
        currentLanguage={props.currentLanguage}
      />
      <IncomingOrdersTable
        data={props.incomingOrders}
        pagination={false}
        search={false}
        exportCSV={false}
        options={props.options}
        isForModal={false}
        incomingFormatter={props.incomingFormatter}
        setActivePage={props.setActivePage}
        totalRecordCount={props.totalRecordCount}
        pages={props.pages}
        searchText={props.searchText}
        fetchOrders={props.fetchOrders}
        handleDateChange={props.handleDateChange}
        setCurrentNewPage={props.setCurrentNewPage}
        currentNewPage={props.currentNewPage}
        currentLanguage={props.currentLanguage}
        fetchingFromReport={props.fetchingFromReport}
        fromDate={props.fromDate}
        toDate={props.toDate}
        checkOrderInbox={props.checkOrderInbox}
        filterfromDt = {props.filterfromDt}
        filtertoDt = {props.filtertoDt}

      />
    </div>
  );
}

/** Orders
 *
 * @description This class is a based class for orders functionality
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class Orders extends React.Component {
  constructor(props) {
    super(props);
    this.handleCsvModal = this.handleCsvModal.bind(this);
    this.handleDeclineModal = this.handleDeclineModal.bind(this);
    this.setActivePage = this.setActivePage.bind(this);
    this.setCurrentOrderId = this.setCurrentOrderId.bind(this);
    this.handleAlertBox = this.handleAlertBox.bind(this);
    this.back = this.back.bind(this);
    this.edit = this.edit.bind(this);
    this.delete = this.delete.bind(this);
    this.place = this.place.bind(this);
    this.viewOrder = this.viewOrder.bind(this);
    this.editOrder = this.editOrder.bind(this);
    this.handleEditChange = this.handleEditChange.bind(this);
    this.deleteOrder = this.deleteOrder.bind(this);
    this.cancelOrder = this.cancelOrder.bind(this);
    this.placeOrder = this.placeOrder.bind(this);
    this.linkOrder = this.linkOrder.bind(this);
    this.incomingFormatter = this.incomingFormatter.bind(this);
    this.linkingFormatter = this.linkingFormatter.bind(this);
    this.outgoingFormatter = this.outgoingFormatter.bind(this);
    this.createOrders = this.createOrders.bind(this);
    this.handleLinkModal = this.handleLinkModal.bind(this);
    this.handleInboxOutboxOrders = this.handleInboxOutboxOrders.bind(this);
    this.goTo = this.goTo.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.resetParentState = this.resetParentState.bind(this);
    this.addProduct = this.addProduct.bind(this);
    this.updateView = this.updateView.bind(this);
    this.confirm = this.confirm.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.resetFilter = this.resetFilter.bind(this);
    this.downloadOrderList = this.downloadOrderList.bind(this);
    this.handleUnreadItems = this.handleUnreadItems.bind(this);
    this.openOrderForm = this.openOrderForm.bind(this);
    this.handleFilterDateChange = this.handleFilterDateChange.bind(this);
    this.handleFilterSearch = this.handleFilterSearch.bind(this);
    this.handleUserRole = this.handleUserRole.bind(this);
    this.externalJob = this.externalJob.bind(this)
    this.createExternal = this.createExternal.bind(this)
    this.checkOrderOutbox = this.checkOrderOutbox.bind(this)
    this.checkOrderInbox = this.checkOrderInbox.bind(this)

    this.state = {
      openModal: false,
      outboxOrders: false,
      callFor: 'inOrder',
      reviewOrderCallFor: 'incomingOrders',
      openDeclineModalFor: 'incomingOrders',
      fromDt: '',
      toDt: '',
      filterfromDt: '',
      filtertoDt: '',
      isSave: false,
      isEdit: false,
      active: "Outbox",
      previousOrderState: '',
      shouldUpdate: '',
      currentOrderId: '',
      currentRow: {},
      isLinked: 0,
      isPlaced: 0,
      role: '',
      isSuperUser: '',
      isViewIncomingOrders: false,
      activeCsvModal: 'upload',
      searchText: '',
      btnType: 'cancelBtn',
      user: {},
      owner: 'Order',
      isPoEdit: false,
      isJob: false,
      orders: ''
    }
  }

  async componentDidMount() {
    console.log(this.props.outgoingOrders)
  }

  async componentWillMount() {
    // console.log(`Orders will mount is called`);
    //used to set unread items in orders/shipment routes.
    this.props.handleUnreadItems('orders');

    let loginDetail = getLocalStorage('loginDetail');


    this.props.setGlobalState({
      navIndex: 0,
      navInnerIndex: (loginDetail.role === 'Brand' || this.props.nav) ? 1 : 0,
      navClass: strings.NavdrawerOptions[0].value
    });


    this.setState({ user: loginDetail });

    if (loginDetail.role !== 'Brand' && !this.props.nav) {
      // console.log("brand case is running");

      this.props.fetchOrders('incomingOrders');
      // this.props.fetchSupplierCompnies(loginDetail.userInfo.company_type_id);
      this.props.fetchProducttType();
      this.props.fetchProductCategory();
      this.setState({
        openModal: false,
        outboxOrders: false,
        role: loginDetail.role,
        isSuperUser: loginDetail.userInfo.is_super_user
      })
    } else {
      await this.setState({
        outboxOrders: true,
        openModal: false,
        callFor: 'outOrder',
        active: 'outbox',
        searchText: '',
        reviewOrderCallFor: 'outgoingOrders',
        openDeclineModalFor: 'outgoingOrders',
        isPlaced: 0,
        role: loginDetail.role,
        isSuperUser: loginDetail.userInfo.is_super_user
      });
      this.props.openCreateOrderForm(false);
      this.props.fetchOrders('outgoingOrders');
    }
    this.handleUserRole()
  }

  componentWillUnmount() {
    this.props.openCreateOrderForm(false);
    this.props.setCurrentNewPage(1)
    this.props.poEditStatus(false)
  }

  updateView() {
    this.props.fetchShipment('outgoingShipment');
  }

  resetProps() {
    this.props.resetProps();
  }

  async handleUserRole() {
    let loginData = getLocalStorage('loginDetail');
    await this.props.fetchCompanyRole(loginData.role)
    await this.props.viewCompanyDetail(loginData.userInfo.company_key_id)
    if (loginData.role === "Integrated Player")
      if (!_.isEmpty(this.props.viewCompanyInfo) && !_.isEmpty(this.props.companyRole)) {
        var userRole = this.props.viewCompanyInfo.roles.some((val) => (val.company_role_id === (this.props.companyRole.filter((val) => (val.company_rolename === "Garment Manufacturer") ? val.company_role_id : false))[0].company_role_id) ? true : false)
        await this.setState({ userRole: userRole })
      }
  }
  async addProduct() {
    this.props.setAddProductState(true);
    await this.setState({ active: 'addProduct' });
    let loginDetail = getLocalStorage('loginDetail');
  }

  async setCurrentOrderId(id) {
    await this.setState({ currentOrderId: id });
  }

  async setActivePage(status) {
    await this.setState({ active: status });
    // console.log(this.state.active)
  }

  async handleUnreadItems(orderId) {
    // Updating count to db
    await this.props.handleUnreadItems('orders', orderId);
    // Refreshing count
    this.props.handleUnreadItems('orders');
    // Refreshing order list to reset bold row


    this.props.fetchOrders('incomingOrders', options.currentPage, options.pageSize, '', '', this.state.searchText);
  }

  async handleAlertBox() {
    this.props.resetAlertBox(false, "");
  }

  async handleSearch(query) {
    await this.setState({ searchText: query });
    if (this.state.active === 'inbox') {
      if (query.length >= 2 || query.length === 0) {
        this.props.fetchOrders('incomingOrders', options.currentPage, options.pageSize, '', '', this.state.searchText);
      }
    } else {
      if (query.length >= 2 || query.length === 0) {
        this.props.fetchOrders('outgoingOrders', options.currentPage, options.pageSize, '', '', this.state.searchText);
      }
    }
  }

  async handleFilterSearch(query) {
    await this.setState({ searchText: query });
    if (this.state.active === 'inbox') {
      if (query.length >= 2 || query.length === 0) {
        this.props.fetchOrders('incomingOrders', options.currentPage, options.pageSize, this.state.filterfromDt, this.state.filtertoDt, this.state.searchText);
      }

    } else {
      if (query.length >= 2 || query.length === 0) {
        this.props.fetchOrders('outgoingOrders', options.currentPage, options.pageSize, this.state.filterfromDt, this.state.filtertoDt, this.state.searchText);
      }
    }
  }

  async handleFilterDateChange(date, key) {
    let filterfromDt = '';
    let filtertoDt = '';
    if (key === 'fromDate' && key !== 'toDate') {
      filterfromDt = Object.values(date).reduce((total, val, index) => (index <= 9) ? total + val : total);
      await this.setState({ filterfromDt: filterfromDt });
    } else if (key === 'toDate' && key !== 'fromDate') {
      filtertoDt = Object.values(date).reduce((total, val, index) => (index <= 9) ? total + val : total);
      await this.setState({ filtertoDt: filtertoDt });
    }

    (this.state.active === 'inbox') ?
      this.props.fetchOrders('incomingOrders', options.currentPage, options.pageSize, this.state.filterfromDt, this.state.filtertoDt, this.state.searchText)
      :
      this.props.fetchOrders('outgoingOrders', options.currentPage, options.pageSize, this.state.filterfromDt, this.state.filtertoDt, this.state.searchText);
  }

  async handleCsvModal(status, key) {
    if (key === 'upload') {
      await this.setState({
        activeCsvModal: 'upload',
        owner: 'Order',
      });
      this.props.setProps(status, 'isUpload');
      this.props.setGlobalState({ isUploaded: false });
    } else if (key === 'downloadCatalogue') {
      await this.setState({
        activeCsvModal: 'downloadCatalogue',
        owner: 'Order_Catalogue'
      });
      this.props.setProps(status, 'isDownloadCatalogue');
      this.props.setGlobalState({ isUploaded: false });
      if (status) {
        await this.props.fetchProducts();
        await this.props.fetchSupplierCompnies(this.state.user.userInfo.company_type_id);
      }
    } else {
      await this.setState({
        activeCsvModal: 'download',
        owner: 'Order'
      });
      this.props.setProps(status, 'isDownload');
      this.props.setGlobalState({ isUploaded: false });
    }
  }

  async handleDeclineModal(row, status, btnType) {
    // console.log(row, status, btnType);
    //setting report boolean
    this.props.setReportFetching(false);
    await this.setState({ btnType: btnType });
    if (!_.isEmpty(row)) {
      this.setState({
        currentOrderId: row.order_id,
        currentRow: row,
      });
    }

    this.props.setProps(status, 'isDecline');
    if (btnType === 'reasonBtn') {
      await this.setState({ openDeclineModalFor: 'reason' });
    } else if (btnType === 'cancelBtn') {
      await this.setState({ openDeclineModalFor: 'outgoingOrders' });
    } else {
      this.handleUnreadItems(row.order_id);
      await this.setState({ openDeclineModalFor: 'incomingOrders' });
    }
  }

  async viewOrder(row, key, Type) {

    //setting report boolean
    await this.props.fetchingOrders(true)
    setTimeout(
      async function () {
        this.props.setReportFetching(false);

        this.handleUserRole()
        await this.setState({
          isLinked: row.is_linked,
          isPlaced: row.is_order_placed,
          reviewOrderCallFor: key
        });
        //For BC supplier companies would not be available
        if (this.state.role !== 'Birla Cellulose') await this.props.fetchSupplierCompnies(this.state.user.userInfo.company_type_id);
        if (Type === 'incomingOrders') {
          this.handleUnreadItems(row.order_id);
          await this.props.viewOrder(row.order_id, key);
          this.setState({ isViewIncomingOrders: true })
        } else {
          await this.props.viewOrder(row.order_id, key);
        }
        this.setState({
          previousOrderState: this.state.active,
          active: 'reviewOrder',
        });
        this.props.setProps(true, 'isReviewOrder');
        this.props.openCreateOrderForm(true);
        this.props.fetchingOrders(false)
      }
        .bind(this),
      500
    );


  }

  async editOrder(row, key) {
    //setting report boolean
    this.props.setReportFetching(false);
    this.handleUserRole()
    await this.setState({ isLinked: row.is_linked });
    await this.props.fetchSupplierCompnies(this.state.user.userInfo.company_type_id);
    await this.props.viewOrder(row.order_id, key);
    await this.setState({
      isPlaced: row.is_order_placed
    })
    this.handleEditChange('mount');
    this.props.openCreateOrderForm(true);
    this.setState({ active: 'createOrder' });
  }

  async poEditOrder(row, key) {
    //setting report boolean
    this.props.setReportFetching(false);
    this.handleUserRole()
    await this.setState({ isLinked: row.is_linked });
    await this.props.fetchSupplierCompnies(this.state.user.userInfo.company_type_id);
    await this.props.viewOrder(row.order_id, key);
    await this.setState({
      isPlaced: row.is_order_placed,
      isPoEdit: true
    })

    await this.props.poEditStatus(true)
    this.handleEditChange('mount');
    this.props.openCreateOrderForm(true);
    this.setState({ active: 'createOrder' });
  }

  async handleEditChange(mode) {
    if (mode === 'mount') {
      await this.props.setProps(true, 'isEdit');
    } else if (mode === 'unmount') {
      this.setState({ isLinked: 0 });
      await this.props.setProps(false, 'isEdit');
      await this.props.setProps(false, 'isSave');
      this.props.resetProps();
    }
  }

  resetParentState() {
    this.setState({
      isViewIncomingOrders: false,
      active: this.state.previousOrderState
    });
    this.props.resetProps();
    this.props.setProps(false, 'isReviewOrder');
  }

  confirm(key, msg = '', callingFrom = '') {
    Swal({
      title: 'Warning!',
      text: `${msg}`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'yes, delete it',
      cancelButtonText: 'No, keep it'
    }).then(async (result) => {
      if (result.value) {
        if (key === 'delete') {
          if (callingFrom === 'fromTable') {
            await this.props.deleteOrder(this.state.currentOrderId);
            this.props.fetchOrders('outgoingOrders');
          } else if (callingFrom === 'fromReview') {
            await this.props.deleteOrder(this.state.currentOrderId);
            this.props.fetchOrders('outgoingOrders');
            this.props.openCreateOrderForm(false);
            this.props.setProps(false, 'isReviewOrder');
          }
        }
      }
    })
  }

  async deleteOrder(row, key) {
    //setting report boolean
    this.props.setReportFetching(false);
    await this.setState({ currentOrderId: row.order_id });
    this.props.resetProps();
    this.confirm('delete', 'Do you really want to delete this order ?', 'fromTable');
  }

  async placeOrder(row) {
    //setting report boolean
    this.props.setReportFetching(false);
    await this.props.viewOrder(row.order_id);
    if (this.props.viewOrderDetail.products.length > 0) {
      await this.props.placeOrder(row.order_id, row.order_number, row.sender_company_key_id);
      this.props.fetchOrders('outgoingOrders');
    } else {
      this.props.resetAlertBox(true, 'Add at least one product to order');
    }
  }

  async cancelOrder(value) {
    // console.log(`cancel is called`);
    let orderDetail = {
      order_id: this.state.currentOrderId,
      cancel_reason: value.message,
      order_number: this.state.currentRow.order_number,
      recv_company_key_id: (this.state.openDeclineModalFor === 'incomingOrders') ? this.state.currentRow.sender_company_key_id : this.state.currentRow.receiver_company_key_id
    }
    await this.props.cancelOrder(orderDetail);
    this.handleDeclineModal({}, false, this.state.btnType);
    // console.log(this.state.openDeclineModalFor);


    if (this.state.openDeclineModalFor === 'incomingOrders') {
      this.props.fetchOrders('incomingOrders');

    } else {
      this.props.fetchOrders('outgoingOrders');

    }


  }

  async linkOrder(row, order_number) {
    if (!_.isEmpty(this.props.viewOrderDetail.orderDetail) && this.props.viewOrderDetail.orderDetail.linked_order_number !== null && this.props.viewOrderDetail.orderDetail.linked_order_number !== row.order_number) {
      this.props.resetAlertBox(true, 'First unlink the linked order');
    } else {
      let lStorage = getLocalStorage('orders');
      let linkOrderVal = {
        order_id: lStorage.order_id,
        linkedorder_id: row.order_id,
        linkedcompany_key_id: row.sender_company_key_id
      };

      // this.line is working as a switch. it close the link modal when user link/unlink the shipment
      (this.props.viewOrderDetail.orderDetail.linked_order_number === row.order_number) ?
        await this.setState({ openModal: true, isLinked: 1 }) :
        await this.setState({ openModal: false, isLinked: 0 });

      // performing link and unlink order
      await this.props.linkOrder(linkOrderVal, this.state.isLinked);

      // fetch outgoing orders to reset isLinked state accordingly
      this.props.fetchOrders('outgoingOrders');

      this.props.outgoingOrders.map(async (item) => {
        if (item.order_id === lStorage.order_id) {
          await this.setState({ isLinked: item.is_linked });
        }
      });

      // fetch updated viewOrderDetail to render the form with updated data
      await this.props.viewOrder(lStorage.order_id, 'outgoingOrders');

      this.props.getLinkOrders(options.currentPage, options.modalPageSize, this.state.fromDt, this.state.toDt);
      // this.line is working as a switch. it close the link modal when user link/unlink the order
      //this.setState({openModal: false});

      // if (this.state.isLinked !== 1) {
      //   // open again the link modal if user unlink the order to reset the link/unlink button value 
      //   this.setState({openModal: true});
      //   this.props.fetchOrders('incomingOrders');
      // }
    }
  }

  openOrderForm() {
    this.props.openCreateOrderForm(true);
    this.setState({ active: 'createOrder' });
    this.props.setProps(false, 'isReviewOrder');
    this.props.setProps([], 'doSupplierEmpty');
    saveLocalStorage('supplierDetail', '');
  }

  incomingFormatter(row, cell) {
    return (
      <ButtonGroup>
        <Button className="action" onClick={() => this.viewOrder(cell, 'incomingOrders', 'incomingOrders')} >{strings.incomingFormatterText.view}</Button>
        {(cell.is_cancelled !== 1 && cell.order_status !== 'Fulfilled') && <Button className="action delete" onClick={() => this.handleDeclineModal(cell, true, 'declineBtn')} >{strings.incomingFormatterText.cancel}</Button>}
        {(cell.is_cancelled === 1) && <Button className="action" onClick={() => this.handleDeclineModal(cell, true, 'reasonBtn')} >{strings.incomingFormatterText.reason}</Button>}
      </ButtonGroup>
    );
  }

  linkingFormatter(row, cell) {
    return (
      <ButtonGroup>
        <Button className="action" onClick={() => this.linkOrder(cell, this.props.viewOrderDetail.orderDetail.linked_order_number)} >{(!_.isEmpty(this.props.viewOrderDetail.orderDetail)) ? ((cell.order_number === this.props.viewOrderDetail.orderDetail.linked_order_number) ? 'unlink' : 'link') : 'link'}</Button>
      </ButtonGroup>
    );
  }

  outgoingFormatter(row, cell) {
    return (
      <ButtonGroup>
        <Button className="action" onClick={() => this.viewOrder(cell, 'outgoingOrders', 'outgoingOrders')} >{strings.outgoingFormatterText.view}</Button>
        {(cell.is_order_placed !== 1) && <Button className="action" onClick={() => this.editOrder(cell, 'outgoingOrders')} >{strings.outgoingFormatterText.edit}</Button>}
        {(cell.is_order_placed !== 1) && <Button className="action delete" onClick={() => this.deleteOrder(cell)} >{strings.outgoingFormatterText.delete}</Button>}
        {(cell.is_linked !== 1 && cell.is_cancelled !== 1 && this.state.role !== 'Brand' && cell.is_order_placed !== 1) && <Button className="action delete" onClick={() => this.handleLinkModal('open', cell)} >{strings.outgoingFormatterText.link}</Button>}
        {(cell.is_order_placed !== 1) && <Button className="action" onClick={() => this.placeOrder(cell)} >{strings.outgoingFormatterText.place}</Button>}
        {(cell.is_order_placed === 1 && cell.is_cancelled !== 1 && cell.order_status !== 'Fulfilled') && <Button className="action delete" onClick={() => this.handleDeclineModal(cell, true, 'cancelBtn')} >{strings.outgoingFormatterText.cancel}</Button>}
        {(cell.is_cancelled === 1) && <Button className="action" onClick={() => this.handleDeclineModal(cell, true, 'reasonBtn')} >{strings.outgoingFormatterText.reason}</Button>}
        {((cell.is_order_placed === 1) && (cell.is_cancelled !== 1)) && <Button className="action" onClick={() => this.poEditOrder(cell, 'outgoingOrders')} >{strings.outgoingFormatterText.edit}</Button>}
      </ButtonGroup>
    );
  }

  checkOrderOutbox(row, cell) {
    return (
      <ButtonGroup>
        {(cell.order_status == "Fulfilled") ? <img title="Shipment Received" style={{ width: '15px' }} src={Images.greenLight} /> : <img title="Waiting for Shipment" style={{ width: '15px' }} src={Images.yellowLight} />}

      </ButtonGroup>
    );
  }


  checkOrderInbox(row, cell) {
    return (
      <ButtonGroup>
        {(cell.linked_order_id == null) ? <img title="Order not linked" style={{ width: '15px' }} src={Images.orangeLight} /> : <img title="Order Linked" style={{ width: '15px' }} src={Images.yellowLight} />}
        {(cell.order_status == "Fulfilled") && <img title="Shipment Placed" style={{ width: '15px', marginLeft: '26%' }} src={Images.greenLight} />}
      </ButtonGroup>
    );
  }





  async createOrders(orders) {
    console.log(orders);
    let loginDetail = getLocalStorage('loginDetail');
    let supplierDetail = getLocalStorage('supplierDetail');
    orders.company_name = supplierDetail;
    orders.order_date = (orders.order_date.split('T'))[0];
    if (orders.order_expected_date !== undefined) orders.order_expected_date = (orders.order_expected_date.split('T'))[0];
    if(loginDetail.role !== "Integrated Player"){
    let supplier_company_key_id = getSupplierCompanyKeyId(this.props.supplierCompniesList, orders);
    orders.supplier_company_key_id = supplier_company_key_id;
    if (orders.supplier_company_key_id === undefined || orders.supplier_company_key_id === '') {
      this.props.resetAlertBox(true, 'Please choose supplier name');
    } else if (orders.company_name === undefined || orders.company_name === '') {
      this.props.resetAlertBox(true, 'Please choose correct supplier name');
    } else if ((this.props.isEdit) && _.isEmpty(orders.products)) {
      this.props.resetAlertBox(true, 'Add at least one product to order');
    } else {
      if (this.props.isEdit) {
        this.setState({
          active: 'reviewOrder',
          orders: orders
        });
        let lStorage = getLocalStorage('orders');
        orders.order_id = lStorage.order_id;
        await this.props.saveOrders(orders, this.props.isEdit);
        await this.props.viewOrder(getLocalStorage('orders').order_id);// calling viewOrder api is must to show updated review page
        // must call fetchOrders to show update outgoing orders list after edit orders
        this.props.fetchOrders('outgoingOrders');

        this.props.setProps(true, 'isReviewOrder');
      } else {
        await this.props.saveOrders(orders, this.props.isEdit);

        // if order save successfully
        if (!this.props.error) {
          this.props.fetchOrders('outgoingOrders');

          this.props.viewOrder(getLocalStorage('orders').order_id); // calling viewOrder is must here to show the link/unlik button based on is_linked key
        }
      }
    }
    await this.props.poEditStatus(false)
    }

    else{
      //orders.company_name = orders.supplier_company_name;
      //orders.company_key_id =  getCompanyId(this.props.companyDetailVal, orders.company_key_id, false);
      let supplier_company_key_id = getCompanyId(this.props.supplierCompniesList, orders.company_name, false);
      orders.supplier_company_key_id = supplier_company_key_id;
      if (orders.supplier_company_key_id === undefined || orders.supplier_company_key_id === '') {
        this.props.resetAlertBox(true, 'Please choose supplier name');
      } else if (orders.company_name === undefined || orders.company_name === '') {
        this.props.resetAlertBox(true, 'Please choose correct supplier name');
      } else if ((this.props.isEdit) && _.isEmpty(orders.products)) {
        this.props.resetAlertBox(true, 'Add at least one product to order');
      } else {
        if (this.props.isEdit) {
          this.setState({
            active: 'reviewOrder',
            orders: orders
          });
          let lStorage = getLocalStorage('orders');
          orders.order_id = lStorage.order_id;
          await this.props.saveOrders(orders, this.props.isEdit);
          await this.props.viewOrder(getLocalStorage('orders').order_id);// calling viewOrder api is must to show updated review page
          // must call fetchOrders to show update outgoing orders list after edit orders
          this.props.fetchOrders('outgoingOrders');
  
          this.props.setProps(true, 'isReviewOrder');
        } else {
          await this.props.saveOrders(orders, this.props.isEdit);
  
          // if order save successfully
          if (!this.props.error) {
            this.props.fetchOrders('outgoingOrders');
  
            this.props.viewOrder(getLocalStorage('orders').order_id); // calling viewOrder is must here to show the link/unlik button based on is_linked key
          }
        }
      }
      await this.props.poEditStatus(false)
    }
    
  }

  async handleLinkModal(key, row = {}) {
    //setting report boolean
    this.props.setReportFetching(false);
    if (!_.isEmpty(row)) await this.props.viewOrder(row.order_id);
    (key === 'open') ? this.setState({ openModal: true }) : this.setState({ openModal: false });
  }

  handleInboxOutboxOrders(classType, innerType) {
    this.props.setInnerNav(false);
    if (classType === 'orders' && innerType === 'inbox') {
      this.props.setCurrentNewPage(1)
      this.setState({
        outboxOrders: false,
        openModal: false,
        callFor: 'inOrder',
        active: 'inbox',
        searchText: '',
        reviewOrderCallFor: 'incomingOrders',
        openDeclineModalFor: 'incomingOrders',
        isPlaced: 0,
      });
      this.props.openCreateOrderForm(false);
      this.props.fetchOrders('incomingOrders');

    } else if (classType === 'orders' && innerType === 'outbox') {
      this.props.setCurrentNewPage(1)
      this.setState({
        outboxOrders: true,
        openModal: false,
        callFor: 'outOrder',
        active: 'outbox',
        searchText: '',
        reviewOrderCallFor: 'outgoingOrders',
        openDeclineModalFor: 'outgoingOrders',
        isPlaced: 0,
      });
      this.props.openCreateOrderForm(false);

      this.props.fetchOrders('outgoingOrders');
    } else if (classType === 'shipments' && innerType === 'inbox') {
    } else if (classType === 'shipments' && innerType === 'outbox') {
    } else if (classType === 'logout') {
      this.setState({
        outboxOrders: false,
        openModal: false,
        callFor: 'logout',
        active: 'logout'
      });

      // console.log(`logout case callled`);
      this.props.setCurrentLanguage('en');
      // console.log(this.props.currentLanguage)
      removeLocalStorage('loginDetail');
      browserHistory.push(Url.HOME_PAGE);
    }
  }

  async back() {
    await this.props.poEditStatus(false)
    await this.setState({
      filterfromDt: '', 
      filtertoDt: ''

    })

    await this.props.resetCompanyKeyId()
    await console.log(this.props.companyKeyIdVal)
    if (this.props.jobStatus === true && this.props.isProduct === true) {
      await this.props.setProductStatus(false)
    }
    else if (this.props.jobStatus === true) {
      let lStorage = getLocalStorage('orders');
      this.state.orders.order_id = lStorage.order_id;
      //await this.props.saveOrders(this.state.orders, this.props.isEdit);
      await this.props.viewOrder(getLocalStorage('orders').order_id);// calling viewOrder api is must to show updated review page
      // must call fetchOrders to show update outgoing orders list after edit orders
      this.props.fetchOrders('outgoingOrders');
      this.props.setProps(true, 'isReviewOrder');
      await this.props.externalJobStatus(false)
    }
    else if (this.props.isAddProduct) {
      this.setActivePage('createOrder');
      this.props.setAddProductState(false);
    } else {
      this.props.openCreateOrderForm(false);
      this.props.setProps(false, 'isReviewOrder');
      await this.props.externalJobStatus(false)
      await this.props.resetJobActive('')
    }
    (this.state.openDeclineModalFor === 'incomingOrders') ?
      this.props.fetchOrders('incomingOrders', this.props.currentNewPage, options.pageSize, this.state.fromDt, this.state.toDt) :
      this.props.fetchOrders('outgoingOrders', this.props.currentNewPage, options.pageSize, this.state.fromDt, this.state.toDt);
  }

  async edit() {
    let lStorage = getLocalStorage('orders');
    await this.props.viewOrder(lStorage.order_id);

    // setting isLinked state accordingly
    this.setState({ isLinked: this.props.viewOrderDetail.orderDetail.is_linked });
    this.handleEditChange('mount');
    this.props.setProps(false, 'isReviewOrder');
    this.props.openCreateOrderForm(true);
    this.setState({ active: 'createOrder' });
  }

  async delete() {
    let lStorage = getLocalStorage('orders')
    await this.setState({ currentOrderId: lStorage.order_id });
    this.props.resetProps();
    this.confirm('delete', 'Do you really want to delete this order ?', 'fromReview');
  }

  async place() {
    let lStorage = getLocalStorage('orders');
    await this.props.viewOrder(lStorage.order_id);
    if (this.props.viewOrderDetail.products.length > 0) {
      await this.props.placeOrder(lStorage.order_id, lStorage.order_number, lStorage.order_company_key_id);
      //commented
      this.props.fetchOrders('outgoingOrders');

      this.props.openCreateOrderForm(false);
      this.props.setProps(false, 'isReviewOrder');
    } else {
      this.props.resetAlertBox(true, 'Add at least one product to order');
    }
  }

  async handleDateChange(date, key) {
    let fromDt = '';
    let toDt = '';
    if (key === 'fromDate' && key !== 'toDate') {
      fromDt = Object.values(date).reduce((total, val, index) => (index <= 9) ? total + val : total);
      await this.setState({ fromDt: fromDt });
    } else if (key === 'toDate' && key !== 'fromDate') {
      toDt = Object.values(date).reduce((total, val, index) => (index <= 9) ? total + val : total);
      await this.setState({ toDt: toDt });
    }

    if (this.state.fromDt !== '' && this.state.toDt !== '' && this.state.fromDt >= this.state.toDt) {
      alert('From-Date must be less then To-Date');
    } else if (this.state.fromDt !== '' && this.state.toDt !== '') {
      this.props.getLinkOrders(options.currentPage, options.modalPageSize, this.state.fromDt, this.state.toDt);
    }
  }

  async resetFilter() {
    await this.setState({
      fromDt: '',
      toDt: ''
    });
  }

  goTo() {
    if (this.props.isOrderFormOpen) {
      this.props.openCreateOrderForm(false);
    } else {
      browserHistory.goBack();
    }
  }

  async download(list, type, fileName) {
    let data = 'purchase_order,buyer,order_date,description,order_status,supplier,product_name,product_qty,product_unit,product_description,\n';
    if (!_.isEmpty(list)) {
      list.map((item) => {
        data += `${item.order_number},`
        data += `${item.sender_company_name},`
        data += `${item.order_date},`
        data += `${item.order_description},`
        data += `${item.order_status},`
        data += `${item.receiver_company_name},`
        data += `${item.product_name},`
        data += `${item.product_qty},`
        data += `${item.product_uom},`
        data += `${item.product_description},`
        data += '\n';
      });
    }

    FileDownload(data, fileName);
    await this.props.fetchOrders(type, options.currentPage, options.pageSize, '', '', this.state.searchText);
  }

  async downloadOrderList(key) {
    let data = '',
      fileName = 'Order.csv',
      type = 'incomingOrders';
    if (key === 'inOrder') {
      fileName = 'IncomingOrderList.csv';
      type = 'incomingOrders';
      await this.props.fetchDownloadOrders(type, options.currentPage, this.props.totalRecordCount, '', '', this.state.searchText);
      this.download(this.props.incomingDownloadOrder, type, fileName);
    } else {
      fileName = 'OutgoingOrderList.csv';
      type = 'outgoingOrders';
      await this.props.fetchDownloadOrders(type, options.currentPage, this.props.totalRecordCount, '', '', this.state.searchText);
      this.download(this.props.outgoingDownloadOrder, type, fileName);
    }
  }

  async externalJob() {
    // await this.setState({
    //   isJob: true,
    //   active: 'externalJob'
    // })
    this.props.externalJobStatus(true)
  }

  async createExternal(values) {
    values.preventDefault()
    // console.log("yes")
    // console.log(values)
  }

  render() {
    strings.setLanguage(this.props.currentLanguage);
    // console.log(this.props.fromDate, this.props.toDate);
    return (
      <div>
        {/* <Loader loading={this.props.fetching} /> */}
        {this.props.fetching &&
          <ReactLoading type="bubbles" style={{
            fill: '#4e5be1e0',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundSize: '200px',
            position: 'fixed',
            zIndex: 10000,
            width: '6%',
            height: '72%',
            top: '50%',
            left: '50%'
          }} />
        }
        <Alert
          showAlert={(typeof this.props.showAlert !== "undefined" ? this.props.showAlert : false)}
          message={(typeof this.props.alertMessage !== "undefined" ? this.props.alertMessage : "")}
          handleAlertBox={this.handleAlertBox}
          status={this.props.status}
        />
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-2 col-lg-2 sidebar">
              <NavDrawer
                role={this.state.role}
                isSuperUser={this.state.isSuperUser}
                setGlobalState={this.props.setGlobalState}
                navIndex={this.props.navIndex}
                navInnerIndex={this.props.navInnerIndex}
                navClass={this.props.navClass}
                handleInboxOutboxOrders={this.handleInboxOutboxOrders}
                unreadItems={this.props.unreadItems}
                currentLanguage={this.props.currentLanguage}
                setInnerNav={this.props.setInnerNav}
                setParam={this.props.setParam}
                setSearchString={this.props.setSearchString}
                setReportFetching={this.props.setReportFetching}

              />
            </div>
            <div className="col-md-10 col-lg-10 col-lg-offset-2 col-md-offset-2 main">
              {(this.props.isJobActive) ?
                <HeaderSection
                  active={this.props.isJobActive}
                  searchText={this.state.searchText}
                  handleSearch={this.handleSearch}
                  setCurrentLanguage={this.props.setCurrentLanguage}
                  HeaderArray={strings.HeaderArray}
                  currentLanguage={this.props.currentLanguage}

                />
                :
                <HeaderSection
                  active={this.state.active}
                  searchText={this.state.searchText}
                  handleSearch={this.handleSearch}
                  setCurrentLanguage={this.props.setCurrentLanguage}
                  HeaderArray={strings.HeaderArray}
                  currentLanguage={this.props.currentLanguage}

                />
              }
              <Modal
                isOpen={this.state.openModal}
                style={customStyles}
                ariaHideApp={false}
              >
                <LinkModal
                  handleLinkModal={this.handleLinkModal}
                  getLinkOrders={this.props.getLinkOrders}
                  orders={this.props.linkOrders}
                  options={options}
                  linkingFormatter={this.linkingFormatter}
                  handleDateChange={this.handleDateChange}
                  pages={this.props.pages}
                  fromDt={this.state.fromDt}
                  toDt={this.state.toDt}
                  resetFilter={this.resetFilter}
                  currentLanguage={this.props.currentLanguage}
                  fetchOrders={this.props.fetchOrders}
                />
              </Modal>
              <Modal
                isOpen={this.props.isUpload || this.props.isDownload || this.props.isDownloadCatalogue}
                style={customStyles}
                ariaHideApp={false}
              >
                <CsvModal
                  owner={this.state.owner}
                  role={this.state.role}
                  handleCsvModal={this.handleCsvModal}
                  getText={getText}
                  activeCsvModal={this.state.activeCsvModal}
                  uploadFile={this.props.uploadFile}
                  updateView={this.props.fetchOrders}
                  viewToBeUpdated={'outgoingOrders'}
                  data={this.props.outgoingOrders}
                  isUploaded={this.props.isUploaded}
                  msg={this.props.msg}
                  productsList={this.props.products}
                  compniesList={this.props.supplierCompniesList}
                  currentLanguage={this.props.currentLanguage}
                  downloadOrderCompany={this.props.downloadOrderCompany}
                  downloadOrderProduct={this.props.downloadOrderProduct}
                />
              </Modal>
              <Modal
                isOpen={this.props.isDecline}
                style={customStyles}
                ariaHideApp={false}
              >
                <DeclineModal
                  onSubmit={this.cancelOrder}
                  handleDeclineModal={this.handleDeclineModal}
                  openDeclineModalFor={this.state.openDeclineModalFor}
                  currentOrderId={this.state.currentOrderId}
                  btnType={this.state.btnType}
                  viewOrder={this.props.viewOrder}
                  viewOrderDetail={this.props.viewOrderDetail}
                  currentLanguage={this.props.currentLanguage}
                />
              </Modal>
              {(!this.props.isOrderFormOpen) ?
                <div>
                  {(this.state.outboxOrders) ?
                    <OutgoingOrder
                      handleFilterSearch={this.handleFilterSearch}
                      outgoingOrders={this.props.outgoingOrders}
                      pagination={false}
                      search={false}
                      exportCSV={false}
                      options={options}
                      isForModal={false}
                      outgoingFormatter={this.outgoingFormatter}
                      setActivePage={this.setActivePage}
                      totalRecordCount={this.props.totalRecordCount}
                      pages={this.props.pages}
                      searchText={this.state.searchText}
                      filterfromDt={this.state.filterfromDt}
                      filtertoDt={this.state.filtertoDt}
                      fetchOrders={this.props.fetchOrders}
                      handleFilterDateChange={this.handleFilterDateChange}
                      setCurrentNewPage={this.props.setCurrentNewPage}
                      currentNewPage={this.props.currentNewPage}
                      currentLanguage={this.props.currentLanguage}
                      fetchingFromReport={this.props.fetchingFromReport}
                      fromDate={this.props.fromDate}
                      toDate={this.props.toDate}
                      checkOrderOutbox={this.checkOrderOutbox}
                      filterfromDt = {this.state.filterfromDt}
                      filtertoDt = {this.state.filtertoDt}

                    />
                    :
                    <IncomingOrder
                      incomingOrders={this.props.incomingOrders}
                      handleFilterSearch={this.handleFilterSearch}
                      pagination={false}
                      search={false}
                      exportCSV={false}
                      options={options}
                      isForModal={false}
                      incomingFormatter={this.incomingFormatter}
                      setActivePage={this.setActivePage}
                      totalRecordCount={this.props.totalRecordCount}
                      pages={this.props.pages}
                      searchText={this.state.searchText}
                      filterfromDt={this.state.filterfromDt}
                      filtertoDt={this.state.filtertoDt}
                      fetchOrders={this.props.fetchOrders}
                      handleFilterDateChange={this.handleFilterDateChange}
                      setCurrentNewPage={this.props.setCurrentNewPage}
                      currentNewPage={this.props.currentNewPage}
                      currentLanguage={this.props.currentLanguage}
                      fetchingFromReport={this.props.fetchingFromReport}
                      fromDate={this.props.fromDate}
                      toDate={this.props.toDate}
                      checkOrderInbox={this.checkOrderInbox}
                      filterfromDt = {this.state.filterfromDt}
                      filtertoDt = {this.state.filtertoDt}

                    />
                  }
                  <BtnGrp
                    callFor={this.state.callFor}
                    role={this.state.role}
                    openOrderForm={this.openOrderForm}
                    handleCsvModal={this.handleCsvModal}
                    setReportFetching={this.props.setReportFetching}
                    downloadOrderList={this.downloadOrderList}
                  />
                </div>
                :
                <div>
                  {
                    (!this.props.jobStatus) &&
                    <div>
                      {(!this.props.isReviewOrder) ?
                        (!this.props.isPoEdit) &&
                        <CreateOrder
                          userRole={this.state.userRole}
                          onSubmit={this.createOrders}
                          role={this.state.role}
                          handleLinkModal={this.handleLinkModal}
                          supplierCompniesList={this.props.supplierCompniesList}
                          productTypeList={this.props.productTypeList}
                          productCategoryList={this.props.productCategoryList}
                          isSave={this.props.isSave}
                          isEdit={this.props.isEdit}
                          viewOrderDetail={this.props.viewOrderDetail}
                          handleEditChange={this.handleEditChange}
                          setCurrentOrderId={this.setCurrentOrderId}
                          options={options}
                          deleteOrder={this.props.deleteOrder}
                          fetchOrders={this.props.fetchOrders}
                          viewOrder={this.props.viewOrder}
                          addProduct={this.addProduct}
                          editProduct={this.editProduct}
                          isAddProduct={this.props.isAddProduct}
                          fetchProducts={this.props.fetchProducts}
                          products={this.props.products}
                          resetProducts={this.props.resetProducts}
                          setAddProductState={this.props.setAddProductState}
                          addProducts={this.props.addProducts}
                          editProduct={this.props.editProduct}
                          setProps={this.props.setProps}
                          isProductEdit={this.props.isProductEdit}
                          fetchUOM={this.props.fetchUOM}
                          uom={this.props.uom}
                          setActivePage={this.setActivePage}
                          showCreateOrderBtn={this.props.showCreateOrderBtn}
                          fetchSupplierCompnies={this.props.fetchSupplierCompnies}
                          companyId={this.state.user.userInfo.company_type_id}
                          isPlaced={this.state.isPlaced}
                          currentLanguage={this.props.currentLanguage}
                          supplierCompanyType={this.props.supplierCompanyType}
                          supplierCompanyList={this.props.supplierCompanyList}
                          getCompany={this.props.getCompany}
                          companyDetailVal={this.props.companyDetailVal}
                          companyKeyId = {this.props.companyKeyId}
                          companyKeyIdVal = {this.props.companyKeyIdVal}
                          fetchProductsByID = {this.props.fetchProductsByID}
                        />
                        :
                        <ReviewOrder
                          fetching={this.props.fetching}
                          role={this.state.role}
                          userRole={this.state.userRole}
                          handleLinkModal={this.handleLinkModal}
                          supplierCompniesList={this.props.supplierCompniesList}
                          productTypeList={this.props.productTypeList}
                          productCategoryList={this.props.productCategoryList}
                          options={options}
                          shouldUpdate={this.state.shouldUpdate}
                          incomingOrders={this.props.incomingOrders}
                          viewOrderDetail={this.props.viewOrderDetail}
                          handleEditChange={this.handleEditChange}
                          resetParentState={this.resetParentState}
                          deleteOrder={this.props.deleteOrder}
                          fetchOrders={this.props.fetchOrders}
                          viewOrder={this.props.viewOrder}
                          setProps={this.props.setProps}
                          resetProps={this.props.resetProps}
                          isViewIncomingOrders={this.state.isViewIncomingOrders}
                          setActivePage={this.setActivePage}
                          reviewOrderCallFor={this.state.reviewOrderCallFor}
                          isPlaced={this.state.isPlaced}
                          currentLanguage={this.props.currentLanguage}
                          supplierCompanyType={this.props.supplierCompanyType}
                          supplierCompanyList={this.props.supplierCompanyList}
                          getCompany={this.props.getCompany}
                          companyDetailVal={this.props.companyDetailVal}
                        />
                      }
                    </div>
                  }
                  <div>
                    {this.props.isPoEdit &&
                      <PoEdit
                        userRole={this.state.userRole}
                        onSubmit={this.createOrders}
                        role={this.state.role}
                        handleLinkModal={this.handleLinkModal}
                        supplierCompniesList={this.props.supplierCompniesList}
                        productTypeList={this.props.productTypeList}
                        productCategoryList={this.props.productCategoryList}
                        isSave={this.props.isSave}
                        isEdit={this.props.isEdit}
                        viewOrderDetail={this.props.viewOrderDetail}
                        handleEditChange={this.handleEditChange}
                        setCurrentOrderId={this.setCurrentOrderId}
                        options={options}
                        deleteOrder={this.props.deleteOrder}
                        fetchOrders={this.props.fetchOrders}
                        viewOrder={this.props.viewOrder}
                        addProduct={this.addProduct}
                        editProduct={this.editProduct}
                        isAddProduct={this.props.isAddProduct}
                        fetchProducts={this.props.fetchProducts}
                        products={this.props.products}
                        resetProducts={this.props.resetProducts}
                        setAddProductState={this.props.setAddProductState}
                        addProducts={this.props.addProducts}
                        editProduct={this.props.editProduct}
                        setProps={this.props.setProps}
                        isProductEdit={this.props.isProductEdit}
                        fetchUOM={this.props.fetchUOM}
                        uom={this.props.uom}
                        setActivePage={this.setActivePage}
                        showCreateOrderBtn={this.props.showCreateOrderBtn}
                        fetchSupplierCompnies={this.props.fetchSupplierCompnies}
                        companyId={this.state.user.userInfo.company_type_id}
                        isPlaced={this.state.isPlaced}
                        currentLanguage={this.props.currentLanguage}

                      />
                    }
                  </div>
                  <div>
                    {this.props.jobStatus &&
                      <ExternalJob
                        onSubmit={this.createExternal}
                        setActivePage={this.setActivePage}
                        currentLanguage={this.props.currentLanguage}
                        setCurrentLanguage={this.props.setCurrentLanguage}
                        currentLanguage={this.props.currentLanguage}
                        setJobActive={this.props.setJobActive}
                        resetJobActive={this.props.resetJobActive}
                        externalJobStatus={this.props.externalJobStatus}


                        userRole={this.state.userRole}
                        role={this.state.role}
                        handleLinkModal={this.handleLinkModal}
                        supplierCompniesList={this.props.supplierCompniesList}
                        productTypeList={this.props.productTypeList}
                        productCategoryList={this.props.productCategoryList}
                        isSave={this.props.isSave}
                        isEdit={this.props.isEdit}
                        viewOrderDetail={this.props.viewOrderDetail}
                        handleEditChange={this.handleEditChange}
                        setCurrentOrderId={this.setCurrentOrderId}
                        options={options}
                        deleteOrder={this.props.deleteOrder}
                        fetchOrders={this.props.fetchOrders}
                        viewOrder={this.props.viewOrder}
                        addProduct={this.addProduct}
                        editProduct={this.editProduct}
                        isAddProduct={this.props.isAddProduct}
                        fetchProducts={this.props.fetchProducts}
                        products={this.props.products}
                        resetProducts={this.props.resetProducts}
                        setAddProductState={this.props.setAddProductState}
                        addProducts={this.props.addProducts}
                        editProduct={this.props.editProduct}
                        setProps={this.props.setProps}
                        isProductEdit={this.props.isProductEdit}
                        fetchUOM={this.props.fetchUOM}
                        uom={this.props.uom}
                        setActivePage={this.setActivePage}
                        showCreateOrderBtn={this.props.showCreateOrderBtn}
                        fetchSupplierCompnies={this.props.fetchSupplierCompnies}
                        companyId={this.state.user.userInfo.company_type_id}
                        isPlaced={this.state.isPlaced}
                        currentLanguage={this.props.currentLanguage}

                        setProductStatus={this.props.setProductStatus}
                        isProduct={this.props.isProduct}
                      />
                    }


                  </div>
                  <div className='btn-right'>
                    <Button className={(this.props.isReviewOrder) ? "btn btn-primary reviewReview" : "btn btn-primary  create non-cancel"} onClick={this.back}>{strings.scan.back}</Button>
                    {((this.props.isReviewOrder) && this.state.isPlaced === 0) && <Button className={(this.props.isReviewOrder) ? "btn btn-primary reviewReview" : "btn btn-primary  create non-cancel"} onClick={this.edit}>{strings.captalise.edit}</Button>}
                    {((this.props.isReviewOrder) && this.state.isPlaced === 0) && <Button className={(this.props.isReviewOrder) ? "btn btn-primary reviewCancel" : "btn btn-primary  create cancel"} onClick={this.delete}>{strings.captalise.delete}</Button>}
                    {((this.props.isReviewOrder) && this.state.isPlaced === 0) && <Button className={(this.props.isReviewOrder) ? "btn btn-primary reviewReview" : "btn btn-primary  create non-cancel"} onClick={this.place}>{strings.captalise.place}</Button>}
                  </div>
                </div>
              }
              <div className="clearfix"></div>
              <Footer
                currentLanguage={this.props.currentLanguage}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Orders.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default Orders;