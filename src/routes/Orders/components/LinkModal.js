import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';

import LinkingOrderTable from './LinkingOrderTable';
import RenderDatePicker from 'components/RenderDatePicker';

import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);


export const LinkModalHeader = (props) => {
	return(
		<Button className="modal-btn" onClick={() => {

			props.handleLinkModal('close');
			props.fetchOrders('outgoingOrders');

			
		}}
		
		>X</Button>
	);
}

/** LinkModal
 *
 * @description This class having a modal that will display a list of incoming orders and having a filter on that list
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class LinkModal extends React.Component {
	constructor(props) {
		super(props);
	}

  componentWillUnmount() {
    this.props.resetFilter();
  }

  async componentWillMount() {
    await this.props.getLinkOrders(this.props.options.currentPage, this.props.options.modalPageSize, this.props.fromDt, this.props.toDt);
  }

	render() {
		const { handleSubmit, currentLanguage } = this.props;
		strings.setLanguage(currentLanguage);

		return(
			<div className="modal">
				<Row>
					<LinkModalHeader 
						handleLinkModal={this.props.handleLinkModal}
						fetchOrders = {this.props.fetchOrders}
						/>
				</Row>
				<Row>
					<form onSubmit={handleSubmit}>
						<div className='col-12 col-sm-12 col-xl-12 mb-2'>
		        	<Row>
		        		<Col className="text-center date-picker col-6 col-sm-6 col-xl-6 pr-1">
		        			<label>{strings.OrderFilterText.From}</label>
		        			<Field
		                name='from_date'
		                type='date'
		                component={RenderDatePicker}
		                label='Select'
                    onChange={(date) => this.props.handleDateChange(date, 'fromDate')}
		              />
		        		</Col>
		        		<Col className="text-center date-picker col-6 col-sm-6 col-xl-6 pr-1">
		        			<label>{strings.OrderFilterText.To}</label>
		        			<Field
		                name='to_date'
		                type='date'
		                component={RenderDatePicker}
		                label='Select'
                    onChange={(date) => this.props.handleDateChange(date, 'toDate')}
		              />
		        		</Col>
		        	</Row>
		        </div>
		      </form>
		    </Row>
				<LinkingOrderTable
					data={this.props.orders}
					pagination={false}
          search={false}
          exportCSV={false}
          isForModal={false}
          linkingFormatter={this.props.linkingFormatter}
          getLinkOrders={this.props.getLinkOrders}
          pages={this.props.pages}
          fromDt={this.props.fromDt}
					toDt={this.props.toDt}
					currentLanguage =  {this.props.currentLanguage}
				/>
			</div>
		);
	}
}

export default reduxForm({
  form: 'LinkModal',
})(LinkModal)