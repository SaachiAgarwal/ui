import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Pagination from 'react-paginate';
import { Images } from 'config/Config';
//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);

/** OutgoingOrdersTable
 *
 * @description This class is responsible to display a orders list of outgoing orders
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class OutgoingOrdersTable extends Component {

  constructor(props) {
    super(props);
    this.handlePageClick = this.handlePageClick.bind(this);

    this.state = {
      order_status: [],
      var: 0
    }
    // console.log(props.data)  
  }

  componentWillMount() {
    this.props.setActivePage('outbox');
  }

  handlePageClick(value) {
    this.props.fetchOrders('outgoingOrders', value.selected + 1, this.props.options.pageSize, this.props.filterfromDt, this.props.filtertoDt, this.props.searchText);
    this.props.setCurrentNewPage(value.selected + 1)
  }

  render() {
    const { currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);
    var i = 0
    console.log(this.state.order_status)
    // console.log(this.props.data)
    return (
      <div>

        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options}>
          <TableHeaderColumn dataField='order_id' hidden={true}>Order Id</TableHeaderColumn>
          <TableHeaderColumn dataField='order_number'>{strings.OrdersTableText.purchaseOrder}</TableHeaderColumn>
          <TableHeaderColumn dataField='receiver_company_name' columnTitle={true}>{strings.OrdersTableText.Supplier}</TableHeaderColumn>
          <TableHeaderColumn dataField='new_order_date'>{strings.OrdersTableText.orderDate}</TableHeaderColumn>
          <TableHeaderColumn dataField='order_description' isKey={true}>{strings.OrdersTableText.orderDescription}</TableHeaderColumn>
          <TableHeaderColumn dataField='order_status'>{strings.OrdersTableText.orderStatus}</TableHeaderColumn>
          <TableHeaderColumn dataFormat={this.props.checkOrderOutbox}>{strings.OrdersTableText.orderChecklist}</TableHeaderColumn>
          {/* <TableHeaderColumn dataField='sender_company_name'>{strings.OrdersTableText.Supplier}</TableHeaderColumn> */}
          <TableHeaderColumn key="editAction" dataFormat={this.props.outgoingFormatter}>{strings.OrdersTableText.Action}</TableHeaderColumn>
        </BootstrapTable>
     
        
          {/* <span style={{ color: 'rgba(0,0,0,.5)'}}><img style={{width: '26px',paddingLeft: '10%',marginRight: '4px'}} title="FulFilled" src={Images.greenLight} />**Fulfilled Order</span> */}
          {/* <p><span style={{ color: 'rgba(0,0,0,.5)'}}><img style={{width: '26px',paddingLeft: '10%',marginRight: '4px'}} title="FulFilled" src={Images.greenLight} />**Fulfilled Order</span></p> */}

     
        <div className="pagination-box">
          <Pagination
            initialPage={this.props.currentNewPage - 1}
            previousLabel={strings.prev}
            nextLabel={strings.next}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={this.props.pages}
            pageRangeDisplayed={5}
            onPageChange={(value) => {


              this.handlePageClick(value);
            }
            }
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        </div>
      </div>
    );
  }
}

OutgoingOrdersTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default OutgoingOrdersTable;