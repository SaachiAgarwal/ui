import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
import DropdownList from 'react-widgets/lib/DropdownList';
import 'react-widgets/dist/css/react-widgets.css';
import _ from 'lodash';
import Autocomplete from "react-autocomplete";

import SubmitButtons from 'components/SubmitButtons';
import renderField from './renderField';
import { getSupplierCompanyKeyId, getCategory, getType } from 'components/Helper';
import { getLocalStorage } from 'components/Helper';
import HeaderSection from 'components/HeaderSection';
//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
import RenderDatePicker from 'components/RenderDatePicker';
import AddProduct from './AddProduct';
import AddExternalProduct from './AddExternalProduct'
let strings = new LocalizedStrings(
  data
);


/** func Validate
 * description validate method will validate all the control fields of form based on provided criteria
 *
 * return Array of Errors
 */
const validate = values => {
  const errors = {}
  if (!values.inv_number) {
    errors.inv_number = 'Required'
  }
  if (!values.external_vendor) {
    errors.external_vendor = 'Required'
  }
  if (!values.ship_trans) {
    errors.ship_trans = 'Required'
  }
  if (!values.inv_date){
    errors.inv_date = 'Required'
  }
  return errors
}

const renderDropdownList = ({ input, data, valueField, textField, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div>
      <DropdownList {...input}
        data={data}
        onChange={input.onChange}
      />
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

/** CreateOrder
 *
 * @description This class is responsible to display a form to create order
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class ExternalJob extends React.Component {
  constructor(props) {
    super(props);
    // this.showMenu = this.showMenu.bind(this);
    // this.returnAutoComplete = this.returnAutoComplete.bind(this);
    // this.matchProducts = this.matchProducts.bind(this);

    this.state = {
      initData: {},
      blendpercentage: "100",
      active: 'addProduct',
      isProduct: false
    }
    this.props.setJobActive('externalJob')
    this.onSubmit = this.onSubmit.bind(this)
    this.onAddProduct = this.onAddProduct.bind(this)
  }

  async componentWillUnmount() {
    this.props.resetJobActive('')
    this.props.externalJobStatus(false)
  }

  async onSubmit(values) {
    // console.log(values)

  }

  // async componentDidMount(){
  //   await this.props.setProps(true, 'isReviewOrder')
  // }

  async onAddProduct() {
    this.setState({
      isProduct: true
    })
    this.props.setProductStatus(true)
  }



  render() {
    const { handleSubmit, companiesList } = this.props;
    strings.setLanguage(this.props.currentLanguage);
    const {shipmentDetail, invoiceNumber, transportMode, invoiceDate, addProduct} = strings.createShipment;
    const {externalVendor, orderNumber} = strings.vendor;
    return (
      <div className='col-md-12 col-lg-12 create-order'>
        { !this.props.isProduct ? 
          <div>
          <h2>{shipmentDetail}</h2>
          <form onSubmit={handleSubmit(this.onSubmit)}>
            <div className="form-row">
              <div className="form-group col-md-4" style={{ paddingLeft: '5px' }}>
                <Field
                  name="inv_number"
                  type="number"
                  label={invoiceNumber}
                  component={renderField}
                />
              </div>
              <div className="form-group col-md-4" style={{ paddingLeft: '5px' }}>
                <Field
                  name="external_vendor"
                  type="text"
                  label={externalVendor}
                  component={renderField}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name="ship_trans"
                  type="text"
                  label={transportMode}
                  component={renderField}
                />
              </div>

              <div className="form-group col-md-4">
                <Field
                  name="order_number"
                  type="number"
                  label={orderNumber}
                  component={renderField}
                />
              </div>
              <div className="form-group col-md-4">
                <label>{invoiceDate}</label>
                <Field
                  name="inv_date"
                  type="number"
                  component={RenderDatePicker}
                />
              </div>

              <div className="form-group col-md-4">
                <label>{shipmentDetail}</label>
                <Field
                  name="ship_date"
                  type="number"
                  component={RenderDatePicker}
                />
              </div>

              <div className="form-group col-md-4">
                <Field
                  name="ship_desc"
                  type="text"
                  label="Description"
                  component={renderField}
                />
              </div>
            </div>
            <div>
              <button className="btn btn-primary create" onClick={this.onAddProduct} >{`${addProduct} +`}</button>
            </div>
            <div>
              <input type='submit' value={`${strings.createOrder.create} >`} className="btn btn-primary create" style={{left: '69%', marginBottom: '2px', width: '16%'}} />
            </div>
            </form>
            </div>
        
              :
              <div>
                <AddExternalProduct
                  userRole={this.props.userRole}
                  onSubmit={this.createProducts}
                  role={this.props.role}
                  selectedProduct={this.selectedProduct}
                  showMenu={this.showMenu}
                  inputvalue={this.state.inputvalue}
                  products={this.props.products}
                  fetchProducts={this.props.fetchProducts}
                  fetchAutoProducts={this.fetchAutoProducts}
                  resetProducts={this.props.resetProducts}
                  setAddProductState={this.props.setAddProductState}
                  isProductEdit={this.props.isProductEdit}
                  fetchUOM={this.props.fetchUOM}
                  uom={this.props.uom}
                  setActivePage={this.props.setActivePage}
                  setProps={this.props.setProps}
                  currentLanguage={this.props.currentLanguage}
                  setProductStatus={this.props.setProductStatus}
                  isProduct={this.props.isProduct}
                />
              </div>
        }
      </div >
    );
  }
}

ExternalJob.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
};

export default reduxForm({
  form: 'ExternalJob',
  validate,
})(ExternalJob)