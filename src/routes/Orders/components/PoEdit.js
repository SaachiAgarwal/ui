import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
import RenderDatePicker from 'components/RenderDatePicker';
import DropdownList from 'react-widgets/lib/DropdownList';
import 'react-widgets/dist/css/react-widgets.css';
import Autocomplete from "react-autocomplete";

import SubmitButtons from 'components/SubmitButtons';
import renderField from './renderField';
import { Images } from 'config/Config';
import { getSupplierCompanyKeyId, getCategory, getType, getProductId, getLocalStorage, getCurrentDate, saveLocalStorage } from 'components/Helper';
import AddOrderTable from './AddOrderTable';
import AddProduct from './AddProduct';

let productCategoryList = [];
let productTypeList = [];



//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);

/** func Validate
 * description validate method will validate all the control fields of form based on provided criteria
 *
 * return Array of Errors
 */
const validate = values => {
  const errors = {}
  if (!values.order_number) {
    errors.order_number = 'Required'
  }
  if (!values.company_name) {
    errors.company_name = 'Required'
  }
  if (!values.order_date) {
    errors.order_date = 'Required'
  } else if (values.order_date > getCurrentDate()) {
    errors.order_date = 'Date must not be greater then today'
  }
  /*if (!values.order_expected_date) {
    errors.order_expected_date = 'Required'
  }*/
  if (values.order_expected_date <= values.order_date) {
    errors.order_expected_date = 'Order due date must be greater then order date'
  }
  return errors
}

/** func renderItems
 *
 * @description This method push an object of the defined input controls into fields array to display a field array of 
 *   certain inputs
 *
 * return Array of control fields
 */
const renderItems = ({ fields, customProps, meta: { error, submitFailed } }) => {
  return (
    <div>
      <button type="button" className="btn btn-primary" onClick={customProps.addProduct}>
        {strings.createShipment.addProduct}<span className="glyphicon glyphicon-plus"></span>
      </button>
      {submitFailed && error && <span>{error}</span>}
    </div>
  );
}


/** func renderLink
 *
 * @description This method returns a field object having a span to achieve onClick functionality. Simply we can not use
 *   onClick on field wrapper.
 *
 * return A component
 */
const renderLink = ({ input, label, type, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className="btn-bs-file">
      <span className='form-control'>
        <p>{customProps.linked_order_number}</p>
        <span onClick={() => customProps.handleLinkModal('open')}>
          <img src={Images.fileUploadIcon} alt="" data-toggle="modal" data-target="#myModal" />
        </span>
      </span>
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

const renderLinkNew = ({ input, label, type, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className="btn-bs-file">
      <span className='form-control'>
        <p>{customProps.value}</p>
        {(input.name === 'linkTo') &&
          <span>
            <img src={Images.fileUploadIcon} alt="" data-toggle="modal" data-target="#myModal" />
          </span>
        }
      </span>
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

const renderDropdownList = ({ input, data, valueField, textField, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div>
      <DropdownList {...input}
        data={data}
        onChange={input.onChange}
      />
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

/** CreateOrder
 *
 * @description This class is responsible to display a form to create order
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class CreateOrder extends React.Component {
  constructor(props) {
    super(props);
    this.addOrderFormatter = this.addOrderFormatter.bind(this);
    this.deleteProductOrder = this.deleteProductOrder.bind(this);
    this.createProducts = this.createProducts.bind(this);
    this.selectedProduct = this.selectedProduct.bind(this);
    this.fetchAutoProducts = this.fetchAutoProducts.bind(this);
    this.showMenu = this.showMenu.bind(this);
    this.setParentState = this.setParentState.bind(this);

    this.state = {
      initData: {},
      editFormData: {},
      inputvalue: '',
      selectedProduct: '',
      selectedSupplier: '',
      companyName: '',
    }
  }

  async setParentState(key, status) {
    let obj = {};
    obj[key] = status;
    await this.setState(obj);
  }

  async componentWillMount() {
    // console.log("This is Po edit")
    this.props.setActivePage('createOrder');
    if (this.props.isEdit) {
      let lStorage = getLocalStorage('orders');
      await this.props.viewOrder(lStorage.order_id);
    }
    productCategoryList = this.props.productCategoryList;
    productTypeList = this.props.productTypeList;

    // Display default supplier for spinner
    if (this.props.role === 'Spinner') {
      this.selectedSupplier('Birla Cellulose');
      this.handleSupplier('Birla Cellulose');
    }
  }

  componentDidMount() {
    if (this.props.isEdit)
      this.handleInitialize();
  }

  componentWillUnmount() {
    this.props.handleEditChange('unmount');
    this.props.setProps(false, 'showCreateOrderBtn');
  }

  async handleInitialize() {
    if (!_.isEmpty(this.props.viewOrderDetail)) {
      this.props.setCurrentOrderId(this.props.viewOrderDetail.orderDetail.order_id);
      let company_name = getSupplierCompanyKeyId(this.props.supplierCompniesList, this.props.viewOrderDetail.orderDetail, true);
      const initData = {
        order_id: this.props.viewOrderDetail.orderDetail.order_id,
        order_number: this.props.viewOrderDetail.orderDetail.order_number,
        company_name: company_name,
        linked_order_number: this.props.viewOrderDetail.orderDetail.linked_order_number,
        order_date: this.props.viewOrderDetail.orderDetail.order_date.toString().split('T')[0],
        order_expected_date: (this.props.viewOrderDetail.orderDetail.order_expected_date !== null) ? this.props.viewOrderDetail.orderDetail.order_expected_date.toString().split('T')[0] : undefined,
        order_description: this.props.viewOrderDetail.orderDetail.order_description,
        products: this.props.viewOrderDetail.products,
      };
      initData.products.map((item, index) => {
        initData.products[index].index = index + 1;
        initData.products[index].order_id = initData.order_id;
        initData.products[index].category = getCategory(item.product_category_id, this.props.productCategoryList);
        initData.products[index].type = getType(item.product_type_id, this.props.productTypeList);
      });

      await this.setState({
        initData: initData,
        companyName: company_name
      });
      // save company_name to validate supplier name when creating/updating orders
      saveLocalStorage('supplierDetail', company_name);
      this.props.initialize(initData);
    }
  }

  async deleteProductOrder(row) {
    await this.props.deleteOrder(row.order_product_id, 'review');
    // fetch incoming orders to reset isLinked state accordingly
    await this.props.fetchOrders('outgoingOrders');
    // fetch updated viewOrderDetail to render the form with updated data
    await this.props.viewOrder(row.order_id, 'outgoingOrders');
    this.handleInitialize();
  }

  async editProductOrder(row) {
    this.setState({
      editFormData: row,
      selectedProduct: row.product_name
    });
    this.props.setProps(true, 'isProductEdit');// opening in edit state
    this.props.setAddProductState(true);// opening product form
  }

  addOrderFormatter(row, cell) {
    return (
      <ButtonGroup>
        <Button className="action" onClick={() => this.editProductOrder(cell, 'review')} >{strings.outgoingFormatterText.edit}</Button>
        <Button className="action delete" onClick={() => this.deleteProductOrder(cell, 'review')} >{strings.outgoingFormatterText.delete}</Button>
      </ButtonGroup>
    );
  }

  showMenu() {
    if (empty(this.props.product)) {
      return (
        "{'none'}"
      );
    }
  }

  async createProducts(values) {
    let lStorage = getLocalStorage('orders');
    let product_id = getProductId(this.props.products, this.state.selectedProduct);
    values.product_id = product_id;
    values.order_id = lStorage.order_id;
    values.product_name = this.state.selectedProduct;

    await this.props.addProducts(values, this.props.isProductEdit);
    await this.props.viewOrder(lStorage.order_id);
    this.props.setActivePage('createOrder');
    this.handleInitialize();
    this.props.setAddProductState(false);
  }

  fetchAutoProducts(product) {
    this.setState({ selectedProduct: product });
    this.setState({ inputvalue: product });
    if (product.length > 1) {
      this.props.fetchProducts();
    } else {
      this.props.setProps([], 'doProductEmpty');
    }
  }

  async selectedProduct(product, mode = '') {
    if (mode === 'unmount') {
      this.setState({
        inputvalue: '',
        selectedProduct: '',
      });
    } else {
      let productDetail = {};
      if (product != "product not found") {
        await this.setState({ selectedProduct: product });
        this.setState({ inputvalue: product });
        saveLocalStorage('productDetail', product);
      }
    }
  }

  returnAutoComplete() {
    return (
      <div className="customAutoComplete">
        <Autocomplete
          className="form-control"
          ref="autocomplete"
          menuStyle={{ display: this.props.showMenu, minWidth: '100%', }}
          inputProps={{ placeholder: "Start typing supplier name" }}
          items={this.props.supplierCompniesList}
          getItemValue={item => item.company_name}
          shouldItemRender={this.matchBuyer}
          renderItem={(item, highlighted) =>
            <div className='col px-2'
              key={item.product_id}
              style={{
                backgroundColor: "white", width: "100%", color: "black", paddingBottom: '5px',
                paddingTop: '5px', maxHeight: '150px', overflowY: 'auto'
              }}
            >
              {item.company_name}
            </div>
          }
          value={this.state.companyName}
          onChange={e => this.handleSupplier(e.target.value)}
          onSelect={value => this.selectedSupplier(value)}
        />
      </div>
    );
  }

  async selectedSupplier(supplier, mode = '') {
    if (mode === 'unmount') {
      this.setState({
        companyName: '',
        selectedSupplier: ''
      });
    } else {
      if (supplier != "product not found") {
        await this.setState({ selectedSupplier: supplier });
        this.setState({ companyName: supplier });
        saveLocalStorage('supplierDetail', supplier);
      }
    }
  }

  async handleSupplier(supplier) {
    await this.setState({ companyName: supplier });
    if (supplier.length > 1) {
      this.props.fetchSupplierCompnies(this.props.companyId);
    } else {
      this.props.setProps([], 'doSupplierEmpty');
    }
  }

  matchBuyer(state, value) {
    return (
      state.company_name.toLowerCase().indexOf(value.toLowerCase()) !== -1
    );
  }

  render() {
    const { handleSubmit, currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);
    const {linkBuyer, orderDate, orderDue, description} = strings.createOrder;
    let loginData = getLocalStorage('loginDetail'),
      dispbutn = (loginData.role === "Spinner" || loginData.role === "Birla Cellulose") ? true : false
    return (
      <div>
        {(!this.props.isAddProduct) ?
          <div className='col-md-12 col-lg-12 create-order'>
            <h2>{strings.createOrder.orderDetail}</h2>
            <div>
              <form onSubmit={handleSubmit}>
                <div className="form-row">
                  <div className="form-group col-md-4">
                    <Field
                      name='order_number'
                      type="text"
                      component={renderField}
                      label={`${strings.createOrder.purchaseOrder} *`}
                    />
                  </div>
                  <div className="form-group col-md-4">
                    <label>{(this.props.isViewIncomingOrders) ? `${strings.OrdersTableText.Buyer}` : `${strings.OrdersTableText.Supplier}`}</label>
                    <Field
                      className="select-menu"
                      name="company_name"
                      component={renderLinkNew}
                      customProps={{
                        value: (!_.isEmpty(this.state.initData)) ? this.state.initData.company_name : ''
                      }}
                    />
                  </div>
                  {(this.props.role !== 'Brand') &&
                    <div className="form-group col-md-4 ">
                      <label>{linkBuyer}</label>
                      <Field
                        name='linkTo'
                        type='text'
                        component={renderLinkNew}
                        customProps={{
                          value: (!_.isEmpty(this.state.initData)) ? this.state.initData.linked_order_number : ''
                        }}
                      />
                    </div>
                  }
                </div>
                <div className="form-row">
                  <div className="form-group col-md-4">
                    <label>{orderDate}</label>
                    <Field
                      name='orderDateorderDateorderDateorder_date'
                      type='date'
                      component={renderLinkNew}
                      customProps={{
                        value: (!_.isEmpty(this.state.initData)) ? this.state.initData.order_date : ''
                      }}
                    />
                  </div>
                  <div className="form-group col-md-4">
                    <label>{orderDue}</label>
                    <Field
                      name='order_expected_date'
                      type='date'
                      component={renderLinkNew}
                      customProps={{
                        value: (!_.isEmpty(this.state.initData)) ? this.state.initData.order_expected_date : ''
                      }}
                    />
                  </div>
                  <div className="form-group col-md-4">
                    {/* <label>{description}</label> */}
                    <Field
                      name='order_description'
                      type="text"
                      component={renderField}
                      customProps={{
                        value: (!_.isEmpty(this.state.initData)) ? this.state.initData.order_description : ''
                      }}
                      label={description}
                    />
                  </div>
                </div>
                {/* {(this.props.isEdit) &&
                  <div>
                    {(!_.isEmpty(this.state.initData) && !_.isEmpty(this.state.initData.products)) &&
                      <div className="col-md-12 nopad">
                        <h2>{strings.createOrder.productDetail}</h2>
                        <AddOrderTable
                          data={this.state.initData.products}
                          pagination={false}
                          search={false}
                          exportCSV={false}
                          options={this.props.options}
                          addOrderFormatter={this.addOrderFormatter}
                          role={this.props.role}
                          display={dispbutn}
                          isPlaced={this.props.isPlaced}
                          currentLanguage = {this.props.currentLanguage}
                        />
                      </div>
                    }
                    <FieldArray
                      name="products"
                      component={renderItems}
                      customProps={{
                        addProduct: this.props.addProduct,
                      }}
                    />
                  </div>
                } */}
                <div>
                  <SubmitButtons
                    submitLabel={(!this.props.isEdit) ? strings.next : strings.setting.create}
                    className='btn btn-primary create'
                    submitting={this.props.showCreateOrderBtn}
                  />
                </div>
              </form>
            </div>
          </div>
          :
          <div>
            <AddProduct
              userRole={this.props.userRole}
              onSubmit={this.createProducts}
              role={this.props.role}
              selectedProduct={this.selectedProduct}
              showMenu={this.showMenu}
              inputvalue={this.state.inputvalue}
              products={this.props.products}
              fetchProducts={this.props.fetchProducts}
              fetchAutoProducts={this.fetchAutoProducts}
              resetProducts={this.props.resetProducts}
              setAddProductState={this.props.setAddProductState}
              isProductEdit={this.props.isProductEdit}
              editFormData={this.state.editFormData}
              setParentState={this.setParentState}
              fetchUOM={this.props.fetchUOM}
              uom={this.props.uom}
              setActivePage={this.props.setActivePage}
              setProps={this.props.setProps}
              currentLanguage={this.props.currentLanguage}
            />
          </div>
        }
      </div>
    );
  }
}

CreateOrder.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
};

export default reduxForm({
  form: 'CreateOrder',
  validate,
})(CreateOrder)