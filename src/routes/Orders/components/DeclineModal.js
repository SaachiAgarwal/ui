import React from 'react';
import PropTypes from 'prop-types';
import {Button} from 'reactstrap';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import _ from 'lodash';
import FileDownload from 'js-file-download';

import SubmitButtons from 'components/SubmitButtons';
import RenderTextArea from 'components/RenderTextArea';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);


export const validate = (values) => {
  const errors = {}
  if (!values.message) {
    errors.message = 'Required'
  }
  return errors
}

export const CsvModalHeader = (props) => {
  const buttons = ['declineBtn', 'cancelBtn'];
  return(
    <div className="headertext">
    {(buttons.indexOf(props.btnType) > -1) ? 
      <div>
        <h2 className="heading-model-new">{strings.declineModal.declineOrder}</h2>
        <h3 className="text1">{strings.declineModal.declineReasonOrder}</h3>
      </div>
      :
      <div>
        <h2 className="heading-model-new">{strings.declineModal.cancellationReason}</h2>
      </div>
    }
      <Button className="modal-btn" onClick={() => props.handleDeclineModal({}, false)}>X</Button>
    </div>
  );
}

/** LinkModal
 *
 * @description This class having a modal that will display a list of incoming orders and having a filter on that list
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class DeclineModal extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.viewOrder(this.props.currentOrderId);
  }

  componentWillUnmount() {
    this.props.handleDeclineModal({}, false);
  }

  render() {
    strings.setLanguage(this.props.currentLanguage);
    const { handleSubmit, onChange, viewOrderDetail, openDeclineModalFor, btnType } = this.props;
    const {status, rejected, date, reason} = strings.declineModal;
    return(
      <div className="model_popup">
        <div>
          <CsvModalHeader
            handleDeclineModal={this.props.handleDeclineModal}
            openDeclineModalFor={this.props.openDeclineModalFor}
            btnType={btnType}
          />
        </div>
        <div>
        {(openDeclineModalFor === 'incomingOrders' || openDeclineModalFor === 'outgoingOrders') ? 
          <form onSubmit={handleSubmit}>
            <div>
              <Field
                name='message'
                className='form-control'
                component={RenderTextArea}
                placeholder={"Type here..."}
              />
            </div>
            <div>
              <SubmitButtons
                submitLabel={strings.declineModal.declineOrder}
                className='btn btn-primary create'
                submitting={this.props.submitting}
              />
            </div>
          </form>
          :
          <div>
          {(!_.isEmpty(viewOrderDetail)) && 
            <div>
              <p>{status}: {rejected}</p>
              <p>{date}: {(viewOrderDetail.orderDetail.cancellation_date !== undefined && viewOrderDetail.orderDetail.cancellation_date !== null) && viewOrderDetail.orderDetail.cancellation_date.toString().split('T')[0]}</p>
              <p>{reason}: {(viewOrderDetail.orderDetail.cancellation_reason !== undefined && viewOrderDetail.orderDetail.cancellation_reason !== null) && viewOrderDetail.orderDetail.cancellation_reason}</p>
            </div>
          }
          </div>
        }
        </div>
      </div>
    );
  }
}

export default reduxForm({
  form: 'DeclineModal',
  validate,
})(DeclineModal)