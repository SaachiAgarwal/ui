import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);

/** AddOrderTable
 *
 * @description This class is responsible to display added orderd as outgoing orders
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class AddOrderTable extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);
    return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options}>
          <TableHeaderColumn dataField='order_product_id' hidden={true}>Order Id</TableHeaderColumn>
          <TableHeaderColumn dataField='order_id' hidden={true}>Order Id</TableHeaderColumn>
          <TableHeaderColumn dataField='index' isKey={true} autoValue={true}>{strings.addProduct.sNo}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_name' columnTitle={true}>{strings.addProduct.product}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_description' columnTitle={true}>{strings.addProduct.productDescription}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_qty'>{strings.addProduct.quantity}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_uom'>{strings.addProduct.unit}</TableHeaderColumn>
          {(this.props.display) ? false : <TableHeaderColumn dataField='blend_percentage' >{strings.addProduct.blend}</TableHeaderColumn>}
          {(this.props.role === 'Garment Manufacturer') && <TableHeaderColumn dataField='GLM'>{strings.addProduct.grams}</TableHeaderColumn>}
          {(this.props.role === 'Garment Manufacturer') && <TableHeaderColumn dataField='MPG'>{strings.addProduct.meters}</TableHeaderColumn>}
          <TableHeaderColumn key="editAction" dataFormat={this.props.addOrderFormatter}>{strings.OrdersTableText.Action}</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}

AddOrderTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default AddOrderTable;