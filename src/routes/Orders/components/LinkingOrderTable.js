import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Pagination from 'react-paginate';

import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);


/** LinkingOrdersTable
 *
 * @description This class is responsible to display a orders list of incoming orders for link model
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class LinkingOrderTable extends Component {
  constructor(props) {
    super(props);
    this.handlePageClick = this.handlePageClick.bind(this);
  }

  static defaultProps = {
    linkOrderOptions: {
      sizePerPage: 5,
      pageSize: 5,
      pageStartIndex: 1,
      paginationSize: 3,
      prePage: 'Prev',
      nextPage: 'Next',
      firstPage: 'First',
      lastPage: 'Last',
      clearSearch: true,
      hideSizePerPage: true,
      onRowClick: this.editFormatter
    }
  }

  handlePageClick(value) {
    this.props.getLinkOrders(value.selected+1, this.props.linkOrderOptions.pageSize, this.props.fromDt, this.props.toDt);
  }

  render() {
    const { currentLanguage } = this.props;
		strings.setLanguage(currentLanguage);

    return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.linkOrderOptions}>
          <TableHeaderColumn dataField='order_id' hidden={true}>Order Id</TableHeaderColumn>
          <TableHeaderColumn dataField='new_order_date'>{strings.OrdersTableText.orderDate}</TableHeaderColumn>
          <TableHeaderColumn dataField='sender_company_name'>{strings.OrdersTableText.buyer}</TableHeaderColumn>
          <TableHeaderColumn dataField='order_number' isKey={true}>{strings.OrdersTableText.purchaseOrder}</TableHeaderColumn>
          <TableHeaderColumn key="editAction" dataFormat={this.props.linkingFormatter}>{strings.OrdersTableText.Action}</TableHeaderColumn>
        </BootstrapTable>
        <div className="pagination-box">
          <Pagination
            previousLabel={strings.prev}
            nextLabel={strings.next}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={this.props.pages}
            pageRangeDisplayed={5}
            onPageChange={this.handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        </div>
      </div>        
    );
  }
}

LinkingOrderTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  linkOrderOptions: PropTypes.object.isRequired,
};

export default LinkingOrderTable;