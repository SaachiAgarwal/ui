import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
import DropdownList from 'react-widgets/lib/DropdownList';
import 'react-widgets/dist/css/react-widgets.css';
import _ from 'lodash';
import Autocomplete from "react-autocomplete";

import SubmitButtons from 'components/SubmitButtons';
import renderField from './renderField';
import { getSupplierCompanyKeyId, getCategory, getType } from 'components/Helper';
import { getLocalStorage } from 'components/Helper';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);


/** func Validate
 * description validate method will validate all the control fields of form based on provided criteria
 *
 * return Array of Errors
 */
const validate = values => {
  const errors = {}
  if (!values.product_name) {
    errors.product_name = 'Required'
  }
  if (!values.order_number) {
    errors.order_number = 'Required'
  }
  if (!values.product_uom) {
    errors.product_uom = 'Required'
  }
  if (!values.product_qty) {
    errors.product_qty = 'Required'
  }
  if (!values.glm) {
    errors.glm = 'Required'
  }
  if (values.blend_percentage > 100) {
    errors.blend_percentage = 'Invalid percentage'
  }
  if (!values.mpg) {
    errors.mpg = 'Required'
  } else if (values.mpg > 9 || values.mpg < 0) {
    errors.mpg = 'Mpg must be 0 to 9'
  }

  return errors
}

const renderDropdownList = ({ input, data, valueField, textField, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div>
      <DropdownList {...input}
        data={data}
        onChange={input.onChange}
      />
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

/** CreateOrder
 *
 * @description This class is responsible to display a form to create order
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class AddExternalProduct extends React.Component {
  constructor(props) {
    super(props);
    this.showMenu = this.showMenu.bind(this);
    this.returnAutoComplete = this.returnAutoComplete.bind(this);
    this.matchProducts = this.matchProducts.bind(this);
    this.onProdSubmit = this.onProdSubmit.bind(this)
    this.state = {
      initData: {},
      blendpercentage: "100"
    }
  }

  componentWillMount() {
    this.props.setActivePage('addProduct');
    this.props.fetchUOM('orp');
  }

  componentWillReceiveProps(props) {
    if (typeof this.props.inputvalue !== "undefined" || this.props.inputvalue !== "") {
      this.setState({ errorMsg: "" });
    }
  }

  componentDidMount() {
    if (this.props.isProductEdit)
      this.handleInitialize();
  }

  componentWillUnmount() {
    // this.props.setAddProductState(false);// closing product form
    // this.props.setProps(false, 'isProductEdit');// closing edit state
    // this.props.resetProducts();
    // this.props.selectedProduct('', 'unmount');
    this.props.setProductStatus(false)
  }

  async handleInitialize() {
    if (!_.isEmpty(this.props.editFormData)) {
      const initData = {
        order_id: this.props.editFormData.order_id,
        order_product_id: this.props.editFormData.order_product_id,
        product_id: this.props.editFormData.product_id,
        product_name: this.props.editFormData.product_name,
        product_qty: this.props.editFormData.product_qty,
        product_uom: this.props.editFormData.product_uom,
        product_description: this.props.editFormData.product_description,
        glm: this.props.editFormData.GLM,
        mpg: this.props.editFormData.MPG,
        blend_percentage: this.props.editFormData.blend_percentage
      };
      await this.props.setParentState('inputvalue', this.props.editFormData.product_name);
      this.setState({ initData: initData, blendpercentage: this.props.editFormData.blend_percentage });
      this.props.initialize(initData);
    }
  }

  showMenu() {
    if (empty(this.props.products)) {
      return (
        "{'none'}"
      );
    }
  }

  renderBlendPer = ({ input, label, defvalue, type, meta: { touched, error, invalid } }) => (
    // <div className={`form-group ${touched && invalid ? 'has-error' : ''}`}>
    <div>
      <label className="control-label">{label}{(label === 'Glm *' || label === 'Mpg *') && <img className="image-info" src={Images.infoIcon} />}</label>
      <div>
        <input {...input}
          className="form-control"
          type={type}
          value={defvalue}
          autoComplete="off"
          autoCorrect="off"
          spellCheck="off"
          onChange={(e) => { this.setState({ blendpercentage: e.target.value }) }}
        />
        <div className="help-block">
          {touched && (error && <span className="error-danger">
            <i className="fa fa-exclamation-circle">{error}</i></span>)}
        </div>
      </div>
    </div>
  )

  returnAutoComplete() {
    return (
      <div className="customAutoComplete">
        <Autocomplete
          className="form-control"
          ref="autocomplete"
          menuStyle={{ display: this.props.showMenu, minWidth: '100%', }}
          inputProps={{ placeholder: "Start typing product name" }}
          items={this.props.products}
          getItemValue={item => item.product_name}
          shouldItemRender={this.matchProducts}
          renderItem={(item, highlighted) =>
            <div className='col px-2'
              key={item.product_id}
              style={{
                backgroundColor: "white", width: "100%", color: "black", paddingBottom: '5px',
                paddingTop: '5px', maxHeight: '150px', overflowY: 'auto'
              }}
            >
              {item.product_name}
            </div>
          }
          value={this.props.inputvalue}
          onChange={e => this.props.fetchAutoProducts(e.target.value)}
          onSelect={value => this.props.selectedProduct(value)}
        />
      </div>
    );
  }

  matchProducts(state, value) {
    return (
      state.product_name.toLowerCase().indexOf(value.toLowerCase()) !== -1
    );
  }

  async onProdSubmit(values){
    // console.log(values)
    this.props.setProductStatus(false)
  }

  render() {
    const { handleSubmit, currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);
    let loginData = getLocalStorage('loginDetail'),
      dispbutn = (loginData.role === "Spinner" || loginData.role === "Birla Cellulose") ? true : false
    return (
      <div className='col-md-12 col-lg-12 create-order'>
        <h2>{strings.addProduct.productDetail}</h2>
        <div>
          <form onSubmit={handleSubmit(this.onProdSubmit)}>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='product_name'
                  type="text"
                  component={renderField}
                  label={`${strings.addProduct.productName} *`}
                />
              </div>
              {(dispbutn) ? false : (
                <div className="form-group col-md-4">
                  <Field
                    name='blend_percentage'
                    value="100"
                    type="number"
                    component={this.renderBlendPer}
                    label={strings.addProduct.blend}
                    defvalue={this.state.blendpercentage}
                  />
                </div>)}
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='product_qty'
                  type="number"
                  component={renderField}
                  label={`${strings.addProduct.quantity} *`}
                />
              </div>
              <div className="form-group col-md-4">
                <label>{strings.addProduct.unit} *</label>
                <Field
                  name="product_uom"
                  component={renderDropdownList}
                  data={(!_.isEmpty(this.props.uom)) ? this.props.uom.map(function (data) { return data.uom }) : []}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name='product_description'
                  type="text"
                  component={renderField}
                  label={strings.addProduct.productDescription}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name="order_product_id"
                  className="form-control"
                  type="hidden"
                  component={renderField}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name="product_id"
                  className="form-control"
                  type="hidden"
                  component={renderField}
                />
              </div>
            </div>  
            <div>
              <input type='submit' value={`${strings.vendor.createProduct} >`} className="btn btn-primary create" style={{ left: '82%',marginBottom: '2px', width: '16%'}} />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

AddExternalProduct.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
};

export default reduxForm({
  form: 'AddExternalProduct',
  validate,
})(AddExternalProduct)