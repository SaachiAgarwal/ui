import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

//localization
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
let strings = new LocalizedStrings(
  data
);



/** ViewOrderTable
 *
 * @description This class is responsible to display a orders list of incoming orders for link model
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class ViewOrderTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      shouldUpdate: ''
    }
  }

  componentWillMount() {
    this.setState({shouldUpdate: this.props.shouldUpdate});
  }

  render() {
    const {role, reviewOrderCallFor, currentLanguage} = this.props;
    strings.setLanguage(currentLanguage);
    const {sNo, product, category, type, productDescription, quantity, unit,blend, grams, meters} = strings.addProduct;
    return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options}>
          <TableHeaderColumn dataField='order_product_id' hidden={true}>Order Id</TableHeaderColumn>
          <TableHeaderColumn dataField='order_id' hidden={true}>Order Id</TableHeaderColumn>
          <TableHeaderColumn dataField='index' isKey={true} autoValue={true}>{sNo}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_name' columnTitle={true}>{product}</TableHeaderColumn>
          <TableHeaderColumn dataField='category' hidden={true}>{category}</TableHeaderColumn>
          <TableHeaderColumn dataField='type' hidden={true}>{type}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_description' columnTitle={true}>{productDescription}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_qty'>{quantity}</TableHeaderColumn>
          <TableHeaderColumn dataField='product_uom'>{unit}</TableHeaderColumn>
          {(this.props.display) ? false : <TableHeaderColumn dataField='blend_percentage' >{blend}</TableHeaderColumn>}
          {((role === 'Finish Fabric Manufacturer' && reviewOrderCallFor === 'incomingOrders') || (role === 'Garment Manufacturer' && reviewOrderCallFor === 'outgoingOrders') || (this.props.userRole && reviewOrderCallFor === 'outgoingOrders')) ? <TableHeaderColumn dataField='GLM'>{grams}</TableHeaderColumn> :false}
          {((role === 'Finish Fabric Manufacturer' && reviewOrderCallFor === 'incomingOrders') || (role === 'Garment Manufacturer' && reviewOrderCallFor === 'outgoingOrders') || (this.props.userRole && reviewOrderCallFor === 'outgoingOrders')) ? <TableHeaderColumn dataField='MPG'>{meters}</TableHeaderColumn> : false}
          <TableHeaderColumn key="editAction" hidden={true} dataFormat={this.props.reviewFormatter}>{strings.OrdersTableText.Action}</TableHeaderColumn>
        </BootstrapTable>
      </div>        
    );
  }
}

ViewOrderTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default ViewOrderTable;