import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
import SubmitButtons from 'components/SubmitButtons';
import { Images } from 'config/Config';
import { getSupplierCompanyKeyId, getCategory, getType } from 'components/Helper';
import ViewOrderTable from './ViewOrderTable'
import { getLocalStorage } from 'components/Helper';

//localization
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
let strings = new LocalizedStrings(
  data
);



let productCategoryList = [];
let productTypeList = [];

/** func renderLink
 *
 * @description This method returns a field object having a span to achieve onClick functionality. Simply we can not use
 *   onClick on field wrapper.
 *
 * return A component
 */
const renderLink = ({ input, label, type, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className="btn-bs-file">
      <span className='form-control'>
        <p>{customProps.value}</p>
        {(input.name === 'linkTo') &&
          <span>
            <img src={Images.fileUploadIcon} alt="" data-toggle="modal" data-target="#myModal" />
          </span>
        }
      </span>
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

/** CreateOrder
 *
 * @description This class is responsible to display a form to create order
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class ReviewOrder extends React.Component {
  constructor(props) {
    super(props);
    this.deleteProductOrder = this.deleteProductOrder.bind(this);

    this.state = {
      initData: {},
    }
  }

  componentWillMount() {
    if (this.props.reviewOrderCallFor === 'incomingOrders') {
      this.props.setActivePage('inboxReviewOrder');
    } else {
      if (this.props.isPlaced) {
        this.props.setActivePage('placedReviewOrder');
      } else {
        this.props.setActivePage('outboxReviewOrder');
      }
    }
    productCategoryList = this.props.productCategoryList;
    productTypeList = this.props.productTypeList;
  }

  componentDidMount() {
    this.handleInitialize();
  }

  componentWillUnmount() {
    this.props.resetParentState();
    this.props.resetProps();
    this.props.setProps(false, 'showCreateOrderBtn');
  }

  handleInitialize() {
    if (!_.isEmpty(this.props.viewOrderDetail)) {
      let company_name = (!this.props.isViewIncomingOrders) ? getSupplierCompanyKeyId(this.props.supplierCompniesList, this.props.viewOrderDetail.orderDetail, true) : '';
      const initData = {
        order_id: this.props.viewOrderDetail.orderDetail.order_id,
        order_number: this.props.viewOrderDetail.orderDetail.order_number,
        company_name: (!this.props.isViewIncomingOrders) ? company_name : this.props.viewOrderDetail.orderDetail.order_company_name,
        linked_order_number: this.props.viewOrderDetail.orderDetail.linked_order_number,
        order_date: this.props.viewOrderDetail.orderDetail.new_order_date,
        order_expected_date: (this.props.viewOrderDetail.orderDetail.new_order_expected_date !== null) ? this.props.viewOrderDetail.orderDetail.new_order_expected_date : '',
        order_description: this.props.viewOrderDetail.orderDetail.order_description,
        products: this.props.viewOrderDetail.products
      };
      initData.products.map((item, index) => {
        initData.products[index].index = index + 1;
        initData.products[index].order_id = initData.order_id;
        initData.products[index].category = getCategory(item.product_category_id, this.props.productCategoryList);
        initData.products[index].type = getType(item.product_type_id, this.props.productTypeList);
      });
      this.setState({ initData: initData });
      this.props.initialize(initData);
    }
  }

  async deleteProductOrder(row) {
    await this.props.deleteOrder(row.order_product_id, 'review');
    // fetch incoming orders to reset isLinked state accordingly
    await this.props.fetchOrders('incomingOrders');
    // fetch updated viewOrderDetail to render the form with updated data
    await this.props.viewOrder(row.order_id, 'incomingOrders');
    this.handleInitialize();
  }

  render() {
    const { handleSubmit,currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);
    const {orderDetail, purchaseOrder, linkBuyer,orderDate,orderDue, description, productDetail} = strings.createOrder;
    let loginData = getLocalStorage('loginDetail'),
      dispbutn = ((loginData.role === "Spinner" && this.props.reviewOrderCallFor === "outgoingOrders") || loginData.role === "Birla Cellulose") ? true : false
    return (
      <div className='col-md-12 col-lg-12 create-order'>
        <h2>{orderDetail}</h2>
        <div>
          <form onSubmit={handleSubmit}>
            <div className="form-row">
              <div className="form-group col-md-4">
                <label>{purchaseOrder}</label>
                <Field
                  name='order_number'
                  type="text"
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.order_number : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <label>{(this.props.isViewIncomingOrders) ? `${strings.OrdersTableText.Buyer}` : `${strings.OrdersTableText.Supplier}`}</label>
                <Field
                  className="select-menu"
                  name="company_name"
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.company_name : ''
                  }}
                />
              </div>
              {(this.props.role !== 'Brand') &&
                <div className="form-group col-md-4 ">
                  <label>{linkBuyer}</label>
                  <Field
                    name='linkTo'
                    type='text'
                    component={renderLink}
                    customProps={{
                      value: (!_.isEmpty(this.state.initData)) ? this.state.initData.linked_order_number : ''
                    }}
                  />
                </div>
              }
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <label>{orderDate}</label>
                <Field
                  name='order_date'
                  type='date'
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.order_date : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <label>{orderDue}</label>
                <Field
                  name='order_expected_date'
                  type='date'
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.order_expected_date : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <label>{description}</label>
                <Field
                  name='order_description'
                  type="text"
                  component={renderLink}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.order_description : ''
                  }}
                />
              </div>
            </div>
            {(!_.isEmpty(this.state.initData.products)) &&
              <div className="col-md-12 nopad">
                <h2>{productDetail}</h2>
                <ViewOrderTable
                  userRole={this.props.userRole}
                  data={this.state.initData.products}
                  pagination={false}
                  search={false}
                  exportCSV={false}
                  options={this.props.options}
                  isForModal={false}
                  shouldUpdate={this.props.shouldUpdate}
                  incomingOrders={this.props.incomingOrders}
                  viewOrderDetail={this.props.viewOrderDetail}
                  role={this.props.role}
                  reviewOrderCallFor={this.props.reviewOrderCallFor}
                  display={dispbutn}
                  currentLanguage = {this.props.currentLanguage}
                />
              </div>
            }
          </form>
        </div>
      </div>
    );
  }
}

ReviewOrder.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
};

export default reduxForm({
  form: 'ReviewOrder'
})(ReviewOrder)