import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import DropdownList from 'react-widgets/lib/DropdownList';
import 'react-widgets/dist/css/react-widgets.css';
import '../../../styles/form.scss';

import renderField from './renderField';
import RenderDatePicker from 'components/RenderDatePicker';
import {getEntityId} from 'components/Helper';
//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);


const renderDropdownList = ({ input, data, valueField, textField, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div>
      <DropdownList {...input}
        data={data}
        onChange={input.onChange}
        onSelect={customProps.onSelectHandler(input.value)}
        value={customProps.value}
      />
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  ); 
}

class OrderFilter extends React.Component {
  constructor(props) {
    super(props);
    this.onSelectHandler = this.onSelectHandler.bind(this);
    this.fetchOrders = this.fetchOrders.bind(this);
  }

  fetchOrders(name) {
    this.props.setBrandEntityName(name);
    let defaultEntityId = getEntityId(this.props.brandEntities, name);
    this.props.fetchBrandsOrders(defaultEntityId, this.props.fromDt, this.props.toDt, this.props.searchText, this.props.options.currentPage, this.props.options.pageSize);
  }

  onSelectHandler(value) {
    if (!_.isEmpty(value)) {
      this.fetchOrders(value);
    }
  }

  render() {
    const { handleSubmit, role, currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);

    return (
      <div>
        <form onSubmit={handleSubmit}>
          <div className='col-12 col-sm-12 col-xl-12 mb-2'>
            <Row>
             <Col className="text-center col-4 col-sm-4 col-xl-6 pr-1">
                <Field
                  name='product'
                  type="text"
                  component={renderField}
                  label={strings.OrderFilterText.Search}
                  value={this.props.searchText}
                  onChange={(e) => this.props.handleFilterSearch(e.target.value)} 
                  />
              </Col>
              <Col className={(role === 'Birla Cellulose') ? "text-center col-4 col-sm-4 col-xl-2 pr-1" : "text-center col-4 col-sm-4 col-xl-3 pr-1"}>
                <label>{strings.OrderFilterText.From}</label>
                <Field
                  name='from_date'
                  type='date'
                  component={RenderDatePicker}
                  label='Select'
                  onChange={(date) => this.props.handleFilterDateChange(date, 'fromDate')}
                />
              </Col>
              <Col className={(role === 'Birla Cellulose') ? "text-center col-4 col-sm-4 col-xl-2 pr-1" : "text-center col-4 col-sm-4 col-xl-3 pr-1"}>
                <label>{strings.OrderFilterText.To}</label>
                <Field
                  name='to_date'
                  type='date'
                  component={RenderDatePicker}
                  label='Select'
                  onChange={(date) => this.props.handleFilterDateChange(date, 'toDate')}
                />
              </Col>
            </Row>
          </div>
        </form>
      </div>
    );
  }
}

OrderFilter.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
};

export default reduxForm({
  form: 'OrderFilter',
})(OrderFilter)