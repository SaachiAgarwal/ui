/** SettingsContainer
 *
 * @description This is container that manages the states for all settings related stuff.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */

import { connect } from 'react-redux';

import Configuration from '../components/Configuration';
import {updateConfiguration, resetAlertBox, setConfigDetail} from '../modules/configuration';
import {setInnerNav,setParam,setSearchString,setReportFetching,setGlobalState, setCurrentLanguage} from '../../../store/app';

const mapStateToProps = (state) => {
  return({
    navIndex: state.app.navIndex,
    navInnerIndex: state.app.navInnerIndex,
    navClass: state.app.navClass,
    fetching: state.Configuration.fetching,
    error: state.Configuration.error,
    showAlert: state.Configuration.showAlert,
    alertMessage: state.Configuration.alertMessage,
    status: state.Configuration.status,
    configData: state.Configuration.configData,
    currentLanguage: state.app.currentLanguage
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    setGlobalState: (value) => dispatch(setGlobalState(value)),
    resetAlertBox: (showAlert, message) => dispatch(resetAlertBox(showAlert, message)),
    updateConfiguration: (value) => dispatch(updateConfiguration(value)),
    setConfigDetail: () => dispatch(setConfigDetail()),
    setCurrentLanguage: (lang) => dispatch(setCurrentLanguage(lang)),
    setInnerNav: (lang) => dispatch(setInnerNav(lang)),
    setParam: (lang) => dispatch(setParam(lang)),
    setSearchString: (lang) => dispatch(setSearchString(lang)),
    setReportFetching: (lang) => dispatch(setReportFetching(lang)),
  });
};

export default connect(mapStateToProps, mapDispatchToProps)(Configuration);
