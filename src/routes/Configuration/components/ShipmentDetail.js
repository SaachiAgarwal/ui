import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Row, Col, Label } from 'reactstrap';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import SubmitButtons from 'components/SubmitButtons';
import { getUsreInfo } from 'components/Helper';
import 'css/style.scss';

//localization 
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';

let strings = new LocalizedStrings(
  data
);

class ShipmentDetail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      seller_val: true,
      buyer_val: true,
      invoice_val: true,
      mot_val: true,
      ship_val: true,
      receive_val: true,
      product_val: true,
      quantity_val: true,
      unit_val: true,
      description_val: true
    }
    this.onChangeSeller = this.onChangeSeller.bind(this);
    this.onChangeBuyer = this.onChangeBuyer.bind(this);
    this.onChangeInvoice = this.onChangeInvoice.bind(this);
    this.onChangeMot = this.onChangeMot.bind(this);
    this.onChangeShipment = this.onChangeShipment.bind(this);
    this.onChangeReceive = this.onChangeReceive.bind(this);
    this.onChangeProduct = this.onChangeProduct.bind(this);
    this.onChangeQuantity = this.onChangeQuantity.bind(this);
    this.onChangeUnit = this.onChangeUnit.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.getData = this.getData.bind(this);
    this.selectAll = this.selectAll.bind(this)
  }

  componentDidMount() {
    this.getData();
  }
  async getData() {
    await this.props.setConfigDetail();
    if (this.props.configData !== "") {
      this.setState({
        seller_val: this.props.configData.data.sd_seller_col,
        buyer_val: this.props.configData.data.sd_buyer_col,
        invoice_val: this.props.configData.data.sd_invoice_number_col,
        mot_val: this.props.configData.data.sd_MoT_col,
        ship_val: this.props.configData.data.sd_shipment_date_col,
        product_val: this.props.configData.data.pd_product_col,
        quantity_val: this.props.configData.data.pd_quantity_col,
        unit_val: this.props.configData.data.pd_unit_col,
        receive_val: this.props.configData.data.sd_received_date_col,
        description_val: this.props.configData.data.pd_description_col
      })
    }




    //     pd_description_col: 0
    // pd_product_col: 0
    // pd_quantity_col: 0
    // pd_unit_col: 1
    // sd_MoT_col: 0
    // sd_buyer_col: 1
    // sd_invoice_number_col: 1
    // sd_received_date_col: 0
    // sd_seller_col: 1
    // sd_shipment_date_col: 0
  }
  onChangeSeller(e) {
    this.setState({
      seller_val: !this.state.seller_val
    })
  }
  onChangeBuyer(e) {
    this.setState({
      buyer_val: !this.state.buyer_val
    })
  }
  onChangeInvoice(e) {
    this.setState({
      invoice_val: !this.state.invoice_val
    })
  }
  onChangeMot(e) {
    this.setState({
      mot_val: !this.state.mot_val
    })
  }
  onChangeShipment(e) {
    this.setState({
      ship_val: !this.state.ship_val
    })
  }
  onChangeReceive(e) {
    this.setState({
      receive_val: !this.state.receive_val
    })
  }
  onChangeProduct(e) {
    this.setState({
      product_val: !this.state.product_val
    })
  }
  onChangeQuantity(e) {
    this.setState({
      quantity_val: !this.state.quantity_val
    })
  }
  onChangeUnit(e) {
    this.setState({
      unit_val: !this.state.unit_val
    })
  }
  onChangeDescription(e) {
    this.setState({
      description_val: !this.state.description_val
    })
  }
  async onSubmit(e) {
    e.preventDefault();
    let data = {
      sd_sellercol: this.state.seller_val,
      sd_buyercol: this.state.buyer_val,
      sd_invoice_numbercol: this.state.invoice_val,
      sd_MoTcol: this.state.mot_val,
      sd_shipment_datecol: this.state.ship_val,
      sd_received_datecol: this.state.receive_val,
      pd_productcol: this.state.product_val,
      pd_quantitycol: this.state.quantity_val,
      pd_unitcol: this.state.unit_val,
      pd_descriptioncol: this.state.description_val

    }
    await this.props.updateConfiguration(data);

  }
  async selectAll() {
    this.setState({
      seller_val: true,
      buyer_val: true,
      invoice_val: true,
      mot_val: true,
      ship_val: true,
      receive_val: true,
      product_val: true,
      quantity_val: true,
      unit_val: true,
      description_val: true
    })
  }

  render() {
    const { currentLanguage } = this.props;
    strings.setLanguage(currentLanguage);
    const { seller, buyer, invoiceNumber, mot, shipmentDate, receivedDate } = strings.shipmentTableText;
    const { description } = strings.createOrder;
    const { product, quantity, unit } = strings.addProduct;
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <div className="form-group setting-detail" style={{ width: "76%" }}>
            <div style={{ paddingTop: "37px" }}>
              <div className="confi-check-seller">
                <label>{seller}</label>
                <input type="checkbox"
                  className="conf-checkbox"
                  id="sellerCheckbox"
                  name="sellerCheckbox"
                  checked={this.state.seller_val}
                  onChange={this.onChangeSeller}
                />
              </div>
              <div className="confi-check">
                <label>{buyer}</label>
                <input type="checkbox"
                  className="conf-checkbox"
                  id="buyerCheckbox"
                  name="buyerCheckbox"
                  checked={this.state.buyer_val}
                  onChange={this.onChangeBuyer}
                />
              </div>
              <div className="confi-check">
                <label>{invoiceNumber}</label>
                <input type="checkbox"
                  className="conf-checkbox"
                  id="invoiceCheckbox"
                  name="invoiceCheckbox"
                  checked={this.state.invoice_val}
                  onChange={this.onChangeInvoice}
                />
              </div>
              <div className="confi-check">
                <label>{mot}</label>
                <input type="checkbox"
                  className="conf-checkbox"
                  id="motCheckbox"
                  name="motCheckbox"
                  checked={this.state.mot_val}
                  onChange={this.onChangeMot}
                />
              </div>
              <div className="confi-check">
                <label>{shipmentDate}</label>
                <input type="checkbox"
                  className="conf-checkbox"
                  id="shipmentDateCheckbox"
                  name="shipmentDateCheckbox"
                  checked={this.state.ship_val}
                  onChange={this.onChangeShipment}
                />
              </div>
              <div className="confi-check">
                <label>{receivedDate}</label>
                <input type="checkbox"
                  className="conf-checkbox"
                  id="receivedDateCheckbox"
                  name="receivedDateCheckbox"
                  checked={this.state.receive_val}
                  onChange={this.onChangeReceive}
                />
              </div>
            </div>
          </div>


          <h2 style={{ paddingLeft: "2%" }}>{strings.addProduct.productDetail}</h2>

          <div className="form-group setting-detail" style={{ width: "76%" }}>
            <div style={{ paddingTop: "37px" }}>
              <div className="confi-check-seller">
                <label>{product}</label>
                <input type="checkbox"
                  className="conf-checkbox"
                  id="productCheckbox"
                  name="productCheckbox"
                  checked={this.state.product_val}
                  onChange={this.onChangeProduct}
                />
              </div>
              <div className="confi-check">
                <label>{quantity}</label>
                <input type="checkbox"
                  className="conf-checkbox"
                  id="quantityCheckbox"
                  name="quantityCheckbox"
                  checked={this.state.quantity_val}
                  onChange={this.onChangeQuantity}
                />
              </div>
              <div className="confi-check">
                <label>{unit}</label>
                <input type="checkbox"
                  className="conf-checkbox"
                  id="unitCheckbox"
                  name="unitCheckbox"
                  checked={this.state.unit_val}
                  onChange={this.onChangeUnit}
                />
              </div>
              <div className="confi-check">
                <label>{description}</label>
                <input type="checkbox"
                  className="conf-checkbox"
                  id="descriptionCheckbox"
                  name="descriptionCheckbox"
                  checked={this.state.description_val}
                  onChange={this.onChangeDescription}
                />
              </div>
            </div>
          </div>
          <br />
          <br />
          <div className="form-group" style={{ paddingLeft: "2%" }}>
            <input type='submit' value={`${strings.configure} >`} className="btn btn-primary" />
          </div>
        </form>
        <div style= {{position: 'absolute',left: '15%',bottom: '139px'}}>
          <button className="btn btn-primary" onClick={this.selectAll}>{strings.selectAll}</button>
        </div>
      </div>
    );
  }
}



export default ShipmentDetail;