/** CompanyContainer
 *
 * @description This is container that manages the states for all Company related stuff.
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */

import { connect } from 'react-redux';

import Company from '../components/Company';
import { resetAlertBox, getCompanyDetails, setProps, getCompanyType, fetchBrandEntities, addCompany, viewCompanyDetail, deleteCompany, fetchCompanyRole, linkParentCompany, setCompanyFormState,getParentCompanyDetails,setAlertMeassage ,resetProps, setCurrentNewPage, setLatitude, setLongitude, externalCompanyCheck, setEditState, updateExternalCompanyNormal, getPulpCompany, setIsPulp, getForestCompany, setIsForest, uploadPulpDoc, getDocData, downloadDoc, resetDocData, deleteDoc} from '../modules/company';
import {setInnerNav,setParam,setSearchString,setReportFetching, setGlobalState, setCurrentLanguage } from '../../../store/app';

const mapStateToProps = (state) => {
  return ({
    navIndex: state.app.navIndex,
    navInnerIndex: state.app.navInnerIndex,
    navClass: state.app.navClass,
    showAlert: state.Company.showAlert,
    status: state.Company.status,
    alertMessage: state.Company.alertMessage,
    fetching: state.Company.fetching,
    error: state.Company.error,
    totalRecordCount: state.Company.totalRecordCount,
    pages: state.Company.pages,
    companyDetail: state.Company.companyDetail,
    isCompanyFormOpen: state.Company.isCompanyFormOpen,
    isReviewCompany: state.Company.isReviewCompany,
    isEdit: state.Company.isEdit,
    isLinked: state.Company.isLinked,
    companyType: state.Company.companyType,
    viewCompanyInfo: state.Company.viewCompanyInfo,
    companyRole: state.Company.companyRole,
    showRoleCheckbox: state.Company.showRoleCheckbox,
    brandEntities: state.Company.brandEntities,
    linkedPCompanyDetail: state.Company.linkedPCompanyDetail,
    getPCompanyData: state.Company.getPCompanyData,
    currentNewPage: state.Company.currentNewPage,
    latitude: state.Company.latitude,
    longitude: state.Company.longitude,
    currentLanguage: state.app.currentLanguage,
    externalCompany: state.Company.externalCompany,
    pulpDrop: state.Company.pulpDrop,
    isPulp: state.Company.isPulp,
    isForest: state.Company.isForest,
    forestDrop: state.Company.forestDrop,
    docData: state.Company.docData
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    setGlobalState: (value) => dispatch(setGlobalState(value)),
    resetAlertBox: (showAlert, message) => dispatch(resetAlertBox(showAlert, message)),
    setProps: (status, key) => dispatch(setProps(status, key)),
    fetchBrandEntities: () => dispatch(fetchBrandEntities()),
    getCompanyDetails: (query, currentPage, pageSize) => dispatch(getCompanyDetails(query, currentPage, pageSize)),
    linkParentCompany: (data, isLinked) => dispatch(linkParentCompany(data, isLinked)),
    getCompanyType: () => dispatch(getCompanyType()),
    getParentCompanyDetails: (query, currentPage, pageSize, id) => dispatch(getParentCompanyDetails(query, currentPage, pageSize, id)),
    addCompany: (values, isEdit) => dispatch(addCompany(values, isEdit)),
    viewCompanyDetail: (companyId) => dispatch(viewCompanyDetail(companyId)),
    deleteCompany: (companyId) => dispatch(deleteCompany(companyId)),
    fetchCompanyRole: (company) => dispatch(fetchCompanyRole(company)),
    setCompanyFormState: (status) => dispatch(setCompanyFormState(status)),
    setAlertMeassage: (status, message, val) => dispatch(setAlertMeassage(status, message, val)),
    resetProps: () => dispatch(resetProps()),
    setCurrentNewPage: (page) => dispatch(setCurrentNewPage(page)),
    setLatitude: (value) => dispatch(setLatitude(value)),
    setLongitude: (value) => dispatch(setLongitude(value)),
    setCurrentLanguage: (lang) => dispatch(setCurrentLanguage(lang)),
    setInnerNav: (lang) => dispatch(setInnerNav(lang)),
    setParam: (lang) => dispatch(setParam(lang)),
    setSearchString: (lang) => dispatch(setSearchString(lang)),
    setReportFetching: (lang) => dispatch(setReportFetching(lang)),
    externalCompanyCheck: (val) => dispatch(externalCompanyCheck(val)),
    setEditState: (val) => dispatch(setEditState(val)),
    updateExternalCompanyNormal: (val) => dispatch(updateExternalCompanyNormal(val)),
    getPulpCompany: () => dispatch(getPulpCompany()),
    setIsPulp: (val) => dispatch(setIsPulp(val)),
    getForestCompany: () => dispatch(getForestCompany()),
    setIsForest: () => dispatch(setIsForest()),
    uploadPulpDoc: (file, company_key_id, appendFile) => dispatch(uploadPulpDoc(file, company_key_id, appendFile)),
    getDocData: (companay_key_id) => dispatch(getDocData(companay_key_id)),
    downloadDoc: (document_file_name, document_name) => dispatch(downloadDoc(document_file_name, document_name)),
    resetDocData: () => dispatch(resetDocData()),
    deleteDoc: (linked_forest_document_id, document_file_name) => dispatch(deleteDoc(linked_forest_document_id, document_file_name))
  });
};

export default connect(mapStateToProps, mapDispatchToProps)(Company);
