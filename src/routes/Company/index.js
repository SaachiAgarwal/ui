import { injectReducer } from '../../store/reducers';

export default (store) => ({
  path: 'company',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const Company = require('./containers/CompanyContainer').default;
      const reducer = require('./modules/company').default;
      injectReducer(store, { key: 'Company', reducer });
      cb(null, Company);
  }, 'Company');
  },
});
