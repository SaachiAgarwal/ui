import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Label, Input, FormGroup, Button } from 'reactstrap';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';

import LinkingOrderTable from './LinkingPCompanyTable';
import RenderDatePicker from 'components/RenderDatePicker';
import { getLocalStorage } from 'components/Helper';

export const LinkModalHeader = (props) => {
  return (
    <Button className="modal-btn" onClick={() => props.handleLinkModal('close')}>X</Button>
  );
}

/** LinkModal
 *
 * @description This class having a modal that will display a list of incoming orders and having a filter on that list
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class LinkModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filterText: '',
      initData: {}
    }
  }

  async componentWillMount() {
    await this.props.getParentCompanyDetails(this.state.filterText, this.props.options.currentPage, this.props.options.modalPageSize, getLocalStorage('companyDetail').company_key_id);
  }

  async lotFilter(query) {
    await this.setState({ filterText: query });
    if (this.props.totalRecordCount !== 0) await this.props.getParentCompanyDetails(this.state.filterText, this.props.options.currentPage, this.props.options.modalPageSize, getLocalStorage('companyDetail').company_key_id);
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="modal">
        <Row>
          <LinkModalHeader
            handleLinkModal={this.props.handleLinkModal}
          />
        </Row>
        <form onSubmit={handleSubmit}>
          <div className='col-12 col-sm-12 col-xl-12 mb-2'>
            <Row>
              <Col className="text-center col-12 col-sm-12 col-xl-12 pr-1">
                <input
                  type="text"
                  className="lot-filter-search"
                  placeholder="Search"
                  value={this.state.filterText}
                  onChange={(e) => this.lotFilter(e.target.value)}
                />
              </Col>
            </Row>
          </div>
        </form>
        <LinkingOrderTable
          data={(!_.isEmpty(this.props.getPCompanyData)) ? this.props.getPCompanyData : {}}
          totalRecordCount={this.props.totalRecordCount}
          pagination={false}
          search={false}
          exportCSV={false}
          isForModal={false}
          options={this.props.options}
          companyTypeId={this.props.companyTypeId}
          filterText={this.state.filterText}
          linkingFormatter={this.props.linkingFormatter}
          getCompanyDetails={this.props.getCompanyDetails}
          buyerCompanyKeyId={this.state.buyerCompanyKeyId}
          pages={this.props.pages}
          resetAlertBox={this.props.resetAlertBox}
          getParentCompanyDetails={this.props.getParentCompanyDetails}
        />
      </div>
    );
  }
}

export default reduxForm({
  form: 'LinkModal',
})(LinkModal)