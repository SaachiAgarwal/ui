import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Button } from 'reactstrap';
import _ from 'lodash';
import DropdownList from 'react-widgets/lib/DropdownList';
import 'react-widgets/dist/css/react-widgets.css';
import UploadDocTable from './UploadDocTable'
import renderField from './renderField';
import SubmitButtons from 'components/SubmitButtons';
import { getCompanyTypeId, getCompanyId, getLocalStorage } from 'components/Helper';
import CheckboxGroup from 'components/CheckboxGroup';
import { Images } from 'config/Config';

import Modal from 'react-modal';
import LotModal from './parentCompanyModal';
import options from 'components/options';
import Alert from "components/Alert";
import opencage from 'opencage-api-client'
// import Geocode from "react-geocode";
/** func Validate
* description validate method will validate all the control fields of form based on provided criteria
*
* return Array of Errors
*/
// Custom styles for link modal
const customStyles = {
  content: {
    top: '0',
    left: '0',
    right: '0',
    bottom: '0',
    marginRight: '0',
    transform: 'translate(0, 0)'
  }
};
const validate = values => {
  const errors = {}
  if (!values.company_name) {
    errors.company_name = 'Required'
  }
  if (!values.company_type_name) {
    errors.company_type_name = 'Required'
  }
  if (!values.country) {
    errors.country = 'Required'
  }
  if (!values.state) {
    errors.state = 'Required'
  }
  if (!values.city) {
    errors.city = 'Required'
  }
  if (!values.zip) {
    errors.zip = 'Required'
  }
  if(!values.pulp_company){
    errors.pulp_company = 'Required'
  }
  if(!values.forest_company){
    errors.forest_company = 'Required'
  }
  if(!values.conversion_factor){
    errors.conversion_factor = 'Required'
  }
  return errors
}

const renderDropdownList = ({ input, data, valueField, textField, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div>
      <DropdownList {...input}
        data={data}
        onChange={input.onChange}
        onSelect={customProps.handleDropdown(input.value)}
      />
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}
const renderDropdownPulp = ({ input, data, valueField, textField, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div>
      <DropdownList {...input}
        data={data}
        onChange={input.onChange}
        onSelect={customProps.handlePulpDrop(input.value)}
      />
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

const renderDropdownForest = ({ input, data, valueField, textField, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div>
      <DropdownList {...input}
        data={data}
        onChange={input.onChange}
        onSelect={customProps.handleForestDrop(input.value)}
      />
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}
/** func renderLink
*
* @description This method returns a field object having a span to achieve onClick functionality. Simply we can not use
* onClick on field wrapper.
*
* return A component
*/
const renderLink = ({ input, label, type, customProps, meta: { touched, error, invalid, submitFailed } }) => {
  return (
    <div className="btn-bs-file">
      <span className='form-control'>
        <p>{customProps.parent_company_name}</p>
        <span onClick={() => customProps.handleLinkModal('open')}>
          <img src={Images.fileUploadIcon} alt="" data-toggle="modal" data-target="#myModal" />
        </span>
      </span>
      {submitFailed && error && <span className="error-danger">{error}</span>}
    </div>
  );
}

/** AddCompany
*
* @description This class is a based class for Company form functionality
*
* @author Ketan Kumar<ketan@incaendo.com>
* @link http://www.incaendo.com
* @copyright (c) 2018, Grant Thornton India LLP.
*/
class AddCompany extends React.Component {
  constructor(props) {
    super(props);
    this.handleDropdown = this.handleDropdown.bind(this);
    this.handleLinkModal = this.handleLinkModal.bind(this);
    this.linkingFormatter = this.linkingFormatter.bind(this);
    this.linkParentCompany = this.linkParentCompany.bind(this);
    this.onCityChange = this.onCityChange.bind(this);
    this.onStateChange = this.onStateChange.bind(this);
    this.onCountryChange = this.onCountryChange.bind(this);
    this.handlePulpDrop = this.handlePulpDrop.bind(this)
    this.handleForestDrop = this.handleForestDrop.bind(this)
    this.onDocUpload = this.onDocUpload.bind(this)
    this.onFileSelected = this.onFileSelected.bind(this)
    this.uploadFormatter = this.uploadFormatter.bind(this)
    this.onDownloadDoc = this.onDownloadDoc.bind(this)
    this.onDeleteDoc = this.onDeleteDoc.bind(this)
    this.state = {
      initData: {},
      getcompany: {},
      openModal: false,
      selectedCompany: {},
      companyTypeId: '',
      isLinkedCompany: 0,
      country: '',
      state: '',
      city: '',
      selectedFile: '',
      appendFile: ''
    }
    // Geocode.setApiKey("AIzaSyDa8IbCs7c-NM1Yw0eoXAqvxFJ27aQ6s58");
  }

  uploadFormatter(row, cell) {
    return (
      <ButtonGroup >
        <Button className="action" title="view" style={{ paddingRight: '0px', paddingLeft: '1px' }} onClick={() => this.onDownloadDoc(cell)}>Download</Button>
        <Button className="action" title="approve" style={{ paddingRight: '0px' }} onClick={() => this.onDeleteDoc(cell)}>Delete</Button>
      </ButtonGroup>
    );
  }

  async onDownloadDoc(cell){
    console.log(cell)
    this.props.downloadDoc(cell.document_file_name, cell.document_name)
  }

  async onDeleteDoc(cell){
    await this.props.deleteDoc(cell.linked_forest_document_id, cell.document_file_name)
    await this.props.getDocData(getLocalStorage('companyDetail').company_key_id);
    console.log(cell)
  }
  async componentWillMount() {
    await this.props.setActivePage('addCompany');
    await this.props.resetDocData();
    await this.props.fetchBrandEntities();
    if (!this.props.isEdit) this.props.getCompanyType();
  }

  componentDidMount() {
    if(this.props.viewCompanyInfo.company_key_id !== undefined && this.props.viewCompanyInfo.company_key_id !== '' && this.props.viewCompanyInfo.length !==0){
       this.props.getDocData(this.props.viewCompanyInfo.company_key_id);
    }
    
    if (this.props.isEdit) this.handleInitialize();
  }

  handleInitialize() {
    if (!_.isEmpty(this.props.viewCompanyInfo)) {
      let roles = [];
      if (this.props.viewCompanyInfo.roles !== undefined) {// roles (key) must not be undefined
        this.props.viewCompanyInfo.roles.map((item) => {
          roles.push(item.company_role_id); // preparing roles for integrated player to open form in edit state
        });
      }
      // let pulp_company = this.props.viewCompanyInfo.forest_company_name || ''
      let forest_company = this.props.viewCompanyInfo.forest_product_name || ''
      let conversion_factor = this.props.viewCompanyInfo.conversion_factor || ''
      const initData = {
        company_name: this.props.viewCompanyInfo.company_name,
        company_type_name: this.props.viewCompanyInfo.company_type_name,
        company_type_id: this.props.viewCompanyInfo.company_type_id,
        country: this.props.viewCompanyInfo.country,
        state: this.props.viewCompanyInfo.state,
        city: this.props.viewCompanyInfo.city,
        zip: this.props.viewCompanyInfo.zip,
        company_address: this.props.viewCompanyInfo.company_address,
        phone_number: this.props.viewCompanyInfo.phone_number,
        email_address: this.props.viewCompanyInfo.email_address,
        company_role: roles,
        parent_company_id: this.props.viewCompanyInfo.parent_company_id,
        // pulp_company: pulp_company,
        forest_company: forest_company,
        conversion_factor: conversion_factor
      };

      this.setState({ initData: initData });
      this.props.initialize(initData);
    }
  }

  componentWillUnmount() {
    this.props.setActivePage('company');
    this.props.setProps(false, 'isCompanyFormOpen');
    this.props.setProps(false, 'isEdit');
    this.props.setProps(false, 'showRoleCheckbox');
    this.props.setProps(false, 'isPulp');
    this.props.setProps(false, 'isForest');
    this.props.resetProps()
  }

   handleDropdown(company) {
    if (company === 'Integrated Player') {
      this.props.fetchCompanyRole(company);
      this.props.setProps(true, 'isLinked');
    }
    // else if(company === 'Pulp'){
    //   console.log("In pulp")
    //   this.props.getPulpCompany();
    //   this.props.setProps(false, 'isLinked');
    //   this.props.setProps(true, 'isPulp');
    //   this.props.setProps(false, 'isForest');
    //   // console.log(this.props.pulpDrop)
    //   // console.log(this.props.isPulp)
    // }
      else if(company === 'Pulp'){
      console.log("In pulp")
      this.props.setProps(false, 'isForest');
      this.props.setProps(false, 'isLinked');
      this.props.setProps(true, 'isPulp');
      
      // console.log(this.props.pulpDrop)
      // console.log(this.props.isPulp)
    }
    else if(company === 'Forest'){
      console.log("In Forest")
      this.props.getForestCompany()
      this.props.setProps(false, 'isLinked');
      this.props.setProps(true, 'isForest');
      this.props.setProps(false, 'isPulp');
      // console.log(this.props.isForest);
      // console.log(this.props.forestDrop)

    }
    
    else {
      let companykeyid = this.props.companyType.filter((key, index) => (company === key.company_type_name) ? key.company_type_id : false)
      if (companykeyid.length !== 0) this.setState({ companyTypeId: companykeyid[0].company_type_id })
      this.props.fetchCompanyRole(company);
      this.props.setProps(false, 'isLinked');
      this.props.setProps(false, 'isPulp');
      this.props.setProps(false, 'isForest');
      
    }

  }
  onCityChange(e){
    this.setState({
      city: e.target.value
    })
    // Geocode.fromAddress(`${this.state.country + ''+ this.state.state +''+ this.state.city}`).then(
    //   response => {
    //     const { lat, lng } = response.results[0].geometry.location;
    //     // console.log(lat);
    //     // console.log(lng);
    //     this.props.setLatitude(lat);
    //     this.props.setLongitude(lng);
    //   },
    //   error => {
    //     console.error(error);
    //   }
    // );
    //console.log(this.state.city)


    // opencage
    // .geocode({ key: '4fb85e6f207343a1b222fc6ea5dbed54', q: `${this.state.country + ',' + this.state.state + ',' + this.state.city}` })
    // .then(response => {
    //   console.log(response);
      
    //   this.setState({ response, isSubmitting: false });
    // })
    // .catch(err => {
    //   console.error(err);
    //   this.setState({ response: {}, isSubmitting: false });
    // });




  }
  onStateChange(e){
    this.setState({
      state: e.target.value
    })
    //console.log(this.state.city)
  }
  onCountryChange(e){
    this.setState({
      country: e.target.value
    })
    //console.log(this.state.city)
  }

  handleLinkModal(key) {
    (key === 'open') ? this.setState({ openModal: true }) : this.setState({ openModal: false });
  }

  async linkParentCompany(row, order_number) {
    if (!_.isEmpty(this.props.viewCompanyInfo) && this.props.viewCompanyInfo.parent_company_id !== null && this.props.viewCompanyInfo.parent_company_id !== row.company_key_id) {
      this.props.resetAlertBox(true, 'First unlink the linked order');
      this.setState({ openModal: true, isLinkedCompany: 1 });
    } else {
      let val = this.props.viewCompanyInfo
      this.setState({ showProductAfterLink: true });
      let lStorage = getLocalStorage('companyDetail'),
        companyData = {};


      // performing link and unlink shipment
      if (this.props.viewCompanyInfo.parent_company_id === row.company_key_id) {
        companyData = {
          company_name: lStorage.company_name,
          company_type_id: lStorage.company_type_id,
          company_key_id: lStorage.company_key_id,
          parent_company_id: 0,
        }
        await this.props.linkParentCompany(companyData, true);
        this.setState({ openModal: true, isLinkedCompany: 0 });
      } else {
        companyData = {
          company_name: lStorage.company_name,
          company_type_id: lStorage.company_type_id,
          company_key_id: lStorage.company_key_id,
          parent_company_id: row.company_key_id,
        }
        await this.props.linkParentCompany(companyData, true);
        this.setState({ openModal: false });
      }
      await this.props.viewCompanyDetail(lStorage.company_key_id);
    }
  }

  async handleAlertBox() {
    this.props.resetAlertBox(false, "");
  }

  linkingFormatter(row, cell) {
    return (
      <ButtonGroup>
        <Button className="action" onClick={() => this.linkParentCompany(cell, this.props.viewCompanyInfo.parent_company_id)}>{(!_.isEmpty(this.props.viewCompanyInfo)) ? ((cell.company_key_id === this.props.viewCompanyInfo.parent_company_id) ? 'unlink' : 'link') : 'link'}</Button>
      </ButtonGroup>
    );
  }

   handlePulpDrop(company){
    
    // this.props.pulpDrop.map((val, index) => {
    //   if(val.company_name === company){
    //     console.log(val.company_key_id);
    //   }
    // })
    console.log(company)
  }

  handleForestDrop(company){
    console.log(company)
  }

  async onDocUpload(){
    this.refs.fileUploader.click();

  }

  async onFileSelected(event) {
    let company_key_id = getLocalStorage('companyDetail').company_key_id
    console.log(company_key_id);
    await this.setState({ selectedFile: event.target.files[0] });

    var today = new Date();
    var date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    let appendFile = this.state.selectedFile.name + '_' + company_key_id + '_' + date + '_' + time
    await this.setState({
      appendFile: appendFile
    })

    // uploading files when file is selected (not undefined)
    if (this.state.selectedFile !== undefined) {
      if (this.state.selectedFile.size > 10485760) {
        this.props.setAlertMeassage(true, "Size should be less than 10MB", false)
      }
      else {
        
        await this.props.uploadPulpDoc(this.state.selectedFile, company_key_id, this.state.appendFile);
        await this.props.getDocData(company_key_id);
        console.log(this.props.docData)
      }

    }


  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div>
        <Alert
          showAlert={(typeof this.props.showAlert !== "undefined" ? this.props.showAlert : false)}
          message={(typeof this.props.alertMessage !== "undefined" ? this.props.alertMessage : "")}
          handleAlertBox={this.handleAlertBox}
          status={this.props.status}
        />
        <Modal
          isOpen={this.state.openModal}
          style={customStyles}
          ariaHideApp={false}
        >
          <LotModal
            handleLinkModal={this.handleLinkModal}
            totalRecordCount={this.props.totalRecordCount}
            orders={this.props.buyerIncomingOrders} // shipment will be linked to buyer incoming order
            options={options}
            fetchBrandEntities={this.props.fetchBrandEntities}
            linkingFormatter={this.linkingFormatter}
            companyDetail={(!_.isEmpty(this.props.companyDetail)) ? this.props.companyDetail : {}}
            handleDateChange={this.handleDateChange}
            pages={this.props.pages}
            openModal={this.state.openModal}
            resetAlertBox={this.props.resetAlertBox}
            selectedCompany={this.state.selectedCompany}
            companyTypeId={this.state.companyTypeId}
            getParentCompanyDetails={this.props.getParentCompanyDetails}
            getPCompanyData={(!_.isEmpty(this.props.getPCompanyData)) ? this.props.getPCompanyData : {}}
          />
        </Modal>
        <div className='col-md-12 col-lg-12 create-order'>
          <h2>Company Details</h2>
          <div>
            <form onSubmit={handleSubmit}>
              <div className="form-row">
                <div className="form-group col-md-4">
                  <Field
                    name='company_name'
                    type="text"
                    component={renderField}
                    label="Company Name *"
                  />
                </div>
                <div className="form-group col-md-4">
                  <label>Company Type*</label>
                  <Field
                    className="select-menu"
                    name="company_type_name"
                    component={renderDropdownList}
                    data={(!_.isEmpty(this.props.companyType)) ? this.props.companyType.map(function (list) { return list.company_type_name }) : []}
                    customProps={{
                      handleDropdown: this.handleDropdown
                    }}
                  />
                </div>
                {(this.props.isEdit && !this.props.isLinked) &&
                  <div className="form-group col-md-4">
                    <label>Link Parent Organization</label>
                    <Field
                      name='parent_company_id'
                      type='text'
                      component={renderLink}
                      customProps={{
                        handleLinkModal: this.handleLinkModal,
                        parent_company_name: (!_.isEmpty(this.props.viewCompanyInfo)) ? this.props.viewCompanyInfo.parent_company_name : ''
                      }}
                    />
                  </div>}
              </div>
              {(this.props.showRoleCheckbox) &&
                <div className="form-row">
                  <div className="form-group col-md-4">
                    <Field
                      name='hidden'
                      type="hidden"
                      component={renderField}
                      hidden={true}
                    />
                  </div>
                  <div className="form-group col-md-4">
                    <Field
                      name="company_role"
                      type="checkbox"
                      disabled={false}
                      className="company"
                      component={CheckboxGroup}
                      options={this.props.companyRole}
                    />
                  </div>
                </div>
              }
                { this.props.isPulp && this.props.isEdit  && 
                <div className="form-row">
                  
                  <div className="form-group col-md-4">
              
                        <Field
                        name='conversion_factor'
                        type="number"
                        component={renderField}
                        label="Conversion Factor*"
                      />
                    </div>
 
                  <div className="form-group col-md-4">
              
                      <div>
                        <label style={{marginLeft: '4px', marginTop: '8px', fontWeight: '500'}}>List of forest entities</label>
                        <p>(Click on excel icon to upload.)</p>
                        <img onClick = {this.onDocUpload}src={Images.excelImage} style={{ position: 'absolute', top: '9px',left: '170px', width: '17%', cursor: 'pointer' }} />

                      </div>
                    </div>
    
                </div>
                
                }
                {
                  this.props.isPulp && this.props.isEdit  && this.props.docData && this.props.docData.length !==0 && 
                  <div style={{top: '128px', right: '1px', position: 'absolute', width: '40%'}}>
                    <UploadDocTable 
                      data = {this.props.docData}
                      pagination={false}
                      search={false}
                      exportCSV={false}
                      haveAction={false}
                      options = {options}
                      uploadFormatter={this.uploadFormatter}
                    />
                  </div>
                }

                { this.props.isForest && this.props.isEdit  && 
                <div className="form-row">
                <div className="form-group col-md-4">
                  <label>Species*</label>
                  <Field
                    className="select-menu"
                    name="forest_company"
                    component={renderDropdownForest}
                    data={(!_.isEmpty(this.props.forestDrop)) ? this.props.forestDrop.map(function (list) { return list.product_name }) : []}
                    customProps={{
                      handleForestDrop: this.handleForestDrop
                    }}
                  />
                  </div>
                </div>}
              <h2>Locations Details</h2>
              <div className="form-row">
                <div className="form-group col-md-4">
                  <Field
                    name="country"
                    type="text"
                    label="Country*"
                    component={renderField}
                    onChange={(e) => this.onCountryChange(e)}
                  />
                </div>
                <div className="form-group col-md-4">
                  <Field
                    name="state"
                    type="text"
                    label="State*"
                    component={renderField}
                    onChange={(e) => this.onStateChange(e)}
                  />
                </div>
                <div className="form-group col-md-4">
                  <Field
                    name="city"
                    type="text"
                    label="City*"
                    component={renderField}
                    onChange={(e) => this.onCityChange(e)}
                  />
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md-4">
                  <Field
                    name='zip'
                    type="text"
                    component={renderField}
                    label="Zip*"
                  />
                </div>
                <div className="form-group col-md-4">
                  <Field
                    name='company_address'
                    type="text"
                    component={renderField}
                    label="Address"
                  />
                </div>
              </div>
              <h2>Contact Details</h2>
              <div className="form-row">
                <div className="form-group col-md-4">
                  <Field
                    name='phone_number'
                    type="text"
                    component={renderField}
                    label="Phone"
                  />
                </div>
                <div className="form-group col-md-4">
                  <Field
                    name='email_address'
                    type="email"
                    component={renderField}
                    label="Email"
                  />
                </div>
              </div>
              <div>
                <SubmitButtons
                  submitLabel={(!this.props.isEdit && !this.props.isLinked) ? 'Add' : 'Create'}
                  className='btn btn-primary create'
                  submitting={this.props.submitting}
                />
              </div>
            </form>
            
          </div>
          <div><p style={{paddingTop: '27px'}}>*Disclaimer: In order to link this company to its parent company, click on Add</p></div>
        </div>
        <input
          type="file"
          name='upfile'
          ref={"fileUploader"}
          onChange={this.onFileSelected}
          style={{ display: "none" }}
          onClick={(event) => {
            event.target.value = null
          }}
        />
      </div>
    );
  }
}

AddCompany.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default reduxForm({
  form: 'AddCompany',
  validate,
})(AddCompany)
