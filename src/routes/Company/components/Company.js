import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { Button } from 'reactstrap';
import _ from 'lodash';
import Swal from 'sweetalert2';

import Loader from 'components/Loader';
import options from 'components/options';
import Alert from "components/Alert";
import HeaderSection from 'components/HeaderSection';
import NavDrawer from 'components/NavDrawer';
import { getLocalStorage, getCompanyId, getCompanyTypeId,getForestId, getProductId } from 'components/Helper';
import CompanyDetail from './CompanyDetail';
import AddCompany from './AddCompany';
import ReviewCompany from './ReviewCompany';
import Footer from 'components/Footer';
import 'css/style.scss';
import ReactLoading from 'react-loading';
// import Geocode from "react-geocode";
import opencage from 'opencage-api-client'

/** Company
 *
 * @description This class is a based class for Company stuff
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */

class Company extends React.Component {
  constructor(props) {
    super(props);
    this.confirm = this.confirm.bind(this);
    this.handleAlertBox = this.handleAlertBox.bind(this);
    this.handleInboxOutboxOrders = this.handleInboxOutboxOrders.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.setActivePage = this.setActivePage.bind(this);
    this.openCompanyForm = this.openCompanyForm.bind(this);
    this.companyFormatter = this.companyFormatter.bind(this);
    this.back = this.back.bind(this);
    this.createCompany = this.createCompany.bind(this);
    this.viewCompany = this.viewCompany.bind(this);
    this.editCompany = this.editCompany.bind(this);
    this.deleteCompany = this.deleteCompany.bind(this);
    this.resetFilter = this.resetFilter.bind(this);
    this.confirmExt = this.confirmExt.bind(this)

    this.state = {
      active: 'company',
      fromDt: '',
      toDt: '',
      role: '',
      isSuperUser: '',
      searchText: '',
      previousActivePage: '',
      createCompanyState: true,
      lat: 0,
      lng: 0
    }
    // Geocode.setApiKey("AIzaSyDa8IbCs7c-NM1Yw0eoXAqvxFJ27aQ6s58");
  }


  async componentWillMount() {
    let loginDetail = getLocalStorage('loginDetail');
    await this.setState({
      role: loginDetail.role,
      isSuperUser: loginDetail.userInfo.is_super_user
    });
    this.props.setGlobalState({
      navIndex: 6,
      navInnerIndex: 0,
      navClass: 'Company',
    });
    // getting company details
    this.props.getCompanyDetails();
  }

  componentWillUnmount() {
    this.props.setCurrentNewPage(1)
  }

  async resetFilter() {
    await this.setState({
      fromDt: '',
      toDt: ''
    });
  }

  async handleSearch(query) {
    await this.setState({ searchText: query });
    if (query.length >= 2 || query.length === 0) this.props.getCompanyDetails(this.state.searchText, options.currentPage, options.pageSize);
  }

  async handleAlertBox() {
    this.props.resetAlertBox(false, "");
  }

  handleInboxOutboxOrders(classType, innerType) {
    if (classType === 'orders' && innerType === 'inbox') {
    } else if (classType === 'orders' && innerType === 'outbox') {
    } else if (classType === 'shipments' && innerType === 'inbox') {
    } else if (classType === 'Audit Trail') {
    } else if (classType === 'logout') {
      removeLocalStorage('loginDetail');
      browserHistory.push(Url.HOME_PAGE);
    }
  }

  async setActivePage(state) {
    this.setState({ active: state });
  }

  async openCompanyForm() {
    await this.setState({ previousActivePage: this.state.active });
    await this.setState({ active: 'addCompany' });
    this.props.setProps(true, 'isCompanyFormOpen');
  }

  confirm(id = '', key = '', msg, confirmText, cancelText) {
    Swal({
      title: 'Warning!',
      text: `${msg}`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmText,
      cancelButtonText: cancelText
    }).then(async (result) => {
      this.props.setProps(false, 'isConfirm');
      if (result.value) {
        if (key === 'deleteCompany') {
          await this.props.deleteCompany(id);
          this.props.getCompanyDetails();
        }
      }
    })
  }

  async viewCompany(row) {
    await this.setState({ previousActivePage: this.state.active });
    await this.props.viewCompanyDetail(row.company_key_id);
    await this.props.getCompanyType();
    this.props.setProps(true, 'isReviewCompany');
    this.props.setProps(true, 'isCompanyFormOpen');
  }

  async editCompany(row) {
    await this.setState({ previousActivePage: this.state.active });
    await this.props.viewCompanyDetail(row.company_key_id);
    await this.props.getCompanyType();
    this.props.setProps(true, 'isEdit');
    this.props.setProps(true, 'isCompanyFormOpen');
  }

  deleteCompany(row) {
    this.confirm(row.company_key_id, 'deleteCompany', 'Do you really want to delete', 'Yes delete it', 'No keep it');
  }

  async back() {
    this.setState({ active: this.state.previousActivePage });
    this.props.setProps(false, 'isCompanyFormOpen');
    await this.props.resetDocData();
    await this.props.getParentCompanyDetails('', this.props.currentNewPage, options.modalPageSize, getLocalStorage('companyDetail').company_key_id)
  }

  async createCompany(values) {
    var self = this
    // await Geocode.fromAddress(`${values.country + '' + values.state + '' + values.city}`).then(
    //   response => {
    //     const { lat, lng } = response.results[0].geometry.location;
    //     // console.log(lat);
    //     // console.log(lng);
    //     this.props.setLatitude(lat);
    //     this.props.setLongitude(lng);
    //   },
    //   error => {
    //     console.error(error);
    //   }
    // );

    await opencage
      .geocode({ key: '4fb85e6f207343a1b222fc6ea5dbed54', q: `${values.country + ',' + values.state + ',' + values.city + ',' + values.zip}` })
      .then(response => {
        console.log(response);
        const lat = response.results[0].geometry.lat;
        const lng = response.results[0].geometry.lng;

        this.props.setLatitude(lat);
        this.props.setLongitude(lng);
        
        //this.setState({ response, isSubmitting: false });
      })
      .catch(err => {
        console.error(err);
        //this.setState({ response: {}, isSubmitting: false });
      });

    let company_role_id = '';
    let a123 = 0;

    if (values.company_role !== undefined) {
      if (values.company_type_name === "Integrated Player") {
        values.company_role.sort().forEach(function (abbc, test123) {
          if (test123 + 1 < values.company_role.length) {
            if (values.company_role[test123 + 1] - values.company_role[test123] !== 1) {
              a123 = 1;
            }
          }
        })
        if (a123 === 1) {
          await this.props.setAlertMeassage(true, "Please select continuous roles");
          await this.setState({ createCompanyState: false });
        }
        else if (values.company_role.length < 2) {
          await this.props.setAlertMeassage(true, "Select atleast 2 roles");
          await this.setState({ createCompanyState: false });
        } else {
          values.company_role.map((item, index) => {
            company_role_id += `${item}`;
            if (index !== values.company_role.length - 1) company_role_id
              += ',';
          });
          await this.setState({ createCompanyState: true });
        }
      }
    }
    else {
      if (values.company_type_name === "Integrated Player") {
        await this.props.setAlertMeassage(true, "Select atleast 2 roles");
        await this.setState({ createCompanyState: false });
      }
    }
    if (this.state.createCompanyState) {
      // console.log(this.props.latitude);
      // console.log(this.props.longitude);
      let data = {
        company_name: values.company_name,
        company_type_id: getCompanyTypeId(this.props.companyType, values.company_type_name,
          false),
        country: values.country,
        state: values.state,
        city: values.city,
        zip: values.zip,
        company_address: values.company_address,
        phone_number: values.phone_number,
        email_address: values.email_address,
        company_role_id: company_role_id,
        parent_company_id: (this.props.viewCompanyInfo === undefined || this.props.viewCompanyInfo.parent_company_id === undefined || values.company_type_name === "Integrated Player") ? "0" : this.props.viewCompanyInfo.parent_company_id,
        latitude: this.props.latitude,
        longitude: this.props.longitude,
        conversion_factor: values.conversion_factor
      }

      if (this.props.isEdit) {
        // if(this.props.isPulp){
        //   data.linked_forest_key_id = getForestId(this.props.pulpDrop, values.pulp_company)
        // }
         if(this.props.isForest){
          data.product_id = getProductId(this.props.forestDrop, values.forest_company)
        }
        data.company_key_id = getLocalStorage('companyDetail').company_key_id;
        await this.props.viewCompanyDetail(getLocalStorage('companyDetail').company_key_id);
        await this.props.addCompany(data, this.props.isEdit)
        await this.props.setAlertMeassage(true, "Company created Successfully", true);
        await this.props.setCompanyFormState(false);
        await this.props.setLatitude(0);
        await this.props.setLongitude(0);
      }
      else if (this.props.isLinked) {
        data.company_key_id = getLocalStorage('companyDetail').company_key_id;
        await this.props.addCompany(data, false);
        await this.props.viewCompanyDetail(getLocalStorage('companyDetail').company_key_id);
        await this.props.setAlertMeassage(true, "Company created Successfully", true);
        await this.props.setCompanyFormState(false);
        await this.props.setLatitude(0);
        await this.props.setLongitude(0);
      }
      else {
        await this.props.addCompany(data, this.props.isEdit);
        if(this.props.externalCompany){
          this.confirmExt();
        }
        else{
          await this.props.viewCompanyDetail(getLocalStorage('companyDetail').company_key_id);
          await this.props.setLatitude(0);
          await this.props.setLongitude(0);
          this.props.getCompanyDetails(this.state.searchText, options.currentPage,
            options.pageSize)
        }
       
      }
    }
    await this.props.resetDocData();

  }

  async confirmExt(){
    Swal.fire({
      title: 'This company is registered as an external company',
      text: "Do you want to convert it into regular company",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then(async (result) => {
      if (result.value) {
        await this.props.setEditState(true)
        await this.props.updateExternalCompanyNormal(getLocalStorage('companyDetail').company_key_id)
        await this.props.viewCompanyDetail(getLocalStorage('companyDetail').company_key_id);
        await this.props.setLatitude(0);
        await this.props.setLongitude(0);
        this.props.getCompanyDetails(this.state.searchText, options.currentPage,
          options.pageSize)

        Swal.fire(
          'Okay!',
          'Company is converted',
          'success'
        )

        await this.props.externalCompanyCheck(false)

      }
  
    })
  }


  companyFormatter(row, cell) {
    return (
      <ButtonGroup>
        <Button className="action" onClick={() => this.viewCompany(cell)} >view</Button>
        <Button className="action" onClick={() => this.editCompany(cell)} >edit</Button>
        <Button className="action delete" onClick={() => this.deleteCompany(cell)} >delete</Button>
      </ButtonGroup>
    );
  }

  render() {
    return (
      <div>
        {/* <Loader loading={this.props.fetching} /> */}
        {this.props.fetching &&
          <ReactLoading type="bubbles" style={{
            fill: '#4e5be1e0',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundSize: '200px',
            position: 'fixed',
            zIndex: 10000,
            width: '6%',
            height: '72%',
            top: '50%',
            left: '50%'
          }} />
        }
        <Alert
          showAlert={(typeof this.props.showAlert !== "undefined" ? this.props.showAlert : false)}
          message={(typeof this.props.alertMessage !== "undefined" ? this.props.alertMessage : "")}
          handleAlertBox={this.handleAlertBox}
          status={this.props.status}
        />
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-2 col-lg-2 sidebar">
              <NavDrawer
                role={this.state.role}
                isSuperUser={this.state.isSuperUser}
                setGlobalState={this.props.setGlobalState}
                navIndex={this.props.navIndex}
                navInnerIndex={this.props.navInnerIndex}
                navClass={this.props.navClass}
                handleInboxOutboxOrders={this.handleInboxOutboxOrders}
                currentLanguage={this.props.currentLanguage}
                setInnerNav={this.props.setInnerNav}
                setParam={this.props.setParam}
                setSearchString={this.props.setSearchString}
                setReportFetching={this.props.setReportFetching}
              />
            </div>
            <div className="col-md-10 col-lg-10 col-lg-offset-2 col-md-offset-2 main">
              <HeaderSection
                active={this.state.active}
                searchText={this.state.searchText}
                handleSearch={this.handleSearch}
                currentLanguage={this.props.currentLanguage}
                setCurrentLanguage={this.props.setCurrentLanguage}
              />
              {(!this.props.isCompanyFormOpen) ?
                <div>
                  <CompanyDetail
                    data={this.props.companyDetail}
                    pagination={false}
                    search={false}
                    exportCSV={false}
                    options={options}
                    companyFormatter={this.companyFormatter}
                    getCompanyDetails={this.props.getCompanyDetails}
                    companyDetail={this.props.companyDetail}
                    totalRecordCount={this.props.totalRecordCount}
                    pages={this.props.pages}
                    searchText={this.state.searchText}
                    setCurrentNewPage={this.props.setCurrentNewPage}
                    currentNewPage={this.props.currentNewPage}
                  />
                  <Button className='add-button' hidden={(this.props.isCompanyFormOpen)} onClick={() => this.openCompanyForm()}>+</Button>
                </div>
                :
                <div>
                  {(!this.props.isReviewCompany) ?
                    <AddCompany
                      resetProps={this.props.resetProps}
                      pages={this.props.pages}
                      onSubmit={this.createCompany}
                      setActivePage={this.setActivePage}
                      setProps={this.props.setProps}
                      getCompanyType={this.props.getCompanyType}
                      companyType={this.props.companyType}
                      isEdit={this.props.isEdit}
                      viewCompanyInfo={this.props.viewCompanyInfo}
                      fetchCompanyRole={this.props.fetchCompanyRole}
                      companyRole={this.props.companyRole}
                      showRoleCheckbox={this.props.showRoleCheckbox}
                      handleDropdown={this.handleDropdown}
                      brandEntities={this.props.brandEntities}
                      fetchBrandEntities={this.props.fetchBrandEntities}
                      companyDetail={this.props.companyDetail}
                      isLinked={this.props.isLinked}
                      linkParentCompany={this.props.linkParentCompany}
                      viewCompanyDetail={this.props.viewCompanyDetail}
                      linkedPCompanyDetail={this.props.linkedPCompanyDetail}
                      totalRecordCount={this.props.totalRecordCount}
                      resetAlertBox={this.props.resetAlertBox}
                      getParentCompanyDetails={this.props.getParentCompanyDetails}
                      getPCompanyData={(!_.isEmpty(this.props.getPCompanyData)) ? this.props.getPCompanyData : {}}
                      setLatitude={this.props.setLatitude}
                      setLongitude={this.props.setLongitude}
                      getPulpCompany={this.props.getPulpCompany}
                      pulpDrop={this.props.pulpDrop}
                      isPulp={this.props.isPulp}
                      getForestCompany={this.props.getForestCompany}
                      isForest={this.props.isForest}
                      forestDrop={this.props.forestDrop}
                      uploadPulpDoc={this.props.uploadPulpDoc}
                      getDocData={this.props.getDocData}
                      docData={this.props.docData}
                      downloadDoc={this.props.downloadDoc}
                      resetDocData={this.props.resetDocData}
                      deleteDoc={this.props.deleteDoc}
                    />
                    :
                    <ReviewCompany
                      isLinked={this.props.isLinked}
                      setActivePage={this.setActivePage}
                      setProps={this.props.setProps}
                      isEdit={this.props.isEdit}
                      viewCompanyInfo={this.props.viewCompanyInfo}
                      fetchCompanyRole={this.props.fetchCompanyRole}
                      companyRole={this.props.companyRole}
                      showRoleCheckbox={this.props.showRoleCheckbox}
                      getDocData={this.props.getDocData}
                      docData={this.props.docData}
                      downloadDoc={this.props.downloadDoc}
                      resetDocData={this.props.resetDocData}
                      deleteDoc={this.props.deleteDoc}

                    />
                  }
                  <div className='btn-right'>
                    <Button className={(this.props.isReviewCompany) ? "btn btn-primary reviewCancel" : "btn btn-primary  create cancel"} onClick={this.back}>Back</Button>
                  </div>
                </div>
              }
              <div className="clearfix"></div>
              <Footer
                currentLanguage={this.props.currentLanguage}

              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Company.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default Company;