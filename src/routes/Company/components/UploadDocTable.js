import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

//localization
import LocalizedStrings from 'react-localization';
import data from '../../../localization/data';
let strings = new LocalizedStrings(
  data
);

const columnHover = (cell, row, enumObject, rowIndex) => {
  return cell
}


/** AddShipmentTable
 *
 * @description This class is responsible to display added orderd as outgoing shipment
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class UploadDocTable extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    // const { currentLanguage } = this.props;
    // strings.setLanguage(currentLanguage);

    return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.options}>
          <TableHeaderColumn dataField='company_key_id' hidden={true}>company_key_id</TableHeaderColumn>
          <TableHeaderColumn dataField='created_date' hidden={true} >created_date</TableHeaderColumn>
          <TableHeaderColumn dataField='document_file_name' hidden={true} isKey={true} autoValue={true} >document_file_name</TableHeaderColumn>
          <TableHeaderColumn dataField='document_name' tooltip={true} columnTitle={columnHover}>Document Name</TableHeaderColumn>
          <TableHeaderColumn dataField='linked_forest_document_id' hidden={true}>linked_forest_document_id</TableHeaderColumn>
          <TableHeaderColumn key="editAction" dataFormat= {this.props.uploadFormatter}>Action</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}
UploadDocTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  options: PropTypes.object.isRequired,
};

export default UploadDocTable;