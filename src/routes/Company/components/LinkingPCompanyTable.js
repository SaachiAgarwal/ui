import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Pagination from 'react-paginate';
import { getLocalStorage } from 'components/Helper';



/** LinkingOrdersTable
 *
 * @description This class is responsible to display a orders list of incoming orders for link model
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class LinkingOrderTable extends Component {
  constructor(props) {
    super(props);
    this.handlePageClick = this.handlePageClick.bind(this);
  }
  
  static defaultProps = {
    linkOrderOptions: {
      sizePerPage: 5,
      pageSize: 5,
      pageStartIndex: 1,
      paginationSize: 3,
      prePage: 'Prev',
      nextPage: 'Next',
      firstPage: 'First',
      lastPage: 'Last',
      clearSearch: true,
      hideSizePerPage: true,
      onRowClick: this.editFormatter
    }
  }

  handlePageClick(value) {
    this.props.getParentCompanyDetails(this.props.filterText, value.selected + 1, this.props.options.modalPageSize, getLocalStorage('companyDetail').company_key_id);
  }

  render() {
    return (
      <div className='ordertable'>
        <BootstrapTable id="exportTable" data={this.props.data} pagination={this.props.pagination} search={this.props.search} exportCSV={this.props.csv} options={this.props.linkOrderOptions} cellEdit={{
          mode: 'click', blurToSave: true,
        }}>
          <TableHeaderColumn dataField='company_id' hidden={true}>Order Id</TableHeaderColumn>
          <TableHeaderColumn dataField='company_name' isKey={true}>Company Name</TableHeaderColumn>
          <TableHeaderColumn key="editAction" dataFormat={this.props.linkingFormatter}>Action</TableHeaderColumn>
        </BootstrapTable>
        <div className="pagination-box">
          <Pagination
            previousLabel={"Prev"}
            nextLabel={"Next"}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={this.props.pages}
            pageRangeDisplayed={5}
            onPageChange={this.handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        </div>
      </div>
    );
  }
}

LinkingOrderTable.propTypes = {
  data: PropTypes.array.isRequired,
  pagination: PropTypes.bool,
  search: PropTypes.bool,
  exportCSV: PropTypes.bool,
  linkShipmentOptions: PropTypes.object.isRequired,
};

export default LinkingOrderTable;