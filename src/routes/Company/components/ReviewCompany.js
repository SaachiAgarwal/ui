import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { reduxForm, Field, SubmissionError, FieldArray } from 'redux-form';
import { Button } from 'reactstrap';
import _ from 'lodash';

import renderOnly from 'components/renderOnly';
import SubmitButtons from 'components/SubmitButtons';
import { getCompanyTypeId, getCompanyId } from 'components/Helper';
import CheckboxGroup from 'components/CheckboxGroup';
import UploadDocTable from './UploadDocTable'
import { Images } from 'config/Config';
import options from 'components/options';
/** ReviewCompany
 *
 * @description This class is a based class to view company detail
 *
 * @author Ketan Kumar<ketan@incaendo.com>
 * @link http://www.incaendo.com
 * @copyright (c) 2018, Grant Thornton India LLP.
 */
class ReviewCompany extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      initData: {},
    }
    this.uploadFormatter = this.uploadFormatter.bind(this)
    this.onDownloadDoc = this.onDownloadDoc.bind(this)
    this.onDeleteDoc = this.onDeleteDoc.bind(this)
  }

  componentWillMount() {
    this.props.resetDocData();
    this.props.setActivePage('reviewCompany');
    this.props.fetchCompanyRole(this.props.viewCompanyInfo.company_type_name);
  }

  async componentDidMount() {
    if(this.props.viewCompanyInfo.company_key_id !== undefined && this.props.viewCompanyInfo.company_key_id !== '' && this.props.viewCompanyInfo.length !==0){
      await this.props.getDocData(this.props.viewCompanyInfo.company_key_id);
      console.log(this.props.docData)
   }
    this.handleInitialize();
  }

  uploadFormatter(row, cell) {
    return (
      <ButtonGroup >
        <Button className="action" title="view" style={{ paddingRight: '0px', paddingLeft: '1px' }} onClick={() => this.onDownloadDoc(cell)}>Download</Button>
        <Button className="action" title="approve" style={{ paddingRight: '0px' }} onClick={() => this.onDeleteDoc(cell)}>Delete</Button>
      </ButtonGroup>
    );
  }

  async onDownloadDoc(cell){
    console.log(cell)
    this.props.downloadDoc(cell.document_file_name, cell.document_name)
  }

  async onDeleteDoc(cell){
    await this.props.deleteDoc(cell.linked_forest_document_id, cell.document_file_name)
    await this.props.getDocData(this.props.viewCompanyInfo.company_key_id);
    console.log(cell)
  }

  handleInitialize() {
    if (!_.isEmpty(this.props.viewCompanyInfo)) {
      let roles = [];
      if (this.props.viewCompanyInfo.roles !== undefined) {// roles (key) must not be undefined
        this.props.viewCompanyInfo.roles.map((item) => {
          roles.push(item.company_role_id); // preparing roles for integrated player to open form in edit state
        });
      }
      // let forest_company_name = this.props.viewCompanyInfo.forest_company_name || ''
      let forest_product_name = this.props.viewCompanyInfo.forest_product_name || ''
      let conversion_factor = this.props.viewCompanyInfo.conversion_factor || ''
      const initData = {
        company_name: this.props.viewCompanyInfo.company_name,
        company_type_name: this.props.viewCompanyInfo.company_type_name,
        country: this.props.viewCompanyInfo.country,
        state: this.props.viewCompanyInfo.state,
        city: this.props.viewCompanyInfo.city,
        zip: this.props.viewCompanyInfo.zip,
        company_address: this.props.viewCompanyInfo.company_address,
        phone_number: this.props.viewCompanyInfo.phone_number,
        email_address: this.props.viewCompanyInfo.email_address,
        company_role: roles,
        parent_company_name:this.props.viewCompanyInfo.parent_company_name,
        // forest_company_name: forest_company_name,
        forest_product_name: forest_product_name,
        conversion_factor: conversion_factor
      };

      this.setState({ initData: initData });
      this.props.initialize(initData);
    }
  }

  componentWillUnmount() {
    this.props.setProps(false, 'isReviewCompany');
    this.props.setProps(false, 'isCompanyFormOpen');
    this.props.setProps(false, 'showRoleCheckbox');
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div className='col-md-12 col-lg-12 create-order'>
        <h2>Company Details</h2>
        <div>
          <form onSubmit={handleSubmit}>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  className="select-menu"
                  name="company_name"
                  label="Company Name"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.company_name : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  className="select-menu"
                  name="company_type_name"
                  label="Company Type"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.company_type_name : ''
                  }}
                />
              </div>
              {(this.state.initData.company_type_name !=="Integrated Player") &&
                <div className="form-group col-md-4">
                  <Field
                    className="select-menu"
                    name="parent_company_name"
                    label="Company Type"
                    label='Link Parent Organization'
                    component={renderOnly}
                    customProps={{
                      value: (!_.isEmpty(this.state.initData)) ? this.state.initData.parent_company_name : ''
                    }}
                  />
                </div>}
            </div>
            
            {/* {(this.state.initData.forest_company_name ) &&
            <div className="form-row">
                <div className="form-group col-md-4">
                  <Field
                    className="select-menu"
                    name="forest_company_name"
                    label='Linked Forest'
                    component={renderOnly}
                    customProps={{
                      value: (!_.isEmpty(this.state.initData)) ? this.state.initData.forest_company_name : ''
                    }}
                  />
                </div>
                <div>
                <label style={{marginLeft: '43px', marginTop: '8px', fontWeight: '500'}}>List of forest entities</label>
                <img src={Images.excelImage} style={{ position: 'absolute', top: '175px', left: '404px', width: '5%' }} />

                </div>

              </div>
                } */}
                <div className="form-row">
                { this.props.viewCompanyInfo.conversion_factor && 
                      <div className="form-group col-md-4">
                
                          <Field
                          name='conversion_factor'
                          type="number"
                          component={renderOnly}
                          label="Conversion Factor*"
                        />
                      </div>
                   }
                  <div className="form-group col-md-4">
                      {
                        this.props.viewCompanyInfo.company_type_name === "Pulp" && this.props.docData && this.props.docData.length !== 0 &&
                        <div style={{marginLeft: '-14px'}}>
                          <label style={{paddingLeft: '5%', marginBottom: '-7px'}}>List of forest entities</label>
                          <UploadDocTable 
                            data = {this.props.docData}
                            pagination={false}
                            search={false}
                            exportCSV={false}
                            haveAction={false}
                            options = {options}
                            uploadFormatter={this.uploadFormatter}
                          />
                        </div>
                      }
                  </div>
                  
                </div>
                {(this.state.initData.forest_product_name ) &&
                <div className="form-row">
                <div className="form-group col-md-4">
                  <Field
                    className="select-menu"
                    name="forest_product_name"
                    label='Species'
                    component={renderOnly}
                    customProps={{
                      value: (!_.isEmpty(this.state.initData)) ? this.state.initData.forest_product_name : ''
                    }}
                  />
                </div>
              </div>
                }
             
            {(this.props.showRoleCheckbox) &&
              <div className="form-row">
                <div className="form-group col-md-4">
                  <Field
                    name='hidden'
                    type="hidden"
                    component={renderOnly}
                    hidden={true}
                  />
                </div>
                <div className="form-group col-md-4">
                  <Field
                    name="company_role"
                    type="checkbox"
                    disabled={true}
                    component={CheckboxGroup}
                    options={this.props.companyRole}
                  />
                </div>
              </div>
            }
            <h2>Location Details</h2>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='country'
                  type="text"
                  label="Country"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.country : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name='state'
                  type="text"
                  label="State"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.state : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name="city"
                  type="text"
                  label="City"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.city : ''
                  }}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='zip'
                  type="text"
                  label="Zip"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.zip : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name='company_address'
                  type="text"
                  label="Address"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.company_address : ''
                  }}
                />
              </div>
            </div>
            <h2>Contact Details</h2>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Field
                  name='phone_number'
                  type="text"
                  label="Phone"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.phone_number : ''
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <Field
                  name='email_address'
                  type="email"
                  label="Email"
                  component={renderOnly}
                  customProps={{
                    value: (!_.isEmpty(this.state.initData)) ? this.state.initData.email_address : ''
                  }}
                />
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

ReviewCompany.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool
}

export default reduxForm({
  form: 'ReviewCompany',
})(ReviewCompany)