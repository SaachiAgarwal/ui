import axios from 'axios';
import { browserHistory } from 'react-router';
import Swal from 'sweetalert2';
import { Config, Url } from 'config/Config';
import { getLocalStorage, saveLocalStorage, getToken, getPages, addFullName } from 'components/Helper';
// ------------------------------------
// Constant
// ------------------------------------
export const SET_ALERT_MESSAGE = 'SET_ALERT_MESSAGE';
export const FETCHING = 'FETCHING';
export const ERROR = 'ERROR';
export const SET_TOTAL_RECORDS_COUNT = 'SET_TOTAL_RECORDS_COUNT';
export const SET_TOTAL_PAGES = 'SET_TOTAL_PAGES';
export const COMPANY_RECEIVED_SUCCES = 'COMPANY_RECEIVED_SUCCES';
export const SET_COMPANY_FORM_STATE = 'SET_COMPANY_FORM_STATE';
export const SET_REVIEW_COMPANY_STATE = 'SET_REVIEW_COMPANY_STATE';
export const SET_EDIT_STATE = 'SET_EDIT_STATE';
export const SET_COMPANY_TYPE = 'SET_COMPANY_TYPE';
export const SET_COMPANY_DETAIL = 'SET_COMPANY_DETAIL';
export const COMPANY_ROLE_SUCCESS = 'COMPANY_ROLE_SUCCESS';
export const SET_ROLE_CHECKBOX_STATE = 'SET_ROLE_CHECKBOX_STATE';
export const BRAND_ENTITIES_SUCCESS = 'BRAND_ENTITIES_SUCCESS';
export const LINKING_PCOMPANY_SUCCESS = 'LINKING_PCOMPANY_SUCCESS';
export const COMPANY_TYPE_SUCCESS = 'COMPANY_TYPE_SUCCESS';
export const PARENT_COMPANY_DATA_SUCCESS = 'PARENT_COMPANY_DATA_SUCCESS';
export const SET_LINKED_STATE = 'SET_LINKED_STATE';
export const RESET_PROPS = 'RESET_PROPS';
export const SET_CURRENT_NEW_PAGE = 'SET_CURRENT_NEW_PAGE'
export const SET_LATITUDE = 'SET_LATITUDE'
export const SET_LONGITUDE = 'SET_LONGITUDE'
export const EXTERNAL_COMPANY = 'EXTERNAL_COMPANY'
export const PULP_COMPANY = 'PULP_COMPANY'
export const IS_PULP = 'IS_PULP'
export const IS_FOREST = 'IS_FOREST'
export const FOREST_COMPANY = 'FOREST_COMPANY'
export const UPLOAD_DOC_DATE = 'UPLOAD_DOC_DATE'
export const RESET_DOC_DATA = 'RESET_DOC_DATA'
// ------------------------------------
// Actions
// ------------------------------------
export function setAlertMeassage(status, message, orderStatus = false) {
  return {
    type: SET_ALERT_MESSAGE,
    showAlert: status,
    alertMessage: message,
    status: orderStatus
  };
}

export function fetching(status) {
  return {
    type: FETCHING,
    fetching: status,
  };
}

export function errorFetching(status) {
  return {
    type: ERROR,
    error: status,
  };
}

export function setTotalRecordCount(payload) {
  return {
    type: SET_TOTAL_RECORDS_COUNT,
    totalRecordCount: payload,
  };
}

export function setTotalPages(payload) {
  return {
    type: SET_TOTAL_PAGES,
    pages: payload,
  };
}

export function companyReceivedSuccess(payload) {
  return {
    type: COMPANY_RECEIVED_SUCCES,
    companyDetail: payload,
  };
}

export function setCompanyFormState(status) {
  return {
    type: SET_COMPANY_FORM_STATE,
    isCompanyFormOpen: status,
  };
}

export function setReviewCompanyState(status) {
  return {
    type: SET_REVIEW_COMPANY_STATE,
    isReviewCompany: status,
  };
}

export function setEditState(status) {
  return {
    type: SET_EDIT_STATE,
    isEdit: status,
  };
}

export function setLinkedState(status) {
  return {
    type: SET_LINKED_STATE,
    isLinked: status,
  };
}

export function fetchingCompanyTypeSuccess(payload) {
  return {
    type: SET_COMPANY_TYPE,
    companyType: payload,
  };
}

export function companyTypeSuccess(payload) {
  return {
    type: COMPANY_TYPE_SUCCESS,
    companyDetail: payload,
  };
}

export function setCompanyDetail(payload) {
  return {
    type: SET_COMPANY_DETAIL,
    viewCompanyInfo: payload,
  };
}

export function companyRoleSuccess(payload) {
  return {
    type: COMPANY_ROLE_SUCCESS,
    companyRole: payload,
  };
}

export function setRoleCheckboxState(status) {
  return {
    type: SET_ROLE_CHECKBOX_STATE,
    showRoleCheckbox: status,
  };
}

export function fetchBrandEntitiesSuccess(payload) {
  return {
    type: BRAND_ENTITIES_SUCCESS,
    brandEntities: payload,
  }
}

export function linkingPCompanySuccess(payload) {
  return {
    type: LINKING_PCOMPANY_SUCCESS,
    linkedPCompanyDetail: payload
  }
}

export function companyDataReceivedSuccess(payload) {
  return {
    type: PARENT_COMPANY_DATA_SUCCESS,
    getPCompanyData: payload
  }
}

export function resetAlreadySetProps() {
  return {
    type: RESET_PROPS,
    viewCompanyInfo: []
  };
}
export function setCurrentNewPage(payload) {
  return {
    type: SET_CURRENT_NEW_PAGE,
    currentNewPage: payload
  }
}
export function setLatitude(payload) {
  return {
    type: SET_LATITUDE,
    latitude: payload
  }
}
export function setLongitude(payload) {
  return {
    type: SET_LONGITUDE,
    longitude: payload
  }
}
export function externalCompanyCheck(payload) {
  return {
    type: EXTERNAL_COMPANY,
    externalCompany: payload
  }
}
export function pulpCompany(payload){
  return{
    type: PULP_COMPANY,
    pulpDrop: payload
  }
}
export function setIsPulp(payload){
  return{
    type: IS_PULP,
    isPulp: payload
  }
}
export function setIsForest(payload){
  return{
    type: IS_FOREST,
    isForest: payload
  }
}
export function forestCompany(payload){
  return{
    type: FOREST_COMPANY,
    forestDrop: payload
  }
}

export function uploadDocData(payload){
  return{
    type: UPLOAD_DOC_DATE,
    docData: payload
  }
}

export function resetDocData(){
  return{
    type: RESET_DOC_DATA,
    docData: ''
  } 
}
// ------------------------------------
// Action creators
// ------------------------------------
export const resetAlertBox = (showAlert, message) => {
  return (dispatch) => {
    dispatch(setAlertMeassage(showAlert, message));
  }
}

export const setProps = (status, key) => {
  return (dispatch) => {
    switch (key) {
      case 'isCompanyFormOpen':
        dispatch(setCompanyFormState(status));
        break;
      case 'isReviewCompany':
        dispatch(setReviewCompanyState(status));
        break;
      case 'isEdit':
        dispatch(setEditState(status));
        break;
      case 'showRoleCheckbox':
        dispatch(setRoleCheckboxState(status));
        break;
      case 'isLinked':
        dispatch(setLinkedState(status));
        break;
      case 'isPulp':
        dispatch(setIsPulp(status));
        break;
      case 'isForest':
        dispatch(setIsForest(status));
        break;
    }
  }
}

export const fetchBrandEntities = () => {
  return (dispatch) => {
    let endPoint = 'masters/get_all_brands';
    let method = 'get';

    return new Promise((resolve, reject) => {
      axios({
        method: method,
        url: Config.url + endPoint,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(fetchBrandEntitiesSuccess(response.data.data));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        console.log(error)
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'There is no brand entity.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}

export const getParentCompanyDetails = (query = '', currentPage = 1, pageSize = 5, companykeyid = '') => {
  return (dispatch) => {
    if (!query) dispatch(fetching(true));
    let requestMethod = 'post';
    let data = {};
    if (query !== '') data = { search_text: query }
    data = {
      search_text: query,
      page_size: pageSize,
      page_number: currentPage,
      company_key_id: companykeyid
    }

    let endPoint = 'admin/list_companies_by_companyid';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          let totalPage = getPages(response.data.total_recordcount, pageSize);
          if (response.data.total_recordcount !== 0) dispatch(companyDataReceivedSuccess(response.data.data));
          dispatch(setTotalRecordCount(response.data.total_recordcount));
          dispatch(setTotalPages(totalPage));
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'No company found.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const getCompanyDetails = (query = '', currentPage = 1, pageSize = 10) => {
  return (dispatch) => {
    if (!query) dispatch(fetching(true));
    let requestMethod = 'post';
    let data = {};
    if (query !== '') data = { search_text: query }
    data = {
      search_text: query,
      page_size: pageSize,
      page_number: currentPage
    }

    let endPoint = 'admin/list_companies';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          let totalPage = getPages(response.data.total_recordcount);
          dispatch(companyReceivedSuccess(response.data.data));
          dispatch(setTotalRecordCount(response.data.total_recordcount));
          dispatch(setTotalPages(totalPage));
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'No company found.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const getCompanyType = () => {
  return (dispatch) => {
    dispatch(fetching(true));
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: `${Config.url}users/get_company_type`,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(fetchingCompanyTypeSuccess(response.data));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'There is no company type.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}
export const linkParentCompany = (linkParentCompData, isLinked) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = (isLinked === true) ? 'admin/update_company' : 'admin/create_company';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: linkParentCompData,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(linkingPCompanySuccess(response.data));
          if (typeof response.data.data !== 'undefined') {
            // dispatch(setAlertMeassage(true, response.data.data, true));
          }
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const addCompany = (values, isEdit) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let requestMethod = 'post';
    let endPoint = (!isEdit) ? 'admin/create_company' : 'admin/update_company';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: values,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {

          if (response.data.data === "Company is registered as an external company") {
            dispatch(fetching(false));
            console.log(response.data.company_key_id)
            // dispatch(setEditState(true));
            let company_key = response.data.company_key_id;
            if (typeof company_key !== 'undefined' && typeof company_key !== 'undefined') {
              let lStorage = {
                company_key_id: company_key
              }
              saveLocalStorage('companyDetail', lStorage);
            }
            dispatch(externalCompanyCheck(true))
          }
          else {
            dispatch(fetching(false));
            dispatch(setAlertMeassage(true, response.data.data, false));
          }

        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          //dispatch(setCompanyFormState(false));
          dispatch(setEditState(true));
          let company_key = response.data.data[0].company_key_id;
          if (typeof company_key !== 'undefined' && typeof company_key !== 'undefined') {
            let lStorage = {
              company_key_id: company_key
            }
            saveLocalStorage('companyDetail', lStorage);
          }

          //dispatch(setAlertMeassage(true, response.data.data, true));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'Company can not be added.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const viewCompanyDetail = (companyId) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let requestMethod = 'get';
    let endPoint = 'admin/company_detail';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        params: {
          company_key_id: companyId
        },
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          //dispatch(setCompanyFormState(false));
          if (response.data.data.roles !== undefined) dispatch(setRoleCheckboxState(true));
          let lStorage = {
            company_name: response.data.data.company_name,
            company_type_id: response.data.data.company_type_id,
            company_key_id: response.data.data.company_key_id
          }
          saveLocalStorage('companyDetail', lStorage);
          dispatch(setCompanyDetail(response.data.data));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'There is no company type.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const deleteCompany = (companyId) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let requestMethod = 'post';
    let endPoint = 'admin/delete_company';
    return new Promise((resolve, reject) => {
      axios({
        method: requestMethod,
        url: Config.url + endPoint,
        data: {
          company_key_id: companyId
        },
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, true));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'company can not be deleted.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const fetchCompanyRole = (company) => {
  return (dispatch) => {
    let requestMethod = 'get';
    let endPoint = 'admin/company_role';
    if (company === 'Integrated Player') {
      return new Promise((resolve, reject) => {
        axios({
          method: requestMethod,
          url: Config.url + endPoint,
          headers: { 'token': getToken() }
        }).then(response => {
          if (response.data.error === 1) {
            dispatch(fetching(false));
            dispatch(setAlertMeassage(true, response.data.data, false));
          } else if (response.data.error === 5) {
            dispatch(fetching(false));
            dispatch(setAlertMeassage(true, response.data.data, false));
            browserHistory.push(Url.LOGIN_PAGE);
          } else if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
            dispatch(fetching(false));
            dispatch(companyRoleSuccess(response.data.data));
            dispatch(setRoleCheckboxState(true));
          } else if (response.data.error === 1) {
            dispatch(fetching(false));
            dispatch(setAlertMeassage(true, response.data.data, false));
          }
          resolve(true);
        }).catch(error => {
          console.log(error);
          dispatch(fetching(false));
          dispatch(errorFetching(true));
          // dispatch(setAlertMeassage(true, 'Role can not be fetched.', false));
          browserHistory.push(Url.ERRORS_PAGE);
          reject(true);
        })
      });
    } else {
      dispatch(setRoleCheckboxState(false));
    }
  }
}

export const updateExternalCompanyNormal = (company_key_id) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'admin/update_external_company_to_normal';
    let data = {
      company_key_id: company_key_id
    }
    return new Promise((resolve, reject) => {
      axios({
        method: 'put',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}
export const getPulpCompany = () => {
  return (dispatch) => {
    dispatch(fetching(true));
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: `${Config.url}users/get_company`,
        params: {
          company_type_id: 11
        },
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response);
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(pulpCompany(response.data))
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'There is no company type.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}


export const getForestCompany = () => {
  return (dispatch) => {
    dispatch(fetching(true));
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: `${Config.url}masters/get_product_by_type_id`,
        params: {
          product_type_id: 7
        },
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response);
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(forestCompany(response.data))
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'There is no company type.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}


export const uploadPulpDoc = (file, company_key_id, appendFile) => {
  return (dispatch) => {
    dispatch(fetching(true));
    // console.log(file);
    // console.log(owner);
    // console.log(shipment_id);
    // console.log(appendFile);
    const data = new FormData();
    data.append('upfile', file);
    data.append('company_key_id', company_key_id);
    data.append('document_filename', appendFile)
    // console.log(data)
    //debugger
    let endPoint = 'admin/upload_linked_forest';
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: {
          'token': getToken(),
          'Content-Type': 'multipart/form-data'
        }
      }).then(response => {
        console.log('response ', response);
        if (!_.isEmpty(response) && typeof response.data !== 'undefined' && response.data.error !== 1) {
          dispatch(fetching(false));
          // dispatch(setAlertMeassage(true, response.data.data, true));
        } else if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
      })
    });
  }
}
export const getDocData = (company_key_id) => {
  return (dispatch) => {
    let data = {
      company_key_id: company_key_id
    }
    dispatch(fetching(true));
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: `${Config.url}admin/get_forest_file`,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response);
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          dispatch(uploadDocData(response.data.documents));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'There is no company type.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const downloadDoc = (document_file_name, document_name) => {
  return (dispatch) => {
    dispatch(fetching(true));
    let endPoint = 'admin/download_linked_forest_file';
    let data = {
      file_name: document_file_name
    };
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: Config.url + endPoint,
        data: data,
        headers: { 'token': getToken() },
        responseType: 'blob', 
      }).then(response => {
        console.log(response)
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          link.href = url;
          link.setAttribute('download', document_name);
          document.body.appendChild(link);
          link.click();

          resolve(true);
        }
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      });
    });
  }
}



export const deleteDoc = (linked_forest_document_id, document_file_name) => {
  return (dispatch) => {
    let data = {
      linked_forest_document_id: linked_forest_document_id,
      document_file_name: document_file_name
    }
    dispatch(fetching(true));
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: `${Config.url}admin/delete_linked_forest_document`,
        data: data,
        headers: { 'token': getToken() }
      }).then(response => {
        console.log(response);
        if (response.data.error === 1) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
        } else if (response.data.error === 5) {
          dispatch(fetching(false));
          dispatch(setAlertMeassage(true, response.data.data, false));
          browserHistory.push(Url.LOGIN_PAGE);
        } else if (!_.isEmpty(response) && typeof response.data !== 'undefined') {
          dispatch(fetching(false));
        }
        resolve(true);
      }).catch(error => {
        dispatch(fetching(false));
        dispatch(errorFetching(true));
        // dispatch(setAlertMeassage(true, 'There is no company type.', false));
        browserHistory.push(Url.ERRORS_PAGE);
        reject(true);
      })
    });
  }
}

export const resetProps = () => {
  return (dispatch) => {
    dispatch(resetAlreadySetProps());
  }
}


export const action = {
  resetAlertBox,
  setProps,
  getCompanyDetails,
  fetchBrandEntities,
  linkParentCompany,
  getParentCompanyDetails
};

// ------------------------------------
// Action handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_ALERT_MESSAGE]: (state, action) => {
    return {
      ...state,
      showAlert: action.showAlert,
      alertMessage: action.alertMessage,
      status: action.status
    }
  },
  [FETCHING]: (state, action) => {
    return {
      ...state,
      fetching: action.fetching
    }
  },
  [ERROR]: (state, action) => {
    return {
      ...state,
      error: action.error
    }
  },
  [SET_TOTAL_RECORDS_COUNT]: (state, action) => {
    return {
      ...state,
      totalRecordCount: action.totalRecordCount
    }
  },
  [SET_TOTAL_PAGES]: (state, action) => {
    return {
      ...state,
      pages: action.pages
    }
  },
  [COMPANY_RECEIVED_SUCCES]: (state, action) => {
    return {
      ...state,
      companyDetail: action.companyDetail
    }
  },
  [SET_COMPANY_FORM_STATE]: (state, action) => {
    return {
      ...state,
      isCompanyFormOpen: action.isCompanyFormOpen
    }
  },
  [SET_REVIEW_COMPANY_STATE]: (state, action) => {
    return {
      ...state,
      isReviewCompany: action.isReviewCompany
    }
  },
  [SET_EDIT_STATE]: (state, action) => {
    return {
      ...state,
      isEdit: action.isEdit
    }
  },
  [SET_LINKED_STATE]: (state, action) => {
    return {
      ...state,
      isLinked: action.isLinked
    }
  },
  [SET_COMPANY_TYPE]: (state, action) => {
    return {
      ...state,
      companyType: action.companyType
    }
  },
  [SET_COMPANY_DETAIL]: (state, action) => {
    return {
      ...state,
      viewCompanyInfo: action.viewCompanyInfo
    }
  },
  [COMPANY_ROLE_SUCCESS]: (state, action) => {
    return {
      ...state,
      companyRole: action.companyRole
    }
  },
  [SET_ROLE_CHECKBOX_STATE]: (state, action) => {
    return {
      ...state,
      showRoleCheckbox: action.showRoleCheckbox
    }
  },
  [BRAND_ENTITIES_SUCCESS]: (state, action) => {
    return {
      ...state,
      brandEntities: action.brandEntities,
    }
  },
  [LINKING_PCOMPANY_SUCCESS]: (state, action) => {
    return {
      ...state,
      linkedPCompanyDetail: action.linkedPCompanyDetail,
    }
  },
  [COMPANY_TYPE_SUCCESS]: (state, action) => {
    return {
      ...state,
      companyDetail: action.companyDetail
    }
  },
  [PARENT_COMPANY_DATA_SUCCESS]: (state, action) => {
    return {
      ...state,
      getPCompanyData: action.getPCompanyData
    }
  },
  [RESET_PROPS]: (state, action) => {
    return {
      ...state,
      viewCompanyInfo: action.viewCompanyInfo,
    }
  },
  [SET_CURRENT_NEW_PAGE]: (state, action) => {
    return {
      ...state,
      currentNewPage: action.currentNewPage
    }
  },
  [SET_LATITUDE]: (state, action) => {
    return {
      ...state,
      latitude: action.latitude
    }
  },
  [SET_LONGITUDE]: (state, action) => {
    return {
      ...state,
      longitude: action.longitude
    }
  },
  [EXTERNAL_COMPANY]: (state, action) => {
    return {
      ...state,
      externalCompany: action.externalCompany
    }
  },
  [PULP_COMPANY]: (state, action) => {
    return{
      ...state,
      pulpDrop: action.pulpDrop
    }
  },
  [IS_PULP]: (state, action) => {
    return{
      ...state,
      isPulp: action.isPulp
    }
  },
  [IS_FOREST]: (state, action) => {
    return{
      ...state,
      isForest: action.isForest
    }
  },
  [FOREST_COMPANY]: (state, action) => {
    return{
      ...state,
      forestDrop: action.forestDrop
    }
  },
  [UPLOAD_DOC_DATE]: (state, action) => {
    return{
      ...state,
      docData: action.docData
    }
  },
  [RESET_DOC_DATA]: (state, action) => {
    return{
      ...state,
      docData: action.docData
    }
  }
}



// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  showAlert: false,
  status: false,
  alertMessage: '',
  fetching: false,
  error: false,
  totalRecordCount: 0,
  pages: 1,
  companyDetail: [],
  isCompanyFormOpen: false,
  isReviewCompany: false,
  isEdit: false,
  isLinked: true,
  companyType: [],
  viewCompanyInfo: {},
  companyRole: [],
  showRoleCheckbox: false,
  getPCompanyData: {},
  currentNewPage: 1,
  latitude: 0,
  longitude: 0,
  externalCompany: false,
  pulpDrop: '',
  isPulp: false,
  isForest: false,
  forestDrop: '',
  docData: ''
};

export default function companyReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}