## Invoice Generation
===================

## Requirements
* node `^5.0.0`
* yarn `^0.23.0` or npm `^3.0.0`


## Installation

After confirming that your environment meets the above [requirements](#requirements), you can set up invoice generation project.
```bash
$ git clone http://silos.incaendo.com/incaendo/invoice-generation.git 
$ cd invoice-generation
```


* We are using [react redux directory structure](https://github.com/davezuko/react-redux-starter-kit). Before starting development, it is recommended to have an idea about react redux directory structure.


When that's done, install the project dependencies. It is recommended that you use [Yarn](https://yarnpkg.com/) for deterministic dependency management, but `npm install` will suffice.

```bash
$ yarn  # Install project dependencies (or `npm install`)
```

## Running the Project

After completing the [installation](#installation) step, you're ready to start the project!

```bash
$ yarn start  # Start the development server (or `npm start`)
```

